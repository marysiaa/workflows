#!/usr/bin/env python3

import os
import sys
import argparse

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 2)[0])

from scripts.lib.validator import validate

__version__ = '0.0.1'


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='jakiś sensowny opis, co robi skrypt')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-u', '--url', help="URL to be validated")
    arguments = parser.parse_args()

    print(validate(arguments.url))
