#!/bin/bash

function help() {
  printf "  \n"
  printf "  validate\n"
  printf "  ************\n"
  printf "  \n"
  printf "  Info:\n"
  printf "  -----\n"
  printf "    Status: Beta\n"
  printf "    Last edited: 24-05-2019\n"
  printf "    Last edited by: Katarzyna Kolanek\n"
  printf "    Author(s):\n"
  printf "      + Marcin Piechota, <piechota@intelliseq.pl>, https://gitlab.com/marpiech\n"
  printf "    Maintainer(s):\n"
  printf "      + Katarzyna Kolanek <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw\n"
  printf "    Copyright: Copyright 2019 Intelliseq\n"
  printf "    License: All rights reserved\n"
  printf "  \n"
  printf "  Description:\n"
  printf "  ------------\n"
  printf "  \n"
  printf "  Validates latest version of task\n"
  printf "  \n"
  printf "      -h|--help                      print help\n"
  printf "      -t|--target [path/to/task or module]   build and push docker image. Pushing can be diabled with option -p|--nopush\n"
  printf "      -v|--verbose                   print 'docker build' and 'docker push' logs\n"
  printf "  \n"
}

export -f help

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -h|--help)
    help
    exit 0
    shift # past argument
    ;;
    -v|--verbose)
    export VERBOSE=TRUE
    shift # past argument
    ;;
    -t|--target)
    export TARGET="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

function getpath {
  echo $1 | \
    sed "s|.*$SRC_DIR||" | \
    cut -d '/' --output-delimiter '/' -f1-2 | \
    awk -v path="$(pwd)/$SRC_DIR" '{print path$1}'
}

function gettestpath {
  echo $(getpath $1)"/latest/test.sh" | sed "s|main|test|"
}

function testwdl {
  $(chmod +x $(gettestpath $1))
  $(gettestpath $1)
  echo "tadam" $?
}

function exists {
  if [ ! -d "$(getpath $1)/latest" ]; then
    echo "ERROR: target does not exists"; exit 1;
  fi
}

function validate {
  exists $1
  testwdl $1
}

### run script ###
SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR=$( cd -P "$( dirname "$SOURCE" )" && pwd | xargs dirname)
SRC_DIR="src/main/wdl/"
cd $PROJECT_DIR

if [ ! -z "$TARGET" ]; then
  TARGET=$(printf $TARGET | sed "s/:/\//g")
  validate $TARGET
  exit 0
else
  help
  printf "  ERROR:\n"
  printf "  Target not provided:\n"
  printf "    -t|--target \n"
  printf "  \n"
fi
