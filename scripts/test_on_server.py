#!/usr/bin/python3

import os
import sys
import argparse
import asyncio
import json
from typing import Dict

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 2)[0])

from scripts.lib.my_sh_utils import get_output_from_command_unsafe

__version__ = '0.0.1'


def get_commit() -> str:
    cmd = "git log | grep -m1 '^commit' | cut -f2 -d' '"
    return get_output_from_command_unsafe(cmd)


async def send_data_to_server(data: Dict[str, str], address: str, port: int, loop):
    reader, writer = await asyncio.open_connection(address, port, loop=loop)
    message = json.dumps(data)
    writer.write(message.encode())
    print('Send: %r' % message)

    data = await reader.read(124)
    print('Received: %r' % data.decode())

    print('Close the socket')
    writer.close()
    decoded_message = data.decode()
    if decoded_message != 'Started testing on server':
        raise RuntimeError(decoded_message)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Performs tests on test server')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-p', '--port', type=int, required=True)
    parser.add_argument('-i', '--ip', type=str, required=True)
    arguments = parser.parse_args()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(send_data_to_server({'commit':get_commit()}, arguments.ip, arguments.port, loop))
    loop.close()
