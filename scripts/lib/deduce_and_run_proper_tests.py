#!/usr/bin/python3

import os
import sys
import json
import pathlib
from typing import Dict
from typing import Any
from typing import Iterable

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 3)[0])

from scripts.lib.WDL import create_objects_for_wdl_files
from scripts.lib.WDL import get_new_dockers_and_run_tests
from scripts.lib.my_sh_utils import get_output_and_error_from_command_unsafe
from scripts.lib.my_sh_utils import ProcessData
from scripts.lib.validator import validate
from scripts.lib.WDL import test_container

__version__ = '0.1.0'


# class TestDoesNotExist(RuntimeError):
#
#     def __init__(self, file_location:pathlib.PurePath, test_location:pathlib.PurePath):
#         super().__init__(self, "Couldn't find test for file {}. Tried {}".format(file_location, test_location))
#         self.file_location = file_location
#
#
# def test_for_wdl(path:pathlib.PurePath) -> Dict[pathlib.PurePath, ProcessData]:
#     ret = {}
#     test_path = find_test_for_given_wdl(path)
#     if not test_path.exists():
#         tests_to_run = list(test_path.parent.rglob('test*sh'))
#         if not tests_to_run:
#             raise TestDoesNotExist(path, test_path)
#     else:
#         tests_to_run = [test_path]
#     for test_path in tests_to_run:
#         ret[test_path] = get_output_and_error_from_command_unsafe('bash {}'.format(test_path))
#     return ret


def construct_url(wdl_relative_path:str, branch:str, remote_repo_raw_files_address:str='https://gitlab.com/intelliseq/workflows/raw/') -> str:
    return '{}{}/{}'.format(remote_repo_raw_files_address, branch, wdl_relative_path)


def wdl_url_to_path(url:str, branch:str, remote_repo_raw_files_address:str='https://gitlab.com/intelliseq/workflows/raw/') -> str:
    return url.replace('/{}/'.format(branch), '/').replace(remote_repo_raw_files_address, '').strip('/')


def validate_urls(urls:Iterable[str], proper_code:int=200) -> Dict[str, Any]:
    ret_dict = {}
    for url in urls:
        res = json.loads(validate(url, proper_code))
        if res['code'] == proper_code:
            ret_dict[url] = None
        else:
            ret_dict[url] = res['error_message']
    return ret_dict


def run_tests_based_on_extension(paths: Iterable[str], cloned_project_directory:str, branch_or_tag:str, proper_code:int=200) -> Dict[str, ProcessData]:
    wdl_urls = [construct_url(p, branch_or_tag) for p in paths if p.endswith('.wdl')]
    validated_wdl_urls = validate_urls(wdl_urls, proper_code)
    valid_wdl_paths = [pathlib.PurePath(os.path.abspath(wdl_url_to_path(url, branch_or_tag))) for url, message in validated_wdl_urls.items() if '/wdl/' in url and not message]
    invalid_wdl_paths = {wdl_url_to_path(url, branch_or_tag):message for url, message in validated_wdl_urls.items() if message}
    for wdl_path, message in invalid_wdl_paths.items():
        test_container['Test for {}'.format(wdl_path)] = ProcessData(out='', err=message, code=1)
    wdls, dockers = create_objects_for_wdl_files(valid_wdl_paths, cloned_project_directory)
    get_new_dockers_and_run_tests(wdls.values(), dockers.values())
    #print(test_container)


# def test_for_script(path:pathlib.PurePath):
#     test_path = find_test_for_given_script(path)
#     if test_path.exists():
#         try:
#             output = get_output_from_command_safe(['bash', test_path])
#             print("test {}".format(test_path))
#             print(output)
#         except RuntimeError:
#             print('Runtime error when executing {}'.format(test_path))
#     else:
#         print("no tests for {}".format(path))
#
#
# def find_test_for_given_script(path:pathlib.PurePath) -> pathlib.Path:
#     print(type(pathlib))
#     parts = ['test' if x == 'main' else x for x in path.parts]
#     parts[-1] = parts[-1].replace(pathlib.Path(parts[-1]).suffix, '-test.sh')
#     return pathlib.Path(*parts)


# def find_test_for_given_wdl(path:pathlib.PurePath) -> pathlib.Path:
#     parts = ['test' if x == 'main' else x for x in path.parts]
#     parts[-1] = "test.sh"
#     return pathlib.Path(*parts)
#
#
# if __name__ == '__main__':
#     for path in pathlib.Path('src/main').resolve().rglob('*.py'):
#         if path.name != '__init__.py':
#             run_tests_based_on_extension(path)
#
#     non_existing = []
#     for path in pathlib.Path('src/main/').resolve().rglob('*.wdl'):
#         try:
#             run_tests_based_on_extension(path)
#         except TestDoesNotExist as e:
#             non_existing.append(str(e.file_location))
#     for wdl in non_existing:
#         print(wdl)
