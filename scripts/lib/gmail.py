#!/usr/bin/python3
import os
import base64
import pickle
import google
import datetime
from email.mime.text import MIMEText
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

__version__ = '0.0.1'

SCOPES = ['https://www.googleapis.com/auth/gmail.compose']
TOKEN_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gmail_token.pickle')
SECRETS_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gmail_credentials.json')


# Check the gmail API documentation for details about these files (https://developers.google.com/gmail/api/quickstart/python)


def read_token(path_to_the_token: str = TOKEN_PATH):
    with open(path_to_the_token, 'rb') as token:
        return pickle.load(token)


def get_gmail_credentials(path_to_the_token: str = TOKEN_PATH,
                          secrets_file: str = SECRETS_PATH) -> google.oauth2.credentials.Credentials:
    """
    This function has to be run for the first time with access to a web browser and downloaded secrets file.
    Check the gmail API documentation for details (https://developers.google.com/gmail/api/quickstart/python)
    """
    credentials = None
    if os.path.exists(path_to_the_token):
        credentials = read_token(path_to_the_token)
    # If there are no (valid) credentials available, let the user log in.
    if not credentials or not credentials.valid or not is_token_up_to_date(credentials):
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(secrets_file, SCOPES)
            credentials = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(path_to_the_token, 'wb') as token:
            pickle.dump(credentials, token)
    return credentials


def create_message(sender, to, subject, message_text):
    """Create a message for an email.

    Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    subject: The subject of the email message.
    message_text: The text of the email message.

    Returns:
    An object containing a base64url encoded email object.
    """
    message = MIMEText(message_text)
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    return {'raw': base64.urlsafe_b64encode(message.as_string().encode()).decode()}


def send_message(service, user_id, message):
    """Send an email message.

    Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    message: Message to be sent.

    Returns:
    Sent Message.
    """
    try:
        message = (service.users().messages().send(userId=user_id, body=message)
                   .execute())
        # todo logging
        print('Message Id: %s' % message['id'])
        return message
    except Exception as error:
        # TODO logging
        print('An error occurred: %s' % error)


def authenticate_and_send_email(receiver: str, message_str: str = None, subject: str = 'Popraw kod, kurwa!',
                                sender: str = 'wojciech.galan+test-engine@intelliseq.pl', exception: Exception = None,
                                path_to_the_token: str = TOKEN_PATH, secrets_file: str = SECRETS_PATH):
    credentials = get_gmail_credentials(path_to_the_token=path_to_the_token, secrets_file=secrets_file)
    service = build('gmail', 'v1', credentials=credentials)
    if exception:
        message_str += ('\n' + str(exception))
    message = create_message(sender, receiver, subject, message_str)
    send_message(service, "me", message)


def is_token_up_to_date(token) -> bool:
    # https://developers.google.com/identity/protocols/OAuth2
    return (token.expiry - datetime.datetime.now()) > datetime.timedelta(seconds=5)
