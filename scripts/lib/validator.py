#!/usr/bin/env python3

import json
import requests

__version__ = '0.0.1'


def validate(ulr_raw_wdl: str, proper_code: int = 200) -> str:
    ret_dir = {}
    response = requests.get('http://genetraps.intelliseq.pl:8087/wdl/parse?url={}'.format(ulr_raw_wdl))
    code = response.status_code
    ret_dir['code'] = code
    ret_dir['error_message'] = '' if code == proper_code else response.json()['message']
    return json.dumps(ret_dir)