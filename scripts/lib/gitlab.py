#!/usr/bin/python3
import os
import json
import requests
from typing import Set
from typing import Dict
from typing import List
from typing import Union
from typing import Generator

__version__ = '0.0.1'


class NoMoreDataException(RuntimeError):
    pass


def read_token(fname: str = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gitlab_token.json')) -> str:
    with open(fname) as f:
        return json.load(f)['token']


def get_pipelines(project_id: int, token: str, page_num: int, records_per_page: int) -> List[
    Dict[str, Union[int, str]]]:
    uri = 'https://gitlab.com/api/v4/projects/{}/pipelines?private_token={}&per_page={}&page={}'.format(project_id, token,
                                                                                                      records_per_page,
                                                                                                      page_num)
    response = requests.get(uri)
    try:
        response_content = json.loads(response.text)
        if not response_content:
            raise NoMoreDataException
        if 'error' in response_content:
            raise RuntimeError('Invalid response from gitlab server: {}'.format(response_content))
        return response_content
    except json.decoder.JSONDecodeError:
        # receives HTML instead of json.
        raise NoMoreDataException


def get_pipelines_generator(project_id: int, token: str, records_per_page: int) -> Generator[Dict[str, Union[int, str]], None, None]:
    page_num = 1
    while True:
        try:
            pipelines = get_pipelines(project_id, token, page_num, records_per_page)
        except NoMoreDataException:
            break
        for pipeline in pipelines:
            yield pipeline
        page_num += 1


def pick_successfully_pushed_pipelines_from_given_pipelines(pipelines: List[Dict[str, Union[int, str]]]) -> \
        List[Dict[str, Union[int, str]]]:
    """
    Example pipeline:
    {'id': 119360376,
    'sha': 'c9f556e0b3585ab193b5fa558b5fc101838f84f9',
    'ref': 'dev',
    'status': 'success',
    'created_at': '2020-02-19T17:53:36.065Z',
    'updated_at': '2020-02-19T17:54:56.268Z',
    'web_url': 'https://gitlab.com/intelliseq/workflows/pipelines/119360376'}
    """
    return [pipeline for pipeline in pipelines if pipeline['status'] == 'success']


def get_successfully_pushed_pipelines(project_id: int, token: str) -> Generator[Dict[str, Union[int, str]], None, None]:
    page_num = 1
    while True:
        successful_pipelines = pick_successfully_pushed_pipelines_from_given_pipelines(
            get_pipelines(project_id, token, page_num, 100))
        for successful_pipeline in successful_pipelines:
            yield successful_pipeline
        page_num += 1


def determine_branch(commit: str, project_id: int, token: str) -> str:
    pipelines = get_pipelines_generator(project_id, token, 20)
    for pipeline in pipelines:
        if pipeline['sha'] == commit:
            return pipeline['ref']
    raise RuntimeError


def previous_successfully_pushed_commit_on_the_branch(branch: str, commit: str, project_id: int, token: str) -> str:
    successfully_pushed_pipelines_generator = get_successfully_pushed_pipelines(project_id, token)
    given_commit_found = False
    while True:
        pipeline_data = next(successfully_pushed_pipelines_generator)  # may throw NoMoreDataException
        if not given_commit_found:
            if pipeline_data['ref'] == branch and pipeline_data['sha'] == commit:
                given_commit_found = True
        else:
            if pipeline_data['ref'] == branch:
                return pipeline_data['sha']


def previous_successfully_pushed_commit_on_this_branch_or_merged_branch(branch: str, commit: str, project_id: int,
                                                                        token: str,
                                                                        commits_on_the_branch: Set[str]) -> str:
    successfully_pushed_pipelines_generator = get_successfully_pushed_pipelines(project_id, token)
    commit_found = None
    while True:
        pipeline_data = next(successfully_pushed_pipelines_generator)
        if not commit_found:
            if pipeline_data['ref'] == branch and pipeline_data['sha'] == commit:
                commit_found = pipeline_data['sha']
        else:
            if pipeline_data['sha'] != commit_found and pipeline_data['sha'] in commits_on_the_branch:
                return pipeline_data['sha']


def previous_successfully_pushed_commit(branch: str, commit: str, project_id: int, token: str,
                                        commits_on_the_branch: Set[str]) -> str:
    try:
        return previous_successfully_pushed_commit_on_the_branch(branch, commit, project_id, token)
    except NoMoreDataException:
        return previous_successfully_pushed_commit_on_this_branch_or_merged_branch(branch, commit, project_id, token,
                                                                                   commits_on_the_branch)


if __name__ == '__main__':
    token = read_token()
    i = 0
    successfully_pushed_pipelines_generator = get_successfully_pushed_pipelines(10410163, token)
    while i < 2000:
        print(i, next(successfully_pushed_pipelines_generator))
        i += 1
