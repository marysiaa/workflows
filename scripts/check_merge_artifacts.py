#!/usr/bin/python3

import os
import argparse
import pathlib
from typing import List

__version__ = '0.0.1'


def find_merge_artifacts(dir_path: str, strings_to_find: List[str] = ['<<<<<<<', '>>>>>>>']) -> List[str]:
    ret = []
    for path in pathlib.Path(dir_path).rglob('*'):
        if path.is_file() and all(x not in path.parts for x in ['.git', 'venv']) and pathlib.Path(
                __file__).resolve() != path:
            if file_contains_strings(path, strings_to_find):
                ret.append(str(path).replace(workflows_rot_path, ''))
    return ret


def file_contains_strings(path: str, strings_to_find: List[str]) -> bool:
    with open(path) as f:
        for string_to_find in strings_to_find:
            try:
                if string_to_find in f.read():
                    return True
            except UnicodeDecodeError:
                # probably binary file
                return False
    return False


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Looks for merge artifacts in the repository')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    arguments = parser.parse_args()
    script_path = os.path.abspath(os.path.dirname(__file__))
    workflows_rot_path = script_path.rsplit(os.path.sep, 1)[0]
    files_containing_merge_artifacts = find_merge_artifacts(workflows_rot_path)
    if files_containing_merge_artifacts:
        raise RuntimeError('{} file(s) contain merge artifacts'.format(files_containing_merge_artifacts))
