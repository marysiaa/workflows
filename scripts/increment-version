#!/bin/bash

function help() {
  printf "  \n"
  printf "  increment-version\n"
  printf "  ************\n"
  printf "  \n"
  printf "  Info:\n"
  printf "  -----\n"
  printf "    Status: Beta\n"
  printf "    Last edited: 24-05-2019\n"
  printf "    Last edited by: Katarzyna Kolanek\n"
  printf "    Author(s):\n"
  printf "      + Marcin Piechota, <piechota@intelliseq.pl>, https://gitlab.com/marpiech\n"
  printf "    Maintainer(s):\n"
  printf "      + Katarzyna Kolanek <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw\n"
  printf "    Copyright: Copyright 2019 Intelliseq\n"
  printf "    License: All rights reserved\n"
  printf "  \n"
  printf "  Description:\n"
  printf "  ------------\n"
  printf "  \n"
  printf "  Validates latest version of task\n"
  printf "  \n"
  printf "      -h|--help                      print help\n"
  printf "      -t|--target [path/to/task or module]   build and push docker image. Pushing can be diabled with option -p|--nopush\n"
  printf "      -v|--verbose                   print 'docker build' and 'docker push' logs\n"
  printf "      -m|--major                     increment major (e.g. 1.0->2.0 version)\n"
  printf "  \n"
}

export -f help

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -h|--help)
    help
    exit 0
    shift # past argument
    ;;
    -v|--verbose)
    export VERBOSE=TRUE
    shift # past argument
    ;;
    -m|--major)
    export MAJOR=TRUE
    shift # past argument
    ;;
    -t|--target)
    export TARGET="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

function getpath {
  echo $1 | \
    sed "s|.*$SRC_DIR||" | \
    cut -d '/' --output-delimiter '/' -f1-2 | \
    awk -v path="$(pwd)/$SRC_DIR" '{print path$1}'
}

function gettestpath {
  echo $(getpath $1) | sed 's|/main/|/test/|'
}

function getimportpath {
  echo "raw/dev/"$(echo $(getpath $1) | sed 's|.*/workflows/||')"/latest"
}

function incrementpath {
  LATEST_PATH=$(getimportpath $1)
  if ischanged $1; then
    VERSION=$(getincrementedversion $1)
  else
    VERSION="$(getlastversion $1)"
  fi
  VERSIONED_PATH=$(echo $LATEST_PATH | sed 's|raw/dev/|raw/master/|' | sed "s|/latest|/$VERSION|")
  echo $VERSIONED_PATH
}

function exists {
  if [ ! -d "$(getpath $1)/latest" ]; then
    echo "ERROR: target does not exists"; exit 1;
  fi
}

function getlastversion {
  VERSION=$(ls $(getpath $1) | grep -v latest | grep -P "^v" | sed 's/v//' | sort -t'.' -k1,1 -k2,2 -n | tail -1)
  if [ -z "$VERSION" ]; then
    VERSION="latest";
  else
    VERSION=v$VERSION
  fi
  echo $VERSION
}

function getincrementedversion {
  VERSION=$(getlastversion $1 | sed 's/v//')
  if [ "$VERSION" == "latest" ]; then VERSION="0.0"; fi
  VERSION=$(echo $VERSION | awk -F. -v OFS=. '{print $1"."++$2}')
  echo "v$VERSION"
}

function calculatemd5sum {
  #USAGE: calculatemd5sum $1 _version_
  MD5SUM=$(md5sum $(getpath $1)/$2/*.wdl | cut -d ' ' -f1)
  IMPORTS=$(cat $(getpath $1)/$2/*.wdl | grep "import \"https" | grep "raw/" | sed 's|.*raw/dev/||' | sed 's|.*raw/master||' | sed 's|" as .*||')
  for IMPORT in $IMPORTS
  do
    if [ $2 == "latest" ]; then
      MD5SUM="$MD5SUM $(calculatemd5sum $IMPORT $2)"
    else
      MD5SUM="$MD5SUM $(calculatemd5sum $IMPORT $(getlastversion $IMPORT))"
    fi
  done
  echo $MD5SUM
}

function ischanged {
  if [ "$(getlastversion $1)" == "latest" ]; then
    true;
  else
    PREVIOUS_VERSION_MD5="$(calculatemd5sum $1 $(getlastversion $1))"
    LATEST_VERSION_MD5="$(calculatemd5sum $1 latest)"
    #echo $PREVIOUS_VERSION_MD5 "---" $LATEST_VERSION_MD5 >&2
    if [ ! "$PREVIOUS_VERSION_MD5" == "$LATEST_VERSION_MD5" ]; then true; else false; fi
  fi
}

function increment {
  IMPORTS=$(cat $(getpath $1)/latest/*.wdl | grep "import \"https" | grep "raw/dev" | sed 's|.*raw/dev/||' | sed 's|" as .*||')
  NEW_VERSION=$(getincrementedversion $1)
  if ischanged $1; then
    echo "changed";
    #if [ ! "$PREVIOUS_VERSION_MD5" == "$LATEST_VERSION_MD5" ];
    cp -r $(getpath $1)/latest $(getpath $1)/$NEW_VERSION
    cp -r $(gettestpath $1)/latest $(gettestpath $1)/$NEW_VERSION
    for IMPORT in $IMPORTS
    do
      WDL_NAME=$(ls $(getpath $1)/latest/*.wdl | head -1 | xargs basename)
      sed -i "s|$(getimportpath $IMPORT)|$(incrementpath $IMPORT)|" $(getpath $1)"/"$NEW_VERSION"/"$WDL_NAME
      scripts/increment-version --target $IMPORT

      #echo $(cat $(getpath $1)/latest/*.wdl | grep $IMPORT)
      #printf "PATH:"$(getpath $IMPORT)"\n"
      #printf "VER:"$(getincrementedversion $IMPORT)"\n"
      #printf "TEST:"$(gettestpath $IMPORT)"\n"
      #printf "OLD:"$(getimportpath $IMPORT)"\n"
      #printf "NEW:"$(incrementpath $IMPORT)"\n"
    done
  else echo "unchanged"; fi
}

function validate {
  exists $1
  increment $1
}

### run script ###
SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR=$( cd -P "$( dirname "$SOURCE" )" && pwd | xargs dirname)
SRC_DIR="src/main/wdl/"
cd $PROJECT_DIR

if [ ! -z "$TARGET" ]; then
  TARGET=$(printf $TARGET | sed "s/:/\//g")
  TARGET=$(echo $TARGET | xargs)
  printf "incrementing $TARGET : "
  validate $TARGET
  #echo "done $TARGET"
  exit 0
else
  help
  printf "  ERROR:\n"
  printf "  Target not provided:\n"
  printf "    -t|--target \n"
  printf "  \n"
fi
