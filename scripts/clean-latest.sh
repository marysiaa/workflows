#!/bin/bash

set -e
tasks=(vcf-acmg-ba1 vcf-acmg-bp4 vcf-acmg-bp3 vcf-acmg-bp4 vcf-acmg-bp7 vcf-acmg-bs2 vcf-acmg-pm4i vcf-acmg-pp3 vcf-acmg-bp3 vcf-acmg-summary vcf-anno-func-gene vcf-anno-func-var)

for i in ${tasks[@]}; do
		if [ -d "src/main/wdl/tasks/${i}/latest/" ]
	then
		echo "removing ${i}"
		for x in "main" "test"; do
			rm -rf src/${x}/wdl/tasks/${i}/latest/
			echo $i removed
		done
		src/test/wdl/tasks/${i}/test.sh
	else
		echo "Latest for $i does not exists"
	fi
done
