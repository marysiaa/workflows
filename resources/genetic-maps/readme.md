#  GENETIC MAPS  
  
Directory with genetic maps.  
  
**This readme contains instructions on how to create a given resource. The resources are already created. YOU DON'T NEED TO execute any command if you just want to use a resource!**  
The location of resources:  
  - **anakin** (main location):  
    - directory: **/data/public/intelliseqngs/workflows/resources/genetic-maps**
    - on-line: http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps   
  - **kenobi** (rsynced to main location on anakin):  
    - directory: **/data/public/intelliseqngs/workflows/resources/genetic-maps**  
   
## Broad institute/Eagle hg38  (hg38 directory)    

**Last update date:** 24-03-2017   
     
**1.** Download hg38 genetic map file   

```
wget https://data.broadinstitute.org/alkesgroup/Eagle/downloads/tables/genetic_map_hg38_withX.txt.gz 
```
**2.** Prepare chromosome-wise files (format required by bcftolls roh   

```
for i in {1..23} do;   
cat <( echo 'position COMBINED_rate(cM/Mb) Genetic_Map(cM)' ) <( zcat genetic_map_hg38_withX.txt.gz | awk -v CHR="chr"$i '$1 == CHR' | cut -f2- -d ' ' ) >  chr"$i".genetic-map-hg38.txt   
mv chr23.genetic-map-hg38.txt chrX.genetic-map-hg38.txt   
```        

