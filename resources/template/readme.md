# TEMPLATE

Proposed template how to maintain resources + how to create them. Free to include some new sections, new ideas.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/template**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/template**

---

## table of contents

- [example-resource-1](#example-resource-1)
  * [Description:](#description-)
  * [How to create](#how-to-create)
  * [Content of directory](#content-of-directory)
  * [File contents](#file-contents)
  * [Some metadata files used to create resource](#some-metadata-files-used-to-create-resource)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>



## example-resource-1

**Last update date:** day-month-year (01-01-20)

**Update requirements:** every month / every release 

**History of versions and updates:** (examples)
 - **version** (12-03-20 , newest, docker version:1.0.0, script version x.x.x, and so on...)
 - **version** (27-02-20)
 - **version** 1.0.0 (resolved some bug in previous version - optional comment)

---

### Description:

Write here whatever you think is necessary. The more the better. Can be table or whatever you want. Can contain some points:
- what is this resource in few sentences + link to place where you red this
- link to documentation that was used to understand or create,
- where in our software this resource is used
- why this resource is here

### How to create

Here some instructions how to run the code. All code needed to create resources should be inside some scripts (Python, bash, Makfile, Dockerfile). 

Run from any directory:

```docker run --rm -it -v /data/public/intelliseqngs/workflows/resources/template:/outputs -v /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38:/reference-genomes intelliseqngs/template:1.0.0```

or another example:

```
python biomart-table-maker.py \
    --input-dataset "hsapiens_gene_ensembl" \
    --input-host "http://ensembl.org" \
    --output-biomart-csv 'biomart-genes-exon-position.csv'
```

### Content of directory

From raw data: 
```bash
├── file1 (*some comment)
├── file2 (*some comment)
└── file3
```
...are created this files:

```bash
├── result1
├── result2 (*some comment)
```

### File contents

Example: vcf file needs description of the added header fields. This can be stored in metadata.json, which can be used in script. 
Note: better to avoid write in readme exactly the same data that is inside some file. 

All data/metadata about file content should be stored inside some file (f.e. metadata.json). 

EXAMPLE:

instead of:

List of TSV field included in the file:
AnnotSV ID, SV end, SV length, SV type, AnnotSV type, Gene name, NM, CDS length, tx length, location, location2, intersectStart, intersectEnd, DGV_GAIN_IDs, DGV_GAIN_n_samples_with_SV, DGV_GAIN_n_samples_tested,

Write:

Here is the tsv file and contain some fields: ```link to this file```

This file was created by ```some_script/by_hand``` Fields have been taken from this documentation in ```this website/from this publication```...

### Some metadata files used to create resource

Here is an example of metadata file (not described): https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v2-liftover/gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt
It will be helpful to add some info what is this file, where is used (or maybe can be used in the future), how was created


[Return to the table of contents](#table-of-contents)