#  HPO

Files from hpo are downloaded in docker:

- hp.obo
- phenotype.hpoa
- genes_to_phenotype.txt
- phenotype_to_genes.txt

Additionally, on files `genes_to_phenotype.txt` and `phenotype_to_genes.txt`, script `hpo-resources.R` is run, 
which replaces the names of genes with ensembl.

### Updates
 
**Last update date:** 12-09-2022

**History of versions and updates:**
 - **version** (12-09-2022)
   - docker version - 1.1.4
   - ensembl update
   - ubuntu-minimal update
 - **version** (29-12-2021)
   - docker version - 1.1.2
   - add uniprot to ensembl-genes dockerfile
 - **version** (17-11-2021)
   - docker version - 1.1.1
   - R script change (added two columns from ensembl: gene start and gene end)
   - docker ensembl-genes update
 - **version** (18-10-2021)
   - ensembl version - 104
   - docker version - 1.0.0
     
---

After updating the resources, update the docker `task_vcf-anno-func-gene` and task `vcf-anno-func-gene`.
Additionally, please notify the Dev team to update the Explorare in docker.
