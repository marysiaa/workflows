# GATK BEST PRACTICES SOMATIC HG38

Resources for somatic variants calling. Reference genome: **GRCh38**.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/gatk-best-practices-somatic-hg38**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/gatk-best-practices-somatic-hg38**

---
**Last update date:** 16-03-2021

**Update requirements:** almost never (with new version of gatk resources)

**History of versions:**  
  + 16-03-2021    
   
---
**Description:**

**[Instructions on how to obtain GATK best practices somatic hg38](#instructions-on-how-to-obtain-gatk-best-practices-somatic-hg38)**

**Directory contents:**

```bash
   <gatk-best-practices-somatic-hg38>
      |
  <version>  ## these files are used in bam-gatk-m2 task
      ├── 1000g_pon.hg38.vcf.gz
      ├── 1000g_pon.hg38.vcf.gz.tbi
      ├── af-only-gnomad.hg38.vcf.gz
      ├── af-only-gnomad.hg38.vcf.gz.tbi
      ├── common-snps-gnomad.hg38.vcf.gz
      └── common-snps-gnomad.hg38.vcf.gz.tbi

```

### Instructions on how to obtain Broad Institute MT resources   

**1.**  Prepare the workspace.


```bash
VERSION="16-03-2021"
mkdir $VERSION
cd $VERSION
```

**2.**  Commands:
   
```bash
## Download files from GATK
gsutil cp gs://gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz .
gsutil cp gs://gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz.tbi .
gsutil cp gs://gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz .
gsutil cp gs://gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz.tbi .

## Prepare file with common biallelic variants   
## (as described in https://github.com/broadinstitute/gatk/blob/master/scripts/mutect2_wdl/mutect_resources.wdl )
gatk -V af-only-gnomad.hg38.vcf.gz \
      -select-type SNP -restrict-alleles-to BIALLELIC \
      -select "AF > 0.05" \
      -O common-snps-gnomad.hg38.vcf.gz \
      --lenient
```  
 
[Return to: Resources](./../readme.md) 

---
