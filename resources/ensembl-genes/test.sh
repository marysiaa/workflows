docker create -ti --name datasource intelliseqngs/ensembl-genes-resources:1.2.0 .
docker cp datasource:/resources/ensembl-genes /tmp/ensembl-genes
docker rm -f datasource