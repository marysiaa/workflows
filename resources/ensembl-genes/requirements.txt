pyOpenSSL==19.1.0
ndg-httpsclient==0.5.1
pyasn1==0.4.8
pybiomart==0.2.0
requests==2.25.0
pandas==1.2.4