#  Ensembl genes

Docker is used to prepare a json and a table with genes from an ensembl and the resulting files are stored in the docker.
Only the genes with biotype protein_coding are added to the list.
### Updates
 
**Last update date:** 12-09-2022

**History of versions and updates:**
 - **version** (12-09-2022)
   - docker version - 1.2.0
   - update ensembl version from 104 to 107
   - update ubuntu-minimal version in docker from 3.0.5 to 3.0.6
 - **version** (30-03-2022)
   - docker version - 1.1.0
   - add `biomart-table-maker.py` which generates `biomart-genes-exon-position.tsv` used in `bam-panel-coverage` task 
 - **version** (29-12-2021)
   - docker version - 1.0.2
   - rename and reformat script (to add uniprot id to the next .tsv file)
   - reformat test just to get the results out of docker
 - **version** (15-11-2021)
   - docker version - 1.0.1
   - add two columns (Gene start and Gene end) to generated .tsv file in docker
 - **version** (13-10-2021)
   - ensembl version - 104
   - docker version - 1.0.0
     
---
Docker's name is `ensembl-genes-resources` and it's written in Dockerfile as ARG variable `IMAGE_NAME`.
After changing the docker's content you need to modify ARGs: (if needed) `ENSEMBL_VERSION` and `TIMESTAMP`.

Example of building a docker from the root (change the version):

   `docker build -f resources/ensembl-genes/Dockerfile -t intelliseqngs/ensembl-genes-resources:1.2.0 .`


Push the docker:

   `docker push intelliseqngs/ensembl-genes-resources:1.2.0`


After updating the docker, update its version and keep compatibility in:
   * `resources/gene-panels`,
   * `resources/hpo`,
   * `resources/acmg/uniprot`,
   * `src/main/docker/task/task_bed-to-interval-list`,
   * `src/main/docker/task/task_bam-panel-coverage`.

Additionally, please notify the Dev team to update the Explorare in docker.

A fragment of the prepared resource:

- JSON format (gene: synonyms):

```
{
  "": [],
  "A1BG": [],
  "A1CF": [
    "ACF",
    "ACF64",
    "ACF65",
    "APOBEC1CF",
    "ASP"
  ],
  "A2M": [
    "CPAMD5",
    "FWP007",
    "S863-7"
  ],
  "A2ML1": [
    "CPAMD9",
    "FLJ25179",
    "p170"
  ],
  "A3GALT2": [
    "A3GALT2P",
    "IGB3S",
    "IGBS3S"
  ],
  "A4GALT": [
    "A14GALT",
    "Gb3S",
    "P(k)",
    "P1"
  ],
  "A4GNT": [
    "alpha4GnT"
  ]
}
```

- TSV format (general):

|Gene stable ID |Gene name|NCBI gene (formerly Entrezgene) accession|NCBI gene (formerly Entrezgene) ID|Chromosome/scaffold name|Gene start (bp)|Gene end (bp)|
|---------------|---------|-----------------------------------------|----------------------------------|------------------------|---------------|-------------|
|ENSG00000284662|OR4F16   |OR4F16                                   |81399                             |1                       |685679         |686673       |
|ENSG00000186827|TNFRSF4  |TNFRSF4                                  |7293                              |1                       |1211340        |1214153      |
|ENSG00000186891|TNFRSF18 |TNFRSF18                                 |8784                              |1                       |1203508        |1206592      |
|ENSG00000160072|ATAD3B   |ATAD3B                                   |83858                             |1                       |1471765        |1497848      |
|ENSG00000041988|THAP3    |THAP3                                    |90326                             |1                       |6624866        |6635586      |
|ENSG00000142611|PRDM16   |PRDM16                                   |63976                             |1                       |3069168        |3438621      |
|ENSG00000067606|PRKCZ    |PRKCZ                                    |5590                              |1                       |2050411        |2185395      |
|ENSG00000131584|ACAP3    |ACAP3                                    |116983                            |1                       |1292390        |1309609      |
|ENSG00000169972|PUSL1    |PUSL1                                    |126789                            |1                       |1308597        |1311677      |


- TSV format (uniprot ids):

|Gene name|UniProtKB Gene Name ID|
|---------|----------------------|
|OR4F16   |Q6IEY1                |
|OR4F16   |A0A126GV92            |
|TNFRSF4  |P43489                |
|TNFRSF18 |Q9Y5U5                |
|TNFRSF18 |J3KT02                |
|ATAD3B   |Q5T9A4                |
|ATAD3B   |A0A5K1VW56            |
|THAP3    |Q8WTV1                |
|THAP3    |K7EIZ2                |


- biomart-genes-exon-position.tsv:

|   | Gene name | Gene stable ID | Transcript stable ID |Chromosome/scaffold name|Exon region start (bp)|Exon region end (bp)|
|---|---|---|---|---|---|---|
| 5 | MT-ND2 | ENSG00000198888 | ENST00000361390 |MT|3307|4262|
| 9 | MT-ND2 | ENSG00000198763 | ENST00000361453 |MT|4470|5511|
| 15 | MT-CO1 | ENSG00000198804 | ENST00000361624 |MT|5904|7445|
| 18 | MT-CO2 | ENSG00000198712 | ENST00000361739 |MT|7586|8269|
