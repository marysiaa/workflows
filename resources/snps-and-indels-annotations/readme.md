# SNPS AND INDELS ANNOTATIONS

Resources to annotate VCF files containing SNPs and INDELs variants. Reference genome: **GRCh38**

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations**

---

## Table of Contents

**SNPs and INDELs annotations**
  * [**dbSNP**](./dbsnp/readme.md)
    * [dbsnp only rs IDs](./dbsnp/readme.md#dbsnp-only-rs-ids)
      * build 153
  * [**Frequencies**](./frequencies/readme.md)
    * [Merged frequencies, exome](./frequencies/readme.md#merged-frequencies-exome)
      * 0.1.0
      * 0.0.1
    * [Merged frequencies, genome](./frequencies/readme.md#merged-frequencies-genome)
      * 0.1.0
      * 0.0.1
    * [1000 Genomes](./frequencies/readme.md#1000-genomes)
    * [ESP6500](./frequencies/readme.md#esp6500)
    * [ExAC](./frequencies/readme.md#exac)
    * [gnomAD exomes  v2, liftover](./frequencies/readme.md#gnomad-exomes-v2-liftover)
    * [gnomAD genomes v3](./frequencies/readme.md#gnomad-genomes-v3)
    * [gnomAD genomes v3 exome, calling intervals](./frequencies/readme.md#gnomad-genomes-v3-exome-calling-intervals)
    * [gnomAD genomes  v2, liftover](./frequencies/readme.md#gnomad-genomes-v2-liftover)
    * [gnomAD genomes v2, liftover, exome calling intervals](./frequencies/readme.md#gnomad-genomes-v2-liftover-exome-calling-intervals)
    * [Mitomap](./frequencies/readme.md#mitomap)
    * [Gnomad-coverage](./frequencies/readme.md#gnomad-coverage)
  * [**Functional annotations - gene level**](./functional-annotations-gene-level/readme.md)
    * [All functional annotations, gene level](./functional-annotations-gene-level/readme.md#all-functional-annotations-gene-level)
      * 0.2.0
      * 0.1.0
    * [ClinVar diseases](./functional-annotations-gene-level/readme.md#clinvar-diseases)
    * [Human Phenotype Ontology](./functional-annotations-gene-level/readme.md#human-phenotype-ontology)
  * [**Functional annotations - variant level**](./functional-annotations-variant-level/readme.md)
    * [Merged VCF functional annotations, variant level](./functional-annotations-variant-level/readme.md#merged-vcf-functional-annotations-variant-level)
      * 2.0.0
      * 1.0.0
      * 0.2.0
      * 0.1.0
    * [ClinVar](./functional-annotations-variant-level/readme.md#clinvar)
    * [Mitomap diseases](./functional-annotations-variant-level/readme.md#mitomap-diseases)
    * [cosmic-ensembl](./cosmic-ensembl/readme.md#cosmic-ensembl)
  * [**Scores**](./scores/readme.md)
    * [Merged VCF scores annotations](./scores/readme.md#merged-vcf-scores-annotations)
      * 0.1.0
    * [CADD](./scores/readme.md#cadd)
    * [GERP](./scores/readme.md#gerp)
    * [M-CAP](./scores/readme.md#m-cap)
      * v1.4
    * [PhastCons](./scores/readme.md#phastcons)
    * [PhyloP](./scores/readme.md#phylop)
    * [SIFT4G](./scores/readme.md#sift4g)

[Return to: Resources](./../readme.md)
