#!/usr/bin/env python3
from sys import stdin
import re
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)
info_add=". Commas in original dbNSFP field are replaced by colons. More detailed description can be found in https://sites.google.com/site/jpopgen/dbNSFP\",Source=\"dbNSFP\",Version=\"v4.0\">"

for raw_line in stdin:
    if raw_line.startswith("#"):
        if raw_line.startswith("##INFO=<ID=dbNSFP"):
            print((re.sub("\">", info_add, re.sub("Number=.", "Number=A", re.sub("Type=Character","Type=String",raw_line)))).strip())
        else:
            print(raw_line.strip())
    else:
        INFO = []
        for raw_line_tab_div in re.split("\t", raw_line):
            INFO_colon = []
            for raw_line_tab_div_colon_div in re.split(";", raw_line_tab_div.strip()):
                if raw_line_tab_div_colon_div.startswith("dbNSFP"):
                    INFO_colon.append(raw_line_tab_div_colon_div.replace(",",":"))
                else:
                    INFO_colon.append(raw_line_tab_div_colon_div)
            INFO.append(";".join(INFO_colon))
        print("\t".join(INFO))
#part = ";".join(INFO)


#delete \n from it
