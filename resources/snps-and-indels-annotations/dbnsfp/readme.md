# dbNSFP



Resources to annotate VCF files with dbNSFP. Reference genome: **GRCh38**



**This readme contains instructions on how to create a given resource. The resources are already created. YOU DON'T NEED TO execute any command if you just want to use a resource!**



The location of resources:

  - **anakin** (main location):

    - directory: **/data/public/intelliseqngs/workflows/resources/vcf-annotations/dbnsfp**

    - on-line: http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/vcf-annotations/dbnsfp

  - **kenobi** (rsynced to main location on anakin):

    - directory: **/data/public/intelliseqngs/workflows/resources/vcf-annotations/dbnsfp**





# Table of Contents



<!---toc start-->



  * **dbNSFP**



<!---toc end-->



## dbNSFP



**Last update date:** 26-11-2021  

**Update requirements:** every few months



**1.**  Download newest dbNSFP database from [dbNSFP page](http://database.liulab.science/dbNSFP#database)

Note: Use the commercial version (`c`)

```bash

curl ftp://dbnsfp:dbnsfp@dbnsfp.softgenetics.com/dbNSFP4.2c.zip --output dbNSFP4.2c.zip  
unzip dbNSFP4.2c.zip
rm try* search*
```

**2.** Combine chrY and chrM to chrY-and-the-rest 

```bash

cp dbNSFP4.2c_variant.chrY.gz dbNSFP4.2c_variant.chrY-and-the-rest.gz
zcat dbNSFP4.2c_variant.chrM.gz | sed '1d' | gzip >> dbNSFP4.2c_variant.chrY-and-the-rest.gz
rm dbNSFP4.2c_variant.chrM.gz dbNSFP4.2c_variant.chrY.gz

```

**3.** Change compression method to bgzip and prepare index files

```bash
`for i in {1..22} X Y-and-the-rest;do
  zcat dbNSFP4.2c_variant.chr"$i".gz | bgzip > dbNSFP4.2c_variant.chr"$i".tmp.gz
  mv dbNSFP4.2c_variant.chr"$i".tmp.gz dbNSFP4.2c_variant.chr"$i".gz
  tabix -s 1 -b 2 -e 2 dbNSFP4.2c_variant.chr"$i".gz 
done`

```

