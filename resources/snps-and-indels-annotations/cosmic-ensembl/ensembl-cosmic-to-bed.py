#!/usr/bin/python3
import argparse
from pybiomart import Server
import pandas as pd
from pathlib import Path
from requests.exceptions import HTTPError, ConnectionError
import time
import json

__version__ = "0.0.1"


def attempt_to_get_database_query(chromosome_name, version_json, version):
    ensembl_available = False
    num_of_attempts = 0
    max_n_of_attempts = 2
    while not ensembl_available:
        try:
            return get_database_query(chromosome_name, version_json, version)
            ensembl_available = True
        except (HTTPError, ConnectionError):
            if num_of_attempts >= max_n_of_attempts:
                raise
            time.sleep(2 ** num_of_attempts)
        num_of_attempts += 1


def get_database_query(chromosome_name, version_json, version):
    with open(version_json) as file:
        versions_dict = json.load(file)

    server = Server(versions_dict[version])
    mart = server['ENSEMBL_MART_SNP']
    dataset = mart['hsapiens_snp_som']

    queried = dataset.query(attributes=['chr_name', 'chrom_start',
                                        'chrom_end', 'refsnp_id',
                                        'phenotype_description', 'refsnp_source'],
                                        filters={'chr_name': chromosome_name})
    return queried


def filter_cosmic_sources(queried):
    return queried[
        queried['Variant source'] == 'COSMIC']


def sort_chromosome_positions(queried):
    start = 'Chromosome/scaffold position start (bp)'
    end = 'Chromosome/scaffold position end (bp)'

    s = (queried[end] < queried[start])

    queried.loc[s, [start, end]] = queried.loc[s, [end, start]].values

    queried[start] = queried[start].astype(int)
    queried[end] = queried[end].astype(int)
    return queried


def aggregate_by_unique_cosmic_id_and_sort(queried):
    aggregated = queried.groupby('Variant name', as_index=False).agg({
        'Chromosome/scaffold name': lambda x: x.iloc[0],
        'Chromosome/scaffold position start (bp)': lambda x: x.iloc[0],
        'Chromosome/scaffold position end (bp)': lambda x: x.iloc[0],
        'Phenotype description': lambda x: ','.join(x)
    })

    aggregated = aggregated.sort_values(
        by=['Chromosome/scaffold position start (bp)', 'Chromosome/scaffold position end (bp)'])

    return aggregated


def export_to_bed_file(beds_to_export, output_dir, version):
    version_folder = f'{output_dir}/{version}'
    Path(version_folder).mkdir(parents=True, exist_ok=True)

    beds_to_export.to_csv(f'{version_folder}/cosmic-id-phenotype.bed', sep='\t',
                      encoding='utf-8-sig', index=False,
                      header=False,
                      columns=['Chromosome/scaffold name',
                               'Chromosome/scaffold position start (bp)',
                               'Chromosome/scaffold position end (bp)',
                               'Variant name', 'Phenotype description'])


def generate_chromosome_names():
    chromosome_numbers = [str(x) for x in range(1,23)]
    return chromosome_numbers + ['X', 'Y', 'MT']


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates .bed file from cosmic database using pybiomart api')
    parser.add_argument('--input-version', '-j', metavar='input_version', type=str, required=True,
                        help='Version of dataset')
    parser.add_argument('--input-versions-json', '-k', metavar='input_versions_json', type=str, required=True,
                        help='Release versions in key-value format (version-link)')
    parser.add_argument('--output-dir', '-l', metavar='output_dir', type=str, required=True,
                        help='Directory where goes output')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--max_attempts', type=int, default=8,
                        help='Max number of attempts when retrieving data from ensembl.')
    args = parser.parse_args()
    return args


def get_cosmic_bed_files(args):
    bed_files = list()
    chromosome_names = generate_chromosome_names()

    for chromosome_name in chromosome_names:
        queried = attempt_to_get_database_query(chromosome_name, args.input_versions_json, args.input_version)
        queried = filter_cosmic_sources(queried)

        queried['Chromosome/scaffold name'] = 'chr' + queried['Chromosome/scaffold name'].astype(str)
        queried['Phenotype description'] = queried['Phenotype description'].str.replace(' ', '_')

        queried = sort_chromosome_positions(queried)
        aggregated = aggregate_by_unique_cosmic_id_and_sort(queried)
        # 0-based indexing
        aggregated['Chromosome/scaffold position start (bp)'] -= 1

        bed_files.append(aggregated)

    return bed_files


if __name__ == "__main__":
    args = args_parser_init()
    bed_files = get_cosmic_bed_files(args)

    beds_to_export = pd.concat(bed for bed in bed_files)
    export_to_bed_file(beds_to_export, args.output_dir, args.input_version)

