The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/cosmic-ensembl**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/cosmic-ensembl**


## table of contents

- [cosmic-ensembl](#cosmic-ensembl)
  * [Description:](#description-)
  * [How to create](#how-to-create)
  * [Content of directory](#content-of-directory)
  * [File contents](#file-contents)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


## cosmic-ensembl

**Last update date:** 01-10-20

**Update requirements:** This needs to be updated whenever the new version of ensembl database is released. 
To do this you need to modify ensembl-versions.json (add new version's name as a key and it's direct link as a value).
The file ensembl-versions.json can be found at: ```https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/ensembl-genes/ensembl-versions.json```

**History of versions and updates:**
 - **version** 100 
 - **version** 101

---

### Description

This is the table in .bed format which is used to annotate vcf file. It contains chromosome segments in tsv format with following columns: chromosome_name, chromosome_start, chromosome_end, cosmic_id, phenotype_description. All its parts were obtained using pybiomart python library.

### How to create

How to run script locally:

```
python3 ensembl-cosmic-to-bed.py \
    --input-version 'VERSION' \
    --input-versions-json 'path/to/versions/json' \
    --output-dir 'path/to/output/directory'
```

How to run script in docker (for example on anakin):

```
docker run --rm -it -v /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/cosmic-ensembl:/outputs \
intelliseqngs/ensembl-cosmic-to-bed:1.0.0 \
python3 /intelliseqtools/ensembl-cosmic-to-bed.py \
--input-version '101' \
--input-versions-json '/resources/ensembl-versions.json' \
--output-dir '/outputs'
```

### Content of directory

This is how the directory containing version folders looks like: 
```bash
├── 99
├── 100 
└── 101
```
Each folder contains the .bed file:

```bash
├── cosmic-id-phenotype.bed
```

### File contents
chrom: chromosome name (e.g. chr3, chrY)

chromosome_start: start coordinate on the chromosome 

chromosome_end: end coordinate on the chromosome

cosmic_id: id in cosmid database

phenotype_description: phenotype named and separated by comma

All elements are stored in tsv format.


The output is used in vcf-ann-cosmic.wdl to annotate vcf
```src/main/wdl/tasks/vcf-anno-cosmic/vcf-anno-cosmic.wdl```
using docker image task_vcf-anno-cosmic in which the .bed file, conf.toml and vcfanno script are stored
```src/main/docker/task/task_vcf-anno-cosmic/Dockerfile``` 

[Return to the table of contents](#table-of-contents)