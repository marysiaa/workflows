#!/bin/bash

set -eo pipefail
VERSION="0.0.2"

## create mitomap update directory and go there
UPDATE_DATE=$( date +%d-%m-%Y )
MITOMAP_DISEASES_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/mitomap-diseases"
cd $MITOMAP_DISEASES_DIR
mkdir $UPDATE_DATE
cd $UPDATE_DATE

## download the raw resource mitomap vcf
wget -O disease.vcf  http://mitomap.org/cgi-bin/disease.cgi?format=vcf

## change Number from . to A, which is needed to split annotations of the multi-allelic sites properly
# (items divided by , concern different alleles - see https://mitomap.org/cgi-bin/disease.cgi?)
sed -i 's/<ID=Disease,Number=./<ID=Disease,Number=A/' disease.vcf
sed -i 's/<ID=DiseaseStatus,Number=./<ID=DiseaseStatus,Number=A/' disease.vcf

## change contig name
sed 's/^MT/chrM/' disease.vcf | bgzip > disease.vcf.gz
tabix -p vcf disease.vcf.gz

## split multi-allelic sites, normalize indels
bcftools norm disease.vcf.gz  --multiallelics -any -cs \
    --fasta-ref /data/public/intelliseqngs/workflows/resources/broad-institute-mt/0.0.1/Homo_sapiens_assembly38.chrM.fasta  \
    | bcftools annotate -x '^INFO/Disease,INFO/DiseaseStatus'  \
    | bcftools filter -e 'ALT = "."' -o tmp1.vcf.gz -O z

tabix -p vcf tmp1.vcf.gz

## download python scripts
wget https://gitlab.com/intelliseq/workflows/-/raw/create-mitomap-diseases-annotation-vcf.py@2.0.0/src/main/scripts/resources-tools/create-mitomap-diseases-annotation-vcf.py
wget https://gitlab.com/intelliseq/workflows/-/raw/merge-mitomap-annotations.py@0.0.1/src/main/scripts/resources-tools/merge-mitomap-annotations.py

## reformat the vcf file: (## change name and format of the "Disease" and "DiseaseStatus" fields):

python3 create-mitomap-diseases-annotation-vcf.py \
  -i tmp1.vcf.gz -o tmp2.vcf.gz

tabix -p vcf tmp2.vcf.gz


### join lines describing the same alleles (such lines are created during the indels normalization)
python3 merge-mitomap-annotations.py \
   -i tmp2.vcf.gz -o tmp3.vcf

## remove two extra lines from the header
bcftools annotate -x 'INFO/Disease,INFO/DiseaseStatus' tmp3.vcf \
  | grep -v '##contig=<ID=MT' \
  | bgzip > mitomap-diseases.variant-level.vcf.gz

## create index for the final vcf
tabix -p vcf mitomap-diseases.variant-level.vcf.gz

## check the file
vcf-validator mitomap-diseases.variant-level.vcf.gz

## remove not needed files
rm disease.vcf* tmp* *.py