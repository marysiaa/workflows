# FUNCTIONAL ANNOTATIONS - VARIANT LEVEL

Resources to annotate VCF files containing SNPs and INDELs variants with functional annotations (variant level).
Reference genome: **GRCh38**

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level**

---

## Table of Contents

**Functional annotations - variant level**

`Used in task_vcf-anno-func-var docker`  
  * [**Merged VCF functional annotations, variant level (CLINVAR + MITOMAP)**](#merged-vcf-functional-annotations-variant-level)
  * [ClinVar](#clinvar)
  * [Mitomap diseases](#mitomap-diseases)

[Return to: SNPs and INDELs annotations](./../readme.md)

---

## Merged VCF functional annotations, variant level

**Last update date:** 10-12-2022

**Update requirements:** monthly

**Current version**: 6.0.0

**History of versions**:
+ 6.0.0
+ 5.0.0
+ 4.0.0
+ 3.0.0
+ 2.1.0  
+ 2.0.0
+ 1.0.0
+ 0.2.0
+ 0.1.0

---
**Description**:

This folder contains bgzipped VCFs files created by merging the bgzipped annotation
VCFs from the [ClinVar](#clinvar) and [Mitomap diseases](#mitomap-diseases) databases. 
The folder contains files based on the most recently downloaded version of ClinVar and MITOMAP diseases.

**[Instructions on how to create VCF with functional annotations (variant level, CLINVAR + MITOMAP)](#instructions-on-how-to-create-vcf-with-functional-annotations-variant-level)**

[(Archival) Details of previous versions of VCF with functional annotations (variant level)](#history-of-previous-versions-of-vcf-with-functional-annotations-variant-level)

**Directory contents:**

```bash
<version>
    ├── all-variant-level.vcf.gz
    └── all-variant-level.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)
---

### **Instructions on how to create VCF with functional annotations (variant level):**

Update the ClinVar and Mitomap vcfs. Instructions are here: 
 * [ClinVar](#instructions-on-how-to-create-clinvar-annotation-vcf)
 * [Mitomap](#instructions-on-how-to-create-mitomap-diseases-annotation-vcf)

Check and remember their update dates (folders).    
Then, on anakin, prepare the merged files. 
Requirements:
 - bcftools 
 - python 
 - vcf-validator
 - python (with pysam)
All programs are installed on anakin.  
   
```bash
## get the merge-clinvar-and-mitomap.sh script
wget https://gitlab.com/intelliseq/workflows/-/raw/merge-clinvar-and-mitomap.sh@0.0.1/resources/snps-and-indels-annotations/functional-annotations-variant-level/merge-clinvar-and-mitomap.sh

## run it with:
# clinvar update date as the first positional argument (for example 04-05-2021)
# mitomap update date as the second positional argument (for example 05-06-2021)
# merged resources version as the third positional argument (for example 3.0.0)
# the latest version of the resources was prepared with the commands
chmod +x merge-clinvar-and-mitomap.sh
./merge-clinvar-and-mitomap.sh 04-06-2021 05-06-2021 3.0.0

## remove the script
rm merge-clinvar-and-mitomap.sh
  
```

[Return to the table of contents](#table-of-contents)

 ### History of previous versions of VCF with functional annotations (variant level):

**version 6.0.0**

Last update: 10-12-2022


```bash
VERSION="6.0.0"
CLINVAR_VERSION="10-12-2022"
MITOMAP_DISEASES_VERSION="10-12-2022"
```


```bash
VERSION="5.0.0"
CLINVAR_VERSION="20-06-2022"
MITOMAP_DISEASES_VERSION="20-06-2022"
```

```bash
VERSION="4.0.0"
CLINVAR_VERSION="22-11-2021"
MITOMAP_DISEASES_VERSION="22-11-2021"
```

```bash
VERSION="3.0.0"
CLINVAR_VERSION="04-06-2021"
MITOMAP_DISEASES_VERSION="05-06-2021"
```

```bash
VERSION="2.0.0"
CLINVAR_VERSION="20-02-2020"
MITOMAP_DISEASES_VERSION="06-02-2020"
```

```bash
VERSION="1.0.0"
CLINVAR_VERSION="06-02-2020"
MITOMAP_DISEASES_VERSION="06-02-2020"
```

**version 0.2.0**

```bash
VERSION="0.2.0"
CLINVAR_VERSION="27-12-2019"
MITOMAP_DISEASES_VERSION="28-12-2019"
```

**version 0.1.0**

```bash
VERSION="0.1.0"
CLINVAR_VERSION="01-10-2019"
MITOMAP_DISEASES_VERSION="01-10-2019"
```

[Return to the table of contents](#table-of-contents)

## ClinVar

**Last update date:** 10-12-2022
**Update requirements:** monthly

**History of updates:**
 - 10-12-2022 (clinvar-update.sh script v0.0.2)
 - 22-06-2022 (clinvar-update.sh script)
 - 22-11-2021
 - 04-06-2021
 - 10-08-2021
 - 04-06-2021 (intelliseqngs/clinvar-func-anno:2.0.2)
 - 09-12-2020 
 - 13-10-2020 (intelliseqngs/clinvar-func-anno:1.0.1)
 - 17-06-2020 (intelliseqngs/clinvar-func-anno:1.0.0)
 - 07-05-2020
 - 20-02-2020
 - 06-02-2020
 - 27-12-2019
 - 01-10-2019

---

**Description:**

This folder contains bgzipped VCFs files containing annotations based on ClinVar
database downloaded from [ClinVar FTP site](ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/). 
The new release of ClinVar database is released approximately monthly. 
The folder contains files based on the most recently downloaded version of ClinVar vcf.  

**[Instructions on how to create ClinVar annotation VCF](#instructions-on-how-to-create-clinvar-annotation-vcf)**

**Directory contents:**

```bash
{last update date}
   ├── clinvar.variant-level.vcf.gz <- main ClinVar annotation VCF, needed to create acmg resources and merged func-variant-level resources
   ├── clinvar.variant-level.vcf.gz.tbi
   ├── clinvar.vcf.gz
   ├── clinvar.vcf.gz.tbi
   ├── submission_summary.txt.gz
   └── variation_allele.txt.gz
```

 [Return to the table of contents](#table-of-contents)

---

**List of ClinVar INFO fields:**

**Source:** ClinVar

**Version:** 10-12-2022

| ID | Number | Type | Description |
|----|--------|------|-------------|
| ISEQ_CLINVAR_ALLELE_ID | A | String | ClinVar Allele ID |
| ISEQ_CLINVAR_VARIATION_ID | A | String | ClinVar Variation ID of a variation consisted of the variant alone (Type of variation: Variant). |
| ISEQ_CLINVAR_VARIANT_TYPE | A | String | ClinVar variant type. Possible values: single_nucleotide_variant, indel, deletion, insertion, duplication, inversion, copy_number_gain, copy_number_loss, microsatellite, variation. |
| ISEQ_VARIANT_CLINVAR_DISEASES | A | String | Diseases associated with the variant in ClinVar. Format: disease_name : (...) : disease_name.|
| ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE | A | String | The overall interpretation of clinical significance of a variation based on aggregating data from submitters. Format: clinical_significance : (...) : clinical_significance. |
| ISEQ_CLINVAR_SIGNIFICANCE | A | String | List of clinical significances of a variation gathered from ClinVar submissions. Taken from tab-file variant_summary.txt.gz. Format: clinical_significance : (...) : clinical_significance. |
| ISEQ_CLINVAR_DATE_LAST_EVALUATED | A | String | List of the last dates the variation-condition relationship last evaluated by the submitter. Taken from tab-file variant_summary.txt.gz. The order corresponds to the order in ISEQ_CLINVAR_SIGNIFICANCE. Format: date : (...) : date. |
| ISEQ_CLINVAR_COLLECTION_METHOD | A | String | List of the methods by which the submitter obtained the information provided. Taken from tab-file variant_summary.txt.gz. The order corresponds to the order in ISEQ_CLINVAR_SIGNIFICANCE. Format: collection_method : (...) : collection_method. |
| ISEQ_CLINVAR_REVIEW_STATUS | A | String | Review status for the ClinVar submissions. Possible values: criteria_provided_-_conflicting_interpretations, criteria_provided_-_multiple_submitters_-_no_conflicts, criteria_provided_-_single_submitter, no_assertion_criteria_provided, no_assertion_provided, practice_guideline, reviewed_by_expert_panel. |
| ISEQ_CLINVAR_GOLD_STARS | A | Integer | Assignment of stars for the ClinVar submissions. Based on review status (ISEQ_CLINVAR_REVIEW_STATUS). Possible values: 0, 1, 2, 3, 4. For more information see: https://www.ncbi.nlm.nih.gov/clinvar/docs/review_status/#revstat. |
| ISEQ_CLINVAR_VARIATION_INCLUDED_INFO | A | String | Information regarding ClinVar Variations including this variant (Types of variation: CompoundHeterozygote, Diplotype, Distinct chromosomes, Haplotype, Phase unknown). Format: variation_id \| variation_type \| < clinical_significance ^ collection_method ^ date_last_evaluated > (...) < clinical_significance ^ collection_method ^ date_last_evaluated > : (...) : variation_id \| variation_type \| < clinical_significance ^ collection_method ^ date_last_evaluated > (...) < clinical_significance ^ collection_method ^ date_last_evaluated >. |
| ISEQ_CLINVAR_GENE_INFO | A | String | Name(s) and ID(s) of genes for the Clinvar variant. Format: gene_name : gene_id | (...) | gene_name : gene_id. |


[Return to the table of contents](#table-of-contents)

---

####  Instructions on how to create ClinVar annotation VCF

To create this automatically:
```
TARGET="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar"
docker run --rm -it -v ${TARGET}:/outputs -v /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38:/resources/reference-genomes intelliseqngs/clinvar-func-anno:2.0.3

 or
 ```bash
 ## get script
  wget https://gitlab.com/intelliseq/workflows/-/raw/clinvar-update.sh@0.0.2/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar/clinvar-update.sh
  
  ## run script (it will take about 5-10 minutes)
  chmod +x clinvar-update.sh
  ./clinvar-update.sh

 ```
All the above code (how to create clinvar functional annotation vcf) is included in intelliseqngs/clinvar-func-anno:2.0.3
- src/main/docker/resources/clinvar-func-anno/Dockerfile
- /resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar/Makefile

Archival:  https://gitlab.com/intelliseq/workflows/-/blob/e3c527cf22414895ebbde6c7b09f713d12613ae9/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md

[Return to the table of contents](#table-of-contents)

## Mitomap diseases

**Last update date:** 10-12-2022

**Update requirements:** every few months.

**History of updates:**
 - 10-12-2022  
 - 22-06-2022
 - 22-11-2021
 - 05-06-2021
 - 14-12-2020
 - 06-02-2020
 - 28-12-2019
 - 01-10-2019
---

**Description:**

This folder contains bgzipped VCFs files containing annotations based on diseases.vcf file downloaded from MITOMAP
Resources site [MITOMAP Resources](https://mitomap.org/foswiki/bin/view/MITOMAP/Resources).  
MITOMAP diseases VCF files are updated approximately every few months. 
The folder contains files based on the most recently downloaded version of MITOMAP diseases VCF.  

**Directory contents:**

```bash
<last update date>
    ├── mitomap-diseases.variant-level.vcf.gz
    └── mitomap-diseases.variant-level.vcf.gz.tbi
```

**List of MITOMAP INFO fields**

**Source:** MITOMAP

(the most recent)  **Version:** 20221210

| INFO                   | Number | Type   | Description                            |
|------------------------|--------|--------|----------------------------------------|
| MITOMAP_DISEASE        | A      | String | Putative Disease Association - MITOMAP. Format: disease : (...) : disease |
| MITOMAP_DISEASE_STATUS | A      | String | Disease Association Status - MITOMAP. Format: status : (...) : status  |


#### Instructions on how to create MITOMAP diseases annotation VCF
Requirements:   
 * bcftools 
 * tabix  
 * vcf-validator  
 * python and pysam 
 (all programs are installed on anakin)   

```bash
## download the mitomap-disease.sh script
wget https://gitlab.com/intelliseq/workflows/-/raw/mitomap-disease.sh@0.0.2/resources/snps-and-indels-annotations/functional-annotations-variant-level/mitomap/mitomap-disease.sh
 
## run the script (on anakin)  
chmod +x mitomap-disease.sh
./mitomap-disease.sh 

## remove the script
rm mitomap-disease.sh
```
This should create the "today-date-directory" in the
`/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/mitomap-diseases"` 
directory with the annotation files.  

Archival: https://gitlab.com/intelliseq/workflows/-/blob/e3c527cf22414895ebbde6c7b09f713d12613ae9/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md

[Return to the table of contents](#table-of-contents)

***
