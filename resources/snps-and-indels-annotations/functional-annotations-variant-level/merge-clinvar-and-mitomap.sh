#!/bin/bash

set -eo pipefail
VERSION="0.0.1"
## get dates
CLINVAR_DATE=$1
MITOMAP_DATE=$2
MERGED_VERSION=$3

## set directories
FUNCTIONAL_ANNOTATIONS_VARIANT_LEVEL="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level"
ALL_VARIANT_LEVEL_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/all-variant-level"
cd $ALL_VARIANT_LEVEL_DIR
mkdir $MERGED_VERSION
cd $MERGED_VERSION

CLINVAR="$FUNCTIONAL_ANNOTATIONS_VARIANT_LEVEL/clinvar/$CLINVAR_DATE/clinvar.variant-level.vcf.gz"
MITOMAP_DISEASES="$FUNCTIONAL_ANNOTATIONS_VARIANT_LEVEL/mitomap-diseases/$MITOMAP_DATE/mitomap-diseases.variant-level.vcf.gz"

## get python script
wget https://gitlab.com/intelliseq/workflows/-/raw/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.py

## merge clinvar and mitomap vcfs
bcftools merge $CLINVAR $MITOMAP_DISEASES \
  | python3 keep-or-remove-vcf-fields.py -r -l AC,AN,SF  \
  | grep -v "##reference" \
  | grep -v "##FILTER" \
  | grep -v "##source" \
  | grep -v "##contig" \
  | grep -v -i "##filedate" \
  | awk '{if(/^#/) print; else print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t.\t"$8}' \
  | bgzip > all-variant-level.unsorted.unnormalized.vcf.gz

## symlink reference genome
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

## remove fields from header
zgrep "^#" all-variant-level.unsorted.unnormalized.vcf.gz  \
   | awk '{if (/^##INFO/) { gsub("Source=\"", "Source="); gsub("\",Version=", ",Version="); gsub(",Version=\"", ",Version="); \
    gsub("\">", ">"); gsub("Source=", "Source=\""); gsub(",Version=", "\",Version="); \
    gsub(",Version=", ",Version=\""); gsub(">", "\">"); print} else {print}}' \
    | bgzip > all-variant-level.sorted.unnormalized.vcf.gz

## sort data lines
zcat all-variant-level.unsorted.unnormalized.vcf.gz \
 | grep -v "^#" \
 | sort -k1,1V -k2,2n \
 | bgzip >> all-variant-level.sorted.unnormalized.vcf.gz

## create index
tabix -p vcf all-variant-level.sorted.unnormalized.vcf.gz

## normalize indels, split multi-allelic sites
bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz  \
  --multiallelics -any all-variant-level.sorted.unnormalized.vcf.gz -o all-variant-level.vcf.gz -O z

## check
vcf-validator all-variant-level.vcf.gz
tabix -p vcf all-variant-level.vcf.gz

## clean
rm *norm* *Homo* *py