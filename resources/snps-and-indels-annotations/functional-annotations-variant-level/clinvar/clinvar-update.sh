#!/bin/bash

set -e -o pipefail
VERSION="0.0.2"

UPDATE_DATE=$( date +%d-%m-%Y )
CLINVAR_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar"
cd $CLINVAR_DIR
#mkdir $UPDATE_DATE
cd $UPDATE_DATE
REF="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa"

## download files
wget -c ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz
wget -c ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz
wget -c ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variation_allele.txt.gz

## find and remove non-asci characters
zcat clinvar.vcf.gz | iconv -c -f  utf-8 -t ascii//translit | bgzip > tmp.vcf.gz
zcat tmp.vcf.gz | awk '{if (/^#/) {print;} else if (/^MT/) {FS="\t"; OFS="\t"; $1="chrM"; print} else {print "chr"$0}}' \
| grep -v "NW_009646201.1" \
| grep -w -v "23868631" \
| grep -w -v "5226739" | bgzip > tmp2.vcf.gz
zcat tmp2.vcf.gz | grep '^#' | bgzip > tmp3.vcf.gz
zcat tmp2.vcf.gz | grep -v '^#' | sort -k1,1V -k2,2n | bgzip >> tmp3.vcf.gz
tabix -p vcf tmp3.vcf.gz
bcftools norm --fasta-ref $REF --multiallelics -any tmp3.vcf.gz | bgzip > tmp_clinvar.split.normalized.vcf.gz
tabix -p vcf tmp_clinvar.split.normalized.vcf.gz
zcat submission_summary.txt.gz  | grep -v "ENIGMA rules, 2015" | bgzip > tmp.txt.gz
mv tmp.txt.gz submission_summary.txt.gz
wget https://gitlab.com/intelliseq/workflows/-/raw/create-clinvar-annotation-vcf.py@2.0.2/src/main/scripts/resources-tools/create-clinvar-annotation-vcf.py
python3 create-clinvar-annotation-vcf.py submission_summary.txt.gz variation_allele.txt.gz tmp_clinvar.split.normalized.vcf.gz | bgzip > clinvar.variant-level.vcf.gz
tabix -p vcf clinvar.variant-level.vcf.gz

#vcf-validator clinvar.variant-level.vcf.gz

rm tmp* *py
