#!/bin/bash
GENOMES_COV_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-coverage/2.1.0/gnomad-coverage-genomes"
EXOME_BED="/data/public/intelliseqngs/workflows/resources/intervals/agilent/v6-v7-v8-combined-padded/v6-v7-v8-combined-padded.bed"
TARGET_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-coverage/2.1.0/gnomad-coverage-exomes"

zcat "$GENOMES_COV_DIR"/chr1.coverage.tsv.gz | head -3 | bgzip > header

set -e -o pipefail

for i in  "$GENOMES_COV_DIR"/chr*.coverage.tsv.gz
do
  NAME=$(basename $i)
  cp header "$TARGET_DIR"/"$NAME"
  tabix $i -R "$EXOME_BED" | bgzip >> "$TARGET_DIR"/"$NAME"
done

rm header
tabix "$GENOMES_COV_DIR"/chrY-and-the-rest.coverage.tsv.gz | bgzip >> "$TARGET_DIR"/chrY-and-the-rest.coverage.tsv.gz 