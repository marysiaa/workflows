#!/bin/bash

set -e

echo "Doing"

ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai


RESOURCES="/data/public/intelliseqngs/workflows/resources"
for i in {1..22} X Y-and-the-rest; do
  OUTPUT="$RESOURCES/snps-and-indels-annotations/frequencies/all-frequencies-exome/3.1.0/chr${i}.frequencies.vcf"
  echo "view for chromosome $i"
  bcftools view \
    $RESOURCES/snps-and-indels-annotations/frequencies/all-frequencies-genome/0.2.0/chr${i}.frequencies.vcf.gz \
    -R $RESOURCES/intervals/agilent/v6-v7-v8-combined-padded/v6-v7-v8-combined-padded.bed | bcftools sort | bgzip > $OUTPUT.gz

  echo "Index for ${OUTPUT}"
  tabix -p vcf $OUTPUT.gz
done

