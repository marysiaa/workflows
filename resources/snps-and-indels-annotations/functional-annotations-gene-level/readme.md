# FUNCTIONAL ANNOTATIONS - GENE LEVEL (HPO) 

Resources to annotate VCF files with functional annotations (gene level). Reference genome: **GRCh38**
NOTE: Starting from the docker `intelliseqngs/task_vcf-anno-func-gene` version 1.2.0 these resources
contain the HPO data only.  

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/human-phenotype-ontology**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/human-phenotype-ontology**

---

**Directory contents:**

```bash 

functional-annotations-gene-level    
    │
human-phenotype-ontology
    │
update-date
    ├── gene-symbol-to-modes-of-inheritance.tsv
    └── gene-symbol-to-phenotypes.tsv
```

[Return to the table of contents](#table-of-contents)

---

## Human Phenotype Ontology

**Last update date:** 25-05-2021 (`intelliseqngs/hpo-resources:1.0.0`)

**Update requirements:** monthly

**History of updates:**
 - 25-05-2021
 - 15-12-2020
 - 12-02-2020
 - 01-10-2019

---
**Description:**

This folder contains a tab-delimited files: gene <=> hpo phenotypes, gene <=> inheritance mode.
These files are based on [HPO (Human Phenotype Ontology)](https://hpo.jax.org/app/) 
and Ensembl BioMart gene <=> ncbi gene mapping (v104).  
The new release of the HPO database is released approximately monthly. Ensembl is updated about 3-4 times a year.  

### Instructions on how to create HPO files

**1.** Create folder for the newest version of the database.

```bash
HPO_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/human-phenotype-ontology"
UPDATE_DATE="25-05-2021"
mkdir "$HPO_DIR"/"$UPDATE_DATE"
```
**2.** Prepare tsv files for annotation  
Resources were prepared within the [intelliseqngs/hpo-resources:1.0.0](https://gitlab.com/intelliseq/workflows/-/blob/vcf-anno-func-gene@1.3.0/src/main/docker/hpo-resources/Dockerfile) 
docker and copied to their destination folder. 
```bash
scripts/dockerbuilder -d src/main/docker/hpo-resources/Dockerfile ## build docker
docker run -v "$HPO_DIR"/"$UPDATE_DATE":/dest intelliseqngs/hpo-resources:1.0.0 /bin/bash -c "cp /outdir/* /dest" ## copy files
```
To build a new version of the resources you may use a slightly modified version of this docker,
[intelliseqngs/hpo-resources:1.0.1](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/docker/hpo-resources/Dockerfile):  
  
```bash
docker run -v "$HPO_DIR"/"$UPDATE_DATE":/outdir intelliseqngs/hpo-resources:1.0.1
```
which should:  
 - download the latest versions of the Ensembl and HPO data;
 - prepare files needed to annotation using Rscript [hpo-resources.R](https://gitlab.com/intelliseq/workflows/-/blob/hpo-resources.R@0.0.1/src/main/scripts/resources-tools/hpo-resources.R).  
The resulting annotation files will sit in the `"$HPO_DIR"/"$UPDATE_DATE"` (mounted to `/outdir` folder inside the docker container).  
You may also check whether the resulting files does not contain any non asci-ii characters:   
```bash
cat "$HPO_DIR"/"$UPDATE_DATE"/*.tsv | grep --color='auto' -P -o "[\x80-\xFF]" | sort | uniq ## should give no result
```
    
[Return to the top](#functional-annotations---gene-level-hpo)
