#  Cancer Gene Census
  
A `.tsv` file containing the Cancer Gene Census data.
  
---  
The location of resources:  
  - **Gitlab** (main location):  
    - https://gitlab.com/intelliseq/workflows/-/raw/dev/resources/cancer-gene-census/cancer_gene_census.tsv
---

### Updates
 
**Last update date:** 14-12-2022
     
---

The `.tsv` file is used as input to the `add-info-to-somatic-variants.py` and `vcf_filter_somatic.py` scripts, which are used in the `report-somatic.wdl` and `vcf-select-var-somatic.wdl` tasks.

---
