for i in {1..22} X
do
  wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.chr"$i".shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz
  wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.chr"$i".shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz.tbi
done

wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/integrated_call_samples_v3.20130502.ALL.panel
bcftools concat $(ls *.vcf.gz | grep ALL | sort -k1,1V | tr -s '\n' ' ' | sed 's/ *$//g') | bgzip -c > 1kg.merged.vcf.gz
tabix -p vcf 1kg.merged.vcf.gz
## describe how the GRCh38.dbSNP155.chr.norm.vcf.gz was created
## annotate 1000G with dbSNP 155
bcftools annotate 1kg.merged.chr.vcf.gz -c "ID" -a  GRCh38.dbSNP155.chr.norm.vcf.gz --collapse none -o tmp.rsid.chr.vcf.gz -O z
tabix -p vcf tmp.rsid.chr.vcf.gz

## remove annotations, set missing ids,
bcftools annotate -x '^INFO/VT' --set-id +'%CHROM\_%POS\_%REF\_%FIRST_ALT' tmp.vcf.gz -o tmp1.vcf.gz -O z
tabix -p vcf tmp1.vcf.gz

# add INFO/RS_ID field (Number:A)- needed to set variant id to specific allele and not the whole line
bcftools view -h tmp1.vcf.gz |  \
  sed 's/##INFO=<ID=VT,Number=.,Type=String,Description="indicates what type of variant the line represents">/##INFO=<ID=VT,Number=A,Type=String,Description="indicates variant type">\n##INFO=<ID=RS_ID,Number=A,Type=String,Description="Variant id">/' \
  | bgzip > tmp2.vcf.gz
bcftools view -H tmp1.vcf.gz | awk '{print $0";RS_ID="$3}' | bgzip >> tmp2.vcf.gz
tabix -p vcf tmp2.vcf.gz

## join variants into multiallelic lines: this is necessary to correctly merge with the genotyped vcf
bcftools norm -m +any tmp2.vcf.gz -o polygenic.vcf.gz -O z
tabix -p vcf polygenic.vcf.gz -O z

rm tmp*vcf.*



