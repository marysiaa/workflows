# Clinvar annotation

This data was created using script clinvar-annotation-script.sh. 

Environment is defined in docker image intelliseqngs/intelliseqngs/clinvar-annotation:2.5.0.

Dockerfile: /workflows/src/main/docker/resources/clinvar-annotation/Dockerfile

```bash
docker run \
  -v /data/public/intelliseqngs/workflows/resources/miscellaneous/clinvar-compare-acmg/:/outputs \
  -v /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38:/reference-genomes \
  intelliseqngs/clinvar-annotation:2.5.0

```

This vcf is created using vcf from clinvar and submission-summary.txt. 

Release: every month. 