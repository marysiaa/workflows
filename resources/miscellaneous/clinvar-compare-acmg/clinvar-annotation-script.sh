#!/bin/bash
# version: 2.5.0
# last update: 23.03.22
# author: Katarzyna Kolanek, Monika Krzyżanowska
# Dockerfile: https://gitlab.com/intelliseq/workflows/-/tree/dev/src/main/docker/clinvar-annotation/
# Dockerimage: intelliseqngs/clinvar-annotation/2.5.0

#TEST

set -e -o pipefail

DATE=$(date +%d-%m-%Y | sed 's/:/-/g')

echo "Creating directory with today's date..."
mkdir -p outputs/$DATE/
cd outputs/$DATE/

echo "Downloading data from clinvar..."
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz.tbi
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz

echo "Removing invalid characters from clinvar.vcf.gz..."
    zcat clinvar.vcf.gz | sed 's/ß/B/g' | sed 's/ã/a/g' | sed 's/ä/a/g' | \
    sed 's/é/e/g' | sed 's/è/e/g' | sed 's/ê/e/g' | sed 's/ë/e/g' | sed 's/ô/o/g' | \
    sed 's/ö/o/g' | sed 's/ú/u/g' | sed 's/ü/u/g' | grep -v '^NW_009646201.1' | \
    bgzip > tmp.vcf.gz
    zcat tmp.vcf.gz | awk '{if (/^#/) {print;} else if (/^MT/) {FS="\t"; OFS="\t"; $1="chrM"; print} else {print "chr"$0}}' | grep -v "NW_009646201.1" | bgzip > tmp1.vcf.gz
    tabix -p vcf tmp1.vcf.gz

echo "Normalize vcf..."
    bcftools norm --fasta-ref /reference-genomes/Homo_sapiens_assembly38.fa --multiallelics -any tmp1.vcf.gz -o tmp2.vcf.gz -O z

echo "Tabix: creating index for  vcf..."    
    tabix -p vcf tmp2.vcf.gz

echo "Running create-clinvar-annotation-file-no-included-info.py..."
    python3 /intelliseqtools/create-clinvar-annotation-file-no-included-info.py  \
    submission_summary.txt.gz \
    tmp2.vcf.gz | sed 's/enigma_rules,_2015/enigma_rules_2015/g' | bgzip > tmp3.vcf.gz 

echo "Tabix: creating index for vcf..."
    tabix -p vcf tmp3.vcf.gz

echo "Running vcf normalization and creating multiallelic lines..."
    bcftools norm -m +any tmp3.vcf.gz -f /reference-genomes/Homo_sapiens_assembly38.fa -o clinvar-annotation-file.vcf.gz -O z

echo "Tabix: creating index for vcf..."
    tabix -p vcf clinvar-annotation-file.vcf.gz    

echo "Creating bed file..."
    zcat clinvar-annotation-file.vcf.gz | awk '{print $1"\t"($2 - 1)"\t"$2}' | sort -k1,1d -k2,2n | grep -v "^#" | bgzip > clinvar-annotation-file.bed.gz

echo "Tabix: creating index for bed..."
    tabix -p bed clinvar-annotation-file.bed.gz

echo "Removing unnecessary files..."
    rm tmp*
    rm clinvar.vcf.gz*
    rm submission_summary.txt.gz 
    

# for testing:
# scripts/docker-build -t clinvar-annotation:X.X.X --context --nochecksum --nopush
# docker run --rm -it -v /tmp/monika:/outputs -v /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38:/reference-genomes --user $(id -u):$(id -g) intelliseqngs/clinvar-annotation:X.X.X
