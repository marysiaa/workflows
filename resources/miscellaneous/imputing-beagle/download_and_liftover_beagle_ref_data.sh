#!/usr/bin/env bash

VERSION='2.1.0'

dbsnp_file="GCF_000001405.39.gz"

function version() {
  script_name=`basename "$0"`
  printf "$script_name $VERSION\n"
}

function download_until_checksum_ok () {
  url=$1
  checksum=$2
  fname=`basename $url`

  touch $fname
  while [ $checksum != $(md5sum $fname | cut -f1 -d ' ') ]
  do
    rm $fname
    echo "Downloading $url"
    curl  -L \
          --retry 5 \
          -O $url
  done
}

while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
    -v|--version)
    version
    exit 0;;
    -o|--output)
    export OUTPUT_DIR=$2
    shift # past argument
    shift # past value
    ;;
    *)
    echo "Invalid argument"
    exit 1;
  esac
done


set -e -o pipefail

# download 1000G checksums
curl --fail -O http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/20190312_biallelic_SNV_and_INDEL_MANIFEST.txt

# download 1000G
for chrom in {1..22} X
do
  echo "Processing chrom $chrom"
  vcf_url=http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.chr"$chrom".shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz
  vcf=$( basename $vcf_url )
  vcf_tbi_url=$vcf_url.tbi
  vcf_tbi=$( basename $vcf_tbi_url )
  checksum_vcf=`grep $vcf[[:space:]] 20190312_biallelic_SNV_and_INDEL_MANIFEST.txt | cut -f 3`
  checksum_vcf_tbi=`grep $vcf_tbi[[:space:]] 20190312_biallelic_SNV_and_INDEL_MANIFEST.txt | cut -f 3`
  download_until_checksum_ok $vcf_url $checksum_vcf
  download_until_checksum_ok $vcf_tbi_url $checksum_vcf_tbi
done

## dbSNP checksums
curl -O https://ftp.ncbi.nih.gov/snp/latest_release/VCF/CHECKSUMS

## download dbSNP
echo "Downloading  dbSNP"
checksum=`grep $dbsnp_file$ CHECKSUMS | cut -f 1`
checksum_tbi=`grep $dbsnp_file.tbi$ CHECKSUMS | cut -f 1`
download_until_checksum_ok https://ftp.ncbi.nih.gov/snp/latest_release/VCF/$dbsnp_file $checksum
download_until_checksum_ok https://ftp.ncbi.nih.gov/snp/latest_release/VCF/$dbsnp_file.tbi $checksum_tbi

# contig names
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.39_GRCh38.p13/GCF_000001405.39_GRCh38.p13_assembly_report.txt

grep -v '^#' GCF_000001405.39_GRCh38.p13_assembly_report.txt | grep -v ^H | while read line;
do
  old=$( echo $line | cut -f7 -d ' ' )
  new=$( echo $line | cut -f1 -d ' ' )
  tabix -h $dbsnp_file "$old" | sed "s/$old/$new/" | bgzip > chr"$new".dbsnp.vcf.gz
  tabix -p vcf chr"$new".dbsnp.vcf.gz
done

### prepare reference file (with chromosome names 1,2,3...)
ref_file="/resources/Homo_sapiens_assembly/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa.gz"
for i in {1..22} X
do
  samtools faidx $ref_file chr"$i" | sed 's/>chr/>/' | bgzip >> grch38-ref.fa.gz
done
samtools faidx grch38-ref.fa.gz

### normalize indels in both datsets
for i in {1..22} X
do
  bcftools norm -c x -f grch38-ref.fa.gz chr"$i".dbsnp.vcf.gz -o chr"$i".dbsnp.norm.vcf.gz -O z
  tabix -p vcf chr"$i".dbsnp.norm.vcf.gz
done
for i in {1..22}
do
  bcftools norm -c x -f grch38-ref.fa.gz ALL.chr"$i".shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz -o chr"$i".1000g.norm.vcf.gz -O z
  tabix -p vcf chr"$i".1000g.norm.vcf.gz
done

### filter 1000G data
  ## 1. check number of samples
    N=$( zgrep '^#C' -m1 ALL.chr1.shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz | cut -f10- | wc -w )
    threshold_N=$(( 2*$N -5 ))
  ## 2. Remove variants with alt count below 5 or above 5091 and sites with low depth (less than 500)
  for i in {1..22}
  do
    s="bcftools filter -e 'INFO/AC <5 | INFO/AC > $threshold_N | INFO/DP < 500'  chr${i}.1000g.norm.vcf.gz -o chr${i}.1000g.norm.filtered.vcf.gz -O z"
    eval $s
  tabix -p vcf chr"$i".1000g.norm.filtered.vcf.gz
  done

### annotate 1000G with rsID add also ids to variants w-o  rs (chr:pos_ref_alt), remove INFO
for i in {1..22}
do
     bcftools annotate  chr"$i".1000g.norm.filtered.vcf.gz \
                        -c "ID" -a  chr"$i".dbsnp.norm.vcf.gz \
                        --set-id +'%CHROM\:%POS\_%REF\_%FIRST_ALT' \
                        --collapse none \
                        -x "INFO" \
                        -o chr"$i".1000g.norm.filtered.rsid.vcf.gz -O z
     tabix -p vcf chr"$i".1000g.norm.filtered.rsid.vcf.gz
done

### Find and remove duplicated ids (this step is described in the original beagle resources readme)
for i in {1..22}
do
  zgrep -v '^#'   chr${i}.1000g.norm.filtered.rsid.vcf.gz | cut -f3  | tr ';' "\n" | sort  > chr${i}.all.rsid;
done
sort -m chr*.all.rsid | uniq -d > duplicated.rsid

## check list and, if needed, remove duplicated ids (only from chr1 and 16)
for i in {1..22}
do
  bcftools filter   -e 'ID = @duplicated.rsid' \
                    chr"$i".1000g.norm.filtered.rsid.vcf.gz \
                    -o chr"$i".1000g.norm.rsid.filtered2.vcf.gz \
                    -O z
  tabix -p vcf -f chr"$i".1000g.norm.rsid.filtered2.vcf.gz
done

for i in {1..22}
do
   java -jar /tools/bref3.29May21.d6d.jar chr"$i".1000g.norm.rsid.filtered2.vcf.gz > "$i".38.bref3
done

mkdir chrX
cd chrX
curl -o X.b37.bref3 http://bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.bref3/chrX.1kg.phase3.v5a.b37.bref3
java -jar /tools/unbref3.29May21.d6d.jar X.b37.bref3 > X.b37.vcf
grep '^#' X.b37.vcf > header
CrossMap.py   vcf \
              /resources/chain_files/hg19ToHg38.over.chain.gz \
              X.b37.vcf \
              /resources/Homo_sapiens_assembly/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa.gz \
              X.hg38.vcf \
              --discard_unmapped

# after crossmap: it may happen, that some variants lie on chromosomes different than X
grep '^X' X.hg38.vcf | sort -k2,2 -n  > X.hg38.sorted.vcf
rm X.hg38.vcf

cut -f2 X.hg38.sorted.vcf |uniq -d > X.hg38.duplicated.vcf

python3 /intelliseqtools/remove_duplicated.py X.hg38.sorted.vcf X.hg38.final.vcf X.hg38.duplicated.vcf --invalid_out X.hg38.invalid.vcf
rm X.hg38.duplicated.vcf
rm X.hg38.invalid.vcf

sort -k2,2 -n X.hg38.final.vcf > X.hg38.final.sorted.vcf
rm X.hg38.final.vcf

cat header X.hg38.final.sorted.vcf > X.38.vcf
bgzip X.38.vcf

tabix -p vcf X.38.vcf.gz

## normalize indels, split, remove sites where ref does not match the reference
bcftools norm -c -x -m -any -f ../grch38-ref.fa.gz X.38.vcf.gz -o X.norm.vcf.gz -O z
tabix -p vcf X.norm.vcf.gz

## remove very common/rare variants
bcftools filter -e 'MAC < 5' X.norm.vcf.gz -o X.norm.filtered.vcf.gz -O z
tabix -p vcf X.norm.filtered.vcf.gz

## remove old rsid
bcftools annotate -x "ID" X.norm.filtered.vcf.gz -o X.norm.filtered.stripped.vcf.gz -O z
tabix -p vcf X.norm.filtered.stripped.vcf.gz

bcftools annotate X.norm.filtered.stripped.vcf.gz -c "ID" -a  ../chrX.dbsnp.norm.vcf.gz \
      --set-id +'%CHROM\:%POS\_%REF\_%FIRST_ALT' --collapse none -x "INFO" -o X.norm.filtered.rsid.vcf.gz -O z
     tabix -p vcf X.norm.filtered.rsid.vcf.gz


# remove duplicated lines
bcftools norm -d none   X.norm.filtered.rsid.vcf.gz -o X.norm.filtered2.rsid.vcf.gz -O z
tabix -p vcf X.norm.filtered2.rsid.vcf.gz

## make bref
java -jar /tools/bref3.29May21.d6d.jar X.norm.filtered2.rsid.vcf.gz > X.38.bref3

cd ..
cp *.38.bref3 $OUTPUT_DIR
cp chrX/X.38.bref3 $OUTPUT_DIR