# family trio

Raw reads, created by simulate-neat@1.0.0 to obtain 4 haploid sampes with coverage 15:

-sample with missnese mutation in ATAD3A gene (name: SNP, defined in vcf inside "vcf" directory")
-sample with around 8000 deletion in ATAD3A gene (name: DEL-8000, defined in vcf inside "vcf" directory")
-2 different sampses without any variants (names: EMPTY1, EMPTY2), EMPTY2 contains random variants, choosen from: https://www.ensembl.org/Homo_sapiens/Location/Variant/Table?r=1:1506374-1534374)

IF YOU WANT TO USE THIS SAMPLES PLEASE LOOK FOR RESOURCES DIRECTORY IN ANAKIN OR KENOBI - EVERYTHING IS THERE. THIS README DESCRIBES WHAT IS IT AND HOW WAS MADE. 

### About samples: 

FOR each of SNP, DEL-8000, EMPTY1, EMPTY2 there are reads for 28000 region in chr1, and 5MLN region in chr1. Precise start and stop is defined in bed files in "bed" directory.

- 28000 chr1
- 5MLN chr1
- 5MLN + sex chr 
- WGS

28000:
-child: DEL-8000 + SNP
-mother: SNP + EMPTY1
-father: DEL-8000 + EMPTY2

5MLN additonally:
mother: X1 + X2,
father: X3 + Y,
child: X1 + Y

Warning! Temporary SNP and DEL-8000 simulations are completely the same in sequences, because the same seed was used in generating. In progress changing

### How to create

In wdl-inputs there are inputs needed to create samples, to documentation purposes.
Sex chromosomes are in samples/sex-chr


