## CCDC6-RET fusion  

Path to files:  
`/data/public/intelliseqngs/workflows/resources/miscellaneous/simulated-samples/CCDC6-RET-fusion`  

Files:  
```bash
.
├── fusion-wgs.bed                                   ## WGS fusion bed
├── ccdc6-ret-fusion.bed                             ## WES fusion bed
├── panel-genes-exons.bed                            ## WES panel bed
├── panel-genes-genomic.bed                          ## WGS panel bed
├── arriba-wes                                       ## STAR + arriba results for merged WES samples 
│   ├── ccdc6-ret-fusion-wes.bam
│   ├── ccdc6-ret-fusion-wes.bam.bai
│   ├── ccdc6-ret-fusion-wes_fusions-discarded.tsv
│   └── ccdc6-ret-fusion-wes_fusions.tsv
├── arriba-wgs                                       ## STAR +arriba results for merged WGS samples 
│   ├── ccdc6-ret-fusion-wgs.bam
│   ├── ccdc6-ret-fusion-wgs.bam.bai
│   ├── ccdc6-ret-fusion-wgs_fusions-discarded.tsv
│   └── ccdc6-ret-fusion-wgs_fusions.tsv
├── ccdc6-ret-fusion-in-intron-dna.fa                ## fusion fasta file
├── ccdc6-ret-fusion-in-intron-dna.fa.fai            ## fusion fasta index
├── ccdc6-ret-fusion-simulated-wes_1.fq.gz           ## merged WES fastq
├── ccdc6-ret-fusion-simulated-wes_2.fq.gz           ## merged WES fastq
├── ccdc6-ret-fusion-simulated-wgs_1.fq.gz           ## merged WGS fastq
├── ccdc6-ret-fusion-simulated-wgs_2.fq.gz           ## merged WGS fastq
├── fusion-wes                                       ## fusion WES reads
│   ├── ccdc6-ret-fusion_1.fq.gz
│   └── ccdc6-ret-fusion_2.fq.gz
├── fusion-wgs                                       ## fusion WGS reads
│   ├── ccdc6-ret-fusion_1.fq.gz
│   └──ccdc6-ret-fusion_2.fq.gz
├── panel-wes                                        ## panel WES reads
│   ├── cancer-panel_1.fq.gz
│   └── cancer-panel_2.fq.gz
└── panel-wgs                                        ## panel WGS reads
    ├── cancer-panel_1.fq.gz
    └── cancer-panel_2.fq.gz
   

```

All fastq files were created wih the [simulate-neat.wdl](https://gitlab.com/intelliseq/workflows/-/blob/fusion-simulation/resources/miscellaneous/simulated-samples/samples/CCDC6-RET-fusion/simulate-neat.wdl) (version with fasta input)  

### Fusion reads  
The fasta file `ccdc6-ret-fusion-in-intron-dna.fa` contains first exon and 386 nucleotides of the first 
intron of the *CCDC6* gene (coding strand, which is the reverse genomic strand) followed by the partial genomic sequence of 
the *RET* gene (from the beginning of exon12 to the end of the gene, with introns, forward genomic strand). 
It represents the known inverted fusion of both genes:  
![CCDC6-RET fusion](ccdc6-ret-fusion.png).   

Reads were simulated with the following input jsons:
```bash
# WES (exons with +/1 100 bp padding)
{
    "simulate_neat_workflow.simulate_neat.average_mutation_rate": 0,
    "simulate_neat_workflow.simulate_neat.ploidy": 1,
    "simulate_neat_workflow.simulate_neat.no_jobs": 1,
    "simulate_neat_workflow.simulate_neat.read_length": 150,
    "simulate_neat_workflow.simulate_neat.off_target_coverage_scalar": 0,
    "simulate_neat_workflow.simulate_neat.output_golden_vcf_file": false,
    "simulate_neat_workflow.simulate_neat.output_golden_bam_file": false,
    "simulate_neat_workflow.simulate_neat.reference_genome": "grch38-no-alt",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta_fai": "/path/to/ccdc6-ret-fusion-in-intron-dna.fa.fai",
    "simulate_neat_workflow.simulate_neat.rng_seed_value": 0,
    "simulate_neat_workflow.simulate_neat.empirical_fragment_length_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.simulate_paired_end_reads": true,
    "simulate_neat_workflow.simulate_neat.average_coverage":  50,
    "simulate_neat_workflow.simulate_neat.simulated_sample_name": "ccdc6-ret-fusion",
    "simulate_neat_workflow.simulate_neat.average_error_rate":  0.001,
    "simulate_neat_workflow.simulate_neat.empirical_GC_coverage_bias_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta": "/path/to/ccdc6-ret-fusion-in-intron-dna.fa",
    "simulate_neat_workflow.simulate_neat.user_defined_targeted_regions_bed_file": "/path/to/ccdc6-ret-fusion.bed"
  }

  ## WGS (whole sequence present in the ccdc6-ret-fusion-in-intron-dna.fa file )
  {
    "simulate_neat_workflow.simulate_neat.average_mutation_rate": 0,
    "simulate_neat_workflow.simulate_neat.ploidy": 1,
    "simulate_neat_workflow.simulate_neat.no_jobs": 1,
    "simulate_neat_workflow.simulate_neat.read_length": 150,
    "simulate_neat_workflow.simulate_neat.off_target_coverage_scalar": 0,
    "simulate_neat_workflow.simulate_neat.output_golden_vcf_file": false,
    "simulate_neat_workflow.simulate_neat.output_golden_bam_file": false,
    "simulate_neat_workflow.simulate_neat.reference_genome": "grch38-no-alt",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta_fai": "/path/to/ccdc6-ret-fusion-in-intron-dna.fa.fai",
    "simulate_neat_workflow.simulate_neat.rng_seed_value": 0,
    "simulate_neat_workflow.simulate_neat.empirical_fragment_length_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.simulate_paired_end_reads": true,
    "simulate_neat_workflow.simulate_neat.average_coverage":  50,
    "simulate_neat_workflow.simulate_neat.simulated_sample_name": "ccdc6-ret-fusion",
    "simulate_neat_workflow.simulate_neat.average_error_rate":  0.001,
    "simulate_neat_workflow.simulate_neat.empirical_GC_coverage_bias_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta": "/path/to/ccdc6-ret-fusion-in-intron-dna.fa",
    "simulate_neat_workflow.simulate_neat.user_defined_targeted_regions_bed_file": "/tmp/kt/fusion-ccd6-ret/fusion-wgs.bed"
  }
  ```
The input `BED` files contain whole region present in the above
 fasta file (fusion-wgs.bed) or exons with 100bp padding (ccdc6-ret-fusion.bed).


### Panel reads   

The panel reads cover exons with 100bp padding (WES) or genomic sequence (WGS) of the following [genes](https://gitlab.com/intelliseq/workflows/-/blob/fusion-simulation/resources/miscellaneous/simulated-samples/samples/CCDC6-RET-fusion/panel-genes.txt).
The panel reads were simulated with the following inputs:  
```bash
## WES
{
    "simulate_neat_workflow.simulate_neat.average_mutation_rate": 0,
    "simulate_neat_workflow.simulate_neat.ploidy": 1,
    "simulate_neat_workflow.simulate_neat.no_jobs": 1,
    "simulate_neat_workflow.simulate_neat.read_length": 150,
    "simulate_neat_workflow.simulate_neat.off_target_coverage_scalar": 0,
    "simulate_neat_workflow.simulate_neat.output_golden_vcf_file": false,
    "simulate_neat_workflow.simulate_neat.output_golden_bam_file": false,
    "simulate_neat_workflow.simulate_neat.reference_genome": "grch38-no-alt",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta_fai": "/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa.fai",
    "simulate_neat_workflow.simulate_neat.rng_seed_value": 1,
    "simulate_neat_workflow.simulate_neat.empirical_fragment_length_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.simulate_paired_end_reads": true,
    "simulate_neat_workflow.simulate_neat.average_coverage":  200,
    "simulate_neat_workflow.simulate_neat.simulated_sample_name": "cancer-panel",
    "simulate_neat_workflow.simulate_neat.average_error_rate":  0.001,
    "simulate_neat_workflow.simulate_neat.empirical_GC_coverage_bias_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta": "/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa",
    "simulate_neat_workflow.simulate_neat.user_defined_targeted_regions_bed_file": "/path/to/panel-genes-exons.bed"
  }

  ## WGS
  {
    "simulate_neat_workflow.simulate_neat.average_mutation_rate": 0,
    "simulate_neat_workflow.simulate_neat.ploidy": 1,
    "simulate_neat_workflow.simulate_neat.no_jobs": 1,
    "simulate_neat_workflow.simulate_neat.read_length": 150,
    "simulate_neat_workflow.simulate_neat.off_target_coverage_scalar": 0,
    "simulate_neat_workflow.simulate_neat.output_golden_vcf_file": false,
    "simulate_neat_workflow.simulate_neat.output_golden_bam_file": false,
    "simulate_neat_workflow.simulate_neat.reference_genome": "grch38-no-alt",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta_fai": "/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa.fai",
    "simulate_neat_workflow.simulate_neat.rng_seed_value": 1,
    "simulate_neat_workflow.simulate_neat.empirical_fragment_length_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.simulate_paired_end_reads": true,
    "simulate_neat_workflow.simulate_neat.average_coverage":  200,
    "simulate_neat_workflow.simulate_neat.simulated_sample_name": "cancer-panel",
    "simulate_neat_workflow.simulate_neat.average_error_rate":  0.001,
    "simulate_neat_workflow.simulate_neat.empirical_GC_coverage_bias_distribution": "wgs-50x-pcr-free-150gb",
    "simulate_neat_workflow.simulate_neat.reference_genome_fasta": "/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa",
    "simulate_neat_workflow.simulate_neat.user_defined_targeted_regions_bed_file": "/path/to/panel-genes-genomic.bed"
  }
  ```

### merged reads
The fusion and panel reads were merged for WES and WGS. The coverage of the *CCDC6-RET* region should be about 250, with about 1/5 coming from the fusion samples.
```bash
 cat panel-wes/cancer-panel_1.fq.gz fusion-wes/ccdc6-ret-fusion_1.fq.gz > ccdc6-ret-fusion-simulated-wes_1.fq.gz
 cat panel-wes/cancer-panel_2.fq.gz fusion-wes/ccdc6-ret-fusion_2.fq.gz > ccdc6-ret-fusion-simulated-wes_2.fq.gz
 cat panel-wgs/cancer-panel_1.fq.gz fusion-wgs/ccdc6-ret-fusion_1.fq.gz > ccdc6-ret-fusion-simulated-wgs_1.fq.gz
 cat panel-wgs/cancer-panel_2.fq.gz fusion-wgs/ccdc6-ret-fusion_2.fq.gz > ccdc6-ret-fusion-simulated-wgs_2.fq.gz
``` 

### arriba output
Merged reads were aligned with STAR and analyzed with arriba (`fq-fusion-arriba.wdl`). 
The `arriba-wes` and `arriba-wgs` directories contain the output BAM, BAI and fusion TSV files.
 