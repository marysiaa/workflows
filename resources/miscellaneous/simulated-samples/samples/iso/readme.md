## iso sample

---

Simulated samples directories (on anakin):

`/data/test/large-data/iso-simulated-sample`


Directory contents:
``` bash
/data/test/large-data/iso-simulated-sample
  ├── 12-07-2021_simulated_2_wgs_annotated-with-acmg-summary.variants  #for germline test - first 5 colums from final vcf from germline 1.20.0 on this sample
  ├── clinvar-benign-random.vcf.gz  #for simulating
  ├── clinvar-pathogenic-random.positions-list  #for germline test - first two colums from clinvar-pathogenic-random.vcf.gz
  ├── clinvar-pathogenic-random.vcf.gz  #for simulating
  ├── clinvar.bed  #for simulating
  ├── gnomad-random.vcf.gz  #for simulating
  ├── input-random.vcf.gz  #for simulating: merged clinvar-benign-random.vcf.gz, clinvar-pathogenic-random.vcf.gz and gnomad-random.vcf.gz
  ├── simulated_2_1.fq.gz  #sample
  └── simulated_2_2.fq.gz  #sample
```


#### Script
``` bash

CLINVAR="/data/public/intelliseqngs/workflows/resources/acmg/clinvar/09-12-2020/clinvar.variant-level-annotated-with-snpeff.vcf.gz"
GNOMAD="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/04-11-2019"

echo -n > clinvar.bed
echo -n > gnomad-random-tmp.vcf
echo -n > clinvar-benign-random-tmp.vcf
for i in {1..22} "X"; do
        #two random pathogenic mutations
        zcat $CLINVAR | grep -w "^chr$i" | grep "CLINVAR_SIGNIFICANCE=pathogenic" | sort --random-sort --random-source=/dev/urandom | head -2 | sort -k2 -n > clinvar-chr$i.vcf
        #create bed
        awk -v OFS='\t' '{print $1, $2, $2}' clinvar-chr$i.vcf >  clinvar-chr$i.bed
        bedtools slop -b 3000 -i clinvar-chr$i.bed -g GRCh38.no_alt_analysis_set.genome | bedtools merge > clinvar-chr$i-padded-3000.bed
        #generate random gnomad variants 
        bcftools view -R clinvar-chr$i-padded-3000.bed $GNOMAD/chr$i.gnomad-genomes-v3.vcf.gz | bgzip > chr$i-gnomad.vcf.gz
        tabix -p vcf chr$i-gnomad.vcf.gz
        python3 /home/maria/workflows/resources/simulated-samples/gnomadrandom.py -i chr$i-gnomad.vcf.gz -f ISEQ_GNOMAD_GENOMES_V3_AF_nfe | bgzip > chr$i-gnomad-random.vcf.gz

        #add to one file for all chromosomes
         #copy header to other file
        if [ "$i" -eq "1" ]; then
                zcat chr$i-gnomad-random.vcf.gz | grep ^# > gnomad-random.vcf
                for vcf in "clinvar-pathogenic-random.vcf" "clinvar-benign-random.vcf"; do
                        zcat $CLINVAR | grep ^## > $vcf
                        echo "#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	synthetic_sample" >> $vcf
                done
        fi

         #copy (and add two last columns - `GT, 1/1` for clinvar pathogenic vcf)
        cat clinvar-chr$i-padded-3000.bed >> clinvar.bed
        cat clinvar-chr$i.vcf | awk -F"\t" '{OFS=FS}{$9="GT"; $10="1/1"; print}' >> clinvar-pathogenic-random.vcf

        #add gnomad and benign clinvar variants (all)
	zcat chr$i-gnomad-random.vcf.gz | grep -v ^# >> gnomad-random-tmp.vcf
        bcftools view -R clinvar-chr$i-padded-3000.bed $CLINVAR | grep "CLINVAR_SIGNIFICANCE=benign;" | grep -v ^# | awk -F"\t" '{OFS=FS}{$9="GT"; $10="1/1"; print}' >> clinvar-benign-random-tmp.vcf

        #delete chromosome files
        rm chr$i-gnomad-random.vcf.gz chr$i-gnomad.vcf.gz chr$i-gnomad.vcf.gz.tbi clinvar-chr$i.vcf clinvar-chr$i-padded-3000.bed clinvar-chr$i.bed
	echo "Done for chromosome " $i

done

# choose 400 (or less) gnomad variants
     sort --random-sort --random-source=/dev/urandom gnomad-random-tmp.vcf | head -400 | sort -V -k1,1 -k2,2n >> gnomad-random.vcf

# choose 100 (or less) benign clinvar variants
     sort --random-sort --random-source=/dev/urandom clinvar-benign-random-tmp.vcf | head -100 | sort -V -k1,1 -k2,2n  >> clinvar-benign-random.vcf


for i in "gnomad-random.vcf" "clinvar-pathogenic-random.vcf" "clinvar-benign-random.vcf"; do
        bgzip $i
        tabix -p vcf $i.gz
done

vcf-concat gnomad-random.vcf.gz clinvar-pathogenic-random.vcf.gz clinvar-benign-random.vcf.gz | vcf-sort | bgzip > input-random.vcf.gz
```


#### Input json for simulate_neat

``` bash 
{
  "simulate_neat_workflow.simulate_neat.vcf_gz_with_variants_to_insert": "input-random.vcf.gz",
  "simulate_neat_workflow.simulate_neat.user_defined_targeted_regions_bed_file": "clinvar.bed",
  "simulate_neat_workflow.simulate_neat.output_golden_bam_file": true,
  "simulate_neat_workflow.simulate_neat.no_jobs": 1,
  "simulate_neat_workflow.simulate_neat.predefined_targeted_regions": "whole-reference-sequence",
  "simulate_neat_workflow.simulate_neat.simulated_sample_name": "simulated_2",
  "simulate_neat_workflow.simulate_neat.average_coverage": "30",
  "simulate_neat_workflow.simulate_neat.ploidy": 2
}
```
