#!/bin/bash

## REGIONS DIFFICULT FOR MAPPING
## 1. prepare input files for simulation (bed and variant vcf)
zcat dangertrack.bed.gz | awk '$5 > 500' > tmp.bed
zcat dangertrack.bed.gz | awk '$5 == 500' >> tmp.bed
head -10000 tmp.bed | sort -k1,1V -k2,2n -k3,3n > hard-regions.bed
rm tmp.bed

for i in {1..22} X Y-and-the-rest; do
    bcftools view /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/04-11-2019/chr"$i".gnomad-genomes-v3.vcf.gz\
    -R hard-regions.bed -o chr"$i".hard.regions.vcf.gz -O z
done 

for i in *.hard.regions.vcf.gz; do tabix -p vcf $i;done

bcftools concat `ls  *.hard.regions.vcf.gz | sort -k1,1V` -n -o hard.regions.vcf.gz -O z 
tabix -p vcf hard.regions.vcf.gz

rm chr*.hard.regions.vcf.gz*

python3 ~/workflows/resources/simulated-samples/gnomadrandom.py \
   -f ISEQ_GNOMAD_GENOMES_V3_AF -i hard.regions.vcf.gz \
   | bgzip > variants.in.hard.regions.h1.vcf.gz

python3 ~/workflows/resources/simulated-samples/gnomadrandom.py \
   -f ISEQ_GNOMAD_GENOMES_V3_AF -i hard.regions.vcf.gz \
   | bgzip > variants.in.hard.regions.h2.vcf.gz

## 2. run simulations (wgs, 15x, ploidy 1)
cromwellnodb run ~/workflows/src/main/wdl/tasks/simulate-neat/simulate-neat.wdl \
   -i simulate-hard-h1.json  \
   -o options.json &> neat/hh1.log

cromwellnodb run ~/workflows/src/main/wdl/tasks/simulate-neat/simulate-neat.wdl \
   -i simulate-hard-h2.json \
   -o options.json &> neat/hh2.log

## 3. process output files (merge files to have diploid data)
## 3A. merge halotype golden vcfs, fill genotypes 
bcftools isec  results/hard_regions_h1-golden.vcf.gz results/hard_regions_h2-golden.vcf.gz \
   -p results/hard-vcf-isec

grep '^##' results/hard-vcf-isec/0000.vcf \
   | bgzip > results/hard_regions_golden.vcf.gz

printf '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotpe">\n' \
   | bgzip >> results/hard_regions_golden.vcf.gz

printf '#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tsynthetic_sample\n' \
   | bgzip >> results/hard_regions_golden.vcf.gz

grep -v '^#' results/hard-vcf-isec/0000.vcf \
   | awk -F '\t' 'BEGIN {OFS="\t"} {$9="GT"; $10 = "0/1"} {print $0}' > results/hard-tmp.vcf

grep -v '^#' results/hard-vcf-isec/0001.vcf \
   | awk -F '\t' 'BEGIN {OFS="\t"} {$9="GT"; $10 = "0/1"} {print $0}'>> results/hard-tmp.vcf

grep -v '^#' results/hard-vcf-isec/0002.vcf \
   | awk -F '\t' 'BEGIN {OFS="\t"} {$9="GT"; $10 = "1/1"} {print $0}'>> results/hard-tmp.vcf

sort -k1,1V -k2,2n results/hard-tmp.vcf \
   | sed 's/WP=1/WP=2/' |  bgzip >> results/hard_regions_golden.vcf.gz

tabix -p vcf results/hard_regions_golden.vcf.gz


## 3B. merge fq files
cat results/hard_regions_h1_1.fq.gz results/hard_regions_h2_1.fq.gz > results/hard_regions_1.fq.gz
cat results/hard_regions_h1_2.fq.gz results/hard_regions_h2_2.fq.gz > results/hard_regions_2.fq.gz

## 3C. merge golden bams
samtools merge results/hard_regions_golden.bam \
    results/hard_regions_h1-golden.bam \
    results/hard_regions_h2-golden.bam -O BAM
samtools index results/hard_regions_golden.bam

## 4. clean
rm variants.in.hard* hard.regions.vcf.gz* 
rm results/hard_regions_h1_1.fq.gz results/hard_regions_h2_1.fq.gz hard.regions.vcf.gz*
rm results/hard_regions_h1-golden.vcf.gz results/hard_regions_h2-golden.vcf.gz
rm results/hard_regions_h1-golden.bam* results/hard_regions_h2-golden.bam*
rm results/hard-tmp.vcf
rm -r results/hard-vcf-isec
rm results/hard_regions_h1-golden.bam* results/hard_regions_h2-golden.bam*
