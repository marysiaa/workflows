#!/bin/bash

## REGIONS EASY FOR MAPPING
## 1. prepare input files for simulation (bed and variant vcf)
zcat dangertrack.bed.gz  \
   | awk '$5 <300'| head -10000  \
   | sort -k1,1V -k2,2n -k3,3n  > easy-regions.bed

bcftools view /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/04-11-2019/chr1.gnomad-genomes-v3.vcf.gz \
   -R easy-regions.bed -o easy.regions.vcf.gz -O z

tabix -p vcf easy.regions.vcf.gz

python3 ~/workflows/resources/simulated-samples/gnomadrandom.py \
   -f ISEQ_GNOMAD_GENOMES_V3_AF -i easy.regions.vcf.gz \
   | bgzip > variants.in.easy.regions.h1.vcf.gz
  
python3 ~/workflows/resources/simulated-samples/gnomadrandom.py \
   -f ISEQ_GNOMAD_GENOMES_V3_AF -i easy.regions.vcf.gz \
   | bgzip > variants.in.easy.regions.h2.vcf.gz

## 2. run simulations (wgs, 15x, ploidy 1)
cromwellnodb run ~/workflows/src/main/wdl/tasks/simulate-neat/simulate-neat.wdl \
   -i simulate-easy-h1.json  \
   -o options.json &> neat/eh1.log

cromwellnodb run ~/workflows/src/main/wdl/tasks/simulate-neat/simulate-neat.wdl \
   -i simulate-easy-h2.json \
   -o options.json &> neat/eh2.log

## 3. process output files: create diploids from two haploids
## 3A. merge halotype golden vcfs, fill genotypes 
bcftools isec  results/easy_regions_h1-golden.vcf.gz results/easy_regions_h2-golden.vcf.gz \
   -p results/easy-vcf-isec

grep '^##' results/easy-vcf-isec/0000.vcf \
   | bgzip > results/easy_regions_golden.vcf.gz

printf '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotpe">\n' \
   | bgzip >> results/easy_regions_golden.vcf.gz

printf '#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tsynthetic_sample\n' \
   | bgzip >> results/easy_regions_golden.vcf.gz

grep -v '^#' results/easy-vcf-isec/0000.vcf \
   | awk -F '\t' 'BEGIN {OFS="\t"} {$9="GT"; $10 = "0/1"} {print $0}' > results/easy-tmp.vcf

grep -v '^#' results/easy-vcf-isec/0001.vcf \
   | awk -F '\t' 'BEGIN {OFS="\t"} {$9="GT"; $10 = "0/1"} {print $0}'>> results/easy-tmp.vcf

grep -v '^#' results/easy-vcf-isec/0002.vcf \
   | awk -F '\t' 'BEGIN {OFS="\t"} {$9="GT"; $10 = "1/1"} {print $0}'>> results/easy-tmp.vcf

sort -k1,1V -k2,2n results/easy-tmp.vcf \
   | sed 's/WP=1/WP=2/' |  bgzip >> results/easy_regions_golden.vcf.gz

tabix -p vcf results/easy_regions_golden.vcf.gz

## 3B. merge fq files 
cat results/easy_regions_h1_1.fq.gz results/easy_regions_h2_1.fq.gz > results/easy_regions_1.fq.gz
cat results/easy_regions_h1_2.fq.gz results/easy_regions_h2_2.fq.gz > results/easy_regions_2.fq.gz

## 3C. merge golden bams
samtools merge results/easy_regions_golden.bam \
    results/easy_regions_h1-golden.bam \
    results/easy_regions_h2-golden.bam -O BAM
samtools index results/easy_regions_golden.bam

## 4. clean
rm variants.in.easy* easy.regions.vcf.gz* 
rm results/easy_regions_h1_1.fq.gz results/easy_regions_h2_1.fq.gz easy.regions.vcf.gz*
rm results/easy_regions_h1-golden.vcf.gz results/easy_regions_h2-golden.vcf.gz
rm results/easy_regions_h1-golden.bam* results/easy_regions_h2-golden.bam*
rm results/easy-tmp.vcf
rm -r resclearults/easy-vcf-isec
rm results/easy_regions_h1-golden.bam* results/easy_regions_h2-golden.bam*


