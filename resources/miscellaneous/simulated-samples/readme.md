#  SIMULATED SAMPLES

Miscellaneous resources.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/miscellaneous**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/miscellaneous**

---

## Table of Contents

**NEAT-genReads models**

  * [NEAT genReads, WES models](#neat-genreads-wes-models)
  * [NEAT genReads, WGS models](#neat-genreads-wgs-models)

**NEAT-genReads simulated-samples**

   * **WES**
     * [NF2 gene, chr22](#nf2-gene-chr22-wes)
     * [chr22](#chr22-wes)
   * **WGS**
     * [NF2 gene, chr22](#nf2-gene-chr22-wgs)

[Return to: Resources](./../readme.md)

---


## NEAT genReads, WES models

**Last update date:** 24-02-2020

**Update requirements:** unknown

---

**Description:**

WES, PE150, 20M, 6Gb

[Instuctions on how to create NEAT genReads models based on WES, PE150, 20M, 6Gb samples](#instuctions-on-how-to-create-neat-genreads-models-based-on-wes-pe150-20m-6gb-samples)

**Directory contents:**


---

### Instuctions on how to create NEAT genReads models based on WES, PE150, 20M, 6Gb samples

**1.**  Set variables. WES sample 449 comes from *shipment 38*.

```bash
SEQUENCING_TYPE="wes-pe150-20m-150gb"
SAMPLE_ID="499"
BAM="/data/ngs-projects/jp2-wes/per-patient-info/shipment-38-wes_499/run-analysis/results/"$SAMPLE_ID"/"$SAMPLE_ID".markdup.recalibrated.bam"
REF_FA_GZ="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz"
REF_FA="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa"

compute_fraglen_py="/opt/tools/neat-genreads/utilities/computeFraglen.py"
compute_gc_py="/opt/tools/neat-genreads/utilities/computeGC.py"
genseq_error_model_py="/opt/tools/neat-genreads/utilities/genSeqErrorModel.py"

NEAT_GENREADS_DIR="/data/public/intelliseqngs/workflows/resources/simulated-samples/nest-genreads"
mkdir -p "$NEAT_GENREADS_DIR"/"$SEQUENCING_TYPE"
cd "$NEAT_GENREADS_DIR"/"$SEQUENCING_TYPE"
```


**2.**  Compute empirical fragment length distribution from sample data:

```bash
 samtools view "$BAM" | python2.7 $compute_fraglen_py &> "$SAMPLE_ID"_compute-fraglen.log
 mv fraglen.p "$SAMPLE_ID"_fraglen.p
```

**3**. Bug fix in computeGC.py script fron NEAT-genReads software:

```bash
cat $compute_gc_py | sed "s/refName = line.strip()\[1:\]/refName = line.split()\[0\].replace('>', '')/g" > computeGC.py
```

**4.** Compute GC% coverage bias distribution from sample.

**Requires:** bedtools

```bash
bedtools genomecov -d -g $REF_FA_GZ -ibam $BAM > "$SAMPLE_ID"_genomecovfile
```

```bash
python2.7 computeGC.py -r $REF_FA -i "$SAMPLE_ID"_genomecovfile -w 50 -o ./"$SAMPLE_ID"_model.p > "$SAMPLE_ID"_compute-gc.log
```

**5.** Remove *_genomecovfile (due to it's large size)

```bash
rm "$SAMPLE_ID"_genomecovfile
```


[Return to the table of contents](#table-of-contents)



---

## NEAT genReads, WGS models

**Last update date:** 24-02-2020

**Update requirements:** unknown

---

**Description:**

WGS 50x PCR-free, 150Gb

[Instuctions on how to create NEAT genReads models based on WGS 50x PCR free, 150Gb samples](#instuctions-on-how-to-create-neat-genreads-models-based-on-wgs-50x-pcr-free-150gb-samples)

**Directory contents:**


---

### Instuctions on how to create NEAT genReads models based on WGS 50x PCR free, 150Gb samples

**1.**  Set  variables. WGS samples comes from *shipment 29*, samples ids are:  440, 441, 442.

```bash
SEQUENCING_TYPE="wgs-50x-pcr-free-150gb"
SAMPLE_ID="440"
BAM="/data/ngs-projects/shipment-29-wgs/bams/"$SAMPLE_ID".markdup.realigned.recalibrated.bam"
REF_FA_GZ="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz"
REF_FA="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa"

compute_fraglen_py="/opt/tools/neat-genreads/utilities/computeFraglen.py"
compute_gc_py="/opt/tools/neat-genreads/utilities/computeGC.py"
genseq_error_model_py="/opt/tools/neat-genreads/utilities/genSeqErrorModel.py"

NEAT_GENREADS_DIR="/data/public/intelliseqngs/workflows/resources/simulated-samples/nest-genreads"
mkdir -p "$NEAT_GENREADS_DIR"/"$SEQUENCING_TYPE"
cd "$NEAT_GENREADS_DIR"/"$SEQUENCING_TYPE"
```

**2.**  Compute empirical fragment length distribution from sample data:

```bash
 samtools view "$BAM" | python2.7 $compute_fraglen_py &> "$SAMPLE_ID"_compute-fraglen.log
 mv fraglen.p "$SAMPLE_ID"_fraglen.p
```

**3**. Bug fix in computeGC.py script fron NEAT-genReads software:

```bash
cat $compute_gc_py | sed "s/refName = line.strip()\[1:\]/refName = line.split()\[0\].replace('>', '')/g" > computeGC.py
```

**4.** Compute GC% coverage bias distribution from sample.

**Requires:** bedtools

```bash
bedtools genomecov -d -g $REF_FA_GZ -ibam $BAM > "$SAMPLE_ID"_genomecovfile
```

```bash
python2.7 computeGC.py -r $REF_FA -i "$SAMPLE_ID"_genomecovfile -w 50 -o ./"$SAMPLE_ID"_model.p > "$SAMPLE_ID"_compute-gc.log
```

**5.** Remove *_genomecovfile (due to it's large size)

```bash
rm "$SAMPLE_ID"_genomecovfile
```

[Return to the table of contents](#table-of-contents)

---

### NF2 gene, chr22 WES

|                           | |  
|---------------------------|-|  
| **Sample ID:** |  nf2-set-1-wes-pe150-20m-150gb-FLD-GCD |  
| **Seqencing type**:       | [wes-pe150-20m-150gb](#neat-genreads-wes-models) |  
| **Reference genome:** | broad-institute-hg38 |  
| **Regions:** | chr22, NF2 gene |  
| **Inserted variants:** | [NF2 gene variants - set 1](#../../src/test/resources/data/vcf/readme.md#nf2-gene-chr22) |  

**Description:**

This directory contains synthetic reads generated with [NEAT-genReads](https://github.com/zstephens/neat-genreads/) software that are meant to mimic reads generated from NGS platform.  There were following files generated:
 * (bgzipped) FASTQ,
 * 'golden' BAMs and VCFs ('golden' set of aligned reads and 'golden' set of true positive variants - more information can bee found on  [NEAT-genReads](https://github.com/zstephens/neat-genreads/) site)
 * BAMs - aligned with the use of BWA MEM


There are several versions that differ in which user-defined models were provided:

* (MAIN) **nf2-set-1-wes-pe150-20m-150gb-FLD-GCD** - *both fraglen.p and model.p for [wes-pe150-20m-150gb](#neat-genreads-wes-models) were used*
* nf2-set-1-wes-pe150-20m-150gb-FLD - *only fraglen.p for [wes-pe150-20m-150gb](#neat-genreads-wes-models) was used*
* nf2-set-1-wes-pe150-20m-150gb-GCD - *only model.p for [wes-pe150-20m-150gb](#neat-genreads-wes-models) was used*
* nf2-set-1-wes-pe150-20m-150gb - *no models were used*

**nf2-set-1-wes-pe150-20m-150gb-FLD-GCD** is, for now, the main version. Other versions are provided for comparison purposes.

---

**How those files were created?**

Files were created with [simulate-sample-neat-genreads.wdl](https://gitlab.com/intelliseq/workflows/-/raw/fe19e91fbb1a7877ddb9686fdbe5f10c533c4f5f/src/main/wdl/modules/simulate-sample-neat-genreads/latest/simulate-sample-neat-genreads.wdl) module with the use of following [JSON files](#nf2-wes-input-jsons).

---

**List of files**

```bash
   nf2
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.bam
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.bam.bai
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.vcf.gz
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.vcf.gz.tbi
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-markdup.bam
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-markdup.bam.bai
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD_1.fq.gz
    ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD_2.fq.gz
    └── simulated-samples-for-comparison-purposes
        ├── empirical-GC-coverage-bias-distribution
        │   ├── nf2-set-1-wes-pe150-20m-150gb-GCD-golden.bam
        │   ├── nf2-set-1-wes-pe150-20m-150gb-GCD-golden.bam.bai
        │   ├── nf2-set-1-wes-pe150-20m-150gb-GCD-golden.vcf.gz
        │   ├── nf2-set-1-wes-pe150-20m-150gb-GCD-golden.vcf.gz.tbi
        │   ├── nf2-set-1-wes-pe150-20m-150gb-GCD-markdup.bam
        │   ├── nf2-set-1-wes-pe150-20m-150gb-GCD-markdup.bam.bai
        │   ├── nf2-set-1-wes-pe150-20m-150gb-GCD_1.fq.gz
        │   └── nf2-set-1-wes-pe150-20m-150gb-GCD_2.fq.gz
        ├── empirical-GC-coverage-bias-distribution_and_empirical-fragment-length-distribution
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.bam -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.bam
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.bam.bai -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.bam.bai
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.vcf.gz -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.vcf.gz
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.vcf.gz.tbi -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-golden.vcf.gz.tbi
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-markdup.bam -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-markdup.bam
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-markdup.bam.bai -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD-markdup.bam.bai
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD_1.fq.gz -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD_1.fq.gz
        │   └── nf2-set-1-wes-pe150-20m-150gb-FLD-GCD_2.fq.gz -> ../../nf2-set-1-wes-pe150-20m-150gb-FLD-GCD_2.fq.gz
        ├── empirical-fragment-length-distribution
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-golden.bam
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-golden.bam.bai
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-golden.vcf.gz
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-golden.vcf.gz.tbi
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-markdup.bam
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD-markdup.bam.bai
        │   ├── nf2-set-1-wes-pe150-20m-150gb-FLD_1.fq.gz
        │   └── nf2-set-1-wes-pe150-20m-150gb-FLD_2.fq.gz
        └── no-models
            ├── nf2-set-1-wes-pe150-20m-150gb-no-models-golden.bam
            ├── nf2-set-1-wes-pe150-20m-150gb-no-models-golden.bam.bai
            ├── nf2-set-1-wes-pe150-20m-150gb-no-models-golden.vcf.gz
            ├── nf2-set-1-wes-pe150-20m-150gb-no-models-golden.vcf.gz.tbi
            ├── nf2-set-1-wes-pe150-20m-150gb-no-models-markdup.bam
            ├── nf2-set-1-wes-pe150-20m-150gb-no-models-markdup.bam.bai
            ├── nf2-set-1-wes-pe150-20m-150gb-no-models_1.fq.gz
            └── nf2-set-1-wes-pe150-20m-150gb-no-models_2.fq.gz
```

[Return to the table of contents](#table-of-contents)

---

### NF2 WES input JSONs

**TASK_TEST_RESOURCES** variable points to **src/test/resources/**


(MAIN) **nf2-set-1-wes-pe150-20m-150gb-FLD-GCD**
```bash
{ "simulate_sample_neat_genreads_workflow.empirical_GC_coverage_bias_distribution": "wes-pe150-20m-150gb",
  "simulate_sample_neat_genreads_workflow.empirical_fragment_length_distribution": "wes-pe150-20m-150gb",
  "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wes-pe150-20m-150gb-FLD-GCD",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene-sureselect-human-all-exon-v6-r2.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

 **nf2-set-1-wes-pe150-20m-150gb-FLD**

```bash
{ "simulate_sample_neat_genreads_workflow.empirical_fragment_length_distribution": "wes-pe150-20m-150gb",
  "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wes-pe150-20m-150gb-FLD",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene-sureselect-human-all-exon-v6-r2.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

(MAIN) **nf2-set-1-wes-pe150-20m-150gb-GCD**

```bash
{ "simulate_sample_neat_genreads_workflow.empirical_GC_coverage_bias_distribution": "wes-pe150-20m-150gb",
  "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wes-pe150-20m-150gb-GCD",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene-sureselect-human-all-exon-v6-r2.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

**nf2-set-1-wes-pe150-20m-150gb-no-models**

```bash
{ "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wes-pe150-20m-150gb-no-models",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene-sureselect-human-all-exon-v6-r2.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

 [Return to the table of contents](#table-of-contents)

---

### chr22 WES

|                           | |  
|---------------------------|-|  
| **Sample ID:** | 1-synthetic-wes-chr22 |  
| **Seqencing type**:       | [1-wes-pe150-20m-150gb](#neat-genreads-wes-models) |  
| **Reference genome:** | broad-institute-hg38 |  
| **Regions:** | chr22 |  

**Description:**

This directory contains synthetic reads generated with [NEAT-genReads](https://github.com/zstephens/neat-genreads/) software that are meant to mimic reads generated from NGS platform for WES sequencing, restricted to chr22.  There were following files generated:
 * (bgzipped) FASTQ,
 * 'golden' BAMs and VCFs ('golden' set of aligned reads and 'golden' set of true positive variants - more information can bee found on  [NEAT-genReads](https://github.com/zstephens/neat-genreads/) site)
 * BAMs - aligned with the use of BWA MEM


**List of files:**

```bash
  chr22
    ├── 1-synthetic-wes-chr22-golden.bam
    ├── 1-synthetic-wes-chr22-golden.bam.bai
    ├── 1-synthetic-wes-chr22-golden.vcf.gz
    ├── 1-synthetic-wes-chr22-golden.vcf.gz.tbi
    ├── 1-synthetic-wes-chr22-markdup.bam
    ├── 1-synthetic-wes-chr22-markdup.bam.bai
    ├── 1-synthetic-wes-chr22_1.fq.gz
    └── 1-synthetic-wes-chr22_2.fq.gz
```
---

**How those files were created?**

Files were created with [simulate-sample-neat-genreads.wdl](https://gitlab.com/intelliseq/workflows/-/raw/fe19e91fbb1a7877ddb9686fdbe5f10c533c4f5f/src/main/wdl/modules/simulate-sample-neat-genreads/latest/simulate-sample-neat-genreads.wdl) module with the use of following JSON files:

```bash
{ "simulate_sample_neat_genreads_workflow.simulated_sample_name": "1-synthetic-wes-chr22",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.01,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

---
 [Return to the table of contents](#table-of-contents)


### NF2 gene, chr22 WGS

|                           | |  
|---------------------------|-|  
| **Sample ID:** |  nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD |  
| **Seqencing type**:       | [wgs-50x-pcr-free-150gb](#neat-genreads-wgs-models) |  
| **Reference genome:** | broad-institute-hg38 |  
| **Regions:** | chr22, NF2 gene |  
| **Inserted variants:** | [NF2 gene variants - set 1](#../../src/test/resources/data/vcf/readme.md#nf2-gene-chr22) |  

**Description:**

This directory contains synthetic reads generated with [NEAT-genReads](https://github.com/zstephens/neat-genreads/) software that are meant to mimic reads generated from NGS platform.  There were following files generated:
 * (bgzipped) FASTQ,
 * 'golden' BAMs and VCFs ('golden' set of aligned reads and 'golden' set of true positive variants - more information can bee found on  [NEAT-genReads](https://github.com/zstephens/neat-genreads/) site)
 * BAMs - aligned with the use of BWA MEM


There are several versions that differ in which user-defined models were provided:

* (MAIN) **nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD** - *both fraglen.p and model.p for [wgs-50x-pcr-free-150gb](#neat-genreads-wgs-models) were used*
* nf2-set-1-wgs-50x-pcr-free-150gb-FLD - *only fraglen.p for [wgs-50x-pcr-free-150gb](#neat-genreads-wgs-models) was used*
* nf2-set-1-wgs-50x-pcr-free-150gb-GCD - *only model.p for [wgs-50x-pcr-free-150gb](#neat-genreads-wgs-models) was used*
* nf2-set-1-wgs-50x-pcr-free-150gb - *no models were used*

**nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD** is, for now, the main version. Other versions are provided for comparison purposes.

---

**How those files were created?**

Files were created with [simulate-sample-neat-genreads.wdl](https://gitlab.com/intelliseq/workflows/-/raw/fe19e91fbb1a7877ddb9686fdbe5f10c533c4f5f/src/main/wdl/modules/simulate-sample-neat-genreads/latest/simulate-sample-neat-genreads.wdl) module with the use of following [JSON files](#nf2-wgs-input-jsons).


---

**List of files**

```bash
   nf2
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.bam
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.bam.bai
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.vcf.gz
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.vcf.gz.tbi
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-markdup.bam
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-markdup.bam.bai
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD_1.fq.gz
    ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD_2.fq.gz
    └── simulated-samples-for-comparison-purposes
        ├── empirical-GC-coverage-bias-distribution
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-GCD-golden.bam
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-GCD-golden.bam.bai
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-GCD-golden.vcf.gz
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-GCD-golden.vcf.gz.tbi
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-GCD-markdup.bam
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-GCD-markdup.bam.bai
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-GCD_1.fq.gz
        │   └── nf2-set-1-wgs-50x-pcr-free-150gb-GCD_2.fq.gz
        ├── empirical-GC-coverage-bias-distribution_and_empirical-fragment-length-distribution
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.bam -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.bam
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.bam.bai -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.bam.bai
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.vcf.gz -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.vcf.gz
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.vcf.gz.tbi -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-golden.vcf.gz.tbi
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-markdup.bam -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-markdup.bam
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-markdup.bam.bai -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD-markdup.bam.bai
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD_1.fq.gz -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD_1.fq.gz
        │   └── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD_2.fq.gz -> ../../nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD_2.fq.gz
        ├── empirical-fragment-length-distribution
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-golden.bam
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-golden.bam.bai
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-golden.vcf.gz
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-golden.vcf.gz.tbi
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-markdup.bam
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD-markdup.bam.bai
        │   ├── nf2-set-1-wgs-50x-pcr-free-150gb-FLD_1.fq.gz
        │   └── nf2-set-1-wgs-50x-pcr-free-150gb-FLD_2.fq.gz
        └── no-models
            ├── nf2-set-1-wgs-50x-pcr-free-150gb-no-models-golden.bam
            ├── nf2-set-1-wgs-50x-pcr-free-150gb-no-models-golden.bam.bai
            ├── nf2-set-1-wgs-50x-pcr-free-150gb-no-models-golden.vcf.gz
            ├── nf2-set-1-wgs-50x-pcr-free-150gb-no-models-golden.vcf.gz.tbi
            ├── nf2-set-1-wgs-50x-pcr-free-150gb-no-models-markdup.bam
            ├── nf2-set-1-wgs-50x-pcr-free-150gb-no-models-markdup.bam.bai
            ├── nf2-set-1-wgs-50x-pcr-free-150gb-no-models_1.fq.gz
            └── nf2-set-1-wgs-50x-pcr-free-150gb-no-models_2.fq.gz
```

[Return to the table of contents](#table-of-contents)

---

### NF2 WGS input JSONs

**TASK_TEST_RESOURCES** variable points to **src/test/resources/**


(MAIN) **nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD**
```bash
{ "simulate_sample_neat_genreads_workflow.empirical_GC_coverage_bias_distribution": "wgs-50x-pcr-free-150gb",
  "simulate_sample_neat_genreads_workflow.empirical_fragment_length_distribution": "wgs-50x-pcr-free-150gb",
  "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wgs-50x-pcr-free-150gb-FLD-GCD",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

 **nf2-set-1-wgs-50x-pcr-free-150gb-FLD**

```bash
{ "simulate_sample_neat_genreads_workflow.empirical_fragment_length_distribution": "wgs-50x-pcr-free-150gb",
  "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wgs-50x-pcr-free-150gb-FLD",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

(MAIN) **nf2-set-1-wgs-50x-pcr-free-150gb-GCD**

```bash
{ "simulate_sample_neat_genreads_workflow.empirical_GC_coverage_bias_distribution": "wgs-50x-pcr-free-150gb",
  "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wgs-50x-pcr-free-150gb-GCD",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

**nf2-set-1-wgs-50x-pcr-free-150gb-no-models**

```bash
{ "simulate_sample_neat_genreads_workflow.simulated_sample_name": "nf2-set-1-wgs-50x-pcr-free-150gb-no-models",
  "simulate_sample_neat_genreads_workflow.reference_genome": "broad-institute-hg38",
  "simulate_sample_neat_genreads_workflow.chromosome": "chr22",
  "simulate_sample_neat_genreads_workflow.predefined_targeted_regions": "agilent-sureselect-human-all-exon-v6-r2",
  "simulate_sample_neat_genreads_workflow.user_defined_targeted_regions_bed_file": "$TASK_TEST_RESOURCES/data/bed/chr22-nf2-gene.bed",
  "simulate_sample_neat_genreads_workflow.vcf_gz_with_variants_to_insert": "$TASK_TEST_RESOURCES/data/vcf/nf2-gene-variants-set-1.vcf.gz",
  "simulate_sample_neat_genreads_workflow.neat_genreads.off_target_coverage_scalar": 0.00,
  "simulate_sample_neat_genreads_workflow.simulate_paired_end_reads": true,
  "simulate_sample_neat_genreads_workflow.read_length": 150,
  "simulate_sample_neat_genreads_workflow.average_coverage": 50,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_std": 30,
  "simulate_sample_neat_genreads_workflow.paired_end_fragment_lenghth_mean": 300,
  "simulate_sample_neat_genreads_workflow.output_golden_vcf_file": true,
  "simulate_sample_neat_genreads_workflow.output_golden_bam_file": true,
  "simulate_sample_neat_genreads_workflow.bwa_mem_arguments": "-K 100000000 -v 3 -Y ",
  "simulate_sample_neat_genreads_workflow.alignment_bwa_mem_no_threads": 8,
  "simulate_sample_neat_genreads_workflow.neat_genreads": 1}
```

[Return to the table of contents](#table-of-contents)
