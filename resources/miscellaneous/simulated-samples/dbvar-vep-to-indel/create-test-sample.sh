#!/bin/bash

variant_id=$1
chr=$2

mkdir $variant_id
cd $variant_id

bash take-dbvar-variant.sh $variant_id $chr

tabix vep-${variant_id}.vcf.gz

python3 dbvar-vep-to-indel.py -r /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz -s vep-${variant_id}.vcf.gz -o indel-${variant_id}.vcf.gz
