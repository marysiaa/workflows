#!/usr/bin/env python3

import gzip
import datetime
from collections import namedtuple
from typing import Dict, List
import argparse

from pysam import VariantFile
from pysam import FastaFile

__version__ = '1.0.0'

class AnnotationInfo(object):

    def __init__(self, info_id: str, info_number: str, info_type: str, info_description: str, info_source=None,
                 info_version=None, source_field_id=None):
        super().__init__()
        self.info_id = info_id
        self.info_number = info_number
        self.info_type = info_type
        self.info_description = info_description
        self.info_source = info_source
        self.info_version = info_version
        self.source_field_id = source_field_id

    def get_header_info_line(self):
        info_source = '' if None else ',Source="{}"'.format(self.info_source)
        info_version = '' if None else ',Version="{}"'.format(self.info_version)

        return '##INFO=<ID={},Number={},Type={},Description="{}"{}{}>'.format(self.info_id, self.info_number,
                                                                              self.info_type,
                                                                              self.info_description, info_source,
                                                                              info_version)

class _Annotations(object):
    AnnotationsList: List[AnnotationInfo] = list()

    SVTYPE = AnnotationInfo(
        info_id='SVTYPE',
        info_number='A',
        info_type='String',
        info_description='Type of structural variant',
        source_field_id='SVTYPE')
    AnnotationsList.append(SVTYPE)

    SVLEN = AnnotationInfo(
        info_id='SVTYPE',
        info_number='A',
        info_type='String',
        info_description='Difference in length between REF and ALT alleles',
        source_field_id='SVLEN')
    AnnotationsList.append(SVLEN)

    END = AnnotationInfo(
        info_id='SVTYPE',
        info_number='A',
        info_type='Integer',
        info_description='End position of the variant described in this record',
        source_field_id='END')
    AnnotationsList.append(END)

    SEQ = AnnotationInfo(
        info_id='SVTYPE',
        info_number='A',
        info_type='String',
        info_description='Variation sequence',
        source_field_id='SEQ')
    AnnotationsList.append(SEQ)

def new_record(output_vcf, original_record, start, stop, REF, ALT):

    new_record = output_vcf.new_record(
        contig=original_record.contig, start=start, stop=stop,
        alleles=(REF, ALT),
        id=original_record.id, qual=original_record.qual, filter=original_record.filter, info=None)
    new_record.samples['synthetic_sample']['GT'] = (0, 1)
    output_vcf.write(new_record)

def add_duplication_record(ref_fa_gz, output_vcf, original_record):

    svlen = int(original_record.info[_Annotations.SVLEN.source_field_id][0])

    start = original_record.start - 1
    stop = original_record.stop
    contig = original_record.contig
    ALT = ref_fa_gz.fetch(contig, start, original_record.start + svlen)
    REF = ALT[0]
    new_record(output_vcf, original_record, start, stop, REF, ALT)

def add_inversion_record(ref_fa_gz, output_vcf, original_record):

    stop = original_record.stop
    start = original_record.start
    contig = original_record.contig
    REF = ref_fa_gz.fetch(contig, start, stop)
    
    def inverted(REF):

        reverse = REF[::-1]
        complement = {"A" : "T", "T": "A", "C" : "G", "G" : "C"}
        ALT = "".join(complement.get(base, base) for base in reverse)
        return ALT

    ALT = inverted(REF)
    new_record(output_vcf, original_record, start, stop, REF, ALT)


def add_insertion_record(ref_fa_gz, output_vcf, original_record):

    seq = str(original_record.info[_Annotations.SEQ.source_field_id])

    REF = original_record.alleles[0]
    ALT = REF + seq.upper()
    start = original_record.start
    stop = original_record.stop

    new_record(output_vcf, original_record, start, stop, REF, ALT)
    
def add_deletion_record(ref_fa_gz, output_vcf, original_record):
    
    svlen = int(original_record.info[_Annotations.SVLEN.source_field_id][0])

    start = original_record.start - 1
    stop = original_record.stop
    REF = ref_fa_gz.fetch(original_record.contig, start, original_record.stop)
    ALT = REF[0]

    new_record(output_vcf, original_record, start, stop, REF, ALT)

dict_of_get_alleles_functions = {
    'DUP': add_duplication_record,
    'INS': add_insertion_record,
    'INV': add_inversion_record,
    'DEL': add_deletion_record
}

def generate_fixed_vcf(ref_fa_gz_path, dbvar_vcf_gz_path, output_vcf_path):
    ref_fa_gz = FastaFile(ref_fa_gz_path)
    dbvar_original_vcf = VariantFile(dbvar_vcf_gz_path, 'r')
    output_vcf = VariantFile(output_vcf_path, 'w')
        
    # Add fileDate and reference fields
    output_vcf.header.add_line("##fileDate=" + datetime.date.today().strftime("%d-%m-%Y"))
    output_vcf.header.add_line("##reference=grch38")
    output_vcf.header.add_line("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">")
    output_vcf.header.add_sample('synthetic_sample')

    # Add INFO fields
    version = [record.value for record in output_vcf.header.records if record.key == "fileDate"][0]
    for annotation in _Annotations.AnnotationsList:
        annotation.info_version = version
        output_vcf.header.add_line(annotation.get_header_info_line())

    # Add contig fields
    for contig in ['chr{}'.format(i) for i in list(range(1, 23)) + ['X', 'Y', 'M']]:
        output_vcf.header.add_line("##contig=<ID={}>".format(contig))

    for original_record in dbvar_original_vcf.fetch():
        svtype = str(original_record.info[_Annotations.SVTYPE.source_field_id])
        if svtype in ['DEL', 'DUP', 'INS', 'INV']:
            dict_of_get_alleles_functions[svtype](ref_fa_gz, output_vcf, original_record)
            

    output_vcf.close()

def main():
    
    generate_fixed_vcf(arguments.ref_fa_gz_path, arguments.dbvar_vcf_gz_path, arguments.output_vcf_gz_path)
       
    
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Convert vcf with Structural variants in format VEP to normal vcf format.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-r', '--ref_fa_gz_path', help='path to fasta file with reference genome')
    parser.add_argument('-s', '--dbvar_vcf_gz_path', help='Path to vcf file in vep format to convert')
    parser.add_argument('-o', '--output_vcf_gz_path', help='Path to output vcf file in "indel" style, converted from dbvar_vcf_gz_path')
    arguments = parser.parse_args()
    main()
