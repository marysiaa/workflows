#!/usr/bin/env bash

set -ex

: "${RESOURCES_DIR:=/data/public/intelliseqngs/workflows/resources}"

docker run -u "$(id -u):$(id -g)" --rm -it -v $PWD/../..:/intelliseqtools -w /intelliseqtools/tests/nf2 -v $RESOURCES_DIR:/resources intelliseqngs/pysam:0.0.6 \
    python3 /intelliseqtools/main.py \
    -r /resources/reference-genomes/broad-institute-hg38/chromosome-wise/chr22/chr22.Homo_sapiens_assembly38.fa.gz \
    -s small.vcf.gz \
    -o output.vcf.gz

pwd

if [ -f expected.vcf.gz ]; then
    diff <(gzcat output.vcf.gz) <(gzcat expected.vcf.gz)
else
    cp output.vcf.gz expected.vcf.gz
fi
