#!/bin/bash

set -ex

if [ -z "$A" ]; then
  exec docker run -e A=1 --rm -it -u "$(id -u):$(id -g)" -e HOME=$HOME -v $HOME:$HOME -w $PWD intelliseqngs/gatk-4.1.7.0:1.0.4 /bin/bash "$0" $@
fi

: "${dbvar_vcf_gz:="$HOME/resources/dbvar/GRCh38.variant_call.all.vcf.gz"}"
variant_id=$1
chr=$2
echo $dbvar_vcf_gz
echo $variant_id
zcat "$dbvar_vcf_gz" | awk -v variant_id="$variant_id" "$(echo '
    /^##/ { print; next }
    /^#CHROM/ { print "##contig=<ID=chr${chr}>\n" $0; next }
    $3 == variant_id { print "chr" $0; exit }
' | sed "s/\${chr}/${chr}/")" | bgzip > $PWD/vep-${variant_id}.vcf.gz
