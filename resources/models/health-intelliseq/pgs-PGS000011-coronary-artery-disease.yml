descriptions:
  about_the_test:
    genes: [PSRC1, CXCL12, SHE, PCSK9, PLG, COL4A2, ABO, RAI1, LDLR, LIPA, UBE2Z,
      LPA, ATXN2, WDR12, GUCY1B1, AIDA, SRR, FURIN, KCNE2, JCAD, CNNM2, BCAP29, SLC22A4,
      PDGFD, MORF4L1, ABCG8, ZC3HC1, PHACTR1, ESYT3, FLT1, PLPP3, CDKN2B, KCNK5, CYP46A1,
      TWIST1, EDNRA, TDRD15, C12orf43, MAT2A, ZEB2]
    text1: Our test is based on the results of several GWAS studies (see references)
      compiled into the model deposited in Polygenic Score (PGS) Catalog (<a href="https://www.pgscatalog.org/score/PGS000011/">https://www.pgscatalog.org/score/PGS000011/</a>).
      The model includes 46 variants in 40 genes.
    text2: The heritability of CAD has been estimated between 40% and 60%, which means
      that genetic underpinnings are considered equivalent to that of environmental
      factors (like diet, excercises, and level of stress). Heritable effects are
      most manifested in younger individuals so the genetic influence is the greatest
      for early-onset CAD events (i.e. < 40 years).
  family_description: ['You are invited to inform your family members about your results.
      This knowledge may aid them in determining whether or not genetic testing is
      appropriate for them. Keep in mind, though, that genetic testing is a personal
      choice.', 'As with anything genetic, diseases can run in families. If you have
      a close relative (a parent, a sibling) with a heart disease, speak to your practitioner
      about the family history. If you have blood tests to check your cholesterol
      and glucose levels, information about your family history will help with understanding
      your results.']
  id: cad
  model_description: ['Coronary artery disease occurs when plaque (atherosclerosis)
      builds up in the arteries of the heart, reducing blood flow to the heart muscle.
      It is the most prevalent cardiovascular disorder. The artery''s lining hardens,
      stiffens, and collects calcium, fatty triglycerides, and aberrant inflammatory
      cells to create a plaque with atherosclerosis. Calcium phosphate (hydroxyapatite)
      deposits in the muscle layer of blood vessels appear to play a key role in hardening
      arteries and initiating coronary arteriosclerosis in the early stages.', 'Coronary
      artery disease may lead to ischemia (cell starvation due to a lack of oxygen)
      of the heart''s muscle cells. It occurs when blood flow to the heart is restricted.
      Temporary ischemia can lead to the onset of a ventricular arrhythmia, which
      can progress to ventricular fibrillation, a severe cardiac rhythm that often
      results in death. ', 'A myocardial infarction occurs when the heart''s muscle
      cells die due to a lack of oxygen (commonly referred to as a heart attack).
      It causes damage to the heart muscle, death, and scarring, with no regeneration
      of heart muscle cells.']
  more_about_the_disease:
  - {text: 'High blood pressure, smoking, diabetes, lack of exercise, obesity, high
      blood cholesterol, poor nutrition, depression, and excessive alcohol use are
      all risk factors.', title: Causes}
  - {text: 'Chest pain or discomfort is a typical symptom that might spread to the
      shoulder, arm, back, neck, or jaw. It may feel like heartburn at times. Symptoms
      are usually triggered by exertion or mental stress, linger for a few minutes,
      and improve with rest. Shortness of breath is also a possibility, and there
      are occasions when no symptoms are present. A heart attack is frequently the
      first symptom. Heart failure or an irregular heartbeat are two further problems.',
    title: Symptoms}
  - text: 'The nature of the symptoms plays a big role in determining whether or not
      you have coronary disease. The electrocardiogram (ECG/EKG) is the first test.
      A chest X-ray and blood testing may be necessary. There are also the following
      tests:'
    title: Screening
    unordered_list: [Exercise ECG - Stress test, 'Exercise radioisotope test (nuclear
        stress test, myocardial scintigraphy)', Echocardiography (including stress
        echocardiography), Coronary angiography, Intravascular ultrasound, Magnetic
        resonance imaging (MRI)]
  name: Coronary artery disease
  other_factors:
  - factors:
    - {description: 'High cholesterol levels in the blood (specifically, serum LDL
        concentrations) can increase the risk of coronary artery disease. HDL (high
        density lipoprotein) level has been shown to reduce the risk of coronary artery
        disease.', name: Cholesterol}
    - {description: High blood triglycerides can increase the risk of CAD., name: Triglycerides}
    - {description: 'High levels of lipoprotein(a), a molecule generated when LDL
        cholesterol mixes with a protein called apolipoprotein(a), can increase the
        risk of CAD.', name: Lipoprotein}
    icon: icon-cholesterol
    id: Cholesterol components
  - factors:
    - {description: Endometriosis in women under the age of 40 is a risk factor for
        CAD., name: Endometriosis}
    icon: icon-sex
    id: endometriosis
  - factors:
    - {description: Depression and hostility appear to be risk factors., name: Mental
        condition}
    icon: icon-lifestyle
    id: depression
  - factors:
    - {description: The presence of adult diseases such as coronary artery (ischemic
        heart) disease showed a graded correlation with the number of adverse childhood
        experiences., name: Adverse childhood experiences}
    icon: icon-family
    id: childhood
  - factors:
    - {description: The risk of coronary artery disease is increased when hemoglobin
        levels are low., name: Hemoglobin}
    - {description: High levels of fibrinogen and coagulation factor VII have been
        linked to a higher risk of CAD., name: Hemostatic factors}
    icon: icon-dna
    id: Blood components
  recommendations:
    average:
      description: 'Even though you are not genetically predisposed to CAD, there
        are other factors that may influence your risk for this disease. Up to 90%
        of cardiovascular disease may be prevented if recognized risk factors are
        avoided. To maintain health, do the following:'
      set:
      - {description: 'Eat a nutritious diet - rich in fruits and vegetables, high
          in fiber (like oatmeal, whole grains, and beans), and low in saturated and
          trans-fat (avoid fried foods and processed meats). Helpful foods include
          fishes high in omega-3 (eg. salmon, mackerel, lake trout, herring), chicken
          or turkey, lentils, and peas.', icon: icon-diet, name: Diet}
      - {description: 'Be physically active - regular exercise can help you stay fit
          and healthy. It''s recommended that adults get 2.5 hours of moderate physical
          activity, like brisk walking or biking, every week. If you smoke, try to
          quit. If you have high blood pressure, talk to your practitioner how to
          address it.', icon: icon-lifestyle, name: Lifestyle}
    increased:
      description: 'Up to 90% of cardiovascular disease may be prevented if recognized
        risk factors are avoided. To reduce the risk, do the following:'
      set:
      - {description: 'Eat a nutritious diet - rich in fruits and vegetables, high
          in fiber (like oatmeal, whole grains, and beans), and low in saturated and
          trans-fat (avoid fried foods and processed meats). Helpful foods include
          fishes high in omega-3 (eg. salmon, mackerel, lake trout, herring), chicken
          or turkey, lentils, and peas.', icon: icon-diet, name: Diet}
      - {description: 'Be physically active - regular exercise can help you stay fit
          and healthy. It''s recommended that adults get 2.5 hours of moderate physical
          activity, like brisk walking or biking, every week. If you smoke, try to
          quit. If you have high blood pressure, talk to your practitioner how to
          address it.', icon: icon-lifestyle, name: Lifestyle}
    reduced:
      description: 'Even though you are not genetically predisposed to CAD, there
        are other factors that may influence your risk for this disease. Up to 90%
        of cardiovascular disease may be prevented if recognized risk factors are
        avoided. To maintain health, do the following:'
      set:
      - {description: 'Eat a nutritious diet - rich in fruits and vegetables, high
          in fiber (like oatmeal, whole grains, and beans), and low in saturated and
          trans-fat (avoid fried foods and processed meats). Helpful foods include
          fishes high in omega-3 (eg. salmon, mackerel, lake trout, herring), chicken
          or turkey, lentils, and peas.', icon: icon-diet, name: Diet}
      - {description: 'Be physically active - regular exercise can help you stay fit
          and healthy. It''s recommended that adults get 2.5 hours of moderate physical
          activity, like brisk walking or biking, every week. If you smoke, try to
          quit. If you have high blood pressure, talk to your practitioner how to
          address it.', icon: icon-lifestyle, name: Lifestyle}
  references:
    data_analysis_articles: ['Tada H, Melander O, Louie JZ, et al. Risk prediction
        by genetic risk scores for coronary heart disease is independent of self-reported
        family history. <i>Eur Heart J</i>. 2016;37(6):561-567. <a href="https://doi.org/10.1093/eurheartj/ehv462">doi:10.1093/eurheartj/ehv462</a>',
      'Roberts R, Stewart AF. Genetics of coronary artery disease in the 21st century.
        <i>Clin Cardiol</i>. 2012;35(9):536-540. <a href="https://doi.org/10.1002/clc.22002">doi:10.1002/clc.22002</a>',
      'Yeboah J, McClelland RL, Polonsky TS, et al. Comparison of novel risk markers
        for improvement in cardiovascular risk assessment in intermediate-risk individuals.
        <i>JAMA</i>. 2012;308(8):788-795. <a href="https://doi.org/10.1001/jama.2012.9624">doi:10.1001/jama.2012.9624</a>',
      'Gui L, Wu F, Han X, et al. A multilocus genetic risk score predicts coronary
        heart disease risk in a Chinese Han population. <i>Atherosclerosis</i>. 2014;237(2):480-485.
        <a href="https://doi.org/10.1016/j.atherosclerosis.2014.09.032">doi:10.1016/j.atherosclerosis.2014.09.032</a>',
      'Yiannakouris N, Katsoulis M, Trichopoulou A, Ordovas JM, Trichopoulos D. Additive
        influence of genetic predisposition and conventional risk factors in the incidence
        of coronary heart disease: a population-based study in Greece. <i>BMJ Open</i>.
        2014;4(2):e004387. Published 2014 Feb 5. <a href="https://doi.org/10.1136/bmjopen-2013-004387">doi:10.1136/bmjopen-2013-004387</a>',
      "Berglund G, Elmst\xE4hl S, Janzon L, Larsson SA. The Malmo Diet and Cancer\
        \ Study. Design and feasibility. <i>J Intern Med</i>. 1993;233(1):45-51. <a\
        \ href=\"https://doi.org/10.1111/j.1365-2796.1993.tb00647.x\">doi:10.1111/j.1365-2796.1993.tb00647.x</a>",
      'Polygenic Score (PGS) Catalog <a href="https://www.pgscatalog.org/score/PGS000011/">https://www.pgscatalog.org/score/PGS000011/</a>']
    recommendations_articles: []
  results_description:
    average:
      descriptions: ['Based only on the genetic factors that we''ve tested, you have
          an average risk of coronary artery disease (CAD) so your chances of developing
          the specific phenotype are close to the chances of an average person in
          the population.', 'According to scientific studies, a mix of genetic and
          environmental variables contribute to the total risk of coronary artery
          disease. To stay healthy, you can follow the advice below.']
      risk_text: Average risk
      risk_value: medium
    increased:
      descriptions: ['Based on the genetic factors tested, you may be predisposed
          to the coronary artery disease (CAD). Your odds of acquiring this disease
          exceeds values obtained by 90% of the population so you are in a high risk
          group.', 'However, studies have revealed that a mix of hereditary and environmental
          variables contribute to the overall risk of coronary artery disease. Follow
          the guidelines below to reduce your chances of developing this disease.']
      risk_text: High risk
      risk_value: high
    reduced:
      descriptions: ['Based on the genetic factors that we''ve tested, you are not
          predisposed to the coronary artery disease (CAD). It means that your odds
          of acquiring this disease are very poor.', 'However, scientific studies
          have revealed that a mix of genetic and environmental variables contribute
          to the overall risk of the coronary artery disease. To stay healthy, follow
          the advice below.']
      risk_text: Low risk
      risk_value: low
  trait_heritability: 0.5
score_model:
  categories:
    average: {from: -0.6811952719292345, to: 0.5898961814996471}
    increased: {from: 0.5898961814996471, to: 2.8477093973008283}
    reduced: {from: -1.3758792373633715, to: -0.6811952719292345}
  variants:
    rs10455872: {effect_allele: G, effect_size: 0.371563556432483, gnomadid: '6:160589086_A_G',
      ref: A, symbol: LPA}
    rs10947789: {effect_allele: C, effect_size: -0.058268908123975824, gnomadid: '6:39207146_T_C',
      ref: T, symbol: KCNK5}
    rs10953541: {effect_allele: T, effect_size: -0.0769610411361284, gnomadid: '7:107604100_C_T',
      ref: C, symbol: BCAP29}
    rs11206510: {effect_allele: C, effect_size: -0.0769610411361284, gnomadid: '1:55030366_T_C',
      ref: T, symbol: PCSK9}
    rs11226029: {effect_allele: A, effect_size: -0.06765864847381486, gnomadid: '11:103822899_G_A',
      ref: G, symbol: PDGFD}
    rs1122608: {effect_allele: T, effect_size: -0.09531017980432493, gnomadid: '19:11052925_G_T',
      ref: G, symbol: LDLR}
    rs11556924: {effect_allele: T, effect_size: -0.08617769624105241, gnomadid: '7:130023656_C_T',
      ref: C, symbol: ZC3HC1}
    rs11984041: {effect_allele: T, effect_size: 0.06765864847381486, gnomadid: '7:18992312_C_T',
      ref: C, symbol: TWIST1}
    rs12413409: {effect_allele: A, effect_size: -0.11332868530700327, gnomadid: '10:102959339_G_A',
      ref: G, symbol: CNNM2}
    rs12526453: {effect_allele: G, effect_size: 0.09531017980432493, gnomadid: '6:12927312_C_G',
      ref: C, symbol: PHACTR1}
    rs12936587: {effect_allele: A, effect_size: -0.058268908123975824, gnomadid: '17:17640408_G_A',
      ref: G, symbol: RAI1}
    rs1412444: {effect_allele: T, effect_size: 0.058268908123975824, gnomadid: '10:89243170_C_T',
      ref: C, symbol: LIPA}
    rs17114036: {effect_allele: G, effect_size: -0.10436001532424286, gnomadid: '1:56497149_A_G',
      ref: A, symbol: PLPP3}
    rs1746048: {effect_allele: T, effect_size: -0.06765864847381486, gnomadid: '10:44280376_C_T',
      ref: C, symbol: CXCL12}
    rs17465637: {effect_allele: C, effect_size: 0.131028262406404, gnomadid: '1:222650187_A_C',
      ref: A, symbol: AIDA}
    rs17514846: {effect_allele: A, effect_size: 0.04879016416943205, gnomadid: '15:90873320_C_A',
      ref: C, symbol: FURIN}
    rs1878406: {effect_allele: T, effect_size: 0.058268908123975824, gnomadid: '4:147472512_C_T',
      ref: C, symbol: EDNRA}
    rs2028900: {effect_allele: T, effect_size: 0.04879016416943205, gnomadid: '2:85540612_C_T',
      ref: C, symbol: MAT2A}
    rs2047009: {effect_allele: G, effect_size: 0.04879016416943205, gnomadid: '10:44044465_T_G',
      ref: T, symbol: CXCL12}
    rs2048327: {effect_allele: C, effect_size: 0.058268908123975824, gnomadid: '6:160442500_T_C',
      ref: T, symbol: LPA}
    rs216172: {effect_allele: C, effect_size: 0.06765864847381486, gnomadid: '17:2223210_G_C',
      ref: G, symbol: SRR}
    rs2252641: {effect_allele: C, effect_size: 0.03922071315328133, gnomadid: '2:145043894_T_C',
      ref: T, symbol: ZEB2}
    rs2259816: {effect_allele: T, effect_size: 0.0769610411361284, gnomadid: '12:120997784_G_T',
      ref: G, symbol: C12orf43}
    rs2487928: {effect_allele: A, effect_size: 0.058268908123975824, gnomadid: '10:30034963_G_A',
      ref: G, symbol: JCAD}
    rs273909: {effect_allele: G, effect_size: 0.08617769624105241, gnomadid: '5:132331660_A_G',
      ref: A, symbol: SLC22A4}
    rs2895811: {effect_allele: C, effect_size: 0.058268908123975824, gnomadid: '14:99667605_T_C',
      ref: T, symbol: CYP46A1}
    rs318090: {effect_allele: A, effect_size: 0.058268908123975824, gnomadid: '17:48914390_G_A',
      ref: G, symbol: UBE2Z}
    rs3184504: {effect_allele: C, effect_size: -0.06765864847381486, gnomadid: '12:111446804_T_C',
      ref: T, symbol: ATXN2}
    rs3217992: {effect_allele: T, effect_size: 0.14842000511827322, gnomadid: '9:22003224_C_T',
      ref: C, symbol: CDKN2B}
    rs3798220: {effect_allele: C, effect_size: 0.412109650826833, gnomadid: '6:160540105_T_C',
      ref: T, symbol: LPA}
    rs3825807: {effect_allele: G, effect_size: -0.0769610411361284, gnomadid: '15:78796769_A_G',
      ref: A, symbol: MORF4L1}
    rs4252120: {effect_allele: C, effect_size: -0.058268908123975824, gnomadid: '6:160722576_T_C',
      ref: T, symbol: PLG}
    rs4299376: {effect_allele: T, effect_size: -0.058268908123975824, gnomadid: '2:43845437_G_T',
      ref: G, symbol: ABCG8}
    rs4773144: {effect_allele: G, effect_size: 0.06765864847381486, gnomadid: '13:110308365_A_G',
      ref: A, symbol: COL4A2}
    rs4845625: {effect_allele: C, effect_size: -0.03922071315328133, gnomadid: '1:154449591_T_C',
      ref: T, symbol: SHE}
    rs4977574: {effect_allele: G, effect_size: 0.25464221837358075, gnomadid: '9:22098575_A_G',
      ref: A, symbol: CDKN2B}
    rs515135: {effect_allele: C, effect_size: 0.0769610411361284, gnomadid: '2:21063185_T_C',
      ref: T, symbol: TDRD15}
    rs579459: {effect_allele: T, effect_size: -0.06765864847381486, gnomadid: '9:133278724_C_T',
      ref: C, symbol: ABO}
    rs646776: {effect_allele: T, effect_size: 0.10436001532424286, gnomadid: '1:109275908_C_T',
      ref: C, symbol: PSRC1}
    rs6725887: {effect_allele: C, effect_size: 0.11332868530700327, gnomadid: '2:202881162_T_C',
      ref: T, symbol: WDR12}
    rs7173743: {effect_allele: C, effect_size: -0.06765864847381486, gnomadid: '15:78849442_T_C',
      ref: T, symbol: MORF4L1}
    rs7692387: {effect_allele: A, effect_size: -0.058268908123975824, gnomadid: '4:155714157_G_A',
      ref: G, symbol: GUCY1B1}
    rs9319428: {effect_allele: A, effect_size: 0.04879016416943205, gnomadid: '13:28399484_G_A',
      ref: G, symbol: FLT1}
    rs9515203: {effect_allele: C, effect_size: -0.0769610411361284, gnomadid: '13:110397276_T_C',
      ref: T, symbol: COL4A2}
    rs9818870: {effect_allele: T, effect_size: 0.06765864847381486, gnomadid: '3:138403280_C_T',
      ref: C, symbol: ESYT3}
    rs9982601: {effect_allele: T, effect_size: 0.12221763272424911, gnomadid: '21:34226827_C_T',
      ref: C, symbol: KCNE2}
