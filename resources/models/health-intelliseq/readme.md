## List of models
 * [Coronary artery disease (cad)](#coronary-artery-disease-cad)

## Description of models

### Coronary artery disease (cad)
 - source: The Polygenic Score (PGS) Catalog
 - code: PGS000011
 - prepared with `marpiech/polygenictk:2.0.45` 
and [health-google-spreadsheet-to-yaml.py](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/health-google-spreadsheet-to-yaml.py) (version `0.2.4`)
 - description version: 04-01-2022
 
 ```bash
 OUTPUT_DIRECTORY="/tmp/cad-output"
 ## create raw model
 docker run -v "$OUTPUT_DIRECTORY":/output marpiech/polygenictk:2.0.45 pgstk model-pgscat --code PGS000011 --origin-genome-build GRCh38 --output-directory /output
 cp /tmp/cad-output/pgscat-coronary_artery_disease-PGS000011-EUR.yml resources/models/health-intelliseq/raw-models/
 
 ## add description
 python3 health-google-spreadsheet-to-yaml.py \
   --input-credentials-json 'my_key.json' \
   --input-google-spreadsheet-key "1nZL-I0BxsTpsam6VRnGL40o2Ax-IwY-UxVS9FLqYQ4o" \
   --input-model-yaml resources/models/health-intelliseq/raw-models/pgscat-coronary_artery_disease-PGS000011-EUR.yml \
   --output-model-yaml resources/models/health-intelliseq/pgs-PGS000011-coronary-artery-disease.yml
 ```