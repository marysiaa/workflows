## List of models
 * [Bone mineral density (bmd)](#bone-mineral-density-bmd)
 * [Male pattern baldness (mpb)](#male-pattern-baldness-mpb)
 * [LDL direct](#ldl-direct)
 * [HDL cholesterol](#hdl-cholesterol)
 * [Body mass index (bmi)](#body-mass-index-bmi)

## Description of models

### Bone mineral density (bmd)  
 - source: Global Biobank Engine
 - code: INI78
 - prepared with `marpiech/polygenicmaker:2.0.26` 
and [health-google-spreadsheet-to-yaml.py](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/health-google-spreadsheet-to-yaml.py) (version `0.2.4`)
 - description version: 04-01-2022
 
 ```bash
 OUTPUT_DIRECTORY="/tmp/bmd-output"
 ## create raw model
 docker run -v "$OUTPUT_DIRECTORY":/output marpiech/polygenicmaker:2.0.26 polygenicmaker gbe-model --code INI78
 cp /tmp/bmd-output/INI78.yml resources/models/wellness-intelliseq/raw-models/

 ## add description
 python3 src/main/scripts/tools/health-google-spreadsheet-to-yaml.py \
   --input-credentials-json 'my_key.json' \
   --input-google-spreadsheet-key "1HwVga2tY5TAgj6hM9dvK1DGFMyPgXTUmg0pes__hLEs" \
   --input-model-yaml resources/models/wellness-intelliseq/raw-models/INI78.yml \
   --output-model-yaml resources/models/wellness-intelliseq/gbe-INI78-bone-density.yml
 ```

### Male pattern baldness (mpb)  
 - source: UK Biobank
 - code: 2395 Pattern 4
 - prepared with `marpiech/polygenictk:2.0.40` 
and [health-google-spreadsheet-to-yaml.py](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/health-google-spreadsheet-to-yaml.py) (version `0.2.4`)
 - description version: 04-01-2022
 
 ```bash
 OUTPUT_DIRECTORY="/tmp/mpb-output"
 ## create raw model
 docker run -v "$OUTPUT_DIRECTORY":/output marpiech/polygenictk:2.0.40 pgstk model-biobankuk --code 2395 --sex both_sexes --coding 4 --output-directory /output
 cp /tmp/mpb-output/biobankuk-hair_balding_pattern-2395-both_sexes-4-EUR-1e-08.yml resources/models/wellness-intelliseq/raw-models/
 
 ## add description
 python3 health-google-spreadsheet-to-yaml.py \
   --input-credentials-json 'my_key.json' \
   --input-google-spreadsheet-key "1bZd1_8dBmDmwQePkgc5YSjDwEXARhav0WG8zsG2aztw" \
   --input-model-yaml resources/models/wellness-intelliseq/raw-models/biobankuk-hair_balding_pattern-2395-both_sexes-4-EUR-1e-08.yml \
   --output-model-yaml resources/models/wellness-intelliseq/ukb-2395-pattern-balding.yml
 ```

### LDL direct 
 - source: Global Biobank Engine
 - code: INI30780
 - prepared with `marpiech/polygenictk:2.0.45` 
and [health-google-spreadsheet-to-yaml.py](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/health-google-spreadsheet-to-yaml.py) (version `0.2.4`)
 - description version: 04-01-2022
 
 ```bash
 OUTPUT_DIRECTORY="/tmp/ldl-output"
 ## create raw model
 docker run -v "$OUTPUT_DIRECTORY":/output marpiech/polygenictk:2.0.45 pgstk model-biobankuk --code 30780 --sex both_sexes --output-directory /output
 cp /tmp/ldl-output/biobankuk-ldl_direct-30780-both_sexes--EUR-1e-08.yml resources/models/wellness-intelliseq/raw-models/INI30780.yml

 ## add description
 python3 src/main/scripts/tools/health-google-spreadsheet-to-yaml.py \
   --input-credentials-json 'my_key.json' \
   --input-google-spreadsheet-key "" \
   --input-model-yaml resources/models/wellness-intelliseq/raw-models/INI30780.yml \
   --output-model-yaml resources/models/wellness-intelliseq/gbe-INI30780-ldl.yml
 ```

### HDL cholesterol
 - source: Global Biobank Engine
 - code: INI30760
 - prepared with `marpiech/polygenictk:2.0.45` 
and [health-google-spreadsheet-to-yaml.py](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/health-google-spreadsheet-to-yaml.py) (version `0.2.4`)
 - description version: 04-01-2022
 
 ```bash
 OUTPUT_DIRECTORY="/tmp/hdl-output"
 ## create raw model
 docker run -v "$OUTPUT_DIRECTORY":/output marpiech/polygenictk:2.0.45 pgstk model-biobankuk --code 30760 --sex both_sexes --output-directory /output
 cp /tmp/hdl-output/biobankuk-hdl_cholesterol-30760-both_sexes--EUR-1e-08.yml resources/models/wellness-intelliseq/raw-models/INI30760.yml

 ## add description
 python3 src/main/scripts/tools/health-google-spreadsheet-to-yaml.py \
   --input-credentials-json 'my_key.json' \
   --input-google-spreadsheet-key "" \
   --input-model-yaml resources/models/wellness-intelliseq/raw-models/INI30760.yml \
   --output-model-yaml resources/models/wellness-intelliseq/gbe-INI30760-hdl.yml
 ```

### Body mass index (bmi)
 - source: Global Biobank Engine
 - code: INI21001
 - prepared with `marpiech/polygenictk:2.0.45` 
and [health-google-spreadsheet-to-yaml.py](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/health-google-spreadsheet-to-yaml.py) (version `0.2.4`)
 - description version: 04-01-2022
 
 ```bash
 OUTPUT_DIRECTORY="/tmp/bmi-output"
 ## create raw model
 docker run -v "$OUTPUT_DIRECTORY":/output marpiech/polygenictk:2.0.45 pgstk model-biobankuk --code 21001 --sex both_sexes --output-directory /output
 cp /tmp/bmi-output/biobankuk-body_mass_index_bmi_-21001-both_sexes--EUR-1e-08.yml resources/models/wellness-intelliseq/raw-models/INI21001.yml

 ## add description
 python3 src/main/scripts/tools/health-google-spreadsheet-to-yaml.py \
   --input-credentials-json 'my_key.json' \
   --input-google-spreadsheet-key "" \
   --input-model-yaml resources/models/wellness-intelliseq/raw-models/INI21001.yml \
   --output-model-yaml resources/models/wellness-intelliseq/gbe-INI21001-bmi.yml
 ```