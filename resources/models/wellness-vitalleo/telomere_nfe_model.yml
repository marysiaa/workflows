descriptions:
  about_the_test:
    genes:
    - TSPYL6
    - MYNN
    - TERT
    - AC003973.1
    - ZBTB46-AS1
    - STN1
    - MIR4454
    text1: 'Telomere length is highly heritable, and it is estimated that about 44-80%
      of differences observed among people at the same age has a genetic origin [1].
      Our test is based on results of the genome wide association studies (GWAS) [1],
      which included over 47,000 individuals of European ancestry. The study identified
      7 variants significantly associated with the telomere length. Together these
      SNPs explain about 1.2% of between people variance in telomere length [2].

      Our analyses use genetic data (genotypes) for all those SNPs to calculate the
      polygenic score.'
    text2: Your results are based on genetic studies published in peer-revieved scientific
      and medical articles. Sometimes, the effects of many different genetic factors
      are combined into one risk estimate.
    text3: Your result is a risk based on 7 genetic variants, specifically called
      SNPs, found in genes associated with telomere length. Overall, this explains
      about 1.2% of the genetic variation in telomere length between people, based
      on current scientific knowledge.
  environmentScore: 56
  geneticScore: 1
  genotypeScore: 44
  id: telomere_nfe_model
  image_top: telomere.jpg
  model_description:
  - Telomeres are areas of DNA found at the ends of chromosomes (structure that contain
    genes and DNA). Telomeres give stability to chromosomes and protect them from
    damage. When most cells in the body divide, which happens often, the telomeres
    shorten a little. Over time, the cell's telomeres get too short and that cell
    may not be able to divide anymore or may die. This can naturally happen with age
    or for other reasons. Having a lower than average telomere length (when compared
    to others of the same age and biological sex) is a potential risk factor for health
    issues related to aging, like some forms of heart disease and cancer. However,
    the exact impact of telomere length on these health risks is still unclear. Several
    things impact a person's telomere length, including genetics.
  name: Telomere Length
  other_factors:
  - factors: []
    icon: icon-lifestyle
    id: Lifestyle
  - factors:
    - description: Having diabetes or a high blood sugar level contributes to telomere
        length.
      name: Other Health Issues
    icon: icon-health
    id: Other Health Issues
  - factors:
    - description: Sometimes, genetic risk factors can cause multiple people in the
        same family to have short telomere lengths. These types of risk factors, which
        are usually due to rare genetic variants, are not included in this test. If
        you have a family history of a genetic condition that causes people to have
        a short telomere length, talk to your Practitioner or a genetic counselor.
      name: Family History
    icon: icon-dna
    id: Family History
  recommendations:
    average:
      description: There are things you can do to try to maintain telomere length
        and stay healthy. Certain blood tests can also check your risk factors for
        heart disease or other health issues. Talk to your practitioner about what
        is right for you.
      set:
      - description: Other factors besides genetics may impact your risk for a shorter
          telomere length. Consider talking to your practitioner about ordering tests
          to check your risk factors of related health issues, and possible next steps
          that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: High emotional stress, low physical activity, low amounts of
          sleep, and being overweight are risk factors for a shorter telomere length.
          Try to find ways to address these if they apply to you, since it is good
          for overall wellness. Talking to friends, family, a mental health specialist
          or meditating can help to manage stress. Adults should get 2.5 hours of
          moderate physical activity, like brisk walking or biking, every week. Not
          only can this lower your risk of heart disease, but it can also help you
          maintain a healthy weight and get better sleep.
        icon: icon-lifestyle
        name: Lifestyle
      - description: Short telomere length can run in families, and these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with a genetic condition that causes people to have a short telomere
          length, speak to your practitioner about your family history. If you have
          blood tests to look for related health issues, information about your family
          history will help with understanding your blood test results.
        icon: icon-dna
        name: Family History
    increased:
      description: There are things you can do to try to maintain telomere length
        and stay healthy. Certain blood tests can also check your risk factors for
        heart disease or other health issues. Talk to your practitioner about what
        is right for you.
      set:
      - description: Other factors besides genetics may impact your risk for a shorter
          telomere length. Consider talking to your practitioner about ordering tests
          to check your risk factors of related health issues, and possible next steps
          that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: High emotional stress, low physical activity, low amounts of
          sleep, and being overweight are risk factors for a shorter telomere length.
          Try to find ways to address these if they apply to you, since it is good
          for overall wellness. Talking to friends, family, a mental health specialist
          or meditating can help to manage stress. Adults should get 2.5 hours of
          moderate physical activity, like brisk walking or biking, every week. Not
          only can this lower your risk of heart disease, but it can also help you
          maintain a healthy weight and get better sleep.
        icon: icon-lifestyle
        name: Lifestyle
      - description: Short telomere length can run in families, and these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with a genetic condition that causes people to have a short telomere
          length, speak to your practitioner about your family history. If you have
          blood tests to look for related health issues, information about your family
          history will help with understanding your blood test results.
        icon: icon-dna
        name: Family History
    reduced:
      description: There are things you can do to try to maintain telomere length
        and stay healthy. Certain blood tests can also check your risk factors for
        heart disease or other health issues. Talk to your practitioner about what
        is right for you.
      set:
      - description: Your practitioner may be able to order tests to check your risk
          factors for heart disease or other health issues related to a shorter telomere
          length. Talk to them about this and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: High emotional stress, low physical activity, low amounts of
          sleep, and being overweight are risk factors for a shorter telomere length.
          Try to find ways to address these if they apply to you. Talking to friends,
          family, a mental health specialist or meditating can help to manage stress.
          Adults should get 2.5 hours of moderate physical activity, like brisk walking
          or biking, every week. Not only can this lower your risk of heart disease,
          but it can also help you maintain a healthy weight and get better sleep.
          Talk to your practitioner if you need resources or support with these.
        icon: icon-lifestyle
        name: Lifestyle
      - description: If you have a high blood sugar level or diabetes you may have
          a risk for shorter telomere length. Speak to your practitioner about how
          this factors into things, if your telomere length is found to be low.
        icon: icon-health
        name: Other Health Issues
      - description: Short telomere length can run in families, and these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with a genetic condition that causes people to have a short telomere
          length, speak to your practitioner about your family history. If you have
          blood tests to look for related health issues, information about your family
          history will help with understanding your blood test results.
        icon: icon-dna
        name: Family History
  references:
    data_analysis_articles:
    - Codd V, Nelson CP, Albrecht E, et al. Identification of seven loci affecting
      mean telomere length and their association with disease. <i>Nat Genet</i>. 2013;45(4):422-427e4272.
      <a href="https://doi.org/10.1038/ng.2528">doi:10.1038/ng.2528</a>
    recommendations_articles:
    - '"Tell Me More about Telomeres" from Centers for Disease Control and Prevention
      <a href="https://blogs.cdc.gov/genomics/2011/06/09/tell-me-more-about-telomeres/">https://blogs.cdc.gov/genomics/2011/06/09/tell-me-more-about-telomeres/</a>
      (Accessed June 26, 2019)'
    - Codd V, et al. Nature Genetics. 2014 <a href="https://www.ncbi.nlm.nih.gov/pubmed/23535734">https://www.ncbi.nlm.nih.gov/pubmed/23535734</a>
      (Accessed June 26, 2019)
    - "Wang J, et al. Journal of International Medical Research. 2016 <a href=\"https://www.ncbi.nlm.nih.gov/pubmed/28322101\"\
      >https://www.ncbi.nlm.nih.gov/pubmed/28322101</a> (Accessed July 1, 2019. \n\
      )"
    - Starkweather AR, et al. Nursing Research. 2014 <a href="https://www.ncbi.nlm.nih.gov/pubmed/24335912">https://www.ncbi.nlm.nih.gov/pubmed/24335912</a>
      (Accessed June 26, 2019)
  results_description:
    average:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have an average
        telomere length.
      - Depending on your telomere length and other factors, you may have an average
        risk for some forms of heart disease, cancer, or other health issues.
      risk_text: Average risk
      risk_value: medium
    increased:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a longer
        than average telomere length.
      - Depending on your telomere length and other factors, you may have a lower
        risk for some forms of heart disease, cancer, or other health issues.
      risk_text: Low risk
      risk_value: low
    reduced:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a shorter
        telomere length.
      - Depending on your telomere length and other factors, you may have a higher
        risk for some forms of heart disease, cancer, or other health issues. The
        exact risks related to telomere length are still unclear.
      risk_text: High risk
      risk_value: high
  trait_category: Physiology
  trait_explained: 0.012
  trait_heritability: 0.44
  trait_snp_heritability: ''
score_model:
  categories:
    average:
      from: -0.7001
      to: -0.4219
    reduced:
      to: -0.7001
    increased:
      from: -0.4219
  variants:
    rs10936599:
      effect_size: -0.097
      effect_allele: T
    rs11125529:
      effect_size: -0.056
      effect_allele: C
    rs2736100:
      effect_size: -0.078
      effect_allele: A
    rs755017:
      effect_size: -0.062
      effect_allele: A
    rs7675998:
      effect_size: -0.074
      effect_allele: A
    rs8105767:
      effect_size: -0.048
      effect_allele: A
    rs9420907:
      effect_size: -0.069
      effect_allele: A
