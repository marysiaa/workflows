descriptions:
  about_the_test:
    genes:
    - KRT8P51
    - SATB1-AS1
    - AC022101.1
    - CAV1
    - GNG11
    - SYT10
    - CNTN3
    - ACTG1P18
    - NDUFA11
    - RNA5SP94
    - RNU7-104P
    - POP4
    - SRRT
    - AP000593.1
    - MIR5584
    - AC004947.2
    - MIR7843
    - PAX2
    - AC009387.1
    - KNOP1P1
    - GDI2P2
    - SERINC2
    - AC087633.2
    - PRDM6
    text1: 'Heart rate after exercise is partially controlled by genetic factors and
      about 60% of differences observed among people has a genetic origin. Our test
      is based on results of the genome wide association studies (GWAS) [1], which
      included over 59,000 individuals of European ancestry (BioBank UK). The study
      reports 25 variants significantly associated with the after exercise heart hate
      [1].

      Our analyses use genetic data (genotypes) for all those SNPs to calculate the
      polygenic score.'
    text2: Your results are based on genetic studies published in peer-revieved scientific
      and medical articles. Sometimes, the effects of many different genetic factors
      are combined into one risk estimate.
    text3: Your result is a risk based on 25 genetic variants, specifically called
      SNPs, associated with heart rate recovery.
  environmentScore: 40
  geneticScore: NA
  genotypeScore: 60
  id: heart_rate_nfe_model
  image_top: heart_rate.jpg
  model_description:
  - Heart rate goes up during exercise, then naturally recovers and comes back down.
    This is how the heart muscle works and strengthens during exercise. If the heart
    is working well, heart rate quickly recovers to normal after exercise. For some
    people, their heart rate stays high for too long after exercise. This could be
    a sign of a heart poorly working, or possibly a sign of a heart problem or other
    health problems. A longer post-exercise heart rate can also happen in people who
    are less physically fit than others their age. Several things impact post-exercise
    heart rate, including genetics.
  name: Post-Exercise Heart Rate
  other_factors:
  - factors: []
    icon: icon-lifestyle
    id: Lifestyle
  - factors:
    - description: Some health issues, like high blood pressure or diabetes, can affect
        someone's heart rate. Certain medicines, like some for asthma, can have a
        similar effect.
      name: Other Health Issues
    icon: icon-health
    id: Other Health Issues
  - factors:
    - description: Sometimes, genetic risk factors can cause multiple people in the
        same family to have a longer post-exercise heart rate. These types of risk
        factors, which are usually due to rare genetic variants, are not included
        in this test. If you have a family history of a high post-exercise heart rate
        recovery time, talk to your Practitioner or a genetic counselor.
      name: Family History
    icon: icon-dna
    id: Family History
  recommendations:
    average:
      description: There are things you can do to try to prevent a higher post-exercise
        heart rate and stay healthy. Certain blood tests can also check your risk
        factors for some forms of heart disease. Talk to your practitioner about what
        is right for you.
      set:
      - description: Other factors besides genetics may impact your risk for a higher
          post-exercise heart rate. Consider talking to your practitioner about ordering
          blood tests to check your risk factors for heart disease or other health
          issues, and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: Being physically active is a good workout for your heart and
          keeps it strong, especially as your heart rate increases and decreases.
          Regular physical exercise can "condition" your heart and keep your post-exercise
          heart rate in the normal range. It's recommended that adults get 2.5 hours
          of moderate physical activity, like brisk walking or biking, every week.
          When exercise generates a high demand on the body's need for oxygen during
          the workout, it increases the body's need for oxygen after the workout.
          As a result, you can continue to burn fat and calories long after the workout
          is finished. To enhance these benefits, try incorporating High Intensity-Interval
          Training (HIIT) weight lifting exercises as part of a circuit.
        icon: icon-lifestyle
        name: Lifestyle
      - description: A high post-exercise heart rate can run in families, and these
          risk factors are not included in this test. If you have a close relative
          (parent, child, sibling) with a high post-exercise heart rate, speak to
          your practitioner about your family history. If you have blood tests to
          look for related health issues, information about your family history will
          help with understanding your blood test results.
        icon: icon-dna
        name: Family History
    increased:
      description: There are things you can do to try to prevent a higher post-exercise
        heart rate and stay healthy. Certain blood tests can also check your risk
        factors for some forms of heart disease. Talk to your practitioner about what
        is right for you.
      set:
      - description: Your practitioner may be able to order blood tests to check your
          risk factors for heart disease or other health issues. Talk to them about
          this and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: Being physically active is a good workout for your heart and
          keeps it strong, especially as your heart rate increases and decreases.
          Regular physical exercise can "condition" your heart and lower your post-exercise
          heart rate. It's recommended that adults get 2.5 hours of moderate physical
          activity, like brisk walking or biking, every week. Not only can this lower
          your risk of heart disease, but it can also help you maintain a healthy
          weight. When exercise generates a high demand on the body's need for oxygen
          during the workout, it increases the body's need for oxygen after the workout.
          As a result, you can continue to burn fat and calories long after the workout
          is finished. To enhance these benefits, try incorporating High Intensity-Interval
          Training (HIIT) weight lifting exercises as part of a circuit.
        icon: icon-lifestyle
        name: Lifestyle
      - description: If you have high blood pressure or diabetes, this can raise your
          heart rate. Some antibiotics, antidepressants and some medications for asthma,
          thyroid conditions, cough, colds, and allergies can have a similar effect.
          Speak to your practitioner about how this factors into things, if your post-exercise
          heart rate is found to be high.
        icon: icon-health
        name: Other Health Issues
      - description: A high post-exercise heart rate can run in families, and these
          risk factors are not included in this test. If you have a close relative
          (parent, child, sibling) with a high post-exercise heart rate, speak to
          your practitioner about your family history. If you have blood tests to
          look for related health issues, information about your family history will
          help with understanding your blood test results.
        icon: icon-dna
        name: Family History
    reduced:
      description: There are things you can do to try to prevent a higher post-exercise
        heart rate and stay healthy. Certain blood tests can also check your risk
        factors for some forms of heart disease. Talk to your practitioner about what
        is right for you.
      set:
      - description: Other factors besides genetics may impact your risk for a higher
          post-exercise heart rate. Consider talking to your practitioner about ordering
          blood tests to check your risk factors for heart disease or other health
          issues, and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: Being physically active is a good workout for your heart and
          keeps it strong, especially as your heart rate increases and decreases.
          Regular physical exercise can "condition" your heart and keep your post-exercise
          heart rate low. It's recommended that adults get 2.5 hours of moderate physical
          activity, like brisk walking or biking, every week. When exercise generates
          a high demand on the body's need for oxygen during the workout, it increases
          the body's need for oxygen after the workout. As a result, you can continue
          to burn fat and calories long after the workout is finished. To enhance
          these benefits, try incorporating High Intensity-Interval Training (HIIT)
          weight lifting exercises as part of a circuit.
        icon: icon-lifestyle
        name: Lifestyle
      - description: A high post-exercise heart rate can run in families, and these
          risk factors are not included in this test. If you have a close relative
          (parent, child, sibling) with a high post-exercise heart rate, speak to
          your practitioner about your family history. If you have blood tests to
          look for related health issues, information about your family history will
          help with understanding your blood test results.
        icon: icon-dna
        name: Family History
  references:
    data_analysis_articles:
    - Verweij N, van de Vegte YJ, van der Harst P. Genetic study links components
      of the autonomous nervous system to heart-rate profile during exercise. <i>Nat
      Commun</i>. 2018;9(1):898. Published 2018 Mar 1. <a href="https://doi.org/10.1038/s41467-018-03395-6">doi:10.1038/s41467-018-03395-6</a>
    recommendations_articles:
    - '"Pulse and Heart Rate" from Cleveland Clinic <a href="https://my.clevelandclinic.org/health/diagnostics/17402-pulse--heart-rate">https://my.clevelandclinic.org/health/diagnostics/17402-pulse--heart-rate</a>
      (Accessed June 26, 2019)'
    - '"Know Your Target Heart Rates for Exercise, Losing Weight and Health" from
      American Heart Association <a href="https://www.heart.org/en/healthy-living/fitness/fitness-basics/target-heart-rates">https://www.heart.org/en/healthy-living/fitness/fitness-basics/target-heart-rates</a>
      (Accessed June 26, 2019)'
  results_description:
    average:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have an average
        post-exercise heart rate.
      - Depending on your post-exercise heart rate and other factors, you may have
        an average risk for some forms of heart disease or other health issues.
      risk_text: Average risk
      risk_value: medium
    increased:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a higher
        post-exercise heart rate.
      - Depending on your post-exercise heart rate and other factors, you may have
        a higher risk for some forms of heart disease or other health issues.
      risk_text: High risk
      risk_value: high
    reduced:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a lower post-exercise
        heart rate.
      - Depending on your post-exercise heart rate and other factors, you may have
        a lower risk for some forms of heart disease or other health issues.
      risk_text: Low risk
      risk_value: low
  trait_category: Physical activity
  trait_explained: ''
  trait_heritability: 0.6
  trait_snp_heritability: 0.2
score_model:
  categories:
    average:
      from: 1.2941
      to: 1.7179
    increased:
      from: 1.7179
    reduced:
      to: 1.2941
  variants:
    rs11589125:
      effect_size: 0.075
      effect_allele: T
    rs12906962:
      effect_size: 0.048
      effect_allele: T
    rs12974440:
      effect_size: 0.067
      effect_allele: G
    rs12986417:
      effect_size: 0.037
      effect_allele: G
    rs151283:
      effect_size: 0.042
      effect_allele: C
    rs17168815:
      effect_size: 0.062
      effect_allele: G
    rs17180489:
      effect_size: 0.055
      effect_allele: C
    rs17362588:
      effect_size: 0.062
      effect_allele: G
    rs180238:
      effect_size: 0.043
      effect_allele: T
    rs1899492:
      effect_size: 0.04
      effect_allele: T
    rs1997571:
      effect_size: 0.042
      effect_allele: A
    rs2158712:
      effect_size: 0.045
      effect_allele: A
    rs2224202:
      effect_size: 0.043
      effect_allele: A
    rs272564:
      effect_size: 0.046
      effect_allele: A
    rs34310778:
      effect_size: 0.036
      effect_allele: C
    rs35596070:
      effect_size: 0.06
      effect_allele: C
    rs3757868:
      effect_size: 0.077
      effect_allele: G
    rs4836027:
      effect_size: 0.05
      effect_allele: T
    rs4963772:
      effect_size: 0.09
      effect_allele: A
    rs61765646:
      effect_size: 0.056
      effect_allele: A
    rs61928421:
      effect_size: 0.09
      effect_allele: C
    rs6488162:
      effect_size: 0.103
      effect_allele: C
    rs7072737:
      effect_size: 0.079
      effect_allele: A
    rs7130652:
      effect_size: 0.076
      effect_allele: T
    rs73043051:
      effect_size: 0.041
      effect_allele: C
