descriptions:
  about_the_test:
    genes:
    - SEC16B
    - GPRC5B
    - AC007238.1
    - CADM2-AS2
    - RNU4-17P
    - RPL31P12
    - CRTC1
    - FBXL17
    - YWHAQP6
    - ATXN2L
    - AL079352.1
    - FHIT
    - KCTD15
    - TNNI3K
    - KCTD10P1
    - LMOD1
    - TMEM18
    - AC007092.1
    - ADCY3
    - MRAS
    - MAP2K5
    - PACSIN1
    - LEMD2
    - AC091826.1
    - SLC39A8
    - ETV5
    - FTH1P5
    - AC103591.3
    - AL133166.1
    - AL161630.1
    - MIR642B
    - POC5
    - AC131157.1
    - AL450423.1
    - RNA5SP125
    - AC022469.2
    - GNAT2
    - ADCY9
    - AC008755.1
    - KRT8P44
    - AC109458.1
    - AC108467.1
    - HNF4G
    - BDNF
    - FTO
    - DTX2P1
    - AC090559.2
    - AKAP6
    - TRIM66
    text1: 'Body mass index is highly heritable. However, there is substantial variation
      in BMI heritability estimates, which range from 24% to 90%. Moreover, the genetic
      contribution to BMI appears to vary with age and may have a greater influence
      during childhood than adult life [1]. Our test is based on results of the genome
      wide association studies (GWAS) [2], which included over 180 000 individuals
      of European ancestry. The study identified 49 variants significantly associated
      with the BMI in European population. Together these SNPs explain about 12% of
      between people variance in BMIl [2].

      Our analyses use genetic data (genotypes) for those SNPs to calculate the polygenic
      score.'
    text2: Your results are based on genetic studies published in peer-revieved scientific
      and medical articles. Sometimes, the effects of many different genetic factors
      are combined into one risk estimate.
    text3: Your result is a risk based on 49 genetic variants, specifically called
      SNPs, found in genes associated with body mass index. Overall, this explains
      about 12% of the genetic variation in body mass index between people, based
      on current scientific knowledge.
  environmentScore: 20
  geneticScore: 12
  genotypeScore: 80
  id: bmi_nfe_model
  image_top: bmi.jpg
  model_description:
  - 'Body mass index (BMI) is a measure of an adult''s healthy weight, based on their
    current weight and height. It usually gives results in give categories: underweight,
    normal/healthy weight, overweight, and obese. These categories are a general guide
    and do not fit everyone perfectly, such as body builders, older adults, or athletes.
    But in general, a high BMI is a risk factor for heart disease, metabolic syndrome,
    diabetes, sleep apnea, and other health issues.'
  name: Body Mass Index (BMI)
  other_factors:
  - factors:
    - description: A diet high in carbohydrates, processed foods, and bad fats along
        with a sedentary lifestyle contribute to poor body-mass-index (BMI). Switching
        to a whole-food based, low-carb diet and incorporating HIIT (High intensity,
        interval training) can radically improve your BMI.
      name: Diet
    icon: icon-diet
    id: Diet
  - factors:
    - description: Lifestyle can impact the effect of a high body mass index. Smoking
        and lack of physical activity increase the risk of heart disease and may lead
        to being overweight. These are important risk factors to address if body mass
        index is also high. High intensity, interval training can radically improve
        your BMI.
      name: Lifestyle
    icon: icon-lifestyle
    id: Lifestyle
  - factors:
    - description: Sometimes, genetic risk factors can cause multiple people in the
        same family to have a high body mass index. These types of risk factors, which
        are usually due to rare genetic variants, are not included in this test. If
        you have a family history of a high body mass index, talk to your Practitioner
        or a genetic counselor.
      name: Family History
    icon: icon-dna
    id: Family History
  recommendations:
    average:
      description: There are things you can do to try to prevent a high body mass
        index and stay healthy. Certain blood tests can also check your risk factors
        for heart disease, diabetes, and other health issues. Talk to your practitioner
        about what is right for you.
      set:
      - description: Other factors besides genetics may impact your risk for a high
          body mass index. Consider talking to your practitioner about ordering blood
          tests to check your risk factors for health issues related to a high body
          mass index, and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: In general, try to pick foods that are low in calories, saturated
          fat, sodium, and trans fat. These include lean meats, nuts, fat-free or
          low-fat dairy products, fruits, and vegetables. A diet higher in plant products
          such as vegetables and low-glycemic fruits like berries, good fats, and
          quality proteins will help you to lower your risk.  Talk to your practitioner
          to determine whether a Mediterranean plan, Modified Mediterranean, or a
          low-carb/keto plan is right for you. Sticking to recommended limits for
          alcohol intake (2 drinks max/day for men, 1 drink/day max for women) and
          portion sizes (especially when dining out) can also help maintain a healthy
          weight.
        icon: icon-diet
        name: Diet
      - description: If you smoke, try to quit -- smoking increases the risk for heart
          disease. As well, try to get your body moving. It's recommended that adults
          get 2.5 hours of moderate physical activity, like brisk walking or biking,
          every week. Not only can this lower your risk of heart disease, but it can
          also help you maintain a healthy weight. HIIT exercise (High Intensity-Interval
          Training) will help to stimulate the body along with the good hormones necessary
          to mobilize fat and build muscle.
        icon: icon-lifestyle
        name: Lifestyle
      - description: High body mass index can run in families, and these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with a high body mass index, speak to your practitioner about your
          family history. If you have blood tests to look for health risk factors,
          information about your family history will help with understanding your
          blood test results.
        icon: icon-dna
        name: Family History
    increased:
      description: There are things you can do to try to prevent a high body mass
        index and stay healthy. Certain blood tests can also check your risk factors
        for heart disease, diabetes, and other health issues. Talk to your practitioner
        about what is right for you.
      set:
      - description: Your practitioner may be able to order blood tests to check your
          risk factors for health issues related to a high body mass index, like heart
          disease or diabetes. Talk to them about this and possible next steps that
          you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: Try to pick foods that are low in calories, saturated fat, sodium,
          and trans fat. These include lean meats, nuts, fat-free or low-fat dairy
          products, fruits, and vegetables. A diet higher in plant products such as
          vegetables and low-glycemic fruits like berries, good fats, and quality
          proteins will help you to lower your risk.  Talk to your practitioner to
          determine whether a Mediterranean plan, Modified Mediterranean, or a low-carb/keto
          plan is right for you. Sticking to recommended limits for alcohol intake
          (2 drinks max/day for men, 1 drink/day max for women) and portion sizes
          (especially when dining out) can also help maintain a healthy weight.
        icon: icon-diet
        name: Diet
      - description: If you smoke, now is a good time to quit -- smoking increases
          the risk for heart disease. As well, try to get your body moving. It's recommended
          that adults get 2.5 hours of moderate physical activity, like brisk walking
          or biking, every week. Not only can this lower your risk of heart disease,
          but it can also help you maintain a healthy weight. HIIT exercise (High
          Intensity-Interval Training) will help to stimulate the body along with
          the good hormones necessary to mobilize fat and build muscle.
        icon: icon-lifestyle
        name: Lifestyle
      - description: High body mass index can run in families, and these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with a high body mass index, speak to your practitioner about your
          family history. If you have blood tests to look for health risk factors,
          information about your family history will help with understanding your
          blood test results.
        icon: icon-dna
        name: Family History
    reduced:
      description: There are things you can do to try to prevent a high body mass
        index and stay healthy. Certain blood tests can also check your risk factors
        for heart disease, diabetes, and other health issues. Talk to your practitioner
        about what is right for you.
      set:
      - description: Other factors besides genetics may impact your risk for a high
          body mass index. Consider talking to your practitioner about ordering blood
          tests to check your risk factors for health issues related to a high body
          mass index, and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: It's always a good idea to try to pick foods that are low in
          calories, saturated fat, sodium, sugar, and trans fat. These include lean
          meats, nuts, fat-free or low-fat dairy products, fruits, and vegetables.
          A diet higher in plant products such as vegetables and low-glycemic fruits
          like berries, good fats, and quality proteins will help you to lower your
          risk. Talk to your practitioner to determine whether a Mediterranean plan,
          Modified Mediterranean, or a low-carb/keto plan is right for you. Sticking
          to recommended limits for alcohol intake (2 drinks max/day for men, 1 drink/day
          max for women) and portion sizes (especially when dining out) can also help
          maintain a healthy weight.
        icon: icon-diet
        name: Diet
      - description: If you smoke, try to quit -- smoking increases the risk for heart
          disease. As well, try to get your body moving. It's recommended that adults
          get 2.5 hours of moderate physical activity, like brisk walking or biking,
          every week. Not only can this lower your risk of heart disease, but it can
          also help you maintain a healthy weight. HIIT exercise (High Intensity-Interval
          Training) will help to stimulate the body along with the good hormones necessary
          to mobilize fat and build muscle.
        icon: icon-lifestyle
        name: Lifestyle
      - description: High body mass index can run in families, and these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with a high body mass index, speak to your practitioner about your
          family history. If you have blood tests to look for health risk factors,
          information about your family history will help with understanding your
          blood test results.
        icon: icon-dna
        name: Family History
  references:
    data_analysis_articles:
    - Graff M, Scott RA, Justice AE, et al. Genome-wide physical activity interactions
      in adiposity - A meta-analysis of 200,452 adults [published correction appears
      in PLoS Genet. 2017 Aug 23;13(8):e1006972]. <i>PLoS Genet</i>. 2017;13(4):e1006528.
      Published 2017 Apr 27. <a href="https://doi.org/10.1371/journal.pgen.1006528">doi:10.1371/journal.pgen.1006528</a>
    recommendations_articles:
    - '"Body mass index" from Medline Plus at National Institutes of Health <a href="https://medlineplus.gov/ency/article/007196.htm">https://medlineplus.gov/ency/article/007196.htm</a>
      (Accessed June 23, 2019.

      )'
    - '"About Adult BMI" from Centers for Disease Control and Prevention <a href="https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/index.html">https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/index.html</a>
      (Accessed June 23, 2019)'
  results_description:
    average:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have an average
        body mass index (BMI).
      - Depending on your body mass index and other factors, you may have an average
        risk for heart disease, diabetes, or other health issues.
      risk_text: Average risk
      risk_value: medium
    increased:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a higher
        body mass index (BMI).
      - Depending on your body mass index and other factors, you may have a higher
        risk for heart disease, diabetes, or other health issues.
      risk_text: High risk
      risk_value: high
    reduced:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a lower body
        mass index (BMI).
      - Depending on your body mass index and other factors, you may have a lower
        risk for heart disease, diabetes, or other health issues.
      risk_text: Low risk
      risk_value: low
  trait_category: Physical activity
  trait_explained: 0.12
  trait_heritability: 0.8
  trait_snp_heritability: ''
score_model:
  categories:
    average:
      from: 1.382
      to: 1.75891
    increased:
      from: 1.75891
    reduced:
      to: 1.382
  variants:
    rs10132280:
      effect_size: 0.023
      effect_allele: C
    rs10938397:
      effect_size: 0.035
      effect_allele: G
    rs10968577:
      effect_size: 0.03
      effect_allele: T
    rs11209951:
      effect_size: 0.032
      effect_allele: T
    rs11672660:
      effect_size: 0.029
      effect_allele: C
    rs12325113:
      effect_size: 0.03
      effect_allele: C
    rs12429545:
      effect_size: 0.031
      effect_allele: A
    rs12446632:
      effect_size: 0.036
      effect_allele: G
    rs12885454:
      effect_size: 0.022
      effect_allele: C
    rs13107325:
      effect_size: 0.051
      effect_allele: T
    rs13191362:
      effect_size: 0.036
      effect_allele: A
    rs17024393:
      effect_size: 0.084
      effect_allele: C
    rs1720825:
      effect_size: 0.026
      effect_allele: A
    rs17405819:
      effect_size: 0.024
      effect_allele: T
    rs17522122:
      effect_size: 0.021
      effect_allele: T
    rs185350:
      effect_size: 0.024
      effect_allele: T
    rs1928295:
      effect_size: 0.022
      effect_allele: T
    rs2035935:
      effect_size: 0.044
      effect_allele: G
    rs2049045:
      effect_size: 0.043
      effect_allele: G
    rs2075171:
      effect_size: 0.026
      effect_allele: G
    rs2112347:
      effect_size: 0.023
      effect_allele: T
    rs2241423:
      effect_size: 0.033
      effect_allele: G
    rs2303108:
      effect_size: 0.025
      effect_allele: C
    rs2365389:
      effect_size: 0.028
      effect_allele: C
    rs2430307:
      effect_size: 0.043
      effect_allele: T
    rs2531995:
      effect_size: 0.031
      effect_allele: T
    rs2820315:
      effect_size: 0.024
      effect_allele: T
    rs2867125:
      effect_size: 0.07
      effect_allele: C
    rs40067:
      effect_size: 0.035
      effect_allele: G
    rs4130548:
      effect_size: 0.025
      effect_allele: C
    rs4715210:
      effect_size: 0.039
      effect_allele: T
    rs4929923:
      effect_size: 0.025
      effect_allele: C
    rs571312:
      effect_size: 0.059
      effect_allele: A
    rs633715:
      effect_size: 0.053
      effect_allele: C
    rs6752378:
      effect_size: 0.033
      effect_allele: A
    rs6804842:
      effect_size: 0.023
      effect_allele: G
    rs6809651:
      effect_size: 0.048
      effect_allele: G
    rs6864049:
      effect_size: 0.021
      effect_allele: G
    rs6870983:
      effect_size: 0.025
      effect_allele: C
    rs7124681:
      effect_size: 0.026
      effect_allele: A
    rs7138803:
      effect_size: 0.03
      effect_allele: A
    rs7141420:
      effect_size: 0.024
      effect_allele: T
    rs7551507:
      effect_size: 0.023
      effect_allele: C
    rs757318:
      effect_size: 0.021
      effect_allele: C
    rs759250:
      effect_size: 0.03
      effect_allele: A
    rs7757419:
      effect_size: 0.028
      effect_allele: T
    rs943466:
      effect_size: 0.024
      effect_allele: G
    rs9852127:
      effect_size: 0.03
      effect_allele: A
    rs9930333:
      effect_size: 0.084
      effect_allele: G
