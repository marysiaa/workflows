descriptions:
  about_the_test:
    genes:
    - HFE
    text1: 'Heritable hemochromatosis is caused usually by variants localized within
      the HFE gene and impairing the function of protein encoded by this gene. Two
      such mutations are relatively frequent in human populations: rs1799945 (His63Asp)
      and rs1800562 (Cys282Tyr). These variants are found in 87% of patients with
      iron overload [1-6].

      Our analyses use genetic data (genotypes) for these SNPs to calculate the polygenic
      score.'
    text2: Your results are based on genetic studies published in peer-revieved scientific
      and medical articles. Sometimes, the effects of many different genetic factors
      are combined into one risk estimate.
    text3: Your result is a risk based on 2 genetic variants, specifically called
      SNPs, in 1 gene associated with iron overload. These variants are found in approximately
      87% of individuals with iron overload.
  environmentScore: NA
  geneticScore: NA
  genotypeScore: NA
  id: iron_overload_nfe_model
  image_top: report.jpg
  model_description:
  - The body needs iron and gets it from sources like diet, but the right amount is
    critical. Too little iron is called iron deficiency, or anemia. Too much iron,
    called iron overload, is also a problem. If iron overload occurs for some time
    (perhaps years), iron can accumulate in the body's organs -- this may cause liver
    disease, heart disease, or diabetes if not treated. Treatment for iron overload
    includes phlebotomy (medical removal of blood from the body) or other measures,
    and in many cases this can prevent symptoms. Iron overload may be acquired or
    inherited. In the U.S., inherited iron overload is a very common genetic disease.
  name: Iron overload
  other_factors:
  - factors:
    - description: Genetics has a moderate overall impact on telomere length when
        you consider all the other factors.
      name: Biological Sex
    icon: icon-sex
    id: Biological Sex
  - factors:
    - description: People who are of Northern European Caucasian descent have a higher
        risk for iron overload. It is less common in Africans, Hispanics, Asians,
        and First Nations peoples.
      name: Ethnic Background
    icon: icon-race
    id: Ethnic Background
  - factors:
    - description: Heavy drinking or alcoholism, which taxes the liver, is a risk
        factor for iron overload.
      name: Lifestyle
    icon: icon-lifestyle
    id: Lifestyle
  - factors:
    - description: Sometimes, genetic risk factors can cause multiple people in the
        same family to have iron overload. These types of risk factors, which include
        diabetes, heart attack, liver disease, arthritis, and erectile dysfunction,
        are not included in this test. If you have a family history of iron overload,
        talk to your Practitioner.
      name: Family History
    icon: icon-dna
    id: Family History
  recommendations:
    average:
      description: There are things you can do to try to prevent symptoms of iron
        overload and stay healthy. Certain blood tests can also check your risk factors
        for iron overload. Talk to your practitioner about what is right for you.
      set:
      - description: Other factors besides genetics may impact your risk factors for
          iron overload or other health issues. Consider talking to your practitioner
          about ordering blood tests to check your iron levels or risks for other
          health issues, and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: Try to avoid regular heavy drinking (8 or more drinks per week
          for women, 15 or more drinks per week for men). Heavy drinking is a risk
          factor for iron overload. Additionally, being physically active -- such
          as 2.5 hours of moderate physical activity every week -- can lower your
          risk of heart disease and diabetes, which are risk factors for iron overload.
        icon: icon-lifestyle
        name: Lifestyle
      - description: Iron overload can run in families, and some of these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with iron overload, speak to your practitioner about your family
          history. If you have blood tests to look for related health issues, information
          about your family history will help with understanding your blood test results.
        icon: icon-dna
        name: Family History
    increased:
      description: There are things you can do to try to prevent symptoms of iron
        overload and stay healthy. Certain blood tests can also check your risk factors
        for iron overload. Talk to your practitioner about what is right for you.
      set:
      - description: Your practitioner may be able to order blood tests to check your
          risk factors for iron overload or other health issues. Treatment may be
          available if your iron levels are high. Talk to them about this and possible
          next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: Keep a food diary to track iron-rich items in your diet, then
          discuss with your practitioner before making dietary changes. Iron-rich
          foods include beef, dark meat turkey broccoli, spinach, tofu, organ meats
          (like kidney, brain, heart, liver), clams, legumes, quinoa, and dark chocolate.
          Avoid regular heavy drinking (8 or more drinks per week for women, 15 or
          more drinks per week for men). Heavy drinking is a risk factor for iron
          overload. Speak to your practitioner if you need help on ways to make lifestyle
          changes, including speaking to a registered dietician about your diet.
        icon: icon-lifestyle
        name: Lifestyle
      - description: Iron overload can run in families, and some of these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with iron overload, speak to your practitioner about your family
          history. If you have blood tests to look for related health issues, information
          about your family history will help with understanding your blood test results.
        icon: icon-dna
        name: Family History
    reduced:
      description: There are things you can do to try to prevent symptoms of iron
        overload and stay healthy. Certain blood tests can also check your risk factors
        for iron overload. Talk to your practitioner about what is right for you.
      set:
      - description: Other factors besides genetics may impact your risk factors for
          iron overload or other health issues. Consider talking to your practitioner
          about ordering blood tests to check your iron levels or risks for other
          health issues, and possible next steps that you can take.
        icon: icon-talk2
        name: Talk to Your Practitioner
      - description: Try to avoid regular heavy drinking (8 or more drinks per week
          for women, 15 or more drinks per week for men). Heavy drinking is a risk
          factor for iron overload. Additionally, being physically active -- such
          as 2.5 hours of moderate physical activity every week -- can lower your
          risk of heart disease and diabetes, which are risk factors for iron overload.
        icon: icon-lifestyle
        name: Lifestyle
      - description: Iron overload can run in families, and some of these risk factors
          are not included in this test. If you have a close relative (parent, child,
          sibling) with iron overload, speak to your practitioner about your family
          history. If you have blood tests to look for related health issues, information
          about your family history will help with understanding your blood test results.
        icon: icon-dna
        name: Family History
  references:
    data_analysis_articles:
    - Gochee PA, Powell LW, Cullen DJ, Du Sart D, Rossi E, Olynyk JK. A population-based
      study of the biochemical and clinical expression of the H63D hemochromatosis
      mutation [published correction appears in Gastroenterology 2002 Apr;122(4):1191].
      <i>Gastroenterology</i>. 2002;122(3):646-651. <a href="https://doi.org/10.1016/s0016-5085(02)80116-0">doi:10.1016/s0016-5085(02)80116-0</a>
    - Kelley M, Joshi N, Xie Y, Borgaonkar M. Iron overload is rare in patients homozygous
      for the H63D mutation. <i>Can J Gastroenterol Hepatol</i>. 2014;28(4):198-202.
      <a href="https://doi.org/10.1155/2014/468521">doi:10.1155/2014/468521</a>
    - 'Burke W, Imperatore G, McDonnell SM, Baron RC, Khoury MJ. Contribution of different
      HFE genotypes to iron overload disease: a pooled analysis. <i>Genet Med</i>.
      2000;2(5):271-277. <a href="https://doi.org/10.1097/00125817-200009000-00001">doi:10.1097/00125817-200009000-00001</a>'
    - "Barton JC, Edwards CQ. <i>HFE</i> Hemochromatosis. In: Adam MP, Everman DB,\
      \ Mirzaa GM, et al., eds. <i>GeneReviews\xAE</i>. Seattle (WA): University of\
      \ Washington, Seattle; April 3, 2000."
    - 'Kanwar P, Kowdley KV. Diagnosis and treatment of hereditary hemochromatosis:
      an update [published correction appears in Expert Rev Gastroenterol Hepatol.
      2013 Nov;7(8):767]. <i>Expert Rev Gastroenterol Hepatol</i>. 2013;7(6):517-530.
      <a href="https://doi.org/10.1586/17474124.2013.816114">doi:10.1586/17474124.2013.816114</a>'
    - Alexander J, Kowdley KV. HFE-associated hereditary hemochromatosis. <i>Genet
      Med</i>. 2009;11(5):307-313. <a href="https://doi.org/10.1097/GIM.0b013e31819d30f2">doi:10.1097/GIM.0b013e31819d30f2</a>
    recommendations_articles: []
  results_description:
    average:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have an average
        risk for iron overload.
      - Depending on your blood iron levels and other factors, you may have an average
        risk for liver disease, diabetes, or other health issues related to iron overload.
      risk_text: Average risk
      risk_value: medium
    increased:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a higher
        risk for iron overload.
      - Depending on your blood iron levels and other factors, you may have a higher
        risk for liver disease, diabetes, or other health issues related to iron overload.
      risk_text: High risk
      risk_value: high
    reduced:
      descriptions:
      - Based on the genetic factors tested, you are more likely to have a lower risk
        for iron overload.
      - Depending on your blood iron levels and other factors, you may have a lower
        risk for liver disease, diabetes, or other health issues related to iron overload.
      risk_text: Low risk
      risk_value: low
  trait_category: Nutrition
  trait_explained: ''
  trait_heritability: ''
  trait_snp_heritability: ''
score_model:
  categories:
    average:
      from: 1.1
      to: 1.9
    increased:
      from: 1.9
    reduced:
      to: 1.1
  variants:
    rs1799945:
      effect_size: 0.6
      effect_allele: G
    rs1800562:
      effect_size: 1.0
      effect_allele: A
