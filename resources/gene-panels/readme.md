#  GENE-PANELS
  
Gene panels developed by clinical molecular laboratories assess multiple potential genetic causes of a suspected disorder(s) 
simultaneously and reduce the cost and time of diagnostic testing.

Websites: 
- https://panelapp.genomicsengland.co.uk/panels/
- https://www.ncbi.nlm.nih.gov/clinvar/docs/acmg/
  
---  
The location of resources:  
  - **Docker** (main location):  
    - intelliseqngs/gene-panels-resources
---

### Updates
 
**Last update date:** 12-09-2022

**History of versions and updates:**
 - **version** (12-09-2022)
   - docker version - 1.0.4
   - ensembl update
   - ubuntu-minimal update
 - **version** (20-01-2022)
   - docker version - 1.0.3
   - update panels
 - **version** (29-12-2021)
   - docker version - 1.0.2
   - add uniprot to ensembl-genes dockerfile
 - **version** (17-11-2021)
   - docker version - 1.0.1
   - docker ensembl-genes update
 - **version** (14-10-2021)
   - validated by ensembl version - 104
   - docker version - 1.0.0
     
---

Panels are used in a few tasks and if you update panel please update following tasks:
- `vcf-anno-ge-panel.wdl` (build new Dockerfile: task_vcf-anno-ge-panels)
- `panel-generate.wdl` (build new Dockerfile: task_panel-generate)

Additionally, please notify the Dev team to update the Explorare in docker.

---

### How to create ACMG panel 

From [NCBI website](https://www.ncbi.nlm.nih.gov/clinvar/docs/acmg/) using the `pandas` library obtain a table with ACMG genes. 

```
url = 'https://www.ncbi.nlm.nih.gov/clinvar/docs/acmg/'
html = requests.get(url).content
df_list = pd.read_html(html)
df = df_list[-1]
ACMG_genes = df["Gene via GTR"].tolist()
ACMG_genes = [gene.split(" ")[0] for gene in ACMG_genes]

# save
with open("ACMG_Incidental_Findings.txt", "w") as outfile:
    outfile.write(", ".join(ACMG_genes))
```