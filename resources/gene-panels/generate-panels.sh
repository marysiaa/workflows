#!/bin/bash

## get panel genes
cd /intelliseqtools/
python3 generate-panels.py

## create json panels from genes list
for file in *.txt
do
	split_genes=$(cat "$file" | sed -r 's/,//g')
	genes_json="["
  	for gene in $split_genes
  	do
    		genes_json=$genes_json'{"name":"'$gene'","score":30.0,"type":"user"},'
  	done
  	genes_json=$(echo $genes_json] | sed 's/\(.*\),/\1/')
  	file_basename=$(echo "${file%.*}")
  	echo "$genes_json" > "/panels/${file_basename}.json"
done

rm *.txt
cd ../

python3 /intelliseqtools/synonym_panel_validation.py \
  --ensembl_genes /resources/ensembl-genes.json \
  -d /panels/ > /resources/gene-panels.json
