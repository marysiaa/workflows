#  COLUMN-LIST-TO-JSON
  
A `.csv` file for selecting selected columns from a JSON file, consisting of:
- column name (`column_name`)
- column description (`column_description`)
- field in VCF, e.g. INFO, FORMAT, CHROM (`field`)
- type od field, e.g. Array (`type`)
- option whether a given field is used in report (`report`)
- option whether a given field should be selected from JSON (`Field`)
- option whether a given field is default or not (`default`)
  
---  
The location of resources:  
  - **Gitlab** (main location):  
    - https://gitlab.com/intelliseq/workflows/-/raw/dev/resources/column-list-to-json/column-list-to-json.csv
---

### Updates
 
**Last update date:** 17-01-2023
     
---

The `.csv` file is used as input to the `json_select_fields.py` script, which is used in the `vcf-to-json-or-tsv.wdl` task.

---

### How to create 

Run `./resources/column-list-to-json/google_spreadsheet_to_column_list.sh` script.
