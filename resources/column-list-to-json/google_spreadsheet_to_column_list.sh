#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
SCRIPT=${PROJECT_DIR}resources/column-list-to-json/google_spreadsheet_to_column_list.py

python3 $SCRIPT \
  --input-credentials-json "" \
  --input-google-spreadsheet-key "1sA5HBW5c8YydT8J347NotkJLBt1SgG6WHHZUHqPXkXE" \
  --output-csv "${PROJECT_DIR}resources/column-list-to-json/column-list-to-json.csv"
