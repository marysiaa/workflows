import gspread
import argparse
import pandas as pd

pd.options.mode.chained_assignment = None
__version__ = "0.0.1"


def json_to_df(records: dict) -> pd.DataFrame:
    """Convert JSON to df

    Args:
        records (dict): Records JSON

    Returns:
        pd.DataFrame: Dataframe with all records
    """
    return pd.DataFrame.from_dict(records)


def select_wanted_records(df: pd.DataFrame) -> pd.DataFrame:
    """Select only records with `yes` in field `Field`

    Args:
        df (pd.DataFrame): Dataframe with all records

    Returns:
        pd.DataFrame: Dataframe with only wanted records
    """
    return df[(df['Field']=='yes')]
    

def convert_to_bool(df: pd.DataFrame) -> pd.DataFrame:
    """Convert column to bool

    Args:
        df (pd.DataFrame): Dataframe with `yes` and `` in column Default

    Returns:
        pd.DataFrame: Dataframe with `True` and `False` in column Default
    """
    return df['Default'].map({'yes':True ,'':False})


def save_to_csv(df: pd.DataFrame, output_filename):
    """Save to csv

    Args:
        df (pd.DataFrame): Dataframe with only wanted records
        output_filename (_type_): Output filename
    """
    df.to_csv(output_filename, index=False)  


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('--input-credentials-json', type=str, required=True,
                        help='Credentials.json containing data necessary to get access to google developer\'s privileges')
    parser.add_argument('--input-google-spreadsheet-key', type=str, required=True,
                        help='A key to access to the google spreadsheet')
    parser.add_argument('--output-csv', type=str, required=True,
                        help='A csv file')
    parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')

    args = parser.parse_args()


    # G all records from input google spreadsheet
    gc = gspread.service_account(filename=args.input_credentials_json)
    sheets = gc.open_by_key(args.input_google_spreadsheet_key)
    columns_descriptions = sheets.worksheet('columns_descriptions')
    all_records = columns_descriptions.get_all_records()


    # Convert JSON to df
    records_df = json_to_df(all_records)


    # Select wanted records
    selected_records = select_wanted_records(records_df)
    

    # Convert `yes` and `` in column Default to `True` and `False`
    selected_records['Default'] = convert_to_bool(selected_records)


    # Save to csv
    save_to_csv(selected_records, args.output_csv)
