#  RESOURCES

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources**

---

## Table of Contents

**Resources**

  * [***Broad institute resources for BQSR and VQSR***](./broad-institute-resources-for-bqsr-and-vqsr/readme.md)
     * [INDELs, known sites](./broad-institute-resources-for-bqsr-and-vqsr/readme.md#indels-known-sites)
     * [SNPs, known sites](./broad-institute-resources-for-bqsr-and-vqsr/readme.md#snps-known-sites)
     * [dbSNP for BQSR and VQSR](./broad-institute-resources-for-bqsr-and-vqsr/readme.md#dbsnp-for-bqsr-and-vqsr)
  * [***Broad institute resources for SV calling***](./broad-institute-resources-for-sv-calling/readme.md)
     * [Ploidy priors tables](./broad-institute-resources-for-sv-calling/readme.md#ploidy-priors-tables)
  * [***Column list to json***](./column-list-to-json/readme.md)   
  * [***Gene panels***](./gene-panels/readme.md)
  * [***Selectable (dataindex repository)***](https://gitlab.com/intelliseq/dataindex)
  * [***Intervals***](./intervals/readme.md)
     * [Agilent](./intervals/readme.md#agilent)
     * [Broad institute WGS calling regions](./intervals/readme.md#broad-institute-wgs-calling-regions)
     * [UCSC genes](./intervals/readme.md#ucsc-genes)
     * [UCSC, simple repeats](./intervals/readme.md#ucsc-simple-repeats)
  * [***Miscellaneous***](./miscellaneous/readme.md)
     * [Umap mappability](./miscellaneous/readme.md#umap-mappability)
     * [dbSNP for genotyping purposes](./miscellaneous/readme.md#dbsnp-for-genotyping-purposes)
     * [Clinvar vcf to compare with ACMG](./miscellaneous/readme.md#clinvar-vcf-to-compare-with-acmg)
     * [Array annotation files](./miscellaneous/readme.md#array-annotation)
     * [Phasing with Eagle](./miscellaneous/readme.md#phasing-with-eagle)
     * [Imputing with Beagle](./miscellaneous/readme.md#imputing-with-beagle)
     * [Genetics home reference](./miscellaneous/readme.md#genetics-home-reference) 
     * [Genomics England panels](./miscellaneous/readme.md#genomics-england-panels)  
     * [WisecondorX reference](./miscellaneous/readme.md#wisecondorx-reference)  
     * [MultiQC FastQC inputs](./miscellaneous/readme.md#multiqc-fastqc-inputs)  
     * [MultiQC BAM metrics](./miscellaneous/readme.md#multiqc-bam-metrics)  
  * [***Reference genomes***](./reference-genomes/readme.md)
    * [Broad institute hg19](./reference-genomes/readme.md#broad-institute-hg19)
    * [Broad institute hg38](./reference-genomes/readme.md#broad-institute-hg38)
    * [GRCh38 no alt analysis set](./reference-genomes/readme.md#grch38-no-alt-analysis-set)
    * [GRCh38 no alt plus hs38d1 analysis set](./reference-genomes/readme.md#grch38-no-alt-plus-hs38d1-analysis-set)
  * [***Resources for American College of Medical Genetics and Genomics (ACMG)***](./acmg/readme.md)
     * [ClinVar](./acmg/readme.md#clinvar)
     * [UniProt](./acmg/readme.md#uniprot)
     * [gnomAD](./acmg/readme.md#gnomad)  
  * [***Simulated samples***](./simulated-samples/readme.md) 
  * [***SNPs and INDELs annotations***](./snps-and-indels-annotations/readme.md)
    * [**dbSNP**](./snps-and-indels-annotations/dbsnp/readme.md)
      * [dbsnp only rs IDs](./snps-and-indels-annotations/dbsnp/readme.md#dbsnp-only-rs-ids)
        * build 153
    * [**Frequencies**](./snps-and-indels-annotations/frequencies/readme.md)
      * [Merged frequencies, exome](./snps-and-indels-annotations/frequencies/readme.md#merged-frequencies-exome)
        * 0.1.0
        * 0.0.1
      * [Merged frequencies, genome](./snps-and-indels-annotations/frequencies/readme.md#merged-frequencies-genome)
        * 0.1.0
        * 0.0.1
      * [1000 Genomes](./snps-and-indels-annotations/frequencies/readme.md#1000-genomes)
      * [ESP6500](./snps-and-indels-annotations/frequencies/readme.md#esp6500)
      * [ExAC](./snps-and-indels-annotations/frequencies/readme.md#exac)
      * [gnomAD exomes  v2, liftover](./snps-and-indels-annotations/frequencies/readme.md#gnomad-exomes-v2-liftover)
      * [gnomAD genomes v3](./snps-and-indels-annotations/frequencies/readme.md#gnomad-genomes-v3)
      * [gnomAD genomes v3 exome, calling intervals](./snps-and-indels-annotations/frequencies/readme.md#gnomad-genomes-v3-exome-calling-intervals)
      * [gnomAD genomes  v2, liftover](./snps-and-indels-annotations/frequencies/readme.md#gnomad-genomes-v2-liftover)
      * [gnomAD genomes v2, liftover, exome calling intervals](./snps-and-indels-annotations/frequencies/readme.md#gnomad-genomes-v2-liftover-exome-calling-intervals)
      * [Mitomap](./snps-and-indels-annotations/frequencies/readme.md#mitomap)
    * [**dbNSFP**](./snps-and-indels-annotations/dbnsfp/readme.md)
    * [**Functional annotations - gene level**](./snps-and-indels-annotations/functional-annotations-gene-level/readme.md)
      * [All functional annotations, gene level](./snps-and-indels-annotations/functional-annotations-gene-level/readme.md#all-functional-annotations-gene-level)
        * 0.2.0
        * 0.1.0
      * [ClinVar diseases](./snps-and-indels-annotations/functional-annotations-gene-level/readme.md#clinvar-diseases)
      * [Human Phenotype Ontology](./snps-and-indels-annotations/functional-annotations-gene-level/readme.md#human-phenotype-ontology)
    * [**Functional annotations - variant level**](./snps-and-indels-annotations/functional-annotations-variant-level/readme.md)
      * [Merged VCF functional annotations, variant level](./snps-and-indels-annotations/functional-annotations-variant-level/readme.md#merged-vcf-functional-annotations-variant-level)
        * 2.0.0
        * 1.0.0
        * 0.2.0
        * 0.1.0
      * [ClinVar](./snps-and-indels-annotations/functional-annotations-variant-level/readme.md#clinvar)
      * [Mitomap diseases](./snps-and-indels-annotations/functional-annotations-variant-level/readme.md#mitomap-diseases)
    * [**Scores**](./snps-and-indels-annotations/scores/readme.md)
      * [Merged VCF scores annotations](./snps-and-indels-annotations/scores/readme.md#merged-vcf-scores-annotations)
        * 0.1.0
      * [CADD](./snps-and-indels-annotations/scores/readme.md#cadd)
      * [GERP](./snps-and-indels-annotations/scores/readme.md#gerp)
      * [M-CAP](./snps-and-indels-annotations/scores/readme.md#m-cap)
        * v1.4
      * [PhastCons](./snps-and-indels-annotations/scores/readme.md#phastcons)
      * [PhyloP](./snps-and-indels-annotations/scores/readme.md#phylop)
      * [SIFT4G](./snps-and-indels-annotations/scores/readme.md#sift4g)  
    * [**CIVIC**](./snps-and-indels-annotations/civic/readme.md)  
* [***Structural variants annotation***](./structural-variants-annotations/readme.md)
    * [AnnotSV](./structural-variants-annotations/readme.md#annotsv-annotations-description)
      * 0.0.1
 * [***pgx***](./pgx/readme.md)
 * [***Broad Institute MT***](./broad-institute-mt/readme.md)  
       * 0.0.1
 * [***GATK best practices somatic hg38***](./gatk-best-practices-somatic-hg38/readme.md)  
   * 16-03-2021    
