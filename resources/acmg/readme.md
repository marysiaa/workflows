# ACMG

Resources for American College of Medical Genetics and Genomics (ACMG). Reference genome: **GRCh38**.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg**


The location of archival resources (Archival directory):
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg_files**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg_files**

---

## Table of Contents

**ACMG**
  * [ClinVar](#clinvar)
  * [UniProt](#uniprot)
  * [GnomAD](#gnomad)
  * [Archival directory](#archival-directory)  
  
[Return to: Resources](./../readme.md)

---

## ClinVar

**Last update date:** 10-12-2022

**Update requirements:** monthly

**History of versions:**
  + 10-12-2022
  + 22-06-2022
  + 22-11-2021
  + 09-12-2020
  + 30-07-2020  
  + 20-02-2020
  + 15-07-2019 (Archival directory)

---
**Description:**


**[Instructions on how to create ClinVar dictionaries for ACMG annotation](#instructions-on-how-to-create-clinvar-dictionaries-for-acmg-annotation)**

**Directory contents:**

```bash
   <clinvar>
      |
<last-update-date>
      |
      ├── clinvar-lof-dictionary.json
      ├── clinvar-ms-dictionary.json
      ├── clinvar-pathogenic-sites.tab
      ├── clinvar-protein-changes-dictionary.json
      ├── clinvar.variant-level-annotated-with-snpeff.vcf.gz
      └── clinvar.variant-level-annotated-with-snpeff.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)

---


### Instructions on how to create ClinVar dictionaries for ACMG annotation


**1.**  Prepare the workspace.

**Required:** [Latest version of the ClinVar annotation vcf](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md#clinvar)  
Create data directory, VERSION variable should be the same as version of the used clinvar file (`/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar/VERSION/clinvar.variant-level.vcf.gz`):  

```bash
VERSION="10-12-2022"
CLINVAR_ACMG_DIR="/data/public/intelliseqngs/workflows/resources/acmg/clinvar/"
cd $CLINVAR_ACMG_DIR
mkdir $VERSION
cd $VERSION
```
 
**2.**  Annotate **ClinVar annotation vcf** with SnpEff (use task):  

It is very important to use the same version of SnpEff and SnpEff database, as in the pipelines.  
Thus, use the latest version of the [**vcf-anno-snpeff.wdl**](https://gitlab.com/intelliseq/workflows/-/blob/vcf-anno-snpeff@1.5.0/src/main/wdl/tasks/vcf-anno-snpeff/vcf-anno-snpeff.wdl)    
for **ClinVar annotation vcf** annotation. Set the **annotation_mode** argument as **clinvar** (add to input json:   
`"annotate_vcf_with_snpeff_workflow.annotate_vcf_with_snpeff.annotation_mode": "clinvar"`). 
Example of the input json file, substitute the VERSION string with the appropriate clinvar update date:
```bash
{
"vcf_anno_snpeff_workflow.vcf_anno_snpeff.vcf_gz": "/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar/VERSION/clinvar.variant-level.vcf.gz",
"vcf_anno_snpeff_workflow.vcf_anno_snpeff.vcf_gz_tbi": "/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar/VERSION/clinvar.variant-level.vcf.gz.tbi",
"vcf_anno_snpeff_workflow.vcf_anno_snpeff.annotation_mode": "clinvar",
"vcf_anno_snpeff_workflow.vcf_anno_snpeff.vcf_basename": "clinvar.variant-level"
}

```
Copy the resulted files (`clinvar.variant-level_annotated-with-snpeff.vcf.gz` and `clinvar.variant-level_annotated-with-snpeff.vcf.gz.tbi`)
 to the `/data/public/intelliseqngs/workflows/resources/acmg/clinvar/$VERSION` directory. Go to this directory.  

**3.** Create ClinVar based files needed for ACMG annotation:    
* clinvar-lof-dictionary.json (used by: acmg-pvs1.py)  
* clinvar-ms-dictionary.json (used by: acmg-bp1.py, acmg-pp2.py)  
* clinvar-pathogenic-sites.tab.gz (used by: acmg-pm1.py)    
* clinvar-pathogenic-sites.tab.gz.tbi (used by: acmg-pm1.py)
* clinvar-protein-changes-dictionary.json (used by: acmg-pm5.py, acmg-ps1.py)  

**Required:** [**prepare-clinvar-lof-dictionary.py, version 0.0.3**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-lof-dictionary.py)  
**Required:** [**prepare-clinvar-ms-dictionary.py, version 0.0.3**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-ms-dictionary.py)  
**Required:** [**prepare-clinvar-pathogenic-sites-list.py, version 0.0.2**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-pathogenic-sites-list.py)    
**Required:** [**prepare-clinvar-protein-changes-dictionary.py, version 0.0.4**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-protein-changes-dictionary.py)  

  
```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-lof-dictionary.py@0.0.3/src/main/scripts/resources-tools/prepare-clinvar-lof-dictionary.py  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-ms-dictionary.py@0.0.3/src/main/scripts/resources-tools/prepare-clinvar-ms-dictionary.py  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-pathogenic-sites-list.py@0.0.2/src/main/scripts/resources-tools/prepare-clinvar-pathogenic-sites-list.py  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-protein-changes-dictionary.py@0.0.4/src/main/scripts/resources-tools/prepare-clinvar-protein-changes-dictionary.py  
  
python3 prepare-clinvar-lof-dictionary.py -i clinvar.variant-level_annotated-with-snpeff.vcf.gz -o clinvar-lof-dictionary.json   
python3 prepare-clinvar-ms-dictionary.py -i clinvar.variant-level_annotated-with-snpeff.vcf.gz -o clinvar-ms-dictionary.json   
python3 prepare-clinvar-pathogenic-sites-list.py -i clinvar.variant-level_annotated-with-snpeff.vcf.gz -o clinvar-pathogenic-sites.tab   
python3 prepare-clinvar-protein-changes-dictionary.py -i clinvar.variant-level_annotated-with-snpeff.vcf.gz -o clinvar-protein-changes-dictionary.json  
bgzip clinvar-pathogenic-sites.tab
tabix -s1 -b2 -e2 clinvar-pathogenic-sites.tab.gz
```

*** Version notes:
* 10-12-2022
  + Ensembl 107, annotation with `vcf-anno-snpeff@1.5.0`, snpEff version 5.1d   
* 22-06-2022
  + Ensembl 104, updated ClinVar, annotation with `vcf-anno-snpeff@1.4.2` - resolved selenocysteine bug, scripts updated to deal with `pathogenic_low_penetrance` categories
* 22-11-2021 
  + Ensembl 104, updated ClinVar, annotation with `vcf-anno-snpeff@1.3.1`(2.03.2022 - bgzipped version of the `clinvar-pathogenic-sites.tab` file)    
* 05-06-2021
  + new Ensembl version (104), updated ClinVar
* 09-12-2020  
  + new version of SnpEff (5.0c) and SnpEff database (102). This database version is not compatible with snpEff 4.3t

* 30-07-2020    
  + new versions of scripts (prepare-clinvar-lof-dictionary.py@0.0.2, prepare-clinvar-ms-dictionary.py@0.0.2, prepare-clinvar-protein-changes-dictionary.py@0.0.3)  
  fixed bug with omitting variants from Clinvar vcf lines with many genes;  
  + new snpEff database version (Ensembl 100)     

[Return to the table of contents](#table-of-contents)   

---

## UniProt

**Last update date:** 14-10-2022

**Update requirements:** twice a year  
**History of updates:** 
  + 14.10.2022 (1.1.0 docker, 2022_04 release, from 12-10-2022, Genes from Ensembl 107)    
  + 22.06.2022 (1.0.2 docker, 2022_02 release, from 15-05-2022, Genes from Ensembl 104)
  + 22.11.1021 (1.0.0 docker, 2021_04 release, from 02-03-2022 `uniprot-functional-data-from-table.json` not needed any more )
  + 05.06.2021
  + 09.12.2020  
  + 30.07.2020  
  + 24.02.2020
  + 15.09.2020 (Archival directory)


---
**Description:**

**[Instructions on how to create UniProt dictionaries](#instructions-on-how-to-create-uniprot-dictionaries)**


**Directory contents:**

```bash
    <uniprot>
        |
<14-10-2022>
        |
        ├── human-genes-uniprot-ids.txt
        ├── uniprot-functional-data-from-table.json  <- this file is used by the acmg scripts # not needed from 02-03-2022
        ├── uniprot-functional-data.json  # not prepared in this resources version
        ├── uniprot-mutagenesis-data-from-table.json  <- this file is used by the acmg scripts
        ├── uniprot-mutagenesis-data.json # # not prepared in this resources version
        └── uniprot-table.tab
<In the next versions (after 22-11-2021)>
        |
        ├── uniprot-genes-ids.tsv
        ├── uniprot-genes.tsv
        ├── uniprot-functional-data-from-table.json  <- this file is used by the acmg scripts
        ├── uniprot-mutagenesis-data-from-table.json  <- this file is used by the acmg scripts
        └── uniprot-table.tab
```

---

### Instructions on how to create UniProt dictionaries  


**1.**  Update the docker (if needed):
1) Ensembl version update: you need to follow instructions under this link: https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/ensembl-genes
Check if there is the newest uniprot base version (base on the release date). If there is one follow the instruction from 2). 
Later you need to rebuild this docker (and increment the commented version) using the command (from root): `scripts/dockerbuilder -d resources/acmg/uniprot/Dockerfile`.
2) Uniprot version update: find the release date of the latest version (https://www.uniprot.org/) and change the value of UNIPROT_DATE in the Dockerfile.
Rebuild the docker (and increment commented version) using the command shown above in 1).
3) Uniprot preparing scripts update: change the script's version tag inside the docker. Rebuild the docker like in 1).

**2.**  Run the uniprot-resources docker and extract needed files from it (on anakin):
```bash
docker create -ti --name datasource intelliseqngs/uniprot-resources:1.1.0 .  
docker cp datasource:/resources/uniprot/. /data/public/intelliseqngs/workflows/resources/acmg/uniprot
docker rm -f datasource
```

**3.**  This leads to extraction of UniProt based files needed for ACMG annotation:
* uniprot-mutagenesis-data-from-table.json (used by: acmg-ps3.py)
If you updated the docker with the instructions given in point **1** you need to update the dockers and tasks using these scripts.

   
[Return to the table of contents](#table-of-contents)  
       
---

## gnomAD

**Last update date:** 24.02.2020

**Update requirements:** almost never

**History of updates:**
  + 24.02.2020
  + 15.07.2020 (Archival directory)  

---
**Description:**

**[Instructions on how to create gnomAD LOF dictionary](#instructions-on-how-to-create-gnomad-LOF-dictionary)**  


**Directory contents:**  
```bash
  <gnomad>
     |
    <v2>
     |
     ├── gnomad-lof-dictionary.json  <-file used for acmg pvs1 annotation
     ├── gnomad-lof-metrics.txt
     └── gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz
```   

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create gnomAD LOF dictionary  

**1.** Create folder for  the newest version of the database.

```bash
VERSION="v2"
GNOMAD_ACMG_DIR="/data/public/intelliseqngs/workflows/resources/acmg/gnomad"  
cd $GNOMAD_ACMG_DIR

mkdir $VERSION
cd $VERSION

```
**2.**  Download and modify gnomAD constraint metrics file.  
   
The downloaded file contains not unique gene names. Find and remove repeated genes and extract useful data   
(gene name, oe_lof, oe_lof_lower, oe_lof_upper from columns 1,24,29,30):   

```bash
wget 'https://storage.googleapis.com/gnomad-public/release/2.1.1/constraint/gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz'   
zcat gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz > gnomad-temp.txt  
diff <( tail -n +2 gnomad-temp.txt | cut -f 1 | sort ) <( tail -n +2 gnomad-temp.txt | cut -f 1 | sort | uniq ) | grep '<' | cut -f 2 -d ' ' > gnomad-repeated-genes.txt   
grep -w -v -f gnomad-repeated-genes.txt gnomad-temp.txt  | cut -f 1,24,29,30 | grep -v -w 'NA' | tail -n +2  > gnomad-lof-metrics.txt   
rm gnomad-repeated-genes.txt gnomad-temp.txt  

```

**3.** Create final gnomAD LOF dictionary needed for ACMG PVS1 annotation:  
* gnomad-lof-dictionary.json (used by: acmg-pvs1.py)  


**Required:** [**prepare-gnomad-lof-dictionary.py, version 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-gnomad-lof-dictionary.py)   

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-gnomad-lof-dictionary.py@0.0.1/src/main/scripts/resources-tools/prepare-gnomad-lof-dictionary.py   

python3 prepare-gnomad-lof-dictionary.py -i gnomad-lof-metrics.txt  -o gnomad-lof-dictionary.json  
```   
[Return to the table of contents](#table-of-contents)   
   
---
     
## Archival directory       
   
**Directory contents:**  
```bash  
├── clinvar.vcf
├── clinvar_ann.vcf
├── clinvar_gene_changes_dict.txt  <- old version of clinvar-protein-chenges-dictionary.json  
├── clinvar_gene_lof_dict.json  <- old version of clinvar-lof-dictionary.json  
├── clinvar_gene_ms_dict.json  <- old version of clinvar-ms-dictionary.json   
├── clinvar_pathogenic_sites.tab <- old version of clinvar-pathogenic-sites.tab  
├── gnomad_final.txt
├── gnomad_lof_dict.json  <- old version of gnomad-lof-dictionary.json  
├── gnomad_lof_metrics.txt
├── gnomad_populations.json
├── human_genes.txt <- old version of human-genes-uniprot-ids.txt  
├── readme.md
├── uniprot_functional_sites.json <- old version of uniprot-functional-data-from-table.json  
└── uniprot_mutagenesis.json <- old version of uniprot-mutagenesis-data-from-table.json  
```
   
[Return to the table of contents](#table-of-contents)   

---

