#name: civic_resources
#version: 1.1.0

FROM intelliseqngs/ensembl-genes-resources:1.2.0 as helper
FROM intelliseqngs/ubuntu-toolbox-20.04:2.0.7 as worker

ARG MONTH="10"
ARG YEAR="2022"

RUN apt update && \
    apt upgrade -y && \
    apt install -y unzip \
    libcurl4-openssl-dev \
    libssl-dev \
    software-properties-common \
    vcftools

RUN pip3 install CrossMap

#RUN yes | apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
#RUN yes | add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'

RUN apt update && \
    apt install -y r-base

RUN Rscript -e 'install.packages(c("BiocManager","dplyr", "tidyr", "argparse", "readr", "stringr", "limma", "jsonlite"))'
RUN Rscript -e 'BiocManager::install("limma")'


## resources
RUN mkdir /resources
COPY --from=helper /resources/ensembl-genes/ensembl-genes.tsv /resources/ensembl-genes.tsv
ADD resources/civic/ensembl75-transcripts.tsv /resources/ensembl75-transcripts.tsv
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa /resources/Homo_sapiens_assembly38.fa
ADD http://anakin.intelliseq.pl/public//intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai /resources/Homo_sapiens_assembly38.fa.fai
ADD http://anakin.intelliseq.pl/public//intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19/Homo_sapiens_assembly19.fa /resources/Homo_sapiens_assembly19.fa
ADD http://anakin.intelliseq.pl/public//intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19/Homo_sapiens_assembly19.fa.fai /resources/Homo_sapiens_assembly19.fa.fai
RUN wget -O /resources/hg19ToHg38.over.chain.gz  http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz


## scripts
ADD src/main/scripts/resources-tools/merge-civic-annotations.py /intelliseqtools/merge-civic-annotations.py
ADD resources/civic/civic_id.R /intelliseqtools/civic_id.R 
ADD resources/civic/civic.sh /intelliseqtools/civic.sh 
RUN chmod +x /intelliseqtools/civic.sh

## make resources
RUN /intelliseqtools/civic.sh ${MONTH} ${YEAR}


FROM ubuntu:20.04
COPY --from=worker /civic/output/ /civic/output/

