#!/bin/bash
set -e -o pipefail

VERSION="0.2.0"

MONTH=$1
YEAR=$2
DATE="01-$MONTH-$YEAR"
NEW_DATE=`echo $DATE | sed 's/-01-/-Jan-/' | sed 's/-02-/-Feb-/' | sed 's/-03-/-Mar-/' |
                       sed 's/-04-/-Apr-/' | sed 's/-05-/-May-/' | sed 's/-05-/-Jun-/' |
                       sed 's/-07-/-Jul-/' | sed 's/-08-/-Aug-/' | sed 's/-09-/-Sep-/' |
                       sed 's/-10-/-Oct-/' | sed 's/-11-/-Nov-/' | sed 's/-12-/-Dec-/'`
                    
mkdir -p /civic/output
cd /civic

wget https://civicdb.org/downloads/"$NEW_DATE"/"$NEW_DATE"-civic_accepted.vcf
wget https://civicdb.org/downloads/"$NEW_DATE"/"$NEW_DATE"-VariantSummaries.tsv
wget https://civicdb.org/downloads/"$NEW_DATE"/"$NEW_DATE"-GeneSummaries.tsv


## CIVIC VCF
## the vcf is not sorted and has no contigs in the header, chromosomes 1,2,3...
grep '^#' "$NEW_DATE"-civic_accepted.vcf | sed 's/ \"/\"/g' > tmp1.vcf
grep -v '^#' "$NEW_DATE"-civic_accepted.vcf | sort  -k1,1 -k2,2n >> tmp1.vcf
vcf-validator tmp1.vcf
bgzip tmp1.vcf
tabix -p vcf tmp1.vcf.gz

## normalize indels, remove sites not matching the reference (4), change chromosome names
for i in {1..22} X Y;do printf "$i\tchr$i\n" >> map.file;done
printf "MT\tchrM\n" >> map.file
bcftools norm -m -any -c x -f /resources/Homo_sapiens_assembly19.fa tmp1.vcf.gz \
| bcftools annotate --rename-chrs map.file  -o tmp2.vcf.gz -O z

## lift-over
CrossMap.py vcf /resources/hg19ToHg38.over.chain.gz tmp2.vcf.gz \
 /resources/Homo_sapiens_assembly38.fa tmp3.vcf


## remove contigs from header, change chromosomes order

grep -v '^##contig' tmp3.vcf | bgzip > tmp4.vcf.gz
tabix -p vcf tmp4.vcf.gz 
tabix -H tmp4.vcf.gz | bgzip > tmp5.vcf.gz
for i in {1..22} X Y M; do tabix   tmp4.vcf.gz chr"$i" | bgzip >> tmp5.vcf.gz;done
tabix -p vcf tmp5.vcf.gz

## normalize indels, check reference change CSQ field name
bcftools norm -m -any -c x -f /resources/Homo_sapiens_assembly38.fa tmp5.vcf.gz | \
 sed 's/\\x2E/./g' |\
 sed 's/\\x3A/:/g' |\
 sed 's/\\x3E/>/g' |\
 sed 's/\\x5F/_/g' |\
 sed 's/\\x2A/*/g' |\
   bgzip  > tmp6.vcf.gz
tabix -p vcf tmp6.vcf.gz


python3 /intelliseqtools/merge-civic-annotations.py -i tmp6.vcf.gz -o output/civic-hg38.vcf.gz
tabix -p vcf output/civic-hg38.vcf.gz

vcf-validator output/civic-hg38.vcf.gz

## TSV
## fix the tsv civic file a bit
paste \
  <( cut -f 1-27 "$NEW_DATE"-VariantSummaries.tsv ) \
  <(cut -f 28- "$NEW_DATE"-VariantSummaries.tsv  \
  | sed 's/\t/;/g' | rev | sed 's/;/\t/' | rev ) > "$NEW_DATE"-civic.tsv

## run Rscript
Rscript /intelliseqtools/civic_id.R --civic_tsv "$NEW_DATE"-civic.tsv \
 --civic_genes "$NEW_DATE"-GeneSummaries.tsv \
 --ensembl_genes /resources/ensembl-genes.tsv \
 --ensembl_grch37_75 /resources/ensembl75-transcripts.tsv \
 --outdir output

 cat  output/civic-snv-and-indels.json | jq 'reduce .[] as $m ({}; .[$m.gene] += [$m])' > output/civic.json



