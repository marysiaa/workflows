The location of resources:
  - docker `civic_resources`


## table of contents

- [CIViC](#civic)
  * [Description](#description)
  * [How to create](#how-to-create)
  * [Content of directory](#content-of-directory)


## CIViC

**Last update date:** 01-10-2022

**Update requirements:** Monthly 

**History of versions and updates:**
 - **version** 
   * 01-10-2022 => `civic_resources:1.0.0` and `civic_resources:1.1.0` (produces also civic gene list - oncogenes)
   * 01-04-2022
   * 01-05-2021
   * 01-04-2021 


---

### Description

The CIViC resoutces are based on CIViC VCF and TSV files (variants and genes).   
The VCF file contains curated information on cancer related variants for which ref and alt alleles can be provided. 
The TSV file contains additional information on "broad variants", for example truncating mutations of the BRCA1 gene, fusions.  

Both files used for annotation were downloaded from the [CIViC download site](https://civicdb.org/releases) 
The vcf fle was lifted-over to hg38 coordinates.  

### How to create (on anakin)  

To prepare the new resources build the `intelliseqngs/civic_resources` => change   the MONTH (for example "02") 
and YEAR (for example "2022") arguments, and docker version.
If needed, change also version of the `intelliseqngs/ensembl-genes-resources` docker (version 1.2.0 corresponds to Ensembl release 107).
Ready to use files are in the `/civic/output` directory, from where they can be copied to tasks' dockers (`task_vcf-anno-civic` or `task_vcf-sel-var-somatic`)

```bash
## build the docker

dockerbuilder -d resources.civic/Dockerfile

```

### Content of the docker output directory

```bash
civic
|
output (date)
|
├── civic-cnv.tsv                  ## file needed for CNV annotation - based on CIViV TSV (docker: not implemented yet)
├── civic-fusions.tsv              ## file needed for fusion annotation - based on CIViV TSV (docker: task_arriba-anno-civic)
├── civic.json                     ## file needed for civic annotation - based on CIViV TSV (docker: task_vcf-anno-civic)
├── civic-hg38.vcf.gz              ## file needed for civic annotation - based on CIViC VCF (docker: task_vcf-anno-civic) 
└── civic-hg38.vcf.gz.tbi          ## file needed for civic annotation - based on CIViC VCF (docker: task_vcf-anno-civic)

```

[Return to the table of contents](#table-of-contents)