#!/bin/bash

INTERVALS_AGILENT=/data/public/intelliseqngs/workflows/resources/intervals/agilent

gatk IntervalListTools \
       -ACTION CONCAT \
       -SORT true \
       -UNIQUE true \
       -I $INTERVALS_AGILENT/sureselect-human-all-exon-v6-r2/sureselect-human-all-exon-v6-r2.target.broad-institute-hg38.interval_list \
       -I $INTERVALS_AGILENT/sureselect-human-all-exon-v7/sureselect-human-all-exon-v7.target.broad-institute-hg38.interval_list \
       -I $INTERVALS_AGILENT/sureselect-human-all-exon-v8/sureselect-human-all-exon-v8.target.broad-institute-hg38.interval_list \
       -O new.interval_list \
       --PADDING 400
       
gatk IntervalListToBed \
       -I new.interval_list \
       -O $INTERVALS_AGILENT/v6-v7-v8-combined-padded/v6-v7-v8-combined-padded.bed



