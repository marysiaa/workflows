#  INTERVALS

Directory with various types of files containing genomic intervals.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/intervals**
    - on-line: http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/intervals
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/intervals**

---

## Table of Contents

**Intervals**
   * [Agilent](#agilent)
   * [Broad institute WGS calling regions](#broad-institute-wgs-calling-regions)
   * [UCSC genes](#ucsc-genes)
   * [UCSC, simple repeats](#ucsc-simple-repeats)
   * [Combined agilent to restrict freq annotations for WES](#v6-r2-v7-combined-padded)
   * [Pharmacogenomics](#pharmacogenomics)
   * [Polygenic](#polygenic)

[Return to: Resources](./../readme.md)

---

## Agilent

**Last update date:** 25-04-2022 (intervals for V8, re-done target and bait intervals for V7)

**Update requirements:** when data for a new kit is required

**List of kits**:

+ SureSelect Human All Exon V8 
+ SureSelect Human All Exon V7
+ SureSelect Clinical Research Exome V2
+ SureSelect Human All Exon V6, r2      
+ SureSelect Human All Exon V6+COSMIC, r2
+ SureSelect  Human All Exon V6+UTR, r2

---

**Description:**

This directory contains BED and interval_list files for various Agilent probe designs.
*_AllTracks.bed, *_Covered.bed, *_Padded.bed, *_Regions.bed, *_Targets.txt => all provided by producent
The rest of files => created by us to out purposes


[Instructions on how to create interval database for Agilent probe designs](#instructions-on-how-to-create-interval-database-for-agilent-probe-designs)

**Directory contents:**

```bash
├── <kit name>
│   ├── *_AllTracks.bed
│   ├── *_Covered.bed        => (+ 200bp padding) variant calling intervals
│   ├── *_Padded.bed
│   ├── *_Regions.bed        => target intervals
│   ├── *_MergedProbes.bed   => bait intervals
|   ├── *_Targets.txt 
│   ├── <kit name>.covered-padded-200.bed
│   ├── <kit name>.covered-padded-200.broad-institute-hg38.interval_list
│   ├── <kit name>.covered-padded-200.grch38-no-alt-analysis-set.interval_list
│   ├── <kit name>.bait.broad-institute-hg38.interval_list
│   ├── <kit name>.bait.grch38-no-alt-analysis-set.interval_list
│   ├── <kit name>.target.broad-institute-hg38.interval_list
│   └── <kit name>.target.grch38-no-alt-analysis-set.interval_list
(...)

```

 [Return to the table of contents](#table-of-contents)

---

### Instructions on how to create interval database for Agilent probe designs


**1.** Login to [Agilent SureDesign](https://earray.chem.agilent.com/suredesign/).  Navigate to "Find Designs" --> "SureSelect DNA" -->"Agilent Catalog" . Download files for selected kits - select hg38 genome.

 - **SureSelect Human All Exon V8** (S33266436) (hg38)
 - **SureSelect Human All Exon V7** (S31285117) (hg38)
 - **SureSelect Clinical Research Exome V2** (S30409818) (hg38)
 - **SureSelect Human All Exon V6+UTR r2** (S07604624) (hg38)
 - **SureSelect Human All Exon V6 r2** (S07604514) (hg38)
 - **SureSelect Human All Exon V6+COSMIC r2** (S07604715) (hg38)
 

```bash
AGILENT_INTERVALS="/data/public/intelliseqngs/workflows/resources/intervals/agilent"
cd $AGILENT_INTERVALS
```

**2.** Prepare intervals:

```bash
## Prepare target directory (for example sureselect-human-all-exon-v8)
mkdir sureselect-human-all-exon-v8
mv S33266436_hs_hg38.zip sureselect-human-all-exon-v
cd sureselect-human-all-exon-v8
unzip S33266436_hs_hg38.zi
rm S33266436_hs_hg38.zip

## Get script 
wget https://gitlab.com/intelliseq/workflows/raw/prepare-agilent-intervals.sh@0.0.1/resources/intervals/agilent/prepare-agilent-intervals.sh

##Run script (gatk and bedtools required)
./prepare-agilent-intervals.sh S33266436 #where S33266436 is the suffix from the zip file


## Remove some files
rm hg38.genome prepare-agilent-intervals.sh

```
 [Return to the table of contents](#table-of-contents)

---

## Broad institute WGS calling regions

**Last update date:** 22-08-2019

**Update requirements:** once per few months

**History of updates:**
  + 22-08-2019
---

**Description:**

BED and interval_files created using Broad Institute WGS calling regions downloaded from [Broad references Google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0). Interval files are generated only for Broad institute hg38 version of GRCh38 reference genome.


[Instructions on how to create interval database using Broad Institute WGS calling regions](#instructions-on-how-to-create-interval-database-using-broad-institute-wgs-calling-regions)

**Directory contents:**

```bash
<last update date>
  ├── hg38.even.handcurated.20k.bed
  ├── hg38.even.handcurated.20k.broad-institute-hg38.interval_list
  ├── hg38.even.handcurated.20k.intervals
  ├── wgs_calling_regions.hg38.interval_list
  └── wgs_evaluation_regions.hg38.interval_list
```

 [Return to the table of contents](#table-of-contents)

---
### Instructions on how to create interval database using Broad Institute WGS calling regions:

**1.**  Prepare the workspace.

```bash
BROAD_WGS_CALLING_REGIONS="/data/public/intelliseqngs/workflows/resources/intervals/broad-institute-wgs-calling-regions"
cd $BROAD_WGS_CALLING_REGIONS
```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="22-08-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.**  Download WGS calling regions from [Broad references Google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0)

```bash
wget -c https://storage.googleapis.com/broad-references/hg38/v0/wgs_calling_regions.hg38.interval_list
wget -c https://storage.googleapis.com/broad-references/hg38/v0/wgs_evaluation_regions.hg38.interval_list
wget -c https://storage.googleapis.com/broad-references/hg38/v0/hg38.even.handcurated.20k.intervals
```


**3.** Prepare reference genome dictionary (Broad institute hg38 version of GRCh38):

-   **Homo_sapiens_assembly38.dict** - reference genome, from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)  (see [Broad institute hg38](#broad-institute-hg38))

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.dict
```

**4** Create interval_file using  **hg38.even.handcurated.20k.intervals** file and Broad institute hg38 version of GRCh38 reference genome:

**Required:**  GATK4

```bash
cat hg38.even.handcurated.20k.intervals | sed "s/:/\t/g" | sed "s/-/\t/g" | awk '{print $1"\t"($2-1)"\t"$3}' > hg38.even.handcurated.20k.bed

gatk BedToIntervalList -SD Homo_sapiens_assembly38.dict -I hg38.even.handcurated.20k.bed -O hg38.even.handcurated.20k.broad-institute-hg38.interval_list
```

 [Return to the table of contents](#table-of-contents)

---

## UCSC genes

**Last update date:** 17-02-2020

**Update requirements:** every few months

**History of updates:**
  + 17-02-2020 (refGene.txt.gz date: 20-10-2019)
  + 22-08-2019 (refGene.txt.gz date: 28-07-2019)

---

**Description:**

This directory contains BED files and interval files containing regions of UCSC genes padded with 200bp. Theese files are created with the use of **refGene.txt.gz** downloaded from [UCSC hg38 site with databases](http://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/). Interval files are generated only for Broad institute hg38 version of GRCh38 reference genome.

[Instructions on how to create intervals database using UCSC genes regions](#instructions-on-how-to-create-intervals-database-using-ucsc-genes-regions)

**Directory contents:**

```bash
<last update date>
  ├── refGene.txt.gz
  ├── ucsc-genes-padded-200.bed
  └── ucsc-genes-padded-200.broad-institute-hg38.interval_list
```

 [Return to the table of contents](#table-of-contents)

---
### Instructions on how to create intervals database using UCSC genes regions:

**1.**  Prepare the workspace.

```bash
UCSC="/data/public/intelliseqngs/workflows/resources/intervals/ucsc-genes"
cd $UCSC
```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="17-02-2020"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.** Download **refGene.txt.gz** from [UCSC hg38 site with databases](http://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/):

```bash
wget -c http://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/refGene.txt.gz
```

**3.** Prepare reference genome dictionary (Broad institute hg38 version of GRCh38):

-   **Homo_sapiens_assembly38.dict** - reference genome, from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)  (see [Broad institute hg38](#broad-institute-hg38))

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.dict
```

```bash
cat Homo_sapiens_assembly38.dict | grep "^@SQ" | cut -f 2,3 | grep -v "HLA" | sed "s/SN://g" | sed "s/LN://g" > Homo_sapiens_assembly38.genome
```

**4.** Extract genes coordinates from refGene.txt.gz, merge them and extend resulting intervals 200bp at each side. Remove intervals from *_alt and *_fix chromosomes:

**Required:**  bedtools

```bash
zcat refGene.txt.gz | cut -f 3,5,6,13 | sort -t $'\t' -k1,1 -k2,2n | grep -v "_fix" | grep -v "_alt" | bedtools merge -c 4 -o distinct -i - | bedtools slop -b 200 -g Homo_sapiens_assembly38.dict -i - > ucsc-genes-padded-200.bed
```

**5.** Create interval_files using  **ucsc-genes-padded-200.bed** file and Broad institute hg38 version of GRCh38 reference genome::

**Required:**  GATK4

```bash
gatk BedToIntervalList -SD Homo_sapiens_assembly38.dict -I ucsc-genes-padded-200.bed -O ucsc-genes-padded-200.broad-institute-hg38.interval_list
```

**6.** Cleanup:

```bash
rm Homo*
```

 [Return to the table of contents](#table-of-contents)

---

## UCSC, simple repeats

**Last update date:** 28-11-2019

**Update requirements:** every few months

**History of updates:**
  + 28-11-2019 (refGene.txt.gz date: 11-03-2019)

---

**Description:**

This directory contains bgzipped BED file containing simple repeats regions created with the use of  **simpleRepeat.txt.gz** downloaded from [UCSC hg38 genome database](http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/). The file contains repeats with unit length from 1 to >1000 and was made with [Tandem Repeats Finder](http://tandem.bu.edu/trf/trf.html).  Additionally, it contains a directory with full-genome and chromosome-wise BEDs containing simple repeats regions with repeated units not exceeding 6bp.

[Instructions on how to create interval files from UCSC simple repeats regions](#instructions-on-how-to-create-interval-files-from-ucsc-simple-repeats-regions)

**Directory contents:**

```bash
<last update date>
├── 6bp-units-or-less
│   ├── chr1.simple-repeats-6bp-or-less.bed.gz
│   ├── chr1.simple-repeats-6bp-or-less.bed.gz.tbi
│   ├── chr2.simple-repeats-6bp-or-less.bed.gz
│   ├── chr2.simple-repeats-6bp-or-less.bed.gz.tbi
│ (...)
│   ├── chrX.simple-repeats-6bp-or-less.bed.gz
│   ├── chrX.simple-repeats-6bp-or-less.bed.gz.tbi
│   ├── chrY-and-the-rest.simple-repeats-6bp-or-less.bed.gz
│   ├── chrY-and-the-rest.simple-repeats-6bp-or-less.bed.gz.tbi
│   ├── simple-repeats-6bp-or-less.bed.gz
│   └── simple-repeats-6bp-or-less.bed.gz.tbi
├── simpleRepeat.bed.gz
├── simpleRepeat.bed.gz.tbi
└── simpleRepeat.txt.gz

```

 [Return to the table of contents](#table-of-contents)

---
### Instructions on how to create interval files from UCSC simple repeats regions:

**1.**  Prepare the workspace.

```bash
UCSC_SIMPLE_REPEATS="/data/public/intelliseqngs/workflows/resources/intervals/ucsc-simple-repeats"
cd $UCSC_SIMPLE_REPEATS
```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="28-11-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```


**2.** Download **simpleRepeat.txt.gz** from [UCSC hg38 genome database](http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/). The file contains repeats with unit length from 1 to >1000 and was made with [Tandem Repeats Finder](http://tandem.bu.edu/trf/trf.html).

```bash
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/simpleRepeat.txt.gz
```

**3**.  Create chromosome-wise beds restricted to repeats consisting of units of 6bp or less:

```bash
mkdir 6bp-units-or-less

zcat simpleRepeat.txt.gz | awk '($6 <= 6)' | awk -v OFS='\t' '{print $2,$3,$4}' | bgzip > 6bp-units-or-less/simple-repeats-6bp-or-less.bed.gz

cd 6bp-units-or-less

zcat simple-repeats-6bp-or-less.bed | awk  'BEGIN { for (i = 1; i <= 22; i++) { chr[i] = "chr"i } chr["X"] = "chrX"; chr["Y-and-the-rest"] = "chrY-and-the-rest"} {if (match($1, "^chr[0-9X]{1}[0-9]{0,1}$")) { file_name = $1".simple-repeats-6bp-or-less.bed.gz"; print $0 | "bgzip >> "file_name } else {file_name = "chrY-and-the-rest.simple-repeats-6bp-or-less.bed.gz"; print $0 | "bgzip >> "file_name}}'

tabix -p bed simple-repeats-6bp-or-less.bed.gz
parallel -j 24 tabix -p bed ::: chr*.simple-repeats-6bp-or-less.bed.gz
```

## v6-r2-v7-combined-padded and v6-v7-v8-combined-padded


**Update date date:** 26.04.2022

**Update requirements: with new kit

### Description

Intervals for 3 kits described above (agilent v8, v7 and agilent v6-r2), combined and padded, often used in WES analyses. Needed to restrict frequencies data for genome (https://gitlab.com/intelliseq/workflows/-/blob/readme@1.0.1/resources/snps-and-indels-annotations/frequencies/readme.md#merged-frequencies-genome) to remove no-coding regions and have more memory space, smaller files. Needed for task vcf-anno-freq.wdl and vcf-anno-gnomadcov.wdl

### How were created

```
wget https://gitlab.com/intelliseq/workflows/-/raw/agilent-combined-padded@0.0.2/resources/intervals/agilent-combined-padded/run.sh
bash run.sh

```

## Pharmacogenomics

**Last update date:** 22-02-2021

**History of updates:**
   4-02-2021 (dir: aldy_astrolabe_pharmcat_cyrius - intervals with genes for tools: astrolabe, pharmcat, aldy, cyrius)
   22-02-2012 (dir: pharmcat_astrolabe - intervals with genes for anly pharmcat and astrolabe - only for them gvcf is needed)

location on anakin: ``/data/public/intelliseqngs/workflows/resources/intervals/pgx``

Based on base created by hand from here:
https://docs.google.com/spreadsheets/d/1KhSMf18fcbBrX1CO_8bREGTFUZTLVDbOzmAGLyxPbMU/edit?usp=sharing

1. Download tsv only with chr, start, stop
2. name as .bed
3. ``wget https://gitlab.com/intelliseq/workflows/-/raw/bed-to-interval-list@1.0.1/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl``
4. Edit ~/workflows/resources/intervals/pgx/bed-to-interval-list.json to have your bed in inputs
5. ``cromwell run -i ~/workflows/resources/intervals/pgx/bed-to-interval-list.json bed-to-interval-list.wdl``


 [Return to the table of contents](#table-of-contents)

## Polygenic

**Last update date:** 09.09.2021

### History of updates

**Last update date:** 09-09-2021

**History of versions and updates:**
 - **version** (09-09-2021)


### How to create

Based on the file `/data/public/intelliseqngs/workflows/resources/intervals/broad-institute-wgs-calling-regions/wgs_calling_regions.hg38.interval_list`, 
a `phasing_wgs_calling_regions.bed` file is created, which is later divided into even smaller fragments (4 million each).

```
< /data/public/intelliseqngs/workflows/resources/intervals/broad-institute-wgs-calling-regions/wgs_calling_regions.hg38.interval_list grep -v @ | sed 's/chr//g' | awk '{print $1":"$2"-"$3}' | grep -v Y > phasing_wgs_calling_regions.bed

FILENAME='phasing_wgs_calling_regions.bed'
while read -r LINE; do
  SPLITTED_FOR_COLON=(${LINE//:/ })
  CHROMOSOME=${SPLITTED_FOR_COLON[0]}
  RANGE=${SPLITTED_FOR_COLON[1]}
  SPLITTED_FOR_DASH=(${RANGE//-/ })
  SUBTRACTION=$((SPLITTED_FOR_DASH[1] - SPLITTED_FOR_DASH[0]))

  if (( SUBTRACTION > 4000000 )); then
    (( MAX = SPLITTED_FOR_DASH[1] ))
    (( NUM = SPLITTED_FOR_DASH[0] ))
    (( COUNT = 0 ))
    while (( MAX >= NUM )); do
        if (( COUNT > 0 )); then
          (( PREV = NUM + 1 ))
          (( NUM = PREV + 3999999 ))
        else
          (( PREV = NUM ))
          (( NUM = PREV + 3999999 ))
        fi
        if (( NUM >= MAX )); then
          echo "$CHROMOSOME":"$PREV""-""$MAX" >> phasing.bed
        else
          echo "$CHROMOSOME":"$PREV""-""$NUM" >> phasing.bed
          (( COUNT++ ))
        fi
    done
  else
    echo "$CHROMOSOME":"${SPLITTED_FOR_DASH[0]}""-""${SPLITTED_FOR_DASH[1]}" >> phasing.bed
  fi
done < $FILENAME
```

The final `phasing.bed` file is used in the vcf-imputing module.

[Return to the table of contents](#table-of-contents)
