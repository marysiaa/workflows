#!/bin/bash

set -e -o pipefail

NAME=$( dirname $(realpath $0) | sed 's|.*/||g' )

hg38="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38"
grch38="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set"

## prepare file with contigs lengths (one version is enougth)
cat "$hg38".dict | grep "^@SQ" | cut -f 2,3 | grep -v "HLA" | sed "s/SN://g" | sed "s/LN://g" > hg38.genome

## remove chr22_KI270879v1_alt regions
for i in *.bed; do name=$(basename $i | sed 's/\.bed/-no-alt.bed/'); grep -v '^chr22_KI270879v1_alt' $i > $name; mv $name $i; done

## prepare padding covered bed
bedtools slop -b 200 -i "$1"_Covered.bed -g hg38.genome > "$NAME".covered-padded-200.bed

## prepare intervals for both versions of the reference genome (with and without alts)

## variant calling
gatk BedToIntervalList \
    -SD "$hg38".dict \
    -I "$NAME".covered-padded-200.bed \
    -O "$NAME".covered-padded-200.broad-institute-hg38.interval_list

gatk BedToIntervalList \
    -SD "$grch38".dict \
    -I "$NAME".covered-padded-200.bed \
    -O "$NAME".covered-padded-200.grch38-no-alt-analysis-set.interval_list
  
## target  
gatk BedToIntervalList \
     -SD "$hg38".dict \
     -I "$1"_Regions.bed \
     -O "$NAME".target.broad-institute-hg38.interval_list

gatk BedToIntervalList \
     -SD "$grch38".dict \
     -I "$1"_Regions.bed \
     -O "$NAME".target.grch38-no-alt-analysis-set.interval_list

## bait
merged_probes_bed=$(ls "$1"_Merged[pP]robes.bed)
gatk BedToIntervalList \
    -SD "$grch38".dict \
    -I "$merged_probes_bed" \
    -O "$NAME".bait.grch38-no-alt-analysis-set.interval_list

gatk BedToIntervalList \
    -SD "$hg38".dict \
    -I "$merged_probes_bed" \
    -O "$NAME".bait.broad-institute-hg38.interval_list
 
