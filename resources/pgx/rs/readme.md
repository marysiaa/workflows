## Ummar RS

The `rs_Ummar.tsv` file contains a list of genes and a list of rs.

File header:

| Gene | RS |
|------|----|
| 12q15 | rs7297610 |
| 4q25 | rs2200733 |
| 9p21 | rs10757278 |
| 9p21 | rs1333049 |
| ABCB1 | rs1045642 |

Genes in the file:

NAT2, SLC47A2, 9p21, NUDT15, BDNF, ABCG2, CACNA1C, F13A1, GRIK4, TPMT, CYP1A2, OPRM1, ANKK1/DRD2, CYP2C8, ADH1B, DPYD, CYP2B6, CYP2C19, HTR2A, IFNL3/IFNL4, MT-RNR1, MC4R, DRD2, 12q15, VKORC1, DBH, CACNA1S, BCHE, F2, LPA, CBR3, SLC28A3, CYP3A5, UGT1A6, ALDH2, APOE, CYP2C9, ANK3, HTR2C, GRIK1, OPRK1, NOS3, CYP2C, RYR1, F5, GRIN2B, ADRB2, HLA-B, CYP3A4, COMT, ITGB3, SLC6A4, UGT2B15, OPRD1, CYP2D6, HLA-A, MTHFR, RARG, UGT1A1, FKBP5, CEP72, 4q25, ACYP2, HCP5/HLA-B, ABCB1, ADRA2A, SLCO1B1, G6PD, ATM/C11orf65, CYP4F2

------

## Genes encoding

Dictionary to convert genotype to encoding to use query in openpgx.


| Gene   | RS        | GT  | Encoding                     |
|--------|-----------|-----|------------------------------|
| F5     | rs6025    | T/T | Factor V Leiden homozygous   |
| F5     | rs6025    | A/A | Factor V Leiden homozygous   |
| F5     | rs6025    | C/T | Factor V Leiden heterozygous |
| F5     | rs6025    | T/C | Factor V Leiden heterozygous |
| F5     | rs6025    | C/A | Factor V Leiden heterozygous |
| F5     | rs6025    | A/C | Factor V Leiden heterozygous |
| VKORC1 | rs9923231 | C/C | rs9923231 reference (C)      |
| VKORC1 | rs9923231 | T/T | rs9923231 variant (T)        |
| VKORC1 | rs9923231 | C/T | rs9923231 variant (T)        |
| VKORC1 | rs9923231 | T/C | rs9923231 variant (T)        |

Genes in the file:

F5, VKORC1

------