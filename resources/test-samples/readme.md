Sample: 193_wes

Intelliseq Flow
http://genetraps.intelliseq.pl:8085/sample/c77c3c63-cec7-4173-bc1d-a8868008759d

Google Cloud 
gs://iseq/samples/c77c3c63-cec7-4173-bc1d-a8868008759d/WES_14_DHE07417-17_HJLYWALXX_L4_1.fq.gz
gs://iseq/samples/c77c3c63-cec7-4173-bc1d-a8868008759d/WES_14_DHE07417-17_HJLYWALXX_L4_2.fq.gz

Sample: 189_wes
Opis przypadku: Rozpoznanie dotyczy pacjenta z postepujacym oslabieniem i zanikiem miesni. 
Poprzednio znaleziono: DYSF - possible compund heterozygote 
Kit: exome-v6 
Dane do panelu: 
    HPO: Muscular dystrophy HP:0003560


Intelliseq Flow
http://genetraps.intelliseq.pl:8085/sample/ca60c1b6-85f8-415c-a348-5822e7a1e423

Google Cloud:
gs://iseq/samples/ca60c1b6-85f8-415c-a348-5822e7a1e423/FAP_DHE07415-13_HJLYWALXX_L8_1.fq.gz
gs://iseq/samples/ca60c1b6-85f8-415c-a348-5822e7a1e423/FAP_DHE07415-13_HJLYWALXX_L8_2.fq.gz