# Intelliseq Flow<sup>TM</sup> Workflows

Public repository for wdl formatted workflows for NGS analysis. License: contact klaudia@intelliseq.pl

## Table of contents
- [General Information](#general-information)
- [Workflow validation](#workflow-validation)
- [Developer documentation](#developer-documentation)
  * [Data sources](#data-sources)
  * [Conventions](#conventions)
  * [Workflow description documentation](#workflow-description-documentation)
  * [TASK DEVELOPMENT](#task-development)
    + [1. Creating new task](#1-creating-new-task)
    + [2. Task development](#2-task-development)
      - [Task version control](#task-version-control)
    + [3. Task testing](#3-task-testing)
      - [Creating test](#creating-test)
      - [Inputs for the task - Test data](#inputs-for-the-task---test-data)
      - [How to run test for specific task](#how-to-run-test-for-specific-task)
    + [3. Developing docker images](#3-developing-docker-images)
      - [Docker](#docker)
      - [New Dockerfile](#new-dockerfile)
      - [Changes in existing docker images ***-it is better to avoid this practice***](#changes-in-existing-docker-images-----it-is-better-to-avoid-this-practice---)
      - [Build docker images](#build-docker-images)
      - [Name of the docker image vs localisation of its dockerfile](#name-of-the-docker-image-vs-localisation-of-its-dockerfile)
      - [Version control](#version-control)
      - [Import](#import)
      - [Building docker environment](#building-docker-environment)
    + [4. Developing scripts (Python, Bash, R, jinja templates) used in WDL commandline](#4-developing-scripts--python--bash--r--jinja-templates--used-in-wdl-commandline)
      - [General information](#general-information)
        * [Reports generated automatically (jinja)](#reports-generated-automatically--jinja-)
      - [Testing](#testing)
        * [Unit tests for python scripts](#unit-tests-for-python-scripts)
        * [Testing scripts](#testing-scripts)
  * [MODULE DEVELOPMENT](#module-development)
    + [1. Creating new module](#1-creating-new-module)
    + [2. Importing tasks in module](#2-importing-tasks-in-module)
    + [3. Testing](#3-testing)
  * [PIPELINE DEVELOPMENT](#pipeline-development)
    + [1. Creating new pipeline](#1-creating-new-pipeline)
    + [2. Importing tasks and modules in pipeline](#2-importing-tasks-and-modules-in-pipeline)
    + [3. Testing](#3-testing-1)
  * [ADDITIONAL INFORMATION](#additional-information)
    + [wdl metadata validation](#wdl-metadata-validation)
    + [Parsing meta to json](#parsing-meta-to-json)
    + [Parsing google spreadsheet to meta](#parsing-google-spreadsheet-to-meta)
    + [Testing on google cloud](#testing-on-google-cloud)
    + [Testing on aws](#testing-on-aws)
    + [Versioning](#versioning)
    + [Bioobjects (BCO)](#bioobjects--bco-)
- [Developer tips (also for dummies)](#developer-tips--also-for-dummies-)
  * [How to run jupyter lab on the server](#how-to-run-jupyter-lab-on-the-server)
  * [Easy working in docker environment](#easy-working-in-docker-environment)
  * [Create Python local environment](#create-python-local-environment)
  * [Tagging commits](#tagging-commits)
  * [Keyboard shortcuts](#keyboard-shortcuts)
  * [Usefull links](#usefull-links)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## General Information
Intelliseq Flow<sup>TM</sup> workflows is a portfolio of pipelines for next genetic data analysis. Pipelines were developed with focus on:
* reproducibility - all numbered versions are fixed and have 2 year guarantee of reproducibility
* portability - all pipelines can be run on single PC, private cloud, Google cloud and AWS without data source preparation
* excellence - pipelines implement all industry standards like GATK best practices, ACMG recommendations, bioobject specification and CE certification
* quality assurance - thoroughly tested, rich quality-check at fastq, bam and vcf levels
* workforce reduction - integration with LIMS, automatic reporting
* affordable price - integration with existing LIMS systems instead of developing our own LIS reduces costs of development

List of workflows include:
* Intelliseq Flow<sup>TM</sup> germline - targeted (exome or panel) sequencing analysis for germline mutations

    Intelliseq Flow<sup>TM</sup> germline is a software pipeline processing and analyzing raw human genomic data obtained using next-generation sequencing (NGS) technology. 
The pipeline is dedicated to targeted (exome or custom panel) sequencing analysis of germline mutations. The developed tools automatically identify and annotate genetic 
variants based on the comparison with the human reference genome, and publicly available databases. The software supports decision-making in diagnostics of monogenic and 
oligogenic disorders (mainly rare diseases and syndromes) by providing high-quality genome interpretation reports. The reports are intended to assist a physician, specializing 
in clinical genetics, in taking further diagnostic steps. The analysis report also contains a list of medical recommendations following the ACMG (American College of Medical 
Genetics) guidelines regarding variants and mutations identified by the software, that are potentially related to the patient's phenotype.

## Workflow validation
There are 3 methods used to validate Intelliseq FlowTM germline workflow. Methods described in the table. 

| Validation Method                                                                                                                                                               | Results                                                                                                                                                                                                                                                                                                     |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| The automatic analysis using the IntelliseqFlow Germline of genetic data modified in silico through changes of DNA sequences by the implementation of 50 SNP polymorphisms (identified using Rs ID). | The intelliseqFlow Germline software is expected to detect no less than 85% of the polymorphisms (SNPs) introduced into the data.                                                         |
| The comparison of automatic analysis using the IntelliseqFlow Germline with manual analysis (all the tools will be used manually by bioinformatician). Both types of analysis will be performed on the same NGS sequencing results obtained from 20 individuals.  |The IntelliseqFlow Germline software is expected to produce similar results to manual analysis (not less than 85% compliance).|
| The validation of the implemented variant classification method according to ACMG (American College of Medical Genetics) recommendations by the InteliseqFlow Germline in comparison to classification obtained from the ClinVar database. | The IntelliseqFlow Germline software is expected to incorrectly classify no more than 15% of the tested variants (1000 or more variants will be tested). Incorrect classification is defined as the variant classified by ClinVar as pathogenic or likely pathogenic will be classified as benign or likely benign by the software, and vice versa). |

## Developer documentation

### Data sources 
Instructions about how to maintain resources, where store resources, where store scripts needed to prepare resources and version care on this link:

https://gitlab.com/intelliseq/workflows/-/tree/dev/resources

Inside resources section some resources were already created and be careful to not create them one more time. Information is only to know HOW particular resource were created. If you need to update some resource to newer version use this instruction.

Note that before placing file in resources folder described in above link is necessary to prepare (both on repository and server): 
- separated readme.md about this file and folder if do not exists on the server and on dev branch in folders (check if somebody already created folder for this kind of resources),
- update readme and put your new folders in tree "The structure of this directory" inside resources readme, 
- if any code necessary to prepare, parse, download or process data was used place it inside src/main/resources/resources-tools/
- to save environment for later users which was used to run code to generate/parse/modify resource it can be build in docker image. 
  Docker should be build using script scripts/docker-build and Dockerfile should be placed in main/docker/name_of_script. Dockerfile should have line ENTRYPOINT to run script automaticaly while running docker image. 

Usefull to maintain code needed to prepare resource: Makefile https://learnxinyminutes.com/docs/make/
Usefull to see graph: https://dreampuf.github.io/GraphvizOnline/#digraph%20G%20%7B%0A%7D
Example of Makefile on repo: scripts/test/genotype-gvcf/Makefile

### Conventions

. (dots) in any file name are only used in extensions (.jpg, .txt, .json etc.). 

Working on server: outputs of the scripts/tests should be located in /tmp folder. If some output will be needed later move it to another directory. 

Development should be performed in separate branch and merge to dev using pull request. Before merge request remember to test changes.

Software is developed in tha "latest" versions on the dev branch. After merge from dev to master version is changed: latest => x.x.x (by marpiech). 
### Workflow description documentation 

The application consists of ***pipeline***, ***modules*** and ***tasks***. The instruction how to develop each of them is described below and contains obligatory steps. 
Workflow bases on cromwell data processing tool (official documentation is herein: https://github.com/openwdl/wdl/blob/master/versions/1.0/SPEC.md). 
Keep in mind that only TASK DEVELOPMENT section contains information about developing docker images and developing scripts.
Modules import tasks and execute them in their wdl commandline and pipelines import modules/tasks and execute them in wdl commandline therefore modules and pipelines do not need docker images. 

### TASK DEVELOPMENT

Development should be performed according to the list in task template on trello: https://trello.com/c/ajBSn9Cx/238-task-template-estimated-time-in-hours

Details below:

#### 1. Creating new task 

***task-name*** should:
- have up to 20 characters,
- be confirmed by marpiech,
- have "-" insted of "_",

***outputs names***
- Every output should have name according to convention: ```samplename_output-name.extension1(.extension2)```

***Note!*** naming rules should be applied on modules and pipelines also.

**To create task always use script:**

```
scripts/create-task --name task-name
```

***How does it work?***

Script creates wdl and test files based on git user.name and task name.\
`create-task` creates proper directories in `scr/main/wdl/tasks/task-name/` and `src/test/wdl/tasks/task-name/` and puts there:

 * WDL scripts: 
 `src/main/wdl/tasks/task-name/task-name.wdl`
 * Script to run test for task-name task:
 `src/test/wdl/tasks/task-name/test.sh `
 * Test for task-name task:
 `src/main/wdl/tasks/test-test/test.json`

Additionally, `src/test/wdl/tasks/task-name/` is symlinked to `src/main/wdl/tasks/task-name/` directory.


#### 2. Task development

Developing task should be on seperate branch, until is finished. 

*Note: if task does not exists yet on the dev branch script "before finish" does not work (https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/before-finish.sh)*

When development is ready solve merge conflicts with dev and make merge request and choose reviewer. 

You can remove unncesessary branch, but is not obligatory. Branches that are not used for long time are deleted automaticly by admin.  

##### Task version control
Analogically to [Versioning](#versioning)

#### 3. Task testing

##### Creating test

Tests are created automaticaly as described above in step 1 - script `scripts/create-task` produces files `test.sh` and `test.json` from test template. `test.sh` is the same for each task and should not be changed. It runs test, which is configured in `test.json` file.

##### Inputs for the task - Test data

* **Small files** (max 400 KB) for testing are stored at `$PROJECT_DIR/src/test/resources/data`
* **Large files** for testing are stored at `/data/test/large-data/` at `anakin` and should be symlinked from root `workflows` directory as:
	```
	ln -s /data/test/large-data src/test/resources/large-data
	```
Paths to files in `test.json` should begin with `${ROOTDIR}/src/test/resources/data/`.

***Note!: Is forbidden to push files test.json if paths are invalid, not prepared or files in path does not exists in repository or symlink.***

##### How to run test for specific task
To run the test use command:
`src/test/wdl/tasks/task-name/latest/test.sh`
For newer tasks there is no `latest` folder:
`src/test/wdl/tasks/task-name/test.sh`

This test checks the following things:
* if bioobject exists
* if bioobject contains all domains and they are not empty
* if stdout file exists

You have to add your own tests. Remember to check output correctness, not its stability. 

More on testing here: https://gitlab.com/intelliseq/workflows/tree/dev/src/test/wdl

#### 4. Developing scripts (Python, Bash, R, jinja templates) used in WDL commandline 

##### General information

It is possible to paste code inside WLD commandline, but more complicated code should be placed inside separated script which will be inside dockerimage. 

* Avoid writing large and complicated code (especially in Python) in the WDL. However, if you want to place some ***short and easy*** Python code in the WDL, it should be done this way: 
```
  ## Python script START ##

  python3 (> output) <<EOF
  (kod)
  EOF

 ## Python script END ##
```
* The scripts should be placed in:
```src/main/scripts/script-dir```
 
	```script-dir``` is the name of the directory that defines the filed for which the script was created. For example, the scripts for ACMG recommendations can be found in ```src/main/scripts/acmg```.  If you do not have any field for your script, just put it in: ```src/main/scripts/tools```
* help section should be implemented in every script, you can use templates:
	* bash: (Kasia - template)
	* Python: [https://gitlab.com/intelliseq/workflows/raw/dev/docs/templates/python_script_template.py](https://gitlab.com/intelliseq/workflows/raw/dev/docs/templates/python_script_template.py)
	* R: [https://gitlab.com/intelliseq/workflows/-/raw/dev/docs/templates/R-script-template.R](https://gitlab.com/intelliseq/workflows/-/raw/dev/docs/templates/R-script-template.R)

###### Reports generated automatically (jinja)

More information about our templates is here: https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/reports/readme.md

##### Testing

###### Unit tests for python scripts
The tests should be placed in `src/test/unit/python` directory. Unit tests for one file should be placed in one subdirectory.
Tests for each function/class should be put in a separate file in the directory. 
Test name as well name of the file containing tests should begin with `test_` or end with `_test`.
You can run all unit tests with the following command:
```bash
pytest src/test/unit/python
```
Unittests will be run after each `git push`.

You can check test coverage for code from given directory using a command similar to this one:
```bash
pytest --cov-report term-missing --cov docs/templates/ src/test/unit/python/
```

Some tips:
- pytest library detects file as a test if name of the file begins with prefix ```_test``` or ```test_```,
- unitest should test simple components, for example one function,
- good practice is to define requirements for the function before creating it,
- pytest documentation: https://docs.pytest.org/en/latest/contents.html


###### Testing scripts
Each script should have a test. Obligatory component of the test is line with run the code and output saved in /tmp folder. 

* The test name should be: ```<script-name>-test.sh```
* The test can be found in relevant directory in: ```src/test/scripts```
For example:
	* script: ```src/main/scripts/wdltools/wdltools.sh```
	* test: ```src/test/scripts/wdltools/wdltools-test.sh```

* Test powinien być uruchamialny z każdego katalogu i powinien pokazywać jak uruchomić dany skrypt. Nie musi niczego sprawdzać. Można to zrobić tak jak poniżej:

**PRZYKŁADOWY TEMPLATE TESTU DO SKRYPTU PYTHONOWEGO**
```bash
#!/bin/bash 
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

$SCRIPT plot --wdl "$PROJECT_DIR/src/main/wdl/modules/coverage-statistics/latest/coverage-statistics.wdl"
```

### MODULE DEVELOPMENT

#### 1. Creating new module
TODO: script create-module

***Module name should:***
- have up to 20 characters,
- be confirmed by marpiech,
- have "-" insted of "_",

***output files names***
- Every output file should have name according to convention: ```samplename_output-name.extension1(.extension2)```


#### 2. Importing tasks in module

- Construction of tasks import with "_task" in postfix:
```
import "https://.../task-name.wdl" as task_name_task
```
#### 3. Testing

The same rules as for tasks:
 
[3. Task testing](#3-task-testing)


### PIPELINE DEVELOPMENT
#### 1. Creating new pipeline
TODO: script create-pipeline

Pipeline name should:
- have up to 20 characters,
- be confirmed by marpiech,
- have "-" insted of "_",
#### 2. Importing tasks and modules in pipeline

- Contruction of **tasks** import with "_task" in postfix:
```
import "https://.../task-name.wdl" as task_name_task

```
- Contruction of **modules** import with "_module" in postfix:
```
import "https://.../task-name.wdl" as task_name_module

```

#### 3. Testing
The same rules as for tasks:
 
[3. Task testing](#3-task-testing)


### ADDITIONAL INFORMATION

#### wdl metadata validation

# Script meta-validator

Visit: 

```
https://workflows-dev-documentation.readthedocs.io/en/latest/Developer%20tools.html#validating-meta

```


How to validate .wdl's meta description
1. Open your web browser and go to: https://prod.api-wdl.flow.intelliseq.com/wdl/parse?url=https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/pipelines/target-seq/target-seq.wdl
You can also use path to wdl with tags:
https://prod.api-wdl.flow.intelliseq.com/wdl/parse?url=https://gitlab.com/intelliseq/workflows/raw/target-seq@1.1.7/src/main/wdl/pipelines/target-seq/latest/target-seq.wdl
2. There are two possible outcomes:
a.) Error 500 with two possibilities:
- java.io.FileNotFoundException - which means that provided address was incorrect
- com.fasterxml.jackson.databind.JsonMappingException: YAML decoding problem - which means that chosen .wdl file is wrongly formatted
b.) JSON file containing parsed meta of .wdl file

### Parsing meta to json

Creating meta.json from meta that already are inside wdl code in "meta" section:

``python3 scripts/meta/parse_meta.py -p PATH_TO_YOUR_WDL ``

1. Check all fields if looks properly
2. Check code: are there any Boolean, Int or Float used as String? Please, change this to proper data type in code, because script changes data type for proper in json format (for expamle "12" -> 12, "13.2" -> 13.2, "true" -> true, "True" -> true). There are two possibilities: 
- change unproper type in code
- change type in meta.json
2. Check if works in interface 

------------------------------------

Creagin meta.json from wdl code, that does not have meta section 

``python3 scripts/meta/create_meta.py -p PATH_TO_YOUR_WDL > new_meta.json``

To create in description mode use flad -d like this:

``python3 scripts/create_meta.py -p PATH_TO_YOUR_WDL -d > new_meta.json``

For each type of input there are obligatory fields (the same as described in documentation: https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/wdl/tasks/meta-test/meta-test.wdl

All obligatory fields appear in meta.json. Please make following steps: 
1. Fill "description", "tag", "price", "AUTHOR"
2. Group fields by: variant_1, variant_2 if needed - there are some example fields. 
3. Fill not obligatory fields for specific types (if needed): - groupname, group, required ( nie da się tego wyciągnąć z wdl, ponieważ jest to niespójne z ? albo = ) (true/false), "advanced" (true/false), "multiselect" (true/false), "hidden", "constraints" (values = [])
4. Check all fields if looks properly
5. Check code: are there any Boolean, Int or Float used as String? Please, change this to proper data type in code, because script changes data type for proper in json format (for expamle "12" -> 12, "13.2" -> 13.2, "true" -> true, "True" -> true). There are two possibilities: 
- change unproper type in code (preferred)
- change type in meta.json 
6. Check if works in interface 



### Parsing google spreadsheet to meta
Before you start make sure you are using account with intelliseq domain!
  1. Firstly, you need to generate your own credentials.json file.
  2. Go to the google dev console site https://console.cloud.google.com and create a new project.
  3. To do this you need to look at the top of site and find current chosen project with an arrow pointing down.
  4. After clicking it you should see pop up window containing "new project" button.
  5. For the moment we don't have any naming convention for it so name it as you wish to.
  6. Click create and then select this new project. You can do this in the same place as at the beginning (current chosen project at the top of the site).
  7. Choose "APIs & Services" which is on the left of site.   
  8. Choose "Enable APIs and Services" option when you selected your project.
  9. Search for "Google Drive API" and "Google Sheets API" in the search bar located at the top of the site.
  10. Enable both after clicking each of them. Now it's time to create credentials.
  11. On the left of the site you need to click "Credentials" and then "Create credentials" on the top of the site.
  12. Choose "Service account" from the options. Give it a name (there is no naming convention too).
  13. Click  "Create". As a role you should choose "Editor". Click "continue".
  14. After creating Service account browse your "Service accounts" which can be found on the left bar.
  15. Choose your Service account and click "Keys" bookmark. Then "add key" and finally "create new key".
  16. In "Create key" option choose "JSON" and "Create". Then download the .json file with the credentials.
  17. After downloading credentials file rename it to credentials.json.
  18. In credentials.json select the value of "client_email", go to your google spreadsheet and share permission to edit with that copied email.
  19. Now you are ready to modify the test ```src/test/scripts/tools/google-spreadsheet-to-meta.sh```.
  20. Paste a path to your credentials.json to ```--input-credentials-json``` string and it's ready to use!
```
#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/meta.sh/meta.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/updated-meta

python3 $SCRIPT \
  --input-workflow-name "germline" \
  --input-credentials-json "" \
  --input-meta-json "$PROJECT_DIR/src/main/wdl/pipelines/germline/meta.json" \
  --input-google-spreadsheet-key "12iYYF_JV-xu_dAkgDndIZtMPiMV-iAwIV7kIoVAHn80" \
  --output-meta-json "/tmp/test/tools/updated-meta/meta.json"
```
Install gspread library using your pip with ```pip install gspread```.
Now you just need to modify the values in google spreadsheet and then run script to change meta.json of the chosen workflow.

#### Testing on google cloud
1. Make sure your wdl is perfectly working on your local machine (or anakin).

2. Check if resources used by your script are on google cloud. You can do it by executing ```gsutil ls gs://iseq/path/to/resources```
If they are not on google cloud, you should upload them using ```gsutil cp uploaded_file gs://iseq/path/to/directory/```
Your test-inputs.json file should contain paths to the gcloud, for example:
```
{
 "bco_json_to_csv_workflow.bco_json_to_csv.bco_table": "gs://iseq/data/json/from-fastq-4.7M-plus-sv-calling.bco"
}
```
More about the commands can be found at the documentation: ```https://cloud.google.com/storage/docs/gsutil```

3. Run test on google cloud using ```test.sh --verbose --googlecloud``` (currently not implemented, you should use cromwell run instead, for example: 
```cromwellgoogle run /path/to/wdl/bco-json-to-csv.wdl --inputs /path/to/inputs//test-input.json)```

4. To check if the output exists you should look to the exec directory on google cloud using ```gsutil ls gs://iseq/exec/path/to/output/```

5. To verify the correctness of output text files you can use ```gsutil cat gs://iseq/exec/path/to/text/file```

6. If it works, create a tag with a new version and push it

7. Check if the meta is properly formatted (https:/prod.api-wdl.flow.intelliseq.com/wdl/parse?url=https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/pipelines/target-seq/target-seq.wdl)
   
8. Update workflow tag (http://staging.api-wdl.flow.intelliseq.com/wdl/update, http://prod.api-wdl.flow.intelliseq.com/wdl/update)        

9. Test it on the interface (in the unreleased workflows).

10. Test the script on the most complicated and the largest files you can find.

11. If it works on large files you can contact Dżesika or Marcin to place the path in released workflows


#### Testing on aws
Zamin przystąpisz do testowania na AWS, twój WDL powinien być przetestowany na anaknie na dużych próbkach razem ze sprawdzeniem sensowności outputów!
1. Sprawdź poprawność meta (wdl metadata validation)
https:/prod.api-wdl.flow.intelliseq.com/wdl/parse?url=https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/pipelines/target-seq/target-seq.wdl
2. Utuchom test na aws przy pomocy polecenia:
```bash
test.sh --verbose --aws
```
Test ten sprawdza tylko status analizy, nie sprawdza outputów. Dlatego tak bardzo ważne jest, żeby wdl był poprawnie przetestowany na anakinie

3. Utwórz taga z odpowiednią wersją do swojego wdl 
4. Uruchom:
```bash
http://genetraps.intelliseq.pl:8087/wdl/update
```
Spowoduje to zaktualizowanie WDL w interfejsie.

5. Przejdż do interfejsu
6. Z panelu z lewej strony wybierz workflows i kliknij "Show unreleased Workflows"
7. Wybierz z listy workflow który chcesz testować i uruchom analizę. Docelowo workflow powinien działać na próbkach z genomu
8. Sprawdź czy wyniki analiz mają sens (nie wszystkie taski napisane są super prawidłowo i czasami analiza przechodzi nawet jak wynik jest pusty. Nie powinno tak być i należy to wtedy poprawić w teście)

##### Co jeśli WDL się wywala
1. Trzeba znaleźć taska, który powoduje błąd (przeszukaj logi na "causedBy")
2. Puść test na podejrzanym tasku (być może będzie trzeba go przygotować tak jak opisano wyżej, żeby był widoczny w interfejsie - poprawne meta, update wdl w interfejsie)
3. Jeśli nie potrafisz znaleźć taska powodującego błąd, to niestety trzeba puścić wszystkie taski po kolei (możesz zaczać od modułów, jeśli testujesz pajplanj)

Rozwiązywanie problemu (jak już mamy wywalającego się taska)
Sprawdź każdy z tych punktów niezależnie

1. Sprawdź, czy docker w tasku ma mniej niż 10GB
2. Zwiększ pamięć w runtime
3. Sprawdź, jak długi jest skrypt generowny przez parser wdl (jeśli jest dużo inputów, albo jakiś bardzo długi array, to skrypt będzie bardzo długi i nie będzie możliwy do uruchomienia)
4. Jeśli w WDL jest rozpakowywany jakiś plik, zrób tak, żeby był rozpakowywany w katalogu roboczym.

Jeśli wszystko działa daj znać Dżesice, jaka wesja i jaki pajplajn. Zostanie on umieszczony tutaj: https://gitlab.com/intelliseq/workflows/-/blob/master/released i będzie widoczny w interfejsie.

#### Versioning
Use Semantic Versioning [https://semver.org/](https://semver.org/) for the tasks, modules, pipelines, docker images, templates and scripts. 

Version controll can be included in tagged commit and file content, depending of component of the pipeline, obligatory or optional. 
Rules described as obligatory in table below are very important and not applying them can prompt failure of the software. 


| Rules for developers - how to make version controll |                                                                                                                                   |                                                                                                                               |
|-----------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|
| COMPONENT OF THE PIPELINE                           | TAGGING COMMITS                                                                                                                   | VERSION IN FILE CONTENT                                                                                                       |
| task                                                | OBLIGATORY,  tag:  task-name@x.x.x                                                                                                 | OBLIGATORY,  in inputs:  String task_version = "latest" (version in input inside module code)                                                                         |
| module                                              | OBLIGATORY,  tag:  module-name@x.x.x                                                                                               | OBLIGATORY, in inputs:  String module_version = "latest" (version in input inside pipeline code)                                                                  |
| pipeline                                            | OBLIGATORY,  tag: pipeline-name@x.x.x                                                                                              | OBLIGATORY,   in inputs:  String module_version = "latest" (version in input.json)                                                                 |
| Python scripts                                      | optional,  tag:  script-name.py@x.x.x                                                                                              |  OBLIGATORY:  ```  VERSION="x.x.x"``` |
| templates for reports                               | optional,  There is one docker image for all templates. Therefore version can be the same the same as dockerimage.  reports@x.x.x  | To many files inside template folder  to include version in the header  for each of them.                                     |
| Dockerfile/Dockerimage                              | optional  docker-name@x.x.x   or  task_docker-name@x.x.x                                                                               | OBLIGATORY  In the header of Dockerfile:  ``` #name: reports  \n #version: x.x.x  ```                                           |
| tests                                               | no rules yet                                                                                                                      | no rules                                                                                                                      |
| resources                                           | described in section [Data sources](#data-sources)                                                                                | described in section [Data sources](#data-sources)                                                                            |

##### Imports in .wdl scripts

All imports in wdl should be imported not to th HEAD of dev branch, but to the file in tagged commit. There is the example" 

```bash
import "https://gitlab.com/intelliseq/workflows/-/raw/resource-clinvar@0.1.0/src/main/wdl/tasks/resource-clinvar/latest/resource-clinvar.wdl" as resource_clinvar_task

### Rules for developers
#### Development
Softaware is developed in tha "latest" versions. After merge from dev to master version is changed: latest => v.x.x. 
#### docker
- Docker images are created in hierarchy 'ubuntu' -> 'ubuntu-toolbox' -> 'python-toolbox' -> 'java-toolbox' -> 'tool' or 'toolset'
- Every docker directory has readme.md (search in templates)
- Every docker directory has subdirectories with versions
#### wdl
- task name max 20 chars length
- all disk operations should be performed in the working directory
#### modules
- Contruction of tasks import with "_workflow" in postfix:
```

Code above imports file resource-clinvar.wdl with version 0.1.0 using "frozen" version in repository from specific commit, which was tagged as ```git tag resource-clinvar@0.1.0```. 

##### Tagging commits step by step


It is reccomended to make alias to not miss very important steps: 

```git config --global alias.mypull 'pull --tags origin'```

```git config --global alias.mypush 'push --tags origin'```

Workflow using alias:

```
git add -A .
git commit -m "Commit message"
git mypull dev
grep -r "<<<<<" .
git tag | grep {yourtag}
export FILE=src/main/wdl/modules/module2/module2.wdl; diff <(git show module2@1.1.0:$FILE) <(git show dev:$FILE)
git tag {yourtag-incremented}
git mypush dev

```

Workflow with original commands when you don`t want to make aliases (please dont do that): 

```
git add -A .
git commit -m "Co sie wydarzylo"
git pull --tags origin dev
grep -r "<<<<<" .
git tag | grep yourtag
export FILE=src/main/wdl/modules/module2/module2.wdl; diff <(git show module2@1.1.0:$FILE) <(git show dev:$FILE)
git tag yourtag-incremented
git push --tags origin dev

```
###### Detailed description of some commands:

```grep -r "<<<<<" .``` - check if there are merge conflicts,

```export FILE=src/main/wdl/modules/module2/module2.wdl; diff <(git show module2@1.1.0:$FILE) <(git show dev:$FILE)``` - check if the last tag used for your script contains the same content of the file as the latest version on the dev branch,

```git tag | grep yourtag``` - check what was the last tag used for this script


#### Bioobjects (BCO)

Our biobject json is based on official BCO Specification: https://github.com/biocompute-objects/BCO_Specification

Bioobjects is description of resources and tools versions used by task, module and pipeline. 

TODO
Do opisania jak bco jest generowane i że ważne jest to jak zbudowany jest docker i co jest w meta


## Developer tips (also for dummies)
### How to run jupyter lab on the server
Jupyterlab allows to easily look to pdf and html files on server, where you have only terminal access (particularly working generating pdf reports). 
Installation (if you do not have sudo): \
```pip3 install jupyterlab --user```\
Choose some port only for you (for example 1900). If somebody else uses the same port it is a problem. Use this command during logging on server (put this inside your logging alias):\
```-L 1900:localhost:1900```\
For example on anakin: \
```alias anakin='ssh -i path_to_your_ssh_locally -p 1022 -L 1900:localhost:1900 user_name@anakin.intelliseq.pl'```\
When you are on server's terminal use command:\
```jupyter lab --port 1900```\
Press link (to not select whole link use "ctr" and click). Now jupyter lab shoud be opened in your browser. \
Finished work? Close jupyter lab in your browser. \
Press ctr+C in terminal. \
**Make some alias inside ```.bashrc``` on server to have faster access: \
```alias lab=jupyter lab --port 1900```

### Easy working in docker environment
 In your home directory:
 ```
 mkdir workdir 
 ```
 *you should have local copy of the workflow repository in your home directory also. 
 ```
 cd workdir
 ```
 Run script:
 ```
 scripts/develop path_to_dockerfile
 ```
 Now you are in docker environment and you can use workflows repository, run some tests, make changes in code using some editor, run jupyter lab in ```workdir``` folder.  (but can not change anything in this repo). 
 Finished?
 ```
 exit
 ```
 
 ### Create Python local environment
 To have control what packages (and their versions) you use in your code you should work in virtual environment (specific for particular project). It also allows to not install huge number of packages on your system usr/bin/Python environment. ```virtualenv``` is popular, example package. [https://virtualenv.pypa.io/en/latest/](https://virtualenv.pypa.io/en/latest/)
 Check where is folder with your system Python
 ```
 which python3
 ```
 => /usr/bin/python3.6
Create a folder with your environment. 
```
virtualenv env_name  -p /usr/bin/python3.6
```
Run your environment:
```
source env_name/bin/activate
```
Now you work in your new, empty environment. Install some python packages, develop code. Save packages to some file to later install them to docker or to save them for another developers:
```
pip freeze > requirements.txt
```
Finished work? 
```
deactivate
```

### Tagging commits

Some dummy tutorial in Polish is here: 
https://github.com/muzic194/version-controll-using-commit-tag/blob/master/README.md



### Keyboard shortcuts

- Delete/add tabs in some section. Select whole section ```tab``` or ```shift+tab```,
- In jupyter notbook/lab to modify multiple columns in the same time - select cursor place using "CMD" (mac) or "Windows key", 
- To find some file in the git repository in web browser press ```t```. 

### Usefull links
json viewer to html table: http://out.accessify.com/visit?domain=json2table.com
Makefile: Makefile https://learnxinyminutes.com/docs/make/
Makefile graph viewer: https://dreampuf.github.io/GraphvizOnline/#digraph%20G%20%7B%0A%7D

