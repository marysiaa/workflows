VEP docker build:  
Docker image`intelliseqngs/loftee-gerp-data:1.0.0` built from  [Dockerfile](path) is required.     
It isused in the folowing line of the Dockerfile from this directory:    
```
COPY --from=intelliseqngs/loftee-gerp-data:1.0.0 /data/${CHROMOSOME}-gerp_conservation_scores.homo_sapiens.GRCh38.bw /opt/vep/.vep/conservation/gerp_conservation_scores.homo_sapiens.GRCh38.bw 
```

Modify this line (and the intelliseqngs/loftee-gerp-data:1.0.0 image),
 only if necessary, as building takes long time)   


VEP usage:  
```bash
/opt/vep/src/ensembl-vep/vep \
          --cache \
          --offline \
          --format vcf --vcf \
          --force_overwrite \
          --dir_cache /opt/vep/.vep/ \
          --dir_plugins /opt/vep/.vep/Plugins \
          --input_file /path/to/input-vcf \
          --output_file /path/to/output-vcf \
          ## optional
          --hgvs \
          --biotype \
          --mane \
          --tsl \
          --appris \
          --gencode_basic \
          --exclude_predicted \
          --uniprot \
          --no_intergenic \
          --symbol --xref_refseq
```


VEP usage with loftee plugin:  
``` bash
/opt/vep/src/ensembl-vep/vep \
          --cache \
          --offline \
          --format vcf --vcf \
          --force_overwrite \
          --dir_cache /opt/vep/.vep/ \
          --dir_plugins /opt/vep/.vep/Plugins \
          --input_file /path/to/input-vcf \
          --output_file /path/to/output-vcf \
          ## other vep options
          --plugin LoF,loftee_path:/opt/vep/.vep/Plugins,human_ancestor_fa:/opt/vep/.vep/human_ancestor_seq/human_ancestor.fa.gz,conservation_file:/opt/vep/.vep/conservation/loftee.sql,gerp_bigwig:/opt/vep/.vep/conservation/gerp_conservation_scores.homo_sapiens.GRCh38.bw
```

