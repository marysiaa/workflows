#!/bin/bash

set -e -o pipefail

RELEASE=$1

DATA_DIR=/opt/vep/.vep/homo_sapiens/"$RELEASE"_GRCh38

## change chromosome names
for chrom in  {1..22} X
do
   mv "$DATA_DIR"/"$chrom" "$DATA_DIR"/chr"$chrom"
done
mkdir "$DATA_DIR"/chrY-and-the-rest
mv "$DATA_DIR"/CHR* "$DATA_DIR"/chrY-and-the-rest
mv "$DATA_DIR"/GL* "$DATA_DIR"/chrY-and-the-rest
mv "$DATA_DIR"/LRG* "$DATA_DIR"/chrY-and-the-rest
mv "$DATA_DIR"/KI* "$DATA_DIR"/chrY-and-the-rest
mv "$DATA_DIR"/Y "$DATA_DIR"/chrY-and-the-rest
mv "$DATA_DIR"/MT "$DATA_DIR"/chrY-and-the-rest

## prepare small chromosome-wise fasta files
for chrom in  {1..22} X
do
    samtools faidx "$DATA_DIR"/Homo_sapiens.GRCh38.dna.toplevel.fa.gz "$chrom" |\
    bgzip > "$DATA_DIR"/chr"$chrom"-Homo_sapiens.GRCh38.dna.toplevel.fa.gz
    samtools faidx "$DATA_DIR"/chr"$chrom"-Homo_sapiens.GRCh38.dna.toplevel.fa.gz
    bgzip -r "$DATA_DIR"/chr"$chrom"-Homo_sapiens.GRCh38.dna.toplevel.fa.gz

done
STRANGE_CONTIGS=$( zcat $DATA_DIR/Homo_sapiens.GRCh38.dna.toplevel.fa.gz | grep "^>" | tail -n +24 | cut -f1 -d " " | sed "s/>//" | tr "\n" " " )
samtools faidx "$DATA_DIR"/Homo_sapiens.GRCh38.dna.toplevel.fa.gz $STRANGE_CONTIGS \
| bgzip > "$DATA_DIR"/chrY-and-the-rest-Homo_sapiens.GRCh38.dna.toplevel.fa.gz
samtools faidx "$DATA_DIR"/chrY-and-the-rest-Homo_sapiens.GRCh38.dna.toplevel.fa.gz 
bgzip -r "$DATA_DIR"/chrY-and-the-rest-Homo_sapiens.GRCh38.dna.toplevel.fa.gz

## install loftee plugin and prepare loftee data
cd /opt/ 
git clone --branch grch38 'https://github.com/konradjk/loftee.git' 
mv /opt/loftee/* /opt/vep/.vep/Plugins && rm -r /opt/loftee

## ancestor sequence
mkdir /opt/vep/.vep/human_ancestor_seq 
wget -O /opt/vep/.vep/human_ancestor_seq/human_ancestor.fa.gz  'https://personal.broadinstitute.org/konradk/loftee_data/GRCh38/human_ancestor.fa.gz' 
wget -O /opt/vep/.vep/human_ancestor_seq/human_ancestor.fa.gz.fai  'https://personal.broadinstitute.org/konradk/loftee_data/GRCh38/human_ancestor.fa.gz.fai' 
wget -O /opt/vep/.vep/human_ancestor_seq/human_ancestor.fa.gz.gzi  'https://personal.broadinstitute.org/konradk/loftee_data/GRCh38/human_ancestor.fa.gz.gzi'

LOFTEE_DATA_DIR=/opt/vep/.vep/human_ancestor_seq
for chrom in  {1..22} X
do
    samtools faidx "$LOFTEE_DATA_DIR"/human_ancestor.fa.gz "$chrom" | bgzip > "$LOFTEE_DATA_DIR"/chr"$chrom"-human_ancestor.fa.gz
    samtools faidx "$LOFTEE_DATA_DIR"/chr"$chrom"-human_ancestor.fa.gz
    bgzip -r "$LOFTEE_DATA_DIR"/chr"$chrom"-human_ancestor.fa.gz
done
samtools faidx "$LOFTEE_DATA_DIR"/human_ancestor.fa.gz Y | bgzip > "$LOFTEE_DATA_DIR"/chrY-and-the-rest-human_ancestor.fa.gz
samtools faidx "$LOFTEE_DATA_DIR"/chrY-and-the-rest-human_ancestor.fa.gz
bgzip -r "$LOFTEE_DATA_DIR"/chrY-and-the-rest-human_ancestor.fa.gz

## GERP values
mkdir /opt/vep/.vep/dbscSNV
cd /opt/vep/.vep/dbscSNV
wget ftp://dbnsfp:dbnsfp@dbnsfp.softgenetics.com/dbscSNV1.1.zip
unzip dbscSNV1.1.zip
head -n1 dbscSNV1.1.chr1 > header
for chrom in {1..22} X Y
do
   grep -v ^chr dbscSNV1.1.chr"$chrom" | sort -k5,5 -k6,6n | cat header - \
   | awk '$5 != "."' | bgzip -c > chr"$chrom"-dbscSNV1.1_GRCh38.txt.gz 
   tabix -s 5 -b 6 -e 6 -c c chr"$chrom"-dbscSNV1.1_GRCh38.txt.gz
done
mv chrY-dbscSNV1.1_GRCh38.txt.gz  chrY-and-the-rest-dbscSNV1.1_GRCh38.txt.gz
mv chrY-dbscSNV1.1_GRCh38.txt.gz.tbi  chrY-and-the-rest-dbscSNV1.1_GRCh38.txt.gz.tbi