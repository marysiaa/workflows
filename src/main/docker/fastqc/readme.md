## image name
fastqc

## description
Image containing fastqc tool.

## version
### 0.11.17_v1.0 (05.04.2019)
Initial version of fastqc image.
