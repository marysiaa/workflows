# name: ubuntu-minimal-22.04
# version: 1.0.1

FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /intelliseqtools

ADD src/main/scripts/docker-scripts/utils/utf-8-update-locale.sh /intelliseqtools/utf-8-update-locale.sh
ADD src/main/scripts/docker-scripts/utils/set-usual-permissions.sh /intelliseqtools/set-usual-permissions.sh
ADD src/main/scripts/docker-scripts/utils/touch-usual-files.sh /intelliseqtools/touch-usual-files.sh

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
    uuid-runtime \
    jo \
    jq \
    curl \
    wget \
    python3-pip \
    python3-dev \
    locales && \
    rm -rf /var/lib/apt/lists/* && \
    ln -s /usr/bin/python3 /usr/local/bin/python

RUN pip3 install miniwdl==0.7.3
RUN /intelliseqtools/utf-8-update-locale.sh
ENV LANG en_US.UTF-8

ADD src/main/scripts/bco/bco-after-start.sh /intelliseqtools/bco-after-start.sh
ADD src/main/scripts/bco/bco-before-finish.sh /intelliseqtools/bco-before-finish.sh
ADD src/main/scripts/bco/bco-meta.py /intelliseqtools/bco-meta.py
ADD src/main/scripts/bco/bco-versions.py /intelliseqtools/bco-versions.py

RUN mkdir -p /root/.local/bin && chmod -R go+rx /root

RUN /intelliseqtools/set-usual-permissions.sh

RUN /intelliseqtools/touch-usual-files.sh

ENV PATH="/root/.local/bin:${PATH}"
