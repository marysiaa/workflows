# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  Name: ibd
#  Version: v0.1
#  Authors:
#    - https://gitlab.com/kattom
#    - https://gitlab.com/marpiech
#  Copyright: Copyright 2019 Intelliseq
#  Description: >
#    Ubuntu 18.04 with:
#    v.1.9, Bcftools 1.9
#    hg38 genetic map files 
#  Layers:
#    - ubuntu:18.04
#    - intelliseqngs/ubuntu-minimal:19.04_v0.3
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

FROM intelliseqngs/ubuntu-minimal:19.04_v0.3

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    unzip \
    tabix \
    gawk \
    parallel \
    software-properties-common \
    build-essential \
    libfontconfig1 \
    zlib1g-dev \
    libz-dev \
    libncurses5-dev \
    libbz2-dev \
    liblzma-dev
 

#RUN apt-get install -y samtools bcftools    
RUN mkdir -p /opt/tools 

ADD https://github.com/samtools/bcftools/releases/download/1.9/bcftools-1.9.tar.bz2 /opt/tools

RUN cd /opt/tools && \
    tar -xvf /opt/tools/bcftools-1.9.tar.bz2 && \
    rm /opt/tools/bcftools-1.9.tar.bz2 && \
    /opt/tools/bcftools-1.9/configure && \
    make -C /opt/tools/bcftools-1.9  && \
    make install -C /opt/tools/bcftools-1.9


RUN mkdir -p /resources/genetic-maps/hg38

ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr1.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr1.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr2.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr2.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr3.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr3.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr4.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr4.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr5.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr5.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr6.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr6.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr7.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr7.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr8.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr8.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr9.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr9.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr10.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr10.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr11.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr11.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr12.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr12.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr13.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr13.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr14.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr14.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr15.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr15.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr16.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr16.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr17.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr17.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr18.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr18.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr19.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr19.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr20.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr20.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr21.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr21.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chr22.genetic-map-hg38.txt /resources/genetic-maps/hg38/chr22.genetic-map-hg38.txt
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/genetic-maps/hg38/chrX.genetic-map-hg38.txt /resources/genetic-maps/hg38/chrX.genetic-map-hg38.txt
