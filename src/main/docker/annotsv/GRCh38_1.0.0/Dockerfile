# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: annotsv
#  version: GRCh38_1.0.0
#  authors:
#      - https://gitlab.com/lltw
#  copyright: Copyright 2019 Intelliseq
#  description: >
#    Docker image containing environment needed to annotate VCF containing SVs (Structural Variants) with the use of AnnotSV v2.3.
#  layers: >
#    + intelliseqngs/ubuntu-minimal:19.04_v0.3
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

FROM intelliseqngs/ubuntu-minimal:19.04_v0.3

# set specific commit to clone AnnotSV 2.3 repository from
ARG COMMIT="b5a65c1ddd71d24547f8eab521925f98ece10df4"

# set timezone (required to install TCL properly)
ENV TZ=Europe/Warsaw
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# install git, unzip, tcl, bedtools and python3.6
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
      git unzip \
      tabix \
      tcl8.6-dev tcllib \
      python3-dev \
      bedtools \
      parallel \
      gawk

# add annotsv-tsv-to-vcf.py 1.0.0 script
RUN mkdir -p /intelliseqtools
ADD https://gitlab.com/intelliseq/workflows/-/raw/annotsv-tsv-to-vcf.py@1.0.0/src/main/scripts/tools/annotsv-tsv-to-vcf.py /intelliseqtools

# add annotsv-annotations-description.csv
RUN mkdir -p /resources/annotsv-annotations-description/1.0.0
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/structural-variants-annotations/annotsv-annotations-description/1.0.0/annotsv-annotations-description.csv \
    /resources/annotsv-annotations-description/1.0.0

# install java
RUN wget -c https://github.com/AdoptOpenJDK/openjdk12-binaries/releases/download/jdk-12.0.1%2B12/OpenJDK12U-jdk_x64_linux_hotspot_12.0.1_12.tar.gz
RUN tar xf OpenJDK12U-jdk_x64_linux_hotspot_12.0.1_12.tar.gz && \
    rm -r  OpenJDK12U-jdk_x64_linux_hotspot_12.0.1_12.tar.gz && \
    mkdir /usr/lib/jvm && \
    mv jdk-12.0.1+12 /usr/lib/jvm/ && \
    ln -s /usr/lib/jvm/jdk-12.0.1+12/bin/java /usr/bin/java && \
    ln -s /usr/lib/jvm/jdk-12.0.1+12/bin/javac /usr/bin/javac

# install AnnotSV 2.3
RUN git clone https://github.com/lgmgeo/AnnotSV.git && \
    cd /AnnotSV/ && \
    git checkout -b specific-commit ${COMMIT} && \
    mkdir -p /tools/AnnotSV/commit-${COMMIT} && \
    mv /AnnotSV/* /tools/AnnotSV/commit-${COMMIT}/ && \
    cd / && \
    rm -r AnnotSV && \
    cd /tools/AnnotSV/commit-${COMMIT}/ && \
    make PREFIX="/tools/AnnotSV/commit-${COMMIT}" install

# install human annotation for AnnotSV 2.3
# remove GRCh37 annotations
# compress GRCh38_chromFa.fasta and 1902_phenotype.h2.db files
RUN cd /tools/AnnotSV/commit-${COMMIT}/ && \
    make PREFIX="/tools/AnnotSV/commit-${COMMIT}" install-human-annotation \
    && \
    rm -r /tools/AnnotSV/commit-b5a65c1ddd71d24547f8eab521925f98ece10df4/share/AnnotSV/Annotations_Human/BreakpointsAnnotations/*/GRCh37 && \
    rm -r /tools/AnnotSV/commit-b5a65c1ddd71d24547f8eab521925f98ece10df4/share/AnnotSV/Annotations_Human/FtIncludedInSV/*/GRCh37 && \
    rm -r /tools/AnnotSV/commit-b5a65c1ddd71d24547f8eab521925f98ece10df4/share/AnnotSV/Annotations_Human/RefGene/GRCh37 && \
    rm -r /tools/AnnotSV/commit-b5a65c1ddd71d24547f8eab521925f98ece10df4/share/AnnotSV/Annotations_Human/SVincludedInFt/*/GRCh37 && \
    rm -r /tools/AnnotSV/commit-b5a65c1ddd71d24547f8eab521925f98ece10df4/share/AnnotSV/Annotations_Human/Users/GRCh37 \
    && \
    bgzip /tools/AnnotSV/commit-b5a65c1ddd71d24547f8eab521925f98ece10df4/share/AnnotSV/Annotations_Human/BreakpointsAnnotations/GCcontent/GRCh38/GRCh38_chromFa.fasta && \
    gzip /tools/AnnotSV/commit-b5a65c1ddd71d24547f8eab521925f98ece10df4/share/AnnotSV/Annotations_Exomiser/1902/1902_phenotype/1902_phenotype.h2.db
