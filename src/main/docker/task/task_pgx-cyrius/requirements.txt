pysam==0.15.4
numpy==1.22.2
scipy==1.5.2
statsmodels==0.12.0
