VCF=$1
mkdir imputed
echo $(date) "Splitting file $VCF";
zgrep -v "^#" $VCF | awk -F"\t" '{OFS=FS}{$1=substr($3, length($3)-2)}{$2=substr($3,3); if($3 ~ /rs/ && $3 !~ /;rs/) print>>"imputed/"$1}';
zgrep "^#" $VCF > header;
find imputed -type f | xargs -n1 -P4 -I % sh -c 'echo "sorting and makind index for %"; sort -k1,1V -k2,2n % > %_sorted; cat header %_sorted > %.vcf; rm %_sorted; rm %; bgzip %.vcf; bcftools index %.vcf.gz'

mkdir initial
echo $(date) "Splitting file $VCF";
zgrep -v "^#" $VCF | awk -F"\t" '{OFS=FS}{$1=substr($3, length($3)-2)}{$2=substr($3,3); if($3 ~ /rs/ && $3 !~ /;rs/) print>>"initial/"$1}';
zgrep "^#" $VCF > header;
find initial -type f | xargs -n1 -P4 -I % sh -c 'echo "sorting and makind index for %"; sort -k1,1V -k2,2n % > %_sorted; cat header %_sorted > %.vcf; rm %_sorted; rm %; bgzip %.vcf; bcftools index %.vcf.gz'
