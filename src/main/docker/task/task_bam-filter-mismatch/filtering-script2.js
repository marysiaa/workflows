// script removing very short and soft-clipped reads

function accept(rec) {
     if (rec.getReadUnmappedFlag()) return true;
     var cigar = rec.getCigar();
     if (cigar == null ) return true;

     var all = 0;
     var i;
     var elements = rec.getCigarLength();
     // first and last cigar elements
     var first = cigar.getCigarElement(0);
     var last =  cigar.getCigarElement(elements-1);
     // print(last);
     // sc: reads soft-clipped from both ends
     var sc = first.getOperator().name() == "S" && last.getOperator().name() == "S";
     for (i = 0; i < elements; i++) {
           var ce = cigar.getCigarElement(i);
           var len = ce.getLength();
           if ( ce.getOperator().name() != "S" && ce.getOperator().name() != "H") { all += len; }
           //print(all);
           //print(mismatch);
     }

     var keep_sc = !(sc && (all < 33));
     return keep_sc;

 }

 accept(record);

