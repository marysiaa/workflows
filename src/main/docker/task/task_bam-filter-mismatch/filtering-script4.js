
// both filters: for mismatches and very short and soft-clipped reads

function accept(rec) {
     if (rec.getReadUnmappedFlag()) return true;
     var cigar = rec.getCigar();
     if (cigar == null ) return true;

     var all = 0;
     var mismatch = 0;
     var indel = 0;
     // mn total  mismatches length (including indels, excluding soft-clipped bases)
     var nm = rec.getAttribute("NM");
     if ( nm != null ) {mismatch += nm; }
     var i;
     var elements = rec.getCigarLength();
     // first and last cigar elements
     var first = cigar.getCigarElement(0); // newline
     var last =  cigar.getCigarElement(elements-1); // newline
     // print(last);
     // sc: reads soft-clipped from both ends
     var sc = first.getOperator().name() == "S" && last.getOperator().name() == "S"; // newline
     for (i = 0; i < elements; i++) {
           var ce = cigar.getCigarElement(i);
           var len = ce.getLength();
           if ( ce.getOperator().name() != "S" && ce.getOperator().name() != "H") { all += len; }
           if ( ce.getOperator().name() == "D") { mismatch += 1; mismatch -= len; indel +=1;}
           if ( ce.getOperator().name() == "I" ) { mismatch+=1; mismatch -= len; indel +=1;}
           //print(all);
           //print(mismatch);
           //print(indel);
     }
     //print(all);
     //print(mismatch);
     //print(indel);
     var keep_sc = !(sc && (all < 33)); // newline
     return mismatch/all < THRESHOLD &&  keep_sc && indel < 2;

 }

 accept(record);
