# name: task_ibd-report
# version: 1.0.1

FROM intelliseqngs/ubuntu-minimal-20.04:3.0.4

RUN apt update && \
    apt install -y \
    zip \
    wkhtmltopdf \
    libreoffice \
    r-base \
    libcurl4-openssl-dev \
    libssl-dev\
    libxml2-dev \
    gawk &&\
    rm -rf /var/lib/apt/lists/*

RUN Rscript -e 'install.packages(c("ggplot2", "dplyr", "scales", "tidyverse", "argparse"))'

RUN pip3 install  numpy==1.18.2 \
                  pybiomart \
                  requests \
                  Jinja2==2.10.3


ADD https://gitlab.com/intelliseq/workflows/raw/ibd-update/src/main/scripts/docker-scripts/ibd-plot/ibd-plot.R /intelliseqtools/ibd-plot.R
ADD https://gitlab.com/intelliseq/workflows/raw/ibd-update/resources/miscellaneous/biomart-genes/biomart-table-maker.py /intelliseqtools/biomart-table-maker.py

RUN mkdir -p /tools/bedtools/v2.29.2 && \
    cd /tools/bedtools/v2.29.2 &&\
    wget -O bedtools https://github.com/arq5x/bedtools2/releases/download/v2.29.2/bedtools.static.binary && \
    chmod +x bedtools && \
    ln -s /tools/bedtools/v2.29.2/bedtools /usr/bin/bedtools


RUN mkdir -p /resources/ensembl/release-101 && \
          python3 /intelliseqtools/biomart-table-maker.py \
          --input-dataset "hsapiens_gene_ensembl"\
          --input-host "http://ensembl.org" \
          --output-biomart-csv /resources/ensembl/release-101/tmp.csv && \
          cat /resources/ensembl/release-101/tmp.csv | \
          awk 'BEGIN{FS=OFS="\t"}; {if ($5 <22 || $5 =="X" ){ print "chr"$5"\t"$6"\t"$7"\t"$2}}' | \
          sort -k1,1 -k2,2n > /resources/ensembl/release-101/ensembl-exons.tsv

ADD src/main/scripts/reports /intelliseqtools