Building the docker for panel-generate:

```
1. Make sure you are on anakin and activated our gcr.io credentials
/opt/auth/gcp-auth-coggen
2. Get into workflows' directory root
3. Get the app.jar from the explorare docker
docker create -ti --name datasource gcr.io/iseqkubernetes/api-explorare-prod:latest .
docker cp datasource:/app.jar .
docker rm -f datasource
4. Install the dockerbuilder
python3 -m pip install --upgrade pip
pip install iseqdockertools
5. Run dockerbuilder script
dockerbuilder -d src/main/docker/task/task_panel-generate/Dockerfile
6. Remove the app.jar from workflows' directory root
rm app.jar
```