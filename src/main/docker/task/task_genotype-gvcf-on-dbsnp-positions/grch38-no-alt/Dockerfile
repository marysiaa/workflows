# name: task_genotype-gvcf-on-dbsnp-positions-grch38-no-alt
# version: 1.0.1

FROM intelliseqngs/gatk-4.2.4.1:1.0.0

ARG CHROMOSOME

# install gawk
RUN apt update && \
    apt install -y gawk && \
    rm -rf /var/lib/apt/lists/*

# download reference genome
RUN  set -e && \
    mkdir -p /resources/reference-genome/grch38-no-alt-analysis-set && \
    chmod -R go+rx /resources/reference-genome/grch38-no-alt-analysis-set && \
    curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/chromosome-wise/${CHROMOSOME}/${CHROMOSOME}.GRCh38.no_alt_analysis_set.fa.gz \
    --output /resources/reference-genome/grch38-no-alt-analysis-set/${CHROMOSOME}.grch38-no-alt-analysis-set.fa.gz && \
     curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/chromosome-wise/${CHROMOSOME}/${CHROMOSOME}.GRCh38.no_alt_analysis_set.fa.gz.fai \
    --output /resources/reference-genome/grch38-no-alt-analysis-set/${CHROMOSOME}.grch38-no-alt-analysis-set.fa.gz.fai && \
     curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/chromosome-wise/${CHROMOSOME}/${CHROMOSOME}.GRCh38.no_alt_analysis_set.fa.gz.gzi \
    --output /resources/reference-genome/grch38-no-alt-analysis-set/${CHROMOSOME}.grch38-no-alt-analysis-set.fa.gz.gzi && \
     curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/chromosome-wise/${CHROMOSOME}/${CHROMOSOME}.GRCh38.no_alt_analysis_set.dict \
    --output /resources/reference-genome/grch38-no-alt-analysis-set/${CHROMOSOME}.grch38-no-alt-analysis-set.dict


# download dbSNP BED
RUN  set -e && \
    mkdir -p /resources/dbsnp-for-genotyping-purposes/build-153 && \
    chmod -R go+rx /resources/dbsnp-for-genotyping-purposes/build-153/ && \
    curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/miscellaneous/dbsnp-for-genotyping-purposes/${CHROMOSOME}.dbsnp-for-genotyping.bed.gz \
    --output /resources/dbsnp-for-genotyping-purposes/build-153/${CHROMOSOME}.dbsnp-for-genotyping.bed.gz && \
    curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/miscellaneous/dbsnp-for-genotyping-purposes/${CHROMOSOME}.dbsnp-for-genotyping.bed.gz.tbi \
    --output /resources/dbsnp-for-genotyping-purposes/build-153/${CHROMOSOME}.dbsnp-for-genotyping.bed.gz.tbi

# download dbSNP VCF
RUN  set -e && \
    curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/miscellaneous/dbsnp-for-genotyping-purposes/${CHROMOSOME}.dbsnp-for-genotyping.vcf.gz \
    --output /resources/dbsnp-for-genotyping-purposes/build-153/${CHROMOSOME}.dbsnp-for-genotyping.vcf.gz && \
    curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/miscellaneous/dbsnp-for-genotyping-purposes/${CHROMOSOME}.dbsnp-for-genotyping.vcf.gz.tbi \
    --output /resources/dbsnp-for-genotyping-purposes/build-153/${CHROMOSOME}.dbsnp-for-genotyping.vcf.gz.tbi

# download UCSC simple reapeats with 6bp units or less BED
RUN  set -e && \
    mkdir -p /resources/ucsc-simple-repeats/28-11-2019/ && \
    chmod -R go+rx /resources/ucsc-simple-repeats/28-11-2019/ && \
    curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/intervals/ucsc-simple-repeats/6bp-units-or-less/${CHROMOSOME}.simple-repeats-6bp-or-less.bed.gz \
    --output /resources/ucsc-simple-repeats/28-11-2019/${CHROMOSOME}.simple-repeats-6bp-or-less.bed.gz && \
    curl http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/intervals/ucsc-simple-repeats/6bp-units-or-less/${CHROMOSOME}.simple-repeats-6bp-or-less.bed.gz.tbi \
    --output /resources/ucsc-simple-repeats/28-11-2019/${CHROMOSOME}.simple-repeats-6bp-or-less.bed.gz.tbi

# download python scripts
RUN set -e && \
    wget -O /intelliseqtools/keep-or-remove-vcf-fields.py \
      https://gitlab.com/intelliseq/workflows/raw/imputing/src/main/scripts/tools/keep-or-remove-vcf-fields.py
    #todo branch!

# add task-specific python script - dbsnp-after-merge-postprocessing.py
RUN wget -O /intelliseqtools/dbsnp-after-merge-postprocessing.py \
    https://gitlab.com/intelliseq/workflows/raw/imputing/src/main/scripts/docker-scripts/gatk_dbsnp-genotyping/dbsnp-after-merge-postprocessing.py