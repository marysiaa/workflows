# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: vcf-annotation
#  version: scores-grch38_v0.2
#  authors:
#      - https://gitlab.com/lltw
#      - https://gitlab.com/marpiech
#  copyright: Copyright 2019 Intelliseq
#  description: >
#    Docker image containig environment and resources needed to annotate VCF with pathogenicity prediction scores, evolutionary conservation scores etc. Resources are hosted on Intelliseq server.
#    Respurces: SIFT4G, M-CAP, GERP++, PhastCons100way, PhyloP100way
#  changes:
#    v0.2:
#      - split docker image into separate chromosomes
#      - resources are now chromosome-wise
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
FROM intelliseqngs/java:openjdk12_v0.3

ARG CHROMOSOME
RUN mkdir -p /resources/scores/
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/vcf-annotations/scores/all-vcf-scores/${CHROMOSOME}.all-vcf-scores.vcf.gz \
    /resources/scores/${CHROMOSOME}.all-vcf-scores.vcf.gz
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/vcf-annotations/scores/all-vcf-scores/${CHROMOSOME}.all-vcf-scores.vcf.gz.tbi \
    /resources/scores/${CHROMOSOME}.all-vcf-scores.vcf.gz.tbi
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/vcf-annotations/scores/gerp/${CHROMOSOME}.gerp.bw \
    /resources/scores/${CHROMOSOME}.gerp.bw
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/vcf-annotations/scores/phastcons/${CHROMOSOME}.phastcons-100-way.bw \
    /resources/scores/${CHROMOSOME}.phastcons-100-way.bw
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/vcf-annotations/scores/phylop/${CHROMOSOME}.phylop-100-way.bw \
    /resources/scores/${CHROMOSOME}.phylop-100-way.bw

RUN cd /opt/tools/ && \
  apt install git -y && \
  git clone "https://github.com/lindenb/jvarkit.git" && \
  cat /opt/tools/jvarkit/src/main/java/com/github/lindenb/jvarkit/util/ncbi/NcbiApiKey.java | grep -v "^>" > NcbiApiKey.java && \
  mv NcbiApiKey.java /opt/tools/jvarkit/src/main/java/com/github/lindenb/jvarkit/util/ncbi/NcbiApiKey.java && \
  cd jvarkit && \
  ./gradlew vcfbigwig
