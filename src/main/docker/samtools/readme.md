## Introduction
This is a bunch of the Dockerfiles based on https://github.com/samtools/

## How to update
Change the value of `ARG VERSION` located in one of the Dockerfiles in the directories above to wanted version (which can be found for example here for samtools: https://github.com/samtools/samtools/releases)

## How to use
In order to move and install all packages from every docker you need to include these lines of code in Dockerfile.

```
ARG SAMTOOLS_VERSION="1.16"

FROM intelliseqngs/iseq-bcftools-${SAMTOOLS_VERSION}:1.0.1 as bcfhelper
FROM intelliseqngs/iseq-samtools-${SAMTOOLS_VERSION}:1.0.1 as samhelper
FROM intelliseqngs/iseq-htslib-${SAMTOOLS_VERSION}:1.0.1 as htshelper

FROM YOUR_MAIN_DOCKER as worker

RUN mkdir -p /intelliseqtools
ADD src/main/scripts/docker-scripts/samtools/install-lib.sh /intelliseqtools/install-lib.sh

ARG SAMTOOLS_VERSION="1.16"

COPY --from=bcfhelper /tools/bcftools/${SAMTOOLS_VERSION} /tools/bcftools/${SAMTOOLS_VERSION}
RUN /intelliseqtools/install-lib.sh bcftools ${SAMTOOLS_VERSION}

COPY --from=samhelper /tools/samtools/${SAMTOOLS_VERSION} /tools/samtools/${SAMTOOLS_VERSION}
RUN /intelliseqtools/install-lib.sh samtools ${SAMTOOLS_VERSION}

COPY --from=htshelper /tools/htslib/${SAMTOOLS_VERSION} /tools/htslib/${SAMTOOLS_VERSION}
RUN /intelliseqtools/install-lib.sh htslib ${SAMTOOLS_VERSION} \
    ldconfig
```

install-lib.sh script is located here (workflows root): `src/main/scripts/docker-scripts/samtools/install-lib.sh`
It does the unpacking and installing these files.