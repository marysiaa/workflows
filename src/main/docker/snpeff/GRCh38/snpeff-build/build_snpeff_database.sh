#!/bin/bash

set -e -o pipefail

RELEASE=$1

cd /tools

## download snpEff
wget -c https://snpeff.blob.core.windows.net/versions/snpEff_latest_core.zip 
unzip snpEff_latest_core.zip 
rm snpEff_latest_core.zip
rm -r snpEff/examples 
rm -r snpEff/galaxy/ 
rm -r snpEff/scripts/
rm -r snpEff/SnpSift.jar



## download Ensembl data
mkdir -p /tools/snpEff/data/GRCh38."$RELEASE" 
cd /tools/snpEff/data/GRCh38."$RELEASE"
wget -c -O genes.gtf.gz ftp://ftp.ensembl.org/pub/release-"$RELEASE"/gtf/homo_sapiens/Homo_sapiens.GRCh38."$RELEASE".gtf.gz
wget -c -O protein.fa.gz ftp://ftp.ensembl.org/pub/release-"$RELEASE"/fasta/homo_sapiens/pep/Homo_sapiens.GRCh38.pep.all.fa.gz
wget -c -O cds.fa.gz ftp://ftp.ensembl.org/pub/release-"$RELEASE"/fasta/homo_sapiens/cds/Homo_sapiens.GRCh38.cds.all.fa.gz
wget -c -O regulation.gff.gz https://ftp.ensembl.org/pub/release-"$RELEASE"/regulation/homo_sapiens/homo_sapiens.GRCh38.Regulatory_Build.regulatory_features.20220201.gff.gz
gunzip *.gz

mkdir /tools/snpEff/data/genomes/
cd /tools/snpEff/data/genomes/
wget -c -O GRCh38."$RELEASE".fa.gz ftp://ftp.ensembl.org/pub/release-"$RELEASE"/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.toplevel.fa.gz
gunzip GRCh38."$RELEASE".fa.gz

## modify snpEff contig (mt genetic code)
cd /tools/snpEff
head -131 snpEff.config > temp.config 
printf "# ENSEMBL release "$RELEASE"\nGRCh38."$RELEASE".genome : Homo_sapiens\n" >> temp.config
printf "GRCh38."$RELEASE".reference : ftp://ftp.ensembl.org/pub/release-"$RELEASE"/gtf/\n" >> temp.config
printf "    GRCh38."$RELEASE".MT.codonTable : Vertebrate_Mitochondrial\n" >> temp.config
tail -n +132 snpEff.config  >> temp.config
mv temp.config snpEff.config


## prepare snpEff database
java -Xmx20g -jar /tools/snpEff/snpEff.jar build -v GRCh38."$RELEASE" -noCheckProtein


## download and prepare nextProt database
mkdir /tools/nextprot
wget -c -O /tools/nextprot/nextprot_all.xml.gz https://download.nextprot.org/pub/current_release/xml/nextprot_all.xml.gz
java "-Xmx32g" -jar /tools/snpEff/snpEff.jar  buildNextProt GRCh38."$RELEASE" /tools/nextprot


## clean
rm -r /tools/snpEff/data/genomes 
rm /tools/snpEff/data/GRCh38."$RELEASE"/cds.fa
rm /tools/snpEff/data/GRCh38."$RELEASE"/protein.fa 
rm /tools/snpEff/data/GRCh38."$RELEASE"/genes.gtf
rm /tools/snpEff/data/GRCh38."$RELEASE"/regulation*.gff
rm -r /tools/nextprot
