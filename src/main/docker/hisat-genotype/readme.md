This docker is used in the `hla-hisatgenotype.wdl` task, which works fine - at least for the
main HLA locus genes: A, B , C, DPA1, DPB1, DQA1, DQB1, DRA, DRB1. It uses the IGMTHLA database 
for star allele assignment (version from 2016, 3.26.0.1). This version of the database is automatically downloaded during installation.

In theory, to update the database in docker you should:

1. Download new IGMTHLA database from  https://github.com/ANHIG/IMGTHLA.git. 
Note: one of the needed files large and is not cloned, so must be downloaded separately.
```bash
git clone https://github.com/ANHIG/IMGTHLA.git
rm IMGTHLA/hla.dat && wget -O IMGTHLA/hla.dat https://media.githubusercontent.com/media/ANHIG/IMGTHLA/Latest/hla.dat
 
```
2. Remove old files:
```bash
rm /hisatgenotype/indicies/hisatgenotype_db/HLA/fasta/*
rm /hisatgenotype/indicies/hisatgenotype_db/HLA/msf/*
rm /hisatgenotype/indicies/hisatgenotype_db/HLA/hla.dat
rm /hisatgenotype/indicies/hla_backbone.fa ## lack of this file is used as "signal" to build the database one more time 
```  
3. Copy files needed for database construction to right place
```bash
cp IMGTHLA/fasta/*gen.fasta /hisatgenotype/indicies/hisatgenotype_db/HLA/fasta
cp IMGTHLA/msf/*gen.msf /hisatgenotype/indicies/hisatgenotype_db/HLA/msf
cp IMGTHLA/msf/*nuc.msf /hisatgenotype/indicies/hisatgenotype_db/HLA/msf
cp IMGTHLA/hla.dat /hisatgenotype/indicies/hisatgenotype_db/HLA
```
4. Run genotyping on small test data
```bash
wget ftp://ftp.ccb.jhu.edu/pub/infphilo/hisat-genotype/data/hla/ILMN.tar.gz
tar xvzf ILMN.tar.gz
hisatgenotype --base hla --locus-list A -1 /ILMN/NA12892.extracted.1.fq.gz -2 /ILMN/NA12892.extracted.2.fq.gz -v 
cat /hisatgenotype_out/*.report 
rm ILMN.tar.gz
rm -r ILMN/ && rm -r /hisatgenotype_out 
```
Unfortunately, update of the database is not easy (or possible?), from IMGTHLA version 3.46.0 [see issue](https://github.com/DaehwanKimLab/hisat-genotype/issues/12). 
This issue gives also some other tips, what must be changed in the scripts to update the db. 
The problems with updating are due to wrong assumption made by authors of the `hisatgenotype_typing_process.py` script (part of the program).
In short:
1. program uses data on whole allele genomic alignments (`gen.msf` files): Reference allele is mapped to the reference genome and 
  map relating nucleotide positions in alignment to positions in genome is made. 
2. Next, from  above genomic map, positions of exons are extracted (based on hla.dat file) and mapped to partial alleles (with only coding sequence known, `nuc.msf` files).
   Program assumes and checks that lengths of exons in both alignments must be the same. This is not valid (it is enough that new insertion in any of the partial alleles is present to make the alignments lengths unequal).  
3. At this point database building stops. Scripts are complicated and fixing it may be  difficult.  

