#!/bin/bash

n=$( ./bigWigInfo -chroms gerp_conservation_scores.homo_sapiens.GRCh38.bw | grep 'chrom' | cut -f2 -d ' ' )
echo $n
./bigWigInfo -chroms gerp_conservation_scores.homo_sapiens.GRCh38.bw | grep 'chrom' -A $n | sed 's/\t//' | cut -f1,3 -d ' ' | tail -n +2 > chroms.sizes
for chrom in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X
do
  ./bigWigToWig gerp_conservation_scores.homo_sapiens.GRCh38.bw -chrom=$chrom "$chrom".wig
  ./wigToBigWig "$chrom".wig <( awk -v chrom=$chrom '$1 == chrom' chroms.sizes ) chr$chrom-gerp_conservation_scores.homo_sapiens.GRCh38.bw
done
other_chroms=$( grep -vw '^[1-9]\|^[1-2][0-9]\|^X' chroms.sizes | cut -f1 )


grep -vw '^[1-9]\|^[1-2][0-9]\|^X' chroms.sizes > other_chroms.sizes
for chrom in $other_chroms
do
  ./bigWigToWig gerp_conservation_scores.homo_sapiens.GRCh38.bw -chrom=$chrom other-chroms/"$chrom".wig
done

cat   other-chroms/*.wig > Y.wig
./wigToBigWig Y.wig other_chroms.sizes chrY-and-the-rest-gerp_conservation_scores.homo_sapiens.GRCh38.bw
