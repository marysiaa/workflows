# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: ubuntu-toolbox
#  version: 18.04_v0.9
#  authors:
#    - https://gitlab.com/lltw
#  copyright: Copyright 2019 Intelliseq
#  decription: |
#    Ubuntu toolbox containes:
#      - Ubuntu 18.04 with eseential tools installed
#        - unzip
#        - tabix
#        - wget
#        - curl
#        - gawk
#        - software-properties-common
#        - libfontconfig1
#      - GNU Parallel
#      - python packages/libraries
#      - bcftools and samtools with dependencies (zlib1g-dev, libbz2-dev, liblzma-dev, libncurses5-dev)
#      - scripts used in tasks (python-tools-v*.tar)
#    List of python packages/libraries:
#      - pysam
#      - argparse
#      - biopython
#      - pandas
#    Python tools package version: v0.6
#    List of python scripts (in alphabetical order):
#      - annotate-snpeff-vcf-with-gene-symbols.py
#      - annotate-vcf-with-a-dictionary-deprecated.py
#      - annotate-vcf-with-a-dictionary.py
#      - exon-coverage.py
#      - filter-vcf-by-snpeff-impact.py
#      - flag-multiallelic-sites.py
#      - group-calling-intervals.py
#      - quality-check.py
#      - remove-records-remaining-in-ann-field-after-filtering-by-snpeff-impact.py
#      - sort-info-columns-in-vcf.py
#      - split-fastq-by-lines.py
#      - split-vcf-file.py
#      - vcftocsv.py
#  layers:
#    - ubuntu:18.04
#  changes:
#    v0.9:
#      - add samtools
#    v0.8:
#      - installed openpyxl
#      - added bioobject
#    v0.7:
#      - installed pandas
#    v0.6:
#      - installed pyvcf
#    v0.5:
#      - add filter-vcf-by-snpeff-impact.py
#      - add remove-records-remaining-in-ann-field-after-filtering-by-snpeff-impact.py
#      - add split-fastq-by-lines.py
#      - add split-vcf-file.py
#      - add vcftocsv.py
#    v0.4:
#      - add new feature to flag-multiallelic-sites.py
#      - replace annotate-vcf-with-a-dictionary.py with a new version
#    v0.3: (depracated)
#      - added flag-multiallelic-sites.py
#    v0.2: (depracated)
#      - added bcftools
#    v0.1: (deprecated)
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

FROM ubuntu:18.04

RUN apt update && \
    apt install -y \
    unzip \
    tabix \
    wget \
    curl \
    gawk \
    software-properties-common \
    parallel \
    libfontconfig1 \
    zlib1g-dev \
    libbz2-dev \
    liblzma-dev \
    libncurses5-dev \
    python3-pip \
    python3-dev \
    locales && \
    ln -s /usr/bin/python3 /usr/local/bin/python && \
    pip3 install pysam && \
    pip3 install openpyxl && \
    pip3 install argparse && \
    pip3 install biopython && \
    pip3 install pandas && \
    mkdir -p /opt/tools && \
    wget -O /opt/tools/python.tar.gz \
      https://gitlab.com/intelliseq/workflows/raw/dev/releases/python-tools/python-tools-v0.6.tar.gz && \
    tar -xvzf /opt/tools/python.tar.gz --strip=4 -C /opt/tools && \
    rm /opt/tools/python.tar.gz && \
    wget -O /opt/tools/bcftools-1.9.tar.bz2 \
    https://github.com/samtools/bcftools/releases/download/1.9/bcftools-1.9.tar.bz2 && \
    tar -xvf /opt/tools/bcftools-1.9.tar.bz2 && \
    mv bcftools-1.9 /opt/tools/ && \
    rm /opt/tools/bcftools-1.9.tar.bz2 && \
    /opt/tools/bcftools-1.9/configure && \
    make -C /opt/tools/bcftools-1.9 && \
    make install -C /opt/tools/bcftools-1.9 && \
    wget -O /opt/tools/samtools-1.9.tar.bz2 \
      https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 && \
    tar -xvf /opt/tools/samtools-1.9.tar.bz2 && \
    mv samtools-1.9 /opt/tools/ && \
    rm /opt/tools/samtools-1.9.tar.bz2 && \
    /opt/tools/samtools-1.9/configure && \
    make -C /opt/tools/samtools-1.9 && \
    make install -C /opt/tools/samtools-1.9

RUN pip3 install PyVCF
RUN printf "[{\"resource-name\":\"interval-file\",\"resource-version\":\"sureselect-human-all-exon-v7\"}]" > /resources.bioobject.json
RUN OS_NAME=$(cat /etc/os-release | grep -e "^NAME" | grep -o "\".*\"" | sed 's/"//g') && \
    OS_VERSION=$(cat /etc/os-release | grep -e "^VERSION" | grep -o "\".*\"" | sed 's/"//g') && \
    printf "[{\"tool-name\":\"$OS_NAME\",\"tool-version\":\"$OS_VERSION\",\"tool-license\":\"BSD 2-Clause\"}]" > /tools.bioobject.json
