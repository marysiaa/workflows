# name: lumpy
# version: 1.0.0

FROM intelliseqngs/ubuntu-toolbox-20.04:2.0.3

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
    autoconf \
    libssl-dev \
    bsdmainutils \
    git


RUN mkdir -p /tools/samblaster/v.0.1.24 && \
    wget -O /tools/samblaster/v.0.1.24/samblaster.tar.gz \
    https://github.com/GregoryFaust/samblaster/releases/download/v.0.1.24/samblaster-v.0.1.24.tar.gz && \
    tar -xvf /tools/samblaster/v.0.1.24/samblaster.tar.gz -C /tools/samblaster/v.0.1.24 && \
    rm /tools/samblaster/v.0.1.24/samblaster.tar.gz && \
    make -C /tools/samblaster/v.0.1.24/samblaster-v.0.1.24 && \
    ln -s /tools/samblaster/v.0.1.24/samblaster-v.0.1.24/samblaster /usr/bin/samblaster


RUN mkdir -p /tools/sambamba/v0.7.1 && \
    wget -O /tools/sambamba/v0.7.1/sambamba-0.7.1-linux-static.gz \
    https://github.com/biod/sambamba/releases/download/v0.7.1/sambamba-0.7.1-linux-static.gz && \
    gunzip /tools/sambamba/v0.7.1/sambamba-0.7.1-linux-static.gz && \
    mv /tools/sambamba/v0.7.1/sambamba-0.7.1-linux-static /tools/sambamba/v0.7.1/sambamba && \
    chmod +x /tools/sambamba/v0.7.1/sambamba && \
    ln -s /tools/sambamba/v0.7.1/sambamba /usr/bin


RUN mkdir -p /tools/lumpy-sv/v0.2.13 && \
    cd /tools/lumpy-sv/v0.2.13 && \
    git clone --recursive https://github.com/arq5x/lumpy-sv.git && \
    cd lumpy-sv && \
    make && \
    cp bin/* /usr/bin
    #ln -s /tools/lumpy-sv/0.2.13/lumpy-sv/bin/* /usr/bin/

## scripts converted from python2 to python3
ADD src/main/docker/lumpy/bamlibs.py /tools/lumpy-sv/v0.2.13/lumpy-sv/scripts/bamkit/bamlibs.py
ADD src/main/docker/lumpy/bamgroupreads.py /tools/lumpy-sv/v0.2.13/lumpy-sv/scripts/bamkit/bamgroupreads.py
ADD src/main/docker/lumpy/bamfilterrg.py /tools/lumpy-sv/v0.2.13/lumpy-sv/scripts/bamkit/bamfilterrg.py

## Add low complexity region bed, source: https://doi.org/10.1093/bioinformatics/btu356, Supplementary materials
RUN mkdir -p /resources/low-complexity-regions/hg38-btu356
ADD http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/structural-variants-annotations/lumpy-sv/btu356_LCR-hs38.bed \
    /resources/low-complexity-regions/hg38-btu356/LCR.bed

## Add low complexity region bed, source: https://github.com/hall-lab/speedseq/raw/master/annotations/exclude.cnvnator_100bp.GRCh38.20170403.bed
RUN mkdir -p /resources/low-complexity-regions/hall-lab-hg38-03-04-2017
ADD https://github.com/hall-lab/speedseq/raw/master/annotations/exclude.cnvnator_100bp.GRCh38.20170403.bed \
    /resources/low-complexity-regions/hall-lab-hg38-03-04-2017/LCR.bed