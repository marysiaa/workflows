#!/usr/bin/python3
import wdlutils
import WDL as miniwdl
import json

def describe(wdlurl):
    try:
        wdl = miniwdl.load(wdlurl, check_quant=True, read_source=wdlutils.read_source)
        meta = str(wdl.workflow.meta)
        meta = meta.replace("'{", "{")
        meta = meta.replace("}'", "}")
        meta = meta.replace("'", '"')
        meta = json.loads(meta)
        print("### " + meta["name"])
        print("> " + meta["description"])
    except Exception as e:
#        print(e)
        print("###" + wdlurl)
        print('> Could not parse wdl')
