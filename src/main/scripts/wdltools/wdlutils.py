#!/usr/bin/python3
import os
import urllib
import WDL as miniwdl
import tempfile

async def read_source(uri, path, importer):
    if uri.startswith("http:") or uri.startswith("https:"):
        fn = os.path.join(tempfile.mkdtemp(prefix="miniwdl_import_uri_"), os.path.basename(uri))
        urllib.request.urlretrieve(uri, filename=fn)
        with open(fn, "r") as infile:
            return miniwdl.ReadSourceResult(infile.read(), uri)
    elif importer and (
        importer.pos.abspath.startswith("http:") or importer.pos.abspath.startswith("https:")
    ):
        assert not os.path.isabs(uri), "absolute import from downloaded WDL"
        return await read_source(urllib.parse.urljoin(importer.pos.abspath, uri), [], importer)
    return await miniwdl.read_source_default(uri, path, importer)
