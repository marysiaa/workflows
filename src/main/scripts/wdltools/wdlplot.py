#!/usr/bin/python3
import WDL as miniwdl
import json
import svgwrite
import argparse
import sys
import os
import tempfile
import urllib

BOARD_WIDTH = "10cm"
BOARD_HEIGHT = "10cm"
BOARD_SIZE = (BOARD_WIDTH, BOARD_HEIGHT)
STROKE_WIDTH = 0.02
VIEWBOX_WIDTH = 1000
VIEWBOX_HEIGHT = 1000
FONT = "@import url('https://fonts.googleapis.com/css?family=Muli:400')"
VERSION = '0.0.1'
__version = '0.0.1'
AUTHOR="https://gitlab.com/marpiech"

# hex color to rgb string
def hex(value):
    value = value.lstrip('#')
    def get_int(pos):
        start = pos * 2
        return int(value[start:start+2], 16)
    return "rgb(" + str(get_int(0)) + ","  + str(get_int(1)) + "," + str(get_int(2)) + ")"

# palette definition https://dribbble.com/shots/2623951-Flat-Color-Palette
GRAPEFRUIT = hex("#EC5564")
GRAPEFRUIT_DARK = hex("#D94452")
AQUA = hex("#4fc0e8")
AQUA_LIGHT = hex("#66d4f1")
AQUA_DARK = hex("#3aadd9")
LIGHT_GRAY = hex("E5E8EC")

# source reader for miniwdl
async def read_source(uri, path, importer):
    if uri.startswith("http:") or uri.startswith("https:"):
        fn = os.path.join(tempfile.mkdtemp(prefix="miniwdl_import_uri_"), os.path.basename(uri))
        urllib.request.urlretrieve(uri, filename=fn)
        with open(fn, "r") as infile:
            return miniwdl.ReadSourceResult(infile.read(), uri)
    elif importer and (
        importer.pos.abspath.startswith("http:") or importer.pos.abspath.startswith("https:")
    ):
        assert not os.path.isabs(uri), "absolute import from downloaded WDL"
        return await read_source(urllib.parse.urljoin(importer.pos.abspath, uri), [], importer)
    return await miniwdl.read_source_default(uri, path, importer)

### utils
def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

### draw helper
class Rectangle:
    def __init__(self, point, size, svgdom):
        self.svgdom = svgdom
        self.x = point[0]
        self.y = point[1]
        self.width = size[0]
        self.height = size[1]
        self.radius = min([self.height, self.width]) / 15
        self.kwargs = {
            'fill': AQUA_LIGHT,
            'stroke': AQUA,
            'stroke_width': STROKE_WIDTH * self.width
        }
        self.whitekwargs = {
            'fill': LIGHT_GRAY
        }
        self.linekwargs = {
            'fill': 'none',
            'stroke': LIGHT_GRAY,
            'stroke_width': STROKE_WIDTH * self.width
        }
        self.textkwargs = {
            'font-size': self.radius * 2,
            'font-family': 'Muli',
            'font-weight': 400
        }
    def slice(self, width_tuple, height_tuple):
        return self.slice_by_width(width_tuple[0],width_tuple[1]).slice_by_height(height_tuple[0],height_tuple[1])
    def slice_by_width(self, start, end):
        return Rectangle(
            (self.x + self.width * start, self.y),
            (self.width * (end - start), self.height),
            self.svgdom
            )
    def slice_by_height(self, start, end):
        return Rectangle(
            (self.x, self.y + self.height * start),
            (self.width, self.height * (end - start)),
            self.svgdom
            )
    def center(self):
        return (self.centerx(), self.centery())
    def centerx(self):
        return self.x + self.width / 2
    def centery(self):
        return self.y + self.height / 2
    def draw_rectangle(self):
        self.svgdom.add(
            self.svgdom.rect(
                insert = (self.x + 10, self.y + 10),
                size = (self.width - 20, self.height - 20),
                **self.kwargs
            )
        )
    def draw_output(self):
        self.svgdom.add(
            self.svgdom.circle(
                center = self.center(),
                r = min([self.width, self.height]) / 4,
                **self.kwargs
            )
        )
        self.svgdom.add(
            self.svgdom.text(
                text = "hello world",
                x = self.x,
                y = self.y,
                **self.kwargs
            )
        )
    def draw_call(self):
        if 1.5 * self.width >= self.height:
            height = 0.5 * self.height
            width = height / 1.5
        else:
            width = 0.5 * self.width
            height = 1.5 * width
        self.svgdom.add(
            self.svgdom.rect(
                insert = (self.x + (self.width - width) / 2, self.y + (self.height - height) / 2),
                size = (width, height),
                rx = self.radius,
                ry = self.radius,
                **self.kwargs
            )
        )
        self.svgdom.add(
            self.svgdom.rect(
                insert = (self.x + self.radius + (self.width - width) / 2, self.y + self.radius * 3 + (self.height - height) / 2),
                size = (width - self.radius * 2, height - self.radius * 4),
                **self.kwargs
            )
        )
    def draw_to(self,node):
        target = node.rectangle
        r = self.radius
        position_diff = (target.position[0] - self.position[0], target.position[1] - self.position[1])
        width_diff = target.centerx() - self.centerx()
        height_diff = target.centery() - self.centery()
        is_adjacent = True if position_diff[0] + position_diff[1] == 1 else False
        is_same_row = True if position_diff[1] == 0 else False
        is_same_column = True if position_diff[0] == 0 else False
        is_next_row = True if abs(position_diff[1]) == 1 else False
        is_next_column = True if abs(position_diff[0]) == 1 else False
        horizontal_sign = "" if position_diff[0] >= 0 else "-"
        horizontal = 1 if horizontal_sign == "" else -1
        vertical_sign = "" if position_diff[1] >= 0 else "-"
        vertical = 1 if vertical_sign == "" else -1
        inverted_horizontal_sign = "" if position_diff[0] < 0 else "-"
        inverted_vertical_sign = "" if position_diff[1] < 0 else "-"
        self.svgdom.add(
            self.svgdom.path(
                " ".join([
                    "" if False else "M" + str(self.centerx()) + " " + str(self.centery()),
                    "" if is_adjacent else "h" + str(horizontal * (self.width / 2 - r) + self.position[1] * 2),
                    "" if is_adjacent or is_same_row else "q" + horizontal_sign + str(r) + " 0 " + horizontal_sign + str(r) + " " + vertical_sign + str(r),
                    "" if is_adjacent or not is_same_row else "q" + str(r) + " 0 " + str(r) + " " + str(r),
                    "" if is_adjacent else "v" + str(self.position[0] * 2),
                    "" if is_adjacent or is_same_row  else "v" + str(vertical * (self.height / 2 - 2 * r)),
                    "" if is_adjacent or not is_same_row else "v" + str(self.height / 2 - 2 * r),
                    "" if is_adjacent or is_same_row or is_next_row else "v" + str(vertical * (abs(height_diff) - self.height)),
                    "" if is_adjacent or not (is_same_column or is_next_column) else "v" + vertical_sign + str(2 * r),
                    "" if is_adjacent or is_same_column or is_next_column else "q" + "0 " + vertical_sign + str(r) + " " + horizontal_sign + str(r) + " " + vertical_sign + str(r),
                    "" if is_adjacent or is_same_column or is_next_column else "h" + str(horizontal * (abs(width_diff) - self.width - 2 * r)),
                    "" if is_adjacent or is_same_column or is_next_column or is_same_row else "q" + horizontal_sign + str(r) + " 0 " + horizontal_sign + str(r) + " " + vertical_sign + str(r),
                    "" if is_adjacent or is_same_column or is_next_column or not is_same_row else "q" + horizontal_sign + str(r) + " 0 " + horizontal_sign + str(r) + " " + inverted_vertical_sign + str(r),
                    "" if is_adjacent or not is_same_row else "v" + str(-1 * (self.height / 2 - 2 * r)),
                    "" if is_adjacent or is_same_row else "v" + str(vertical * (self.height / 2 - 2 * r)),
                    "" if is_adjacent else "v-" + str(self.position[0] * 2),
                    "" if is_adjacent or not is_same_row else "q" + "0 " + "-" + str(r) + " " + "-" + str(r) + " " + "-" + str(r),
                    "" if is_adjacent or is_same_row else "q" + "0 " + vertical_sign + str(r) + " " + horizontal_sign + str(r) + " " + vertical_sign + str(r),
                    "" if False else "L" + str(target.centerx()) + " " + str(target.centery())
                ]),
                **self.linekwargs
            )
        )
### class to store
class WdlPlot:
    def __init__(self, workflow):
        self.nodes = []
        self.svgdom = svgwrite.Drawing(size = BOARD_SIZE)
        self.svgdom.viewbox(0, 0, VIEWBOX_WIDTH, VIEWBOX_HEIGHT)
        self.parse_tree(workflow)
        #print(self.getmaxpathlen())

    def parse_tree(self, workflow):
        for node in (workflow.body or []) + (workflow.outputs or []):
            self.nodes.append(node)
            if isinstance(node, miniwdl.Tree.Call) and hasattr(node.callee, 'body'):
                for innernode in (node.callee.body or []) + (node.callee.outputs or []):
                    self.nodes.append(innernode)
                    node.add_dependency(innernode)
        for node in self.nodes:
            for child in node.children:
                #print(node.id() + " : " + str(child))
                if isinstance(child, miniwdl.Tree.WorkflowNode):
                    self.nodes.append(child)
                    child.add_dependency(node)
        for node in self.nodes:
            node.set_dependencies(self.nodes)
        for node in self.nodes:
            node.sort_dependencies()
        self.positions = {}
        for node in self.get_outputs():
            node.set_graph_position(self.positions, None)

        #for node in self.nodes:
        #    print(node.id() + " :: " + str(node) + " :: " + str([ x.id() for x in node.get_dependencies() ]))

    def get_nodebyid(self, ids):
        return [node for node in self.nodes if node.id() in ids]

    def get_outputs(self):
        outputs = [node for node in self.nodes if node.get_class() == "output"]
        outputs.sort(key=lambda x: x.path_length(), reverse = True)
        return outputs

    def get_max_path_length(self):
        if len(self.get_outputs()) == 0:
            return 1
        return max([output.path_length() for output in self.get_outputs()])

    ### plots
    def to_svg(self):
        view = Rectangle((0,0),(VIEWBOX_WIDTH,VIEWBOX_HEIGHT),self.svgdom)
        try:
            grid_width = 1 / max(self.positions.values(),key=lambda item:item[0])[0]
            grid_height = 1 / max(self.positions.values(),key=lambda item:item[1])[1]
        except:
            grid_width = 1
            grid_height = 1
        for node in self.nodes:
            if node.id() in self.positions.keys():
                node_position = self.positions[node.id()]
                width_slice = ((node_position[0] - 1) * grid_width, node_position[0] * grid_width)
                height_slice = ((node_position[1] - 1) * grid_height, node_position[1] * grid_height)
                node.rectangle = view.slice(width_slice, height_slice)
                node.rectangle.position = node_position
        for node in self.nodes:
            if node.id() in self.positions.keys():
                node.draw()
        return self.svgdom.tostring()

    def draw_outputs(self):
        pass
        #for output in self.getoutputs():

        #    output.get_dependencies() = self.getnodebyid(output.workflow_node_dependencies)
        #    print(output.workflow_node_id + " " + str(output.getdependencies()))

    def draw_output(self):
        pass

### maximum length of dependencies chain
def path_length( self ):
    if len(self.get_dependencies()) == 0:
        return 1
    return max([node.path_length() for node in self.get_dependencies()]) + 1
miniwdl.Tree.WorkflowNode.path_length = path_length

def set_graph_position( self, positions, parent ):
    if self.id() in positions:
        return positions

    if parent is None:
        new_position = (1 + 0, 1 + 0)
    else:
        new_position = (positions[parent.id()][0] + 1, positions[parent.id()][1])
    while new_position in positions.values():
        new_position = (new_position[0], new_position[1] + 1)
    positions[self.id()] = new_position

    # move inputs to the left
    if self.get_class() == "decl" and len(self.get_dependencies()) == 0:
        max_position = max(positions.values(),key=lambda item:item[0])
        if max_position[0] > new_position[0]:
            new_position = max_position
            while new_position in positions.values():
                new_position = (new_position[0], new_position[1] + 1)
            positions[self.id()] = new_position

    if len(self.get_dependencies()) == 0:
        return positions

    for node in self.get_dependencies():
        node.set_graph_position(positions, self)
    return positions
miniwdl.Tree.WorkflowNode.set_graph_position = set_graph_position

### set dependencies
def add_dependency( self, node ):
    if not hasattr(self, '_dependencies'):
        self._dependencies = []
    self._dependencies.append(node)
    self.sort_dependencies()
miniwdl.Tree.WorkflowNode.add_dependency = add_dependency

def get_dependencies( self ):
    if not hasattr(self, '_dependencies'):
        self._dependencies = []
    return self._dependencies
miniwdl.Tree.WorkflowNode.get_dependencies = get_dependencies

def set_dependencies( self , nodes):
    for dependency_name in self.workflow_node_dependencies:
        node = next(x for x in nodes if dependency_name in x.id())
        if isinstance(node, miniwdl.Tree.Gather):
            dependency_name = remove_prefix(dependency_name, "gather-")
        node = next(x for x in nodes if dependency_name in x.id())
        self.add_dependency(node)
    #print(self.workflow_node_id + " " + str(self.workflow_node_dependencies))
miniwdl.Tree.WorkflowNode.set_dependencies = set_dependencies

def sort_dependencies(self):
    self.get_dependencies().sort(key=lambda x: x.path_length(), reverse = True)
miniwdl.Tree.WorkflowNode.sort_dependencies = sort_dependencies

def get_class( self ):
    return self.id().split("-")[0]
miniwdl.Tree.WorkflowNode.get_class = get_class

def id( self ):
    return self.workflow_node_id + "-" + str(abs(hash(str(self))))
miniwdl.Tree.WorkflowNode.id = id

def get_outputs( self ):
    outputs = []
    try:
        for binding in self.effective_outputs:
            outputs.append(binding.name.split(".")[1])
    except:
        pass
    return outputs
miniwdl.Tree.WorkflowNode.get_outputs = get_outputs

def describe( self ):
    print("================================================================")
    print("===== ID: " + self.id())
    print("  dependencies: " + str([dep.id() for dep in self.get_dependencies()]))
    print("  inputs: " + str(self.get_inputs()))
    print("  outputs: " + str(self.get_outputs()))
    print("================================================================")
miniwdl.Tree.WorkflowNode.describe = describe

### node draw
def draw( self ):
    if self.get_class() == "output":
        self.rectangle.draw_output()
    elif self.get_class() == "call":
        self.rectangle.draw_call()
    else:
        self.rectangle.draw_rectangle()
    for node in self.get_dependencies():
        if hasattr(node, 'rectangle'):
            self.rectangle.draw_to(node)
        else:
            print(node.id() + " has no rectangle")

miniwdl.Tree.WorkflowNode.draw = draw

if __name__ == '__main__':

    # parse arguments
    parser = argparse.ArgumentParser(description='produces plot for wdl')
    parser.add_argument('--wdl', '-w', type=str, required=True, help='path to wdl')
    arguments = parser.parse_args(args = sys.argv[1:])

    # read wdl
    wdl = miniwdl.load(arguments.wdl, check_quant = True, read_source = read_source)
    wdlplot = WdlPlot(wdl.workflow)
    print(wdlplot.to_svg())
