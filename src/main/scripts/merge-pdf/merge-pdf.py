#!/usr/bin/python3
__version__ = '0.0.1'

from PyPDF2 import PdfFileReader, PdfFileWriter
from typing import List
import argparse



def merge_pdfs(paths:List[str], output:[str]) -> None:
    pdf_writer = PdfFileWriter()

    for path in paths:
        pdf_reader = PdfFileReader(path)
        for page in range(pdf_reader.getNumPages()):
            # Add each page to the writer object
            pdf_writer.addPage(pdf_reader.getPage(page))

    # Write out the merged PDF
    with open(output, 'wb') as out:
        pdf_writer.write(out)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='merges pdf files')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--sample_id', type=str, nargs=1, help='id of sample')
    parser.add_argument('--pdf_files', '-f', type=str, nargs='+', help='pdf files to merge in order')
    parser.add_argument('--output', '-o', type=str, nargs=1, help='name of output file' )
    # more arguments
    arguments = parser.parse_args()

    merge_pdfs(arguments.pdf_files, output=(arguments.output)[0])
