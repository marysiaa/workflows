#!/usr/bin/env python3

import sys
import os
import urllib
import WDL
import json
import argparse
import requests

__version__ = '1.3.0'

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

async def read_source(uri, path, importer):
    if uri.startswith("http:") or uri.startswith("https:"):
        req = urllib.request.Request(uri, headers={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'})
        with urllib.request.urlopen(req) as response:
            content = response.read().decode()
        return WDL.ReadSourceResult(content, uri)
    elif importer and (
        importer.pos.abspath.startswith("http:") or importer.pos.abspath.startswith("https:")
    ):
        assert not os.path.isabs(uri), "absolute import from downloaded WDL"
        return await read_source(urllib.parse.urljoin(importer.pos.abspath, uri), [], importer)
    return await WDL.read_source_default(uri, path, importer)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='#todo')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('wdl', help='WDL path or url')
    parser.add_argument('-p', '--print_result', action='store_true', default=False, help='Print the result to stdout instead of writing to a file')
    arguments = parser.parse_args()

    try:
        doc = WDL.load(arguments.wdl, check_quant=True, read_source=read_source)
        meta = str(doc.workflow.meta)
        meta = meta.replace("'{", "{")
        meta = meta.replace("}'", "}")
        meta = meta.replace("'", '"')
        meta = json.loads(meta.replace("\'", '"'))
        if not meta:
            request = requests.get(os.path.dirname(arguments.wdl) + "/meta.json")
            if request.ok:
                meta = request.json()
            else:
                print(color.BOLD + color.RED + "Most likely meta for WDL does not exists" + color.END, file=sys.stderr)
                meta = {}

    except (urllib.error.HTTPError, WDL.Error.SyntaxError):
        print(color.BOLD + color.RED + "Most likely wdl with this tag does not exist" + color.END, file=sys.stderr)
        meta = {}

    except json.decoder.JSONDecodeError:
        print(color.BOLD + color.RED + "Expecting key or value enclosed in double quotes in meta section" + color.END, file=sys.stderr)
        meta = {}

    if arguments.print_result:
        print(meta)
    else:
        with open('/meta.json', 'w') as outfile:
            json.dump(meta, outfile)
