### bioobject
wget -O bioobject.py https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/versions.py
python3 bioobject.py

cpu=$(lscpu | grep '^CPU(s)' | grep -o '[0-9]*')
memory=$(cat /proc/meminfo | grep MemTotal | grep -o '[0-9]*' |  awk '{ print $1/1024/1024 ; exit}')
finishtime=$(date +%s)
starttime=$(cat starttime)
tasktime=$((finishtime-starttime))

os_name=$(cat /etc/os-release | grep -e "^NAME" | grep -o "\".*\"" | sed 's/"//g')
os_version=$(cat /etc/os-release | grep -e "^VERSION" | grep -o "\".*\"" | sed 's/"//g')
jo -p name=$os_name version=$os_version > software_ubuntu.bco
software_prerequisites=$(tools="[]"; for toolfile in $(ls software*.bco); do tools=$(echo $tools | jq ". + [$(cat $toolfile)]") ; done; echo $tools)
if [ "$(ls -A $datasource*.bco)" ]; then
  external_data_endpoints=$(tools="[]"; for toolfile in $(ls datasource*.bco); do tools=$(echo $tools | jq ". + [$(cat $toolfile)]") ; done; echo $tools)
fi
provenance_domain=$(jo -p \
  name="$task_name" \
  version="$task_version" \
)

execution_domain=$(jo -p \
  script="https://gitlab.com/intelliseq/workflows/raw/master/src/main/wdl/tasks/alignment-bwa-mem/$task_version/$task_name.wdl" \
  script_driver="shell" \
  software_prerequisites="$software_prerequisites" \
  external_data_endpoints="$external_data_endpoints" \
)

parametric_domain=$(jo -a \
  $(jo cpu="$cpu") \
  $(jo memory="$memory") \
  $(jo tasktime="$tasktime") \
  $(jo name="$task_name") \
  $(jo version="$task_version") \
  $(jo docker_image="$task_docker") \
)

biocomputeobject=$(jo -p \
  bco_spec_version="https://w3id.org/biocompute/1.3.0/" \
  bco_id="https://intelliseq.com/flow/$(uuidgen)" \
  provenance_domain="$provenance_domain" \
  execution_domain="$execution_domain" \
  parametric_domain="$parametric_domain" \
)

echo "$biocomputeobject" > bco.json
