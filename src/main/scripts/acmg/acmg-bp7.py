#!/usr/bin/env python3

import pysam
import argparse
from typing import List

__version__ = '2.0.0'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]

    except KeyError:
        return None


def get_splicing_scores(record):
    try:
        ada_score = float(get_string_info_field_or_none(record, "ISEQ_ada_score")[0])
    except TypeError:
        ada_score = 0.0
    try:
        rf_score = float(get_string_info_field_or_none(record, "ISEQ_rf_score")[0])
    except TypeError:
        rf_score = 0.0

    return max([ada_score, rf_score])


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def find_good_transcripts_syn(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with synonymous mutation; 3) no errors/warnings present#
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                'synonymous' in transcript.variant_type() and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings()) and (
                'ERROR' not in transcript.transcript_warnings())):
            good_transcripts.append(transcript)
    return good_transcripts


def check_mutation_type(transcripts, splicing_score, highest_impact):

    score = 0.0
    description = ""

    if highest_impact in ["MODIFIER", ".", None]:
        description += "This variant lies outside a coding sequence, or does not affect" \
                     "any non-dubious transcript of the protein coding gene."
        if splicing_score <= 0.6:
            score += 1

        else:
            description += " But it may affect splicing."

    elif highest_impact == "LOW":

        syn_transcripts = find_good_transcripts_syn(transcripts)
        #severe_transcripts = find_good_transcripts_ps1(transcripts)

        ## not synonymous, but harmless
        if len(syn_transcripts) == 0:
            description += "This variant lies within the coding sequence, but is unlikely to change a protein"
            if splicing_score <= 0.6:
                description = "."
                score += 1
            else:
                description += ", but it might affect splicing."

        ## synonymous
        else:
            genes = set()
            for transcript in syn_transcripts:
                genes.add(transcript.transcript_gene())
            description += "This is a synonymous variant of the {} gene(s).".format(",".join(genes))

            if splicing_score <= 0.6:
                score += 1

            else:
                description += "But, it may affect splicing."
    ## synonimous, but with worse consequences in some transcripts:
    else:
        syn_transcripts = find_good_transcripts_syn(transcripts)
        if len(syn_transcripts) > 0:
            description += "This is a synonymous variant, but it may also lead " \
                    "to more severe changes in some transcripts."
        else:
            description += "This is a coding, protein-changing variant."
    return score, description.replace("..", ".")


def main(vcf_name, vcf_writer_name):
    # Adding ISEQ_BP7_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BP7_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BP7 (Supporting evidence of benign impact): A positive score is given to synonymous variants. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff ANN field, dbscSNV - ISEQ_ada_score and ISEQ_rf_score'),
             ('Version', 'SnpEff: the same as in the ANN field, dbscSNV: v1.1')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Adding ISEQ_BP7_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BP7_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BP7_SCORE"),
             ('Source', 'SnpEff ANN field, dbscSNV - ISEQ_ada_score and ISEQ_rf_score'),
             ('Version', 'SnpEff: the same as in the SnpEff ANN field, dbscSNV: v1.1')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():

        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            score = 0
            description = "BP7^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"
            description = description.replace(" ", "^")
            record.info["ISEQ_ACMG_BP7_SCORE"] = float(score)
            record.info["ISEQ_ACMG_BP7_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        splicing_score = get_splicing_scores(record)
        try:
            highest_impact = get_string_info_field_or_none(record, "ISEQ_HIGHEST_IMPACT")[0]
        except TypeError:
            highest_impact = None

        score, description = check_mutation_type(transcripts, splicing_score, highest_impact)
        description = description.strip().replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_BP7_SCORE"] = float(score)
        record.info["ISEQ_ACMG_BP7_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='annotates vcf with ACMG BP7')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
