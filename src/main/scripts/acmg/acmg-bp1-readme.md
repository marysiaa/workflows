### **This file describes how acmg-bp1.py should work**

The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BP1 criteria:  
*"BP1 Missense variant in a gene for which primarily truncating variants are known to cause disease"*  

To achieve the above goal acmg_bp1.py does the following:  
It checks if given variant is a missense variant (transcripts without warnings/errors)  
* if not: BP1 SCORE is 0  
* if so, it applies "gene level test(s)"  
   
'lof test': checks if given gene has known (ClinVar) null pathogenic variants  
(performed for all genes with missense mutation in ISEQ vcf)  
lof test score is:  
* 0.5 for genes with one or two known pathogenic lof variants  
* 1.0 for genes with number of such variants greater than 2  
* 0 for genes without any pathogenic lof variant in ClinVar  

'missense test': checks if given gene has any pathogenic missense variants   
(only for genes with non-zero lof test score)  
missense score is:  
* 0.25 for genes with one known pathogenic missense variant  
* 0.05 for genes with number of pathogenic missesne variants higher than 1  
* 1.0 for genes without pathogenic missense variation  
Pathogenic missense variants: described as  "Pathogenic" or "Likely pathogenic" in ClinVar   

The final BP1 SCORE is:   
* 0 for non missense variants    
* 0 for variants in genes without any pathogenic lof variants in ClinVar    
* (lof score)x(missense score) for other variants [max score = 1.0]    
 
 [Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)
