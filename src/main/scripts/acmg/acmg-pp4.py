#!/usr/bin/python3

import pysam
import argparse

__version__ = '0.0.1'

AUTHOR = "gitlab.com/kattom"


def get_first_value_typea_info_field_or_none(line, field_name: str):
    try:
        return line.info[field_name][0]
    except (KeyError, IndexError):
        return None


def get_panel_score(record):
    genes = []
    max_panel_score = 0.0
    try:
        panel_scores = get_first_value_typea_info_field_or_none(record, "ISEQ_GENE_PANEL_SCORE").split(":")
        max_panel_score = max([float(i) for i in panel_scores if i != "."])
        # print(max_panel_score)
        genes = get_first_value_typea_info_field_or_none(record, "ISEQ_GENES_NAMES").split(":")
        # print(genes)

    except (AttributeError, ValueError):
        pass

    if max_panel_score > 0 and len(genes) == 1:
        description = "Variant of the {} gene, likely contributing to the " \
                      "patient phenotype (gene to phenotype match: {}).".format(genes[0], max_panel_score)
    elif max_panel_score > 0 and len(genes) > 1:
        description = "Variant of the following genes: {}, likely contributing " \
                      "to the patient phenotype (maximal gene to phenotype match: {}).".format(", ".join(genes),
                                                                                               max_panel_score)
    else:
        description = "Variant unlikely to contribute to the patient phenotype"
    return max_panel_score / 100, description


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP4_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PP4_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PP4 (Supporting evidence of pathogenic impact): A positive score is given to "
              "variants with ISEQ_PANEL_SCORE score higher than 0.0). Full description of the ACMG criteria "
              "can be found in PMID: 25741868."),
             ('Source', 'ISEQ_PANEL_SCORE field'),
             ('Version', 'the same as in the ISEQ_GENES_PANEL_SCORE field')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP4_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PP4_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description',
              "PP4 (Supporting evidence of pathogenic impact): A positive score is given to variants with "
              "ISEQ_PANEL_SCORE score higher than 0.0). Full description of the ACMG"
              " criteria can be found in PMID: 25741868."),
             ('Source', 'ISEQ_PANEL_SCORE field'),
             ('Version', 'the same as in the ISEQ_GENES_PANEL_SCORE field')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        score, description = get_panel_score(record)
        description = description.replace(" ", "^").replace(",", "*")

        record.info["ISEQ_ACMG_PP4_SCORE"] = score
        record.info["ISEQ_ACMG_PP4_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PP4')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
