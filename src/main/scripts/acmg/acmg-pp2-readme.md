### **This file describes how acmg-pp2.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PP2 criteria:  
*"PP2 Missense variant in a gene that has a low rate of benign missense variation and in which missense variants are a common mechanism of disease"*

To achieve this aim acmg-pp2.py does the following:   
It checks if variant causes amino acid substitution (only protein coding transcripts without errors/warnings added by SnpEff are considered)   
* if not: it the final PP2 SCORE is 0.0   
* if so: it checks whether affected gene (or genes) has any known pathogenic missense variants (ClinVar)  
   * if not: the final P2 SCORE is 0.0   
   * if so: it checks if number of known pathogenic missense variants is greater than twice the number of benign missense variants  
      * if so: it gives 1.0 as the final PP2 SCORE   
      * if not: the final PP2 SCORE is 0.0    

pathogenic missense variants: described in ClinVar as 'Pathogenic' or 'Likely pathogenic'   
benign missense variants: described in ClinVar as 'Benign', 'Likely benign'  or of 'Uncertain significance'   

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

