#!/usr/bin/env python3

import pysam
import argparse
import json

__version__ = '2.0.0'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]

    except KeyError:
        return None


predictor_fields = ["dbNSFP_M_CAP_score", "dbNSFP_PrimateAI_score", "dbNSFP_BayesDel_addAF_score",
                    "dbNSFP_DANN_score", "dbNSFP_fathmm_MKL_coding_score", "dbNSFP_DEOGEN2_score",
                    "dbNSFP_LIST_S2_score", "dbNSFP_MVP_score", "dbNSFP_MutationAssessor_score",
                    "dbNSFP_SIFT4G_score"]

splicing_predictors_fields = ["ISEQ_ada_score", "ISEQ_rf_score"]


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


def get_threshold(predictor_field, predictor_dict):
    return predictor_dict[0][predictor_field]["threshold_benign"]


def get_direction(predictor_field, predictor_dict):
    return predictor_dict[0][predictor_field]["high_score_more_pathogenic"]


def get_predictor_name(predictor_field, predictor_dict):
    return predictor_dict[0][predictor_field]["name"]


def check_appris(record, other_score_as_list):
    selected_scores = None
    try:
        appris = get_string_info_field_or_none(record, "dbNSFP_APPRIS")[0].split(":")
        appris_formatted = [
            int(i.replace("principal", "")) if i not in [None, ".", "alternative1", "alternative2"] else 10
            for i in appris]
        ## check if any transcript is appris "principal"
        check_appris = len([i for i in appris_formatted if i < 10]) > 0
        if check_appris:
            ## pick scores for appris "principal" transcripts
            appris_transcript_scores = [i for i in zip(other_score_as_list, appris_formatted) if
                                        i[0] not in [".", None]]
            if bool(appris_transcript_scores):
                ## pick score for the best transcript (may be more than one equally good)
                best_appris = sorted(appris_transcript_scores, key=lambda x: x[1])[0][1]
                selected_scores = [i[0] for i in appris_transcript_scores if i[1] == best_appris]
    ## lack of appris field
    except (TypeError, AttributeError):
        pass
    return selected_scores


def deal_with_aloft_field(aloft_list):
    #list(map(float, selected_tol))
    try:
        output_list = list(map(float, aloft_list))
    except ValueError:
        output_list = [list(map(float, i.split("_"))) for i in aloft_list]
        output_list = [sum(i)/len(i) for i in output_list]
    return output_list



## check aloft
def check_aloft(record):
    try:
        aloft_tol = get_string_info_field_or_none(record, "dbNSFP_Aloft_prob_Tolerant")[0].split(":")
        aloft_rec = get_string_info_field_or_none(record, "dbNSFP_Aloft_prob_Recessive")[0].split(":")
        aloft_dom = get_string_info_field_or_none(record, "dbNSFP_Aloft_prob_Dominant")[0].split(":")

    except TypeError:
        return None

    selected_tol = check_appris(record, aloft_tol)
    selected_rec = check_appris(record, aloft_rec)
    selected_dom = check_appris(record, aloft_dom)

    ## lack of prediction for principal transcripts => calculate average
    if selected_tol is None:
        selected_tol = [i for i in aloft_tol if i != "."]
        selected_rec = [i for i in aloft_rec if i != "."]
        selected_dom = [i for i in aloft_dom if i != "."]

    selected_tol = deal_with_aloft_field(selected_tol)
    selected_rec = deal_with_aloft_field(selected_rec)
    selected_dom = deal_with_aloft_field(selected_dom)

    tol_score = sum(selected_tol) / len(selected_tol)
    rec_score = sum(selected_rec) / len(selected_rec)
    dom_score = sum(selected_dom) / len(selected_dom)

    if dom_score > rec_score and dom_score > tol_score:
        score = 1.0
    elif rec_score > tol_score and rec_score > dom_score:
        score = 0.5
    else:
        score = 0.0
    return score


## get dbnsfp scores (assigned as one value or one score per transcript):
## possible format: SOME_TAG=., or SOME_TAG=.:0.5:.:0.44 or SOME_TAG=0.98 or tag is lacking
def get_predictor_score(record, score_name, threshold, is_higher_score_patho):
    selected_score = None
    ## get predictor score
    score = get_string_info_field_or_none(record, score_name)  # string/none
    try:
        score = score[0]
        try:
            score = score.split(":")
            check_score = bool([i for i in score if i not in [".", None]])
        ## score fiels is Float
        except AttributeError:
            check_score = score is not None
            score = [score]

        ## only dots or None in predictor field
        if not check_score:
            return None
    ## field not present
    except TypeError:
        return None

    ## check appris field: format principal1:.:principal2:alternative1
    selected_scores = check_appris(record, score)
    ## appris field not present, or lack of predictions for principal appris transcripts
    if selected_scores is None:
        selected_scores = score

    selected_scores = [float(i) for i in selected_scores if i not in [".", None]]
    selected_score = sum(selected_scores) / len(selected_scores)

    if is_higher_score_patho and selected_score <= threshold:
        return True, selected_score
    elif not is_higher_score_patho and selected_score >= threshold:
        return True, selected_score
    else:
        return False, selected_score


def main(vcf_name, vcf_writer_name, predictor_dict):
    # Doddowanie ISEQ_PP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BP4_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BP4 (Supporting evidence of benign character: A positive value is given to variants benign according "
              "to more than 50% of the predictors or variants with dbscSNV splicing scores lower than or qual to 0.6. "
              "Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'dbscSNV - ISEQ_ada_score and ISEQ_rf_score, dbNSFP - other scores'),
             ('Version', 'dbNSFP: v4.2c, dbscSNV: v1.1')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_PP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BP4_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BP4_SCORE"),
             ('Source', 'dbscSNV - ISEQ_ada_score and ISEQ_rf_score, dbNSFP - other scores'),
             ('Version', 'dbNSFP: v4.2c, dbscSNV: v1.1')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():

        description = ""
        splicing_description = ""
        aloft_description = ""
        predictor_names = []
        splicing_predictor_names = {}
        quorum_predictors_with_data = 0
        splicing_predictors_with_data = 0
        splicing_score = None
        ada_score = "NA"
        rf_score = "NA"
        benign_scores = 0

        ## aloft for lof:
        lof_score = check_aloft(record)
        # print(lof_score)
        if lof_score is not None:
            if lof_score == 0.5:
                impact = "recessive disease-causing"
                quorum_predictors_with_data += 1
                predictor_names.append("Aloft")
            elif lof_score == 1.0:
                impact = "dominant disease-causing"
                quorum_predictors_with_data += 1
                predictor_names.append("Aloft")
            else:
                impact = "tolerated"
                quorum_predictors_with_data += 1
                benign_scores += 1
            aloft_description = " According to the Aloft predictor, this is a {} premature stop variant.".format(impact)

        ## predictors quorum for missense:
        for field in predictor_fields:
            is_benign = get_predictor_score(record, field, get_threshold(field, predictor_dict),
                                            get_direction(field, predictor_dict))
            if is_benign is None:  ## lack of data: None
                pass
            elif is_benign[0]:  ## pathogenic: True
                benign_scores += 1
                quorum_predictors_with_data += 1
                predictor_names.append(get_predictor_name(field, predictor_dict))
            else:  ## benign: False
                quorum_predictors_with_data += 1

        ## splicing
        for field in splicing_predictors_fields:
            is_splicing_benign = get_predictor_score(record, field, get_threshold(field, predictor_dict),
                                                     get_direction(field, predictor_dict))
            if is_splicing_benign is None:  ## lack of data: None
                pass
            elif is_splicing_benign[0]:
                try: ## benign: True
                    splicing_score += 1
                except TypeError:
                    splicing_score = 1

                splicing_predictors_with_data += 1
                splicing_predictor_names[get_predictor_name(field, predictor_dict)] = is_splicing_benign[1]
            else:  ## patho: False
                splicing_predictors_with_data += 1
                splicing_score = 0
        ## deal with contardictory ada and rf predicions
        if bool(splicing_score):
            splicing_score = 0 if splicing_score / splicing_predictors_with_data < 1 else 1

        if bool(splicing_score):
            try:
                ada_score = round(splicing_predictor_names["ADA"], 3)
            except KeyError:
                pass
            try:
                rf_score = round(splicing_predictor_names["RF"], 3)
            except KeyError:
                pass

            splicing_description = "unlikely to alter conserved sequence motifs at splice sites which may lead to abnormal" \
                                   "splicing and gene silencing (dbscSNV ADA score: {}, dbscSNV RF score: {}).".format(
                ada_score,
                rf_score)

        try:
            quorum_score = benign_scores / quorum_predictors_with_data

            if quorum_score <= 0.5:
                quorum_score = 0.0
                patho = quorum_predictors_with_data - benign_scores
                description += "This variant is not benign according to at least half of the applied coding-sequence predictors" \
                               "({} out of {} for which data is available). ".format(patho, quorum_predictors_with_data)
                if bool(splicing_score):
                    splicing_description = " But it is " + splicing_description
            else:
                description += "This variant is benign according to the majority of the applied coding-sequence predictors: " \
                               "{} ({} out of {} for which data is available). ".format(", ".join(predictor_names),
                                                                                        benign_scores,
                                                                                        quorum_predictors_with_data)
                if bool(splicing_score):
                    splicing_description = " It is also " + splicing_description

            if quorum_predictors_with_data < 4:
                quorum_score = quorum_score / 2

        except ZeroDivisionError:
            quorum_score = None
            if splicing_predictors_with_data == 0:
                description = "Lack of predictor data for the variant."
            else:
                if splicing_score == 0:
                    splicing_description = "This variant might alter splicing (dbscSNV database)."
                else:
                    splicing_description = "This variant is " + splicing_description

        if quorum_score is None and splicing_score is None:
            score = 0.0
        elif quorum_score is None:
            score = splicing_score
        elif splicing_score is None:
            score = quorum_score
        else:
            score = min([quorum_score, splicing_score])
        description += splicing_description
        description += aloft_description
        description = description.strip().replace(" ", "^").replace(";", ",^").replace(",", "*").replace("^^", "^")

        record.info["ISEQ_ACMG_BP4_SCORE"] = float(score)
        record.info["ISEQ_ACMG_BP4_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PP3')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='Input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='Output vcf.gz file')
    parser.add_argument('--predictor', '-p', type=str, required=True, help='Predictor data json')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    predictor_data = read_json(args.predictor)
    main(in_vcf, out_vcf, predictor_data)
