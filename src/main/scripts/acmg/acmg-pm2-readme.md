### **This file describes how acmg-pm2.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PM2 criteria:   
*"Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium"*   
The results are given as score (PM2 SCORE described below) and text descriptions.       

To achieve the above goal acmg-pm2.py does the following:   
NUCLEAR VARIANTS: 
Script checks overall frequency in gnomAD exomes, or in chosen population:  
* If frequency is above 1e-04: PM2 SCORE = 0  
*  If frequency is very low (below 1e-04): PM2 SCORE = 1 
   * if the frequency is not given then scripts checks frequency in gnomAD genomes v3 database:  
       * If frequency is above 1e-04: PM2 SCORE = 0 
       * If frequency is very low (below 1e-04): PM2 SCORE = 1  
       * If frequency is not given:  
            it checks coverage (for the gnomAD exomes/genomes) in the given position and:  
            * if fraction of samples with coverage > 0 is greater than 0.8:  PM2 SCORE = 1.0  
            * if the above fraction is lower or if coverage is not given: PM2 SCORE = 0.25 
       
       
file gnomad-population.json gives possible values for the --population argument (keys from the json file):      
population name (three letter abbreviation) => description,   
if this argument is not specified or is not valid => the overall frequency in the gnomAD exomes database is used

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)
