#!/usr/bin/python3

import pysam
import argparse
import json
from typing import List

__version__ = '1.0.7'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def check_clinvar_ms(gene):
    try:
        clinvar_ms_dict[gene]
    except KeyError:
        description = "No pathogenic missense variation of the " + gene + " gene is described in the ClinVar database."
        score = 0
    else:
        patho_ms, benign_ms = clinvar_ms_dict[gene][1:]
        if patho_ms == 0:
            description = "Missense variation of the " + gene + " gene is not a common mechanism od disease: no pathogenic missense variants are described in the ClinVar database."  # common mechanism of disease
            score = 0
        elif patho_ms > (2 * benign_ms):
            if patho_ms > 1:
                description = "Missense variants of the " + gene + " gene are a common mechanism of disease. ClinVar database describes " + str(
                    patho_ms) + " pathogenic missense variants for this gene and they make up more than 2/3 of all known missense mutations."
                score = 1
            else:
                description = "Missense variants of the " + gene + " gene are a known mechanism of disease. One pathogenic missense variant is descibed in the ClinVar database and benign missense mutations for this gene are not known."
                score = 1
        elif patho_ms <= (2 * benign_ms):
            if patho_ms > 1:
                description = "Missense variants of the " + gene + " are not a common mechanism of disease. While ClinVar database describes " + str(
                    patho_ms) + " pathogenic missense variants for this gene, they make up only a tiny fraction of all known missense mutations."
                score = 0
            else:
                description = "Missense variants of the " + gene + " are not a common mechanism of disease. Clinvar database describes only one pathogenic and " + str(
                    benign_ms) + " benign missense variants."
                score = 0
    return [score, description]


def check_missense(transcripts):
    good_transcripts = find_good_transcripts_ps1(transcripts)
    n = 0
    genes = set()
    for transcript in good_transcripts:
        if 'missense' in transcript.variant_type():
            genes.add(transcript.transcript_gene())
            n += 1
    if n > 0:
        score = 1
        description = "This variant leads to amino acid substitution."
    else:
        score = 0
        description = "This is not a missense variant."
    return [score, description, genes]


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_PP2_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PP2_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PP2 (Supporting evidence of pathogenicity): A positive score is given to missense variants in genes for which other pathogenic amino acid substitutions are known and for which ratio of benign to pathogenic missense mutations is lower than 1/3.Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 10-12-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_PP2_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PP2_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PP2_SCORE"),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 10-12-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():

        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            final_score = 0
            final_description = "PP2^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"
            record.info["ISEQ_ACMG_PP2_SCORE"] = float(final_score)
            record.info["ISEQ_ACMG_PP2_DESCRIPTION"] = final_description
            vcf_writer.write(record)
            continue

        score, description, genes = check_missense(transcripts)
        if score == 0:
            final_score = 0
            final_description = description
        else:
            if len(genes) == 1:
                gene = list(genes)[0]
                result = check_clinvar_ms(gene)
                final_gene_score, final_gene_description = result[0], "It affects the " + gene + " gene. " + result[1]
            elif len(genes) > 1:
                final_gene_score = 0
                final_gene_description = "It affects the following genes: " + ", ".join(genes) + "."
                for gene in list(genes):
                    result = check_clinvar_ms(gene)
                    final_gene_score += result[0]
                    final_gene_description += " " + result[1]
            if final_gene_score == 0:
                final_score = 0
            else:
                final_score = 1
            final_description = description + " " + final_gene_description

        final_description = final_description.replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_PP2_SCORE"] = float(final_score)
        record.info["ISEQ_ACMG_PP2_DESCRIPTION"] = final_description
        vcf_writer.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PP2')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--clinvar-ms', '-c', metavar='clinvar_ms', type=str, required=True,
                        help='ClinVar missense variants dictionary')
    args = parser.parse_args()
    clinvar_ms_dict = read_json(args.clinvar_ms)
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
