### **This file describes how acmg-bp7.py should work**  
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BP7 criteria (extended to all non-coding 
sequence variants):  
*"BP7 A synonymous (silent) variant for which splicing prediction algorithms predict no impact to the splice consensus sequence nor the creation of a new splice site AND the nucleotide is not highly conserved"*   

The results are given as score (BP7 SCORE described below) and text descriptions."

To achieve the above goal acmg-bp7.py does the following:
It checks:
1) whether given variant leads to protein level changes, according to the SnpEff IMPACT field
(summarized in the ISEQ_HIGHEST_IMPACT annotation). Only protein-coding transcripts without WARNINGS/ERRORS 
added by SnpEff are considered) and   
2) splicing predictor results (dbscSNV ADA and RF scores):   

The final BP7 score is:
 * 0.0 if the variant is predicted to disrupt splicing (with at least one predictor) or the highest impact is HIGH or MODERATE  
 * 1.0 otherwise:  
    * the highest impact field is not present (lack of any reliable protein coding transcript) 
    * the highest impact is LOW (for example synonymous variants) or MODIFIER (for example intron variant).
 

It does not check conservation.    

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

