### **This file describes how acmg-pm1.py should work**    

The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PM1 criteria:  
*"PM1 Located in a mutational hot spot and/or critical and well-established functional domain (e.g., active site of an enzyme) without benign variation"*   
The results are given as score (final PM1 SCORE score described below) and text descriptions.   

To achieve the above goal acmg- pm1.py does the following:  
1) **Critical and well-establish functional domain without benign variation** part   
Script checks if given ISEQ variant leads to one of the listed below protein changes:   
amino acid substitution, frameshift, stop gain, stop/start loss, inframe deletion, inframe insertion or splice acceptor/donor site 
disruption (only protein coding transcripts without errors/warnings are considered).  
Then it checks if such variant lies within the NextProt functional region (uses snpEff nextProt annotations). 
In addition, script checks if variant is annotated by SNPEff as "protein_protein_contact" or "structural_interaction_variant" ##these annotations are not present in the GRCh38.99 SnpEff database   
**Score from this part is**:  
* 1 for protein changing variants localised within functional region without benign variation
* 1 for protein changing variants predicted by SnpEff to change protein residue important for structural conformation or protein-protein interactions (this annotation is also UniProt based)     
* 0 for other variants (different type, not in functional region, in functional region with benign variation)   
   
2) **Mutational hot spot** part   
Script checks if variant is located within 31bp (+/-15bp) DNA region with more than 2 pathogenic or likely pathogenic variants (described in the ClinVar database).  
It uses the *clinvar_pathogenic_sites.tab* file for this task.   
**Score from this part is**:  
* 1 for variants that lies in the hot spot region   
* 0 for all other variants   

**The final PM1 SCORE is**: max of both scores    

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

