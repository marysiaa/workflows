### **This file describes how acmg-bp5.py should work**  
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BP5 criteria:
*"BP5 Variant found in a case with an alternate molecular basis for disease"*

The results are given as score (final BP5 SCORE described below) and text descriptions.

To achieve the above goal acmg-bp5.py does the following:
It checks for each variant its clinical significance in the ClinVar database.   
The significance is taken from the ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE field.     

The final BP5 SCORE is:  
* 1.0 for 'benign' and 'benign/likely_benign' variants   
* 0.75 for 'likely_benign' variants  
* 0 for all other variants (described as 'pathogenic', 'likely_pathogenic' or as of 'uncertain_significance' and for variants without ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE field)     

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)
