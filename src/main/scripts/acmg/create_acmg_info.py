import glob


def find_criteria_names():
    files=glob.glob("acmg_*.py")
    names = []
    for i in files:
        name = i.split('.')[0].replace('acmg_','').split("_")
        for j in name:
            names.append(j.upper())
    return names 

def write_info(name):
    info = '''##INFO=<ID=ISEQ_ACMG_'''+name+''',Number=.,Type=String,Description="ACMG '''+name+''' annotation. Format: 'Score | Alt allele | Description'">\n'''
    return info

def main():
    f = open("acmg_info_lines.txt", "w")
    names = find_criteria_names()
    names.sort()
    for name in names:
        info = write_info(name)
        f.write(info)
    f.close()
if __name__=='__main__': main()   


