#!/usr/bin/python3

import pysam
import argparse
from typing import List

__version__ = '1.1.2'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria:
      1) protein coding transcript,
      2) with mutation causing protein change, also splice acceptor or donor variants
      3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


# ISEQ_SIMPLEREPEAT not required
def check_if_in_nonrepeat_region(line):
    result = get_string_info_field_or_none(line, "ISEQ_SIMPLE_REPEAT")
    if result is None:
        score = 0
        description = "It is localised within nonrepeat region."
    else:
        score = 1
        description = "It is localised within repeat region."
    return score, description


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]
    except KeyError:
        return None


# ANN required
def check_mutation_type(transcripts):
    transcripts = find_good_transcripts_ps1(transcripts)
    n = 0
    n_dangerous = 0
    genes = set()
    types = set()
    for transcript in transcripts:
        gene = transcript.transcript_gene()
        if (("start_lost" in transcript.variant_type()) or ("stop_gained" in transcript.variant_type()) or (
                "frameshift" in transcript.variant_type()) or
                ("splice_acceptor" in transcript.variant_type()) or ("splice_donor" in transcript.variant_type()) or (
                        "missense" in transcript.variant_type()) or ("stop_lost" in transcript.variant_type())):
            n_dangerous += 1
        if "disruptive_inframe_i1nsertion" in transcript.variant_type():
            n += 1
            types.add("disruptive inframe insertion")
            genes.add(gene)
            # mutation = transcript.fields[10][2:]
        if "disruptive_inframe_deletion" in transcript.variant_type():
            n += 1
            types.add("disruptive inframe deletion")
            genes.add(gene)
        if (
                "inframe_insertion" in transcript.variant_type() and "disruptive_inframe_insertion" not in transcript.variant_type()):
            n += 1
            types.add("inframe insertion")
            genes.add(gene)
        if (
                "inframe_deletion" in transcript.variant_type() and "disruptive_inframe_deletion" not in transcript.variant_type()):
            n += 1
            types.add("inframe deletion")
            genes.add(gene)
    if (n == 0):
        score = 0
        description = "This variant does not lead to any in-frame deletion/insertion."
    elif (n > 0) and (n_dangerous == 0):
        score = 1
        if len(genes) > 1:
            description = "This variant leads to in-frame length changes (" + ", ".join(
                types) + ") of the following proteins:" + ", ".join(genes) + "."
        else:
            description = "This variant leads to in-frame length change (" + ", ".join(types) + ") of the " + ''.join(
                genes) + " protein."
    elif (n > 0) and (n_dangerous > 0):
        score = 0
        description = "This variant leads to in-frame deletion/insertion, but it may also have other, more severe consequences."
    else:
        score = 0
        description = ""
    return score, description


def main(vcf_name, vcf_writer_name):
    # Adding ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BP3_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BP3 (Supporting evidence of benign impact): A positive score is given to variants which cause in-frame insertion/deletion in in repeat region. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, UCSC - ISEQ_SIMPLE_REPEAT field'),
             ('Version', 'The same as in the ANN field (SnpEff), the same as in the  ISEQ_SIMPLE_REPEAT field (UCSC)')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BP3_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BP3_SCORE"),
             ('Source', 'SnpEff - ANN field, UCSC - ISEQ_SIMPLE_REPEAT field'),
             ('Version', 'The same as in the ANN field (SnpEff), the same as in the  ISEQ_SIMPLE_REPEAT field (UCSC)')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header


    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():

        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            score = 0
            description = "BP3^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"
            record.info["ISEQ_ACMG_BP3_SCORE"] = float(score)
            record.info["ISEQ_ACMG_BP3_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        repeat_score, repeat_description = check_if_in_nonrepeat_region(record)
        mut_type_score, mut_type_description = check_mutation_type(transcripts)
        if mut_type_score > 0:
            score = repeat_score * mut_type_score
            description = mut_type_description + " " + repeat_description
        else:
            score = mut_type_score
            description = mut_type_description
        description = description.replace(" ", "^").replace(",", "*")

        record.info["ISEQ_ACMG_BP3_SCORE"] = float(score)
        record.info["ISEQ_ACMG_BP3_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG BP3')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

