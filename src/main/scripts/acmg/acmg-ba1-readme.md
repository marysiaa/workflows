### **This file describes how acmg-ba1.py should work**  
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BA1 criteria:  
*"BA1 Allele frequency is >5% in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium"*  
The results are given as score (BA1 SCORE described below) and text descriptions.   

To achieve the above goal acmg-ba1.py checks frequency of ISEQ variants:
1. for nuclear variants: overall frequency in gnomAD exomes - if this is not given - in gnomAD genomes v3 database;   
2. for mitochondrial variants: in MITOMAP database  
   
* If the frequency is above 0.05: BA1 SCORE = 1  
* If the frequency is not given (variant is absent from gnomAD exomes/gnomAD genomes v3/MITOMAP databases): BA1 SCORE = 0  
* If frequency is less or equal 0.05: BA1 SCORE = 0  

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)
