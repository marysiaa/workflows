#!/usr/bin/python3

import pysam
import argparse
import json
from typing import List
from typing import Tuple
from typing import Union

__version__ = '2.0.2'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


class Transcript(object):


    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_id(self):
        return self.fields[6].split(".")[0]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]

    def transcript_feature_type(self):
        return self.fields[5]

    def transcript_protein_hgsv(self):
        return self.fields[10]


def get_variant_location(record:pysam.VariantRecord) -> Tuple[Union[str, int]]:
   return record.contig, record.pos


def check_n_patho(patho:pysam.TabixFile, chrom:str, pos:int) -> int:
    lower_boundary = max([pos - 16, 1])
    upper_boundary = pos + 15
    patho_list = []
    try:

        patho_variants = patho.fetch(chrom, lower_boundary, upper_boundary)
        for variant in patho_variants:
            patho_list.append(variant)

    except ValueError:
        pass
    return len(patho_list)



def find_good_transcripts_interaction(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript,
              2) with mutation affecting residue important for protein-protein interaction or structural conformation,
              3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "structural_interaction_variant" in transcript.variant_type() or
                "protein_protein_contact" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def check_interaction_sites(transcripts):  # protein interaction sites
    return find_good_transcripts_interaction(transcripts)


def find_good_transcripts(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria:
         1) protein coding transcript,
         2) with mutation causing protein change, also splice acceptor or donor variants
         3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or
                "stop_gained" in transcript.variant_type() or
                "frameshift" in transcript.variant_type() or
                "stop_lost" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or
                "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or
                "splice_acceptor" in transcript.variant_type() or
                "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def find_nextprot_transcripts(transcript_list: List):
    nextprot_transcripts = []
    for transcript in transcript_list:
        if transcript.variant_type() == "sequence_feature":
            nextprot_transcripts.append(transcript)
    return nextprot_transcripts


def get_transcripts(good_transcripts, nextprot_transcripts):  ## good_transcript non-empty
    good_nextprot_transcripts = []
    good_transcript_ids = [i.transcript_id() for i in good_transcripts]
    if nextprot_transcripts:
        good_nextprot_transcripts = [i for i in nextprot_transcripts if i.transcript_id() in good_transcript_ids]
        good_nextprot_transcripts_ids = [i.transcript_id() for i in good_nextprot_transcripts]
        good_transcripts = [i for i in good_transcripts if i.transcript_id() in good_nextprot_transcripts_ids]

    return good_transcripts, good_nextprot_transcripts


def make_effect_description(good_transcript):
    effect = good_transcript.variant_type()
    effect_desc = effect.replace("_", " ").replace("&", " and ").replace("variant", "").capitalize()
    return effect_desc


def make_nextprot_description(nextprot_transcript):
    feature = nextprot_transcript.transcript_feature_type()
    return feature.replace("_", " (", 1).replace("_", " ") + ")"


def pick_genes(aa_changes_list):
    try:
        genes = set([i[0] for i in aa_changes_list])
    except (IndexError, ValueError):
        genes = set()
    return genes


def main(vcf_name, vcf_writer_name):

    # Adding ISEQ_PM1_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PM1_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
             "PM1 (Moderate evidence of pathogenicity): "
             "A positive scores is given to variants "
             "1) localized in functional protein region without benign variation; "
             "2)  within mutational hot-spot "
             "3) predicted to change protein residues important for structure or protein-protein interactions. "
             "Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, NextProt, ClinVar'),
             ('Version', 'The same as in the ANN field (SnpEff), NextProt: 18-08-2022, ClinVar: 10-12-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_PM1_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PM1_DESCRIPTION"), ('Number', "1"), ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PM1_SCORE"),
             ('Source', 'SnpEff - ANN field, NextProt, ClinVar'),
             ('Version', 'The same as in the ANN field (SnpEff), NextProt: 18-08-2022, ClinVar: 10-12-2022')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w', header=header_vcf)

    for record in vcf_reader.fetch():
        description = ""
        score = 0

        try:

            transcripts = [Transcript(ann) for ann in record.info["ANN"]]

        except KeyError:
            description += "PM1^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"

            record.info["ISEQ_ACMG_PM1_SCORE"] = float(score)
            record.info["ISEQ_ACMG_PM1_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        ## transcripts with protein changes
        good_transcripts = find_good_transcripts(transcripts)
        nextprot_transcripts = find_nextprot_transcripts(transcripts)

        if not good_transcripts:
            description += "This variant does not lead to protein disruption/change."  # think about better description

        else:
            chosen_transcripts = get_transcripts(good_transcripts, nextprot_transcripts)
            # print(chosen_transcripts)
            if not chosen_transcripts[1]:

                description += " This variant does not change region critical for protein function."
            else:
                score += 1
                nextprot_desc = make_nextprot_description(chosen_transcripts[1][0])
                effect_desc = make_effect_description(chosen_transcripts[0][0])
                protein_hgsv = [i.transcript_id() + ": " + i.transcript_protein_hgsv() for i in chosen_transcripts[0]]
                gene = chosen_transcripts[0][0].transcript_gene()

                description += " {} variant ({}) changing region critical for the {} protein function: {}" \
                               ".".format(effect_desc, ", ".join(protein_hgsv), gene, nextprot_desc)  ## tutaj jestem

            interaction_residues = check_interaction_sites(transcripts)
            interaction_genes = set([i.transcript_gene() for i in interaction_residues])
            if interaction_residues:
                description += " The altered residues may be critical for structural conformation of the " \
                               "{} genes(s), or may mediate protein-protein interactions.".format(
                    ", ".join(interaction_genes))
                score += 1

            chromosome, position = get_variant_location(record)
            patho_count = check_n_patho(f_clinvar_patho, chromosome, position)
            if patho_count > 2:
                score += 1
                description += " Variant is located in the mutational hot spot, with " \
                               "{} known pathogenic mutations within the 31 bp surrounding.".format(str(patho_count))
            else:
                description += " Variant is located outside any known mutational hot spot."

        if score:
            score = 1

        description = description.strip().replace(" ", "^").replace(";", "*").replace(",", "*").replace("..",
                                                                                                        ".").replace(
            "^^", "^")

        record.info["ISEQ_ACMG_PM1_SCORE"] = float(score)
        record.info["ISEQ_ACMG_PM1_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PM1')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--clinvar-patho', '-p', metavar='clinvar_patho', type=str, required=True,
                        help='File with positions of all pathogenic/likely_pathogenic variants (bgzipped and with tabix index)')
    args = parser.parse_args()

    f_clinvar_patho = pysam.TabixFile(args.clinvar_patho, "r")
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
