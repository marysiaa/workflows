#!/usr/bin/python3
import sys
import pysam
import argparse

__version__ = '1.2.0'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def get_numeric_info_field_or_none(self, field_name: str):
    try:

        element = self.info[field_name]
        if type(element) == tuple:
            element = element[0]
        if element is None:
            return None

        return float(element)
    except (ValueError, KeyError):
        return None


def get_frequency_nuclear(variant):
    frequency_exome = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_EXOMES_popmax_AF")
    frequency_genome = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_GENOMES_V3_popmax_AF")
    if (frequency_exome is None) and (frequency_genome is None):
        return None, ""
    elif frequency_exome is None:
        return frequency_genome, "genomes"
    else:
        return frequency_exome, "exomes"


def get_frequency_mito(variant):
    return get_numeric_info_field_or_none(variant, "MITOMAP_AF")


def frequency_test(frequency, base_type):  # base_type: gnomAD vs MITOMAP
    if frequency is None:
        description = "Frequency of this variants is not given in the " + base_type + " database."
        score = 0
    elif frequency >= 0.05:
        description = "This variant is very common in the " + base_type + " database, as its frequency is {:.2f}".format(
            frequency) + ". This gives a stand alone evidence of benign impact."
        score = 1
    else:
        description = "Frequency of this variant in the " + base_type + " database is {:.2e}".format(
            frequency) + ", which is below the 5% ACMG BA1 threshold."
        score = 0
    return score, description


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BA1_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BA1 (Strong evidence of benign impact): A positive score is given to common variants (frequency higher than 0.05). Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'gnomAD - ISEQ_GNOMAD_EXOMES_AF and ISEQ_GNOMAD_GENOMES_V3_popmax_AF fields, MITMOAP -  ISEQ_MITOMAP_AF field'),
             ('Version', 'gnomAD: v2 and v3 MITOMAP: 20190927')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BA1_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BA1_SCORE"),
             ('Source', 'gnomAD - ISEQ_GNOMAD_EXOMES_AF and ISEQ_GNOMAD_GENOMES_V3_popmax_AF fields, MITMOAP -  ISEQ_MITOMAP_AF field'),
             ('Version', 'gnomAD: v2 and v3 MITOMAP: 20190927')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header


    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():

        if record.chrom == "chrM":
            frequency = get_frequency_mito(record)
            score, description = frequency_test(frequency, "MITOMAP")

        else:
            frequency, gnomad_type = get_frequency_nuclear(record)
            database = "gnomAD " + gnomad_type
            score, description = frequency_test(frequency, database)

        description = description.replace(" ", "^").replace(",", "*")

        record.info["ISEQ_ACMG_BA1_SCORE"] = float(score)
        record.info["ISEQ_ACMG_BA1_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG BA1')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
