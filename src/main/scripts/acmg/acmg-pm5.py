#!/usr/bin/python3

import pysam
import argparse
import json
from typing import List

__version__ = '1.2.7'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"

amino_acids = "Ala,Cys,Glu,Asp,Phe,Gly,His,Ile,Lys,Leu,Met,Gln,Asn,Pro,Arg,Ser,Thr,Tyr,Trp,Val"
pt = "pathogenic"
pt_low = "pathogenic_low_penetrance"
lpt = "likely_pathogenic"
lpt_low = "likely_pathogenic_low_penetrance"
pt_lpt = "pathogenic/likely_pathogenic"
bn = "benign"
lbn = "likely_benign"
bn_lbn = "benign/likely_benign"


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


"""Checks if variant fulfill  ACMG PM5 criteria:
1) Variant must be - missense or frameshift or stop gain
2) Similar protein modification (the same aa position, but different altered aa) is classified in ClinVar database as pathogenic (score1) or likely pathgenic (score 0.5)"""


def calculate_frameshift(ref, alt):
    dif = len(ref) - len(alt)
    shift = str(dif % 3)
    return "+" + shift


def check_type(str1, str2):
    if str1[2:] == "fs":
        if str2[2:] == "fs":
            return 1
        elif str2 == "*":
            return 1
        else:
            return 0
    elif str1 == "*":
        if str2[2:] == "fs":
            return 1
        else:
            return 0
    elif str1 in amino_acids:
        if str2 in amino_acids:
            return 1
        else:
            return 0


def find_aa_changes(line, transcripts, alt):
    good_transcripts = find_good_transcripts_ps1(transcripts)
    aa_changes_list = []
    for transcript in good_transcripts:
        variant_type = transcript.fields[1]
        aa_field = transcript.fields[10]
        aa_pos = transcript.fields[13].split('/')[0]
        gene = transcript.transcript_gene()
        transcript_id = transcript.fields[6]
        if "missense" in variant_type:
            aa_ref = aa_field[2:5]
            aa_alt = aa_field[-3:]
            vt = "This variant leads to the " + aa_ref + aa_pos + aa_alt + " substitution in the " + gene + " protein, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("stop_gained" in variant_type) and ("frameshift" not in variant_type):  # or (sl in variant_type):
            aa_ref = aa_field[2:5]
            aa_alt = '*'
            vt = "This variant introduces pre-mature stop codon. It truncates the " + gene + " protein in the " + aa_pos + " position, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "frameshift" in variant_type:
            aa_ref = aa_field[2:5]
            aa_alt = calculate_frameshift(line.ref, alt) + "fs"
            vt = "This variant leads to frameshift (" + aa_alt + "), which starts at the " + aa_pos + " amino acid position of the " + gene + " protein, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
    return aa_changes_list


def pick_genes(aa_changes_list):
    try:
        genes = [i[0] for i in aa_changes_list]
    except (IndexError, ValueError):
        genes = set()
    return set(genes)


def compare_changes(aa_changes_list, gene):
    changes = []
    scores = []
    try:
        clinvar_gene_changes_dict[gene]
    except KeyError:
        max_score = 0
    else:
        clinvar_list = clinvar_gene_changes_dict[gene]
        for mut in aa_changes_list:
            for cl in clinvar_list:
                if (mut[0] == gene) and (mut[1] == cl[0]) and (mut[2] != cl[1]) and (mut[3:5] == cl[2:4]):
                    known_change = cl[1]
                    if check_type(mut[2], known_change) == 1:
                        sign = cl[-1].split(":")  # new
                        if (pt in sign) or (pt_lpt in sign) or (pt_low in sign):
                            score = 1
                        elif (lpt in sign) or (lpt_low in sign):  # and (pt not in sign):
                            score = 0.75
                        else:
                            score = 0
                        change = mut[:] + [known_change] + [score]
                        changes.append(change)

                        scores.append(score)

                try:
                    max_score = max(scores)
                except ValueError:
                    max_score = 0
    changes_sorted = sorted(changes, key=lambda x: int(x[-1]), reverse=True)  # new
    return max_score, changes_sorted


def pick_worst_change(max_score, changes):
    worst_changes = [i[0:-1] for i in changes if i[-1] > 0]
    return worst_changes


def get_known_changes(change):
    return change[1] + change[3] + change[-1]


def pick_one_change(changes_list):
    # changes_sorted = sorted(changes_list, key = lambda x: int(x[-1]), reverse=True)
    # return changes_sorted[0]
    return changes_list[0]


def get_string_info_field_or_none(record, field_name):
    try:
        return record.info[field_name]
    except KeyError:
        return None


def check_acmg_ps1(record):
    result = get_string_info_field_or_none(record, "ISEQ_ACMG_PS1_DESCRIPTION")

    if result is None:
        return 0  # 2
    else:
        description = "".join(result)
        if ("Variants^introducing^the^same^change^are^classified^in^the^ClinVar^database" in description or
                "Variants^introducing^the^same^change^are^not^pathogenic^or^likely^pathogenic" in description):
            # print(description)
            return 1
        else:
            return 0


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_PM5_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PM5_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PM5 (Moderate evidence of pathogenicity): A positive score is given to variants changing protein residue where different change is known to be pathogenic or likely pathogenic. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 10-12-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_PM5_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PM5_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PM5_SCORE"),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 10-12-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        ps1_test = check_acmg_ps1(record)
        if ps1_test == 1:
            final_score = 0
            final_description = "This is not a novel variant."
        # elif ps1_test == 2:
        #    final_score = 0
        #    final_description = "Required ACMG PS1 description not present."
        else:
            try:
                transcripts = [Transcript(ann) for ann in record.info["ANN"]]
            except:
                final_score = 0
                description = "PM5^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"

                record.info["ISEQ_ACMG_PM5_SCORE"] = float(final_score)
                record.info["ISEQ_ACMG_PM5_DESCRIPTION"] = description
                vcf_writer.write(record)
                continue

            aa_changes = find_aa_changes(record, transcripts, record.alts[0])
            similar_changes = []
            scores = []
            if len(aa_changes) == 0:
                final_description = "This variant does not lead to amino acid substitution or protein truncation."

                final_score = 0
            elif len(aa_changes) > 0:
                genes = pick_genes(aa_changes)
                # print(genes)
                for gene in genes:
                    score, changes = compare_changes(aa_changes, gene)
                    similar_changes += changes
                    scores.append(score)
                if len(similar_changes) == 0:
                    final_description = "This is a protein changing variant. Variants introducing similar, but not identical, changes are not described in the ClinVar database."
                    final_score = 0
                else:
                    final_score = max(scores)
                    if final_score == 0:
                        final_description = "This is a protein changing variant. Variants introducing similar but not identical changes are not pathogenic or likely pathogenic, according to the ClinVar database."
                    elif final_score > 0:
                        worst_changes = pick_worst_change(final_score, similar_changes)
                        # print(worst_changes)
                        # known_changes = get_known_changes(worst_changes)
                        worst_changes_genes = pick_genes(worst_changes)
                        # print(worst_changes_genes)
                        if len(worst_changes_genes) == 1:
                            one_change = pick_one_change(worst_changes)
                            best_description = one_change[-2]
                            known_changes = get_known_changes(one_change)

                        else:
                            best_description = ""
                            known_changes_list = []
                            for i in worst_changes_genes:
                                gene_worst_changes = [x for x in worst_changes if x[0] == i]
                                # print(gene_worst_changes)
                                gene_one_change = pick_one_change(gene_worst_changes)
                                # print(gene_one_change)
                                best_description += " " + gene_one_change[-2]
                                # print(best_description)
                                known_changes_list.append(i + ": " + get_known_changes(gene_one_change))
                            known_changes = ", ".join(known_changes_list)
                        final_description = best_description + " Mutations introducing similar changes are classified in the ClinVar database as "
                        if final_score == 0.75:
                            final_description += "likely pathogenic (" + known_changes + ")."
                        elif final_score == 1 and len(worst_changes_genes) == 1:
                            final_description += "pathogenic (" + known_changes + ")."
                        elif final_score == 1 and len(worst_changes_genes) > 1:
                            final_description += "pathogenic/likely pathogenic (" + known_changes + ")."
        description = final_description.replace(" ", "^").replace(",", "*")

        record.info["ISEQ_ACMG_PM5_SCORE"] = float(final_score)
        record.info["ISEQ_ACMG_PM5_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PM5')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--clinvar-changes', '-c', metavar='clinvar_changes', type=str, required=True,
                        help='ClinVar all protein changes dictionary')
    args = parser.parse_args()
    clinvar_gene_changes_dict = read_json(args.clinvar_changes)

    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
