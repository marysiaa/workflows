### **This file describes how acmg-ps1.py should work**

The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG_PS1 criteria:   
*"PS1 Same amino acid change as a previously established pathogenic variant regardless of nucleotide change"*   
The results are given as score (total PS1 SCORE described below) and text descriptions.   

To achieve the above goal acmg-ps1.py does the following:   
It checks if given ISEQ variant leads to protein change (only protein coding transcripts without errors/warnings are considered) and compares if the same change (at the protein level) is described as "pathogenic" or "likely pathogenic" in ClinVar annotated vcf file (by SnpEff).    
Program compares: gene name, transcript id and protein level mutation (the last task works differently for different mutation types, see below).   
Changes in proteins caused by known variants for each gene are described in the clinvar-protein-changes-dictionary.json file.  
Note, that different mutations (in different position of the gene) may lead to the same amino acid change.   
Mutation types and what is assumed to cause the same change as in ClinVar:    
- missense (the same ref, alt and amino acid position)
- start lost (the same ref, alt and position)
- stop gained (the same ref and position; the downstream variants are not checked as this type of mutation may be harmful not because of gene deactivation but due to toxic polypeptide production)
- stop lost (the same ref and position)
- frameshift (the same ref, position and frameshift "phase"; the phase check is added due to the fact that +1 and +2 frameshifts lead to production of different polypeptides) 
- inframe insertion (also disruptive inframe insertion) (the same ref and position)
- inframe deletion (also disruptive inframe deletion) (the same ref and position)
- splice acceptor site change (the same ref and position)
- splice donor site change (the same ref and position)   

The total PS1 SCORE is 0.75 for variants with described in ClinVar as "Likely pathogenic" and 1.0 for "Pathogenic" or "Pathogenic/Lilely pathogenic" variants.   
Variants which are described as 'Benign', 'Likely benign' or of 'Uncertain significance', as wel as variants without matching ClinVar record are scored 0.0 points.  

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)


