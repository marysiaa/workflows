### **This file describes how acmg-pp3.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PP3 criteria:   
*"PP3 Multiple lines of computational evidence support a deleterious effect on the gene or gene product (conservation, evolutionary, splicing impact, etc.)"*   

To achieve this aim acmg-pp3.py does the following:   
It checks outputs of the following coding-sequence variant pathogenicity predictors 
(provided as the precomputed scores in the dbNSFP database v4.2c):
 * BayesDel_addAF (score > 0.0692655; PMID: 27995669)
 * DANN (score > 0.96; PMID: 25338716)
 * DEOGEN2 (score > 0.5; PMID: 28498993)
 * FATHMM-MKL (score > 0.5; PMID: 25583119)
 * LIST-S2 (score > 0.85; PMID: 32352516)
 * M-CAP (score > 0.025; PMID: 27776117)
 * MVP (score > 0.75; PMID: 33479230)
 * PrimateAI (score > 0.8; PMID: 30038395)
 * MutationAssessor (score > 2.26; PMID: 21727090)
 * SIFT4G (score < 0.05; PMID: 11337480)
 * Aloft (classified as disease-causing recessive or dominant variant; PMID: 28851873)  

In addition, we use two dbscSNV splicing scores: 
 * ADA (score > 0.6; PMID: 25416802)
 * RF (score > 0.6; PMID: 25416802)    

In the parenthesis I listed the conditions used for the pathogenic variants and
PMID for the article with the threshold value recommendation.
For scores provided individually for each transcript script uses scores assigned to the 
principal APPRIS transcripts (with the lowest number). If scores for the principal transcripts are
not provided, or any of the gene transcripts is classified as principal, scrip uses the averaged across all 
transcripts score. 


The final PP3 score is:
 * 1.0 for variants predicted to affect conserved splicing site (ADA or RF algorithm)
 * Fraction of the coding-sequence predictors giving pathogenic classification (within all predictors with data) - if more than half predictions is pathogenic and the overall number of predictions is above 3;
 * Half of the above fraction - if more than half predictions is pathogenic and the overall number of predictions is low (three or less)
 * 0.0 for variants not affecting splicing (or with contradicting splicing predictions) and classified as benign with at least half of the coding-sequence predictors.
 * 0.0 for variants without predictor data  


 
[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)
