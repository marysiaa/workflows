### **This file describes how acmg-bp3.py should work**  
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BP3 criteria:  
*"BP3 In-frame deletions/insertions in  a  repetitive region without a known function"*  
  
The results are given as score (final BP3 SCORE described below) and text descriptions.  

To achieve the above goal acmg-bp3.py does the following:  
It checks if given variant leads to one of the following changes (in protein coding transcripts, without errors/warnings added by SnpEff):  
* inframe deletion/insertion  
* disruptive inframe deletion/insertion  
* it also ensures that more severe consequence are not predicted for any transcript affected by the variant (missense, stop lost or gain, splice donor/acceptor site change, frameshift, start lost)      

Then for the appropriate variants program checks if they are in repeat region (specified as simpleRepeat region UCSC)   
   
The final BP3 SCORE is:   
* 1 for variants causing inframe deletion/insertion in repeat region  
* 0 for variants causing inframe deletion/insertion in nonrepeat region   
* 0 for variants causing inframe deletion/insertion but also more severe changes    
* 0 for all other variants 

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)
