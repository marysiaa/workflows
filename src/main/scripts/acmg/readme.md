### **ACMG annotations description**

All scripts evaluate whether variants fulfil given ACMG criterion.   
Results are added to the input vcf file as the ACMG SCORE and ACMG DESCRIPTION  
  
#### **Input file format**:  
 Imput file: Annotated ISEQ vcf, must be "splitted" with only one alt allele per line  
 
 #### Table of Contents
  * [ACMG PVS1](#acmg-pvs1)
  * [ACMG PS1](#acmg-ps1)    
  * [ACMG PS3](#acmg-ps3)  
  * [ACMG PM1](#acmg-pm1) 
  * [ACMG PM2](#acmg-pm2)
  * [ACMG PM4](#acmg-pm4)  
  * [ACMG PM5](#acmg-pm5) 
  * [ACMG PP2](#acmg-pp2)
  * [ACMG PP3](#acmg-pp3) 
  * [ACMG PP4](#acmg-pp4)
  * [ACMG BA1](#acmg-ba1) 
  * [ACMG BS2](#acmg-bs2)
  * [ACMG BP1](#acmg-bp1)
  * [ACMG BP3](#acmg-bp3)
  * [ACMG BP4](#acmg-bp4)
  * [ACMG BP5](#acmg-bp5)  
  * [ACMG BP7](#acmg-bp7)
  * [ACMG SUMMARY](#acmg-summary) 
********
***

#### ACMG PVS1      
[**acmg-pvs1.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pvs1.py)     
`PVS1 null variant (nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single or multiexon deletion) in a gene where LOF is a known mechanism of disease`  
  
**Usage**:    
```bash
python3 acmg-pvs1.py \
        --input-vcf input.vcf \
        --output-vcf annotated-with-acmg.vcf.gz \
        --clinvar-lof clinvar-lof-dictionary.json \
        --gnomad-lof gnomad-lof-dictionary.json \
        --loftee-filters loftee-filters.json
```   

The **PVS1 SCORE**  is the sum of:  
  * **variant score**, which reflects how likely given variant leads to loss of function; based
    on [LOFTEE](https://github.com/konradjk/loftee/blob/grch38/README.md) min = 0, max = 0.5
  * **gene score**, which reflects how likely null mutation of the particular gene is harmful (based on ClinVar and gnomAD lofoe constraint); min = 0, max 0.5.   
       Note, that gene score is **not calculated** for non LOF variants (that is, if variant score equals 0 => final score is 0).   
       Final score is halved for variants with gene score = 0 
      
 For further details about calculation of scores, see:  [**acmg-pvs1-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pvs1-readme.md)  

**Required files**:  
 * clinvar-lof-dictionary.json   
 * gnomad-lof-dictionary.json 
 * loftee-filters.json  
  
Information on location and how to create/update these files can be found in: [**readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/acmg/readme.md)    

**Required annotations:**   
Fields added by SnpEff (ANN)  
Fields added by VEP (CSQ) and loftee VEP plugin: (LoF, LoF_filter, LoF_flag) - inserted into the CSQ field   
   
[Return to the table of contents](#table-of-contents)

****
***
#### ACMG PS1
[**acmg-ps1.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-ps1.py)   
`PS1 Same amino acid change as a previously established pathogenic variant regardless of nucleotide change`    

**Usage**:   
```bash
python3 acmg-ps1.py \
       --input-vcf input.vcf \
       --output-vcf annotated-with-acmg.vcf.gz \
       --clinvar-changes clinvar-protein-changes-dictionary.json
```   
                     
The **PS1 SCORE** is:   
* 0.75 for variants leading to protein changes described in ClinVar as 'Likely pathogenic'  
* 1.0 for variants leading to protein changes described in ClinVar as 'Pathogenic' or 'Pathogenic/Likely pathogenic'     
* 0.0 for all other variants  

For further details about final score calculation, see: [**acmg-ps1-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-ps1-readme.md)

**Required files**:    
 * clinvar-protein-changes-dictionary.json  
   
Information on location and how to create/update this file can be found in: [**readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/acmg/readme.md)  
  
**Required annotations:**   
Fields added by SnpEff (ANN)   

[Return to the table of contents](#table-of-contents)
*************
***************

#### ACMG PS3  
[**acmg-ps3.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-ps3.py)  
`PS3 Well-established in vitro or in vivo functional studies supportive of a damaging effect on the gene or gene product`   

**Usage**:  
```bash
python3 acmg-ps3.py \
        --input-vcf input.vcf \
        --output-vcf annotated-with-acmg.vcf.gz \
        --uniprot-mut uniprot-mutagenesis-data-from-table.json \
        --aa aa-symbols.json  
```   

The **PS3 SCORE** is:
* 1.0 for variants leading to protein missense changes described in the Uniprot mutagenesis section as having negative effect on function    
* 0.25 for variants leading to protein missense changes similar to mutations described in the Uniprot mutagenesis section as having negative effect on function   
* 0.5 for variants leading to protein missense changes described in the Uniprot mutagenesis section as causing protein hyperactivity       
* 0.125 for variants leading to protein missense changes similar to mutations described in the Uniprot mutagenesis section as causing protein hyperactivity   
* 0.0 for other types of variants and for missenses without mutagenesis record or hot changing protein performance    
           
For further details about final score calculation and "similar changes" definition, and on description classification, see: [**acmg-ps3-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-ps3-readme.md)     
**Required files**:  
 * [**aa-symbols.json**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/aa-symbols.json)     
 * uniprot-mutagenesis-data-from-table.json        
   
Information on location and how to create/update **uniprot-mutagenesis-data-from-table.json** can be found in: [**readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/acmg/readme.md)  

**Required annotations:**   
Fields added by SnpEff (ANN)  

**#TODO**: improve the classification algorithm     
**#TODO**: all descriptions can be viewed by typing `cat uniprot-mutagenesis-data-from-table.json | jq .[][][4]`  

[Return to the table of contents](#table-of-contents)   
*********
*********

#### ACMG PM1     
[**acmg-pm1.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm1.py)     
`PM1 Located in a mutational hot spot and/or critical and well-established functional domain (e.g., active site of an enzyme) without benign variation`  

**Usage**:
```bash
python3 acmg-pm1.py \
        --input-vcf input.vcf \
        --output-vcf anotated-with-acmg.vcf.gz \
        --clinvar-patho clinvar-pathogenic-sites.tab.gz 
```

The **PM1 SCORE** is:   
* 1.0 for variants leading to protein changes (for example missense, frameshift, stop gain, amino acid insertion/deletion) which are located within UniProt functional region without known benign variation   
* 1.0 for variants leading to protein changes (for example missense, frameshift, stop gain, amino acid insertion/deletion), which are predicted by SnpEff to affect residues important for protein-protein interactions or protein structure   
* 1.0 for variants (any type) which are localised within mutational hot spot (31bp region with more than 2 pathogenic/likely pathogenic variants in the ClinVar database)      
* 0.0 for variants which do not fulfill any of the above criteria   

For further details on final score calculation, as well as on definitions of: mutational hot spot, residues important for structure, residues important for protein-protein interactions, UniProt functional sites, see: [**acmg-pm1-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm1-readme.md)  

**Required files**:   
   
 * clinvar-pathogenic-sites.tab.gz 
 * clinvar-pathogenic-sites.tab.gz.tbi

Information on location and how to create/update the above files can be found in: [**readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/acmg/readme.md)  
  
**Required annotations:**   
Fields added by SnpEff (ANN) - with NextProt database added  


**#TODO**: structural interaction data not in SnpEff database GRCh38.99 (and 100, 101, 102 databases), add directly from PDB  

[Return to the table of contents](#table-of-contents)        
*********  
********** 

#### ACMG PM2
[**acmg-pm2.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm2.py)  
`PM2 Absent from controls (or at extremely low frequency if recessive) (table 6) in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium`

**Usage**:   
```bash
python3 acmg-pm2.py \
        --input-vcf input.vcf \
        --output-vcf annotated-with-acmg.vcf.gz
```
The **PM2 SCORE** is:
* 1.0 for variants absent from gnomAD exomes and gnomAD genomes v3 databases   
and in regions of good coverage or for variants present at frequency below 0.0001   
* 0.25 for variants absent gnomAD exomes/genomes database regions of poor coverage
* 0.0 for relatively common variants (frequency higher than or equal 0.0001)   
For further details about final score calculation and possible values of the --population argument, see: [**acmg-pm2-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm2-readme.md)


**Required annotations:**   
ISEQ_GNOMAD_EXOMES_popmax_AF (overall frequency required; fields for frequencies in the particular populations needed if population argument added)
ISEQ_GNOMAD_GENOMES_V3_popmax_AF (overall frequency required; fields for frequencies in the particular populations needed if population argument added)    
MITOMAP_AF     
ISEQ_GNOMAD_COV_MEAN (gnomAD exomes)   
ISEQ_GNOMAD_COV_OVER (gnomAD exomes)   


[Return to the table of contents](#table-of-contents)     
*******
*******

#### ACMG PM4  
[**acmg-pm4.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm4.py)    
`PM4 Protein length changes as a result of in-frame deletions/insertions in a nonrepeat region or stop-loss variants`  
**Usage**:  
```bash
python3 acmg-pm4.py \
        --input-vcf imput.vcf \
        --output-vcf annotated-with-acmg.vcf.gz
```   
The **PM4 SCORE** is:  
* 1.0 for mutations leading to in-frame deletions/insertions in nonrepeat region and stop-loss.
* 0.0 otherwise  

For further details about final score calculation, see: [**acmg-pm4-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm4-readme.md)

**Required annotations:**  
SnpEff annotation (ANN)  
UCSC simpleRepeat annotation (ISEQ_SIMPLE_REPEAT) (added only to lines within repeat regions)   

[Return to the table of contents](#table-of-contents)
****************
*****************

#### ACMG PM5    
[**acmg-pm5.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm5.py)    
`PM5 Novel missense change at an amino acid residue where a different missense change determined to be pathogenic has been seen before; Example: Arg156His is pathogenic; now you observe Arg156Cys`  
`Caveat: Beware of changes that impact splicing rather than at the amino acid/protein level.`  

**Usage**:  
```bash
python3 acmg-pm5.py \
        --imput-vcf input.vcf \
        --output-vcf annotated-with-acmg.vcf.gz \
        --clinvar-changes clinvar-protein-changes-dictionary.json  
```

The **PM5 SCORE** is:  
* 0.75 for novel protein level changes similar to changes described in ClinVar as 'Likely pathogenic'
* 1.0 for novel protein level changes similar to those described in ClinVar as 'Pathogenic' or 'Pathogenic/Likely pathogenic'
* 0.0 otherwise
  
Note, that script is fully functional only if run on vcf annotated with ACMG PS1 (description). When this annotation is not present all variants are asumed to be nove.   
For further details about score calculation and "similarity" definition, see: [**acmg-pm5-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pm5-readme.md)  

**Required files**:  
* clinvar-protein-changes-dictionary.json    
Information on location and how to create/update **clinvar-protein-changes-dictionary.json** can be found in: [**readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/acmg/readme.md)    

**Required annotations:**  
Fields added by SnpEff (ANN)  
ISEQ_PM5_DESCRIPTION (used for checking, whether mutation is novel)  

**#TODO**: test *calculate_frameshift* function  

[Return to the table of contents](#table-of-contents)
**********************
*********************

#### ACMG PP2 
[**acmg-pp2.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pp2.py)    
`PP2 Missense variant in a gene that has a low rate of benign missense variation and in which missense variants are a common mechanism of disease`     

**Usage**:  
```bash
python3 acmg-pp2.py  \
        --input-vcf imput.vcf \
        --output-vcf  annotated-with-acmg.vcf.gz \
        --clinvar-ms clinvar-ms-dictionary.json
```  

The **PP2 SCORE** is:  
* 1.0 for missense variant in gene for which number of known missense pathogenic variants exceeds twice the number of benign missense variants (based on ClinVar database)  
* 0.0 for non missense variants and missense variants which do not fulfill the above criterion    

For further details about score calculation, see: [**acmg-pp2-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pp2-readme.md)       

**Required files**:  
* clinvar-ms-dictionary.json  
  
Information on location and how to create/update **clinvar-ms-dictionary.json** can be found in: [**readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/acmg/readme.md)   

**Required annotations:**  
Fields added by SnpEff (ANN)  

[Return to the table of contents](#table-of-contents)
********************   
********************  

#### ACMG PP3  
[**acmg-pp3.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pp3.py)  
`PP3 Multiple lines of computational evidence support a deleterious effect on the gene or gene product (conservation, evolutionary, splicing impact, etc.)`  

**Usage**:  
```bash
python3 acmg-pp3.py \
        --input-vcf imput.vcf  \
        --output-vcf annotated-with-acmg.vcf.gz \
        --predictor predictor-data.json  
```

The **PP3 SCORE** is:  
 * 1.0 for variants predicted to affect conserved splicing site (ADA or RF algorithm)
 * Fraction of the coding-sequence predictors giving pathogenic classification (within all predictors with data) -
   if more than half predictions is pathogenic and the overall number of predictions is above 3;
 * Half of the above fraction - if more than half predictions is pathogenic and the overall number of predictions is
   low (three or less)
 * 0.0 for variants not affecting splicing (or with contradicting splicing predictions) and classified as benign with at least half of the coding-sequence predictors.
 * 0.0 for variants without predictor data  
   
For further details about calculation of the scores, see: [**acmg-pp3-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pp3-readme.md)  

**Required annotations:**  
*for splicing:*   
ISEQ_ada_score  
ISEQ_rf_score  
 
*for the coding-sequence (dbNSFP):*    
dbNSFP_M_CAP_score  
dbNSFP_PrimateAI_score  
dbNSFP_BayesDel_addAF_score  
dbNSFP_DANN_score   
dbNSFP_fathmm_MKL_coding_score  
dbNSFP_DEOGEN2_score  
dbNSFP_LIST_S2_score  
dbNSFP_MVP_score  
dbNSFP_MutationAssessor_score  
dbNSFP_SIFT4G_score  
dbNSFP_Aloft_prob_Tolerant   
dbNSFP_Aloft_prob_Recessive    
dbNSFP_Aloft_prob_Dominant  
  
*for transcript classification:*  
dbNSFP_APPRIS

  
[Return to the table of contents](#table-of-contents)  
***************
***************
  
#### ACMG PP4  
[**acmg-pp4.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pp4.py)  
`Patient’s phenotype or family history is highly specific for a disease with a single genetic etiology`  

**Usage**:  
```bash
python3 acmg-pp4.py \
        --input-vcf imput.vcf  \
        --output-vcf annotated-with-acmg.vcf.gz  
```

The **PP3 SCORE** is:  
* 0.01 times ISEQ_GENE_PANEL_SCORE(max of scores for all genes)
   
For further details about calculation of the scores, see: [**acmg-pp4-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-pp4-readme.md)  

**Required annotations:**  
ISEQ_GENE_PANEL_SCORE  
 

[Return to the table of contents](#table-of-contents)  
***************
***************


#### ACMG BA1    
[**acmg-ba1.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-ba1.py)   
`BA1 Allele frequency is >5% in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium`    

**Usage**:  
```bash
python3 acmg-ba1.py \
        --imput-vcf input.vcf \
        --output-vcf  annotated-with-acmg.vcf.gz 
```
  
The **BA1 SCORE** is:  
* 1.0 for variants with frequency above 0.05 (in gnomAD exomes, gnomAD genomes v3 database or MITOMAP database)  
* 0.0 otherwise (frequency not given, or lower than the above threshold value)  
For further details about score calculation, see: [**acmg-ba1-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-ba1-readme.md)     
  
**Required annotations:**  
ISEQ_GNOMAD_EXOMES_popmax_AF 
ISEQ_GNOMAD_GENOMES_V3_popmax_AF  
MITOMAP_AF  

[Return to the table of contents](#table-of-contents)
******************
******************

#### ACMG BS2    
[**acmg-bs2.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bs2.py)     
`BS2 Observed in a healthy adult individual for a recessive (homozygous), dominant (heterozygous), or X-linked (hemizygous) disorder, with full penetrance expected at an early age`    

**Usage**:  
```bash
python3 acmg-bs2.py \
        --imput-vcf inmut.vcf \
        --output-vcf  annotated-with-acmg.vcf.gz  
```  
The **BS2 SCORE** is:  
* 1.0 for autosomal and X-linked variants with homozygotes count higher than 4 (in 
gnomAD exomes database) or 3 (in gnomAD genomes v3 database) 
* 1.0 for mitochondrial and Y-chromosome variants with alt allele count greater than 2 (
in MITOMAP and gnomAD exomes/genomes v3 databases, respectively)  
* 0.0 otherwise (count not given, or lower than the above threshold values)     
For further details about score calculation, see: [**acmg-bs2-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bs2-readme.md)

**Required annotations:**  
ISEQ_GNOMAD_EXOMES_nhomalt  
ISEQ_GNOMAD_GENOMES_V3_nhomalt  
MITOMAP_AC  
ISEQ_GNOMAD_EXOMES_AC  
ISEQ_GNOMAD_GENOMES_V3_AC   
   
**#TODO**: think about Y-chromosome variants  

[Return to the table of contents](#table-of-contents)   
*****************
*****************

#### ACMG BP1    
[**acmg-bp1.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp1.py)  
`BP1 Missense variant in a gene for which primarily truncating variants are known to cause disease`  

**Usage**:
```bash
python3 acmg-bp1.py \
        --input-vcf input.vcf \
        --output-vcf  annotated-with-acmg.vcf.gz \
        --clinvar-ms clinvar-ms-dictionary.json \
        --clinvar-lof clinvar-lof-dictionary.json 
```

The **BP1 SCORE** is:
* 0.0 for non-missense variants and for missense variants in genes without any known pathogenic LOF variation (ClinVar)  
* (0.0 -1.0] for missense variants with known pathogenic null variants:  
   
The score depends positively on number of pathogenic LOF variants and negatively on number of pathogenic missense variants  
For further details about score calculation, see: [**acmg-bp1-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp1-readme.md)    

**Required files**:  
* clinvar-ms-dictionary.json  
* clinvar-lof-dictionary.json  

**Required annotations:**
SnpEff (ANN)  
  
**#TODO**: does this score make sense?  

[Return to the table of contents](#table-of-contents)
************
************

#### ACMG BP3  
[**acmg-bp3.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp3.py)   
`BP3 In-frame deletions/insertions in a repetitive region without a  known function`  
   
**Usage**:   
```bash
python3 acmg-bp3.py \
        --imput-vcf input.vcf \
        --output-vcf annotated-with-acmg.vcf.gz 
```

The **BP3 SCORE** is:   
* 1.0 for mutations leading to in-frame deletions/insertions (and not any other more severe change) in repeat protein region.   
* 0.0 for other variants    
For further details about final score calculation, see:  [**acmg-bp3-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp3-readme.md)  

**Required annotations:**   
SnpEff annotation (ANN)   
UCSC simpleRepeat annotation (ISEQ_SIMPLE_REPEAT); added only to lines within repeat region        

[Return to the table of contents](#table-of-contents)
**************
**************
#### ACMG BP4  
[**acmg-bp4.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp4.py)  
`BP4 Multiple lines of computational evidence suggest no impact on gene or gene product (conservation, evolutionary, splicing impact, etc.)`     

**Usage**:  
```bash
python3 acmg-bp4.py \
        --input-vcf imput.vcf  \
        --output-vcf annotated-with-acmg.vcf.gz \
        --predictor predictor-ata.json   
```

The **BP4 SCORE** is:  
* 1.0 for variants predicted to have no effect on splicing (ADA or RF algorithm)
* Fraction of the coding-sequence predictors with benign classification (within all predictors with data) -
  if more than half predictions is benign and the overall number of predictions is above 3;
* Half of the above fraction - if more than half predictions is benign and the overall number of predictions is low (three or less)
* 0.0 for variants affecting splicing (or with contradicting splicing predictions), or for variants classified as pathogenic with at least half of the coding-sequence predictors.
* 0.0 for variants without predictor data 
  
For further details about calculation of the scores, see: [**acmg-bp4-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp4-readme.md)    

**Required annotations:**  
*for splicing:*   
ISEQ_ada_score  
ISEQ_rf_score  
 
*for the coding-sequence (dbNSFP):*    
dbNSFP_M_CAP_score  
dbNSFP_PrimateAI_score  
dbNSFP_BayesDel_addAF_score  
dbNSFP_DANN_score   
dbNSFP_fathmm_MKL_coding_score  
dbNSFP_DEOGEN2_score  
dbNSFP_LIST_S2_score  
dbNSFP_MVP_score  
dbNSFP_MutationAssessor_score  
dbNSFP_SIFT4G_score  
dbNSFP_Aloft_prob_Tolerant   
dbNSFP_Aloft_prob_Recessive    
dbNSFP_Aloft_prob_Dominant  
  
*for transcript classification:*  
dbNSFP_APPRIS 

**#TODO**: change SIFT into better algorithm (FATHMM), ideally into something estimating impact also for non-coding variants  

[Return to the table of contents](#table-of-contents) 
*****
*****  

#### ACMG BP5  
[**acmg-bp5.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp5.py)   
`BP5 Variant found in a case with an alternate molecular basis for disease`  
  
**Usage**:  
```bash
python3 acmg-bp5.py \
        --imput-vcf input.vcf \
        --output-vcf annotated-with-acmg.vcf.gz 
```

The **BP5 SCORE** is:  
* 1.0 for variants having 'benign' aggregated ClinVar significance record.  
* 0.75 for variants having 'likely_benign' aggregated ClinVar significance record.  
* 0.0 for all other variants (described as 'pathogenic','likely_pathogenic'or of 'uncertain_significance', variants without aggregated ClinVar significance record)      
For further details about final score calculation, see: [**acmg-bp5-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp5-readme.md)   

**Required annotations:**  
ISEQ_CLINVAR_VARIATION_ID   
ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE   

[Return to the table of contents](#table-of-contents) 
**********
***********

#### ACMG BP7  
[**acmg-bp7.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp7.py)     
`BP7 A synonymous (silent) variant for which splicing prediction algorithms
predict no impact to the splice consensus sequence nor the creation of a
new splice site AND the nucleotide is not highly conserved`    

**Usage**:  
```bash
python3 acmg-bp7.py \
        --input-vcf input.vcf \
        --output-vcf annotated-with-acmg.vcf.gz 
```

The **BP7 SCORE** is:    
* 1.0 for variants not affecting protein (intronic, UTR, synonymous etc.) and not disrupting splicing motifs.  
* 0.0 for other variants  
For further details about final score calculation and "more severe changes" definition, see: [**acmg-bp7-readme.md**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg-bp7-readme.md)  

**Required annotations:**  
SnpEff annotation (ANN)  
ISEQ_HIGHEST_IMPACT 
ISEQ_rf_score  
ISEQ_ada_score  
   
**#TODO**: consider adding conservation test  

[Return to the table of contents](#table-of-contents)
*************
****

#### ACMG SUMMARY
[**acmg_summary.py**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/acmg_summary.py)  

This script calculates the overall ACMG SCORE as:    
```
ACMG SCORE =   
    1 x PVS1 SCORE 
    + 1/2 x (PS1 SCORE + PS3 SCORE)    
    + 1/4 x (PM1 SCORE + PM2 SCORE + PM4 SCORE + PM5 SCORE) 
    + 1/8 x (PP2 SCORE + PP3 SCORE + PP4 SCORE) 
    - 1 x BA1 SCORE 
    - 1/2 x (BS2 SCORE  + BP7 SCORE) 
    - 1/4 x BP5 SCORE
    - 1/8 x (BP1 SCORE + BP3 SCORE + BP4 SCORE )
```
 **Usage**:  
 ```bash
python3 acmg-summary.py --input input.vcf(.gz)  > annotated-with-acmg.vcf
```

Calculated score is then used to classify variants as:   
* Pathogenic (ACMG SCORE greater than or equal to 1.25) 
* Likely pathogenic (ACMG SCORE greater than or equal to 0.75 and less than 1.25) 
* Uncertain (ACMG SCORE greater -0.25 and less than 0.75)
* Likely benign (ACMG SCORE less than  or equal to -0.25 and greater than -1)
* Benign (ACMG SCORE less  than or equal to -1)  

[Return to the table of contents](#table-of-contents)  