#!/usr/bin/python3

import spacy
import string
import pysam
import argparse
import json
from typing import List

__version__ = '1.1.5'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"

# prepared_uniprot_mut_dict = prepare_uniprot_dict(uniprot_mutagenesis_dict)
# training_file = open('train_uniprot_mutagenesis.json', 'r')
# uniprot_classifier = NaiveBayesClassifier(training_file, format="json")
# training_file.close()


nlp = spacy.load('en_core_web_sm')

no_effect_phrases = [" ability to", " no effect", " no change", " retain", " normal", " not abolishe", " no reduction",
                     " not reduce", " normal activity", " not decrease", " not increase", " no increase", " not impair",
                     " not inactivate", " no impact", " not impact", " not enhance", " not inhibit", " no inhibition",
                     " not affect", " not block", " not abrogate", " not disrupt", " not alter", " no defect",
                     " not diminish", " not compromise"]
negative_phrases = [" negative effect", " reduction", " loss in", " unable", " reduce", " decrease", " impair", " abolishe",
                    " negative impact", " inhibit", " inhibition", " low activity", " affect", " prevent", " alter",
                    " lower", " defect", " defective", " diminish", " compromise", " abnormal", " abolish", " loss of",
                    " inactivation", " inactivate", " not active", " block", " abrogate", " disrupt", " no binding",
                    " fail", " mislocalisation", " mislocalise"]
positive_phrases = [" increase", " enhance", " higher", " constitutive"]


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


def do_lemmanisation(sentence):
    sentence_stripped = sentence.lower().replace("-", " ").translate(str.maketrans('', '', string.punctuation))
    loaded_sentence = nlp(sentence_stripped)
    lemma_sentence = [token.lemma_ for token in loaded_sentence]
    return " " + " ".join(lemma_sentence).replace("-PRON-", "").replace("  ", " ") + " "


def check_lists(negative_list, no_effect_list):
    same_words = 0
    for i in negative_list:
        for j in no_effect_list:
            if i in j:
                same_words += 1
    if same_words < len(no_effect_list):
        score = -1
    else:
        score = 0
    return score


def other_sites_dependence(lemma_sentence):
    if 'when associate with' in lemma_sentence:
        final_score = 0
    else:
        final_score = 1
    return final_score


def classify_sentence(lemma_sentence):
    no_effect_count = 0
    negative_count = 0
    positive_count = 0
    no_effect_phrases1 = []
    negative_phrases1 = []
    positive_phrases1 = []
    for ne_phrase in no_effect_phrases:
        # print(ne_phrase)
        n1 = lemma_sentence.count(ne_phrase)
        # print(n1)
        no_effect_count += n1
        if ne_phrase in lemma_sentence:
            no_effect_phrases1.append(ne_phrase)
    for n_phrase in negative_phrases:
        n2 = lemma_sentence.count(n_phrase)
        negative_count += n2
        if n_phrase in lemma_sentence:
            negative_phrases1.append(n_phrase)
    for p_phrase in positive_phrases:
        n3 = lemma_sentence.count(p_phrase)
        positive_count += n3
        if p_phrase in lemma_sentence:
            positive_phrases1.append(p_phrase)
    if negative_count > no_effect_count:
        final_score = -1
    elif positive_count > no_effect_count:
        final_score = 1
    else:
        if (negative_count > 0) and (negative_count == no_effect_count):
            final_score = check_lists(negative_phrases1, no_effect_phrases1)
        else:
            final_score = 0
    return final_score


def sentence_score(sentence):
    # sentence = sys.argv[1]
    lemma_sentence = do_lemmanisation(sentence)
    score = other_sites_dependence(lemma_sentence)
    if score == 0:
        final_score = score
    else:
        final_score = classify_sentence(lemma_sentence)
    return final_score


def find_aa_changes(transcripts):  # only missense

    good_transcripts = find_good_transcripts_ps1(transcripts)
    aa_changes_list = []
    for transcript in good_transcripts:
        variant_type = transcript.fields[1]
        if 'missense' in variant_type:
            aa_field = transcript.fields[10]
            aa_pos = transcript.fields[13].split('/')[0]
            gene = transcript.transcript_gene()
            transcript_id = transcript.fields[6]
            aa_ref = aa_field[2:5]
            aa_alt = aa_field[-3:]
            vt = "This variant leads to the " + aa_ref + aa_pos + aa_alt + " substitution in the " + gene + " protein. "
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
    return aa_changes_list


def pick_genes(aa_changes_list):
    try:
        genes = set([i[0] for i in aa_changes_list])
    except (IndexError, ValueError):
        genes = set()
    return genes


def prepare_uniprot_dict(uniprot_mutagenesis_dict):
    prepared_uniprot_dict = {}
    for key in uniprot_mutagenesis_dict.keys():
        new_items = []
        key_splitted = key.split("_")
        new_key, uniprot_id = "_".join(key_splitted[0:-1]), key_splitted[-1]  # modified
        changes = uniprot_mutagenesis_dict[key]
        for item in changes:
            if item[0] == item[1]:
                for alt in item[3].split(","):
                    if (alt in aa_dict.keys() and item[2] in aa_dict.keys()):
                        new_item = [uniprot_id, aa_dict[item[2]], aa_dict[alt], item[0],
                                    item[4]]  # id,ref,alt,pos,effect
                        new_items.append(new_item)
        prepared_uniprot_dict[new_key] = new_items
    return prepared_uniprot_dict


def compare_changes(aa_changes_list, prepared_uniprot_mut_dict, gene):
    identical_changes = []
    similar_changes = []
    try:
        prepared_uniprot_mut_dict[gene]
    except KeyError:
        pass
    else:
        uniprot_list = prepared_uniprot_mut_dict[gene]
        for mut in aa_changes_list:
            for up in uniprot_list:
                # print(up[1:4])
                # print(mut[1:])
                # print(gene)
                if (mut[0] == gene) and (mut[1:4] == up[1:4]):
                    useful_uniprot_list = [up[2], up[0], up[-1], interpret_uniprot_description(up[-1])]
                    change = mut[:] + useful_uniprot_list
                    identical_changes.append(change)
                elif ((mut[0] == gene) and (mut[1] == up[1]) and (mut[3] == up[3]) and (mut[2] != up[3])):
                    useful_uniprot_list = [up[2], up[0], up[-1], interpret_uniprot_description(up[-1])]
                    change = mut[:] + useful_uniprot_list
                    similar_changes.append(change)
    return identical_changes, similar_changes


def find_good_transcripts_ps1(transcript_list):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def interpret_uniprot_description(text):
    if text == "":
        return 0
    else:
        return sentence_score(text)


def give_score(compared_changes_list):  # variant level
    negative_changes = 0
    positive_changes = 0
    for i in compared_changes_list:
        effect = i[-1]
        if effect < 0:
            negative_changes += effect
        if effect > 0:
            positive_changes += effect
    if negative_changes < 0:
        final_score = -1
        worst_changes = [i for i in compared_changes_list if i[-1] == -1]
    elif positive_changes > 0:
        final_score = 1
        worst_changes = [i for i in compared_changes_list if i[-1] == 1]
    else:
        worst_changes = [i for i in compared_changes_list if i[-1] == 0]
        final_score = 0
    return final_score, worst_changes


def main(vcf_name, vcf_writer_name):
    vcf_name = args.input_vcf

    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PS3_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PS3 (Strong evidence of pathogenicity): A positive score is given to missense  variants causing change  described in the Uniprot database as having negative or hyperactiviting effect on protein. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, Uniprot'),
             ('Version', 'SnpEff: the same as in the ANN field, Uniprot: 14-10-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PS3_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PS3_SCORE"),
             ('Source', 'SnpEff - ANN field, Uniprot'),
             ('Version', 'SnpEff: the same as in the ANN field, Uniprot: 14-10-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer_name = args.output_vcf

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    prepared_uniprot_mut_dict = prepare_uniprot_dict(uniprot_mutagenesis_dict)

    for record in vcf_reader.fetch():

        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            final_score = 0
            description = "PS3^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"
            record.info["ISEQ_ACMG_PS3_SCORE"] = float(final_score)
            record.info["ISEQ_ACMG_PS3_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        aa_changes = find_aa_changes(transcripts)
        if len(aa_changes) == 0:
            final_description = "This is not a missense variant."
            final_score = 0
            text = ""
        else:
            genes = pick_genes(aa_changes)
            identical_changes = []
            similar_changes = []
            final_score = 0
            for gene in genes:
                # print(gene)
                # prepared_uniprot_mut_dict = prepare_uniprot_dict(uniprot_mutagenesis_dict)
                i_changes, s_changes = compare_changes(aa_changes, prepared_uniprot_mut_dict, gene)
                identical_changes += i_changes
                similar_changes += s_changes
            # print(identical_changes)
            if len(identical_changes) == 0 and len(similar_changes) == 0:
                final_description = "This is a missense variant. Protein mutants with similar amino acid substitutions do not have record in the UniProt database."
                final_score = 0
                text = ""
            elif len(identical_changes) > 0:
                score, changes = give_score(identical_changes)
                # print(score)
                description_set = set([i[5] for i in changes])
                uniprot_description_set = set([i[8] for i in changes])
                final_description = " ".join(
                    description_set) + "Identical protein mutants are described in the UniProt database as "
                if score == -1:
                    text = "likely having negative effect on protein function. UniProt record: " + " ".join(
                        uniprot_description_set) + "."
                    final_score = 1
                elif score == 1:
                    text = "likely leading to protein hyperactivity. UniProt record: " + " ".join(
                        uniprot_description_set) + "."
                    final_score = 0.5
                elif score == 0:
                    text = "not affecting protein function."
                    final_score = 0  # tu jestem
            elif len(similar_changes) > 0:
                score, changes = give_score(similar_changes)
                description_set = set([i[5] for i in changes])
                uniprot_description_set = set([i[8] for i in changes])
                similar_mutants = set([i[0] + ": " + i[1] + i[3] + i[6] for i in changes])
                final_description = " ".join(description_set) + "Similar protein mutants [" + ", ".join(
                    similar_mutants) + "] are described in the UniProt database as "
                if score == -1:
                    text = "likely having negative effect on protein function. UniProt record: " + " ".join(
                        uniprot_description_set) + "."
                    final_score = 0.25
                elif score == 1:
                    text = "likely leading to protein hyperactivity. UniProt record: " + " ".join(
                        uniprot_description_set) + "."
                    final_score = 0.125
                elif score == 0:
                    text = "not affecting protein function."
                    final_score = 0
            final_description += text

        description = final_description.replace(" ", "^")
        description = description.replace(";", "*").replace(",", "*").replace("..", ".")
        record.info["ISEQ_ACMG_PS3_SCORE"] = float(final_score)
        record.info["ISEQ_ACMG_PS3_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='annotates vcf with tsv.gz')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--uniprot-mut', '-u', metavar='uniprot_mut', type=str, required=True,
                        help='UniProt mutagenesis data dictionary')
    parser.add_argument('--aa', '-a', type=str, required=True, help='Amino acid dictionary')
    args = parser.parse_args()

    aa_dict = read_json(args.aa)
    uniprot_mutagenesis_dict = read_json(args.uniprot_mut)
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
