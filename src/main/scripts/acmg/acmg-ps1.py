#!/usr/bin/python3

import pysam
import argparse
import json
from typing import List

__version__ = '1.1.5'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"

pt = "pathogenic"
pt_low = "pathogenic_low_penetrance"
pt_lpt = "pathogenic/likely_pathogenic"
lpt = "likely_pathogenic"
lpt_low = "likely_pathogenic_low_penetrance"
bn = "benign"
bn_lbn = "benign/likely_benign"
lbn = "likely_benign"


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def calculate_frameshift(ref, alt):
    dif = len(ref) - len(alt)
    shift = str(dif % 3)
    return "+" + shift


def find_aa_changes(line, transcripts, alt):
    good_transcripts = find_good_transcripts_ps1(transcripts)
    aa_changes_list = []
    for transcript in good_transcripts:
        variant_type = transcript.fields[1]
        aa_field = transcript.fields[10]
        aa_pos = transcript.fields[13].split('/')[0]
        gene = transcript.transcript_gene()
        transcript_id = transcript.fields[6]
        if "missense" in variant_type:
            aa_ref = aa_field[2:5]
            aa_alt = aa_field[-3:]
            vt = "This variant leads to the " + aa_ref + aa_pos + aa_alt + " substitution in the " + gene + " protein, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("stop_gained" in variant_type) and ("frameshift" not in variant_type):  # or (sl in variant_type):
            aa_ref = aa_field[2:5]
            aa_alt = '*'
            vt = "This variant introduces pre-mature stop codon. It truncates the " + gene + " protein at the " + aa_pos + " position, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("start_lost" in variant_type):
            aa_ref = aa_field[2:5]
            aa_alt = "?"
            vt = "This variant leads to loss of the start codon. Thus, it is likely that the " + gene + " protein is not produced, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("stop_lost" in variant_type):
            aa_ref = aa_field[2:5]
            aa_alt = '?*'
            vt = "This variant leads to loss of the stop codon. Thus, it is likely that the " + gene + " protein is elongated, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("frameshift" in variant_type):
            aa_ref = aa_field[2:5]
            aa_alt = calculate_frameshift(line.ref, alt) + "fs"
            vt = "This variant leads to frameshift (" + aa_alt + "), which starts at the " + aa_pos + " amino acid position of the " + gene + " protein, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "disruptive_inframe_insertion" in variant_type:
            try:
                aa_field.split('ins')[1]
            except IndexError:
                aa_ref = aa_field.split('dup')[0][2:]
                aa_alt = 'dup'
                vt = "This variant leads to in-frame insertion of one or more additional amino acids in the " + aa_pos + " position of the " + gene + " protein. It also chamges amino acids surrounding the insertion site, (transcript: " + transcript_id + ")."
                one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
                if one_aa_change not in aa_changes_list:
                    aa_changes_list.append(one_aa_change)
            else:
                aa_ref = aa_field.split('ins')[0][2:]
                aa_alt = aa_field.split('ins')[1]
                vt = "This variant leads to in-frame insertion of one or more additional amino acids in the " + aa_pos + " position of the " + gene + " protein. It also changes amino acids surrounding the insertion site, (transcript: " + transcript_id + ")."
                one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
                if one_aa_change not in aa_changes_list:
                    aa_changes_list.append(one_aa_change)
        if ("inframe_insertion" in variant_type) and ("disruptive_inframe_insertion" not in variant_type):
            aa_ref = aa_field.split('dup')[0][2:]
            aa_alt = 'dup'
            vt = "This variant leads to insertion of one or more additional amino acids in the " + aa_pos + " position of the " + gene + " protein, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("inframe_deletion" in variant_type) and ("disruptive_inframe_deletion" not in variant_type):
            aa_ref = aa_field.split('del')[0][2:]
            aa_alt = 'del'
            vt = "This variant leads to deletion of one or more amino acids of the " + gene + " protein (at position " + aa_pos + "), (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "disruptive_inframe_deletion" in variant_type:
            aa_ref = aa_field.split('del')[0][2:]
            aa_alt = 'del'
            vt = "This variant deletes one or more amino acids of the " + gene + " protein (at position " + aa_pos + "). It also changes amino acids surrounding the deletion site, (transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "splice_acceptor" in variant_type:
            aa_ref = transcript.fields[9].split('.')[1]
            aa_alt = ""
            vt = "This variant disrupts the splice acceptor site (" + aa_ref + ") in the " + gene + " gene, (affected transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "splice_donor" in variant_type:
            aa_ref = transcript.fields[9].split('.')[1]
            aa_alt = ""
            vt = "This variant disrupts the splice donor site (" + aa_ref + ") in the " + gene + " , (affected transcript: " + transcript_id + ")."
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
    return aa_changes_list


def pick_genes(aa_changes_list):
    try:
        genes = set([i[0] for i in aa_changes_list])
    except (IndexError, ValueError):
        genes = set()
    return genes


def pick_best_description(changes_list):
    # changes_sorted = sorted(changes_list, key = lambda x: int(x[4].split(".")[1]))
    # return changes_sorted[0][-1]
    return changes_list[0][-1]


def compare_changes(aa_changes_list, gene):
    changes = []
    scores = []
    try:
        clinvar_gene_changes_dict[gene]
    except KeyError:
        max_score = 0
    else:
        clinvar_list = clinvar_gene_changes_dict[gene]
        for mut in aa_changes_list:
            for cl in clinvar_list:
                if (mut[0] == gene) and (mut[1:5] == cl[0:4]):
                    sign = cl[-1].split(":")  # new
                    if (pt in sign) or (pt_lpt in sign) or (pt_low in sign):
                        score = 1
                    elif (lpt in sign) or (lpt_low in sign):  # and (pt not in sign):
                        score = 0.75
                    else:
                        score = 0
                    change = mut[:] + [score]
                    changes.append(change)
                    scores.append(score)
                try:
                    max_score = max(scores)
                except ValueError:
                    max_score = 0
    return max_score, changes


def pick_worst_change(max_score, changes):
    worst_changes = [i[0:-1] for i in changes if i[-1] >= max_score]
    return worst_changes


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PS1_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PS1 (Strong evidence of pathogenicity): A positive score is given to variants causing the same protein change as a previously established pathogenic/likely pathogenic variant regardless of nucleotide change. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 10-12-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PS1_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PS1_SCORE"),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 10-12-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            final_score = 0
            description = "PS1^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"
            record.info["ISEQ_ACMG_PS1_SCORE"] = float(final_score)
            record.info["ISEQ_ACMG_PS1_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        aa_changes = find_aa_changes(record, transcripts, record.alts[0])
        if len(aa_changes) == 0:
            final_description = "This is not a protein changing variant."
            final_score = 0
        else:
            genes = pick_genes(aa_changes)
            identical_changes = []
            scores = []
            for gene in genes:
                score, changes = compare_changes(aa_changes, gene)
                identical_changes += changes
                scores.append(score)
            if len(identical_changes) == 0:
                final_description = "This is a protein altering variant. Variants introducing the same change are not described in the ClinVar database."
                final_score = 0
            else:
                final_score = max(scores)
                if final_score == 0:
                    final_description = "This is a protein altering variant. Variants introducing the same change are not pathogenic or likely pathogenic, according to the ClinVar database."
                elif final_score > 0:
                    worst_changes = pick_worst_change(final_score, identical_changes)
                    worst_changes_genes = pick_genes(worst_changes)
                    if len(worst_changes_genes) == 1:
                        best_description = pick_best_description(worst_changes)
                    else:
                        best_description = ""
                        for i in worst_changes_genes:
                            gene_worst_changes = [x for x in worst_changes if x[0] == i]
                            best_description += " " + pick_best_description(gene_worst_changes)
                    final_description = best_description + " Variants introducing the same change are classified in the ClinVar database as "
                    if final_score == 0.75:
                        final_description += "likely pathogenic."
                    elif final_score == 1:
                        final_description += "pathogenic."

        description = final_description.replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_PS1_SCORE"] = float(final_score)
        record.info["ISEQ_ACMG_PS1_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PS1')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--clinvar-changes', '-c', metavar='clinvar_changes', type=str, required=True,
                        help='ClinVar protein changes dictionary')
    args = parser.parse_args()
    clinvar_gene_changes_dict = read_json(args.clinvar_changes)
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
