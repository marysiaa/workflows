### **This file describes how acmg-pm5.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PM5 criteria:   
*"PM5 Novel missense change at an amino acid residue where a different missense change determined to be pathogenic has been seen before   
Example: Arg156His is pathogenic; now you observe Arg156Cys   
Caveat: Beware of changes that impact splicing rather than at the amino acid/protein level."*   

To achieve this aim acmg-pm5.py does the following:   
First it checks if given ISEQ variant leads to protein change (only protein coding transcripts without errors/warnings are considered).   
Then, it checks whether identical changes was not described in ClinVar - to this end it uses the:   
`Variants^introducing^the^same^change^are^classified^in^the^ClinVar^database` or 
`Variants^introducing^the^same^change^are^not^pathogenic^or^likely^pathogenic` phrases from the ISEQ_ACMG_PS1_DESCRIPTION field.  
Then it evaluates whether novel changes are similar to changes described in ClinVar (see below what similar means).     
Program compares: gene name, transcript id and protein level mutation (the last task works differently for different mutation types, see below)   
Changes in proteins caused by known variants for each gene are described in the clinvar-protein-changes-dictionary.json file.   

Mutation types and what is assumed to cause similar change to variants from ClinVar   
- missense (the same ref and amino acid position, new altered aa)      
- stop gained => frameshift (the same ref and aa position)  
- frameshift => frameshift (the same ref, position but different frameshift "phase";) or frameshift => stop gained (the same ref and position)  

The total PM5 SCORE is:
 - 0.75 for novel variants "similar to" variants described in ClinVar as "likely pathogenic"  
 - 1.0 for novel variants "similar to" pathogenic or pathogenic/likely pathogenic variants   
 - 0.0 for  variants which are "similar to" variants described as benign, likely_benign or of uncertain significance, 
   as well as variants without matching ClinVar record or variants already described in ClinVar

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)
