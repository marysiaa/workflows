def find_good_transcripts_interaction(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation affecting residue important for protein-protein interaction or structural conformation, 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "structural_interaction_variant" in transcript.variant_type() or "protein_protein_contact" in transcript.variant_type() ) and (
                transcript.transcript_type() == "protein_coding") and ('WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type()  or sg in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and ('WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts

def find_good_transcripts_syn(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing  modification of splice donor/acceptor site; 3) no errors/warnings present#
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                'synonymous' in transcript.variant_type()  and (
                transcript.transcript_type() == "protein_coding") and ('WARNING' not in transcript.transcript_warnings()) and ('ERROR' not in transcript.transcript_warnings())):
            good_transcripts.append(transcript)
    return good_transcripts

class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]

    def is_lof(self):
        """for each variant extracts 'reliable' transcripts:
        criteria: 1) protein coding transcript, 2) with mutation causing stop gain, start loss, frameshift, splice acceptor or splice donor site change, 3) no warnings and errors
        returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""

        if (
            self.variant_type() in ["start_lost", "start_gain", "frameshift", "splice_acceptor", "splice_donor"] and
            self.transcript_type() == "protein_coding" and
            'WARNING' not in transcript.transcript_warnings() and
            'ERROR' not in transcript.transcript_warnings()
            ):
            return true
        else:
            return false
