#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import gspread
from typing import Dict
import os
import pandas as pd
from collections import defaultdict
import requests
import shutil
import importlib
import sys


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(SCRIPT_DIR)
    

__version__ = "0.0.2"

folders = {
    'health': '1KedTd6oNwAAjDOqPfCSQVWiIPKU1elBp',
    'vitaleo': '1-i_jdDJmN-fzhZk5OE5Vl45vpPNxu9pn',
    'wellness' : '1mV1CuMoAftqAN0Xzuk8UaEKN3deO8qr4'
}


def args_parser_init():
    parser = argparse.ArgumentParser(description="Convert models in .py format to google sheet.")
    parser.add_argument('--model-py', type=str, required=True,
                        help='A path or url to model.py.')
    parser.add_argument('--credentials-json', type=str, required=False,
                        help='Credentials.json containing data necessary to get access to google developer\'s privileges.')
    parser.add_argument('--folder-id', type=str, default=folders['vitaleo'],
                        help='Pass a google drive\'s folder id')    
    parser.add_argument('--domain', type=str, default='intelliseq.pl',
                        help='A domain to share spreadsheet with.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()
    return args


def check_error_and_get_py(input_py: str, model_name_ext: str):
    """Load .py script to a program. If there's an error print details with a traceback"""
    try:
        if input_py.startswith("https://"):
            r = requests.get(input_py)
            open(f"{SCRIPT_DIR}/{model_name_ext}", 'wb').write(r.content)
        else:
            shutil.copy(input_py, SCRIPT_DIR)
    except ValueError as ex:
        print(f"{ex.with_traceback(ex.__traceback__)} in {input_py} file!")
        exit()


def delete_sheet(spreadsheet, sheet_name="sheet1"):
    for wsh in spreadsheet.worksheets():
        wsh_name = wsh.title
        if sheet_name.lower() == wsh_name.lower():
            spreadsheet.del_worksheet(wsh)


def update_spreadsheet(spreadsheet, model_py, model_name: str):
    update_texts(spreadsheet, model_py, model_name)
    update_model_description(spreadsheet, model_py)
    update_results_description(spreadsheet, model_py)
    update_other_factors(spreadsheet, model_py)
    update_recommendations(spreadsheet, model_py)
    update_about_the_test(spreadsheet, model_py)
    update_references(spreadsheet, model_py)
    delete_sheet(spreadsheet)


def update_texts(spreadsheet, model_py, model_name: str):
    texts = spreadsheet.add_worksheet('texts', cols=50, rows=25)
    model_dict = {
        'key': [
            'name', 
            'id', 
            'trait_category', 
            'genotypeScore', 
            'geneticScore', 
            'environmentScore',
            'trait_heritability',
            'trait_snp_heritability',
            'trait_explained'
        ],
        'value': [
            model_py.trait, 
            model_name, 
            '', 
            '', 
            '', 
            '', 
            model_py.trait_heritability,
            model_py.trait_snp_heritability,
            model_py.trait_explained
        ]
    }
    model_df = pd.DataFrame.from_dict(model_dict)
    texts.update([model_df.columns.values.tolist()] + model_df.values.tolist())    


def update_model_description(spreadsheet, model_py):
    model_description = spreadsheet.add_worksheet('model_description', cols=50, rows=25)
    model_dict = {'model_description': [model_py.about], 'image_top': ''}

    model_df = pd.DataFrame.from_dict(model_dict)
    model_description.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_results_description(spreadsheet, model_py):
    results_description = spreadsheet.add_worksheet('results_description', cols=50, rows=25)
    risks = ['Low risk', 'Average risk', 'High risk'] if \
        model_py.category_on_the_left_side_of_plot == "Low risk" \
        else ['High risk', 'Average risk', 'Low risk']
    risk_value = ['low', 'medium', 'high'] if \
        model_py.category_on_the_left_side_of_plot == "Low risk" \
        else ['high', 'medium', 'low']
    model_dict = {
        'score_value': ['reduced', 'average', 'increased'],
        'risk_value': risk_value,
        'risk_text': risks,
        'array_element_1': [model_py.result_statement[element] for element in risks],
        'array_element_2': [model_py.what_your_result_means[element] for element in risks]
    }

    model_df = pd.DataFrame.from_dict(model_dict)
    results_description.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_other_factors(spreadsheet, model_py):
    other_factors = spreadsheet.add_worksheet('other_factors', cols=50, rows=25)
    factors = model_py.other_factors
    model_dict = {
        'id': [factor['title'] for factor in factors],
        'icon': ['' for factor in factors],
        'factor.name.1': [factor['title'] for factor in factors],
        'factor.description.1': [factor['content'] for factor in factors],
    }

    model_df = pd.DataFrame.from_dict(model_dict)
    other_factors.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_recommendations(spreadsheet, model_py):
    recommendations = spreadsheet.add_worksheet('recommendations', cols=50, rows=25)
    risks = ['Low risk', 'Average risk', 'High risk'] if \
        model_py.category_on_the_left_side_of_plot == "Low risk" \
        else ['High risk', 'Average risk', 'Low risk']
    choices = model_py.what_you_can_do

    lengths = [len(choices[element]) for element in risks]
    max_len = max(lengths)
    
    for element in risks:
        element_len = len(choices[element])
        difference = max_len - element_len
        if difference > 0:
            choices[element] += ([{'content': '', 'title': ''} for _ in range(difference)])
            
    model_dict = defaultdict(list)
    model_dict['score_value'] = ['reduced', 'average', 'increased']
    model_dict['description'] = [model_py.what_you_can_do_statement[element] for element in risks]
    
    for element in risks:
        for i, choice in enumerate(choices[element], start=1):
            model_dict[f'set.name.{i}'].append(choice['title'])
            model_dict[f'set.description.{i}'].append(choice['content'])
            model_dict[f'set.icon.{i}'].append('')

    model_df = pd.DataFrame.from_dict(model_dict)
    recommendations.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_about_the_test(spreadsheet, model_py):
    about_the_test = spreadsheet.add_worksheet('about_the_test', cols=50, rows=25)
    model_dict = dict.fromkeys(['text1', 'text2', 'text3'], [''])
    if hasattr(model_py, 'science_behind_the_test'):
        model_dict['text1'] = [model_py.science_behind_the_test]
    if hasattr(model_py, 'science_behind_your_result'):
        model_dict['text2'] = [model_py.science_behind_your_result]
    if hasattr(model_py, 'how_your_result_was_calculated'):
        model_dict['text3'] = [model_py.how_your_result_was_calculated]
    
    model_df = pd.DataFrame.from_dict(model_dict)
    about_the_test.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_references(spreadsheet, model_py):
    references = spreadsheet.add_worksheet('references', cols=50, rows=25)

    data_analysis_articles = model_py.references.get('article_links')
    recommendations_articles = model_py.references.get('website_text_data')

    model_dict = defaultdict(list)
    if isinstance(data_analysis_articles, list):
        for article in data_analysis_articles:
            model_dict['article_type'].append('data_analysis_articles')
            model_dict['text'].append(article['link'])
            model_dict['pmids'].append('')
    if isinstance(recommendations_articles, list):
        for article in recommendations_articles:
            model_dict['article_type'].append('recommendations_articles')
            model_dict['text'].append(f"{article['title']} {article['links']} ({article['accessed']})")
            model_dict['pmids'].append('')
    if not model_dict:
        model_dict['article_type'].append('')
        model_dict['text'].append('')
        model_dict['pmids'].append('')
        
    model_df = pd.DataFrame.from_dict(model_dict)
    references.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def clean_model(model_path: str):
    cleaned = list()
    with open(model_path, 'r') as model:
        bracket_counter = 0
        for line in model.readlines():
            if line.lstrip().startswith("from"):
                continue
            if line.lstrip().startswith("model") and "Choice" in line and "(" in line:
                bracket_counter += 1
                continue
            if bracket_counter:
                if "(" in line:
                    bracket_counter += 1
                if ")" in line:
                    bracket_counter -= 1
                continue
            if "GenotypeGetter" in line:
                continue
            cleaned.append(line)
    with open(model_path, 'w') as model:
        for line in cleaned:
            model.write(line)


if __name__ == "__main__":
    args = args_parser_init()
    model_name_ext = os.path.basename(args.model_py)
    model_name = os.path.splitext(model_name_ext)[0]

    check_error_and_get_py(args.model_py, model_name_ext)
    clean_model(f"{SCRIPT_DIR}/{model_name_ext}")
    model_py = importlib.import_module(model_name)
    
    gc = gspread.service_account() if not args.credentials_json else gspread.service_account(filename=args.credentials_json)
    
    spreadsheet = gc.create(model_name, folder_id=args.folder_id)
    update_spreadsheet(spreadsheet, model_py, model_name)
    
    spreadsheet.share(args.domain, perm_type='domain', role='writer', notify=False)
    print(f"{model_name} was saved as a spreadsheet and shared to the {args.domain}")
    print(spreadsheet.url)

    os.remove(f"{SCRIPT_DIR}/{model_name_ext}")