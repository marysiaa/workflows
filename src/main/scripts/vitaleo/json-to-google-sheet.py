#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import gspread
import json
from typing import Dict
import urllib
import os
import pandas as pd
from collections import defaultdict


__version__ = "0.0.2"

folders = {
    'health': '1KedTd6oNwAAjDOqPfCSQVWiIPKU1elBp',
    'vitaleo': '1-i_jdDJmN-fzhZk5OE5Vl45vpPNxu9pn',
    'wellness' : '1mV1CuMoAftqAN0Xzuk8UaEKN3deO8qr4'
}

def args_parser_init():
    parser = argparse.ArgumentParser(description="Convert models in .json format to google sheet.")
    parser.add_argument('--model-json', type=str, required=True,
                        help='A path or url to model.json.')
    parser.add_argument('--credentials-json', type=str, required=False,
                        help='Credentials.json containing data necessary to get access to google developer\'s privileges.')
    parser.add_argument('--folder-id', type=str, default=folders['vitaleo'],
                        help='Pass a google drive\'s folder id')    
    parser.add_argument('--domain', type=str, default='intelliseq.pl',
                        help='A domain to share spreadsheet with.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()
    return args


def check_error_and_get_json(input_json: str) -> Dict:
    """Load json to program. If there's an error print details with traceback"""
    try:
        if input_json.startswith("https://"):
            with urllib.request.urlopen(input_json) as json_url:
                output_dict = json.load(json_url)
        else:
            with open(input_json) as json_file:
                output_dict = json.load(json_file)
        return output_dict
    except ValueError as ex:
        print(f"{ex.with_traceback(ex.__traceback__)} in {input_json} file!")
        exit()


def delete_sheet(spreadsheet, sheet_name="sheet1"):
    for wsh in spreadsheet.worksheets():
        wsh_name = wsh.title
        if sheet_name.lower() == wsh_name.lower():
            spreadsheet.del_worksheet(wsh)


def update_spreadsheet(spreadsheet, model_json: Dict, model_name: str):
    update_texts(spreadsheet, model_json, model_name)
    update_model_description(spreadsheet, model_json)
    update_results_description(spreadsheet, model_json)
    update_other_factors(spreadsheet, model_json)
    update_recommendations(spreadsheet, model_json)
    update_about_the_test(spreadsheet, model_json)
    update_references(spreadsheet, model_json)
    delete_sheet(spreadsheet)


def update_texts(spreadsheet, model_json: Dict, model_name: str):
    texts = spreadsheet.add_worksheet('texts', cols=50, rows=25)
    model_dict = {
        'key': ['name', 'id'],
        'value': [model_json['trait'], model_name]
    }
    model_dict = {
        'key': [
            'name', 
            'id', 
            'trait_category', 
            'genotypeScore', 
            'geneticScore', 
            'environmentScore',
            'trait_heritability',
            'trait_snp_heritability',
            'trait_explained'
        ],
        'value': [
            model_json['trait'], 
            model_name, 
            '', 
            '', 
            '', 
            '', 
            model_json['trait_heritability'],
            model_json['trait_snp_heritability'],
            model_json['trait_explained']
        ]
    }
    model_df = pd.DataFrame.from_dict(model_dict)
    texts.update([model_df.columns.values.tolist()] + model_df.values.tolist())    


def update_model_description(spreadsheet, model_json: Dict):
    model_description = spreadsheet.add_worksheet('model_description', cols=50, rows=25)
    model_dict = {'model_description': [model_json['about']], 'image_top': ''}

    model_df = pd.DataFrame.from_dict(model_dict)
    model_description.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_results_description(spreadsheet, model_json: Dict):
    results_description = spreadsheet.add_worksheet('results_description', cols=50, rows=25)
    risks = ['Low risk', 'Average risk', 'High risk'] if \
        model_json['category_on_the_left_side_of_plot'] == "Low risk" \
        else ['High risk', 'Average risk', 'Low risk']
    risk_value = ['low', 'medium', 'high'] if \
        model_json['category_on_the_left_side_of_plot'] == "Low risk" \
        else ['high', 'medium', 'low']
    model_dict = {
        'score_value': ['reduced', 'average', 'increased'],
        'risk_value': risk_value,
        'risk_text': risks,
        'array_element_1': [model_json['result_statement_choice'][element] for element in risks],
        'array_element_2': [model_json['what_your_result_means_choice'][element] for element in risks]
    }

    model_df = pd.DataFrame.from_dict(model_dict)
    results_description.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_other_factors(spreadsheet, model_json: Dict):
    other_factors = spreadsheet.add_worksheet('other_factors', cols=50, rows=25)
    factors = model_json['other_factors']
    model_dict = {
        'id': [factor['title'] for factor in factors],
        'icon': ['' for factor in factors],
        'factor.name.1': [factor['title'] for factor in factors],
        'factor.description.1': [factor['content'] for factor in factors],
    }

    model_df = pd.DataFrame.from_dict(model_dict)
    other_factors.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_recommendations(spreadsheet, model_json: Dict):
    recommendations = spreadsheet.add_worksheet('recommendations', cols=50, rows=25)
    risks = ['Low risk', 'Average risk', 'High risk'] if \
        model_json['category_on_the_left_side_of_plot'] == "Low risk" \
        else ['High risk', 'Average risk', 'Low risk']
    choices = model_json.copy()['what_you_can_do_choice']

    lengths = [len(choices[element]) for element in risks]
    max_len = max(lengths)
    
    for element in risks:
        element_len = len(choices[element])
        difference = max_len - element_len
        if difference > 0:
            choices[element] += ([{'content': '', 'title': ''} for _ in range(difference)])
            
    model_dict = defaultdict(list)
    model_dict['score_value'] = ['reduced', 'average', 'increased']
    model_dict['description'] = [model_json['what_you_can_do_statement_choice'][element] for element in risks]
    
    for element in risks:
        for i, choice in enumerate(choices[element], start=1):
            model_dict[f'set.name.{i}'].append(choice['title'])
            model_dict[f'set.description.{i}'].append(choice['content'])
            model_dict[f'set.icon.{i}'].append('')

    model_df = pd.DataFrame.from_dict(model_dict)
    recommendations.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_about_the_test(spreadsheet, model_json: Dict):
    about_the_test = spreadsheet.add_worksheet('about_the_test', cols=50, rows=25)
    model_dict = {
        'text1': [model_json['science_behind_the_test']],
        'text2': [model_json['science_behind_your_result']],
        'text3': [model_json['how_your_result_was_calculated']]
    }
    
    model_df = pd.DataFrame.from_dict(model_dict)
    about_the_test.update([model_df.columns.values.tolist()] + model_df.values.tolist())


def update_references(spreadsheet, model_json: Dict):
    references = spreadsheet.add_worksheet('references', cols=50, rows=25)

    data_analysis_articles = model_json['references'].get('article_links')
    recommendations_articles = model_json['references'].get('website_text_data')

    model_dict = defaultdict(list)
    
    if isinstance(data_analysis_articles, list):
        for article in data_analysis_articles:
            model_dict['article_type'].append('data_analysis_articles')
            model_dict['text'].append(article['link'])
            model_dict['pmids'].append('')
    if isinstance(recommendations_articles, list):
        for article in recommendations_articles:
            model_dict['article_type'].append('recommendations_articles')
            model_dict['text'].append(f"{article['title']} {article['links']} ({article['accessed']})")
            model_dict['pmids'].append('')
    if not model_dict:
        model_dict['article_type'].append('')
        model_dict['text'].append('')
        model_dict['pmids'].append('')


    model_df = pd.DataFrame.from_dict(model_dict)
    references.update([model_df.columns.values.tolist()] + model_df.values.tolist())


if __name__ == "__main__":
    args = args_parser_init()
    model_json = check_error_and_get_json(args.model_json)
    gc = gspread.service_account() if not args.credentials_json else gspread.service_account(filename=args.credentials_json)
    
    # model_name_ext = os.path.basename(args.model_json)
    # model_name = os.path.splitext(model_name_ext)[0]

    # spreadsheet = gc.create(model_name, folder_id=args.folder_id)
    # update_spreadsheet(spreadsheet, model_json, model_name)
    
    # spreadsheet.share(args.domain, perm_type='domain', role='writer', notify=False)
    # print(f"{model_name} was saved as a spreadsheet and shared to the {args.domain}")
    # print(spreadsheet.url)
    titles_list = []
    for spreadsheet in gc.openall():
        titles_list.append(spreadsheet.id)
        print(spreadsheet.id)
    print(titles_list)
