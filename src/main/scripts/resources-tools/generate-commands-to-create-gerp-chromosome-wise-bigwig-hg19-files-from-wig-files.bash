!/bin/bash

VERSION="0.0.1"

PATH_TO_SCORES_DIRECTORY=$(readlink -f $1)
NAME=$2
CHROM_SIZES_FILE=$(readlink -f $3)

echo "mkdir $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs"

for i in {1..22}; do
  echo "wigToBigWig $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chr$i.$NAME.hg19.wig $CHROM_SIZES_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chr$i.$NAME.hg19.bw &"
done
echo "wigToBigWig $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrX.$NAME.hg19.wig $CHROM_SIZES_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chrX.$NAME.hg19.bw &"
echo "wigToBigWig $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrY.$NAME.hg19.wig $CHROM_SIZES_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chrY.$NAME.hg19.bw &"
echo "wigToBigWig $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrM.$NAME.hg19.wig $CHROM_SIZES_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chrM.$NAME.hg19.bw &"
