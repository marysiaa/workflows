#!/usr/bin/python3

__version__ = '0.0.1'

import json
import argparse
from typing import Dict, List

#gene: lof_oe, lower, upper, flag
"""gene name => [lof_oe, lower CI, upper CI]"""

def make_gnomad_lof_dict(gnomad_lof_metrics: str) -> Dict:
    gnomad_lof_dict={}
    with open(gnomad_lof_metrics) as f1:
       for line in f1:
           l=line.strip().split('\t')
           gnomad_lof_dict[l[0]]=l[1:4]
    return gnomad_lof_dict


if __name__=='__main__' :
    parser = argparse.ArgumentParser(description='Prepares json file needed for acmg pvs1 annotation, "gnomad-lof-dictionary.json": '
                                                 'gene name => [lof_oe, lower CI, upper CI]')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str, required=True, help="Path to modified gnomad lof metrics file" )
    parser.add_argument('-o', '--output', type=str, required=True, help="Path to output json file")
    args = parser.parse_args()
    gnomad_lof_dict = make_gnomad_lof_dict(args.input)
    with open(args.output, "w") as f:
        t = json.dumps(gnomad_lof_dict)
        f.write(t)


