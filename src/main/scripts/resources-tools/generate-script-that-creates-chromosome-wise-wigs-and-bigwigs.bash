#!/bin/bash

VERSION="0.0.1"

NAME=$1
CHROM_SIZES_FILE=$(realpath $2)
BIG_WIG_FILE=$(realpath $3)
OUTPUT_DIR=$(realpath $4)

echo "mkdir -p "$OUTPUT_DIR"/"$NAME"-wigs;" > "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
echo "mkdir -p "$OUTPUT_DIR"/"$NAME"-bigwigs;" >> "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
echo "touch "$OUTPUT_DIR"/"$NAME"-wigs/chrY-and-the-rest."$NAME".wig;" >> "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
cat $CHROM_SIZES_FILE | awk -v name="$NAME" -v big_wig_file="$BIG_WIG_FILE"  -v output_directory="$OUTPUT_DIR" '{print "bigWigToWig -chrom="$1" "big_wig_file" "output_directory"/"name"-wigs/"$1"."name".wig;"}' >> "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
cat $CHROM_SIZES_FILE | grep -v -P "^chr[1-9X]\t" | grep -v -P "^chr1[0-9]\t" | grep -v -P "^chr[2][0-2]\t" | awk -v name="$NAME" -v output_directory="$OUTPUT_DIR" '{print "cat "output_directory"/"name"-wigs/"$1"."name".wig >> "output_directory"/"name"-wigs/chrY-and-the-rest."name".wig;" }' >> "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
cat $CHROM_SIZES_FILE | grep -v -P "^chr[1-9X]\t" | grep -v -P "^chr1[0-9]\t" | grep -v -P "^chr[2][0-2]\t" | awk -v name="$NAME" -v output_directory="$OUTPUT_DIR" '{print "rm "output_directory"/"name"-wigs/"$1"."name".wig;"}' >>  "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
for i in {1..22}; do
  echo "wigToBigWig "$OUTPUT_DIR"/"$NAME"-wigs/chr$i."$NAME".wig "$CHROM_SIZES_FILE" "$OUTPUT_DIR"/"$NAME"-bigwigs/chr$i."$NAME".bw;" >> "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
done

echo "wigToBigWig "$OUTPUT_DIR"/"$NAME"-wigs/chrX."$NAME".wig "$CHROM_SIZES_FILE" "$OUTPUT_DIR"/"$NAME"-bigwigs/chrX."$NAME".bw;" >> "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
echo "wigToBigWig "$OUTPUT_DIR"/"$NAME"-wigs/chrY-and-the-rest."$NAME".wig "$CHROM_SIZES_FILE" "$OUTPUT_DIR"/"$NAME"-bigwigs/chrY-and-the-rest."$NAME".bw;" >> "$OUTPUT_DIR"/"$NAME"-create-chromosome-wise-wigs-and-bigwigs.bash
