#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
import gzip
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This scripts performs postprocessing of chromosoe-wise VCFs created with the use
of script:
  create-dbsnp-vcfs-for-genotyping.py, version 0.0.1
(https://gitlab.com/intelliseq/workflows/-/blob/create-dbsnp-vcfs-for-genotyping.py@0.0.1/src/main/scripts/resources-tools/create-dbsnp-vcfs-for-genotyping.py)
""")

parser.add_argument('-v', '--version', action='version', version=__version__ )
parser.add_argument('VCF_GZ', type=str, help="bgzipped VCF dbSNP for genotyping, before postprocessing")

args = parser.parse_args()
vcf_gz = args.VCF_GZ

header = []
header.append("##fileformat=VCFv4.3")
header.append("##reference=GRCh38.p12")
header.append("##INFO=<ID=RS_INFO,Number=1,Type=String,Description=\"Basic information regarding dbSNP variant - variant identifier (i.e. rs number), a REF alelle and a list of ALT alleles. Format: rs_number : REF_allele : ALT_allele ^ (...) ^ ALT_allele | (.,.) | rs_number : REF_allele : ALT_allele ^ (...) ^ ALT_allele\",Source=\"dbSNP\",Version=\"dbSNP Build 153\">")
header.append("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\thacker")


def get_multiple_records_on_one_positions_rs_info_dictionary(vcf_gz):

    rs_info_dictionary = dict()
    multiple_records_on_one_positions_rs_info_dictionary = dict()

    for raw_line in gzip.open(vcf_gz, 'rb'):

        if not raw_line.decode().startswith("#"):

            line = raw_line.decode().strip().split("\t")

            CHR = line[0]
            POS = line[1]
            CHR_POS = '_'.join([CHR, POS])
            INFO = line[7]

            if CHR_POS in rs_info_dictionary.keys():
                rs_info_dictionary[CHR_POS].append(INFO.replace('RS_INFO=', ''))
            else:
                rs_info_dictionary[CHR_POS] = [INFO.replace('RS_INFO=', '')]

    for key in rs_info_dictionary.keys():
        if len(rs_info_dictionary[key]) > 1:
            multiple_records_on_one_positions_rs_info_dictionary[key] = '|'.join(rs_info_dictionary[key])

    del rs_info_dictionary

    return multiple_records_on_one_positions_rs_info_dictionary


multiple_records_on_one_positions_rs_info_dictionary = get_multiple_records_on_one_positions_rs_info_dictionary(vcf_gz)

print("\n".join(header))


for raw_line in gzip.open(vcf_gz, 'rb'):

    if not raw_line.decode().startswith("#"):

        line = raw_line.decode().strip().split("\t")
        CHR = line[0]
        POS = line[1]
        CHR_POS = '_'.join([CHR, POS])

        if CHR_POS in multiple_records_on_one_positions_rs_info_dictionary.keys():
            line[7] = 'RS_INFO=' + multiple_records_on_one_positions_rs_info_dictionary[CHR_POS]

        print('\t'.join(line))
