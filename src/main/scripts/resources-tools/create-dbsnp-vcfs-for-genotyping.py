#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
from Bio import bgzf
from sys import stdin, stderr
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This script creates contains chromosome-wise bgzipped VCF fils containing basic information
regarding dbSNP variant from dbSNP created created based on dbSNP database
downloaded from  [NCBI ftp site](ftp://ftp.ncbi.nih.gov/snp/) and assembly
report for GRCh38.p12 downloaded from [NCBI ftp site]
(ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.38_GRCh38.p12/).
Those VCFs need to pe postprocessed with:
  **postprocessing-of-chromosome-wise-dbsnp-for-genotyping-vcfs.py, version 0.0.1
 (https://gitlab.com/intelliseq/workflows/-/blob/postprocessing-of-chromosome-wise-dbsnp-for-genotyping-vcfs.py@0.0.1/src/main/scripts/resources-tools/postprocessing-of-chromosome-wise-dbsnp-for-genotyping-vcfs.py)
""")

parser.add_argument('-v', '--version', action='version', version=__version__ )
parser.add_argument('VCF_NAME', type=str, help="output vcf names: chr*.VCF_NAME.vcf.gz")
parser.add_argument('ASSEMBLY_REPORT', type=str, help="path to assembly report")
parser.add_argument('OUTPUT_DIR', type=str, help="path to output directory")

args = parser.parse_args()
assembly_report = args.ASSEMBLY_REPORT
output_directory = args.OUTPUT_DIR
vcf_name = args.VCF_NAME

header = []
header.append("##fileformat=VCFv4.3")
header.append("##reference=GRCh38.p12")
header.append("##INFO=<ID=RS_INFO,Number=1,Type=String,Description=\"Basic information regarding dbSNP variant - variant identifier (i.e. rs number), a REF alelle and a list of ALT alleles. Format: rs_number : REF_allele : ALT_allele ^ (...) ^ ALT_allele\",Source=\"dbSNP\",Version=\"dbSNP Build 153\">")
header.append("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\thacker")

chromosomes = ["chr" + str(i) for i in range(1, 23)] + ["chrX", "chrY-and-the-rest"]
handlers = [bgzf.BgzfWriter(output_directory + "/" + x + "." + vcf_name + ".vcf.gz") for x in chromosomes]

def create_accesion_number_to_chromosome_dict(assembly_report):

    accesion_number_to_chromosome_dict = {}

    for line in open(assembly_report, 'r'):
        if not line.startswith("#"):
            accesion_number = line.split("\t")[6].strip()
            chromosome = line.split("\t")[9].strip()
            if accesion_number not in accesion_number_to_chromosome_dict.keys():
                accesion_number_to_chromosome_dict[accesion_number] = chromosome
            else:
                print("Duplicated accesion number: " + accesion_number, file = stderr)
                quit()

    return accesion_number_to_chromosome_dict

accesion_number_to_chromosome_dict = create_accesion_number_to_chromosome_dict(assembly_report)


for handle in handlers:
    handle.write("\n".join(header) + "\n")

RS = '.'
FILTER = '.'
QUAL = '.'
INFO = 'RS_INFO='
FORMAT = "GT"
GENOTYPE = "1/1"

for raw_line in stdin:

    if not raw_line.startswith("#"):

        line = raw_line.strip().split("\t")

        CHR = accesion_number_to_chromosome_dict[line[0]] if line[0] in accesion_number_to_chromosome_dict.keys() else line[0]

        POS = line[1]
        REF = line[3]
        ALT = ','.join(list(filter(lambda x: x != 'N', [x for x in line[4].split(",")])))

        if CHR != 'na' and REF != "N" and len(ALT.split(",")) > 0 and ALT != '' and ALT != '.':

            new_line = '\t'.join([CHR, POS, RS, REF, ALT, QUAL, FILTER, INFO + ':'.join([line[2], REF, ALT.replace(',', '^')]), FORMAT, GENOTYPE])

            if CHR in chromosomes:
                handlers[chromosomes.index(CHR)].write(new_line + "\n")
            else:
                handlers[chromosomes.index("chrY-and-the-rest")].write(new_line + "\n")

for handle in handlers:
    handle.close()
