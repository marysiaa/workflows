#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This script is used to create hg19 ESP6500 annotation file based on
ESP6500SI-V2-SSA137.GRCh38-liftover.chr<*>.snps_indels.vcf downloaded from
[NHLBI Exome Sequencing Project (ESP) Download page](http://evs.gs.washington.edu/EVS/).

This script calculates AF fields: ISEQ_ESP6500_EA_AF, ISEQ_ESP6500_AA_AF, ISEQ_ESP6500_AN,
ISEQ_ESP6500_AF using AC and AN field.
""")

parser.add_argument('-v', '--version', action='version', version=__version__)
args = parser.parse_args()


header = []
header.append("##fileformat=VCFv4.3")
header.append("##FILTER=<ID=PASS,Description=\"All filters passed\">")
header.append("##reference=hg19")
header.append("##INFO=<ID=ISEQ_ESP6500_EA_AF,Number=A,Type=Float,Description=\"Alternate allele frequency - ESP6500, European American population (EA)\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("##INFO=<ID=ISEQ_ESP6500_AA_AF,Number=A,Type=Float,Description=\"Alternate allele frequency - ESP6500, African American population (AA)\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("##INFO=<ID=ISEQ_ESP6500_AN,Number=1,Type=Integer,Description=\"Total allele count for - ESP6500\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("##INFO=<ID=ISEQ_ESP6500_AF,Number=A,Type=Float,Description=\"Alternate allele frequency - ESP6500\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")

def calculate_af(ac, an, precision=5):
    format_string = '{0:.' + str(precision) + 'f}'
    try:
        x = format_string.format(round(float(ac)/float(an), precision)).rstrip('0').rstrip('.')
    except:
        x = '.'
    return x

print("\n".join(header))

for raw_line in stdin:

    if not raw_line.startswith("#"):

        line = raw_line.strip().split("\t")
        INFO = []

        for info in line[7].split(";"):

            if info.startswith("EA_AC="):
                EA_AC = info[info.index("=") + 1:].split(",")
                AN = sum([int(x) for x in EA_AC])
                INFO.append("ISEQ_ESP6500_EA_AF=" + ",".join([calculate_af(x, AN) for x in EA_AC[:-1]]))

            elif info.startswith("AA_AC="):
                AA_AC = info[info.index("=") + 1:].split(",")
                AN = sum([int(x) for x in AA_AC])
                INFO.append("ISEQ_ESP6500_AA_AF=" + ",".join([calculate_af(x, AN) for x in AA_AC[:-1]]))

            elif info.startswith("TAC="):
                TAC = info[info.index("=") + 1:].split(",")
                AN = sum([int(x) for x in TAC])
                INFO.append("ISEQ_ESP6500_AN=" + str(AN))
                INFO.append("ISEQ_ESP6500_AF=" + ",".join([calculate_af(x, AN) for x in TAC[:-1]]))

        line[7] = ";".join(INFO)

        print("\t".join(line))
