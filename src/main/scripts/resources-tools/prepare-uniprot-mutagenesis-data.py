#!/usr/bin/python3

__version__ = '0.0.3'

import json
import argparse
from typing import Dict, List, Any


def prepare_id_gene_map(ensembl_file: str) -> Dict:
    gene_uniprot_id_dict = {}
    with open(ensembl_file) as f:
        for line in f:
            gene, uniprot_id = line.strip().split("\t")
            gene_uniprot_id_dict[uniprot_id] = gene
    return gene_uniprot_id_dict


def process_mutagenesis_table_cell(cell: str) -> Any:
    try:
        splitted_cell = cell.split("; /")
        #print(splitted_cell)
        beginning = splitted_cell[0].split("..")[0].strip()
        end = splitted_cell[0].split("..")[-1].strip()
        ref = splitted_cell[1].split("->", 1)[0].replace('note="', "")
        alt = splitted_cell[1].split("->", 1)[1].split(":", 1)[0]
        description = splitted_cell[1].split(
            ":", 1)[1].strip().strip(";").strip('"')  # [0:-1]
    except IndexError:
        return None
    else:
        #print(beginning, end, ref, alt, description)
        return [beginning, end, ref, alt, description]


def prepare_uniprot_dictionary(uniprot_input_table: str, ensembl_dict: Dict) -> Dict:
    uniprot_mutagenesis_dictionary = {}
    with open(uniprot_input_table) as f:
        header = f.readline().split("\t")
        uniprot_id_index = header.index("Entry")
        genes_index = header.index("Gene Names")
        mutagenesis_index = header.index("Mutagenesis")
        next(f)
        for line in f:
            if "MUTAGEN" in line:
                splitted_line = line.split("\t")
                uniprot_id = splitted_line[uniprot_id_index]
                genes = [i for i in splitted_line[genes_index].split(" ") if i]
                try:
                    ensembl_gene = ensembl_dict[uniprot_id]
                    if ensembl_gene not in genes:
                        genes.append(ensembl_gene)
                except KeyError:
                    pass
                mutagenesis_table = splitted_line[mutagenesis_index].split("MUTAGEN")[
                    1:]
                parsed_mutagenesis_table = [process_mutagenesis_table_cell(
                    i) for i in mutagenesis_table if process_mutagenesis_table_cell(i)]
                key_names = [gene+"_"+uniprot_id for gene in genes]
                for key_name in key_names:
                    uniprot_mutagenesis_dictionary[key_name] = parsed_mutagenesis_table
    return uniprot_mutagenesis_dictionary


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Prepares json file needed for acmg ps3 annotation: '
                    'gene_uniprotID => [[start1, end1, ref_eq1, alt_seq1, description1], [start2, end2, ref_seq2, alt_seq2, description2], ...]')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('-u', '--uniprot', type=str, required=True,
                        help="Path to tab delimited file with UniProt table file")
    parser.add_argument('-e', '--ensembl', type=str, required=True,
                        help="Path to tab delimited file with Ensembl gene to UniProt ID mapping file")
    parser.add_argument('-o', '--output', type=str,
                        required=True, help="Path to output json file")
    args = parser.parse_args()
    ensembl_gene_dict = prepare_id_gene_map(args.ensembl)
    uniprot_mutagenesis_dict = prepare_uniprot_dictionary(
        args.uniprot, ensembl_gene_dict)
    with open(args.output, "w") as output_json:
        t = json.dumps(uniprot_mutagenesis_dict)
        output_json.write(t)
