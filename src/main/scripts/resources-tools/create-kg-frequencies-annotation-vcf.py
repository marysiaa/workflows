#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
from Bio import bgzf
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
    The script modifies original 1000 Genomes database with SNV and INDEL calls (v2a), aligned directly to GRCh38,
    downloaded from [1000 Genomes FTP]:
    http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20181203_biallelic_SNV/

    The script creates chromosome-wise (chr1, ... chr22, chrX, chrY-and-the-rest) bgzipped VCFs with the following
    INFO fields:
        + ISEQ_KG_AF (original field: AF)
        + ISEQ_KG_EAS_AF (original field: EAS_AF)
        + ISEQ_KG_EUR_AF (original field: EUR_AF)
        + ISEQ_KG_AFR_AF (original field: AFR_AF)
        + ISEQ_KG_AMR_AF (original field: AMR_AF)
        + ISEQ_KG_SAS_AF (original field: SAS_AF)
""")

parser.add_argument('OUTPUT_DIR', type=str, help="path to output directory")
parser.add_argument('-v', '--version', action='version', version=__version__)
args = parser.parse_args()
output_directory = args.OUTPUT_DIR

header = []
header.append("##fileformat=VCFv4.3")
header.append("##FILTER=<ID=PASS,Description=\"All filters passed\">")
header.append("##reference=ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/GRCh38_reference_genome/GRCh38_full_analysis_set_plus_decoy_hla.fa")
header.append("##INFO=<ID=ISEQ_KG_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - 1000 Genomes\",Source=\"1000 Genomes\",Version=\"1000 Genomes - 2019-02-27 biallelic SNVs and INDELs (GRCh38, v2a)\">")
header.append("##INFO=<ID=ISEQ_KG_EAS_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - 1000 Genomes, East Asian population (EAS)\",Source=\"1000 Genomes\",Version=\"1000 Genomes - 2019-02-27 biallelic SNVs and INDELs (GRCh38, v2a)\">")
header.append("##INFO=<ID=ISEQ_KG_EUR_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - 1000 Genomes, European population (EUR)\",Source=\"1000 Genomes\",Version=\"1000 Genomes - 2019-02-27 biallelic SNVs and INDELs (GRCh38, v2a)\">")
header.append("##INFO=<ID=ISEQ_KG_AFR_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - 1000 Genomes, African population (AFR)\",Source=\"1000 Genomes\",Version=\"1000 Genomes - 2019-02-27 biallelic SNVs and INDELs (GRCh38, v2a)\">")
header.append("##INFO=<ID=ISEQ_KG_AMR_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - 1000 Genomes, Mixed American population (AMR)\",Source=\"1000 Genomes\",Version=\"1000 Genomes - 2019-02-27 biallelic SNVs and INDELs (GRCh38, v2a)\">")
header.append("##INFO=<ID=ISEQ_KG_SAS_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - 1000 Genomes, South Asian population (SAS)\",Source=\"1000 Genomes\",Version=\"1000 Genomes - 2019-02-27 biallelic SNVs and INDELs (GRCh38, v2a)\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")

chromosomes = ["chr" + str(i) for i in range(1, 23)] + ["chrX", "chrY-and-the-rest"]
handlers = [bgzf.BgzfWriter(output_directory + "/" + x + ".kg.vcf.gz") for x in chromosomes]

for handle in handlers:
    handle.write("\n".join(header) + "\n")

for raw_line in stdin:

    if not raw_line.startswith("#"):

        line = raw_line.strip().split("\t")

        CHR = line[0]
        INFO = []

        for info in line[7].split(";"):
            if info.startswith("AF="):
                INFO.append("ISEQ_KG_AF=" + info[info.index("=") + 1:])

            elif info.startswith("EAS_AF="):
                INFO.append("ISEQ_KG_EAS_AF=" + info[info.index("=") + 1:])

            elif info.startswith("EUR_AF="):
                INFO.append("ISEQ_KG_EUR_AF=" + info[info.index("=") + 1:])

            elif info.startswith("AFR_AF="):
                INFO.append("ISEQ_KG_AFR_AF=" + info[info.index("=") + 1:])

            elif info.startswith("AMR_AF="):
                INFO.append("ISEQ_KG_AMR_AF=" + info[info.index("=") + 1:])

            elif info.startswith("SAS_AF="):
                INFO.append("ISEQ_KG_SAS_AF=" + info[info.index("=") + 1:])

        line[7] = ";".join(INFO)

        if CHR in chromosomes:
            handlers[chromosomes.index(CHR)].write('\t'.join(line) + "\n")
        else:
            handlers[chromosomes.index("chrY-and-the-rest")].write('\t'.join(line) + "\n")


for handle in handlers:
    handle.close()
