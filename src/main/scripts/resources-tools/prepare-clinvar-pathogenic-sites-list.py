#!/usr/bin/python3


import gzip
import argparse
from typing import List
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)


__version__ = '0.0.2'


def read_file(input_vcf: str) -> List:
    with gzip.open(input_vcf, "rt") as f:
        return f.readlines()


def prepare_data_file(vcf_lines: str, output_tab):
    f = open(output_tab, "w")
    for line in vcf_lines:
        if line[0] != "#":
            splitted_line = line.strip().split("\t")
            chr = splitted_line[0]
            pos = splitted_line[1]
            info = [i.split('=') for i in splitted_line[7].split(";")]
            info_fields = {}
            for i in info:
                info_fields[i[0]] = i[1]
            try:
                significance = info_fields["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"].replace(
                    "conflicting_interpretations_of_pathogenicity", "uncertain_significance").split(":")
            except KeyError:
                continue
            else:
                pathogenic_categories = ["pathogenic", "pathogenic/likely_pathogenic", "pathogenic_low_penetrance", "likely_pathogenic_low_penetrance",
                                         "likely_pathogenic", "likely_pathogenic_low_penetrance"]
                pathogenic_test = bool(
                    [i for i in significance if i in pathogenic_categories])
                if pathogenic_test:
                    text = chr+"\t"+pos+"\t"+":".join(significance)+"\n"
                    f.write(text)
    f.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Prepares tab delimited file needed for acmg annotations, "clinvar-pathogenic-sites.tab":\n'
                                                 'chr, pos, significance')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str,
                        help="Path to annotated with SnpEff Clinvar vcf.gz file, unzipped input may be also piped from stdin)")
    parser.add_argument('-o', '--output', type=str,
                        required=True, help="Path to output tab file")
    args = parser.parse_args()
    if args.input:
        input_vcf = read_file(args.input)
        prepare_data_file(input_vcf, args.output)
    else:
        prepare_data_file(stdin, args.output)
