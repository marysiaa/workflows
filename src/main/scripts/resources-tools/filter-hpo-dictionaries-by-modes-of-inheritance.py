#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This script is used to create HPO-based dictionaries for annotating (HPO - Human Phenotypes Ontology):
anakin:  /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/human-phenotype-ontology

It removes (default) or keeps mode of inheritance related HPO terms from genes-phenotype dictionaries.

""")

parser.add_argument('LIST_OF_MODES_OF_INHERITANCE', type=str, help="Path to list-of-modes-of-inheritance.txt")
parser.add_argument('-o', action='store_true', help="Keep only modes of inheritance")
parser.add_argument('-v', '--version', action='version', version=__version__)

args = parser.parse_args()

list_of_modes_of_inheritance = args.LIST_OF_MODES_OF_INHERITANCE
keep_modes_of_inheritance = args.o

verbose_mode_of_ineritance = []

for line in open(list_of_modes_of_inheritance, 'r'):
    verbose_mode_of_ineritance = verbose_mode_of_ineritance + \
                                 [x.replace(' ', '_') for x in line.strip().split('\t')[0].split(',')]

verbose_mode_of_ineritance = set(verbose_mode_of_ineritance)


for line in stdin:
    if line.startswith('#'):
        print(line.strip())

    else:

        if line.startswith('#'):
            print(line.strip())
        else:

            line = line.strip().split('\t')

            if keep_modes_of_inheritance:

                line[1] = ','.join([x for x in line[1].split(',') if x in verbose_mode_of_ineritance])

            else:

                line[1] = ','.join([x for x in line[1].split(',') if x not in verbose_mode_of_ineritance])

            if line[1]:
                print('\t'.join(line))
