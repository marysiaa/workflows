#!/usr/bin/python3

__version__ = '0.0.3'

import json
import argparse
from typing import Dict, List, Any


def prepare_id_gene_map(ensembl_file: str) -> Dict:
    gene_uniprot_id_dict = {}
    with open(ensembl_file) as f:
        for line in f:
            gene, uniprot_id = line.strip().split("\t")
            gene_uniprot_id_dict[uniprot_id] = gene
    return gene_uniprot_id_dict


def get_functional_sites_column_index(header_line: List) -> List:
    names = ["Active site", "Binding site", "Calcium binding",
             "DNA binding", "Nucleotide binding", "Signal peptide", "Zinc finger"]
    return [header_line.index(i) for i in names if i]


def get_functional_info(functional_cell: str, sequence: str) -> Any:
    try:
        functional_data = functional_cell.split(";  /")
        needed_info = functional_data[0].split()
        try:
            note = [i.split("=")[1][0:-1].replace('"', "")
                    for i in functional_data if i[0:4] == "note"][0]
        except (IndexError, ValueError):
            note = ""
        site_type = needed_info[0]
        beginning = needed_info[1].split("..")[0]
        end = needed_info[1].split("..")[-1]
        functional_seq = sequence[int(beginning)-1:int(end)]
    except (IndexError, ValueError):
        pass
    else:
        return [beginning, end, functional_seq, site_type, note]


def prepare_uniprot_dictionary(uniprot_input_table: str, ensembl_dict: Dict) -> Dict:
    uniprot_functional_sites_dictionary = {}
    with open(uniprot_input_table) as f:
        header = f.readline().strip('\n').split("\t")
        uniprot_id_index = header.index("Entry")
        genes_index = header.index("Gene names")
        sequence_index = header.index("Sequence")
        functional_sites_indexes = get_functional_sites_column_index(header)
        next(f)
        for line in f:
            splitted_line = line.strip('\n').split("\t")
            #uniprot_id = splitted_line[uniprot_id_index]
            #genes = splitted_line[genes_index].split(" ")
            if len(functional_sites_indexes) > 0:
                #uniprot_id = splitted_line[uniprot_id_index]
                #genes = splitted_line[genes_index].split(" ")
                functional_cells = [splitted_line[i]
                                    for i in functional_sites_indexes]
                # print(functional_cells)
                non_empty_functional_cells = [i for i in functional_cells if i]
                # print(non_empty_functional_cells)
                if len(non_empty_functional_cells) > 0:
                    uniprot_id = splitted_line[uniprot_id_index]
                    genes = [
                        i for i in splitted_line[genes_index].split(" ") if i]
                    try:
                        ensembl_gene = ensembl_dict[uniprot_id]
                        if ensembl_gene not in genes:
                            genes.append(ensembl_gene)
                    except KeyError:
                        pass
                    sequence = splitted_line[sequence_index]
                    if sequence:
                        functional_sites_list = [get_functional_info(
                            i, sequence) for i in non_empty_functional_cells if get_functional_info(i, sequence)]
                        if functional_sites_list != [[]]:
                            #print("functional sites list: "+str(functional_sites_list))
                            key_names = [gene+"_"+uniprot_id for gene in genes]
                            for key_name in key_names:
                                uniprot_functional_sites_dictionary[key_name] = functional_sites_list
        return uniprot_functional_sites_dictionary


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Prepares json file needed for acmg pm1 annotation: '
                    'gene_uniprotID => [[region_start1, region_end1, region_seq1, region_type1, additional_info1], [region_start2, region_end2, region_seq2, region_type2, additonal_info2]...]')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('-u', '--uniprot', type=str, required=True,
                        help="Path to tab delimited file with UniProt table file")
    parser.add_argument('-e', '--ensembl', type=str, required=True,
                        help="Path to tab delimited file with Ensembl gene to UniProt ID mapping file")
    parser.add_argument('-o', '--output', type=str,
                        required=True, help="Path to output json file")
    args = parser.parse_args()
    ensembl_gene_dict = prepare_id_gene_map(args.ensembl)
    uniprot_functional_sites_dict = prepare_uniprot_dictionary(
        args.uniprot, ensembl_gene_dict)
    with open(args.output, "w") as output_json:
        t = json.dumps(uniprot_functional_sites_dict)
        output_json.write(t)
