#!/bin/bash

VERSION="0.0.1"

PATH_TO_SCORES_DIRECTORY=$(readlink -f $1)
NAME=$2

echo "mkdir $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs"

for i in {1..22}; do
  echo "printf \"variableStep\tchrom=chr$i\n\" > $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chr$i.$NAME.hg19.wig ; cat $PATH_TO_SCORES_DIRECTORY/chr$i.maf.rates | awk '{print NR\"\t\"\$2}' >> $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chr$i.$NAME.hg19.wig &"
done

echo "printf \"variableStep\tchrom=chrX\n\" > $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrX.$NAME.hg19.wig ; cat $PATH_TO_SCORES_DIRECTORY/chrX.maf.rates | awk '{print NR\"\t\"\$2}' >> $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrX.$NAME.hg19.wig &"
echo "printf \"variableStep\tchrom=chrY\n\" > $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrY.$NAME.hg19.wig ; cat $PATH_TO_SCORES_DIRECTORY/chrY.maf.rates | awk '{print NR\"\t\"\$2}' >> $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrY.$NAME.hg19.wig &"
echo "printf \"variableStep\tchrom=chrM\n\" > $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrM.$NAME.hg19.wig ; cat $PATH_TO_SCORES_DIRECTORY/chrM.maf.rates | awk '{print NR\"\t\"\$2}' >> $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-wigs/chrM.$NAME.hg19.wig &"
