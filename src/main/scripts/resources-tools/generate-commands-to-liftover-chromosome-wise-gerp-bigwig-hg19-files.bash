#!/bin/bash

VERSION="0.0.1"

PATH_TO_SCORES_DIRECTORY=$(readlink -f $1)
NAME=$2
CHAIN_FILE=$(readlink -f $3)

echo "mkdir $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs"
echo "mkdir $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/logs"
echo "mkdir $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/raw-bgrs"
echo "mkdir $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/raw-bigwigs"

for i in {1..22}; do
  echo "CrossMap.py bigwig $CHAIN_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chr$i.$NAME.hg19.bw chr$i.$NAME.hg38 &> $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/logs/chr$i.$NAME-hg38-raw-liftovered-bigwigs.log &"
done

echo "CrossMap.py bigwig $CHAIN_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chrX.$NAME.hg19.bw chrX.$NAME.hg38 &> $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/logs/chrX.$NAME-hg38-raw-liftovered-bigwigs.log &"
echo "CrossMap.py bigwig $CHAIN_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chrY.$NAME.hg19.bw chrY.$NAME.hg38 &> $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/logs/chrY.$NAME-hg38-raw-liftovered-bigwigs.log &"
echo "CrossMap.py bigwig $CHAIN_FILE $PATH_TO_SCORES_DIRECTORY/$NAME-hg19-bigwigs/chrM.$NAME.hg19.bw chrM.$NAME.hg38 &> $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/logs/chrZ.$NAME-hg38-raw-liftovered-bigwigs.log &"

echo "mv *gerp.hg38.bw $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/raw-bigwigs"
echo "mv *gerp.hg38.sorted.bgr $PATH_TO_SCORES_DIRECTORY/$NAME-hg38-raw-liftovered-bigwigs-and-bgrs/raw-bgrs"
