#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)


parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
    The script modifies original MITOMAP polymorphisms.vcf file downloaded from
    MITOMAP Resources site (https://mitomap.org/foswiki/bin/view/MITOMAP/Resources).

    The script performs the following actions:
        - converts AF from percentage to numbers
        - changes AC and AF fields names to MITOMAP_AC and MITOMAP_AF
        - changes chromosome naming convention from MT to chrM
        - modifies MT contig header line
""")
parser.add_argument('-v', '--version', action='version', version=__version__)
args = parser.parse_args()


for line in stdin:


    if line.startswith('##fileDate'):
        date = line.strip()[11:]
        print(line.strip())
    elif line.startswith('##INFO=<ID=AC,'):
        print(line.replace('##INFO=<ID=AC,', '##INFO=<ID=MITOMAP_AC,').replace('">', ' - MITOMAP",Source="MITOMAP",Version="' + date +'">').strip())
    elif line.startswith('##INFO=<ID=AF,'):
        print(line.replace('##INFO=<ID=AF,', '##INFO=<ID=MITOMAP_AF,').replace('">', ' - MITOMAP",Source="MITOMAP",Version="' + date +'">').strip())
    elif line.startswith('##contig=<ID=MT,'):
        print('##contig=<ID=chrM,length=16569>')
    elif line.startswith('#'):
        print(line.strip())
    else:

        line = line.strip().split('\t')

        INFO = line[7].split(';')

        MITOMAP_AC = 'MITOMAP_' + INFO[0]
        MITOMAP_AF = 'MITOMAP_AF=' + ','.join([str(float("{0:.7f}".format(float(af)/100))) for af in INFO[1][INFO[1].index('=') + 1:].split(',')])

        line[7] = MITOMAP_AC + ';' + MITOMAP_AF

        print('chrM\t' + '\t'.join(line[1:]))
