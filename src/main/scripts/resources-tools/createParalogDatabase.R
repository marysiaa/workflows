library(argparse)
library(biomaRt)

version <- '0.0.1'

parser <-
ArgumentParser(description = 'Prepares paralog data for FuSeq_WES, based on the README instructions')
parser$add_argument('-v',
                    '--version',
                    action = 'version',
                    version = paste(sub(".*=.*/", "", commandArgs()[4]), version))
parser$add_argument('--outfile', type = 'character', help = 'Path to output RData file')
parser$add_argument('--ensembl_version', type='character', help = "Ensembl version")

args <- parser$parse_args()



#remove version in the gene name before querying
ensembl = useEnsembl(biomart="ensembl", dataset="hsapiens_gene_ensembl",version = args$ensembl_version) #GRCh=38

att_meta=attributes(ensembl)
att_all=att_meta$attributes


attList=c('ensembl_gene_id',"external_gene_name","hsapiens_paralog_ensembl_gene","hsapiens_paralog_associated_gene_name","hsapiens_paralog_orthology_type")
paralogs <- getBM(attributes=attList, mart = ensembl, filters = 'biotype', values = 'protein_coding')

#rename the column
p=which(att_all$name %in% attList & att_all$page=="homologs")
newName=att_all$description[p]
newName=gsub(" ",".",newName)
colnames(paralogs)=newName

save(paralogs, file=args$outfile)
