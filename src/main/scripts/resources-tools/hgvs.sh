#!/bin/bash

set -e -o pipefail

MAPPING_FILE=$1
CNV_FILE=$2

head -1 $CNV_FILE | cut -f1-4 > tmp.header
tail -n +2 $CNV_FILE | cut -f1-4 > tmp.cnv
cut -f2 tmp.cnv | sed 's/-/_/' > location.txt
cut -f3 tmp.cnv | grep -o 'D[eu][lp]' | sed 's/D/d/' > type.txt

while read f
do
  old=$(echo $f | cut -f1 -d ' ')
  new=$(echo $f | cut -f2 -d ' ')
  sed -i "s/^$old:/$new:g./" location.txt
done<$MAPPING_FILE

printf "HGVS\n" | paste tmp.header - > header.txt
cat header.txt <( paste tmp.cnv <(paste location.txt  type.txt  -d '' )) > decipher-with-hgvs.cnv
rm location.txt type.txt header.txt tmp.header tmp.cnv