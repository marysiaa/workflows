#!/bin/bash

VERSION="0.0.1"

PATH_TO_SCORES_DIRECTORY=$(readlink -f $1)

for i in {1..22}; do
	CHR_SIZE=$(cat "$PATH_TO_SCORES_DIRECTORY"/chr"$i".maf.rates | wc -l)
	echo -e  chr"$i""\t""$CHR_SIZE"
done

CHR_SIZE=$(cat "$PATH_TO_SCORES_DIRECTORY"/chrX.maf.rates | wc -l)
echo -e chrX"\t""$CHR_SIZE"

CHR_SIZE=$(cat "$PATH_TO_SCORES_DIRECTORY"/chrY.maf.rates | wc -l)
echo -e chrY"\t""$CHR_SIZE"

CHR_SIZE=$(cat "$PATH_TO_SCORES_DIRECTORY"/chrM.maf.rates | wc -l)
echo -e chrM"\t""$CHR_SIZE"
