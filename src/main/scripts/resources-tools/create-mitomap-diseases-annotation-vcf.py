#!/usr/bin/env python3

__version__ = '2.0.0'

import argparse
import pysam
import re
from typing import List

#   README: MITOMAP_DISEASES_ABBREVIATIONS
#   --------------------------------------
#
#   Dictionary of MITOMAP abbreviations of diseases was created by copying the
#   content from: http://mitomap.org/foswiki/bin/view/MITOMAP/DiseaseList, with the following amendments applied:
#
#   - item "LD: Leigh Disease (or LS: Leigh Syndrome)" was split into two records: "LD: Leigh Disease"
#     and "LS: Leigh Syndrome"
#   - item "MERME: MERRF/MELAS, Lactic Acidosis, and Stroke-like episodes overlap disease" was changed to
#     "MERME: Myoclonic Epilepsy and Ragged Red Muscle Fibers/Mitochondrial Encephalomyopathy, Lactic Acidosis,
#     and Stroke-like episodes overlap disease"
#   - item "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa; alternate phenotype at this locus is
#     reported as Leigh Disease" was changed to "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa"
#   - item "Multisystem Mitochondrial Disorder (myopathy, encephalopathy, blindness, hearing loss,
#     peripheral neuropathy)" was removed
#
#   Additionally, the following items were added:
#
#   - HCM : Hypertrophic CardioMyopathy
#   - PEO: Progressive External Ophthalmoplegia
#   - PD: Parkinson Disease
#   - FSHD: Facioscapulohumeral Muscular Dystrophy
#   - MIDM: Maternally Inherited Diabetes Mellitus
#   - MDD: Major Depressive Disorder
#   - BD: Bipolar Disorder
#   - SZ: Schizophrenia
#   - BSN: Bilateral Striatal Necrosis
#   - MR: Mental Retardation
#   - DM: Diabetes Mellitus
#   - NSHL: Non-Syndromic Hearing Loss
#   - RP: Retinitis Pigmentosa
#   - CM: Cardiomiopathy
#   - ASD: Autism spectrum disorder
#
#  The resource for this file (http://mitomap.org/foswiki/bin/view/MITOMAP/DiseaseList) was last updated on 15-09-2015.

MITOMAP_DISEASES_ABBREVIATIONS = {
    "AD": "Alzheimer's_Disease",
    "ADPD": "Alzheimer's_Disease_and_Parkinsons's_Disease",
    "AMD": "Age-Related_Macular_Degeneration",
    "AMDF": "Ataxia_Myoclonus_and_Deafness",
    "AMegL": "Acute_Megakaryoblastic_Leukemia",
    "ASD": "Autism_Spectrum_Disorder",
    "BD": "Bipolar_Disorder",
    "BSN": "Bilateral_Striatal_Necrosis",
    "CIPO": "Chronic_Intestinal_Pseudoobstruction_with_myopathy_and_Ophthalmoplegia",
    "CM": "Cardiomiopathy",
    "CPEO": "Chronic_Progressive_External_Ophthalmoplegia",
    "DEAF": "Maternally_inherited_DEAFness_or_aminoglycoside-induced_DEAFness",
    "DEMCHO": "Dementia_and_Chorea",
    "DM": "Diabetes_Mellitus",
    "DMDF": "Diabetes_Mellitus_&_DeaFness",
    "ESOC": "Epilepsy_Strokes_Optic_atrophy_&_Cognitive_decline",
    "EXIT": "Exercise_Intolerance",
    "FBSN": "Familial_Bilateral_Striatal_Necrosis",
    "FICP": "Fatal_Infantile_Cardiomyopathy_Plus_a_MELAS-associated_cardiomyopathy",
    "FSGS": "Focal_Segmental_Glomerulosclerosis",
    "FSHD": "Facioscapulohumeral_muscular_dystrophy",
    "GER": "Gastrointestinal_Reflux",
    "HCM": "Hypertrophic_CardioMyopathy",
    "KSS": "Kearns_Sayre_Syndrome",
    "LD": "Leigh_Disease",
    "LDYT": "Leber's_hereditary_optic_neuropathy_and_DYsTonia",
    "LHON": "Leber_Hereditary_Optic_Neuropathy",
    "LIMM": "Lethal_Infantile_Mitochondrial_Myopathy",
    "LS": "Leigh_Syndrome",
    "LVNC": "Left_Ventricular_Noncompaction",
    "Longevity": "Long_life",
    "MDD": "Major_Depressive_Disorder",
    "MDM": "Myopathy_and_Diabetes_Mellitus",
    "MELAS": "Mitochondrial_Encephalomyopathy_Lactic_Acidosis_and_Stroke-like_episodes",
    "MEPR": "Myoclonic_Epilepsy_and_Psychomotor_Regression",
    "MERME": "Myoclonic_Epilepsy_and_Ragged_Red_Muscle_Fibers/"
             "Mitochondrial_Encephalomyopathy_Lactic_Acidosis_and_Stroke-like_episodes_overlap_disease",
    "MERRF": "Myoclonic_Epilepsy_and_Ragged_Red_Muscle_Fibers",
    "MEc": "Mitochondrial_Encephalocardiomyopathy",
    "MEm": "Mitochondrial_Encephalomyopathy",
    "MHCM": "Maternally_Inherited_Hypertrophic_CardioMyopathy",
    "MI": "Myocardial_Infarction",
    "MICM": "Maternally_Inherited_Cardiomyopathy",
    "MIDD": "Maternally_Inherited_Diabetes_and_Deafness",
    "MIDM": "Maternally_inherited_diabetes_mellitus",
    "MILS": "Maternally_Inherited_Leigh_Syndrome",
    "MM": "Mitochondrial_Myopathy",
    "MMC": "Maternal_Myopathy_and_Cardiomyopathy",
    "MR": "Mental_Retardation",
    "NAION": "Nonarteritic_Anterior_Ischemic_Optic_Neuropathy",
    "NARP": "Neurogenic_muscle_weakness_Ataxia_and_Retinitis_Pigmentosa",
    "NIDDM": "Non-Insulin_Dependent_Diabetes_Mellitus",
    "NRTI-PN": "Antiretroviral_Therapy-Associated_Peripheral_Neuropathy",
    "NSHL": "Non-Syndromic_Hearing_Loss",
    "OAT": "Oligoasthenoteratozoospermia",
    "PD": "Parkinson_Disease",
    "PEG": "Pseudoexfoliation_Glaucoma",
    "PEM": "Progressive_Encephalopathy",
    "PEO": "Progressive_External_Ophthalmoplegia",
    "PME": "Progressive_Myoclonus_Epilepsy",
    "POAG": "Primary_Open_Angle_Glaucoma",
    "RP": "Retinitis_Pigmentosa",
    "RTT": "Rett_Syndrome",
    "SIDS": "Sudden_Infant_Death_Syndrome",
    "SNHL": "Sensorineural_Hearing_Loss",
    "SZ": "Schizophrenia"
}


class AnnotationInfo(object):

    def __init__(self, info_id: str, info_number: str, info_type: str, info_description: str, info_source=None,
                 info_version=None, source_field_id=None):
        super().__init__()
        self.info_id = info_id
        self.info_number = info_number
        self.info_type = info_type
        self.info_description = info_description
        self.info_source = info_source
        self.info_version = info_version
        self.source_field_id = source_field_id

    def get_header_info_line(self):
        info_source = '' if None else ',Source="{}"'.format(self.info_source)
        info_version = '' if None else ',Version="{}"'.format(self.info_version)

        return '##INFO=<ID={},Number={},Type={},Description="{}"{}{}>'.format(self.info_id, self.info_number,
                                                                              self.info_type,
                                                                              self.info_description, info_source,
                                                                              info_version)


class _Annotations(object):
    AnnotationsList: List[AnnotationInfo] = list()

    MITOMAP_DISEASE = AnnotationInfo(
        info_id='MITOMAP_DISEASE',
        info_number='A',
        info_type='String',
        info_description='Putative Disease Association - MITOMAP. Format: disease : (...) : disease',
        info_source='MITOMAP',
        source_field_id='Disease')
    AnnotationsList.append(MITOMAP_DISEASE)

    MITOMAP_DISEASE_STATUS = AnnotationInfo(
        info_id='MITOMAP_DISEASE_STATUS',
        info_number='A',
        info_type='String',
        info_description='Disease Association Status - MITOMAP. Format: status : (...) : status',
        info_source='MITOMAP',
        source_field_id='DiseaseStatus')
    AnnotationsList.append(MITOMAP_DISEASE_STATUS)


sep = re.compile('[\+,]|-&-|\|-|-/')


def get_position_and_alleles(line):
    chrom = line.contig
    pos = int(line.pos)
    ref = line.ref
    alt = line.alts[0]
    return chrom, pos, ref, alt


def change_disease_field(record, field_name, pattern):
    disease_list = re.sub(pattern, "-/-", record.info[field_name][0]).split(
        "-/-")
    new_list = [i.strip().strip("-") for i in disease_list]
    reformatted_disease_list = []
    for item in new_list:
        reformatted_item = []
        subfields = item.split("-")
        for subfield in subfields:
            try:
                reformatted_subfield = MITOMAP_DISEASES_ABBREVIATIONS[subfield]
            except KeyError:
                reformatted_subfield = subfield
            reformatted_item.append(reformatted_subfield)
        reformatted_item = "-".join(reformatted_item)
        reformatted_disease_list.append(reformatted_item)
    return ":".join(reformatted_disease_list).replace("::", "-").replace("---", "-")


def main(vcf_name, vcf_writer_name):
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    header_vcf = vcf_reader.header
    # Add INFO fields
    version = [record.value for record in header_vcf.records if record.key == "fileDate"][0]
    for annotation in _Annotations.AnnotationsList:
        annotation.info_version = version
        header_vcf.add_line(annotation.get_header_info_line())

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)
    for record in vcf_reader.fetch():
        new_record = vcf_writer.new_record(
            contig=record.contig, start=record.start, stop=record.stop,
            alleles=record.alleles,
            id=record.id, qual=record.qual, filter=record.filter, info=None)

        diseases_field = change_disease_field(record, "Disease", sep)
        status_field = change_disease_field(record, "DiseaseStatus", sep)
        new_record.info["MITOMAP_DISEASE"] = diseases_field
        new_record.info["MITOMAP_DISEASE_STATUS"] = status_field
        vcf_writer.write(new_record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=
                                     """
                                     The script modifies original MITOMAP diseases.vcf file downloaded from
                                     MITOMAP  Resources site (https://mitomap.org/foswiki/bin/view/MITOMAP/Resources)
                                 
                                     """)

    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()

    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
