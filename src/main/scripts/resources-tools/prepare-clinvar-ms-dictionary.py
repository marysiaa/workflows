#!/usr/bin/python3

import gzip
import argparse
from typing import Dict, List
import json
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.3'

class Transcript(object):
    """represents description of one transcript extracted from the ANN field
    arguments: one item from the ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


pathogenic_categories = ["pathogenic", "pathogenic/likely_pathogenic", "pathogenic_low_penetrance", "likely_pathogenic_low_penetrance",
                         "likely_pathogenic", "likely_pathogenic_low_penetrance"]
other_categories = ["benign", "likely_benign",
                    "uncertain_significance", "benign/likely_benign"]


def find_good_transcripts(transcript_list: List, gene: str) -> List:
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type()) and \
                (transcript.transcript_type() == "protein_coding") and \
                ('WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings())\
                and (gene in transcript.transcript_gene()):
            good_transcripts.append(transcript)
    return good_transcripts


def process_line(line: str) -> Dict:
    line_splitted = line.strip().split("\t")
    info = [i.split('=') for i in line_splitted[7].split(";")]
    info_fields = {}
    for i in info:
        info_fields[i[0]] = i[1]
    return info_fields


def read_file(input_vcf: str) -> List:
    with gzip.open(input_vcf, "rt") as f:
        return f.readlines()


def make_clinvar_gene_dict(vcf_lines: List) -> Dict:
    clinvar_dict = {}
    # with open(input_vcf) as f:
    for line in vcf_lines:
        if line[0] != '#':
            info_fields = process_line(line)
            try:
                significance = info_fields["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"].replace(
                    "conflicting_interpretations_of_pathogenicity", "uncertain_significance").split(":")
            except KeyError:
                significance = []
            try:
                genes = [
                    i.split(':')[0] for i in info_fields["ISEQ_CLINVAR_GENE_INFO"].split('|')]
                ann = info_fields["ANN"].split(",")
            except KeyError:
                continue
            else:
                for gene in genes:
                    n_ms = 0
                    n_patho_ms = 0
                    n_benign_ms = 0
                    transcripts = [Transcript(item) for item in ann]
                    good_transcripts = find_good_transcripts(transcripts, gene)
                    if len(good_transcripts) > 0:
                        n_ms = 1
                        pathogenic_test = bool(
                            [i for i in significance if i in pathogenic_categories])
                        benign_test = bool(
                            [i for i in significance if i in other_categories])
                        if pathogenic_test:
                            n_patho_ms = 1
                        elif benign_test:
                            n_benign_ms = 1
                    if gene not in clinvar_dict.keys():
                        clinvar_dict[gene] = [n_ms, n_patho_ms, n_benign_ms]
                    else:
                        clinvar_dict[gene][0] += n_ms
                        clinvar_dict[gene][1] += n_patho_ms
                        clinvar_dict[gene][2] += n_benign_ms
    return clinvar_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Prepares json file needed for acmg annotations, "clinvar-ms-dictionary.json":\n'
                                                 'gene_name => [number_of_all_missense_variants, number_of_pathogenic_missense_variants, number_of_benign_missense_variants]')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str,
                        help="Path to annotated with SnpEff Clinvar vcf.gz file, unzipped input may be also piped from stdin")
    parser.add_argument('-o', '--output', type=str,
                        required=True, help="Path to output json file")
    args = parser.parse_args()
    if args.input:
        input_vcf = read_file(args.input)
        clinvar_gene_ms_dict = make_clinvar_gene_dict(input_vcf)
    else:
        clinvar_gene_ms_dict = make_clinvar_gene_dict(stdin)
    with open(args.output, "w") as output_json:
        t1 = json.dumps(clinvar_gene_ms_dict)
        output_json.write(t1)

# usage: python3 /home/kt/acmg/workflows/resources/acmg_files/clinvar_gene_dict_ms.py -i /data/public/intelliseqngs/workflows/resources/acmg/clinvar/20-02-2020/clinvar.variant-level-annotated-with-snpEff.vcf -o /data/public/intelliseqngs/workflows/resources/acmg/clinvar/20-02-2020/clinvar-missense.json
