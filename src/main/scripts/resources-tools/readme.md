## RESOURCES-TOOLS

This directory contains scripts needed to create files stored under [Resources](./../../../../resources/readme.md)

---

## Table of Contents

**Resources tools**
  * **ACMG** - scripts for generating ACMG resources:  
    * [Prepare ClinVar LOF dictionary (.py)](#prepare-clinvar-lof-dictionary)  
    * [Prepare ClinVar ms dictionary( .py)](#prepare-clinvar-ms-dictionary)  
    * [Prepare ClinVar pathogenic sites list(.py)](#prepare-clinvar-pathogenic-sites-list)  
    * [Prepare ClinVar protein changes dictionary( .py)](#prepare-clinvar-protein-changes-dictionary)  
    * [Prepare UniProt mutagenesis data(.py)](#prepare-uniprot-mutagenesis-data)  
    * [Prepare UniProt functional data( .py)](#prepare-uniprot-functional-data)  
    * [Prepare gnomAD LOF dictionary( .py)](#prepare-gnomad-lof-dictionary)  
  * [Create **1000 Genomes** frequencies annotation VCF (.py)](#create-1000-genomes-frequencies-annotation-vcf)
  * [Create a VCF body from **SIFT4G** chromosome file (.py)](#create-a-vcf-body-from-sift4g-chromosome-file)
  * [Create **ClinVa**r annotation VCF (.py)](#create-clinvar-annotation-vcf)
  * [Create **dbSNP**only rsIDs annotation VCF (.py)](#create-dbsnp-only-rsids-annotation-vcf)
  * [Create **ExAC** frequencies annotation VCF (.py)](#create-exac-frequencies-annotation-vcf)
  * [Create **gnomAD** exomes v2 liftover frequencies annotation VCF (.py)](#create-gnomad-exomes-v2-liftover-annotation-vcf)
  * [Create **gnomAD** genomes v2 liftover frequencies annotation VCF (.py)](#create-gnomad-genomes-v2-liftover-annotation-vcf)
  * [Create **gnomAD** genomes v2 liftover frequencies annotation VCF - exome calling intervals(.py)](#create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vcf)
  * [Create **gnomAD** genomes v3 frequencies annotation VCF (.py)](#create-gnomad-genomes-v3-annotation-vcf)
  * [Create **MITOMAP** diseases annotation VCF (.py)](#create-mitomap-diseases-annotation-vcf)
  * [Create **MITOMAP** frequencies annotation VCF (.py)](#create-mitomap-frequencies-annotation-vcf)
  * **dbSNP for genotyping** - scripts to create VCFs:
    * [Create dbSNP VCFs for genotyping (.py)](#create-dbsnp-vcfs-for-genotyping)
    * [Postprocessing of chromosome wise dbSNP foR genotyping VCFs (.py)](postprocessing-of-chromosome-wise-dbsnp-for-genotyping-vcfs)
  * **ESP6500**- scripts for ESPP6500 GRCh38 frequencies annotation VCFs:
    * [Create ESP6500 frequencies annotation VCF (.py)](#create-esp6500-frequencies-annotation-vcf)
    * [Split ESP6500 frequencies annotation VCF chromosome-wise ( .py)](#split-esp6500-frequencies-annotation-vcf-chromosome-wise)
  * [Filter **HPO** dictionaries by modes of inheritance (.py)](#filter-hpo-dictionaries-by-modes-of-inheritance)
  * **GERP** - scripts for generating BigWig GRCh38 annotation files:
    * [Create chrom.sizes file from GERP maf files (.bash)](#create-chrom.sizes-file-from-gerp-maf-files)
    * [Generate commands to create chromosome wise Wig hg19 files from GERP maf files (.bash)](#generate-commands-to-create-chromosome-wise-wig-hg19-files-from-gerp-maf-files)
    * [Generate commands to create GERP chromosome wise BigWig hg19 files from Wig Files (.bash)](#generate-commands-to-create-gerp-chromosome-wise-bigwig-hg19-files-from-wig-files)
    * [Generate commands to liftover chromosome wise GERP BigWig hg19 files (.bash)](#generate-commands-to-liftover-chromosome-wise-gerp-bigwig-hg19-files)
  * [Generate script that creates chromosome-wise **Wigs and BigWigs** (.bash)](#generate-script-that-creates-wigs-and-bigwigs)


[Return to: scripts ](./../readme.md)

### Prepare ClinVar LOF dictionary

**Author**: Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3  

**Current version**:  [**prepare-clinvar-lof-dictionary.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/prepare-clinvar-lof-dictionary.py@0.0.1/src/main/scripts/resources-tools/prepare-clinvar-lof-dictionary.py)

```
usage: prepare-clinvar-lof-dictionary.py [-h] [-v] [-i INPUT] -o OUTPUT

Prepares json file needed for acmg annotations, "clinvar-lof-dictionary.json":
gene name => [number of all pathogenic variants in clinvar, number of all loss
of function variants, number of pathogenic loss of function variants]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -i INPUT, --input INPUT
                        Path to annotated with SnpEff ClinVar vcf.gz file,
                        unzipped input may be also piped from stdin
  -o OUTPUT, --output OUTPUT
                        Path to output json file
 
```

[Return to the table of contents](#table-of-contents)

---

### Prepare ClinVar ms dictionary

**Author**:   Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**:  [**prepare-clinvar-ms-dictionary.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/prepare-clinvar-ms-dictionary.py@0.0.1/src/main/scripts/resources-tools/prepare-clinvar-ms-dictionary.py)

```
usage: prepare-clinvar-ms-dictionary.py [-h] [-v] [-i INPUT] -o OUTPUT

Prepares json file needed for acmg annotations, "clinvar-ms-dictionary.json":
gene_name => [number_of_all_missense_variants,
number_of_pathogenic_missense_variants, number_of_benign_missense_variants]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -i INPUT, --input INPUT
                        Path to annotated with SnpEff Clinvar vcf.gz file,
                        unzipped input may be also piped from stdin
  -o OUTPUT, --output OUTPUT
                        Path to output json file
```

[Return to the table of contents](#table-of-contents)

---
### Prepare ClinVar pathogenic sites list

**Author**:   Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**:  [**prepare-clinvar-pathogenic-sites-list.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/prepare-clinvar-pathogenic-sites-list.py@0.0.1/src/main/scripts/resources-tools/prepare-clinvar-pathogenic-sites-list.py)

```
usage: prepare-clinvar-pathogenic-sites-list.py [-h] [-v] [-i INPUT] -o OUTPUT

Prepares tab deliminated file needed for acmg annotations, "clinvar-
pathogenic-sites.tab": chr, pos, significance

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -i INPUT, --input INPUT
                        Path to annotated with SnpEff Clinvar vcf.gz file,
                        unzipped input may be also piped from stdin)
  -o OUTPUT, --output OUTPUT
                        Path to output tab file
```

[Return to the table of contents](#table-of-contents)

---
### Prepare ClinVar protein changes dictionary

**Author**:   Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**:  [**prepare-clinvar-protein-changes-dictionary.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/prepare-clinvar-protein-changes-dictionary.py@0.0.1/src/main/scripts/resources-tools/prepare-clinvar-protein-changes-dictionary.py)

```
usage: prepare-clinvar-protein-changes-dictionary.py [-h] [-v] [--input INPUT]
                                                     --output OUTPUT

Prepares json file needed for acmg annotations, "clinvar-protein-changes-
dictionary.json": gene => list of protein level changes;protein level changes
are given as follow: [[aa_ref1, aa_alt1, aa_pos1, transcript_id1,
significance1], [aa_ref2, aa_alt2, aa_pos2, transcript_id2,
significance2],[...],...]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  --input INPUT, -i INPUT
                        Path to annotated with SnpEff Clinvar vcf.gz file,
                        unzipped input can be also piped from stdin
  --output OUTPUT, -o OUTPUT
                        Path to output json file
```  
[Return to the table of contents](#table-of-contents)  
 
--- 

### Prepare UniProt mutagenesis data  

**Author**:   Katarzyna Tomala (https://gitlab.com/kattom)  

**Language**: python3  

**Current version**:  [**prepare-uniprot-mutagenesis-data.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/prepare-uniprot-mutagenesis-data.py@0.0.1/src/main/scripts/resources-tools/prepare-uniprot-mutagenesis-data.py)    

```
usage: prepare-uniprot-mutagenesis-data.py [-h] [-v] -u UNIPROT -e ENSEMBL -o
                                           OUTPUT

Prepares json file needed for acmg ps3 annotation: gene_uniprotID => [[start1,
end1, ref_eq1, alt_seq1, description1], [start2, end2, ref_seq2, alt_seq2,
description2], ...]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -u UNIPROT, --uniprot UNIPROT
                        Path to tab deliminated file with UniProt table file
  -e ENSEMBL, --ensembl ENSEMBL
                        Path to tab deliminated file with Ensembl gene to
                        UniProt ID mapping file
  -o OUTPUT, --output OUTPUT
                        Path to output json file
```
[Return to the table of contents](#table-of-contents)  
   
---
### Prepare UniProt functional data  

**Author**:   Katarzyna Tomala (https://gitlab.com/kattom)  

**Language**: python3  

**Current version**:  [**prepare-uniprot-functional-data.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/prepare-uniprot-functional-data.py@0.0.1/src/main/scripts/resources-tools/prepare-uniprot-functional-data.py)  

```
usage: prepare-uniprot-functional-data.py [-h] [-v] -u UNIPROT -e ENSEMBL -o
                                          OUTPUT

Prepares json file needed for acmg pm1 annotation: gene_uniprotID =>
[[region_start1, region_end1, region_seq1, region_type1, additional_info1],
[region_start2, region_end2, region_seq2, region_type2, additonal_info2]...]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -u UNIPROT, --uniprot UNIPROT
                        Path to tab deliminated file with UniProt table file
  -e ENSEMBL, --ensembl ENSEMBL
                        Path to tab deliminated file with Ensembl gene to
                        UniProt ID mapping file
  -o OUTPUT, --output OUTPUT
                        Path to output json file
```  
[Return to the table of contents](#table-of-contents)  
   
---  
### Prepare gnomAD LOF dictionary     

**Author**:   Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**:  [**prepare-gnomad-lof-dictionary.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/prepare-gnomad-lof-dictionary.py@0.0.1/src/main/scripts/resources-tools/prepare-gnomad-lof-dictionary.py)

```
usage: prepare-gnomad-lof-dictionary.py [-h] [-v] -i INPUT -o OUTPUT

Prepares json file needed for acmg pvs1 annotation, "gnomad-lof-
dictionary.json": gene name => [lof_oe, lower CI, upper CI]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -i INPUT, --input INPUT
                        Path to modified gnomad lof metrics file
  -o OUTPUT, --output OUTPUT
                        Path to output json file
```
[Return to the table of contents](#table-of-contents)  
   
---  

### Create 1000 Genomes frequencies annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-kg-frequencies-annotation-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-kg-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-kg-frequencies-annotation-vcf.py)

```
usage: create-kg-frequencies-annotation-vcf.py [-h] [-v] OUTPUT_DIR

    The script modifies original 1000 Genomes database with SNV and INDEL calls (v2a), aligned directly to GRCh38,
    downloaded from [1000 Genomes FTP]:
    http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20181203_biallelic_SNV/

    The script creates chromosome-wise (chr1, ... chr22, chrX, chrY-and-the-rest) bgzipped VCFs with the following
    INFO fields:
        + ISEQ_KG_AF (original field: AF)
        + ISEQ_KG_EAS_AF (original field: EAS_AF)
        + ISEQ_KG_EUR_AF (original field: EUR_AF)
        + ISEQ_KG_AFR_AF (original field: AFR_AF)
        + ISEQ_KG_AMR_AF (original field: AMR_AF)
        + ISEQ_KG_SAS_AF (original field: SAS_AF)

positional arguments:
  OUTPUT_DIR     path to output directory

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```

[Return to the table of contents](#table-of-contents)

---

### Create a VCF body from SIFT4G chromosome file

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-a-vcf-body-from-sift-chromosome-file.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-a-vcf-body-from-sift-chromosome-file.py@0.0.1/src/main/scripts/resources-tools/create-a-vcf-body-from-sift-chromosome-file.py)

```
usage: create-a-vcf-body-from-sift-chromosome-file.py [-h] [-v] CHR

This script is used database consisting of bgzipped VCF files containing SIFT4G scores for GRCh38 reference genome.
It creates a body of VCF file from SIFT4G file for a specified chromosome.

anakin:  /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/scrores/sift4g

positional arguments:
  CHR            name of chromosome

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```

[Return to the table of contents](#table-of-contents)

---

### Create ClinVar annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Historical versions**:
+ [create-clinvar-annotation-vcf.py  1.0.0](https://gitlab.com/intelliseq/workflows/-/blon/create-clinvar-annotation-vcf.py@1.0.0/src/main/scripts/resources-tools/create-clinvar-annotation-vcf.py)
+ [create-clinvar-annotation-vcf.py  0.0.1](https://gitlab.com/intelliseq/workflows/-/blon/create-clinvar-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-clinvar-annotation-vcf.py)

**Current version**:  [**create-clinvar-annotation-vcf.py 2.0.0**](https://gitlab.com/intelliseq/workflows/-/blob/create-clinvar-annotation-vcf.py@2.0.0/src/main/scripts/resources-tools/create-clinvar-annotation-vcf.py)

```
usage: create-clinvar-annotation-vcf-new.py [-h] [-v]
                                            SUBMISSION_SUMMARY_GZ_FILE
                                            VARIATION_ALLELE_GZ_FILE
                                            CLINVAR_VCF_GZ_FILE

Creates VCF database containing ClinVar annotations. Required files:

  * clinvar.vcf.gz
  * variation_allele.txt.gz
  * submission_summary.txt.gz

are released approximately monthly and can be downloaded from ClinVar FTP site (ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/).

NOTES:

1. Database clinvar.vcf.gz should have chromosome naming convention changed to contain chromosomes: chr1, chr2, ..., chr22, chrX, chrY, chrM
2. Database clinvar.vcf.gz should have multiallelic sites split and be left-indel normalized with the use of the command:

    bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any clinvar.vcf.gz | bgzip > clinvar.split.normalized.vcf.gz

3. The clinvar.vcf.gz containes troublesome ASCII characters that are best to be removed. To check which troublesome characters are present the following command can be used:

    zcat clinvar.vcf.gz | grep --color='auto' -P -o "[\x80-\xFF]" | sort | uniq

Troublesome characters can be replaced with the use of sed command, for example:

    zcat clinvar.vcf.gz | sed 's/ß/B/g' | sed 's/ã/a/g' | sed 's/ä/a/g' | sed 's/é/e/g' | sed 's/è/e/g' | sed 's/ê/e/g' | sed 's/ë/e/g' | sed 's/ô/o/g' | sed 's/ö/o/g' | sed 's/ú/u/g' | sed 's/ü/u/g' | bgzip > tmp.vcf.gz
    mv tmp.vcf.gz clinvar.vcf.gz

positional arguments:
  SUBMISSION_SUMMARY_GZ_FILE
                        path to submission_summary.txt.gz
  VARIATION_ALLELE_GZ_FILE
                        path to variation_allele.txt.gz
  CLINVAR_VCF_GZ_FILE   path to clinvar.vcf.gz

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
```
[Return to the table of contents](#table-of-contents)

---

### Create dbSNP only rsIDs annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-dbsnp-only-rs-ids-annotation-vcf.py0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-dbsnp-only-rs-ids-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-dbsnp-only-rs-ids-annotation-vcf.py)

```
usage: create-dbsnp-only-rs-ids-annotation-vcf.py [-h] [-v]
                                                  VCF_NAME ASSEMBLY_REPORT
                                                  OUTPUT_DIR

This script create bgzipped chromosome-wise VCFs files containing rsID from dbSNP.
The annotation VCFs are created based on dbSNP build 153 database downloaded from
[NCBI ftp site](ftp://ftp.ncbi.nih.gov/snp/) and assembly report for GRCh38.p12
dwonloaded from [NCBI ftp site](ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.38_GRCh38.p12/)

positional arguments:
  VCF_NAME         output vcf names: chr*.VCF_NAME.vcf.gz
  ASSEMBLY_REPORT  path to assembly report
  OUTPUT_DIR       path to output directory

optional arguments:
  -h, --help       show this help message and exit
  -v, --version    show program's version number and exit

```
[Return to the table of contents](#table-of-contents)

---

### Create ExAC frequencies annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-exac-frequencies-annotation-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-exac-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-exac-frequencies-annotation-vcf.py)

```
usage: create-exac-frequencies-annotation-vcf.py [-h] [-v] OUTPUT_DIR

The script created chromosome-wise bgzipped ExAC frequencies annotations VCFs,
based on ExAC database in VCF format (version 1.0 lifted over to GRCh38) from
[Broad Institute Google Cloud](https://console.cloud.google.com/storage/browser/gnomad-public/)

positional arguments:
  OUTPUT_DIR     path to output directory

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

---

### Create gnomAD exomes v2 liftover annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-gnomad-exomes-v2-liftover-annotation-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-exomes-v2-liftover-annotation-vcf.py)

```
usage: create-gnomad-exomes-v2-liftover-annotation-vcf.py [-h] [-v] OUTPUT_DIR

This script creates chromosome-wise (chr1, ..., chr22, chrX, chrY-and-the-rest),
bgzippef VCFs, containing frequency annotation based on gnomAD genomes v2 liftover
frequencies database for exomes.

positional arguments:
  OUTPUT_DIR     path to output directory

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

---

### Create gnomAD genomes v2 liftover annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-gnomad-genomes-v2-liftover-annotation-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-genomes-v2-liftover-annotation-vcf.py)

```
usage: create-gnomad-genomes-v2-liftover-annotation-vcf.py [-h] [-v]
                                                           OUTPUT_DIR

This script creates chromosome-wise (chr1, ..., chr22, chrX, chrY-and-the-rest),
bgzippef VCFs, containing frequency annotation based on gnomAD genomes v2 liftover
frequencies database for genomess.

positional arguments:
  OUTPUT_DIR     path to output directory

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

---

### Create gnomAD genomes v2 liftover exome calling intervals annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vc.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vc.py)

```
usage: create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vcf.py
       [-h] [-v] OUTPUT_DIR

This script creates chromosome-wise (chr1, ..., chr22, chrX, chrY-and-the-rest),
bgzippef VCFs, containing frequency annotation based on gnomAD genomes v2 liftover
frequencies database for genomes, restricted to exome calling intervals.

positional arguments:
  OUTPUT_DIR     path to output directory

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```

[Return to the table of contents](#table-of-contents)

---

### Create gnomAD genomes v3 annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-gnomad-genomes-v3-annotation-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-genomes-v3-annotation-vcf.py)

```
usage: create-gnomad-genomes-v3-annotation-vcf.py [-h] [-v] OUTPUT_DIR

This script creates chromosome-wise (chr1, ..., chr22, chrX, chrY-and-the-rest),
bgzippef VCFs, containing frequency annotation based on gnomAD genomes v3 frequencies
database.

positional arguments:
  OUTPUT_DIR     path to output directory

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

---

### Create MITOMAP diseases annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Historical versions**:
+ [create-mitomap-diseases-annotation-vcf.py  0.0.1](https://gitlab.com/intelliseq/workflows/-/raw/create-mitomap-diseases-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-mitomap-diseases-annotation-vcf.py)

**Current version**:  [**create-mitomap-diseases-annotation-vcf.py  1.0.0**](https://gitlab.com/intelliseq/workflows/-/raw/create-mitomap-diseases-annotation-vcf.py@1.0.0/src/main/scripts/resources-tools/create-mitomap-diseases-annotation-vcf.py)

```
usage: create-mitomap-diseases-annotation-vcf.py [-h] [-v]
                                                 MITOMAP_DISEASES_VCF

    The script modifies original MITOMAP diseases.vcf file downloaded from
    MITOMAP  Resources site (https://mitomap.org/foswiki/bin/view/MITOMAP/Resources)

    The script performs the following actions:
        - formats contents of Disease field:
             + changes its name to MITOMAP_DISEASE
             + replaces disease abbreviations with disease names
             + replaces +, &, / with ,
             + replaces whitspaces with _
             + changes the format od field to: disease : (...) : disease
             + changes the number of values to: one per allele (Number=A)
        - formats contents of DiseaseStatus field:
            + changes its name to MITOMAP_DISEASE_STATUS
            + replaces whitspaces with _
            + changes the number of values to: one per allele (Number=A)
        - removes remaining INFO fields
        - changes chromosome naming convention from MT to chrM.
        - removes information about reference allels (as it is not well handled with VCF format and there are only
          several reference allels in the whole MITOMAP database)


positional arguments:
  MITOMAP_DISEASES_VCF  path to MITOMAP database diseases.vcf. File must have
                        multiallelic sites split and must be left-indel
                        normalized

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit

```
[Return to the table of contents](#table-of-contents)

---

### Create MITOMAP frequencies annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-mitomap-frequencies-annotation-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-mitomap-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-mitomap-frequencies-annotation-vcf.py)

```
usage: create-mitomap-frequencies-annotation-vcf.py [-h] [-v]

    The script modifies original MITOMAP polymorphisms.vcf file downloaded from
    MITOMAP Resources site (https://mitomap.org/foswiki/bin/view/MITOMAP/Resources).

    The script performs the following actions:
        - converts AF from percentage to numbers
        - changes AC and AF fields names to MITOMAP_AC and MITOMAP_AF
        - changes chromosome naming convention from MT to chrM
        - modifies MT contig header line

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```

[Return to the table of contents](#table-of-contents)

---

###  Filter HPO dictionaries by modes of inheritance

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**filter-hpo-dictionaries-by-modes-of-inheritance.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/filter-hpo-dictionaries-by-modes-of-inheritance.pyy@0.0.1/src/main/scripts/resources-tools/filter-hpo-dictionaries-by-modes-of-inheritance.py)


```
usage: filter-hpo-dictionaries-by-modes-of-inheritance.py [-h] [-o] [-v]
                                                          LIST_OF_MODES_OF_INHERITANCE

This script is used to create HPO-based dictionaries for annotating (HPO - Human Phenotypes Ontology):
anakin:  /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/human-phenotype-ontology

It removes (default) or keeps mode of inheritance related HPO terms from genes-phenotype dictionaries.

positional arguments:
  LIST_OF_MODES_OF_INHERITANCE
                        Path to list-of-modes-of-inheritance.txt

optional arguments:
  -h, --help            show this help message and exit
  -o                    Keep only modes of inheritance
  -v, --version         show program's version number and exit
```

[Return to the table of contents](#table-of-contents)

---

### Generate script that creates chromosome wise Wigs and BigWigs

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: bash

**Current version**:  [**generate-script-that-creates-chromosome-wise-wigs-and-bigwigs.bash  0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/generate-script-that-creates-chromosome-wise-wigs-and-bigwigs.bash@0.0.1/src/main/scripts/generate-script-that-creates-chromosome-wise-wigs-and-bigwigs.bash)

**Required**:
  + bigWigToWig - linux alias
  + wigToBigWig - linux alias

**Issues**: This script has no --version/-v and --help/-h flags.

```
usage: bash generate-script-that-creates-chromosome-wise-wigs-and-bigwigs.bash NAME CHROM_SIZES_FILE BIG_WIG_FILE OUTPUT_DIR

This script generates a script <NAME>-create-chromosome-wise-wigs-and-bigwigs.bash that when run, creates
chromosome-wise bigwigs for an input BIG_WIG_FILE. Output chromosomes are: chr1, chr2, ..., chr22, chrX, chrY-and-the-rest.

To run properly, script requires the following linux aliases:
  + bigWigToWig
  + wigToBigWig
Those aliases should point to bigWigToWig and wigToBigWig programs from jvarkit (https://github.com/lindenb/jvarkit)

positional arguments:
  NAME                 prefix of outputs
  CHROM_SIZES_FILE     tab-delimited file with names and lenght of contigs
                       in given reference genome (<ref_genome>.chrom.sizes)
  BIG_WIG_FILE         input BigWig file
  OUTPUT_DIR           output directoory

```

[Return to the table of contents](#table-of-contents)

---

### Create dbSNP VCFs for genotyping

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-dbsnp-vcfs-for-genotyping.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-dbsnp-vcfs-for-genotyping.py@0.0.1/src/main/scripts/resources-tools/.py)

```
```

[Return to the table of contents](#table-of-contents)

---

### Postprocessing of chromosome wise dbSNP foR genotyping VCFs

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**postprocessing-of-chromosome-wise-dbsnp-for-genotyping-vcfs.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/.py@0.0.1/src/main/scripts/resources-tools/postprocessing-of-chromosome-wise-dbsnp-for-genotyping-vcfs.py)

```
```

[Return to the table of contents](#table-of-contents)

---

### Create chrom.sizes file from GERP maf files

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: bash

**Current version**:  [**create-chrom-sizes-file-from-gerp-maf-files.bash  0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-chrom-sizes-file-from-gerp-maf-files.BASH@0.0.1/src/main/scripts/create-chrom-sizes-file-from-gerp-maf-files.bash)

**Issues**: This script has no --version/-v and --help/-h flags.

```
usage: bash create-chrom-sizes-file-from-gerp.maf-files.bash PATH_TO_SCORES_DIRECTORY

This script generates a chrom.sizes file using GERP hg19 chr*.maf.rates files

positional arguments:
  PATH_TO_SCORES_DIRECTORY         path to direcory containing GERP hg19 chr*.maf.rates files
```

[Return to the table of contents](#table-of-contents)

---


### Generate commands to create chromosome wise Wig hg19 files from GERP maf files

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: bash

**Current version**:  [**generate-commands-to-create-chromosome-wise-wig-hg19-files-from-gerp-maf-files.bash  0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/generate-commands-to-create-chromosome-wise-wig-hg19-files-from-gerp-maf-files.bash@0.0.1/src/main/scripts/generate-commands-to-create-chromosome-wise-wig-hg19-files-from-gerp-maf-files.bash)

**Issues**: This script has no --version/-v and --help/-h flags.

```
usage: bash generate-commands-to-create-chromosome-wise-Wig-hg19-files-from-gerp-maf-files.bash PATH_TO_SCORES_DIRECTORY NAME

This script generates commands to create a directory <PATH_TO_SCORES_DIRECTORY>/<NAME>-hg19-wigs/ and generates wig files from GERP hg19 <PATH_TO_SCORES_DIRECTORY>/chr*.maf.rates files.

positional arguments:
  PATH_TO_SCORES_DIRECTORY         path to direcory containing GERP hg19 chr*.maf.rates files
  NAME				   prefix name to resulting chromosome-wise Wig files:
                                   <PATH_TO_SCORES_DIRECTORY>/<NAME>-hg19-wigs/chr*.<NAME>.hg19.wig
```

[Return to the table of contents](#table-of-contents)

---

### Generate commands to create GERP chromosome wise BigWig hg19 files from Wig Files

**Language**: bash

**Current version**:  [**generate-commands-to-create-gerp-chromosome-wise-bigwig-hg19-files-from-wig-files.bash  0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/generate-commands-to-create-gerp-chromosome-wise-bigwig-hg19-files-from-wig-files.bash@0.0.1/src/main/scripts/generate-commands-to-create-gerp-chromosome-wise-bigwig-hg19-files-from-wig-files.bash)

**Required**:
  + bigWigToWig - linux alias
  +
**Issues**: This script has no --version/-v and --help/-h flags.

```
usage: bash generate-commands-to-create-gerp-chromosome-wise-bigwig-hg19-files-from-wig-files.bash PATH_TO_SCORES_DIRECTORY NAME CHROM_SIZES_FILE

This script generates commands to create a directory <PATH_TO_SCORES_DIRECTORY>/<NAME>-hg19-bigwigs/ and generates BigWig files from GERP hg19 <PATH_TO_SCORES_DIRECTORY>/<NAME-hg19-wigs>/chr*.<NAME>.hg19.wig wigs.

To run properly, script requires the following linux alias:
  + bigWigToWig
This alias should point to bigWigToWig program from jvarkit (https://github.com/lindenb/jvarkit)

positional arguments:
  PATH_TO_SCORES_DIRECTORY     path to direcory containing GERP hg19 chr*.maf.rates files
  NAME			       prefix name
  CHROM_SIZES_FILE             tab-delimited file with names and lenght of contigs
                               in given reference genome (<ref_genome>.chrom.sizes)
```

[Return to the table of contents](#table-of-contents)

---

### Generate commands to liftover chromosome wise GERP BigWig hg19 files

**Language**: bash

**Current version**:  [**generate-commands-to-liftover-chromosome-wise-gerp-bigwig-hg19-files.bash  0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/generate-commands-to-liftover-chromosome-wise-gerp-bigwig-hg19-files.bash@0.0.1/src/main/scripts/generate-commands-to-liftover-chromosome-wise-gerp-bigwig-hg19-files.bash)

**Required**:
  + CrossMap.py - linux alias

**Issues**: This script has no --version/-v and --help/-h flags.

```
usage: bash generate-commands-to-liftover-chromosome-wise-gerp-bigwig-hg19-files PATH_TO_SCORES_DIRECTORY NAME CHAIN_FILE

This script creates a directory <PATH_TO_SCORES_DIRECTORY>/<NAME>-hg38-raw-liftovered-bigwigs-and-bgrs and populates it with chromosome-wise BigWigs liftovered from <PATH_TO_SCORES_DIRECTORY>/<NAME-hg19-wigs>/chr*.<NAME>.hg19.bw BigWigs.

positional arguments:
  PATH_TO_SCORES_DIRECTORY     path to direcory containing GERP hg19 chr*.maf.rates files
  NAME			       prefix name
  CHAIN_FILE                   chainf file needed to perform CrossMap.py liftover (hg19ToHg38.over.chain.gz)
```

[Return to the table of contents](#table-of-contents)

---

###  Create ESP6500 frequencies annotation VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-esp6500-frequencies-annotation-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-esp6500-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-esp6500-frequencies-annotation-vcf.py)

```
usage: create-esp6500-frequencies-annotation-vcf.py [-h] [-v]

This script is used to create hg19 ESP6500 annotation file based on
ESP6500SI-V2-SSA137.GRCh38-liftover.chr<*>.snps_indels.vcf downloaded from
[NHLBI Exome Sequencing Project (ESP) Download page](http://evs.gs.washington.edu/EVS/).

This script calculates AF fields: ISEQ_ESP6500_EA_AF, ISEQ_ESP6500_AA_AF, ISEQ_ESP6500_AN,
ISEQ_ESP6500_AF using AC and AN field.

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```

[Return to the table of contents](#table-of-contents)

---

###  Split ESP6500 frequencies annotation VCF chromosome-wise

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**split-esp6500-frequencies-annotation-vcf-chromosome-wise.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/split-esp6500-frequencies-annotation-vcf-chromosome-wise.py@0.0.1/src/main/scripts/resources-tools/split-esp6500-frequencies-annotation-vcf-chromosome-wise.py)

```
usage: split-esp6500-frequencies-annotation-vcf-chromosome-wise.py
       [-h] OUTPUT_DIR

This script is used to split chromosome-wise hg19 ESP6500 annotation VCF lifted over to
GRCh38.

positional arguments:
  OUTPUT_DIR  path to output directory

optional arguments:
  -h, --help  show this help message and exit

```

[Return to the table of contents](#table-of-contents)

---
