#!/bin/bash

set -e -o pipefail

## ClinVar
# get clinvar vcf
wget https://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz
tabix -p vcf clinvar.vcf.gz

# change contig names, normalize indels
tabix -l clinvar.vcf.gz > old.contigs.tsv
tabix -l clinvar.vcf.gz | sed 's/MT/M/' | sed 's/^/chr/' >new.contigs.tsv
paste old.contigs.tsv new.contigs.tsv | grep -v NW > contigs.tsv
zcat clinvar.vcf.gz | grep -v -E '5226739' | bgzip > clinvar.fixed.vcf.gz
tabix -p vcf clinvar.fixed.vcf.gz
bcftools annotate --rename-chrs contigs.tsv clinvar.fixed.vcf.gz \
 | bcftools view -t ^NW_009646201.1 \
 | bcftools norm -m -any -f /resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa -o tmp1.vcf.gz -O z

tabix -p vcf tmp1.vcf.gz

rm *contigs*

# get cnv variants
bcftools filter -i 'STRLEN(REF)> 50 & INFO/CLNVC="Deletion,copy_number_loss"' tmp1.vcf.gz -o clinvar.loss.vcf.gz -O z
bcftools filter -i 'STRLEN(ALT)> 50 & INFO/CLNVC="Duplication,copy_number_gain"' tmp1.vcf.gz -o clinvar.gain.vcf.gz -O z

tabix -p vcf clinvar.loss.vcf.gz
tabix -p vcf clinvar.gain.vcf.gz

# prepare BED files

bcftools query -f "%CHROM\n"  clinvar.loss.vcf.gz > clinvar.chrom.loss.txt
bcftools query -f '%CHROM\t%INFO/CLNHGVS\n' clinvar.loss.vcf.gz | cut -f 3 -d '.' | sed 's/del//' | sed 's/_/\t/' \
 > clinvar.pos.loss.txt #;
bcftools query -f \
"%ID;%INFO/ALLELEID;%INFO/CLNDN;%INFO/CLNDNINCL;%INFO/CLNDISDB;%INFO/CLNDISDBINCL;%INFO/CLNHGVS;%INFO/CLNREVSTAT;%INFO/CLNSIG;%INFO/CLNSIGCONF;%INFO/CLNSIGINCL;%INFO/CLNVC;%INFO/CLNVCSO;%INFO/CLNVI;%INFO/DBVARID;%INFO/GENEINFO;%INFO/MC;%INFO/ORIGIN;%INFO/RS;%INFO/SSR;%INFO/AF_ESP;%INFO/AF_EXAC;%INFO/AF_TGP\n" clinvar.loss.vcf.gz \
>  clinvar.info.loss.txt
paste  clinvar.chrom.loss.txt clinvar.pos.loss.txt clinvar.info.loss.txt | sort -k1,1V -k2,2n -k3,3n | bgzip > clinvar.loss.bed.gz
rm clinvar.chrom.loss.txt clinvar.pos.loss.txt clinvar.info.loss.txt

bcftools query -f "%CHROM\n"  clinvar.gain.vcf.gz > clinvar.chrom.gain.txt
bcftools query -f '%CHROM\t%INFO/CLNHGVS\n' clinvar.gain.vcf.gz | cut -f 3 -d '.' | sed 's/dup//' | sed 's/_/\t/' \
> clinvar.pos.gain.txt
bcftools query -f \
"%ID;%INFO/ALLELEID;%INFO/CLNDN;%INFO/CLNDNINCL;%INFO/CLNDISDB;%INFO/CLNDISDBINCL;%INFO/CLNHGVS;%INFO/CLNREVSTAT;%INFO/CLNSIG;%INFO/CLNSIGCONF;%INFO/CLNSIGINCL;%INFO/CLNVC;%INFO/CLNVCSO;%INFO/CLNVI;%INFO/DBVARID;%INFO/GENEINFO;%INFO/MC;%INFO/ORIGIN;%INFO/RS;%INFO/SSR;%INFO/AF_ESP;%INFO/AF_EXAC;%INFO/AF_TGP\n" clinvar.gain.vcf.gz \
 >  clinvar.info.gain.txt
paste  clinvar.chrom.gain.txt clinvar.pos.gain.txt clinvar.info.gain.txt | sort -k1,1V -k2,2n -k3,3n | bgzip > clinvar.gain.bed.gz
rm clinvar.chrom.gain.txt clinvar.pos.gain.txt clinvar.info.gain.txt

tabix -p bed clinvar.loss.bed.gz
tabix -p bed clinvar.gain.bed.gz

if [ $1 = all ]; then
  ## Decipher population data
  wget https://www.deciphergenomics.org/files/downloads/population_cnv_grch38.txt.gz
  zcat population_cnv_grch38.txt.gz | tail -n +2 \
   | awk -F '\t' 'BEGIN { OFS = "\t" }; {if ($5 != 0) {print $2,$3,$4,$1";"$5";"$6";"$7";"$15";"$16 }}' \
   | sed 's/^/chr/' | sed 's/ /_/g' \
   | sort -k1,1V -k2,2n -k3,3n | bgzip > population.loss.bed.gz
  zcat population_cnv_grch38.txt.gz | tail -n +2 \
   | awk -F '\t' 'BEGIN { OFS = "\t" }; {if ($8 != 0) {print $2,$3,$4,$1";"$8";"$9";"$10";"$15";"$16 }}' \
   | sed 's/^/chr/' | sed 's/ /_/g' \
   | sort -k1,1V -k2,2n -k3,3n | bgzip > population.gain.bed.gz

  tabix -p bed population.loss.bed.gz
  tabix -p bed population.gain.bed.gz


  ## Decipher development diseases genes
  wget https://www.ebi.ac.uk/gene2phenotype/downloads/DDG2P.csv.gz
  zcat DDG2P.csv.gz | sed 's/, / /g' | sed 's/1,25-/1*25-/'| sed 's/1,6 BIS/1*6 BIS/' | sed 's/\"//g' | sed 's/;/|/g' > DDG2P-reformatted.csv
  wget -O result.txt 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "chromosome_name" value = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,MT,X,Y"/><Attribute name = "chromosome_name" /><Attribute name = "start_position" /><Attribute name = "end_position" /><Attribute name = "external_gene_name" /><Attribute name = "hgnc_id" /></Dataset></Query>'

  while read f
  do
    gene=$(echo $f | cut -f4 -d ' ')
    id=$(echo $f | cut -f5 -d ' ' | sed 's/HGNC://' )
    coor=$(echo $f | awk 'BEGIN { OFS = "\t" }; {print $1,$2,$3}')
    awk -F "," -v gene=$gene -v id=$id '{if ($1 == gene && $13 == id){print $0}}' DDG2P-reformatted.csv | sed 's/,/;/g' > tmp.gene
    n=$( cat tmp.gene | wc -l)
    i=1
    while [ $i -le $n ];do
      printf "$coor\n" >> tmp.coor
      (( i++ ))
    done
    if [ -f tmp.coor ];then

      paste tmp.coor tmp.gene | sed 's/^/chr/' |  awk '$4 !="" ' >> tmp.ddg2p.genes.bed
      rm tmp.coor
    fi
    rm tmp.gene
  done<result.txt


  for i in {1..22} X Y MT
  do
    grep -w "^chr$i" tmp.ddg2p.genes.bed | sort -k2,2n -k3,3n | sed 's/chrMT/chrM/' | bgzip >> ddg2p.genes.bed.gz
  done

  tabix -p bed ddg2p.genes.bed.gz


  ## Prepare ddg headers
  printf "chr\tgene start\tgene end" >> header.ddg1.txt
  head -1 DDG2P-reformatted.csv | sed 's/,/\t/g' > header.ddg2.txt
  printf "overlap (bp)\toverlap (percent of the variant)\toverlap (percent of the DDD gene)\n" > header.ddg3.txt
  paste header.ddg1.txt header.ddg2.txt  > header.ddg2p.txt

  rm header.ddg1.txt header.ddg2.txt header.ddg3.txt
  rm tmp.ddg2p.genes.bed DDG2P.csv.gz
  rm DDG2P-reformatted.csv population_cnv_grch38.txt.gz result.txt
fi

## Prepare clinvar headers
bcftools view -h tmp1.vcf.gz \
 | grep '##ID=\|##INFO'  | sed 's/INFO=<ID=//' \
 | sed 's/=<//' | cut -f1,4 -d ',' | sed 's/Description=/: /' \
 | sed 's/,:/:/' | sed 's/>//' | sed 's/^#//' | sed 's/\"//g' > header.clinvar1.txt
printf "chr\tClinVar variant start\tClinVar variant end\tID\tALLELEID\tCLNDN\tCLNDNINCL\tCLNDISDB\tCLNDISDBINCL\tCLNHGVS\tCLNREVSTAT\tCLNSIG\tCLNSIGCONF\tCLNSIGINCL\tCLNVC\tCLNVCSO\tCLNVI\tDBVARID\tGENEINFO\tMC\tORIGIN\tRS\tSSR\tAF_ESP\tAF_EXAC\tAF_TGP\toverlap (bp)\toverlap (percent of the variant)\toverlap (percent of the ClinVar variant)\n" > header.clinvar2.txt




rm tmp1.vcf.gz* clinvar.loss.vcf.gz* clinvar.gain.vcf.gz*

