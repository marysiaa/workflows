#!/usr/bin/env python3
import pysam
import argparse

__version__ = '0.0.1'

AUTHOR = "gitlab.com/kattom"


def get_info_field(self, field_name: str):
    return self.info[field_name]


def get_position_and_alleles(line):
    chrom = line.contig
    pos = int(line.pos)
    ref = line.ref
    alt = line.alts[0]
    return chrom, pos, ref, alt


def main(vcf_name, vcf_writer_name):
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w', header=header_vcf)

    csqs = []
    vts = []
    for record in vcf_reader.fetch():
        last_chrom, last_position, last_ref, last_alt = get_position_and_alleles(record)
        csq = get_info_field(record, "CSQ")
        vt = get_info_field(record, "VT")
        csqs.extend(csq)
        vts.append(vt)
        last_record = record
        break

    for record in vcf_reader.fetch():
        chrom, pos, ref, alt = get_position_and_alleles(record)
        if chrom == last_chrom and pos == last_position and ref == last_ref and alt == last_alt:
            if get_info_field(record, "CSQ") not in csqs:
                csqs.extend(get_info_field(record, "CSQ"))
            if get_info_field(record, "VT") not in vts:
                vts.append(get_info_field(record, "VT"))
        else:
            last_record.info["CSQ"] = csqs
            last_record.info["VT"] = ":".join(vts)
            # print(csqs)
            vcf_writer.write(last_record)
            last_record = record
            last_chrom = chrom
            last_position = pos
            last_ref = ref
            last_alt = alt
            csqs = []
            csqs.extend(get_info_field(record, "CSQ"))
            vts = []
            vts.append(get_info_field(record, "VT"))

    last_record.info["CSQ"] = csqs
    last_record.info["VT"] = ":".join(vts)
    # print(csqs)
    vcf_writer.write(last_record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merges CIViC annotations for the same variants (VT, CSQ)')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
