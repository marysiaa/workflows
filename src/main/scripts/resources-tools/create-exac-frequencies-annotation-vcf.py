#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
from Bio import bgzf
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
The script created chromosome-wise bgzipped ExAC frequencies annotations VCFs,
based on ExAC database in VCF format (version 1.0 lifted over to GRCh38) from
[Broad Institute Google Cloud](https://console.cloud.google.com/storage/browser/gnomad-public/)
""")

parser.add_argument('OUTPUT_DIR', type=str, help="path to output directory")
parser.add_argument('-v', '--version', action='version', version=__version__)
args = parser.parse_args()
output_directory = args.OUTPUT_DIR

header = []
header.append("##fileformat=VCFv4.3")
header.append("##FILTER=<ID=VQSRTrancheINDEL95.00to96.00,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod: 0.9503 <= x < 1.2168\">")
header.append("##FILTER=<ID=VQSRTrancheINDEL96.00to97.00,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod: 0.7622 <= x < 0.9503\">")
header.append("##FILTER=<ID=VQSRTrancheINDEL97.00to99.00,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod: 0.0426 <= x < 0.7622\">")
header.append("##FILTER=<ID=VQSRTrancheINDEL99.00to99.50,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod: -0.8363 <= x < 0.0426\">")
header.append("##FILTER=<ID=VQSRTrancheINDEL99.50to99.90,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod: -8.5421 <= x < -0.8363\">")
header.append("##FILTER=<ID=VQSRTrancheINDEL99.90to99.95,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod: -18.4482 <= x < -8.5421\">")
header.append("##FILTER=<ID=VQSRTrancheINDEL99.95to100.00+,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod < -37254.4742\">")
header.append("##FILTER=<ID=VQSRTrancheINDEL99.95to100.00,Description=\"Truth sensitivity tranche level for INDEL model at VQS Lod: -37254.4742 <= x < -18.4482\">")
header.append("##FILTER=<ID=VQSRTrancheSNP99.60to99.80,Description=\"Truth sensitivity tranche level for SNP model at VQS Lod: -4.9627 <= x < -1.8251\">")
header.append("##FILTER=<ID=VQSRTrancheSNP99.80to99.90,Description=\"Truth sensitivity tranche level for SNP model at VQS Lod: -31.4709 <= x < -4.9627\">")
header.append("##FILTER=<ID=VQSRTrancheSNP99.90to99.95,Description=\"Truth sensitivity tranche level for SNP model at VQS Lod: -170.3725 <= x < -31.4709\">")
header.append("##FILTER=<ID=VQSRTrancheSNP99.95to100.00+,Description=\"Truth sensitivity tranche level for SNP model at VQS Lod < -39645.8352\">")
header.append("##FILTER=<ID=VQSRTrancheSNP99.95to100.00,Description=\"Truth sensitivity tranche level for SNP model at VQS Lod: -39645.8352 <= x < -170.3725\">")
header.append("##FILTER=<ID=AC_Adj0_Filter,Description=\"No sample passed the adjusted threshold (GQ >= 20 and DP >= 10)\">")
header.append("##INFO=<ID=ISEQ_EXAC_AC,Number=A,Type=Float,Description=\"Alternate allele count for samples (AC_Adj) - ExAC\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_AN,Number=1,Type=Integer,Description=\"Total number of alleles in samples (AN_Adj) - ExAC\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples (AC_Adj / AN_Adj) - ExAC\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_POPMAX,Number=A,Type=String,Description=\"Population with maximum allele frequency - ExAC\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_POPMAX_AF,Number=A,Type=Float,Description=\"Maximum allele frequency across populations - ExAC\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_AFR_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - ExAC, African / African American population (ARF)\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_AMR_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - ExAC, Latino population (AMR)\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_EAS_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - ExAC, East Asian population (EAS)\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_FIN_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - ExAC, Finnish population (FIN)\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_NFE_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - ExAC, Non-Finnish European population (EUR)\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_OTH_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - ExAC, Other population (OTH)\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_SAS_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - ExAC, South Asian population (SAS)\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_EXAC_FILTER_STATUS,Number=A,Type=String,Description=\"Filter status. Format: filter_status : (...) : filter_status. Possible values: PASS (All filters passed), InbreedingCoeff_Filter (InbreedingCoeff <= -0.8), LowQual (Low quality), NewCut_Filter (VQSLOD > -2.632 && InbreedingCoeff >-0.8), VQSRTrancheINDEL95.00to96.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.9503 <= x < 1.2168), VQSRTrancheINDEL96.00to97.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.7622 <= x < 0.9503), VQSRTrancheINDEL97.00to99.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.0426 <= x < 0.7622), VQSRTrancheINDEL99.00to99.50 (Truth sensitivity tranche level for INDEL model at VQS Lod: -0.8363 <= x < 0.0426), VQSRTrancheINDEL99.50to99.90 (Truth sensitivity tranche level for INDEL model at VQS Lod: -8.5421 <= x < -0.8363), VQSRTrancheINDEL99.90to99.95 (Truth sensitivity tranche level for INDEL model at VQS Lod: -18.4482 <= x < -8.5421), VQSRTrancheINDEL99.95to100.00+ (Truth sensitivity tranche level for INDEL model at VQS Lod < -37254.4742), VQSRTrancheINDEL99.95to100.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: -37254.4742 <= x < -18.4482), VQSRTrancheSNP99.60to99.80 (Truth sensitivity tranche level for SNP model at VQS Lod: -4.9627 <= x < -1.8251), VQSRTrancheSNP99.80to99.90 (Truth sensitivity tranche level for SNP model at VQS Lod: -31.4709 <= x < -4.9627), VQSRTrancheSNP99.90to99.95 (Truth sensitivity tranche level for SNP model at VQS Lod: -170.3725 <= x < -31.4709), VQSRTrancheSNP99.95to100.00+ (Truth sensitivity tranche level for SNP model at VQS Lod < -39645.8352), VQSRTrancheSNP99.95to100.00 (Truth sensitivity tranche level for SNP model at VQS Lod: -39645.8352 <= x < -170.3725), AC_Adj0_Filter (No sample passed the adjusted threshold (GQ >= 20 and DP >= 10)) - ExAC\",Source=\"ExAC\",Version=\"ExAC v1.0, lifted over to GRCh38\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")




chromosomes = ["chr" + str(i) for i in range(1, 23)] + ["chrX", "chrY-and-the-rest"]
handlers = [bgzf.BgzfWriter(output_directory + "/" + x + ".exac.vcf.gz") for x in chromosomes]


def calculate_af(ac, an, precision=5):
    format_string = '{0:.' + str(precision) + 'f}'
    try:
        x = format_string.format(round(float(ac)/float(an), precision)).rstrip('0').rstrip('.')
    except:
        x = '.'
    return x


for handle in handlers:
    handle.write("\n".join(header) + "\n")


populations = ["AFR", "AMR", "EAS", "FIN", "NFE", "OTH", "SAS"]
populations_AC = [[] for x in populations]
populations_AN = ["." for x in populations]
populations_AF = [[] for x in populations]



for raw_line in stdin:

    if not raw_line.startswith("#"):

        line = raw_line.strip().split("\t")

        CHR = line[0]
        line[5] = "."
        REF = line[3]
        ALT = line[4].split(",")
        FILTER = line[6]

        AC_Adj = []
        AN_Adj = "."
        POPMAX = []
        AC_POPMAX = []
        AN_POPMAX = "."

        for info in line[7].split(";"):

            if info.startswith("AN_Adj="):
                AN_Adj = info[info.index("=") + 1:]
            elif info.startswith("AC_Adj="):
                AC_Adj = info[info.index("=") + 1:].split(",")
            elif info.startswith("POPMAX="):
                POPMAX = info[info.index("=") + 1:].split(",")
            elif info.startswith("AC_POPMAX="):
                AC_POPMAX = info[info.index("=") + 1:].split(",")
            elif info.startswith("AN_POPMAX="):
                AN_POPMAX = info[info.index("=") + 1:]

            elif info.startswith(tuple(["AC_" + x for x in populations])):
                population = info[3:info.index("=")]
                populations_AC[populations.index(population)] = info[info.index("=") + 1:].split(",")

            elif info.startswith(tuple(["AN_" + x for x in populations])):
                population = info[3:info.index("=")]
                populations_AN[populations.index(population)] = info[info.index("=") + 1:]

        for alt_index in range(0, len(ALT)):

            if not ALT[alt_index] == REF:

                INFO = []

                INFO.append("ISEQ_EXAC_AC=" + AC_Adj[alt_index])
                INFO.append("ISEQ_EXAC_AN=" + AN_Adj)
                INFO.append("ISEQ_EXAC_AF=" + calculate_af(AC_Adj[alt_index], AN_Adj))

                INFO.append("ISEQ_EXAC_POPMAX=" + POPMAX[alt_index])
                INFO.append("ISEQ_EXAC_POPMAX_AF=" + calculate_af(AC_POPMAX[alt_index], AN_POPMAX))

                for population_index in range(0, len(populations)):
                    INFO.append("ISEQ_EXAC_" + populations[population_index] + "_AF=" + calculate_af(populations_AC[population_index][alt_index], populations_AN[population_index]))

                INFO.append("ISEQ_EXAC_FILTER_STATUS=" + FILTER.replace(',',':'))

                if CHR in chromosomes:
                    handlers[chromosomes.index(CHR)].write('\t'.join(line[:4] + [ALT[alt_index]] + line[5:7] + [';'.join(INFO)] + line[8:]) + "\n")
                else:
                    handlers[chromosomes.index("chrY-and-the-rest")].write('\t'.join(line[:4] + [ALT[alt_index]] + line[5:7] + [';'.join(INFO)] + line[8:]) + "\n")


for handle in handlers:
    handle.close()
