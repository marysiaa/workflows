## TEST-FILES-CREATION-TOOLS

This directory contains scripts needed to create test files (./src/test/resources).

---

## Table of Contents

**Test files creation tools**
  * [Create NF2 sample variants VCF (.py)](#create-nf2-sample-variants-vcf)

[Return to: scripts ](./../readme.md)

---

### Create NF2 sample variants VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**:  [**create-nf2-sample-variants-vcf 1.0.0**](https://gitlab.com/intelliseq/workflows/-/blob/create-nf2-sample-variants-vcf.py@1.0.0/src/main/scripts/test-files-creation-tools/create-nf2-sample-variants-vcf.py)

```
usage: create-nf2-sample-variants-vcf.py [-h] [-v] PATH_TO_FASTA_REF_GZ

    Create VCF file containing several diffrenet NF2 gene variants

positional arguments:
  PATH_TO_FASTA_REF_GZ  Path to GRCh38 bgzipped reference FASTA file

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

---
