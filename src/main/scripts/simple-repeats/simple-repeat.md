## **Simple repeats annotation**  
Download and prepare files needed for annotation:   
```
wget 'ftp://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/simpleRepeat.txt.gz'   
zcat simpleRepeat.txt.gz | cut -f 2-4 > simpleRepeat.bed   
bgzip simpleRepeat.bed    
tabix -p bed simpleRepeat.bed.gz   
```
Bgzip and index input vcf:   
```
bgzip input_vcf   
tabix -p vcf input_vcf  
```
Add annotation ISEQ_SIMPLE_REPEAT (only to lines with variants located within UCSC SimpleRepeat regions):   
```
python3 simple-repeat.py -i input_vcf.gz -r simpleRepeat.bed.gz > output_vcf   
```
   
The **simple_repeat.py** is located  here:   
https://gitlab.com/intelliseq/workflows/tree/dev/src/main/scripts/simple-repeat/simple-repeat.py   
    
Prepared simpleRepeat.bed.gz and simpleRepeat.bed.gz.tbi are located here:       
  - **anakin** (main location):  
    - directory: **/data/public/intelliseqngs/workflows/resources/intervals/ucsc-simple-repeats**  
    - on-line: http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/intervals/ucsc-simple-repeats   
  - **kenobi** (rsynced to main location on anakin):  
    - directory: **/data/public/intelliseqngs/workflows/resources/intervals/ucsc-simple-repeats**   
   
   
  



