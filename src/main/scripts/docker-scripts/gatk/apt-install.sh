apt update && \
apt upgrade -y && \
apt install -y \
uuid-runtime \
jq \
jo \
curl \
libcurl4-openssl-dev \
zlib1g-dev \
libbz2-dev \
liblzma-dev \
libfontconfig1 \
libncurses5-dev \
libssl-dev \
python3-pip \
python3-dev \
locales && \
ln -s /usr/bin/python3 /usr/local/bin/python && \
rm -rf /var/lib/apt/lists/*