#!/bin/bash
set -e
tar -xvf /tools/$1/$2/$1.tar.bz2 -C /tools/$1/$2/ && \
    rm /tools/$1/$2/$1.tar.bz2 && \
    /tools/$1/$2/$1-$2/configure && \
    make -C /tools/$1/$2/$1-$2 && \
    make install -C /tools/$1/$2/$1-$2 && \
    rm -rf /tools/$1/$2/$1-$2