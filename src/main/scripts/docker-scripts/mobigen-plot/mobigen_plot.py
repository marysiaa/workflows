#!/usr/bin/env python3

import argparse
import json
import os
import subprocess
from typing import List
from typing import Dict
from typing import Any
from typing import Iterable

__version__ = '0.0.1'


def load_json(path: str):
    with open(path) as f:
        return json.load(f)


def get_required_data(model_dict: Dict[str, Any]) -> Dict[str, Any]:
    ret_dict = {
        'module_name': get_module_name(model_dict),
        'plot_title': model_dict['trait_title'].replace(' ', '_'),
        'model_type': model_dict['test_type'].replace(' ', '_')
    }
    if ret_dict['model_type'] == 'Polygenic_Risk_Score':
        ret_dict.update(get_polygenic_data(model_dict))
    elif ret_dict['model_type'] == 'Single_SNP_analysis':
        ret_dict.update(get_single_snip_data(model_dict))
    else:
        raise RuntimeError()
    return ret_dict


def get_module_name(model_dict: Dict) -> str:
    return model_dict['module'].rsplit('.', 1)[-1]


def get_polygenic_data(model_dict: Dict[str, Any]) -> Dict[str, Any]:
    return {
        'plot_data': model_dict['plot_data'],
        'lower_boundary': model_dict['category_boundaries']['Average risk']['from'],
        'upper_boundary': model_dict['category_boundaries']['Average risk']['to'],
        'patient_score': model_dict['numerical_output'],
        'left_plot_category': model_dict['category_on_the_left_side_of_plot'].replace(' ', '_'),
        'patient_category': model_dict['categorical_output'].replace(' ', '_')
    }


def get_single_snip_data(model_dict: Dict[str, Any]) -> Dict[str, Any]:
    variant = model_dict['variants'][0]
    return {
        'genotypes': transform_genotypes_to_string(variant['genotypes']),
        'patient_genotype': '/'.join(variant['patient_genotype']),
        'patient_risk': model_dict['categorical_output'].replace(' ', '_'),
        'table_title': model_dict['trait_title'].replace(' ', '_')
    }


def transform_genotypes_to_string(genotypes: List[Dict]) -> str:
    initial_string = 'Genotype\tGenotype_Impact\n{}\n'
    genotypes_impacts_string = '\n'.join(
        '{}\t{}'.format('/'.join(genotype['genotype']), genotype['gene_impact_string']) for genotype in genotypes)
    return initial_string.format(genotypes_impacts_string)


def write_plot_data_to_a_file(plot_data: Dict[str, List], path: str):
    with open(path, 'w') as f:
        pairs = zip(cast_elements_to_str(plot_data['x']), cast_elements_to_str(plot_data['y']))
        f.write('\n'.join('\t'.join(pair) for pair in pairs))


def cast_elements_to_str(elements: Iterable[Any]) -> List[str]:
    return [str(element) for element in elements]


def write_string(my_string: str, path: str):
    with open(path, 'w') as f:
        f.write(my_string)


def run_bash_cmd(cmd: str):
    subprocess.check_call(cmd, shell=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str, required=True, help='Mobigen json path')
    parser.add_argument('--scripts_location', type=str, default=os.path.dirname(os.path.abspath(__file__)))
    arguments = parser.parse_args()

    for model in load_json(arguments.input):
        data = get_required_data(model)
        if data['model_type'] == 'Polygenic_Risk_Score':
            write_plot_data_to_a_file(data['plot_data'], 'plot_data_file.txt')
            run_bash_cmd('Rscript {}/dist-plot.R \
            --input plot_data_file.txt \
            --output {}.svg \
            --plot_title {} \
            --lower_boundary {} \
            --upper_boundary {} \
            --score {} \
            --category_on_the_left_side {} \
            --patient_category {}'.format(
                arguments.scripts_location,
                data['module_name'],
                data['plot_title'],
                data['lower_boundary'],
                data['upper_boundary'],
                data['patient_score'],
                data['left_plot_category'],
                data['patient_category']
            ))
        else:
            write_string(data['genotypes'], 'genotype_data_file.txt')
            run_bash_cmd('Rscript {}/gen-table.R \
            --input genotype_data_file.txt \
            --output {}.svg \
            --patient_genotype {} \
            --patient_risk {} \
            --table_title {}'.format(
                arguments.scripts_location,
                data['module_name'],
                data['patient_genotype'],
                data['patient_risk'],
                data['table_title']
            ))
