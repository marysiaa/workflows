#!/usr/bin/env python3

import argparse
import gzip
from operator import itemgetter

__version__ = '2.0.0'


class AnnotationDescription:

    def __init__(self, tsv_id, info_id, info_number, info_type, info_description, info_source, info_version,
                 formatting_name):

        self.tsv_id = tsv_id.strip()
        self.info_id = info_id.strip()
        self.header_info = '##INFO=<' + ','.join(['ID=' + info_id.strip(),
                                                  'Number=' + info_number.strip(),
                                                  'Type=' + info_type.strip(),
                                                  'Description="' + info_description.strip() + '"',
                                                  'Source="' + info_source.strip() + '"',
                                                  'Version="' + info_version.strip() + '"']) + '>'
        self.formatting_name = formatting_name.strip()

    def add_index(self, index):
        self.index = index

    # Formatting functions
    def basic_formatting(self, x):
        if x in ["None", ""]:
            return "."
        else:
            return x.replace(';', '|').replace(" ", "^").replace(",", "*")

    def acmg_rank_formatting(self, x):
        if x.strip() in ["None", ""]:
            return "."
        else:
            return x.replace('1', 'Benign').replace("2", "Likely^benign").replace("3", "Uncertain").replace("4",
                                                                                                            "Likely^pathogenic").replace(
                "5", "Pathogenic")

    # Getters
    def get_header_info(self):
        return self.header_info

    def get_info_field(self, x):

        if len(x) > 0:
            return '='.join([self.info_id, getattr(AnnotationDescription, self.formatting_name)(self, x)])
        else:
            return None


def get_tuple_annotations_dictionary_and_sorted_annotations_indices_list(tsv_file, annotations_description_file):
    # Create annotations dictionary with TSV names of fields as keys from annotation description file

    annot_dict = {}
    with reading_function(annotations_description_file) as annot:
        for raw_line in annot:
            if not raw_line.startswith('TSV'):
                line = raw_line.strip().split("\t")
                tsv_id = line[0]
                info_id = line[1]
                info_number = line[2]
                info_type = line[3]
                info_description = line[4]
                info_source = line[5]
                info_version = line[6]
                formatting_name = line[7]
                annot_dict[tsv_id] = AnnotationDescription(tsv_id, info_id, info_number, info_type, info_description,
                                                           info_source, info_version, formatting_name)

    # Get:
    #       - sorted indices of annotation columns
    #       - annotations dictionary with indices as keys
    #       - get VCF indices

    annotations_dictionary = {}
    annotations_indices = list()
    tsv_data = []
    with reading_function(tsv_file) as tsv:
        first_line = tsv.readline()
        columns_names = first_line.strip().split("\t")
        variant_id_index = columns_names.index("ID")
        # print(variant_id_index)
        for column_name in columns_names:
            if column_name in annot_dict.keys():
                #print(column_name)

                annotations_dictionary[columns_names.index(column_name)] = \
                    annot_dict[column_name]
                annotations_indices.append((columns_names.index(column_name)))
        for raw_line in tsv:
            tsv_data.append(raw_line.strip("\n").split("\t"))
            # print(raw_line)
    tsv_data = sorted(tsv_data, key=itemgetter(variant_id_index))
    annotations_indices = sorted(annotations_indices)
    # print(annotations_indices)
    return (annotations_dictionary, annotations_indices, tsv_data, variant_id_index)


def get_old_header_and_rest(input_vcf):
    header_list = []
    data_list = []
    chrom_line = ""
    with reading_function(input_vcf) as in_vcf:

        header_list.append("##fileformat=VCFv4.3\n##reference=GRCh38\n")
        for vcf_line in in_vcf:
            # Get INFO, FORMAT, ALT, FILTER, contig, reference and CHROM line from input AnnotSV VCF
            if vcf_line.startswith('##') and not vcf_line.startswith('##fileformat') and not vcf_line.startswith(
                    '##reference'):
                header_list.append(vcf_line)
            if vcf_line.startswith('#CHROM'):
                chrom_line += vcf_line
            if not vcf_line.startswith("#"):
                # print(vcf_line)
                data_list.append(vcf_line.strip("\n").split("\t"))
        sorted_data_list = sorted(data_list, key=itemgetter(2))
    return header_list, chrom_line, sorted_data_list


def get_new_header(annotations_dictionary, annotations_indices):
    new_header = []
    for line in [annotations_dictionary[index].get_header_info() for index in annotations_indices]:
        new_header.append(line)

    return "\n".join(new_header) + "\n"


def reading_function(filename):
    if filename.endswith(".gz"):
        return gzip.open(filename, 'rt')
    else:
        return open(filename, "r")


if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=
    """
    Convert the ISEQ modified short TSV output from AnnotSV 3.0 (for GRCh38) to VCF file.

    This script requires a TSV file containing the description of AnnotSV annotations to include in the output VCF file (--descriptions). 
    This file should start with a header with the following columns:

    (1)   Name: "TSV field name"
          Contents: the name of column in the TSV file produced by AnnotSV (modified by change-annotsv-ranking.py).

    (2-6) Names: "ID", "Number", "Type", "Description", "Source", "Version"
          Contents: the description of the contents of a TSV field converted to VCF format, compatible with VCF v4.3 specification.

    (7)   Name: "Python function"
          Contents: the name of the method (from class AnnotationDescription in this script) that should be used to convert the contents of TSV field into contents of VCF INFO field.
    """)

    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input-vcf', type=str, help="path to the VCF file that was used as an input to AnnotSV")
    parser.add_argument('-t', '--annotsv-table', type=str, help="path to the short AnnotSV table (full lines only)")
    parser.add_argument('-d', '--descriptions', type=str,
                        help="path to a TSV file containing the description of AnnotSV annotations that will be included in an output VCF")
    parser.add_argument('-o', '--output-vcf', type=str,
                        help="path to the output vcf (with AnnotSV annotations added)")

    args = parser.parse_args()
    tsv_file = args.annotsv_table
    input_vcf = args.input_vcf
    output_vcf = args.output_vcf
    annotations_description_file = args.descriptions

    annotations_dictionary, annotations_indices, tsv_data, id_index = get_tuple_annotations_dictionary_and_sorted_annotations_indices_list(
        tsv_file, annotations_description_file)

    new_header = get_new_header(annotations_dictionary, annotations_indices)

    old_header, chrom_line, vcf_data = get_old_header_and_rest(input_vcf)
    #print(vcf_data[0])
    # print(tsv_data[0])
    with open(output_vcf, "w") as out_vcf:

        out_vcf.write("".join(old_header))
        out_vcf.write(new_header)
        out_vcf.write(chrom_line)
        if len(vcf_data) == len(tsv_data):
            for i in range(len(vcf_data)):
                if vcf_data[i][2] == tsv_data[i][id_index]:
                    # print(tsv_data[i])

                    new_info = ';'.join(
                        [annotations_dictionary[index].get_info_field(tsv_data[i][index]) for index in
                         annotations_indices if len(tsv_data[i][index]) > 0])
                    info = [';'.join([vcf_data[i][7], new_info])]
                    vcf_line_start = vcf_data[i][0:7]
                    vcf_line_end = vcf_data[i][8:]
                    new_vcf_line = "\t".join(vcf_line_start + info + vcf_line_end) + "\n"
                    out_vcf.write(new_vcf_line)
                else:
                    raise RuntimeError(
                        "Something went wrong: vcf_data and tsv_data lists are in different order")
        else:
            raise RuntimeError("Something went wrong: vcf_data and tsv_data lists are of different length")
