import re
import argparse
import gzip
from functools import reduce

__version__ = '0.1.0'


def get_index(header, name):
    try:
        return header.index(name)
    except ValueError:
        return None


# variant: list of lines with the same id, first line is full
# check variant type
def check_variant_type(variant_full_line, sv_type_index, alt_index):
    type_value = variant_full_line[sv_type_index]
    type_alt = variant_full_line[alt_index]
    if (type_value == "DEL") or (type_value == "CNV" and type_alt == "<CN0>"):
        return "LOSS"
    elif (type_value == "DUP") or (type_value == "CNV" and type_alt != "<CN0>"):
        return "GAIN"
    elif type_value in ["INS", "LINE1", "SVA", "ALU"]:
        return "INSERTION"
    elif type_value == "INV":
        return "INVERSION"
    elif type_value == "BND":
        return "BREAKPOINT"
    else:
        return "OTHER"


# check variant ACMG score AnnotSV_ranking_score
def check_float_value(variant_list, field_index):
    try:
        return float(variant_list[field_index])
    except (ValueError, TypeError):
        return None


# check if given gene is haploinsufficient (morbid genes are substituted by genes with low gnomAD oe_lof
def check_haploinsufficiency(variant_line, hi_index, omim_morbid_index):
    try:
        hi_cg = int(variant_line[hi_index])
        hi_cg_test = hi_cg == 3
    except ValueError:
        hi_cg_test = False
    try:
        lofoe = variant_line[omim_morbid_index] == "yes"
    except ValueError:
        lofoe = False
    return hi_cg_test or lofoe


# check if given gene is dosage sensitive
def check_dosage_sensitivity(variant_line, ts_index):
    ts_cg = variant_line[ts_index]
    try:
        return int(ts_cg) == 3
    except ValueError:
        return False


# check if variant intersects with gene CDS
def check_cds_intersection(variant_line, overlapped_cdc_length_index):
    cds_overlap = variant_line[overlapped_cdc_length_index]
    try:
        return int(cds_overlap) > 0
    except ValueError:
        return False


# check if variant intersects with transcript
def check_tx_intersection(variant_line, overlapped_tx_length_index):
    tx_overlap = variant_line[overlapped_tx_length_index]
    try:
        return int(tx_overlap) > 0
    except ValueError:
        return False


# check if variant overlaps entire transcript
def check_if_entire_tx(variant_line, location_index, location2_index):
    location1 = variant_line[location_index]
    location2 = variant_line[location2_index]
    return (location1 == "txStart-txEnd") and (location2 == "5'UTR-3'UTR")


# check if variant leads to gene copy gain
def check_gain_of_copy(variant_type, cds, entire_tx):
    gain_test = (cds and variant_type == "GAIN" and entire_tx)
    return gain_test


##  fields manipulation tools
def update_line(line, indexes, values):
    index_val_pairs = zip(indexes, values)
    new_line = line[:]
    for i in index_val_pairs:
        new_line[i[0]] = i[1]
    # text = "\t".join(line_split) + "\n"
    return new_line


def check_conditions_product(bool_list):
    conditions_product_list = [bool(reduce(lambda x, y: x * y, i)) for i in zip(*bool_list)]
    return conditions_product_list


def write_chosen_genes(genes_list, bool_list, prefix_text=""):
    return "(" + prefix_text + ", ".join([genes_list[i] for i in range(len(genes_list)) if bool_list[i]]) + ")"


def check_symbol_in_acmg_criteria(criteria_field, category_symbol):
    criteria_list = [i.split("(")[0].strip() for i in criteria_field.split(";")]
    return category_symbol in criteria_list


def get_gene_symbols_in_acmg_criteria(criteria_field, category_symbol):
    criteria_pairs = [i.replace(" ", "").strip(")").split("(") for i in criteria_field.split(";")]
    try:
        category_genes = [i[1] for i in criteria_pairs if i[0] == category_symbol][0]
        genes_list = [i for i in re.split('[^a-zA-Z0-9.-_]', category_genes) if i != ""]
    except IndexError:
        genes_list = []
    return genes_list


def substitute_categories(acmg_criteria_string, old_category, new_category, genes):
    criteria_pairs = [i.replace(" ", "").strip(")").split("(") for i in acmg_criteria_string.split(";")]
    index_of_old_category = [i[0] == old_category for i in criteria_pairs].index(True)
    criteria_pairs[index_of_old_category] = [new_category, genes]
    return ";".join([" ".join(i) for i in criteria_pairs])


def update_rank(acmg_score):
    if acmg_score <= -0.99:
        rank = 1
    elif acmg_score > -0.99 and acmg_score <= -0.90:
        rank = 2
    elif acmg_score > -0.90 and acmg_score < 0.9:
        rank = 3
    elif acmg_score >= 0.9 and acmg_score < 0.99:
        rank = 4
    else:
        rank = 5
    return rank


## writing functions
def change_items_to_string(line_list):
    return [str(i) for i in line_list]


def check_if_compress(filename):
    return filename.endswith(".gz")


def pick_reading_function(filename):
    if filename.endswith(".gz"):
        return gzip.open(filename, 'rt')
    else:
        return open(filename, "r")


def pick_writing_function(filename, compress):
    if compress:
        return gzip.open(filename, 'wb')
    else:
        return open(filename, "w")


def change_variant_to_text(variant_list, compress, full_output, header=False):
    if header:
        text = variant_list.replace("OMIM_morbid\t", "GnomAD_LoF_intolerant\t").replace("OMIM_morbid_candidate",
                                                                                        "OMIM_morbid")
    else:
        if not full_output:
            text = "\t".join(change_items_to_string(variant_list[0])).replace("(morbid)",
                                                                              "(gnomAD LoF intolerant)") + "\n"
        else:
            text = "\n".join(
                ["\t".join(change_items_to_string(i)).replace("(morbid)", "(gnomAD LoF intolerant)") for i in
                 variant_list]) + "\n"

    if compress:
        return text.encode()
    else:
        return text


# recalculate rank:
def upadte_5fgh(panel_score, threshold1, threshold2):
    if panel_score >= threshold2:
        score_5fgh = 0.15
        category = "5H"
    elif panel_score >= threshold1:
        score_5fgh = 0.1
        category = "5G"
    else:
        score_5fgh = 0.0
        category = "5F"
    return score_5fgh, category


# check and add panel scores => 5F (0), 5G (0.1), 5H (0.15) (should be ok for GAIN too)
def check_panel_for_5fgh(full_line, gene_count_index, panel_score_index, panel_genes_index, threshold1, threshold2):
    if int(full_line[gene_count_index]) > 0:
        try:
            panel_score = check_float_value(full_line, panel_score_index)
            panel_genes = full_line[panel_genes_index].replace(";", ", ")
            if panel_genes == "":
                panel_genes = panel_genes
            else:
                panel_genes = " (" + panel_genes.replace(";", ", ") + ")"

            score_5fgh, category = upadte_5fgh(panel_score, threshold1, threshold2)
            return score_5fgh, category, panel_genes
        except (ValueError, TypeError):
            return None, None, None
    else:
        return None, None, None


def check_panel_for_inv5fgh(variant, gene_count_index, breakpoints_genes_list, gene_index, panel_score_index, theshold1,
                            threshold2):
    full_line = variant[0]
    split_lines = variant[1:]
    if int(full_line[gene_count_index]) > 0:
        try:
            panel_scores = [check_float_value(i, panel_score_index) for i in split_lines]
            is_breakpoint_gene = [i[gene_index] for i in split_lines if i in breakpoints_genes_list]
            panel_scores_for_breakpoints = [panel_scores[i] * is_breakpoint_gene[i] for i in range(len(split_lines))]
            all_genes = [i[gene_index] for i in split_lines]
            panel_score = max(panel_scores_for_breakpoints)
            panel_genes = write_chosen_genes(all_genes, [bool(i) for i in panel_scores_for_breakpoints], prefix_text="")
            if panel_genes == "":
                panel_genes = panel_genes
            else:
                panel_genes = " (" + panel_genes.replace(";", ", ") + ")"

            score_5fgh, category = upadte_5fgh(panel_score, threshold1, threshold2)
            return score_5fgh, category, panel_genes
        except (ValueError, TypeError):
            return None, None, None
    else:
        return None, None, None


# LOSS: only 5FGH
# update acmg scores, categories and rank (5FGH)
def update_fields_for_loss(variant, gene_count_index, panel_score_index, panel_genes_index, acmg_score_index,
                           acmg_criteria_index, acmg_rank_index, threshold1, threshold2):
    new_variant = variant[:]
    full_line_list = variant[0]

    score_5fgh, category_5fgh, panel_genes = check_panel_for_5fgh(full_line_list, gene_count_index, panel_score_index,
                                                                  panel_genes_index,
                                                                  threshold1, threshold2)
    old_acmg_score = check_float_value(full_line_list, acmg_score_index)
    if old_acmg_score is not None and score_5fgh is not None:
        new_score = round(old_acmg_score + score_5fgh, 2)
        new_categories = full_line_list[acmg_criteria_index].replace("5F", category_5fgh + panel_genes)
        new_rank = update_rank(new_score)
        new_line = update_line(full_line_list, [acmg_score_index, acmg_criteria_index, acmg_rank_index],
                               [new_score, new_categories, new_rank])
        new_variant[0] = new_line
    return new_variant


# GAIN (2A, 5FGH, 2HIJ):
# check if overlap with TS gene, if so and 2A 0 => update to 2A 1
def check_2a_for_gain(variant_list, acmg_criteria_index, ts_index, location_index, location2_index, cdc_overlap_index,
                      gene_index):
    full_line = variant_list[0]
    acmg_criteria_field = full_line[acmg_criteria_index]
    if check_symbol_in_acmg_criteria(acmg_criteria_field, "2A"):
        update = False
        ts_genes = ""
    else:
        split_lines = variant_list[1:]
        ts = [check_dosage_sensitivity(i, ts_index) for i in split_lines]
        cds = [check_cds_intersection(i, cdc_overlap_index) for i in split_lines]
        entire_tx = [check_if_entire_tx(i, location_index, location2_index) for i in split_lines]
        copy_gain_test = [check_gain_of_copy("GAIN", cds[i], entire_tx[i]) for i in range(len(split_lines))]
        ts_copy_gain = check_conditions_product([ts, copy_gain_test])
        update = sum(ts_copy_gain) > 0
        all_genes = [i[gene_index] for i in split_lines]
        ts_genes = write_chosen_genes(all_genes, ts_copy_gain, prefix_text="TS: ")
    return update, ts_genes


# check panel and update:
# 2h category (if needed) => from 2H-2 (0)to 2H-1 (0.45)
# 2i category (if needed) => from 2H-3 (0) to 2I-2 (0.45)
# 2j (0) => 2k (0.45)
def check_2hij_for_gain(variant_list, criterium_symbol, acmg_categories_index, panel_score_index, gene_index,
                        threshold2):
    full_line = variant_list[0]
    acmg_criteria_field = full_line[acmg_categories_index]
    update = False
    genes = ""
    if check_symbol_in_acmg_criteria(acmg_criteria_field, criterium_symbol):
        genes = get_gene_symbols_in_acmg_criteria(acmg_criteria_field, criterium_symbol)
        split_lines = variant_list[1:]
        genes_2h1i2k = [i[gene_index] for i in split_lines if
                        i[gene_index] in genes and check_float_value(i,
                                                                     panel_score_index) is not None and check_float_value(
                            i,
                            panel_score_index) >= threshold2]
        # print(genes_2h1i2k)
        if bool(genes_2h1i2k):
            update = True
            genes = "(" + ", ".join(genes_2h1i2k) + ")"
    return update, genes


def update_fields_for_gain(variant, gene_count_index, panel_score_index, panel_genes_index, acmg_score_index,
                           acmg_criteria_index, acmg_rank_index, ts_index, location_index, location2_index, cds_index,
                           gene_index, threshold1, threshold2):
    full_line_list = variant[0]
    new_variant = variant[:]
    old_acmg_score = check_float_value(full_line_list, acmg_score_index)
    if old_acmg_score != "NA":
        decision_2a, genes_2a = check_2a_for_gain(variant, acmg_criteria_index, ts_index, location_index,
                                                  location2_index,
                                                  cds_index, gene_index)
        if decision_2a:
            new_score = old_acmg_score + 1.0
            new_categories = full_line_list[acmg_criteria_index].replace("1A;", "1A;2A " + genes_2a)
        else:
            new_score = old_acmg_score
            new_categories = full_line_list[acmg_criteria_index]

        decision_2h, genes_2h = check_2hij_for_gain(variant, "2H-2", acmg_criteria_index, panel_score_index,
                                                    gene_index,
                                                    threshold2)
        decision_2i, genes_2i = check_2hij_for_gain(variant, "2I-3", acmg_criteria_index, panel_score_index,
                                                    gene_index,
                                                    threshold2)
        decision_2jk, genes_2jk = check_2hij_for_gain(variant, "2J", acmg_criteria_index, panel_score_index,
                                                      gene_index,
                                                      threshold2)
        score_5fgh, category_5fgh, panel_genes = check_panel_for_5fgh(full_line_list, gene_count_index,
                                                                      panel_score_index,
                                                                      panel_genes_index,
                                                                      threshold1, threshold2)
        if decision_2h:
            new_score += 0.45
            new_categories = substitute_categories(new_categories, "2H-2", "2H-1", genes_2h)

        if decision_2i:
            new_score += 0.45
            new_categories = substitute_categories(new_categories, "2I-3", "2I-2", genes_2i)

        if decision_2jk:
            new_score += 0.45
            new_categories = substitute_categories(new_categories, "2J", "2K", genes_2jk)

        try:
            new_score += score_5fgh
            new_categories = new_categories.replace("5F", category_5fgh + panel_genes)
        except TypeError:
            pass

        final_score = round(new_score, 2)
        new_rank = update_rank(new_score)
        new_line = update_line(full_line_list, [acmg_score_index, acmg_criteria_index, acmg_rank_index],
                               [final_score, new_categories, new_rank])
        new_variant[0] = new_line

    return new_variant


## INVERSIONS AND OTHER (simplified "de novo" analysis, just for breakpoints 2IJK and panel match 5FGH)
def check_categories_for_other(variant_list, is_inversion, gene_count_index, panel_score_index, gene_index, hi_index,
                               location_index,
                               location2_index, cds_overlap_index, morbid_index, acmg_rank_index, acmg_criteria_index,
                               acmg_score_index, threshold1,
                               threshold2):
    full_line = variant_list[0]
    split_lines = variant_list[1:]
    new_variant = variant_list[:]
    conditions_2 = 0
    hi_test = [check_haploinsufficiency(i, hi_index, morbid_index) for i in split_lines]
    not_hi_test = [not i for i in hi_test]
    panel_score_test = [check_float_value(i, panel_score_index) >= threshold2 if check_float_value(i,
                                                                                                   panel_score_index) is not None else False
                        for i in split_lines]

    negative_panel_score_test = [check_float_value(i, panel_score_index) < threshold2 if check_float_value(i,
                                                                                                           panel_score_index) is not None else False
                                 for i in split_lines]
    cds_test = [check_cds_intersection(i, cds_overlap_index) for i in split_lines]
    in_exon_test = ["exon" in i[location_index] for i in split_lines]
    genes = [i[gene_index] for i in split_lines]
    acmg_score = 0.0
    acmg_criteria = ""
    all_breakpoint_genes = ""

    if is_inversion:
        not_entire_tx_test = [not check_if_entire_tx(i, location_index, location2_index) for i in split_lines]

        likely_lof_test = [bool(cds_test[i] * in_exon_test[i]) for i in range(len(split_lines))]

        category_2i = check_conditions_product([hi_test, likely_lof_test])
        category_2k = check_conditions_product([hi_test, panel_score_test, not_entire_tx_test])
        category_2j = check_conditions_product([hi_test, negative_panel_score_test, not_entire_tx_test])
        category_2l = check_conditions_product([not_hi_test, not_entire_tx_test])

    else:

        category_2i = check_conditions_product([hi_test, in_exon_test])
        category_2k = check_conditions_product([hi_test, panel_score_test])
        category_2j = check_conditions_product([hi_test, negative_panel_score_test])
        category_2l = not_hi_test

    if sum(category_2i) > 0:
        genes_2i = write_chosen_genes(genes, category_2i)
        acmg_score += 0.45
        acmg_criteria += "2I" + genes_2i + ";"
        conditions_2 += 1
        all_breakpoint_genes += genes_2i
    if sum(category_2j) > 0:
        genes_2j = write_chosen_genes(genes, category_2j)
        acmg_criteria += "2J" + genes_2j + ";"
        conditions_2 += 1
        all_breakpoint_genes += genes_2j
    if sum(category_2k) > 0:
        genes_2k = write_chosen_genes(genes, category_2k)
        acmg_score += 0.45
        acmg_criteria += "2K" + genes_2k + ";"
        conditions_2 += 1
        all_breakpoint_genes += genes_2k
    if sum(category_2l) > 0:
        genes_2l = write_chosen_genes(genes, category_2l)
        acmg_criteria += "2L" + genes_2l + ";"
        conditions_2 += 1
        all_breakpoint_genes += genes_2l

    if conditions_2 == 0:
        acmg_score += -0.60
        acmg_criteria = "2M;"

    else:
        if is_inversion:
            all_breakpoint_genes = set(all_breakpoint_genes.replace("(", "").replace(")", "").split(", "))
            # print(all_breakpoint_genes)
            score_5fgh, category, panel_genes = check_panel_for_inv5fgh(variant_list, gene_count_index,
                                                                        set(all_breakpoint_genes), gene_index,
                                                                        panel_score_index,
                                                                        threshold1, threshold2)
        else:
            score_5fgh, category, panel_genes = check_panel_for_5fgh(full_line, gene_count_index, panel_score_index,
                                                                     panel_genes_index,
                                                                     threshold1, threshold2)
        if score_5fgh is not None:
            acmg_score += score_5fgh
            acmg_criteria += category + panel_genes

    acmg_rank = update_rank(acmg_score)
    acmg_score = round(acmg_score, 2)
    acmg_criteria = acmg_criteria.strip(';')
    new_line = update_line(full_line, [acmg_score_index, acmg_criteria_index, acmg_rank_index],
                           [acmg_score, acmg_criteria, acmg_rank])
    new_variant[0] = new_line
    return new_variant


## main variant analysis
def analyse_variant(variant, sv_type_index, alt_index, gene_count_index, panel_score_index, panel_genes_index,
                    acmg_score_index, acmg_criteria_index, acmg_class_index, ts_index,
                    location_index, location2_index, cds_index, hi_index,
                    gene_index, morbid_index, threshold1, threshold2):
    sv_type = check_variant_type(variant[0], sv_type_index, alt_index)
    if sv_type == "LOSS":
        new_variant = update_fields_for_loss(variant, gene_count_index, panel_score_index, panel_genes_index,
                                             acmg_score_index, acmg_criteria_index, acmg_class_index,
                                             threshold1, threshold2)

    elif sv_type == "GAIN":
        new_variant = update_fields_for_gain(variant, gene_count_index, panel_score_index, panel_genes_index,
                                             acmg_score_index,
                                             acmg_criteria_index, acmg_class_index, ts_index,
                                             location_index, location2_index, cds_index,
                                             gene_index, threshold1, threshold2)
    else:
        inversion = sv_type == "INVERSION"
        new_variant = check_categories_for_other(variant, inversion, gene_count_index, panel_score_index, gene_index,
                                                 hi_index, location_index,
                                                 location2_index, cds_index, morbid_index, acmg_class_index,
                                                 acmg_criteria_index,
                                                 acmg_score_index, threshold1,
                                                 threshold2)
    return new_variant


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Changes AnnotSV ranking, modifies format of the output tsv file with panel info added")
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input_annotsv', required=True, type=str, help='Path to input AnnotSV output tsv file.')
    parser.add_argument('-o', '--output_short', required=True, type=str,
                        help='Path to output file, this file contains only "full" rows.')
    parser.add_argument('-f', '--output_full', required=True, type=str,
                        help='Path to output file, this file contains all data')
    parser.add_argument('-t', '--panel_threshold1', required=False, type=float,
                        help='Defines threshold score for relatively good gene to phenotype match. Default: %(default)s',
                        default=30.0)
    parser.add_argument('-T', '--panel_threshold2', required=False, type=float,
                        help='Defines threshold score for very good gene to phenotype match. Default: %(default)s',
                        default=50.0)
    args = parser.parse_args()

    threshold1 = args.panel_threshold1
    threshold2 = args.panel_threshold2

    compress_full_output = check_if_compress(args.output_full)
    compress_short_output = check_if_compress(args.output_short)

    with pick_reading_function(args.input_annotsv) as table, pick_writing_function(args.output_full,
                                                                                   compress_full_output) as new_full_table, pick_writing_function(
        args.output_short,
        compress_short_output) as new_short_table:

        first_line = table.readline()

        # find all needed indexes in header
        header_list = first_line.strip("\n").split("\t")
        sv_type_index = get_index(header_list, "SV_type")
        alt_index = get_index(header_list, "ALT")
        annotsv_id_index = get_index(header_list, "AnnotSV_ID")
        annot_mode_index = get_index(header_list, "Annotation_mode")
        panel_score_index = get_index(header_list, "Panel_score")
        panel_genes_index = get_index(header_list, "Panel_genes")
        acmg_score_index = get_index(header_list, "AnnotSV_ranking_score")
        acmg_criteria_index = get_index(header_list, "AnnotSV_ranking_criteria")
        acmg_class_index = get_index(header_list, "ACMG_class")
        ts_index = get_index(header_list, "TS")
        location_index = get_index(header_list, "Location")
        location2_index = get_index(header_list, "Location2")
        cds_index = get_index(header_list, "Overlapped_CDS_length")
        gene_index = get_index(header_list, "Gene_name")
        hi_index = get_index(header_list, "HI")
        morbid_index = get_index(header_list, "OMIM_morbid")
        gene_count_index = get_index(header_list, "Gene_count")

        new_full_header = change_variant_to_text(first_line, compress_full_output, full_output=True, header=True)
        new_short_header = change_variant_to_text(first_line, compress_short_output, full_output=False, header=True)
        # change_variant_to_text(variant_list, compress, full_output, header=False)
        new_full_table.write(new_full_header)
        new_short_table.write(new_short_header)
        # variant: all lines about one sv - full and split (with the same id)
        variant = []
        variant_ids = set()
        for line in table:
            # find id, append variant lines to list
            split_line = line.strip("\n").split("\t")
            variant_id = split_line[annotsv_id_index]
            variant_ids.add(variant_id)

            if len(variant_ids) <= 1:

                variant.append(split_line)

            else:
                ## analyse variant lines, change ranking if needed, write to file ...
                updated_variant = analyse_variant(variant, sv_type_index, alt_index, gene_count_index,
                                                  panel_score_index,
                                                  panel_genes_index,
                                                  acmg_score_index, acmg_criteria_index, acmg_class_index, ts_index,
                                                  location_index, location2_index, cds_index, hi_index,
                                                  gene_index, morbid_index, threshold1, threshold2)

                full_variant_as_text = change_variant_to_text(updated_variant, compress_full_output, full_output=True)
                new_full_table.write(full_variant_as_text)
                short_variant_as_text = change_variant_to_text(updated_variant, compress_short_output,
                                                               full_output=False)
                new_short_table.write(short_variant_as_text)

                # empty variant list, add first line with different id to the cleaned list
                variant = []
                variant_ids = set()
                variant.append(split_line)
                variant_ids.add(variant_id)

        # analyse last variant
        updated_last_variant = analyse_variant(variant, sv_type_index, alt_index, gene_count_index, panel_score_index,
                                               panel_genes_index,
                                               acmg_score_index, acmg_criteria_index, acmg_class_index, ts_index,
                                               location_index, location2_index, cds_index, hi_index,
                                               gene_index, morbid_index, threshold1, threshold2)
        full_last_variant_as_text = change_variant_to_text(updated_last_variant, compress_full_output, full_output=True)
        new_full_table.write(full_last_variant_as_text)
        short_last_variant_as_text = change_variant_to_text(updated_last_variant, compress_short_output,
                                                            full_output=False)
        new_short_table.write(short_last_variant_as_text)
