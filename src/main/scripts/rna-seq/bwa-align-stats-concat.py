#!/usr/bin/python3

import argparse
import pandas as pd
import numpy as np
import os

__version__ = '1.0.0'

parser = argparse.ArgumentParser(description='Concat bam stats files')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-path-to-bwa-align-stats-files', '-i', metavar='input_path_to_bwa_align_stats_files', type=str,
                    required=True, help='path to bam stats files')
parser.add_argument('--output-file-name', '-o', metavar='output_file_name', type=str, required=True,
                    help='output file name')
args = parser.parse_args()


def main():

    stats_files = np.sort([f for f in os.listdir(args.input_path_to_bwa_align_stats_files) if f.endswith(".stats")])
    df = pd.DataFrame([])
    os.chdir(args.input_path_to_bwa_align_stats_files)

    for i in range(len(stats_files)):
        column_names = []
        values = []
        stats = pd.read_csv(stats_files[i], header=None)[0].tolist()
        for j in range(len(stats)):
            value_1 = stats[j].split("(")[0].split()[0:3]
            try:
                value_2 = stats[j].split("(")[1]
                value = ' '.join(value_1) + " (" + ''.join(value_2)
            except IndexError:
                value = ' '.join(value_1)
            values.append(value)
            column_name = stats[j].split("(")[0].split()[3:]
            column_names.append(' '.join(column_name))
        values[-1] = ' '.join(value_1)
        column_names[-1] = ' '.join(stats[j].split()[3:])
        df_values = pd.DataFrame(values).transpose()
        df_values.rename(index={0: stats_files[i].split(".stats")[0]}, inplace=True)
        df = pd.concat([df, df_values])

    df.columns = column_names
    df.index.name = "Sample_id"
    df.to_excel(args.output_file_name + ".xlsx")
    df.to_csv(args.output_file_name + ".tsv", sep="\t")

    os.chdir("../")

if __name__ == "__main__": main()
