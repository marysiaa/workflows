#!/usr/bin/python3

import argparse
import pandas as pd
import numpy as np

__version__ = '0.0.2'


def read_tsv_to_df(tsv_file: str):
    return pd.read_csv(tsv_file, sep="\t", index_col=0, engine='python')


def save_df_to_tsv(df, output_file_path: str, output_file_name: str):
    df.to_csv(output_file_path + "/" + output_file_name, sep="\t")


def concat_files(files):
    dataframe = pd.DataFrame([])
    for tsv_file in files:
        dataframe = pd.concat([dataframe, read_tsv_to_df(tsv_file)], axis=1)
    return dataframe


def prepare_files_and_save_to_tsv(tsv_files: list, analysis_id: str, output_file_path: str):
    eff_length = np.sort([f for f in tsv_files if f.endswith("eff_length.tsv")])
    est_counts = np.sort([f for f in tsv_files if f.endswith("est_counts.tsv")])
    tpm = np.sort([f for f in tsv_files if f.endswith("tpm.tsv")])
    filenames = [f"{analysis_id}_transcript-level-length.tsv", f"{analysis_id}_transcript-level-counts.tsv", f"{analysis_id}_transcript-level-tpm.tsv"]
    files = [eff_length, est_counts, tpm]
    for i in range(len(files)):
        df = concat_files(files[i])
        save_df_to_tsv(df, output_file_path, filenames[i])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Concat count reads from HTSeq')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--tsv-files', '-i', metavar='tsv_files', type=str, required=True, nargs='+',
                        help='path to files to be merged from kallisto quant')
    parser.add_argument('--analysis-id', '-a', metavar='analysis_id', type=str, required=True,
                        help='analysis_id')
    parser.add_argument('--output-file-path', '-o', metavar='output_file_path', type=str, required=True,
                        help='output file path')
    args = parser.parse_args()
    prepare_files_and_save_to_tsv(args.tsv_files, args.analysis_id, args.output_file_path)
