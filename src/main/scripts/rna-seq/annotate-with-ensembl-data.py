#!/usr/bin/python3
import pandas as pd
import argparse

__version__ = "0.0.1"


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates two .tsv files: for genes and transcripts. They are based '
                                                 'on given attributes by the user. All data comes from ensembl '
                                                 'database.')
    parser.add_argument('--input-file-to-annotate', '-f', metavar='input_file_to_annotate', type=str, required=True,
                        help='File in .tsv format to annotate')
    parser.add_argument('--input-ensembl-anno-tsv', '-a', metavar='input_ensembl_anno_tsv', type=str, required=True,
                        help='File in .tsv format generated with ensembl database containing attributes to annotate')
    parser.add_argument('--id-column-to-merge-on', '-i', metavar='id_column_to_merge_on', type=str, required=True,
                        help='Name of column with id in input .tsv files to merge on')
    parser.add_argument('--ensembl-column-to-merge-on', '-e', metavar='ensembl_column_to_merge_on', type=str, required=True,
                        help='Column in gene/transcript ensembl .tsv to merge on')
    parser.add_argument('--output-file-annotated', '-o', metavar='output_file_annotated', type=str, required=True,
                        help='A path for annotated file')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = args_parser_init()
    df_to_annotate = pd.read_csv(args.input_file_to_annotate, sep='\t')
    df_ensembl_anno = pd.read_csv(args.input_ensembl_anno_tsv, sep='\t')

    df_to_annotate = df_to_annotate.set_index([args.id_column_to_merge_on])
    df_ensembl_anno = df_ensembl_anno.set_index([args.ensembl_column_to_merge_on])

    merged = pd.merge(df_ensembl_anno, df_to_annotate, how='inner', left_index=True, right_index=True)
    merged.index.name = args.ensembl_column_to_merge_on

    merged.to_csv(args.output_file_annotated, sep='\t')
