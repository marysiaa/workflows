#!/usr/bin/python3

import argparse
import pandas as pd
import os

__version__ = '1.1.0'


def read_tsv_to_df(tsv_file: str):
    return pd.read_csv(tsv_file, sep="\t", index_col=0, header=None, engine='python').dropna(axis=1)


def concat_count_files(count_files: list, tbl_suffix: str):
    dataframe = pd.DataFrame([])
    for tsv_file in count_files:
        count_reads = read_tsv_to_df(tsv_file)
        count_reads.columns = [os.path.basename(tsv_file).split(".tsv")[0].split(f"-{tbl_suffix}-level")[0]]
        dataframe = pd.concat([dataframe, count_reads], axis=1)
    dataframe.index.name = f"{tbl_suffix}_id"
    return dataframe


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Concat count reads from HTSeq')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--count-files', '-i', metavar='count_files', type=str, required=True, nargs='+',
                        help='path to htseq count files')
    parser.add_argument('--tbl-suffix', '-s', metavar='tbl_suffix', type=str, required=True,
                        help='tbl suffix (gene or exon)')
    parser.add_argument('--output-file-name', '-o', metavar='output_file_name', type=str, required=True,
                        help='output file name')
    args = parser.parse_args()

    df = concat_count_files(count_files=args.count_files, tbl_suffix=args.tbl_suffix)
    df.to_csv(args.output_file_name, sep="\t")
