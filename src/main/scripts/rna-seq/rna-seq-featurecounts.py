#!/usr/bin/python3
import pandas as pd
import os
import argparse


__version__ = "0.0.2"


def args_parser_init():
    parser = argparse.ArgumentParser(description='Removes not used columns and renames reads from files')
    parser.add_argument('--input-tsv', '-t', metavar='input_tsv', type=str, required=True,
                        help='Result from featurecounts in .tsv format')
    parser.add_argument('--output-tsv', '-o', metavar='output_tsv', type=str, required=True,
                        help='Path to an output file')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = args_parser_init()
    df_gene = pd.read_csv(args.input_tsv, sep="\t", comment='#')
    df_gene = df_gene.drop(["Chr","Start","End","Strand","Length"], axis=1)

    for col in df_gene.columns:
        df_gene = df_gene.rename(columns={col: os.path.splitext(os.path.basename(col))[0]})

    df_gene.to_csv(args.output_tsv, sep="\t", index=None)