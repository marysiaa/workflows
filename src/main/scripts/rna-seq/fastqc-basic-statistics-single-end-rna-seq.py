#!/usr/bin/python3

import sys
import argparse
import pandas as pd
import os
import numpy as np
import re

__version__ = '1.0.0'

parser = argparse.ArgumentParser(description='Getting basic statistics (single-end)')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-path-to-fastqc-data-files', '-f', metavar='input_path_to_fastqc_data_files', type=str, required=True, help='path to fastqc data files')
parser.add_argument('--input-path-to-summary-files', '-s', metavar='input_path_to_summary_files', type=str, required=True, help='path to summary files')
parser.add_argument('--output-file-name', '-o', metavar='output_file_name', type=str, required=True, help='output file name')
args = parser.parse_args()

def main():

    fastqc_data_files = np.sort([f for f in os.listdir(args.input_path_to_fastqc_data_files) if f.endswith(".txt")])
    summary_files = np.sort([f for f in os.listdir(args.input_path_to_summary_files) if f.endswith(".txt")])

    os.chdir(args.input_path_to_summary_files)

    summary_info = []
    with open(summary_files[0]) as f:
        for line in f:
            summary_info.append(line.split("\t")[1])

    words = ["Total Sequences", "Sequences flagged as poor quality", "Sequence length", "%GC"]
    all_info = words + summary_info

    column_names = []
    for i in range(len(all_info)):
        column_names.append(re.sub("\s+", "_", all_info[i].strip()))

    df = pd.DataFrame([])
    for i in range(len(fastqc_data_files)):
        os.chdir("../")
        os.chdir(args.input_path_to_fastqc_data_files)
        with open(fastqc_data_files[i]) as f1:
            lines_1 = f1.readlines()
            values_fastqc_file = []
            for line in lines_1:
                for word in words:
                    if word in line:
                        values_fastqc_file.append(line.split()[-1])

        os.chdir("../")
        os.chdir(args.input_path_to_summary_files)
        with open(summary_files[i]) as f2:
            values_summary_file = []
            for line in f2:
                values_summary_file.append(line.split("\t")[0])

            values = values_fastqc_file + values_summary_file

            data = pd.DataFrame(values).T
            data.columns = column_names
            data.rename(index={0: fastqc_data_files[i].split("_fastqc.")[0]}, inplace=True)
            df = df.append(data, ignore_index=False)
    df.index.name = "Sample_name"
    df.to_excel(args.output_file_name + ".xlsx")
    df.to_csv(args.output_file_name + ".csv")
    
if __name__ == "__main__": main()

