#!/usr/bin/python3

import argparse
import os, sys
import shutil
import pandas as pd
import numpy as np
import re
import json

__version__ = '1.0.0'

parser = argparse.ArgumentParser(description='Getting overrepresented sequences (part 2)')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-path-to-overrepresented-files', '-i', metavar='input_path_to_overrepresented_files', type=str, required=True, help='path to overrepresented files')
parser.add_argument('--output-file-name', '-o', metavar='output_file_name', type=str, required=True, help='output file name')
args = parser.parse_args()

def main():

    fastqc_pickles = os.listdir(args.input_path_to_overrepresented_files)

    if not fastqc_pickles:
        df = pd.DataFrame(columns=['#Sequence', 'Count', 'Percentage', 'Possible Source', 'sample_name'])
        df.to_excel(args.output_file_name + ".xlsx")
    else:
        main_df = pd.DataFrame()
        for pickle in fastqc_pickles:
            df = pd.read_pickle(os.path.join(args.input_path_to_overrepresented_files, pickle))
            df['sample_name'] = pickle[:-4].split("_fastqc")[0]
            main_df = main_df.append(df)
        main_df = main_df.reset_index()

        duplicateDF = main_df[main_df.duplicated(subset="#Sequence", keep='first') == True]
        duplicated_sequences = duplicateDF['#Sequence']
        main_df['Count'] = pd.to_numeric(main_df['Count'], errors='ignore')
        dropped_duplicates = main_df.drop_duplicates(subset="#Sequence", keep='first')

        percents = []
        counts = []
        samples = []
        sources = []
        for sequence in duplicated_sequences:
            duplicates_df = main_df.loc[main_df['#Sequence'] == sequence]
            percents.append(duplicates_df.Percentage.max())
            counts.append(duplicates_df.Count.max())
            samples.append(', '.join(duplicates_df['sample_name'].tolist()))
            sources.append(duplicates_df['Possible Source'].unique()[0])


        final_df = pd.DataFrame()
        final_df['#Sequence'] = duplicated_sequences
        final_df['Count'] = counts
        final_df['Percentage'] = percents
        final_df['Possible Source'] = sources
        final_df['sample_name'] = samples
        frames = [final_df, dropped_duplicates]

        result = pd.concat(frames)
        result.to_excel(args.output_file_name + ".xlsx")
    
if __name__ == "__main__": main()

