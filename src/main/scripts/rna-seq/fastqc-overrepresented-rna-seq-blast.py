#!/usr/bin/python3

import argparse
import pandas as pd
import subprocess
from Bio.Blast import NCBIXML

__version__ = '2.0.0'

parser = argparse.ArgumentParser(description='Search protein databases using a translated nucleotide query using blastx')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-overrepresented-file', '-i', metavar='input_overrepresented_file', type=str,
                    required=True, help='input file with overrepresented sequences')
parser.add_argument('--fasta-file-name', '-f', metavar='fasta_file_name', type=str, required=True,
                    help='fasta file name')
parser.add_argument('--results-file-name', '-r', metavar='results_file_name', type=str, required=True,
                    help='results file name')
parser.add_argument('--database', '-d', metavar='database', type=str, required=True, help='path to database')
parser.add_argument('--blast-excel-file-name', '-bx', metavar='blast_excel_file_name', type=str, required=True,
                    help='blast excel file name')
parser.add_argument('--output-file-name', '-o', metavar='output_file_name', type=str, required=True,
                    help='output file name')
args = parser.parse_args()

def main():

    df = pd.read_excel(args.input_overrepresented_file,  index_col=0)

    df_to_blast = df.loc[df['Possible Source'] == 'No Hit']

    fasta_seq_list = df_to_blast['#Sequence'].tolist()

    for j in range(len(fasta_seq_list)):
        with open(args.fasta_file_name, 'a') as fasta_file:
            fasta_file.write('> Sequence to analize' + '\n' + fasta_seq_list[j] + '\n')

    # Blast
    if len(fasta_seq_list) > 0:
        bashCommand = "blastx -db {} -query {} -out {} -outfmt 5".format(args.database, args.fasta_file_name,
                                                                         args.results_file_name)
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        process.wait()

        i = 0
        for record in NCBIXML.parse(open(args.results_file_name)):
            if record.alignments:
                blast_info = str(record.alignments[0])
                e_value = "E-value = " + str(record.alignments[0].hsps[0].expect)
                info = (blast_info + e_value).replace("\n", ", ")
                df_to_blast.iloc[i, 3] = df_to_blast.iloc[i, 3].replace(df_to_blast.iloc[i, 3], info)
                df_to_blast.to_excel(args.blast_excel_file_name)
                i += 1
            else:
                df_to_blast.iloc[i, 3] = df_to_blast.iloc[i, 3].replace(df_to_blast.iloc[i, 3], "No hits found")
                df_to_blast.to_excel(args.blast_excel_file_name)
                i += 1
        df.loc[df_to_blast.index] = df_to_blast
        df.to_excel(args.output_file_name + ".xlsx")
        df.to_csv(args.output_file_name + ".csv")
    else:
        df.to_excel(args.output_file_name + ".xlsx")
        df.to_csv(args.output_file_name + ".csv")
    
if __name__ == "__main__": main()

