#!/usr/bin/python3
import argparse
from pybiomart import Server
import pandas as pd
from pathlib import Path
from requests.exceptions import HTTPError, ConnectionError
import time
import json


__version__ = "0.0.2"
AUTHOR = "gitlab.com/olaf.tomaszewski"

pd.set_option('display.max_colwidth', 1000)


def attempt_to_get_database_query(organism_name, version, ensembl_attributes, versions_json):
    ensembl_available = False
    num_of_attempts = 0
    max_n_of_attempts = 2
    while not ensembl_available:
        try:
            return get_database_query(organism_name, version, ensembl_attributes, versions_json)
            ensembl_available = True
        except (HTTPError, ConnectionError):
            if num_of_attempts >= max_n_of_attempts:
                raise
            time.sleep(2 ** num_of_attempts)
        num_of_attempts += 1
    return None


def translate_organism_from_hreadable(organism_name, mart):
    organisms_df = mart.list_datasets()
    organisms_df['display_name'] = organisms_df['display_name'].str.split(' genes').str[0]
    translated_organism = organisms_df[organisms_df['display_name'] == organism_name]['name'].iloc[0]
    return translated_organism


def translate_attributed_from_hreadable(ensembl_attributes, dataset):
    attributes_df = dataset.list_attributes()
    translated = attributes_df[attributes_df['display_name'].isin(ensembl_attributes)]['name'].tolist()
    return translated


def get_database_query(organism_name, version, ensembl_attributes, versions_json):
    with open(versions_json) as versions_file:
        versions_dict = json.load(versions_file)

    server = Server(versions_dict[version])
    mart = server['ENSEMBL_MART_ENSEMBL']
    translated_organism = translate_organism_from_hreadable(organism_name, mart)
    dataset = mart[translated_organism]
    translated_attributes = translate_attributed_from_hreadable(ensembl_attributes, dataset)

    queried = dataset.query(attributes=translated_attributes)
    return queried


def create_version_catalog_and_export(queried, version_catalog, filename):
    Path(version_catalog).mkdir(parents=True, exist_ok=True)
    queried.to_csv(f'{version_catalog}/{filename}', sep='\t')


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates two .tsv files: for genes and transcripts. They are based '
                                                 'on given attributes by the user. All data comes from ensembl database.')
    parser.add_argument('--input-organism-name', '-n', metavar='input_organism_name', type=str, required=True,
                        help='Name of organism in human readable format')
    parser.add_argument('--input-version', '-s', metavar='input_version', type=str, required=True,
                        help='Version of dataset')
    parser.add_argument('--input-genes-anno-list', '-g', metavar='input_genes_anno_list', nargs='*',
                        required=False, help='List of human readable attributes for genes')
    parser.add_argument('--input-transcripts-anno-list', '-t', metavar='input_transcripts_anno_list', nargs='*',
                        required=False, help='List of human readable attributes for transcripts')
    parser.add_argument('--input-versions-json', '-j', metavar='input_versions_json', type=str, required=True,
                        help='Release versions in key-value format (version-link)')
    parser.add_argument('--output-dir', '-o', metavar='output_dir', type=str, required=True,
                        help='Directory where goes output')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--max_attempts', type=int, default=8,
                        help='Max number of attempts when retrieving data from ensembl.')
    args = parser.parse_args()
    return args


def merge_rows(df, id):
    df = df\
        .groupby([id])\
        .agg(lambda x: ';'.join(x.unique().astype(str)))

    df = df.replace({'nan': 'NA'})

    return df


if __name__ == "__main__":
    args = args_parser_init()

    genes_attributes = ["Gene stable ID"]
    if args.input_genes_anno_list:
        genes_attributes += args.input_genes_anno_list
    queried_genes = attempt_to_get_database_query(args.input_organism_name, args.input_version,
                                            genes_attributes, args.input_versions_json)

    transcripts_attributes = ["Transcript stable ID"]
    if args.input_transcripts_anno_list:
        transcripts_attributes += args.input_transcripts_anno_list
    queried_transcripts = attempt_to_get_database_query(args.input_organism_name, args.input_version,
                                            transcripts_attributes, args.input_versions_json)

    gene_id = 'Gene stable ID'
    queried_genes = merge_rows(queried_genes, gene_id)

    transcript_id = 'Transcript stable ID'
    queried_transcripts = merge_rows(queried_transcripts, transcript_id)

    version_catalog = f'{args.output_dir}/{args.input_version}'

    genes_filename = "genes-ensembl.tsv"
    create_version_catalog_and_export(queried_genes, version_catalog, genes_filename)

    transcripts_filename = "transcripts-ensembl.tsv"
    create_version_catalog_and_export(queried_transcripts, version_catalog, transcripts_filename)
