#!/usr/bin/python3

import argparse

__version__ = '1.0.1'


def read_file(filename: str) -> list:
    with open(filename, "r") as file:
        return file.readlines()


def create_dict_with_score(lines: list) -> dict:
    strand = dict()
    for line in lines:
        if line.startswith("Fraction of reads explained by"):
            strand[line.split("\"")[1]] = float(line.split(":")[1].strip())
    return strand


def create_dict_name_to_symbol() -> dict:
    name_to_symbol = {
        "stranded": [
            "1++,1--,2+-,2-+",
            "++,--"
        ],
        "reversely_stranded": [
            "1+-,1-+,2++,2--",
            "+-,-+"
        ]
    }
    return name_to_symbol


def replace_key_with_value(strand: dict, name_to_symbol: dict) -> dict:
    for k in list(strand):
        for key, value in name_to_symbol.items():
            if k in value:
                strand[key] = strand.pop(k)
    return strand


def check_for_diff(strand: dict):
    return abs(float(strand['stranded']) - float(strand['reversely_stranded']))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotate vcf with disease names derived from HPO')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input_rseqc_log', '-i', type=str, required=True, help='input rseqc log file')
    parser.add_argument('--output_file', '-o', type=str, required=True, help='output file')
    args = parser.parse_args()

    # read file
    rseqc_log = read_file(args.input_rseqc_log)

    # create dict with score
    stranded = create_dict_with_score(rseqc_log)

    # dict to replace +- with name (stranded or reversely stranded)
    name_to_symbol_dict = create_dict_name_to_symbol()

    # replace key with value
    stranded = replace_key_with_value(stranded, name_to_symbol_dict)

    # check for diff, and if more than 0.2 then stranded or reversely stranded, else unstranded
    diff = check_for_diff(stranded)
    if diff > 0.2:
        with open(args.output_file, 'w') as f:
            print(max(stranded, key=stranded.get), file=f)
    else:
        with open(args.output_file, 'w') as f:
            print("unstranded", file=f)
