#!/usr/bin/python3

import os

os.environ['OPENBLAS_NUM_THREADS'] = '1'

import argparse
import pysam
import pandas as pd
from civicpy import civic


__version__ = '1.1.0'


def load_vcf(vcf_file) -> pysam.VariantFile:
    vcf = pysam.VariantFile(vcf_file)
    return vcf


def add_info_to_header(vcf):
    vcf.header.info.add("ISEQ_CIVIC_EVIDENCE_INFO", number='1', type='String', 
                        description="CIVIC evidence info in format: evidence.name - evidence.gene.name - evidence.disease.name - evidence.drugs (evidence.drug_interaction_type) - evidence.evidence_level - evidence.rating")
    vcf.header.info.add("ISEQ_CIVIC_EVIDENCE_DESCRIPTION", number='1', type='String', 
                        description="CIViC evidence description")


def check_for_gene(evidence):
    try:
        return evidence.gene.name + " Alterations"
    except:
        return ""


def check_for_disease(evidence):
    try:
        return evidence.disease.name
    except:
        return ""
    
def check_for_rating(evidence):
    try:
        rating = evidence.rating
        if not rating:
            rating = 0
        return rating    
    except:
        return 0


def get_info(evidence, evidence_level, evidence_rating):
    id_full = evidence.name
    gene = check_for_gene(evidence)
    disease = check_for_disease(evidence)
    drugs = []
    for drug in evidence.therapies:
        drugs.append(drug.name)
    drugs = " / ".join(drugs)
    if evidence.therapy_interaction_type:
        drugs = drugs + f" ({evidence.therapy_interaction_type.lower()})"
    level = "Level " + evidence_level
    rating = "Rating " + str(evidence_rating)
    final = [id_full, gene, disease, drugs, level, rating]
    final = [i for i in final if i]
    return " - ".join(final)


def main():
    parser = argparse.ArgumentParser(description='Annotate vcf with CIViC evidence')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input_vcf', '-i', type=str, required=True, help='input vcf file')
    parser.add_argument('--output_vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf file')
    parser.add_argument('--minimal_evidence_level', '-l', default='C', type=str, help='minimal CIViC evidence level')
    parser.add_argument('--minimal_evidence_rating', '-r', default=3, type=int, help='minimal CIViC evidence rating')
    args = parser.parse_args()

    # annotate vcf
    vcf_reader = load_vcf(args.input_vcf)
    add_info_to_header(vcf_reader)
    vcf_writer = pysam.VariantFile(args.output_vcf, 'w', header=vcf_reader.header)

    for record in vcf_reader.fetch():
        evidence_info = []
        evidence_description = []
        iseq_civic_ids = record.info.get("ISEQ_TSV_CIVIC_IDS")
        if iseq_civic_ids:
            for civic_id in iseq_civic_ids.split("|"):
                variant_info = civic.get_variant_by_id(civic_id)
                for molecular_profile in variant_info.molecular_profiles:
                    for evidence in molecular_profile.evidence:
                        level = evidence.evidence_level
                        rating = check_for_rating(evidence)
                        test = rating >= args.minimal_evidence_rating and level <= args.minimal_evidence_level
                        if evidence.status == "accepted" and test:
                            evidence_info.append(get_info(evidence, level, rating).replace(" ", "^"))
                            evidence_description.append(evidence.description.replace(" ", "^").replace(",", "*").replace("\n", "").replace(";", "&"))
            if evidence_info:           
                record.info["ISEQ_CIVIC_EVIDENCE_INFO"] = '_'.join(evidence_info) 
                record.info["ISEQ_CIVIC_EVIDENCE_DESCRIPTION"] = '_'.join(evidence_description)
        vcf_writer.write(record)


if __name__ == "__main__":
    main()
