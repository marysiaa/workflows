#!/usr/bin/env python3

import argparse
import json

__version__ = '0.0.1'

def read_json_to_dict(path_to_json):

    with open(path_to_json, "r") as f:
        result = json.load(f)
        return result

def add_errors_and_warnings(dict_with_errors, dict_from_qc_script):
        
    result = []
    for i in range(0, len(dict_from_qc_script)):
        for record in range(0, len(dict_with_errors)):
        
            if dict_from_qc_script[i]["code"] == dict_with_errors[record]["code"]:
            # add warning to the jason if warning field is not "not_provided"
                warning = "null" if dict_with_errors[record]["warning"] == "not_provided" else dict_with_errors[record]["warning"]
            # add error to the json warning field is not "not_provided"
                error = "null" if dict_with_errors[record]["error"] == "not_provided" else dict_with_errors[record]["error"]
                result.append({
                    "description" : dict_with_errors[i]["description"],
                    "value": round(float(dict_from_qc_script[record]["value"]),2),
                    "warning": warning,
                    "error": error, 
                    "comment": dict_with_errors[i]["comment"],
                    "code": dict_with_errors[i]["code"]
            })
                break
    return(result)

def json_dump(dict_json):
    print(json.dumps(dict_json, indent=2))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Takes 2 jsons: output from quality-check.py and template with errors and warnings (can be "not_provided"). Replace "null" in error, warnings to include them later in quality-check-report. ')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument("json_with_errors", help="The template for this json is in file: workflows/src/main/resources/fastq-quality-check/fastq-quality-check-errors-template.json")
    parser.add_argument("json_output_from_qc_script", help="Output from quality-check.py")
    args = parser.parse_args()

    def main():
        errors =  read_json_to_dict(args.json_with_errors)
       
        no_errors = read_json_to_dict(args.json_output_from_qc_script)
        #print(no_errors)
        result = add_errors_and_warnings(errors, no_errors)
        json_dump(result)

    main()


