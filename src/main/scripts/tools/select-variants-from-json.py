#!/user/bin/env python3

import json
import argparse

__version__ = '0.2.0'


def read_json(json_input):
    with open(json_input) as json_file:
        vcf_dict = json.load(json_file)
        return vcf_dict


def get_items_with_key(variant_list, chosen_key):
    return [i for i in variant_list if chosen_key in i.keys() and i[chosen_key]]


def sort_list_by_acmg_score(variant_list):
    list_with_score = get_items_with_key(variant_list, "ISEQ_ACMG_SUMMARY_SCORE")
    return sorted(list_with_score, key=lambda x: x["ISEQ_ACMG_SUMMARY_SCORE"], reverse=True)


def get_all_clinvar_patho(variant_list):
    list_with_clinvar_field = get_items_with_key(variant_list, "ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE")
    patho = [i for i in list_with_clinvar_field if i["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"].lower() in ["pathogenic", "pathogenic_low_penetrance"]]
    patho_likely_patho = [i for i in list_with_clinvar_field if
                          i["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"].lower() == "pathogenic/likely_pathogenic"]
    likely_patho = [i for i in list_with_clinvar_field if
                    i["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"].lower() in ["likely_pathogenic", "likely_pathogenic_low_penetrance"]]
    return patho + patho_likely_patho + likely_patho


def split_list_by_acmg_sig_and_resort(variant_list):
    patho = [i for i in variant_list if i["ISEQ_ACMG_SUMMARY_CLASSIFICATION"].lower() == "pathogenic"]
    likely_patho = [i for i in variant_list if i["ISEQ_ACMG_SUMMARY_CLASSIFICATION"].lower() == "likely^pathogenic"]
    uncertain = [i for i in variant_list if i["ISEQ_ACMG_SUMMARY_CLASSIFICATION"].lower() == "uncertain"]
    other = [i for i in variant_list if i["ISEQ_ACMG_SUMMARY_CLASSIFICATION"].lower() in ["benign", "likely^benign"]]
    final_list = bring_variants_with_inh_match_to_front(patho) + bring_variants_with_inh_match_to_front(
        likely_patho) + bring_variants_with_inh_match_to_front(uncertain) + bring_variants_with_inh_match_to_front(
        other)
    return final_list


def bring_variants_with_inh_match_to_front(variant_list):
    matching = get_items_with_key(variant_list, "ISEQ_GENOTYPE_TO_INHERITANCE_MATCH")
    matching_wo_chet = [i for i in matching if not i["ISEQ_COMPOUND_HET"]]
    # print(matching_wo_chet[0:5])
    rest = [i for i in variant_list if i not in matching_wo_chet]

    return matching_wo_chet + rest


def get_acmg_non_benign(variant_list):
    return [i for i in variant_list if
            i["ISEQ_ACMG_SUMMARY_CLASSIFICATION"].lower() in ["pathogenic", "likely^pathogenic", "uncertain"]]


def add_acmg_variants(clinvar, sorted_acmg, n):
    output_list = clinvar[:]
    for i in sorted_acmg:
        if i not in clinvar:
            output_list.append(i)
    # print("clinvar len: "+ str(clinvar_len))
    restricted_list = output_list[0:n]
    rest = output_list[n:]
    restricted_acmg_sorted = sort_list_by_acmg_score(restricted_list)
    rest_sorted = sort_list_by_acmg_score(rest)
    return restricted_acmg_sorted, rest_sorted


def select_gene_names(variant_list):
    return [i["ISEQ_GENES_NAMES"].split(":")[0] if i["ISEQ_GENES_NAMES"] else None for i in variant_list]


def add_compound_het(good_list, other_list, n):
    new_list = []
    good_genes = select_gene_names(good_list)
    other_genes = select_gene_names(other_list)
    genes = good_genes + other_genes

    comp_het_list = [i["ISEQ_COMPOUND_HET"] if "ISEQ_COMPOUND_HET" in i.keys() else None for i in good_list + other_list]
    # print(comp_het_list)
    comp_het_check_list = [i for i in enumerate(zip(genes, comp_het_list))]
    # print(comp_het_check_list)
    gene_check_list = comp_het_check_list[:len(good_list)]
    i = 0
    for variant in good_list:

        gene = select_gene_names([variant])[0]

        new_comp_het_check_list = comp_het_check_list[i + 1:]
        new_gene_check_list = gene_check_list[i + 1:]
        try:
            com_het = variant["ISEQ_COMPOUND_HET"]
        except KeyError:
            com_het = None

        if variant not in new_list:
            new_list.append(variant)
        if com_het:
            comp_het_indexes = [j[0] for j in new_comp_het_check_list if j[1][0] == gene and j[1][1]]

            if comp_het_indexes:
                for inx in comp_het_indexes:
                    picked_item = (good_list + other_list)[inx]
                    if picked_item and picked_item not in new_list:  # and len(new_list) < n:
                        new_list.append(picked_item)
                        # mark variants that should be shifted in the report
                        # try:
                        #     new_list[-1]
                        #     print(new_list[-1])
                        # except IndexError:
                        #     new_list.append(picked_item)
                        # else:
                        #     if gene == select_gene_names([new_list[-1]])[0]:
                        #         items = list(picked_item.items())
                        #         items.insert(-1, ('INDENT', 'true'))
                        #         picked_item = dict(items)
                        #     new_list.append(picked_item)
        else:
            other_pairs_indexes = [j[0] for j in new_gene_check_list if j[1][0] == gene and not j[1][1]]
            if other_pairs_indexes:
                for inx in other_pairs_indexes:
                    picked_item = good_list[inx]
                    if picked_item and picked_item not in new_list:
                        new_list.append(picked_item)
        i += 1
    return new_list


def sort_sv_variants(variant_list, n):
    list_with_annotsv_rank = get_items_with_key(variant_list, "ACMG_class")
    non_benign = []
    for rank in ["pathogenic", "likely^pathogenic", "uncertain"]:
        picked_list = [i for i in list_with_annotsv_rank if i["ACMG_class"][0].lower() == rank and i["SVTYPE"] != "BND"]
        sorted_picked_list = sorted(picked_list, key=lambda x: x["AnnotSV_ranking_score"], reverse=True)
        non_benign.extend(sorted_picked_list)
    return non_benign[0:n]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Selects variants that should be shown in report.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-json', '-i', type=str, required=True, help='Input json file (with all variants)')
    parser.add_argument('--output-json', '-o', type=str, required=True,
                        help='output json file (with selected variants)')
    parser.add_argument('--variant-number', '-n', type=int, default=50, help='Number of variants to keep')
    parser.add_argument('--sv', '-s', action='store_true', help='Use this flag for json from SV calling')
    parser.add_argument('--somatic', action='store_true', help='Use this flag for json from somatic')
    args = parser.parse_args()

    # all variants
    vcf_list = read_json(args.input_json)
    if args.sv:
        final_list = sort_sv_variants(vcf_list, args.variant_number)
    elif args.somatic:
        vcf_list = vcf_list["variants"]

        # acmg non benign/likely_benign variants
        acmg_list = get_acmg_non_benign(vcf_list)

        # non benign acmg variants sorted by severity
        sorted_acmg_list = sort_list_by_acmg_score(acmg_list)
            
        # all pathogenic/likely_pathogenic clinvar variants
        clinvar_list = get_all_clinvar_patho(vcf_list)

        # final list: all from clinvar  plus the most severe from acmg list
        final_list, remaining_variants = add_acmg_variants(clinvar_list, sorted_acmg_list, args.variant_number)
    else:
        vcf_list = vcf_list["variants"]

        # acmg non benign/likely_benign variants
        acmg_list = get_acmg_non_benign(vcf_list)

        # non benign acmg variants sorted by severity
        sorted_acmg_list = sort_list_by_acmg_score(acmg_list)

        # all pathogenic/likely_pathogenic clinvar variants
        clinvar_list = get_all_clinvar_patho(vcf_list)

        # final list: all from clinvar  plus the most severe from acmg list
        acmg_and_clinvar, remaining_variants = add_acmg_variants(clinvar_list, sorted_acmg_list, args.variant_number)

        # bring variants with matching inheritance at the beginning of each acmg category
        good_inheritance_first_list = split_list_by_acmg_sig_and_resort(acmg_and_clinvar)

        # keep compound het and other variants within the same gene together
        final_list = add_compound_het(good_inheritance_first_list, remaining_variants, args.variant_number)

    with open(args.output_json, "w") as f:
        t = json.dumps(final_list)
        f.write(t)
