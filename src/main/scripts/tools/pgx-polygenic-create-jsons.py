#!/usr/bin/env python3

import argparse
import json
import os


__version__ = "0.0.2"


def openpgx_genotypes(match, openpgx_dict, model):
    splitted = match.split("_")
    if "*" in splitted[0]:
        first_allele = "*" + splitted[0].split('.')[0].split("*")[1]
        second_allele = "*" + splitted[1].split('.')[0].split("*")[1]      
        openpgx_dict[model] = first_allele + "/" +  second_allele


def pharmgkb_genotypes(match, pharmgkb_dict):
    splitted = match.split("_")
    model = splitted[0].split('.')[0]
    if "*" in splitted[0]:
        first_allele = "*" + splitted[0].split('.')[0].split("*")[1]
        second_allele = "*" + splitted[1].split('.')[0].split("*")[1]
        pharmgkb_dict[model] = first_allele + "/" +  second_allele


def iterate_over_files(files):
    all_models = dict()
    openpgx_dict = dict()
    pharmgkb_dict = dict()
    for filename in files:
        with open(filename.path, "r") as f:
            file = json.load(f)
        model = file["description"]["model_name"].upper()
        all_models[model] = file
        matching_haplotypes = file["haplotype_model"]["haplotypes"]["matching_haplotypes"]
        if len(matching_haplotypes) == 1:
            openpgx_genotypes(matching_haplotypes[0], openpgx_dict, model)
            pharmgkb_genotypes(matching_haplotypes[0], pharmgkb_dict)
        else:
            openpgx_dict[model] = "An ambiguous result from polygenic"
            pharmgkb_dict[model] = "An ambiguous result from polygenic"       
    return all_models, openpgx_dict, pharmgkb_dict


def write_json(output_directory, filename, data):
    with open(os.path.join(output_directory, filename), "w") as f:
        json.dump(data, f, indent=2)


def main():
    parser = argparse.ArgumentParser(description='Create JSONs for OpenPGx and PharmGKB queries and merge all input models')
    parser.add_argument('--input-directory', type=str, required=True, help='Input directory with polygenic results')
    parser.add_argument('--output-directory', type=str, required=True, help='Output directory for JSONs')
    args = parser.parse_args()

    # Iterate over all files in the input directory
    merged_models, openpgx_queries, pharmgkb_queries = iterate_over_files(os.scandir(args.input_directory))

    # Write merged models
    write_json(args.output_directory, "merged_models.json", merged_models)

    # Write OpenPGx queries
    write_json(args.output_directory, "openpgx_queries.json", openpgx_queries)

    # Write PharmGKB queries
    write_json(args.output_directory, "pharmgkb_queries.json", pharmgkb_queries)


if __name__ == '__main__':
    main()
