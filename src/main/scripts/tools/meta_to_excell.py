#!/usr/bin/env python3

import re
import argparse
import urllib.request
from typing import Dict
from typing import Tuple
from openpyxl.workbook import Workbook

__version__ = '0.0.1'


def get_text_starting_with_meta(txt:str) -> str:
    return re.search('(meta\s*\{.*})', txt, re.DOTALL).group(1)


def get_meta(txt_starting_with_meta:str) -> str:
    '''
    Warning: does not deal with incorrectly formatted meta
    :param txt_starting_with_meta: part of wdl containing 'meta' section, starting with 'meta' and containing additional text at the end
    :return: part of wdl containing 'meta'
    '''
    current_unmatched_right_parentheses = 0
    start = txt_starting_with_meta.index('{')
    for i, c in enumerate(txt_starting_with_meta[start:]):
        if c == '{':
            current_unmatched_right_parentheses += 1
        elif c == '}':
            current_unmatched_right_parentheses -= 1
            if current_unmatched_right_parentheses == 0:
                return txt_starting_with_meta[start:start+i+1]
    raise RuntimeError('Sth is wrong with your meta: \n{}'.format(txt_starting_with_meta[start:]))


def get_wdl_content(url:str) -> str:
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'})
    with urllib.request.urlopen(req) as response:
        return response.read().decode()


def get_wdl_name(url:str) -> str:
    return url.rsplit('/', 1)[-1].split('.')[0]


def parse_meta(meta:str)->Dict[str, str]:
    stripped_meta = meta.lstrip('{').rstrip('}').strip()
    ret = {}
    for line in stripped_meta.split('\n'):
        if not line.strip():
            continue
        k, v = parse_line_in_meta(line)
        if k.startswith('input_') or k.startswith('output_'):
            ret[k] = eval(v.strip('"').strip("'"))
        else:
            ret[k] = v.strip('"').strip("'")
    return ret


def parse_line_in_meta(line:str) -> Tuple[str]:
    return re.search("""(\w+)\s*:\s*(['"].+['"])""", line).groups()


def write_excell(in_dict:Dict[str, str], name:str, out_path:str) -> None:
    wb = Workbook()
    ws = wb.active
    ws.cell(row=1, column=1, value='WDL nazwa programistycznav')
    ws.cell(row=1, column=2, value='WDL nazwa dla użytkownika')
    ws.cell(row=1, column=3, value='WDL opis dla użytkownika')
    ws.cell(row=1, column=4, value='Input/output nazwa programistyczna ')
    ws.cell(row=1, column=5, value='Input/output nazwa dla użytkownika')
    ws.cell(row=1, column=6, value='Input/output opis dla użytkownika')
    ws.cell(row=1, column=7, value='Input/output widoczność dla użytkownika')
    ws.cell(row=2, column=1, value=name)
    ws.cell(row=2, column=2, value=in_dict['name'])
    ws.cell(row=2, column=3, value=in_dict['description'])
    in_out_keys = sorted([key for key in in_dict.keys() if key.startswith('input_') or key.startswith('output_')])
    for i, key in enumerate(in_out_keys, 2):
        ws.cell(row=i, column=4, value=key.replace('input_', '').replace('output_', ''))
        ws.cell(row=i, column=5, value=in_dict[key]['name'])
        ws.cell(row=i, column=6, value=in_dict[key]['description'])
        try:
            ws.cell(row=i, column=7, value='' if in_dict[key]['copy'] else 'hidden')
        except KeyError:
            pass
    for col in range(1, 4):
        ws.merge_cells(start_row=2, start_column=col, end_row=i, end_column=col)
    wb.save(out_path)


def main(in_url:str, out_path:str) -> None:
    wdl_content = get_wdl_content(in_url)
    wdl_name = get_wdl_name(in_url)
    text_starting_with_meta = get_text_starting_with_meta(wdl_content)
    meta = get_meta(text_starting_with_meta)
    parsed_meta = parse_meta(meta)
    write_excell(parsed_meta, wdl_name, out_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Converts 'meta' section of valid WDL to Excell")
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input_url', type=str, help='URL of the raw wdl that is about to be processed')
    parser.add_argument('-o', '--output_path', type=str, help='Output file location')
    arguments = parser.parse_args()
    main(arguments.input_url, arguments.output_path)
