#!/usr/bin/env python3

import os
import argparse
import sys
from typing import List
from typing import Tuple

__version__ = '0.0.4'


def check_element_uniqueness(to_be_validated: List) -> None:
    if len(set(to_be_validated)) != len(to_be_validated):
        raise RuntimeError(f"Repeating elements in list {to_be_validated}")


def replace_first_appearance_of_substring(in_string: str, substring: str, replacement: str) -> str:
    index = in_string.index(substring)
    if index < 0:
        raise RuntimeError(f"Couldn't find {substring} in {in_string}")
    return '{}{}{}'.format(in_string[:index], replacement, in_string[index + len(replacement):])


def validate_suffixes(right_suffixes: List[str], left_suffixes: List[str]) -> str:
    for i, right_suffix in enumerate(right_suffixes):
        assert left_suffixes[i] == replace_first_appearance_of_substring(right_suffix, '2', '1')


def make_pairs(f_names: List[str], possible_left_suffixes: List[str], possible_right_suffixes: List[str]) -> Tuple[
    List[str]]:
    f_names.sort()
    validate_suffixes(possible_right_suffixes, possible_left_suffixes)
    left_elements = []
    right_elements = []
    while f_names:
        name = f_names[0]
        found = False
        for i, left_suffix in enumerate(possible_left_suffixes):
            if name.endswith(left_suffix):
                left_elements.append(name)
                f_names.remove(name)
                right_name = construct_right_path(name, left_suffix, possible_right_suffixes[i], f_names)
                right_elements.append(right_name)
                f_names.remove(right_name)
                found = True
                break
        if not found:
            if not any(name.endswith(right_suffix) for right_suffix in possible_right_suffixes):
                print("Extension of the provided fastq files is not in our possible extensions list, consider adding it")
                print("Task failed with error code 86: Invalid fastq file extension;", file=sys.stderr)
                exit(86)
    return left_elements, right_elements


def construct_right_path(left_path:str, left_suffix:str, right_suffix:str, all_paths:List[str]) -> str:
    right_name = os.path.basename(left_path).replace(left_suffix, right_suffix)
    for path in all_paths:
        if path.endswith(right_name):
            return path
    raise RuntimeError(f"Couldn't find {right_name} in available paths")


def hardlink_files(paths: List[str], directory) -> None:
    try:
        os.makedirs(directory)
    except OSError:
        pass
    for path in paths:
        os.link(path, os.path.join(directory, os.path.basename(path).replace('fastq', 'fq')))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Make pairs (if needed), return sample names')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--fastqs', required=True, nargs='+')
    parser.add_argument('--possible_left_suffixes', nargs='+',
                        default=['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz'])
    parser.add_argument('--possible_right_suffixes', nargs='+',
                        default=['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz'])
    parser.add_argument('--outdir1', type=str, default='/fastqs_1')
    parser.add_argument('--outdir2', type=str, default='/fastqs_2')

    arguments = parser.parse_args()

    possible_left_suffixes = sorted(arguments.possible_left_suffixes, key=lambda x: len(x), reverse=True)

    assert len(arguments.possible_left_suffixes) == len(arguments.possible_right_suffixes)
    check_element_uniqueness(arguments.possible_left_suffixes)
    check_element_uniqueness(arguments.possible_right_suffixes)
    possible_right_suffixes = sorted(arguments.possible_right_suffixes, key=lambda x: len(x), reverse=True)
    left_read_files, right_read_files = make_pairs(arguments.fastqs, possible_left_suffixes, possible_right_suffixes)
    hardlink_files(right_read_files, arguments.outdir2)
    hardlink_files(left_read_files, arguments.outdir1)


