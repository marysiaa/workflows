#!/usr/bin/python3

import json
import argparse
import pysam

__version__ = '1.0.3'
AUTHOR = "gitlab.com/MateuszMarynowski"

parser = argparse.ArgumentParser(description='Annotate vcf with disease names derived from Genetics Home Reference')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf file')
parser.add_argument('--input-json', '-j', metavar='input_json', type=str, required=True, help='input json file')
parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf file')
parser.add_argument('--sv', action='store_true', help='Give this flag if the input vcf is a SV file')

args = parser.parse_args()


def main():

    with open(args.input_json, 'r') as f:
        ghr_summaries_dict = json.load(f)

    gene_symbol = []
    for i in range(len(ghr_summaries_dict["summaries"]["gene-summary"])):
        gene_symbol.append(ghr_summaries_dict["summaries"]["gene-summary"][i]['gene-symbol'])

    mydict = {}
    for i in range(len(ghr_summaries_dict["summaries"]["gene-summary"])):
        try:
            diseases = ghr_summaries_dict["summaries"]["gene-summary"][i]["related-health-condition-list"]["related-health-condition"]["name"]
            diseases = diseases.replace(" ", "^")
        except:
            try:
                diseases = []
                for j in range(len(ghr_summaries_dict["summaries"]["gene-summary"][i]["related-health-condition-list"]["related-health-condition"])):
                    diseases.append(ghr_summaries_dict["summaries"]["gene-summary"][i]["related-health-condition-list"]["related-health-condition"][j]["name"])
                    diseases[j] = diseases[j].replace(" ", "^")
            except:
                diseases = None
        finally:
            mydict[gene_symbol[i]] = diseases

    vcf_reader = pysam.VariantFile(filename=args.input_vcf)
    vcf_reader.header.info.add("ISEQ_DISEASE_GHR", "1", "String",
                               "Genes annotated with disease names derived from Genetics Home Reference")

    vcf_writer = pysam.VariantFile(args.output_vcf, 'w',
                                   header=vcf_reader.header)

    for record in vcf_reader.fetch():
        try:
            if args.sv:
                genes=record.info["Gene_name"][0].split("|")
                diseases = []
                for gene in genes:
                    if gene in mydict:
                        if type(mydict[gene]) == list:
                            diseases.append('_'.join(mydict[gene]))
                        elif type(mydict[gene]) == str:    
                            diseases.append(mydict[gene])
                        else:  
                            diseases.append('.')        
                    else:
                        diseases.append('.')
                check = bool([i for i in diseases if i != '.'])
                if  check:
                    record.info["ISEQ_DISEASE_GHR"] = '|'.join(diseases)  
                else:  
                    continue  
            else:                 
                splitted_for_colon = record.info["ISEQ_GENES_NAMES"][0].split(":")[0]
                if splitted_for_colon in mydict:
                    if type(mydict[splitted_for_colon]) == list:
                        record.info["ISEQ_DISEASE_GHR"] = '_'.join(mydict[splitted_for_colon])
                    elif type(mydict[splitted_for_colon]) == str:
                        record.info["ISEQ_DISEASE_GHR"] = mydict[splitted_for_colon]
                    else:
                        continue
                else:
                    continue 
        except:
            continue
        finally:
            vcf_writer.write(record)
        


if __name__ == "__main__":
    main()
