#!/usr/bin/env python3

import argparse
import json


__version__ = "0.0.1"


def load_json(filename: str) -> dict:
    with open (filename, "r") as f:
        return json.load(f)


def write_json(filename: str, data: dict):
    with open(filename, "w") as f:
        json.dump(data, f, indent=2)


def add_variant_anno(main_dict: dict, variant_annotation: dict):
    for gene, variants in variant_annotation.items():
        for variant, infos in variants.items():
            for info in infos:
                for drug in info["Drug(s)"]:
                    if main_dict.get(gene, None):
                        if main_dict[gene].get(drug, None):
                            main_dict[gene][drug]["variant_annotation"].append(info)
                        else:
                            main_dict[gene][drug] = {"variant_annotation": [info]}
                    else:
                        main_dict[gene] = {drug: {"variant_annotation": [info]}}


def add_clinical_anno(main_dict: dict, clinical_annotation: dict) -> dict:
    for gene, variants in clinical_annotation.items():
        for variant, infos in variants.items():
            for info in infos:
                for drug in info["Drug(s)"]:
                    if main_dict.get(gene, None):
                        if main_dict[gene].get(drug, None):
                            if main_dict[gene][drug].get("clinical_annotation", None):
                                main_dict[gene][drug]["clinical_annotation"].append(info)
                            else:
                                main_dict[gene][drug]["clinical_annotation"] = [info]
                        else:
                            main_dict[gene][drug] = {"clinical_annotation": [info]}
                    else:
                        main_dict[gene] = {drug: {"clinical_annotation": [info]}}


def add_openpgx_anno(main_dict: dict, openpgx: dict):
    for drug, databases in openpgx.items():
        for database, info in databases.items():
            info["database"] = database
            for gene, variant in info["factors"].items():
                if main_dict.get(gene, None):
                    if main_dict[gene].get(drug, None):
                        if main_dict[gene][drug].get("openpgx", None):
                            main_dict[gene][drug]["openpgx"].append(info)
                        else:
                            main_dict[gene][drug]["openpgx"] = [info]
                    else:
                        main_dict[gene][drug] = {"openpgx": [info]}
                else:
                    main_dict[gene] = {drug: {"openpgx": [info]}}    


def merge_anno(args) -> dict:
    merged = dict()
    add_variant_anno(merged, load_json(args.pharmgkb_variant_anno))
    add_clinical_anno(merged, load_json(args.pharmgkb_clinical_anno))
    add_openpgx_anno(merged, load_json(args.openpgx_anno))
    return merged


def main():
    parser = argparse.ArgumentParser(description='Merge annotation files')
    parser.add_argument('--openpgx-anno', type=str, required=True, help='OpenPGx recommendations')
    parser.add_argument('--pharmgkb-clinical-anno', type=str, required=True, help='PharmGKB clinical recommendations')
    parser.add_argument('--pharmgkb-variant-anno', type=str, required=True, help='PharmGKB variant recommendations')
    parser.add_argument('--output-filename', type=str, required=True, help='Output filename')
    args = parser.parse_args()

    # Merge annotations
    merged = merge_anno(args)

    # Write output
    write_json(args.output_filename, merged)


if __name__ == "__main__":
    main()
