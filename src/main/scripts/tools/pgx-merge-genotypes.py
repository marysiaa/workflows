#!/usr/bin/env python3

import argparse
import csv
import json
from pathlib import Path
import requests
import re
from urllib.parse import quote_plus

__version__ = '1.0.0'

# load data
def load_json(path=None) -> dict:
    if path != None:
        with open(path) as f:
            return json.load(f)
    else:
        return None
    return {}

def load_tsv(path=None) -> list:
    result = []
    if path != None:
        with open(path,'r') as data:
            for line in csv.reader(data, delimiter="\t"):
                if line[0].startswith('#'):
                    continue
                result.append(line)
    else:
        return None
    return result

# data to list of dictionaries
def cyrius_to_dict(cyrius_list=None) -> list:
    d = {}
    print(cyrius_list)
    if cyrius_list == None or cyrius_list == []:
        return d
    d["tool"] = "cyrius"
    d["gene"] = "CYP2D6"
    d["diplotype"] = cyrius_list[1][1]
    return remove_duplicates_from_list([d])

def pharmcat_to_dict(pharmcat_dict=None) -> list:
    if pharmcat_dict == None:
        return []
    result = []
    for i in pharmcat_dict["geneCalls"]:
        d ={}
        d["tool"] = "pharmcat"
        d["gene"] = i["gene"]
        if type(i["diplotypes"]) == list:
            if len(i["diplotypes"]) == 2 and i["diplotypes"][0] == i["diplotypes"][1]:
                d["diplotype"] = i["diplotypes"][0]
            if len(i["diplotypes"]) > 2:
                d["diplotype"] = i["diplotypes"]
            if len(i["diplotypes"]) == 1:
                d["diplotype"] =i["diplotypes"][0]
            result.append(d)
        else:
            raise Exception(f"Diplotype in pharmcat json for gene {str(i['gene'])} is not in proper format: " + str(i["diplotypes"]))

    UGT1A1 = [i for i in result if i["gene"] == "UGT1A1"][0]
    result.remove(UGT1A1)
    # UGT1A1 is the only gene with metadata extracted from pharmcat report instead of CPIC API, because of difficulty  with genotype -> phenotype convertion
    for i in pharmcat_dict["genotypes"]:
        if i["gene"] == "UGT1A1":
            UGT1A1["phenotype"] = i["phenotype"][0]
            UGT1A1["activity_score"] = "no_data"
            UGT1A1["consultation_info"] = i["functions"][0]
            UGT1A1["lookupkey"] = {"UGT1A1": i["phenotype"][0]}
            result.append(UGT1A1)
        else:
            next
    return result

def extract_stars(diplotype: str) -> str:
    result = diplotype.split("/")
    return "/".join([i[int(i.find("*")):] for i in result])

def astrolabe_to_dict(astrolabe_list=None) -> list:
    if astrolabe_list == None:
        return []
    result = []
    for i in astrolabe_list:
        d = {}
        d["tool"] = "astrolabe"
        d["gene"] = i[0]
        d["diplotype"] = extract_stars(i[1])
        d["notes"] = i[3]
        result.append(d)
    return result

def aldy_to_dict(aldy_list=None) -> list:
    if aldy_list == None:
        return []
    result = []
    for i in aldy_list:
        print(i)
        if len(i) == 1:
            i = i[0].split(" ")
        d = {}
        d["tool"] = "aldy"
        d["gene"] = i[1]
        d["diplotype"] = i[2]
        d["notes"] = ""
        result.append(d)
    return remove_duplicates_from_list(result)


not_called = ["None", "not called", "./.", "No CPIC variants found", "No CPIC decreased or no function variant with strong or moderate evidence found"]

def standarize_diplotype(record: dict) -> dict:
    if record["gene"] == "UGT1A1":
        return record
    if type(record["diplotype"]) == list:
        return record
    if record["diplotype"] in not_called or record["diplotype"] == None:
        record["diplotype"] = None
        return record
    if type(record["diplotype"]) == str:
        gene = record["gene"]
        result = [standarize_haplotype(haplotype, gene="G6PD") for haplotype in record["diplotype"].split("/")]
        record["raw_call"] = record["diplotype"]
        record["diplotype"] = f"{result[0]}/{result[1]}"
        return record
    raise Exception(f"Unproper type of diplotype {record['diplotype']}: {type(record['diplotype'])}")

def standarize_haplotype(haplotype: str, gene=None) -> str:
    # for aldy CYP2D6 monsters: '*1+*1+*1+*1, *16+*1
    if re.match("^(\*\d{1,3}\+)*\*\d{1,3}$", haplotype):
        return haplotype
    # for aldy CYP2C19 *1B
    if re.match("^\*\d{1,3}[A-Z]$", haplotype):
        return re.sub('[A-Z]', '', haplotype)
    # for example -1639G/-1639G
    if re.match("^-?[0-9]+[ACTG]/-?[0-9]+[ACTG]$", haplotype):
        return haplotype
    # for diplotype as rs ID instead of star nomenclature
    if re.match("^(rs[A-Z0-9]\+?)+$", haplotype):
        return haplotype
    # for proper diplotype nomenclature
    if re.match("^\*\d{1,3}$", haplotype):
        return haplotype
    # e.g. *1+rs11045818+rs4149057+rs2291075
    if re.match("^\*\d{1,3}(\+\d{1,3})*(\+rs\d{1,10})*$", haplotype):
        return re.findall('\*\d{1,3}', haplotype)[0]
    if gene == "G6PD" and haplotype == "*B":
        return "B (wildtype)"
    if type(haplotype) == str:
        return haplotype
    raise Exception(f"Not known nomenclature for dipltoype {record['diplotype']}")

assert standarize_haplotype("*1+rs11045818+rs4149057+rs2291075") == "*1"
assert standarize_haplotype("rs11045818+rs4149057+rs2291075") == "rs11045818+rs4149057+rs2291075"
assert standarize_haplotype("*1B") == "*1"
assert standarize_haplotype("*1+*2") == "*1+*2"
assert standarize_haplotype("-1639G") == "-1639G"
assert standarize_haplotype("*B", gene="G6PD") == "B (wildtype)"

def remove_duplicates_from_list(result: list) -> list:
    res_list = []
    for i in range(len(result)):
        if result[i] not in result[i + 1:]:
            res_list.append(result[i])
    return res_list

# combine data
def combine_dicts(results: list) -> list:
    mergedlist = []
    for i in results:
        mergedlist.extend(i)
    return [request_CPIC_phenotype(standarize_diplotype(record)) for record in mergedlist]

# Use CPIC API to extract phenotype data for each gene
def request_CPIC_phenotype(record: dict) -> dict:
    gene = record["gene"]
    diplotype = record["diplotype"]
    if gene == "UGT1A1":
        return record
    if diplotype == None or type(diplotype) == list:
        return record
    diplotype = quote_plus(diplotype, safe="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*/()")
    address = f"https://api.cpicpgx.org/v1/diplotype?genesymbol=eq.{gene}&diplotype=eq.{diplotype}"
    record["address"] = address
    data_dict  = json.loads(requests.get(address).text)
    if data_dict == []:
        record["phenotype"] = f"There is no phenotype data for {gene} gene and {diplotype} diplotype in CPIC database."
        record["activity_score"] = "no_data"
        record["consultation_info"] = "no_data"
        record["lookupkey"] = "no_data"
        return record
    record["phenotype"] = data_dict[0]["generesult"]
    record["activity_score"] = data_dict[0]["totalactivityscore"]
    record["consultation_info"] = data_dict[0]["consultationtext"]
    record["lookupkey"] = data_dict[0]["lookupkey"]
    return record

def combine_same_results(dicts: list) -> dict:
    combined = {}

    for d in dicts:
        index = d["gene"] + "::" + d["diplotype"]

        if not index in combined:
            combined[index] = index

# annotate data with drug-gene reccomendations

# save outputs
def save_json(dicts, sample_id):
    out = f'{sample_id}_merged-diplotypes.json'
    with open(out, 'w') as final_json:
        json.dump(dicts, final_json, indent=2)

def save_tsv(dicts, sample_id):
    out = f'{sample_id}_merged-diplotypes.tsv'
    csv_columns =["tool", "gene", "diplotype", "phenotype", "activity_score", "consultation_info", "lookupkey",  "notes"]
    csv_file = out
    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns, delimiter='\t', extrasaction='ignore')
            writer.writeheader()
            for data in dicts:
                writer.writerow(data)

    except IOError:
        print("I/O error")

assert extract_stars("CYP2D6*4/CYP2D6*10") == "*4/*10"
assert request_CPIC_phenotype({'tool': 'pharmcat', 'gene': 'CYP2C19', 'diplotype': '*17/*17', 'notes': ''})["phenotype"]  == "Ultrarapid Metabolizer"
#assert request_CPIC_phenotype({'tool': 'pharmcat', 'gene': 'WRONG_GENE', 'diplotype': '*17/*17', 'notes': ''})["phenotype"] == "There is no data for WRONG_GENE *17/*17 in CPIC database"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to merge data from all pgx tools in pharmacogenomics wdl module. Tools: cyrius, astrolabe, pharmcat, aldy. Every tool gives different output format. Before merge needs to be process to one d, extract some fields that are different in every output. ')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    #INPUTS
    parser.add_argument('-c', '--cyrius', help="Output from cyrius tool.")
    parser.add_argument('-p', '--pharmcat', help="Output from pharmcat tool, report.json.")
    parser.add_argument('-s', '--astrolabe', help="Output from astrolbe tool - astrolaba out.")
    parser.add_argument('-l', '--aldy', help="Output from aldy tool.")

    #OUTPUTS
    parser.add_argument('-i', '--sample_id', help="Sample_id.")
    args = parser.parse_args()

    #DO
    dicts = [
        pharmcat_to_dict(load_json(args.pharmcat)),
        astrolabe_to_dict(load_tsv(args.astrolabe)),
        aldy_to_dict(load_tsv(args.aldy)),
        cyrius_to_dict(load_tsv(args.cyrius))
        ]

    print("START")
    combined = combine_dicts(dicts)
    items = ["gene", "lookupkey", "diplotype", "phenotype", "consultation", "address", "raw_call", "tool"]
    count = 0
    for record in combined:
        print(count)
        for item in items:
            if item in record:
                print(str(item) + ": " + str(record[item]))
        count = count +1
    save_json(combined, args.sample_id)
    save_tsv(combined, args.sample_id)


