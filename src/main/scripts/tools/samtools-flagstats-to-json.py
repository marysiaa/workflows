import json
import argparse
from typing import Dict, List

__version__ = '0.0.1'


def read_line(line: str) -> Dict:
    line_dict = {}
    splitted_line = line.strip().replace("(mapQ>=5)", "[mapQ>=5]").split(maxsplit=3)
    passed, failed = int(splitted_line[0]), int(splitted_line[2])
    name = splitted_line[3].split("(")[0].strip().capitalize()
    line_dict["name"] = name
    line_dict["passed"] = passed
    line_dict["failed"] = failed
    return line_dict


def calculate_percent(total_list: List, other_dict: Dict) -> Dict:
    other_percent_dict = {"name": other_dict["name"] + " [%]"}
    try:
        other_percent_dict["passed"] = (other_dict["passed"] / total_list[0])*100
    except ZeroDivisionError:
        other_percent_dict["passed"] = ""
    try:
        other_percent_dict["failed"] = (int(other_dict["failed"]) / int(total_list[1]))*100
    except ZeroDivisionError:
        other_percent_dict["failed"] = ""
    return other_percent_dict


def get_good_dict(dictionary_list: List, name: str) -> Dict:
    good_dicts = [item for item in dictionary_list if item["name"] == name]
    try:
        return good_dicts[0]
    except IndexError:
        return {}


def read_file(in_file: str, out_file: str,  percent_list: List, warning_dictionary: Dict):
    initial_list = []
    total = [0, 0]
    for line in open(in_file):
        line_dict = read_line(line)
        if line_dict["name"] == "In total":
            total[0] += line_dict["passed"]
            total[1] += line_dict["failed"]
        initial_list.append(line_dict)
    for element in percent_list:
        element_dict = get_good_dict(initial_list, element)
        if element_dict != {}:
            initial_list.append(calculate_percent(total, element_dict))
    final_list = [add_description_and_warnings(i,warning_dictionary) for i in initial_list]
    out = json.dumps(final_list)
    
    with open(out_file, "w") as f1:
        f1.write(out)

def add_description_and_warnings(element_dict, warning_dict):
    stat_name = element_dict["name"]
    try:
        element_dict["description"] = warning_dict[stat_name][0] if warning_dict[stat_name][0] != "" else element_dict["name"]
        element_dict["comments"] = warning_dict[stat_name][3]
        try:
            element_dict["warning"] = float(warning_dict[stat_name][1])
            element_dict["error"] = float(warning_dict[stat_name][2])

        except ValueError:
            element_dict["warning"] = ""
            element_dict["error"] = ""

    except KeyError:
        element_dict["description"] = element_dict["name"]
        element_dict["warning"] = ""
        element_dict["error"] = ""
        element_dict["comments"] = ""
    return element_dict    


def read_warning_file(warning_file:str) -> Dict:
    warning_dictionary ={}
    with open(warning_file) as f:
        header = f.readline().lower().strip().split("\t")
        #print(header)
        description_index = header.index("description")
        warnings_index = header.index("warnings")
        errors_index = header.index("errors")
        comments_index = header.index("comments")
        #next(f)
        for line in f:
            print(line)
            line_proc = line.strip().split("\t")
            warning_dictionary[line_proc[0].capitalize()] = [line_proc[description_index], line_proc[warnings_index], line_proc[errors_index],line_proc[comments_index]]
    return warning_dictionary


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Changes samtools flagstat output file to json, calculates percentages')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--percent', '-p', nargs='*', type=str,
                        help='Name (or names) of mapping characteristic for which percent should \
                        be calculated. Spaces must be converted to "_", for example: "properly_paired singletons"; \
                        if set as "all" percent will be calculated for all statistics, except read1 and read2')
    parser.add_argument('--input', '-i', type=str, required=True, help='Input file with statistics from samtools flagstat')
    parser.add_argument('--output', '-o', type=str, required=True, help='Output json file')
    parser.add_argument('--warnings', '-w', nargs="?", type=str, help='Input file with statistics descriptions and warnings')
    args = parser.parse_args()
    with_percent = args.percent
    #with_percent = ["mapped", "properly_paired", "singletons", "with_mate_mapped_to_a_different_chr", "duplicates"]
    if args.percent:
        if args.percent[0] == "all":
            with_percent_final =["In total", "Mapped", "Secondary", "Supplementary", "Duplicates", "Paired in sequencing",
                                 "Properly paired", "With itself and mate mapped", "Singletons", "With mate mapped to a different chr",
                                 "With mate mapped to a different chr [mapQ>=5]"]
        else:
            with_percent_final = [i.replace("_", " ").capitalize() for i in with_percent]
    else:
        with_percent_final = []
    if args.warnings:
        warning_dict = read_warning_file(args.warnings)
    else:
        warning_dict = {}
    #print(warning_dict)
    read_file(args.input, args.output, with_percent_final, warning_dict)