#!/usr/bin/python3
import os
import pandas as pd
import argparse

__version__ = "0.0.2"
AUTHOR="https://gitlab.com/olaf.tomaszewski"

parser = argparse.ArgumentParser(description='Translate report from English to Polish')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-xlsx', '-x', metavar='input_xlsx', type=str, required=True, help='input xlsx file containing translation')
parser.add_argument('--input-jinja', '-j', metavar='input_jinja', type=str, required=True, help='input directory containing jinja files')
parser.add_argument('--output-jinja', '-o', metavar='output_jinja', type=str, required=True, help='output directory containing jinja files')
args = parser.parse_args()


def get_english_polish_dict(excel_path):
    english_polish_df = pd.read_excel(excel_path, sheet_name=0, index_col=None, na_values=['NA'], usecols='A:B')
    english_polish_df = english_polish_df.dropna()
    english_polish_np = english_polish_df.to_numpy()
    english_polish_dict = {}

    for eng_word, pol_word in english_polish_np:
        english_polish_dict[eng_word]= pol_word
    return english_polish_dict


def translate(text, english_polish_df):
    for key in english_polish_df.keys():
        text = text.replace(key, english_polish_df[key])
    return text


def save_to_file(filename, text):
    report_pol_dir = args.output_jinja
    save_path = os.path.join(report_pol_dir, filename)
    with open(save_path, 'w') as file:
        file.write(text)


def read_and_translate_all_files_in_directory(report_eng_directory_path, english_polish_dict):
    for filename in os.listdir(report_eng_directory_path):
        if filename.endswith(".jinja"):
            abs_path = os.path.join(report_eng_directory_path, filename)
            with open(abs_path, 'r') as file:
                text = file.read()
                text = translate(text, english_polish_dict)
                save_to_file(filename, text)


if __name__ == "__main__":
    excel_path = args.input_xlsx
    english_polish_dict = get_english_polish_dict(excel_path)
    report_eng_directory_path = args.input_jinja
    read_and_translate_all_files_in_directory(report_eng_directory_path, english_polish_dict)
