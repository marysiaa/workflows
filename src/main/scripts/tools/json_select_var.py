#!/usr/bin/python3
"""Select variants from JSON based on variants list"""

import json
import argparse
from typing import List, Dict


__version__ = '0.0.1'


def load_json(json_path: str) -> List[Dict]:
    """Load JSON

    Args:
        json_path (str): Path to json file

    Returns:
        list: Loaded JSON file
    """
    with open(json_path, 'r', encoding="UTF-8") as json_file:
        return json.load(json_file)


def save_json(json_path: str, json_data: List[Dict]):
    """Save JSON

    Args:
        json_path (str): Path to json file
        json_data (json): JSON data to save
    """
    with open(json_path, 'w', encoding="UTF-8") as json_file:
        json.dump(json_data, json_file, indent=2)


def select_variants(variants: List[Dict], variant_list: list) -> List[Dict]:
    """Select variants from JSON based on variants list

    Args:
        variants (dict): Dict with variants
        variant_list (list): List of variants to select

    Returns:
        list: Selected list of variants from JSON file
    """
    return [variant for variant in variants if variant["ISEQ_GNOMAD_ID"] in variant_list]


def main():
    """Main function"""
    parser = argparse.ArgumentParser(description="Select variants from JSON based on variants list")
    parser.add_argument("--variants-json", help="Input JSON file with variants", required=True)
    parser.add_argument("--variants-list", help="Variants list", required=True)
    parser.add_argument("--output-filename", help="Output JSON file", required=True)
    parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')

    args = parser.parse_args()

    variants = load_json(args.variants_json)
    selected_variants = select_variants(variants["variants"], args.variants_list.replace(" ", "").split(";"))
    variants["variants"] = selected_variants
    save_json(args.output_filename, variants)


if __name__ == "__main__":
    main()
