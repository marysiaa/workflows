#!/usr/bin/python3

import argparse
import pysam
import json

__version__ = '2.1.1'
AUTHOR = "gitlab.com/MateuszMarynowski"

    
def index_panels_by_gene(table: dict) -> dict:
    result = {}
    for panel in table:
        panel["panel_name"] = panel["panel_name"].replace(" ", "^")
        for gene in panel["genes"]:
            if gene in result:
                result[gene] = result[gene] + "_" + panel["panel_name"]
            else:
                result[gene] = panel["panel_name"]
    return result


def load_json(json_path: str) -> dict:
    with open(json_path) as f:
        return json.load(f)


def get_only_genes_names(panels):
    for panel in panels:
        if panel['panel_name'] == "ACMG Incidental Findings":
            panel.clear()
            continue
        genes = []
        for item in panel['genes']:
            genes.append(item['name'])
        panel['genes'] = genes
    return list(filter(None, panels))

    
def process_vcf(panels_by_gene: dict, input_vcf: str, output_vcf: str):
    vcf_reader = pysam.VariantFile(filename=input_vcf)
    vcf_reader.header.info.add("ISEQ_GE_PANELS", "1", "String",
                               "Panels from Genomics England that contain gene affected by the variant")
    
    vcf_writer = pysam.VariantFile(output_vcf, 'w',
                                   header=vcf_reader.header)
    
    for record in vcf_reader.fetch():
        try:
            gene_name = record.info["ISEQ_GENES_NAMES"][0]
            if gene_name in panels_by_gene:
                panel = panels_by_gene[gene_name]
                record.info["ISEQ_GE_PANELS"] = panel
            else:
                continue
        except:
            continue
        finally:
            vcf_writer.write(record)
    

def main(panels_json: str, input_vcf: str, output_vcf: str):
    panels = load_json(panels_json)
    panels = get_only_genes_names(panels)
    panels_by_gene = index_panels_by_gene(panels)
    process_vcf(panels_by_gene, input_vcf, output_vcf)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotate vcf with panels names derived from Genomics England')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input_vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf file')
    parser.add_argument('--panels_json', '-t', metavar='input_tsv', type=str, help='input tsv file',
                        default="/resources/gene-panels.json")
    parser.add_argument('--output_vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf file')
    args = parser.parse_args()
    
    main(args.panels_json, args.input_vcf, args.output_vcf)
