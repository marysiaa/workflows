#!/usr/bin/python3

import pandas as pd
import argparse

__version__ = '1.0.0'
AUTHOR="gitlab.com/GlebLavr"

parser = argparse.ArgumentParser(description='annotates vcf with tsv.gz')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-csv', '-i', metavar='input_csv', type=str, required=True, help='input vcf.gz file')
parser.add_argument('--output-likelypathogenic', '-lp', metavar='output_likelypathogenic', type=str, required=True, help='output crosstab .csv file')
parser.add_argument('--output-pathogenic', '-p', metavar='output_pathogenic', type=str, required=True, help='output crosstab .csv file')
parser.add_argument('--output-benign', '-b', metavar='output_benign', type=str, required=True, help='output crosstab .csv file')
parser.add_argument('--output-crosstab', '-o', metavar='output_crosstab', type=str, required=True, help='output crosstab .csv file')
args = parser.parse_args()

def main():
    data = pd.read_csv(args.input_csv, sep='\t', low_memory=False)
    clinvar_data = data['ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE']
    acmg_data = data['ISEQ_ACMG_SUMMARY_CLASSIFICATION']

    df1 = data[(data.ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE == 'pathogenic/likely_pathogenic') & (data.ISEQ_ACMG_SUMMARY_CLASSIFICATION == 'Uncertain')]
    df1.to_csv(args.output_likelypathogenic, sep='\t')

    df2 = data[(data.ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE == 'pathogenic') & (
                data.ISEQ_ACMG_SUMMARY_CLASSIFICATION == 'Uncertain')]
    df2.to_csv(args.output_pathogenic, sep='\t')

    df3 = data[(data.ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE == 'benign') & (
            data.ISEQ_ACMG_SUMMARY_CLASSIFICATION == 'Uncertain')]
    df3.to_csv(args.output_benign, sep='\t')

    result = pd.crosstab(acmg_data, clinvar_data)
    result.to_csv(args.output_crosstab, sep='\t')

if __name__=='__main__': main()