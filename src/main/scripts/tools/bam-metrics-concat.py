#!/usr/bin/python3
import argparse
import ntpath
import math
import json
import os
import re

__version__ = "0.0.1"
AUTHOR="https://gitlab.com/olaf.tomaszewski"

parser = argparse.ArgumentParser(description='Collects simple-final-csv files created in bam-metrics and concatenates them respectively to chromosomes')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--final_json', '-f', action="append", type=str, default=[], required=True, help='json file containing all pickard metrics calculated in bam-metrics.wdl, for one chromosome')
parser.add_argument('--kit_json', '-j', help='Json with metadata about metrics in simple-json with query how to merge them')
parser.add_argument("--output_name", "-o", help="path to the output file")
args = parser.parse_args()

def load_json(path: str) -> dict:
    with open(path) as f:
        return json.load(f)

def extract_submetrics(query):
    return re.findall(r'(sum|avg)\(([^\)]+)\)', query)

def merge_metrics(kit: list, finals: list) -> dict:
    merged = []

    for meta in kit:
        record = {}
        if meta.get("disabled", False):
            continue
        values = {}
        for func, submetric in extract_submetrics(meta["query"]):
            if func == "sum":
                value = 0
                for final in finals:
                    value += eval(submetric, math.__dict__, final["metrics"])
                values[func + "(" + submetric + ")"] = value
            elif func == "avg":
                value = 0
                for final in finals:
                    value += eval(submetric, math.__dict__, final["metrics"])
                values[func + "(" + submetric + ")"] = value / len(finals)
            else:
                raise Exception("Unimplemented function: " + func)
         
        query = meta["query"]
        for key, value in values.items():
            query = query.replace(key, str(value))

        record["computed_value"] = round(eval(query, math.__dict__), 4)

        for field in ["name", "metric_class", "description", "id", "expected", "source"]:
            record[field] = meta.get(field, "")
        
        for i in ["error", "warning"]:
            if i in meta.keys():
                record[i] = meta[i]
                is_er_war = eval(meta[i], {"computed_value": record["computed_value"]})
                record["is_{0}".format(i)] = is_er_war
        merged.append(record)
    return merged

def merge_finals(finals: list) -> dict:
    return {  "finals": { final["chromosome"]: final["metrics"] for final in finals } }

def dict_to_json(dictionary: dict, json_name: str):
    with open(json_name, 'w') as json_simple:
        json.dump(dictionary, json_simple, indent=2)

if __name__ == '__main__':
    kit = load_json(args.kit_json)
    not_empty_final_json = [f for f in args.final_json if os.stat(f).st_size != 0]
    finals = [load_json(f) for f in not_empty_final_json]
    merged_metrics = merge_metrics(kit, finals)
    dict_to_json(merged_metrics, args.output_name)
    # this can be developed in the future
    dict_to_json(merge_finals(finals), "chromosomes-combined-data.json")
