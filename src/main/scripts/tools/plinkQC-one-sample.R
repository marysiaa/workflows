#!/usr/bin/env Rscript

library(plinkQC)
library(argparse)
library(ggplot2)
library(jsonlite)
library(plotly)
library(htmlwidgets)

version <- '1.0.0' #form plinkQC 1.2.3

parser <- ArgumentParser(description='')
parser$add_argument('-v', '--version', action='version', version=paste(sub(".*=.*/", "", commandArgs()[4]), version))
parser$add_argument('--id', type='character')
parser$add_argument('--pwd', type='character')
parser$add_argument('--imissTh', type='double')
parser$add_argument('--hetTh', type='double')
parser$add_argument('--highIBDTh', type='double')
parser$add_argument("--snps", type = "character")

args <- parser$parse_args()


current_path = args$pwd
plink_path="/intelliseqtools/plink"


check_sex = check_sex(indir=current_path, qcdir=current_path, name=args$id, path2plink=plink_path, interactive=FALSE, verbose=TRUE)
ggsave(plot=check_sex$p_sexcheck, paste(args$id, "check_sex-perindividual_report.jpg", sep="-"))


check_het_and_miss = check_het_and_miss(indir=current_path, qcdir=current_path, name=args$id, path2plink=plink_path, interactive=FALSE, verbose=TRUE,
                    imissTh=args$imissTh, hetTh=args$hetTh)
ggsave(plot=check_het_and_miss$p_het_imiss, paste(args$id, "het_and_miss-perIndividual_report.jpg", sep="-"))


check_hwe=check_hwe(indir=current_path, qcdir=current_path, name=args$id, path2plink=plink_path, interactive=FALSE, verbose=TRUE)
ggsave(plot=check_hwe$p_hwe, paste(args$id, "hwe-perMarkerQC_report.jpg", sep="-"))

check_maf=check_maf(indir=current_path, qcdir=current_path, name=args$id, path2plink=plink_path, interactive=FALSE, verbose=TRUE)
ggsave(plot=check_maf$p_maf, paste(args$id, "maf-perMarkerQC_report.jpg", sep="-"))

snps_path <- args$snps
check_snp_missingness=check_snp_missingness(indir=current_path, qcdir=current_path, name=args$id, path2plink=plink_path, interactive=FALSE, verbose=TRUE, extract_markers = snps_path)
ggsave(plot=check_snp_missingness$p_lmiss, paste(args$id, "snp_missingness-perMarkerQC_report.jpg", sep="-"))

perMarkerQC = perMarkerQC(indir=current_path, qcdir=current_path, name=args$id, path2plink=plink_path, interactive=FALSE, verbose=TRUE)
ggsave(plot=perMarkerQC$p_markerQC, paste(args$id, "perMarkerQC_report.jpg", sep="-"))


perIndividual=list("fail_sex"=check_sex$fail_sex, "fail_imis"=check_het_and_miss$fail_imis, "fail_het"=check_het_and_miss$fail_het)
perIndividual_json=toJSON(perIndividual)
write(perIndividual_json, paste(args$id, "perIndividual_vcf-qc.json", sep="-"))

perMarker=list("fail_hwe"=check_hwe$fail_hwe, "fail_maf"=check_maf$fail_maf, "fail_missingness"=check_snp_missingness$fail_missingness)
perMarker_json=toJSON(perMarker)
write(perMarker_json, paste(args$id, "perMarker_vcf-qc.json", sep="-"))

write.table(data.frame(
  id = check_sex$plot_data$IID, 
  sex = check_sex$plot_data$F,
  sexpass = ifelse(check_sex$plot_data$STATUS == "OK", "passed", "failed"),
  miss = check_het_and_miss$plot_data$F_MISS,
  misspass = ifelse(check_het_and_miss$plot_data$F_MISS < args$imissTh, "passed", "failed"),
  het = check_het_and_miss$plot_data$F,
  hetpass = "passed",
  sexplot = paste(args$id, "check_sex-perindividual_report.html", sep="-"),
  hetmissplot =  paste(args$id, "het_and_miss-perIndividual_report.html", sep="-")
  ),
  file = "qc.tsv",
  quote = F,
  row.names = F,
  sep = "\t"
)
