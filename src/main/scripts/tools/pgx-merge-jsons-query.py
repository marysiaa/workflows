#!/usr/bin/env python3

import argparse
import json
import os


__version__ = "0.0.1"


def load_json(filename: str) -> dict:
    with open (filename, "r") as f:
        return json.load(f)


def write_json(output_directory: str, filename: str, data: dict):
    with open(os.path.join(output_directory, filename), "w") as f:
        json.dump(data, f, indent=2)


def add_to_dict(main_dict: dict, dict_to_add: dict) -> dict:
    return main_dict.update(dict_to_add)


def concat_dicts(result_directory: str) -> dict:
    pharmgkb = dict()
    openpgx = dict()
    for file in os.scandir(result_directory):
        where_to_add = {
            "pharmgkb": lambda : add_to_dict(pharmgkb, load_json(file.path)),
            "openpgx": lambda : add_to_dict(openpgx, load_json(file.path))
        }
        where_to_add.get(file.name.split("_")[0], lambda : "ERROR: Invalid path")()
    return pharmgkb, openpgx


def main():
    parser = argparse.ArgumentParser(description='Merge JSONs queries for OpenPGx and PharmGKB')
    parser.add_argument('--input-directory', type=str, required=True, help='Input directory with OpenPGx and PharmGKB results')
    parser.add_argument('--output-directory', type=str, required=True, help='Output directory for merged JSONs')
    args = parser.parse_args()

    # Concat OpenPGx and PharmGKB queries
    pharmgkb_query, openpgx_query = concat_dicts(args.input_directory)
    write_json(args.output_directory, "pharmgkb.json", pharmgkb_query)
    write_json(args.output_directory, "openpgx.json", openpgx_query)


if __name__ == "__main__":
    main()
