#!/usr/bin/python3

from synonym_panel_validation import *
from numpy.testing import assert_equal


def test_take_json_path_local():
    dir_path = "../../../test/resources/data/only_one_json"
    assert "json" in take_json_path(dir_path)


def test_take_json_path_json():
    dir_path = "../../../test/wdl/tasks/pgx-aldy"
    assert "json" in take_json_path(dir_path)

def test_index_synonyms():
    gene_names, synonyms = index_synonyms({
        "AADACP1": "",
        "AADAT": "Synonyms: kat2, KATII, KYAT2"
    })

    assert_equal(gene_names, ["AADACP1", "AADAT"])
    assert synonyms == {'AADACP1': ['AADACP1'],'AADAT': ['AADAT'], 'KAT2': ['AADAT'], 'KATII': ['AADAT'], 'KYAT2': ['AADAT'],}


def test_index_synonyms_synonym_is_gene():
    gene_names, synonyms = index_synonyms({
        "TKT": "",
        "DDR2": "Synonyms: TKT"
    })

    assert gene_names == ['TKT', 'DDR2']
    assert synonyms == {'DDR2': ['DDR2'], 'TKT': ['DDR2', 'TKT']}


def test_index_synonyms_punctuation():
    gene_names, synonyms = index_synonyms({
        "AaDACP1": "",
        "AaDAT": "Synonyms: kat2, KaTII, KyAT2"
    })
    
    assert_equal(gene_names, ["AaDACP1", "AaDAT"])
    assert synonyms == {'KAT2': ['AaDAT'], 'KATII': ['AaDAT'], 'KYAT2': ['AaDAT'], 'AADACP1': ['AaDACP1'], 'AADAT': ['AaDAT']}


def test_validate_genes_not_exists():
    gene_names, synonyms = index_synonyms({
        "5S_rRNA": "",
        "5_8S_rRNA": "",
    })
    panel = [{"name": "INVALID_GENE", "score": 28.97959183673469, "type": "phenotype"}]
    assert_equal(normalize_genes(panel, gene_names, synonyms), [])


def test_validate_genes_is_synonym():
    gene_names, synonyms = index_synonyms({
        "AADACL3": "Synonyms: OTTHUMG00000001887",
        "WWOX": ""
    })
    result = normalize_genes([{"name": "OTTHUMG00000001887", "score": 28.97959183673469, "type": "phenotype"},
                              {"name": "WWOX", "score": 28.97959183673469, "type": "phenotype"}], gene_names, synonyms)
    assert_equal(result, [
        {"name": "AADACL3", "score": 28.97959183673469, "type": "phenotype"},
        {"name": "WWOX", "score": 28.97959183673469, "type": "phenotype"}
    ])


def test_validate_genes_upper_lower_problem():
    gene_names, synonyms = index_synonyms({
        "CXorf56": "Synonyms: FLJ22965"
    })
    panel = normalize_genes([{"name": "CXORF56", "score": 20, "type": "phenotype"}], gene_names, synonyms)
    expected = [{"name": "CXorf56", "score": 20, "type": "phenotype"}]
    assert_equal(panel, expected)


def test_validate_genes_upper_lower_problem_2():
    gene_names, synonyms = index_synonyms({
        "MT-TF": "Synonyms: MTTF, trnF"
    })
    result = normalize_genes([{"name": "trnF", "score": 20, "type": "phenotype"}], gene_names, synonyms)
    expected = [{"name": "MT-TF", "score": 20, "type": "phenotype"}]
    assert_equal(result, expected)


def test_synonym_for_more_genes():
    gene_names, synonyms = index_synonyms({
        "A1CF": "Synonyms: ASP",
        "ASIP": "Synonyms: ASP",
        "ASPA": "Synonyms: ASP",
        "ASPM": "Synonyms: ASP"
    })
    result = normalize_genes([{"name": "ASP", "score": 28.97959183673469, "type": "phenotype"}], gene_names, synonyms)
    expected = [{"name": "A1CF", "score": 28.97959183673469, "type": "phenotype"},
              {"name": "ASIP", "score": 28.97959183673469, "type": "phenotype"},
              {"name": "ASPA", "score": 28.97959183673469, "type": "phenotype"},
              {"name": "ASPM", "score": 28.97959183673469, "type": "phenotype"}]
    assert result == expected


def test_synonym_is_gene_and_synonim():
    gene_names, synonyms = index_synonyms({
        "DDR2": "Synonyms: TKT, TYRO10",
        "TKT": ""
    })
    result = normalize_genes([{"name": "TKT", "score": 10, "type": "phenotype"}], gene_names, synonyms)
    expected = [{"name": "TKT", "score": 10, "type": "phenotype"},
              {"name": "DDR2", "score": 10, "type": "phenotype"}]
    assert result, expected
    

def test_write_validated_panel():
    write_validated_panel("../../../test/resources/data/json/panel.json",
                          "../../../test/resources/data/json/ensembl-genes-sample.json", "/tmp/test_panel.json")
