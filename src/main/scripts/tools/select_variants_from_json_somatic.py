#!/user/bin/env python3

import json
import argparse
import re

__version__ = '0.2.0'


def read_json(json_input):
    with open(json_input) as json_file:
        vcf_dict = json.load(json_file)
        return vcf_dict


def get_items_with_key(variant_list, chosen_key):
    return [i for i in variant_list if chosen_key in i.keys() and i[chosen_key]]


def sort_list_by_acmg_score(variant_list):
    list_with_score = get_items_with_key(variant_list, "ISEQ_ACMG_SUMMARY_SCORE")
    return sorted(list_with_score, key=lambda x: x["ISEQ_ACMG_SUMMARY_SCORE"], reverse=True)

def find_evidence_level(variant):
    try:
        levels = re.findall("Level\^[A-E]",variant["ISEQ_CIVIC_EVIDENCE_INFO"])
        levels = [i.replace("Level^", "") for i in levels]
        level = sorted(levels)[0]
        print(level)
    except (KeyError, IndexError):
        level = "Z"    
    return level    

def sort_list_by_civic_level(variant_list):
    levels = [find_evidence_level(i) for i in variant_list]
    var_levels = [i for i in zip(levels, variant_list)]
    var_sorted = sorted(var_levels, key = lambda x: x[0])
    return [i[1] for i in var_sorted]    


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Selects variants that should be shown in report.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-json', '-i', type=str, required=True, help='Input json file (with all variants)')
    parser.add_argument('--output-json', '-o', type=str, required=True,
                        help='output json file (with selected variants)')
    parser.add_argument('--variant-number', '-n', type=int, default=50, help='Number of variants to keep')
    args = parser.parse_args()

    # all variants
    vcf_list = read_json(args.input_json)
    vcf_list = vcf_list["variants"]

    # civic variants
    civic_list = get_items_with_key(vcf_list, "ISEQ_CIVIC_EVIDENCE_INFO")

    # non benign acmg variants sorted by severity
    #sorted_civic_list = sort_list_by_civic_level(civic_list)
    sorted_civic_list = sort_list_by_acmg_score(civic_list) 
            
    # other variants
    other_list = [i for i in vcf_list if i not in  civic_list]
    
    # sort other by acmg
    sorted_other = sort_list_by_acmg_score(other_list)
    
    # final list: all from civic  plus the most severe from acmg list
    final_list = sorted_civic_list + sorted_other
    n = max([args.variant_number,len(civic_list)])
    #n = args.variant_number
    final_list = final_list[0:n]
    
    with open(args.output_json, "w") as f:
        t = json.dumps(final_list)
        f.write(t)

