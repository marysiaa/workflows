#!/usr/bin/env python3

import argparse
import json


__version__ = "0.0.1"


def load_json(filename: str) -> dict:
    with open (filename, "r") as f:
        return json.load(f)


def add_report_info(recommendations: dict, genotypes: dict):
    for drug, databases in recommendations.items():
        for database, info in databases.items():
            recommendations[drug][database]["report_genotype"] = []
            recommendations[drug][database]["report_phenotype"] = []
            for gene, factor in info["factors"].items():
                if genotypes.get(gene):
                    if "/" in genotypes[gene]:
                        allele_one = genotypes[gene].split("/")[0]
                        allele_two = genotypes[gene].split("/")[1]
                        recommendations[drug][database]["report_genotype"].append(gene + allele_one + "/" + gene + allele_two)
                    else:
                        recommendations[drug][database]["report_genotype"].append(gene + " " + genotypes[gene])
                    factor = factor.replace("==", "Activity score")
                    recommendations[drug][database]["report_phenotype"].append(factor)


def save_json(rs_to_gt: dict, filename: str):
    with open(filename, 'w') as f:
        json.dump(rs_to_gt, f, indent=2)


def main():
    parser = argparse.ArgumentParser(description='Create JSON for PharmGKB queries from a TSV file')
    parser.add_argument('--recommendations', type=str, required=True, help='Path to a JSON file containing the recommendations')
    parser.add_argument('--genotypes', type=str, required=True, help='Path to a JSON file containing the genotypes')
    parser.add_argument('--output-filename', type=str, required=True, help='Path to output JSON file')
    args = parser.parse_args()

    # Load the JSON files
    recommendations = load_json(args.recommendations)
    genotypes = load_json(args.genotypes)

    # Add the report info
    add_report_info(recommendations, genotypes)

    # Save JSON
    save_json(recommendations, args.output_filename)


if __name__ == '__main__':
    main()
