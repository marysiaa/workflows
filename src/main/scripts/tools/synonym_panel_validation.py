#!/usr/bin/python3

import argparse
import glob
import json
import os
from os import path

from collections import defaultdict

__version__ = '1.0.2'

ERRORS = []
WARNINGS = []


def output_dict_to_json(dict_or_list, json_name: str):
    with open(json_name, 'w') as json_simple:
        json.dump(dict_or_list, json_simple, indent=2)


def load_json(json_path: str) -> dict:
    with open(json_path) as f:
        return json.load(f)


def take_json_path(dir_path: str) -> str:
    if dir_path[-5:] == ".json":
        return dir_path
    result = glob.glob(path.join(dir_path, '**', '*.json'), recursive=True)
    if len(result) == 0:
        raise Exception(f"There is no json file in directory {dir_path}. Please provide path with .json")
    elif len(result) > 1:
        raise Exception(f"There is more than one json file in directory {dir_path}. Please provide path with one .json")
    return result[0]


def index_synonyms(genes: dict) -> dict:
    original_names = list(genes.keys())
    original_names_by_synonym = defaultdict(set)
    for gene_name, synonyms_str in genes.items():
        synonyms_list = []
        if synonyms_str:
            synonyms_list = [x.replace(" ", "").upper() for x in synonyms_str]
        for synonym in synonyms_list:
            original_names_by_synonym[synonym].add(gene_name)
        original_names_by_synonym[gene_name.upper()].add(gene_name)
    return original_names, {synonym: sorted(list(original_names)) for synonym, original_names in original_names_by_synonym.items()}


def create_synonyms_index(ensembl_path: str) -> dict:
    genes = load_json(ensembl_path)
    return index_synonyms(genes)

    
def normalize_genes(panel, gene_names: list, synonyms: dict) -> list:
    new_panel = []
    for gene in panel:
        upper_gene_name = gene["name"].upper()
        
        if upper_gene_name not in synonyms:
            error = f"Provided gene: {gene['name']} was not found in Ensembl and was not used in later analyses."
            ERRORS.append(error)
            continue
            
        original_names = synonyms[upper_gene_name]

        if len(synonyms[upper_gene_name]) > 1 or synonyms[upper_gene_name][0] != gene["name"]:
            text = "it" if len(original_names) == 1 else "them"
            warning = f"Provided gene: {gene['name']} is synonym of {', '.join(synonyms[upper_gene_name])} and was replaced by {text} in later analyses. "
            WARNINGS.append(warning)
        
        for ensembl_gene in original_names:
            new_gene = {"name": ensembl_gene, "score": gene["score"], 'type': gene["type"]}
            new_panel.append(new_gene)
    
    return new_panel


def genomics_england(dir_path, ensembl_genes):
    result = []
    for filename in os.listdir(dir_path):
        if filename.endswith(".json"):
            gene_panel = load_json(os.path.join(dir_path, filename))
            gene_names, synonyms = create_synonyms_index(take_json_path(ensembl_genes))
            validated_panel = normalize_genes(gene_panel, gene_names, synonyms)
            panel_name = filename.replace(".json", "").replace("_", " ")
            result.append({"panel_name": panel_name, "genes": validated_panel})
    return result
        

def write_validated_panel(panel_json: str, ensembl_genes: str, panel_output: str):
    gene_panel = load_json(panel_json)
    gene_names, synonyms = create_synonyms_index(take_json_path(ensembl_genes))
    validated_panel = normalize_genes(gene_panel, gene_names, synonyms)
    output_dict_to_json(validated_panel, panel_output)


def collect_warnings_errors():
    return {"errors": ERRORS, "warnings": WARNINGS}


def write_warnings_errors(output_path):
    result = collect_warnings_errors()
    output_dict_to_json(result, output_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="""Script takes gene panel and removes wrong names, replace synonym of the gene by its proper
        name from ensembl. If synonym of the gene has more than one corresponding gene in ensembl all of them will be
        included in output panel. Any delete or replace will appear in output error_warning.json""")
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--ensembl_genes', required=False, default="/tools/explorare/src/main/resources/ensembl-genes.json",
                        help="HARD CODED - PATH TO DIRS IN DOCKER IMAGE BUT OPTIONAL TO CHANGE TO LOCAL TESTING")
    parser.add_argument('-p', '--panel', type=str, required=False,
                        help=".json panel, example: [{'name': 'ASP','score': 28.97959183673469,'type': 'phenotype'}]")
    parser.add_argument('-w', '--warning_output', type=str, required=False)
    parser.add_argument('-o', '--panel_output', type=str, required=False, help="output filename")
    parser.add_argument('-d', '--directory', type=str, required=False,
                        help="for all genomics england panels conversion")
    args = vars(parser.parse_args())

    if args["panel"]:
        write_validated_panel(args["panel"], args["ensembl_genes"], args["panel_output"])

    if args["warning_output"]:
        write_warnings_errors(args["warning_output"])

    if args["directory"]:
        print(json.dumps(genomics_england(args["directory"], args["ensembl_genes"])))
