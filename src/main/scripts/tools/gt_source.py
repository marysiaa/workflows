import pysam
import pandas as pd
import argparse

__version__ = "0.0.1"
# bcftools filter -i ID=@duplicated_markers.tsv  no_id_provided_gtc2vcf.vcf.gz | bcftools query -f '%CHROM\t%POS\t%ID\t%REF\t%ALT[\t%GT]\n' > /tmp/kt/dup_only.gtc
# bcftools view -h  no_id_provided_gtc2vcf.vcf.gz | tail -1 | cut -f1,2,4,5,3,10- | sed 's/^#//' | cat - /tmp/kt/dup_only.gtc > /tmp/kt/dup_only_gtc.tsv


def prepare_query_df(bcftools_tsv):
    '''aggregates information on duplicated markers'''
    df = pd.read_csv(bcftools_tsv, sep="\t", header=0)
    df_samples = list(df.columns[5:])
    old = ["0[/|]0", "0[|/]1", "1[|/]0", "1[|/]1", "\.[|/]\."]
    new = ["ref", "het", "het", "alt_hom", "mis"]
    df = df.replace(value=new, to_replace=old, regex=True)
    df = df.groupby(['CHROM', 'POS', 'REF', 'ALT']).agg(
        lambda x: '|'.join(x)).reset_index()
    df['POS'] = df['POS'].transform(str)
    df['NEW_ID'] = df[['CHROM', 'POS', 'REF', 'ALT']].agg('-'.join, axis=1)
    df['NEW_ID'] = df['NEW_ID'].str.replace("chr", "")
    df = df.set_index('NEW_ID')
    df = df[['ID'] + df_samples]
    # print(df['ID'].loc['1-1021472-C-T'])
    return df


def make_id(record):
    '''writes gnomAD style id'''
    chrom = record.contig
    chrom = chrom.replace("chr", "")
    pos = str(record.pos)
    ref = record.ref
    alt = record.alts[0]
    return "-".join([chrom, pos, ref, alt])


def main(infile, outfile, df):
    '''annotates vcf file (info on site other marker genotypes and ids)'''
    vcf = pysam.VariantFile(filename=infile)
    items = [('ID', "PRODIA_IDS"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description',
              "IDs of all markers with the same position and alleles")]
    vcf.header.add_meta("INFO", items=items)

    items = [('ID', "PMG"), ('Number', "1"), ('Type', "String"),
             ('Description',
              "PRODIA markers genotypes")]
    vcf.header.add_meta("FORMAT", items=items)

    out_vcf = pysam.VariantFile(outfile, 'w', header=vcf.header)

    for record in vcf.fetch():
        gnomad_id = make_id(record)
        samples = record.samples
        # print(gnomad_id)
        try:
            # print(df['ID'].loc[gnomad_id])
            record.info['PRODIA_IDS'] = df['ID'].loc[gnomad_id]
        except:
            record.info['PRODIA_IDS'] = '.'

        for sample in samples:
            # print(sample)
            try:
                record.samples[sample]["PMG"] = df[sample].loc[gnomad_id]
            except:
                record.samples[sample]["PMG"] = '.'
        out_vcf.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Adds annotations about Prodia markers')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True,
                        help='Input vcf file (may be bgzipped)')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True,
                        help='Output vcf file (will be bgzipped - if .gz suffix present)')
    parser.add_argument('--bcftools-tsv', '-b', metavar='bcftools_tsv', type=str, required=True,
                        help='Bcftools query table (bcftools query')

    args = parser.parse_args()
    data_frame = prepare_query_df(args.bcftools_tsv)
    main(args.input_vcf, args.output_vcf, data_frame)
