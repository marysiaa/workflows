#!/usr/bin/env python3

import argparse
import json
import pandas as pd
from datetime import datetime
import math


__version__ = "0.0.2"


def load_xlsx(filename: str) -> pd.DataFrame:
    return pd.read_excel(filename, sheet_name=None)


def load_json(filename: str) -> dict:
    with open(filename, 'r') as f:
        return json.load(f)


def save_json(json_dict: dict, filename: str):
    with open(filename, 'w') as f:
        json.dump(json_dict, f, indent=2)


def todays_date():
    return datetime.today().strftime('%m/%d/%Y')


def add_basic_info(final_json: dict, analysis_name: str, analysis_version: str):
    final_json['analysisWorkflow'] = analysis_name
    final_json['analysisWorkflowVersion'] = analysis_version
    final_json['reportDate'] = todays_date()
    return final_json


def score_mean(value1: float, value2: float) -> float:
    return (value1 + value2) / 2


def score_to_phenotype(score: float) -> str:
    if 0 <= score < 0.5:
        return "Poor Metabolizer"
    elif 0.5 <= score < 1.0:
        return "Likely Poor Metabolizer"
    elif 1.0 <= score < 1.5:
        return "Intermediate Metabolizer"
    elif 1.5 <= score < 2.0:
        return "Likely Intermediate Metabolizer"
    elif score == 2.0:
        return "Normal Metabolizer"
    elif 2.0 < score < 3.25:
        return "Rapid Metabolizer"
    elif 3.25 <= score:
        return "Ultra Rapid Metabolizer"
    return "Indeterminate"


def check_if_one_score_is_none(score1: float, score2: float) -> str:
    if (score1 and math.isnan(score2)):
        return check_if_likely_in_phenotype(score_to_phenotype(score1))
    elif (math.isnan(score1) and score2):
        return check_if_likely_in_phenotype(score_to_phenotype(score2))
    return "Indeterminate"


def check_if_likely_in_phenotype(phenotype: str) -> str:
    if "Likely" in phenotype:
        return phenotype
    else:
        return "Likely " + phenotype


def generate_final_phenotype(score1: float, score2: float) -> str:
    mean = score_mean(score1, score2)
    if math.isnan(mean):
        return check_if_one_score_is_none(score1, score2)
    return score_to_phenotype(mean)


def create_final_json(final_json: dict, models: dict, diplotype_phenotype_table: pd.DataFrame):
    final_json["phenotypes"] = dict()
    final_json["genes"] = dict()
    score_values = dict()
    for gene, info in models.items():
        gene_diplotypes = diplotype_phenotype_table[gene]
        # Create diplotype (try with haplotype_id, if not found try with 1st item in matching_haplotypes)
        try:
            short_haplotype, long_haplotype = create_haplotype(gene, info["haplotype_model"]["haplotypes"]["haplotype_id"])
        except:
            short_haplotype, long_haplotype = create_haplotype(gene, info["haplotype_model"]["haplotypes"]["matching_haplotypes"][0])
        # Create score (try except because sometimes short_haplotype is reversed: CYP2C19*2/CYP2C19*1)
        try:
            score_values[gene] = gene_diplotypes.loc[gene_diplotypes['Diplotype'] == short_haplotype]["Activity Score"].values[0]
        except:
            short_haplotype = short_haplotype.split("/")[1] + "/" + short_haplotype.split("/")[0]
            score_values[gene] = gene_diplotypes.loc[gene_diplotypes['Diplotype'] == short_haplotype]["Activity Score"].values[0]
        final_json["genes"][gene] = {"haplotype": long_haplotype}
        final_json["genes"][gene]["rs-genotype"] = []
        for gnomad_id, item in info["genotypes"].items():
            final_json["genes"][gene]["rs-genotype"].append(item["genotype"])
    final_json["phenotypes"]["THC"] = generate_final_phenotype(score_values["CYP2C9"], score_values["CYP3A4"])
    final_json["phenotypes"]["CBD"] = generate_final_phenotype(score_values["CYP2C19"], score_values["CYP3A4"])
    return final_json


def create_haplotype(gene: str, haplotype_id: str) -> str:
    first_allele = "*" + haplotype_id.split("_")[0].split(".")[0].split("*")[1]
    second_allele = "*" + haplotype_id.split("_")[1].split(".")[0].split("*")[1]
    short_haplotype = first_allele + "/" + second_allele
    long_haplotype = gene + first_allele + "/" + gene + second_allele
    return short_haplotype, long_haplotype


def main():
    parser = argparse.ArgumentParser(description='Create final JSON report from PGx Cannabinoid workflow')
    parser.add_argument('--models', type=str, required=True, help='Path to a JSON file with models')
    parser.add_argument('--diplotype-phenotype-table', type=str, required=True, help='Path to a XLSX file with diplotype-phenotype table')
    parser.add_argument('--analysis-name', type=str, required=True, help='Analysis name')
    parser.add_argument('--analysis-version', type=str, required=True, help='Analysis version')
    parser.add_argument('--output-filename', type=str, required=True, help='Path to output JSON file')
    args = parser.parse_args()

    # Load TSV files
    all_models = load_json(args.models)

    # Load xlsx file
    diplotype_phenotype_table = load_xlsx(args.diplotype_phenotype_table)

    # Add basic info
    final_json = dict()
    final_json = add_basic_info(final_json, args.analysis_name, args.analysis_version)

    # Create final JSON
    final_json = create_final_json(final_json, all_models, diplotype_phenotype_table)

    # Save final JSON
    save_json(final_json, args.output_filename)


if __name__ == "__main__":
    main()
