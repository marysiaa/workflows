#!/usr/bin/python3

import argparse
import pysam
from pysam import VariantFile, VariantRecord

__version__ = '1.0.2'


def get_variants(input_vcf, output_vcf):
    METHOD = "ISEQ_CLINVAR_COLLECTION_METHOD"
    SIGNIFICANCE = "ISEQ_CLINVAR_SIGNIFICANCE"
    OK_METHOD_SET = {'clinical_testing', 'clinical_testing/curation', 'clinical_testing/provider_interpretation',
                     'curation'}
    OK_SIGNIFICANCE_SET = {'likely_pathogenic', 'pathogenic', 'pathogenic/likely_pathogenic', 'pathogenic_low_penetrance', 'likely_pathogenic_low_penetrance'}

    vcf_in = VariantFile(input_vcf, index_filename=input_vcf + '.tbi')
    vcf_writer = pysam.VariantFile(output_vcf, 'w', header=vcf_in.header)

    for rec in vcf_in.fetch():
        if SIGNIFICANCE in rec.info and METHOD in rec.info:
            methods = list(rec.info[METHOD][0].split(":"))
            significances = list(rec.info[SIGNIFICANCE][0].split(":"))
            trials = list(zip(methods, significances))
            for method, significance in trials:
                if method in OK_METHOD_SET and significance in OK_SIGNIFICANCE_SET:
                    vcf_writer.write(rec)
                    break
                else:
                    continue


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Filtering vcf to only those variants that are pathogenic in '
                                                 'clinical testing.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', type=str, required=True, help='input vcf file')
    parser.add_argument('--output-vcf', '-o', type=str, required=True, help='output vcf file')
    args = parser.parse_args()

    get_variants(args.input_vcf, args.output_vcf)
