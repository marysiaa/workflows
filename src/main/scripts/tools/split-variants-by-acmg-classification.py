#!/usr/bin/python3

import argparse
import json
import re

__version__ = '1.0.3'

def run(arguments):

    outputs = [ {'id': 'benign', 'path': arguments.output_benign, 'classification': 'Benign'},
                {'id': 'likely-benign', 'path': arguments.output_likely_benign, 'classification': 'Likely^benign'},
                {'id': 'uncertan', 'path': arguments.output_uncertain, 'classification': 'Uncertain'},
                {'id': 'likely-pathogenic', 'path': arguments.output_likely_pathogenic, 'classification': 'Likely^Pathogenic'},
                {'id': 'pathogenic', 'path': arguments.output_pathogenic, 'classification': 'Pathogenic'},
                {'id': 'undefined', 'path': arguments.output_undefined, 'classification': None}
              ]

    with open(arguments.input, 'r') as input_file:
        variants = json.load(input_file)

    for output in outputs:
        with open(output['path'], 'w') as output_file:
            data = list(filter(lambda variant: variant['ISEQ_ACMG_SUMMARY_CLASSIFICATION'] == output['classification'], variants))
            data = sorted(data, key = lambda variant: variant['ISEQ_ACMG_SUMMARY_SCORE'], reverse=True)
            json.dump(data, output_file)
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='The script splits json based on acmg_summary field into single files')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str, help="path to input variants file (annotated by acmg_summary and converted to json format)")
    parser.add_argument('-B', '--output-benign', type=str, help="path to benign output file")
    parser.add_argument('-b', '--output-likely-benign', type=str, help="path to likely benign output file")
    parser.add_argument('-u', '--output-uncertain', type=str, help="path to uncertain output file")
    parser.add_argument('-p', '--output-likely-pathogenic', type=str, help="path to likely pathogenic output file")
    parser.add_argument('-P', '--output-pathogenic', type=str, help="path to pathogenic output file")
    parser.add_argument('-n', '--output-undefined', type=str, help="path to undefined output file")

    arguments = parser.parse_args()

    run(arguments)
