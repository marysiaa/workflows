#!/usr/bin/env python3

import argparse
from operator import itemgetter
import pandas as pd

__version__ = "1.0.0"


def analyse_row(one_row):
    return "\t".join(["chr" + str(one_row[1]).replace("MT", "M"), str(int(one_row[2]) - 1), str(one_row[3]),
                      str(one_row[0])]) + "\n"


def sort_ensembl_df(ensembl_df):
    list_of_rows = ensembl_df.values
    sorted_by_coord = sorted(list_of_rows, key=itemgetter(1, 2))  # sort on secondary key
    return sorted_by_coord


def load_and_filter_ensembl_df(input_ensembl_tsv):
    columns = ['Gene name', 'Chromosome/scaffold name', 'Gene start (bp)', 'Gene end (bp)']
    return pd.read_csv(input_ensembl_tsv, sep='\t', usecols=columns)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Filters .tsv file with ensembl biomart data and converts it to bed')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-ensembl-tsv', '-e', metavar='input_ensembl_tsv', type=str,
                        help='A path to a file containing ensembl data in .tsv format')
    parser.add_argument('-o', '--output', type=str, required=True, help="Path to output bed")
    args = parser.parse_args()

    ensembl_df = load_and_filter_ensembl_df(args.input_ensembl_tsv)
    row_list = sort_ensembl_df(ensembl_df)
    
    with open(args.output, "w") as f:
        for row in row_list:
            f.write(analyse_row(row))
