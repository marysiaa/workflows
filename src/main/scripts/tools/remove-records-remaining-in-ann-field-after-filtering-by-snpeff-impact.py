#!/usr/bin/env python3

import argparse
import re
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(description='Removes from ANN field records regading alleles, that were filtered out. Input VCF must be'
                                         ' previously annotated using task \'annotate-vcf-with-snpeff.\' v0.1')

for line in stdin:

    if line[0] == "#":
        print(line.rstrip())

    else:
        # create list
        line_list = line.split('\t')

        # extract INFO field
        if "ANN=" in line_list[7]:

            info_list = line_list[7].split(';')
            ALTs = line_list[4].split(",")

            # find ANNO field
            where_anno = [i for i, s in enumerate(info_list) if s.startswith("ANN=")]
            where_anno = where_anno[0]

            # extract ANNO filed
            annotations = info_list[where_anno].strip()
            annotations_list = re.sub('^ANN=', '', annotations).split(",")

            annotations_alleles_list = [x.split("|")[0] for x in annotations_list]

            if set(annotations_alleles_list) == set(ALTs):
                print(line.strip())

            else:

                new_annotation_list = []

                for anno in annotations_list:
                    if anno.split("|")[0] in set(ALTs):
                        new_annotation_list.append(anno)

                if new_annotation_list is None:
                    info_list.pop(where_anno)

                else:
                    info_list[where_anno] = "ANN=" + ",".join(new_annotation_list)

                info = ';'.join(info_list)
                line_list[7] = info
                line = '\t'.join(line_list)
                print(line.rstrip())

        else:
            print(line.strip())
