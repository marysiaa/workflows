#!/usr/bin/env python3

import re
import json
import argparse
from typing import Any
from typing import Set
from typing import Dict
from typing import List
from typing import Tuple
from typing import Pattern
from typing import NamedTuple

__version__ = '0.0.1'

LINK_STR = '[|/]'
GENOTYPE_RE = re.compile(f'(\d{LINK_STR}\d)')
LINK_RE = re.compile(LINK_STR)


class VariantInfo(NamedTuple):
    ref:str
    alt:List[str]
    genotypes:List[str]


def parse_file(path:str) -> Dict[str, Any]:
    variant_line_counter = {}
    with open(path) as f:
        first_vcf_num_fields, first_vcf_sample_names = parse_header_line(f.readline())
        second_vcf_num_fields, second_vcf_sample_names = parse_header_line(f.readline())
        assert first_vcf_num_fields == second_vcf_num_fields
        assert first_vcf_sample_names == second_vcf_sample_names
        proper_genotypes_counter = [0 for sample_name in first_vcf_sample_names]
        for i, line in enumerate(f):
            for (sample_index, res) in check_imputing_in_line(line, first_vcf_num_fields).items():
                proper_genotypes_counter[sample_index] += int(res)
                try:
                    variant_line_counter[sample_index] += 1
                except KeyError:
                    variant_line_counter[sample_index] = 1

    fraction_of_properly_imptuted_genotypes = {}
    if variant_line_counter:
        for i, sample_name in enumerate(first_vcf_sample_names):
            variant_line_counter[sample_name] = variant_line_counter[i]
            del(variant_line_counter[i])
            try:
                fraction_of_properly_imptuted_genotypes[sample_name] = proper_genotypes_counter[i]/variant_line_counter[sample_name]
            except ZeroDivisionError:
                fraction_of_properly_imptuted_genotypes[sample_name] = 0
    return {
        'fraction_of_properly_imptuted_genotypes': fraction_of_properly_imptuted_genotypes,
        'num_of_considered_lines':variant_line_counter
    }


def parse_header_line(line:str) -> Tuple:
    # Example line to be parsed:
    # #CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  415
    fields:List = line.rstrip('#').strip().split()
    return len(fields), fields[9:]


def check_imputing_in_line(line:str, num_of_fields_in_vcf:int) -> Dict[int, bool]:
    # Example line to be parsed:
    # 19      200352  rs532817504     G       A       .       PASS    DR2=0.00;AF=0.0000;IMP  GT:DS   0|0:0   19      200352  rs532817504     G       A,C     .       PASS    RS_INFO=rs532817504:G:A^C;DP=5  GT:DP:RGQ 0/0:5:15
    fields = line.split()
    assert len(fields) == 2*num_of_fields_in_vcf
    variants1 = VariantInfo(fields[3], fields[4], fields[9:num_of_fields_in_vcf])
    variants2 = VariantInfo(fields[3+num_of_fields_in_vcf], fields[4+num_of_fields_in_vcf], fields[9+num_of_fields_in_vcf:])
    parsed_variants1 = parse_genotypes(variants1)
    parsed_variants2 = parse_genotypes(variants2)
    return {i:parsed_variants1[i]==parsed_variants2[i] for i in range(len(parsed_variants1)) if (parsed_variants1[i] is not None) and (parsed_variants2[i] is not None)}


def parse_genotypes(variant_into:VariantInfo) -> List[Set[str]]:
    return [genotype_string_to_letters(variant_into.ref, variant_into.alt, genotype_string) for genotype_string in variant_into.genotypes]


def genotype_string_to_letters(ref: str, alt: str, genotype_string: str, link_re:Pattern=LINK_RE) -> Set[str]:
    # genotype_string - sth line 1|0:blah
    indices = link_re.split(genotype_string.split(':')[0])
    possible_genotypes = [ref] + alt.split(',')
    genotype = []
    for index in indices:
        try:
            genotype.append(possible_genotypes[int(index)])
        except ValueError: # dot in genotype
            return None
    return set(genotype)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='jakiś sensowny opis, co robi skrypt')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--intersection_file', type=str, required=True,
                        help='file to be parsed and used to calculate imputing quality')
    parser.add_argument('-o', '--out_path', type=str, required=True, help='Output file path')
    arguments = parser.parse_args()

    with open(arguments.out_path, 'w') as f:
        json.dump(parse_file(arguments.intersection_file), f)
