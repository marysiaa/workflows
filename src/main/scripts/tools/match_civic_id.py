#!/usr/bin/env python3

from urllib.request import urlretrieve
import pysam
import argparse
import json
import re


__version__ = '0.0.1'

AUTHOR = "gitlab.com/kattom"

# check ANN -if present, if not pass


aa_dict = {
	"A": "Ala",
	"C": "Cys",
	"D": "Asp",
	"E": "Glu",
	"F": "Phe",
	"G": "Gly",
	"H": "His",
	"I": "Ile",
	"K": "Lys",
	"L": "Leu",
	"M": "Met",
	"N": "Asn",
	"P": "Pro",
	"Q": "Gln",
	"R": "Arg",
	"S": "Ser",
	"T": "Thr",
	"W": "Trp",
	"Y": "Tyr",
	"V": "Val"
}


class Transcript(object):
    """represents description of one transcript extracted from the ANN (or ISEQ_REPORT_ANN or ISEQ_MANE_ANN) field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.field = annotation_string
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def aa_pos(self):
        try:
            return [int(i) for i in self.fields[13].split("/")]
        except ValueError:
            return None

    def transcript_gene(self):
        return self.fields[3]

    def transcript_rank(self):
        return self.fields[8].split("/")

    def transcript_impact(self):
        return self.fields[2]

    def transcript_HGVSc(self):
        return self.fields[9]

    def transcript_HGVSp(self):
        return self.fields[10]

    def transcript_id(self):
        return self.fields[6].split("/")[0].split(".")[0]

    def transcript_version(self):
        try:
            return self.fields[6].split(".")[1]
        except IndexError:
            return ""

    def transcript_warnings(self):
        return self.fields[15]

    def transcript(self):
        return self.field

    def __mul__(self, other):
        if other is True:
            return self
        elif other is False:
            return ""

    def __rmul__(self, other):
        if other is True:
            return self
        elif other is False:
            return ""


def get_best_transcript(record):

    try:
        return Transcript(record.info["ISEQ_MANE_ANN"][0])

    except KeyError:  # report transcript is mane
        return Transcript(record.info["ISEQ_REPORT_ANN"][0])


# select transcripts (good and not nextProt) for the same gene as in the best transcript
def pick_all_good_ann_transcripts(transcript_list, gene):
    good_transcripts = [i for i in transcript_list if ("WARNING" not in i.transcript_warnings(
    ) and "ERROR" not in i.transcript_warnings() and i.transcript_type(
    ) == "protein_coding" and 'sequence_feature' not in i.variant_type() and i.transcript_gene() == gene)]
    return good_transcripts


# check if variant is lof for the "best transcript gene"
def check_record_lof(record_good_tx, record_best_tx):

    transcripts = [record_best_tx] + record_good_tx
    lof_transcripts = [("start_lost" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or
                        "frameshift" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or
                        "splice_donor" in transcript.variant_type()) for transcript in transcripts]

    if lof_transcripts[0]:
        is_lof, warning = True, ""
    elif True in lof_transcripts:
        is_lof, warning = True, "Truncating mutation not in the primary transcript."
    else:
        is_lof, warning = False, ""
    return is_lof, warning


#check moderate impact mutations
# check if variant is lof for the "best transcript gene"
def get_variant_moderate_plus_impact_transcripts(record_good_tx, record_best_tx):

    transcripts = [record_best_tx] + record_good_tx
    moderate_transcripts = [transcript.transcript_impact(
    ) in ["MODERATE", "HIGH"] for transcript in transcripts]
    moderate_transcripts_list = [
        transcripts[i] * moderate_transcripts[i] for i in range(len(transcripts))]

    if moderate_transcripts[0]:
        is_moderate, warning = True, ""
    elif True in moderate_transcripts:
        is_moderate, warning = True, "High/moderate impact mutation not in the primary transcript."
    else:
        is_moderate, warning = False, ""
    return is_moderate, warning, moderate_transcripts_list

# check exon for best transcript only


def which_exon(best_transcript):
    if "intron" not in best_transcript.variant_type():
        exon = best_transcript.transcript_rank()
        try:
            exon = [int(i) for i in exon]
        except ValueError:
            exon = [-1, -1]
    else:
        exon = [-1, -1]
    return exon

## region variants


def check_region_variant(civic_variant, best_transcript):
    #print(best_transcript.transcript_rank())
    type_match, region_match = False, False
    variant_type = best_transcript.variant_type()
    variant_exon = which_exon(best_transcript)
    #print(variant_exon)
    variant_impact = best_transcript.transcript_impact()
    variant_aa_pos = best_transcript.aa_pos()
    civic_type = civic_variant["type"]
    if not civic_type and variant_impact == "MODERATE":
        type_match = True
    else:
        type_match = bool(re.findall(civic_type, variant_type)) | bool(
            re.findall(civic_type.replace(" ", ""), variant_type))

    civic_region = civic_variant["region"]
    if "exon" in civic_region:
        civic_region = civic_region.replace(
            " or ", ":").replace(" and ", ":").replace("&", ":")
        civic_region = re.sub("[()]", "", civic_region)
        if "5'" in civic_region:
            which_civic_exon = [1]
        elif "3'" in civic_region:
            which_civic_exon = [variant_exon[1]]
        else:
            which_civic_exon = re.sub(
                pattern='[A-z ]', repl='', string=civic_region)
            if "-" not in which_civic_exon:
                which_civic_exon = [int(i)
                                    for i in which_civic_exon.split(":")]
            else:
                start = int(which_civic_exon.split("-")[0])
                end = int(which_civic_exon.split("-")[1]) + 1
                which_civic_exon = list(range(start, end))
        if variant_exon[0] in which_civic_exon:
            region_match = True
    # nor working for moderate plus variants
    #if "utr" in civic_region and "3'" in civic_region:
    #    region_match = "3_prime_UTR" in variant_type
    #if "utr" in civic_region and "5'" in civic_region:
    #    region_match = "5_prime_UTR" in variant_type
    if "n-terminal" in civic_region:
        if variant_aa_pos:
            region_match = variant_aa_pos[0] <= 120 and variant_aa_pos[0] / \
                variant_aa_pos[1] < 0.25
    if region_match and type_match:
        return civic_variant["variant_id"], "CIViC ID ({}) matched using region description, should be confirmed manually.".format(civic_variant["variant_id"])
    else:
        return "", ""


def read_json(json_file):
    with open(json_file) as f:
        out_dict = json.load(f)
        return out_dict


def find_broad_specicic_and_region_variants(gene_civic_variants):

    broad_variants = [i for i in gene_civic_variants if (
        i['ref_aa'] == '' and i['pos_aa'] == '' and i['alt_aa'] == '' and i['protein_hgvs'] == '' and i['transcript_hgvs'] == '' and i['region'] == '')]
    specific_variants = [i for i in gene_civic_variants if (
        i['ref_aa'] != '' or i['pos_aa'] != '' or i['alt_aa'] != '' or i['protein_hgvs'] != '' or i['transcript_hgvs'] != '')]

    region_variants = [i for i in gene_civic_variants if (
        i['region'] != '' and i not in specific_variants)]
    return broad_variants, specific_variants, region_variants


## returns reformated hgvs, ref, pos and alt
def analyse_hgvs(hgvs_string):
    hgvs_string = hgvs_string.replace("p.", "")
    if hgvs_string not in ["", "=", "?"]:
        if re.findall("del|dup|ins", hgvs_string):
            return hgvs_string
        else:
            ref = re.split("[0-9]+", hgvs_string)[0]
            pos = re.search("[0-9]+", hgvs_string).group(0)
            alt = re.split("[0-9]+", hgvs_string)[1]
            if re.findall("Ter|X|fs|FS|\*", alt):
                alt = "X"
            return "".join([ref, pos, alt])
    else:
        return hgvs_string


def change_aa(aa):
    if re.findall("Ter|X|fs|FS|\*", aa):
        return "X"

    else:
        try:
            if len(aa) == 1:
                return aa_dict[aa]
            else:
                return "".join([aa_dict[i] for i in list(aa)])
        except KeyError:
            return aa


def check_change(transcript_list, civic_variant, hgvs_type):
    var_id, warning = "", ""
    if transcript_list:
        best_empty = transcript_list[0] == ""
        transcript_list = [i for i in transcript_list if i]
        if hgvs_type == "protein":
            variant_hgvs = [analyse_hgvs(i.transcript_HGVSp())
                            for i in transcript_list]
            civic_hgvs = analyse_hgvs(civic_variant["protein_hgvs"])
            if not civic_hgvs and civic_variant["pos_aa"]:
                pos = civic_variant["pos_aa"]
                alt = civic_variant["alt_aa"]
                test = "del" in alt or "ins" in alt or "dup" in alt or ">" in alt
                if "_" not in pos and not test:
                    ref = change_aa(civic_variant["ref_aa"])
                    alt = change_aa(alt)
                    civic_hgvs = "".join([ref, civic_variant["pos_aa"], alt])
                    civic_hgvs = analyse_hgvs(civic_hgvs)
                else:
                    if "_" in civic_variant["variant"]:
                        var = civic_variant["variant"].replace("DEL", "del").replace(
                            "DUP", "dup").replace("INS", "ins").replace(">", "delins")
                        civic_hgvs = "".join([change_aa(i) for i in list(var)])
                        #print(civic_hgvs)

        else:
            variant_hgvs = [i.transcript_HGVSc() for i in transcript_list]
            civic_hgvs = civic_variant["transcript_hgvs"]

        if civic_hgvs:
            match = [bool(re.findall("^" + re.escape(civic_hgvs), i))
                     for i in variant_hgvs]
            #print(match)
            if True in match:
                var_id = civic_variant["variant_id"]
                if best_empty or not match[0]:
                    warning = "CIViC variant ({}) match to non primary transcript".format(
                        var_id)
    return var_id, warning


def lof_analysis(gene_civic_broad_variants):
    lof_var = [i for i in gene_civic_broad_variants if i["is_lof"] == 'TRUE']
    broad_lof_ids = [i['variant_id'] for i in lof_var]

    return broad_lof_ids


def match_variant(vcf_line):
    civic_ids = []
    warnings = []
    best_transcript = get_best_transcript(vcf_line)
    #print(True*best_transcript)
    gene = best_transcript.transcript_gene()
    transcripts = [Transcript(i) for i in vcf_line.info["ANN"]]
    all_good_gene_transcript = pick_all_good_ann_transcripts(transcripts, gene)
    try:
        civic_gene_dict = civic_dict[gene]
    except KeyError:
        return "", ""
    else:
        broad_civic_variants, specific_civic_variants, region_civic_variants = find_broad_specicic_and_region_variants(
            civic_gene_dict)
        ## broad lof vaiants
        variant_is_lof, lof_warning = check_record_lof(
            all_good_gene_transcript, best_transcript)
        variant_is_moderate_plus, moderate_plus_warning, moderate_plus_transcripts = get_variant_moderate_plus_impact_transcripts(
            all_good_gene_transcript, best_transcript)

        if variant_is_lof:
            broad_lof_ids = lof_analysis(broad_civic_variants)
            if broad_lof_ids:
                civic_ids.extend(broad_lof_ids)
                warnings.append(lof_warning)
        ##specific and region check then other variants
        if variant_is_moderate_plus:
            warnings.append(moderate_plus_warning)
            for s_var in specific_civic_variants:
                s_var_id, s_var_warning = check_change(
                    moderate_plus_transcripts, s_var, hgvs_type="protein")
                if s_var_id not in civic_ids:
                    civic_ids.append(s_var_id)
                    warnings.append(s_var_warning)
                s_var_id, s_var_warning = check_change(
                    moderate_plus_transcripts, s_var, hgvs_type="transcript")
                if s_var_id not in civic_ids:
                    civic_ids.append(s_var_id)
                    warnings.append(s_var_warning)
            ### region variants
            for r_var in region_civic_variants:
                r_var_id, r_var_warning = check_region_variant(
                    r_var, best_transcript)
                if r_var_id not in civic_ids:
                    civic_ids.append(r_var_id)
                    warnings.append(r_var_warning)

    return civic_ids, warnings


def main(vcf_name, vcf_writer_name):

    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_TSV_CIVIC_IDS"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description',
              "CIViC variant ID, match based on properties from the TSV file provided by thr CIViC organisation"),
             ('Source', "CIViC"),
             ('Version', '01-10-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_TSV_CIVIC_WARNINGS"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Warnings encountered during the matching procedure"),
             ('Source', 'CIViC'),
             ('Version', '01-10-2022')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        matched_ids, warnings = match_variant(record)
        matched_ids = [i for i in matched_ids if i]
        warnings = [i for i in warnings if i]
        if matched_ids:
            if not warnings:
                warnings = "."
            record.info["ISEQ_TSV_CIVIC_IDS"] = "|".join(matched_ids)
            record.info["ISEQ_TSV_CIVIC_WARNINGS"] = "|".join(warnings)
        vcf_writer.write(record)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Annotates vcf with CIViC ID')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf',
                        type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf',
                        type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--civic-json', '-c', metavar='civic_json',
                        type=str, required=True, help='CIViC json file (fromTSV)')
    args = parser.parse_args()
    civic_dict = read_json(args.civic_json)
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
    #civic_dict = read_json("/home/kt/civic/civic.json")

    #print(lof_analysis(brca_variants))
