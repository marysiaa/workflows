import argparse

__version__ = '0.0.1'

def main(f1, f2):
    with open(f1) as in_file, open(f2, "w") as out_file:
        for line in in_file:
            all_items = line.strip().split("\t")
            new_transcript_end = str(int(all_items[2])-1)
            new_cds_end = str(int(all_items[7])-1)
            new_exon_ends = ",".join([str(int(i)-1) for i in all_items[9].split(",")[:-1]])+","
            new_items = all_items[:2] + [new_transcript_end] + all_items[3:7] + [new_cds_end] + all_items[8:9] + [new_exon_ends]
            new_line = "\t".join(new_items)+"\n"
            out_file.write(new_line)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Changes refGene.sorted.bed feature ends coordinates to 0-based (needed? in AnnotSV docker)")
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input_bed', type=str, help='Path to input bed')
    parser.add_argument('-o', '--output', type=str, help='Output file location')
    arguments = parser.parse_args()
    main(arguments.input_bed, arguments.output)

