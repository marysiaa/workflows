#!/usr/bin/python3
import re
import gspread
import argparse
import pandas as pd
import yaml

__version__ = "0.2.6"


def args_parser_init():
    parser = argparse.ArgumentParser(description="""Modifies or adds health model description as a dict variable based on google spreadsheet to a python script
    More info at:
        https://workflows-dev-documentation.readthedocs.io/en/latest/Intelliseq%20tools.html#health-google-spreadsheet-to-json-py""")
    parser.add_argument('--input-credentials-json', '-c', type=str, required=True,
                        help='Credentials.json containing data necessary to get access to google developer\'s privileges')
    parser.add_argument('--input-google-spreadsheet-key', '-k', type=str, required=True,
                        help='A key to access to the google spreadsheet')
    parser.add_argument('--input-model-yaml', '-m', type=str, required=True,
                        help='Generated prs model')
    parser.add_argument('--output-model-yaml', '-o', type=str, required=True,
                        help='Modified prs model with description')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()
    return args


def set_texts(converted_json, texts):
    worksheet = texts.get_all_records()
    for key_value in worksheet:
        converted_json[key_value['key']] = key_value['value']


def set_recommendations(converted_json, recommendations):
    worksheet = recommendations.get_all_records()
    res = remove_empty_elements(worksheet)
    # to transform comma separated element to array
    for risk in res:
        set_list = []
        set_dict = dict()
        i = 0
        for key, value in risk.copy().items():
            if "set" in key:
                if "icon" in key:
                    value = "icon-" + value
                set_dict[key.split(".")[1]] = value
                del risk[key]
                i += 1
                if i % 3 == 0:
                    set_list.append(set_dict)
                    set_dict = dict()
        risk["set"] = set_list
    merged = {recommendation.pop('score_value'): recommendation for recommendation in res}
    converted_json['recommendations'] = merged


def set_other_factors(converted_json, other_factors):
    worksheet = other_factors.get_all_records()
    res = remove_empty_elements(worksheet)
    for id in res:
        factor_list = []
        factor_dict = dict()
        i = 0
        for key, value in id.copy().items():
            if key == "icon":
                id[key] = "icon-" + value
            if "factor" in key:
                factor_dict[key.split(".")[1]] = value
                del id[key]
                i += 1
                if i % 2 == 0:
                    factor_list.append(factor_dict)
                    factor_dict = dict()
        id["factors"] = factor_list
    converted_json['other_factors'] = res


def set_results_description(converted_json, results_description):
    worksheet = results_description.get_all_records()
    converted_json['results_description'] = dict()

    for result in worksheet:
        descriptions = []
        for key, value in result.copy().items():
            if "array_element" in key:
                descriptions.append(value)
                del result[key]
        result["descriptions"] = descriptions
    merged = {results_description.pop('score_value'): results_description for results_description in worksheet}
    converted_json['results_description'] = merged


def set_model_description(converted_json, model_description):
    worksheet = model_description.get_all_records()
    model_description_array = [record['model_description'] for record in worksheet]
    converted_json['model_description'] = model_description_array
    try:
        converted_json['image_top'] = worksheet[0]["image_top"]
        converted_json['image_more_about_the_disease'] = worksheet[0]["image_more_about_the_disease"]
    except KeyError:
        pass


def remove_empty_elements(list_of_dicts):
    return list(filter
                (None, ({key: val
                         for key, val in sub.items() if val}
                        for sub in list_of_dicts)))


def set_more_about_the_disease(converted_json, more_about_the_disease):
    worksheet = more_about_the_disease.get_all_records()
    res = remove_empty_elements(worksheet)

    for record in res:
        unordered = record.get('unordered_list')
        if unordered:
            record['unordered_list'] = unordered.replace('\r', '').split('\n')
            record['unordered_list'] = [string for string in record['unordered_list'] if string != ""]

    converted_json['more_about_the_disease'] = res


def set_about_the_test(converted_json, about_the_test, input_model_yaml):
    # first because there is only first non-header row important
    worksheet = about_the_test.get_all_records()[0]
    worksheet['genes'] = input_model_yaml["description"]["genes"]
    worksheet['genes'] = list(set(worksheet['genes']))
    converted_json['about_the_test'] = worksheet


def set_family(converted_json, family):
    worksheet = family.get_all_records()
    model_description_array = [record['family_description'] for record in worksheet]
    converted_json['family_description'] = model_description_array


def find_urls(string):
    regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    url = re.findall(regex, string)

    if url:
        return next(x[0] for x in url)
    else:
        return []


def get_article_from_pmid(pmid, article_type, converted_json_ref):
    citation = pd.read_json(f'https://api.ncbi.nlm.nih.gov/lit/ctxp/v1/pubmed/?format=citation&id={pmid}')
    citation = citation['ama']['format']
    try:
        doi = citation.split('doi:')[1]
        rest = citation.split('doi:')[0]
        doi_org = f"https://doi.org/{doi}"
        converted_json_ref[article_type].append(rest + f"<a href=\"{doi_org}\">doi:{doi}</a>")
    except:
        converted_json_ref[article_type].append(citation)


def set_references(converted_json, references, input_model_yaml):
    worksheet = references.get_all_records()
    res = remove_empty_elements(worksheet)

    converted_json['references'] = {}
    converted_json_ref = converted_json['references']
    converted_json_ref['data_analysis_articles'] = list()
    converted_json_ref['recommendations_articles'] = list()

    # pmids from yaml
    pmids_yaml = ["pmids", "trait_pmids"]
    for pmid_yaml in pmids_yaml:
        if input_model_yaml["description"].get(pmid_yaml):
            for pmid in input_model_yaml["description"][pmid_yaml]:
                get_article_from_pmid(pmid, "data_analysis_articles", converted_json_ref)

    for reference in res:
        if 'pmids' in reference:
            pmids = reference['pmids']
            pmids = str(pmids).replace(' ', '').split(',')
            for pmid in pmids:
                get_article_from_pmid(pmid, reference['article_type'], converted_json_ref)
        else:
            text = reference['text']
            # if url exists always should be one so the first is taken
            url = find_urls(text)
            if url:
                text = text.replace(url, f"<a href=\"{url}\">{url}</a>")

            converted_json_ref[reference['article_type']].append(text)


def get_converted_json(sheets, input_yaml):
    converted_json = dict()

    texts = sheets.worksheet('texts')
    set_texts(converted_json, texts)

    recommendations = sheets.worksheet('recommendations')
    set_recommendations(converted_json, recommendations)

    other_factors = sheets.worksheet('other_factors')
    set_other_factors(converted_json, other_factors)

    results_description = sheets.worksheet('results_description')
    set_results_description(converted_json, results_description)

    model_description = sheets.worksheet('model_description')
    set_model_description(converted_json, model_description)

    try:
        more_about_the_disease = sheets.worksheet('more_about_the_disease')
        set_more_about_the_disease(converted_json, more_about_the_disease)
    except:
        print("No sheet named more_about_the_disease")

    about_the_test = sheets.worksheet('about_the_test')
    set_about_the_test(converted_json, about_the_test, input_yaml)

    try:
        family = sheets.worksheet('family')
        set_family(converted_json, family)
    except:
        print("No sheet named family")

    references = sheets.worksheet('references')
    set_references(converted_json, references, input_yaml)

    return converted_json


def remove_keys_whitespaces(converted_json):
    for key, value in converted_json.copy().items():
        no_whitespaces_key = key.strip()
        if converted_json[key]:
            if isinstance(converted_json[key], dict):
                remove_keys_whitespaces(converted_json[key])
            elif isinstance(converted_json[key], list) and isinstance(converted_json[key][0], dict):
                for element in converted_json[key]:
                    remove_keys_whitespaces(element)

        if no_whitespaces_key != key:
            converted_json[no_whitespaces_key] = value
            del converted_json[key]


def save_to_model_yaml(input_model_yaml, output_model_yaml, converted_json):
    try:
        input_model_yaml.pop('description', None)
        input_model_yaml['descriptions'] = converted_json
        with open(output_model_yaml, 'w') as described_file:
            yaml.safe_dump(input_model_yaml, described_file)
    except yaml.YAMLError as ex:
        print(ex)


def load_yaml(input_yaml_path):
    with open(input_yaml_path, 'r') as source_file:
        return yaml.safe_load(source_file)


if __name__ == '__main__':
    args = args_parser_init()
    gc = gspread.service_account(filename=args.input_credentials_json)
    sheets = gc.open_by_key(args.input_google_spreadsheet_key)
    input_yaml = load_yaml(args.input_model_yaml)

    converted_json = get_converted_json(sheets, input_yaml)
    remove_keys_whitespaces(converted_json)

    save_to_model_yaml(input_yaml, args.output_model_yaml, converted_json)
