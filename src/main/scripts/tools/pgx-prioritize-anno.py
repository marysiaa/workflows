#!/usr/bin/env python3

import argparse
import json
import pandas as pd


__version__ = "0.1.0"


def load_json(filename: str) -> dict:
    with open (filename, "r") as f:
        return json.load(f)


def write_json(filename: str, data: dict):
    with open(filename, "w") as f:
        json.dump(data, f, indent=2)  


def prioritize_anno(merged_anno: dict, order: list) -> dict:
    prioritized = dict()
    for gene, drugs in merged_anno.items():
        for drug, drug_infos in drugs.items():
            for priority in order:
                if drug_infos.get(priority, None):
                    if prioritized.get(gene, None):
                        if prioritized[gene].get(drug, None):
                            prioritized[gene][drug][priority] = drug_infos[priority]
                        else:
                            prioritized[gene][drug] = {priority: drug_infos[priority]}
                    else:
                        prioritized[gene] = {drug: {priority: drug_infos[priority]}} 
                    break
    return prioritized


def pop_no_significance_drugs(anno: dict):
    for gene, drugs in anno.items():
        for drug in drugs.copy():
            for database, info in drugs[drug].items():
                if database == "variant_annotation":
                    anno[gene][drug][database][:] = [x for x in info if x["Significance"] == "yes"]
                    if not anno[gene][drug][database]:
                        drugs.pop(drug)


def check_if_gene_not_empty(anno: dict):
    for gene in anno.copy():
        if not anno[gene]:
            anno.pop(gene)


def load_df(filename: str) -> pd.DataFrame:
    return pd.read_csv(filename, sep="\t")


def filter_drugs(anno: dict, df: pd.DataFrame) -> dict:
    for gene, drugs in anno.copy().items():
        for drug in drugs.copy():
            if not drug.capitalize() in df["Generic Name"].tolist():
                drugs.pop(drug)
            else:
                therapeutic_area = df["Category"].loc[df['Generic Name'].isin([drug.capitalize()])].tolist()
                therapeutic_area = list(set(therapeutic_area))
                drug_class = df["Therapeutic Class"].loc[df['Generic Name'].isin([drug.capitalize()])].tolist()
                drug_class = list(set(drug_class))
                drugs[drug]["therapeutic_area"] = ", ".join(therapeutic_area)
                drugs[drug]["drug_class"] = ", ".join(drug_class)
        if not drugs:
            anno.pop(gene)


def main():
    parser = argparse.ArgumentParser(description='Prioritize annotation')
    parser.add_argument('--recommendations', type=str, required=True, help='Merged openpgx and PharmGKB recommendations')
    parser.add_argument('--drugs-to-filter', type=str, required=True, help='Drugs to filter')
    parser.add_argument('--output-filename', type=str, required=True, help='Output filename')
    args = parser.parse_args()

    # Order list
    order = ["openpgx", "clinical_annotation", "variant_annotation"]

    # Prioritized annotations
    prioritized = prioritize_anno(load_json(args.recommendations), order)

    # Remove drugs with no significance
    pop_no_significance_drugs(prioritized)

    # Remove genes with no drugs
    check_if_gene_not_empty(prioritized)

    # Filter drugs
    filter_drugs(prioritized, load_df(args.drugs_to_filter))

    # Write output
    write_json(args.output_filename, prioritized)


if __name__ == "__main__":
    main()
