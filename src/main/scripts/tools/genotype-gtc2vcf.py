#!/user/bin/env python3

import argparse
import pysam

__version__ = '0.0.1'


def check_if_genotyped(gt_field):
    return None not in gt_field


def get_sample_theta(line, sample):
    try:
        return float(line.samples[sample]["THETA"])
    except (ValueError, KeyError):
        return None


def get_float_info_field(line, field_name):
    try:
        return float(line.info[field_name])
    except (ValueError, KeyError):
        return None


def get_category_theta(line, category):
    return get_float_info_field(line, "meanTHETA_" + category)


def get_category_theta_sd(line, category):
    return get_float_info_field(line, "devTHETA_" + category)


def check_if_between_ci(sample_theta, category_theta, category_sd, dev_multiplier):
    if None in [sample_theta, category_theta, category_sd]:
        return False  # maybe this case should be treated differently?
    else:
        return (sample_theta >= category_theta - dev_multiplier * category_sd) and (
                sample_theta <= category_theta + dev_multiplier * category_sd)


def from_genotype_class_to_genotype(allele_a, bool_class_list):
    if allele_a == 1:
        genotypes = [(1, 1), (0, 1), (0, 0)]
    else:
        genotypes = [(0, 0), (0, 1), (1, 1)]
    return [genotypes[i] * bool_class_list[i] for i in range(3) if genotypes[i] * bool_class_list[i]][0]


def main(in_vcf_name, out_vcf_name, multiplier):
    in_vcf = pysam.VariantFile(filename=in_vcf_name)
    out_vcf = pysam.VariantFile(out_vcf_name, 'w',
                                header=in_vcf.header)
    # print(in_vcf.header.info.keys())
    for record in in_vcf.fetch():
        samples = record.samples
        # print(samples)
        ## get allele A
        allele_a = record.info["ALLELE_A"]
        genotype_classes_thetas = [get_category_theta(record, i) for i in ["AA", "AB", "BB"]]
        genotype_classes_thetas_sd = [get_category_theta_sd(record, i) for i in ["AA", "AB", "BB"]]
        for sample in samples:
            # print(sample)
            gt = record.samples[sample]["GT"]
            if check_if_genotyped(gt):
                pass
            else:
                ## sample theta
                theta = get_sample_theta(record, sample)

                ## check if sample theta in range of mean_theta+/- arg.range ci for any genotype class
                in_genotype_class = [
                    check_if_between_ci(theta, genotype_classes_thetas[i], genotype_classes_thetas_sd[i], multiplier)
                    for i in
                    range(3)]

                ## check if in one and only one genotype class:
                if sum(in_genotype_class) != 1:
                    pass
                else:
                    new_genotype = from_genotype_class_to_genotype(allele_a, in_genotype_class)
                    # print(new_genotype)
                    record.samples[sample]["GT"] = new_genotype

        out_vcf.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Fills genotypes in freeseek gtc2vcf output')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True,
                        help='Path to input vcf file (vcf can be bgzipped)')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True,
                        help='Path to output vcf file (vcf can be bgzipped)')
    parser.add_argument('--ci-multiplier', '-m', metavar='ci_multiplier', type=float, default=3.0,
                        help='This option decides how wide should be THETA CI for genotypes:'
                             ' theta dev times the multiplier. Default: %(default)s')
    args = parser.parse_args()
    vcf = args.input_vcf
    genotyped_vcf = args.output_vcf
    ci_multiplier = args.ci_multiplier
    main(vcf, genotyped_vcf, ci_multiplier)
