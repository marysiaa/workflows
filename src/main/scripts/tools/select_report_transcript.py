#!/usr/bin/env python3

import pysam
import argparse

__version__ = '0.0.2'

AUTHOR = "gitlab.com/kattom"


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.field = annotation_string
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_impact(self):
        return self.fields[2]

    def transcript_id(self):
        return self.fields[6].split(".")[0]

    def transcript_version(self):
        try:
            return self.fields[6].split(".")[1]
        except IndexError:
            return ""

    def transcript_warnings(self):
        return self.fields[15]

    def transcript(self):
        return self.field


# ANN: select protein coding transcripts wo errors/warnings and not nextProt annotations
# ANN: keep only transcripts with impact as high as first (non-nextProt) transcript
def find_good_transcripts(transcript_list):
    good_transcripts = []
    for transcript in transcript_list:
        if (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings() and
                'sequence_feature' not in transcript.variant_type()):
            good_transcripts.append(transcript)
    highest_impact = good_transcripts[0].transcript_impact()
    good_highest_impact_transcripts = [
        i for i in good_transcripts if i.transcript_impact() == highest_impact]
    return good_highest_impact_transcripts


# find CSQ fields indexes
def parse_csq_header(header):
    try:
        csq_header_info = header.info["CSQ"]
        description = csq_header_info.record['Description']
        csq_format = description.split("Format:")[1].strip('"').split("|")
        #  print(csq_format)
        csq_dict = {}
        for i in csq_format:
            csq_dict[i] = csq_format.index(i)
        return csq_dict

    except (KeyError, IndexError):
        return None


class TranscriptCSQ(object):
    """represents description of one transcript extracted from the CSQ field"""

    def __init__(self, annotation_string: str, index_dictionary):
        super().__init__()
        self.field = annotation_string
        self.fields = annotation_string.split('|')
        self.index_dict = index_dictionary

    def transcript_refseq(self):
        return self.fields[self.index_dict["RefSeq"]]

    def transcript_mane(self):
        return self.fields[self.index_dict["MANE_SELECT"]]

    def transcript_id(self):
        return self.fields[self.index_dict["Feature"]]

    def transcript_impact(self):
        return self.fields[self.index_dict["IMPACT"]]

    def transcript(self):
        return self.field


# select VEP transcripts: the same ids as in snpeff transcripts (validated, of highest impact) and non-empty refseq annotation
def check_vep_transcripts(ann_good_transcripts, vep_transcripts):

    good_ids = [i.transcript_id() for i in ann_good_transcripts]
    good_vep_transcripts = [i for i in vep_transcripts if
                            (i.transcript_id() in good_ids and i.transcript_mane() != "")]
    good_vep_transcripts_not_mane = [i for i in vep_transcripts if
                                     (i.transcript_id() in good_ids and i.transcript_refseq() != "" and i.transcript_mane() == "")]
    good_vep_transcripts.extend(good_vep_transcripts_not_mane)
    return good_vep_transcripts


# select mane transcript
def select_mane_transcript(all_ann_transcripts, vep_transcripts):
    try:
        vep_mane_transcript = [i for i in vep_transcripts if (
            i.transcript_mane() != "" and i.transcript_impact() != "MODIFIER")][0]
        mane_id = vep_mane_transcript.transcript_id()
        ann_mane = [i for i in all_ann_transcripts if (
            i.transcript_id() == mane_id and 'sequence_feature' not in i.variant_type())][0]
        ann_mane_list = ann_mane.transcript().split("|")
        ann_mane_list[6] = "{}.{}/{}".format(ann_mane.transcript_id(),
                                             ann_mane.transcript_version(), vep_mane_transcript.transcript_mane())
        return "|".join(ann_mane_list)
    except (IndexError, KeyError):
        return None


# select report transcript from validated transcripts list, sort ann to put selected transcript to front
def select_report_transcript(ann_transcripts, csq_transcripts, all_transcripts_unsorted):
    if not csq_transcripts:
        return (ann_transcripts[0].transcript(), None)
    else:
        csq_ids = [i.transcript_id() for i in csq_transcripts]
        # get only first transcript - probably mane
        matched_ann = [
            i for i in ann_transcripts if i.transcript_id() in csq_ids[0]]
        # mane id not in ann transcripts
        if not matched_ann:
            matched_ann = [
                i for i in ann_transcripts if i.transcript_id() in csq_ids]
        if not matched_ann:
            return (ann_transcripts[0].transcript(), None)
        else:
            first_matched_transcript_id = matched_ann[0].transcript_id()
            first_matched_transcript_version = matched_ann[0].transcript_version(
            )
            refseq_id = csq_transcripts[csq_ids.index(
                first_matched_transcript_id)].transcript_refseq()
            if "&" in refseq_id:  # ensemble transcript has more than one refseq match, for example ENSP00000270776.8 <=> NM_001304452.2&NM_002631.4&NM_001304451.2
                mane_id = csq_transcripts[csq_ids.index(
                    first_matched_transcript_id)].transcript_mane()
                if mane_id:  # check for non-empty mane_selected refseq
                    refseq_id = mane_id
                else:  # show first
                    refseq_id = refseq_id.split("&")[0]
            sorted_ann = sort_ann(all_transcripts_unsorted, matched_ann[0])
            # print(len(sorted_ann))
            first_matched_transcript = matched_ann[0].transcript().split("|")
            first_matched_transcript[6] = "{}.{}/{}".format(first_matched_transcript_id,
                                                            first_matched_transcript_version, refseq_id)
            first_matched_transcript = "|".join(first_matched_transcript)
            return (first_matched_transcript, sorted_ann)


# sort transcripts: selected for the report should be first
def sort_ann(ann_transcripts, selected_transcript):
    temp_ann = ann_transcripts[:]
    temp_ann.remove(selected_transcript)
    # print(len(temp_ann))
    sorted_ann = [selected_transcript] + temp_ann
    return ",".join([i.transcript() for i in sorted_ann])


def main(vcf_name, vcf_writer_name):
    # Add ISEQ_REPORT_ANN to the header
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_REPORT_ANN"),
             ('Number', "A"),
             ('Type', "String"),
             ('Description', 'Modified SnpEff ANN subfield (transcript) that will be displayed in report. '
                             'Format: Allele | Annotation | Annotation_Impact | Gene_Name | Gene_ID | Feature_Type |'
                             ' Feature_ID | Transcript_BioType | Rank | HGVS.c | HGVS.p | cDNA.pos / cDNA.length |'
                             'CDS.pos / CDS.length | AA.pos / AA.length | Distance | ERRORS / WARNINGS / INFO'),
             ('Source', 'SnpEff&VEP'), ('Version', 'SnpEff-5.1, VEP-104')]
    vcf_reader.header.add_meta("INFO", items=items)
    items = [('ID', "ISEQ_MANE_ANN"),
             ('Number', "A"),
             ('Type', "String"),
             ('Description', 'Modified SnpEff ANN subfield (transcript) that corresponds to MANE selected transcript. '
                             'Present only if MANE transcript was not picked to ISEQ_REPORT_ANN field.'
                             'Format: Allele | Annotation | Annotation_Impact | Gene_Name | Gene_ID | Feature_Type |'
                             ' Feature_ID | Transcript_BioType | Rank | HGVS.c | HGVS.p | cDNA.pos / cDNA.length |'
                             'CDS.pos / CDS.length | AA.pos / AA.length | Distance | ERRORS / WARNINGS / INFO'),
             ('Source', 'SnpEff&VEP'), ('Version', 'SnpEff-5.1, VEP-104')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header
    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    csq_header_dict = parse_csq_header(header_vcf)
    for record in vcf_reader.fetch():

        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except (KeyError):
            record.info["ISEQ_REPORT_ANN"] = None
            vcf_writer.write(record)
            continue

        selected_transcripts = find_good_transcripts(transcripts)
        # print(selected_transcripts)
        # all vep transcripts
        try:
            if csq_header_dict is not None:
                all_vep_transcripts = [TranscriptCSQ(
                    csq, csq_header_dict) for csq in record.info["CSQ"]]
            else:
                all_vep_transcripts = []
        except KeyError:
            all_vep_transcripts = []

        selected_vep_transcripts = check_vep_transcripts(
            selected_transcripts, all_vep_transcripts)
        # print(selected_vep_transcripts)
        report_transcript, sorted_ann = select_report_transcript(
            selected_transcripts, selected_vep_transcripts, transcripts)
        mane_transcript = select_mane_transcript(
            transcripts, all_vep_transcripts)
        record.info["ISEQ_REPORT_ANN"] = report_transcript
        # print(len(transcripts))
        if sorted_ann:
            record.info["ANN"] = sorted_ann
        if mane_transcript != report_transcript and mane_transcript:
            record.info["ISEQ_MANE_ANN"] = mane_transcript
        vcf_writer.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Selects SnpEff transcript for display in report, adds to VCF ISEQ_REPORT_ANN field')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf',
                        type=str, required=True, help='Input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf',
                        type=str, required=True, help='Output vcf.gz file')

    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)
