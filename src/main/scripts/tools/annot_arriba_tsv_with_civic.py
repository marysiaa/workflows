#!/usr/bin/env python3

import pandas as pd
import argparse

__version__ = '0.0.1'

def main(civic_df, arriba_df):
    two_genes_df = civic_df[civic_df["gene1"].notna() & civic_df["gene2"].notna()]
    one_gene_df = civic_df[civic_df["gene1"].notna() & civic_df["gene2"].isna()]
    

    annotated1_df = pd.merge(left=arriba_df, right=two_genes_df, left_on=["#gene1", "gene2"],
                         right_on=["gene1", "gene2"], how="left")
    annotated2_df = pd.merge(left=arriba_df, right=one_gene_df, left_on=["#gene1"],
                         right_on=["gene1"], how="left")
    annotated3_df = pd.merge(left=arriba_df, right=one_gene_df, left_on=["gene2"],
                         right_on=["gene1"], how="left")

    annotated_df = pd.concat([annotated1_df, annotated2_df, annotated3_df])
    annotated_df = annotated_df[annotated_df["variant_id"].notna()]
    annotated_df["variant_id"] = annotated_df["variant_id"].astype(int)
    annotated_df = annotated_df.applymap(str)
    #print(annotated_df[['#gene1', 'gene2','variant_id']])
    output = annotated_df.groupby(["breakpoint1", "breakpoint2", "read_identifiers"]).agg(
        civic_variant_id=('variant_id', '|'.join),
        civic_additional_mut=('other_mut', '|'.join),
        civic_additional_mut_gene=('other_mut_gene', '|'.join),
        civic_additional_mut_details=('other_mut_details', '|'.join),
        civic_additional_mut_p_hgvs=('other_mut_protein_hgvs', '|'.join),
        civic_additional_mut_c_hgvs=('other_mut_transcript_hgvs', '|'.join),
        civic_fusion_region=('fusion_region', '|'.join))
    result = pd.merge(left=arriba_df, right=output, on=[
                  "breakpoint1", "breakpoint2", "read_identifiers"], how="left")
    result.replace("nan\|", "|", regex = True, inplace=True) 
    result.replace("\|nan", "|", regex = True, inplace=True) 
    result.replace("^\|+$", "", regex = True, inplace=True)            
                  
    #print(result)
    return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Annotates arriba output file with civic ids.')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('--arriba-tsv', '-a', required=True,
                        type=str, help='Path to arriba output tsv table')
    parser.add_argument('--civic-fusions', '-c', required=True,
                        type=str, help='Path to CIViC fusion tsv file')
    parser.add_argument('--output', '-o', required=True,
                        type=str, help='Output file path')
    args = parser.parse_args()

    civic = pd.read_csv(args.civic_fusions, sep="\t")
    arriba = pd.read_csv(args.arriba_tsv, sep="\t")

    out_df = main(civic, arriba)
    out_df.to_csv(args.output, sep='\t')
