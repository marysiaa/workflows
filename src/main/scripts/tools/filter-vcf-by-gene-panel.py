#!/usr/bin/python3

__version__ = '0.0.3'

import argparse
from sys import stdin
import json

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""FILTERING VCF BY GENE PANEL
        _
      _<_/_
   __/    _>
  '\  '  |
    \___/
    /+++\
 o=|..|..|
   | o/..|
0==|+++++|
 0======/

The script takes input VCF file as stdin and gene panel file in JSON format as argument and perform the following actions:
  - filtering VCF file, so only variant in genes specified in genes panel remains (this may be switch off by adding --skip_filter flag)
  - annotating VCF file with ISEQ_GENE_PANEL_SCORE and ISEQ_GENE_PANEL_TYPE field

This gene panel JSON should be formatted as follows:

    [
        {"name":"<gene_symbol_1>","score":<score>,"type":"<gene_panel_type_a_gene_originates>"},
        {"name":"<gene_symbol_2>","score":<score>,"type":"<gene_panel_type_a_gene_originates>"},
        (...)
        {"name":"<gene_symbol_n>","score":<score>,"type":"<gene_panel_type_a_gene_originates>"}
    ]

 where:

  + score should be in the range from 0 to 100
  + posible gene panels types are:
      - phenotypes panel - type should be "phenotype"
      - user-defined panel - type should be "phenotype" and score should be "100"
      - diseases panel = type should be "<disease_name>" or "<group_of_diseases_name>"

 USE EXAMPLE:
   zcat example.vcf.gz | python3 filter-vcf-by-gene-panel.py gene-panel.json [--skip_filter] | bgzip > example.filtered.vcf.gz

 """)

parser.add_argument('GENE_PANEL_JSON', type=str, help="path to input gene panel file, it should be json "
                                                      "with structure {name: WNT5A, score: 20.0},")
parser.add_argument('--skip_filter', action="store_true", help="Use this flag to ommit filtering step")
parser.add_argument('-v', '--version', action='version', version=__version__)
args = parser.parse_args()

paneljson = json.loads(open(args.GENE_PANEL_JSON).read())
panel = set()
info = {}

for geneline in paneljson:
    panel.add(geneline["name"])
    info[geneline["name"]] = [round(geneline["score"], 2), geneline["type"].replace(" ", "^")]

for line in stdin:
    if line.startswith('##'):
        print(line.strip())
    elif line.startswith('#'):
        print('##INFO=<ID=ISEQ_GENE_PANEL_SCORE,Number=A,Type=String,Description="Gene panel fitness score">')
        print('##INFO=<ID=ISEQ_GENE_PANEL_TYPE,Number=A,Type=String,Description="Gene panel type: phenotype, name of the disease, user-defined">')
        print(line.strip())
    else:
        lines = line.strip().split('\t')

        try:
            genes = set([i for i in lines[7].split(';') if i.startswith('ISEQ_GENES_NAMES')][0].replace("ISEQ_GENES_NAMES=", "").split(":"))
            if bool(panel.intersection(genes)):
                scores = ""
                types = ""
                for gene in genes:
                    try:
                        scores += str(info[gene][0])
                        types += str(info[gene][1])
                    except:
                        scores += "."
                        types += "."
                    scores += ":"
                    types += ":"

                lines[7] += ';ISEQ_GENE_PANEL_SCORE=' + scores[:-1] + ';ISEQ_GENE_PANEL_TYPE=' + types[:-1]
                print('\t'.join(lines))
            else:
                if args.skip_filter:
                    print('\t'.join(lines))
                else:
                    pass
        except:
            if args.skip_filter:
                print('\t'.join(lines))
            else:
                pass

