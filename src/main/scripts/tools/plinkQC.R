#!/usr/bin/env Rscript

library(plinkQC)
library(argparse)
library(ggplot2)
library(jsonlite)
library(plotly)
library(htmlwidgets)

version <- "1.2.4"

parser <- ArgumentParser(description = "")
parser$add_argument("-v", "--version",
  action = "version",
  version = paste(sub(".*=.*/", "", commandArgs()[4]), version)
)
parser$add_argument("--id", type = "character")
parser$add_argument("--pwd", type = "character")
parser$add_argument("--imissTh", type = "double")
parser$add_argument("--hetTh", type = "double")
parser$add_argument("--highIBDTh", type = "double")
parser$add_argument("--snps", type = "character")

args <- parser$parse_args()

current_path <- args$pwd
plink_path <- "/intelliseqtools/plink"

check_sex <- check_sex(
  indir = current_path,
  qcdir = current_path,
  name = args$id,
  path2plink = plink_path,
  interactive = FALSE,
  verbose = TRUE
)

hovertext <- paste(
  check_sex$plot_data$IID,
  paste("PEDSEX:", check_sex$plot_data$PEDSEX, sep = " "),
  paste("SNPSEX:", check_sex$plot_data$SNPSEX, sep = " "),
  paste("Status:", check_sex$plot_data$STATUS, sep = " "),
  paste("F:", check_sex$plot_data$F, sep = " "),
  sep = "<br>"
)

plot1 <- plot_ly(
  x = check_sex$plot_data$PEDSEX,
  y = check_sex$plot_data$F,
  name = check_sex$plot_data$LABELSEX,
  type = "scatter",
  text = hovertext,
  hoverinfo = "text"
) %>%
  layout(
    title = "Check assigned sex versus SNP sex",
    xaxis = list(
      title = "Reported Sex (PEDSEX)",
      showline = TRUE,
      mirror = "ticks",
      range = c(0.4, 2.6),
      tickformat = "d",
      ticktext = list('Male', 'Female'),
      tickvals = list(1, 2),
      tickmode = "array"
    ),
    yaxis = list(
      title = "ChrX heterozygosity",
      showline = TRUE,
      mirror = "ticks",
      zerolinecolor = toRGB("gray")
    )
  ) %>%
  add_segments(
    x = 0.8,
    xend = 1.2,
    y = 0.8,
    yend = ~0.8,
    hoverinfo = "text",
    text = "Male limit",
    line = list(
      color = "rgb(205, 12, 24)",
      width = 1,
      dash = "dash"
    ),
    showlegend = F
  ) %>%
  add_segments(
    x = 1.8,
    xend = 2.2,
    y = 0.2,
    yend = ~0.2,
    hoverinfo = "text",
    text = "Female limit",
    line = list(
      color = "rgb(205, 12, 24)",
      width = 1,
      dash = "dash"
    ),
    showlegend = FALSE
  )

saveWidget(
  widget = partial_bundle(plot1),
  file = paste(args$id, "check_sex-perindividual_report.html", sep = "-")
)

check_het_and_miss <- check_het_and_miss(
  indir = current_path,
  qcdir = current_path,
  name = args$id,
  path2plink = plink_path,
  interactive = FALSE,
  verbose = TRUE,
  imissTh = args$imissTh,
  hetTh = args$hetTh
)

Fmean <- mean(check_het_and_miss$plot_data$F)
Fsd <- sd(check_het_and_miss$plot_data$F)
xrange <- c(
  min(min(check_het_and_miss$plot_data$F_MISS) - 0.05, -0.009),
  max(max(check_het_and_miss$plot_data$F_MISS) + 0.05, args$imissTh + 0.01, 0.06)
)
yrange <- c(Fmean - (args$hetTh + 0.5) * Fsd, Fmean + (args$hetTh + 0.5) * Fsd)
xtickvals <- c(seq(ceiling(xrange[1] * 2) * 0.5, xrange[2], 0.5), c(0.01, 0.03, 0.05, 0.1, args$imissTh))
ytickvals <- c(Fmean - 3 * Fsd, Fmean - 2 * Fsd, Fmean - Fsd, Fmean + Fsd, Fmean + 2 * Fsd, Fmean + 3 * Fsd)
yticktext <- c("-3", "-2", "-1", "+1", "+2", "+3")
yticktext_hover <- c("mean-3sd", "mean-2sd", "mean-1sd", "mean+1sd", "mean+2sd", "mean+3sd")

hovertext <- paste(check_het_and_miss$plot_data$IID,
  paste("Type:", check_het_and_miss$plot_data$type, sep = " "),
  sep = "<br>"
)

plot2 <- plot_ly(
  x = check_het_and_miss$plot_data$F_MISS,
  y = check_het_and_miss$plot_data$F,
  name = check_het_and_miss$plot_data$type,
  type = "scatter",
  text = hovertext,
  hoverinfo = "text"
) %>%
  layout(
    title = "Heterozygosity by Missingness across samples",
    xaxis = list(
      title = "Proportion of missing SNPs",
      showline = TRUE,
      mirror = "ticks",
      range = xrange,
      tickvals = xtickvals,
      tickangle = 0
    ),
    yaxis = list(
      title = "Heterozygosity rate (and sd)",
      showline = TRUE,
      mirror = "ticks",
      range = yrange,
      zerolinecolor = toRGB("gray"),
      tickvals = ytickvals,
      ticktext = yticktext,
      tickmode = "array"
    )
  ) %>%
  add_segments(
    x = args$imissTh,
    xend = args$imissTh,
    y = yrange[1],
    yend = yrange[2],
    hoverinfo = "text",
    text = "imissTh",
    line = list(
      color = "rgb(205, 12, 24)",
      width = 1,
      dash = "dash"
    ),
    showlegend = FALSE
  )

for (i in 1:6) {
  plot2 <- plot2 %>%
    add_segments(
      x = xrange[1],
      xend = xrange[2],
      y = ytickvals[i],
      yend = ytickvals[i],
      hoverinfo = "text",
      text = paste(yticktext_hover[i], ytickvals[i], sep = "<br>"),
      line = list(color = toRGB("gray"), width = 1, dash = "dash"),
      showlegend = FALSE
    )
}

plot2 <- plot2 %>%
  add_segments(
    x = xrange[1],
    xend = xrange[2],
    y = Fmean + args$hetTh * Fsd,
    yend = Fmean + args$hetTh * Fsd,
    hoverinfo = "text",
    text = paste(
      paste("mean+", args$hetTh, "*sd", sep = ""),
      Fmean + args$hetTh * Fsd,
      sep = "<br>"
    ),
    line = list(color = toRGB("red"), width = 1, dash = "dash"),
    showlegend = FALSE
  ) %>%
  add_segments(
    x = xrange[1],
    xend = xrange[2],
    y = Fmean - args$hetTh * Fsd,
    yend = Fmean - args$hetTh * Fsd,
    hoverinfo = "text",
    text = paste(
      paste("mean-", args$hetTh, "*sd", sep = ""),
      Fmean - args$hetTh * Fsd,
      sep = "<br>"
    ),
    line = list(
      color = toRGB("red"),
      width = 1,
      dash = "dash"
    ),
    showlegend = FALSE
  )

saveWidget(
  widget = partial_bundle(plot2),
  file = paste(args$id, "het_and_miss-perIndividual_report.html", sep = "-")
)

# Trycatch error Error: No variants remaining after --extract. 
# It occurs when the same gtc files were used. 
check_relatedness_result <- tryCatch(
  expr = {
    check_relatedness(
      indir = current_path,
      qcdir = current_path,
      name = args$id,
      path2plink = plink_path,
      interactive = FALSE,
      verbose = TRUE,
      highIBDTh = args$highIBDTh,
      imissTh = args$imissTh,
      genomebuild = "hg38"
    )
  },
  error = function(e) {
    message("Function check_relatedness causes the fallowing error:")
    print(e)
    return(list(
      p_IBD = NA,
      fail_highIBD = NA,
      failIDs = NA
    ))
  }
)

if (!is.na(check_relatedness_result$p_IBD)) {
  ggsave(
    plot = check_relatedness_result$p_IBD,
    filename = paste(args$id, "relatedness-perIndividual_report.jpg", sep = "-")
  )
} 

check_hwe <- check_hwe(
  indir = current_path,
  qcdir = current_path,
  name = args$id,
  path2plink = plink_path,
  interactive = FALSE,
  verbose = TRUE
)

ggsave(
  plot = check_hwe$p_hwe,
  filename = paste(args$id, "hwe-perMarkerQC_report.jpg", sep = "-")
)

check_maf <- check_maf(
  indir = current_path,
  qcdir = current_path,
  name = args$id,
  path2plink = plink_path,
  interactive = FALSE,
  verbose = TRUE
)

ggsave(
  plot = check_maf$p_maf,
  filename = paste(args$id, "maf-perMarkerQC_report.jpg", sep = "-")
)

snps_path <- args$snps
sample_list <- check_sex$plot_data[c("FID","IID")]

for (row in 1:nrow(sample_list)){
  write.table(sample_list[row,], file = "individual.tsv", sep="\t",  col.names=FALSE, row.names = FALSE)
  check_snp_missingness <- check_snp_missingness(
    indir = current_path,
    qcdir = current_path,
    name = args$id,
    path2plink = plink_path,
    interactive = FALSE,
    verbose = TRUE,
    extract_markers = snps_path,
    keep_individuals = "individual.tsv"
  )
  ggsave(
    plot = check_snp_missingness$p_lmiss,
    filename = paste(args$id, sample_list[row,"IID"], "snp_missingness-perMarkerQC_report.jpg", sep = "-")
  )
}

perMarkerQC <- perMarkerQC(
  indir = current_path,
  qcdir = current_path,
  name = args$id,
  path2plink = plink_path,
  interactive = FALSE,
  verbose = TRUE
)
ggsave(
  plot = perMarkerQC$p_markerQC,
  filename = paste(args$id, "perMarkerQC_report.jpg", sep = "-")
)

perIndividual <- list(
  "fail_sex" = check_sex$fail_sex,
  "fail_imis" = check_het_and_miss$fail_imis,
  "fail_het" = check_het_and_miss$fail_het,
  "fail_high_IBD" = check_relatedness_result$fail_highIBD,
  "failIDs" = check_relatedness_result$failIDs
)
perIndividual_json <- toJSON(perIndividual)
write(perIndividual_json,
  file = paste(args$id, "perIndividual_vcf-qc.json", sep = "-")
)

perMarker <- list(
  "fail_hwe" = check_hwe$fail_hwe,
  "fail_maf" = check_maf$fail_maf,
  "fail_missingness" = check_snp_missingness$fail_missingness
)
perMarker_json <- toJSON(perMarker)

write(perMarker_json,
  file = paste(args$id, "perMarker_vcf-qc.json", sep = "-")
)

write.table(
  data.frame(
    id = check_sex$plot_data$IID,
    sex = check_sex$plot_data$F,
    sexpass = ifelse(check_sex$plot_data$STATUS == "OK", "passed", "failed"),
    miss = check_het_and_miss$plot_data$F_MISS,
    misspass = ifelse(check_het_and_miss$plot_data$F_MISS < args$imissTh, "passed", "failed"),
    het = check_het_and_miss$plot_data$F,
    hetpass = ifelse(
      test = check_het_and_miss$plot_data$F > Fmean - args$hetTh * Fsd &&
        check_het_and_miss$plot_data$F < Fmean + args$hetTh * Fsd,
      yes = "passed",
      no = "failed"
    ),
    sexplot = paste(args$id, "check_sex-perindividual_report.html", sep = "-"),
    hetmissplot = paste(args$id, "het_and_miss-perIndividual_report.html", sep = "-")
  ),
  file = "qc.tsv",
  quote = FALSE,
  row.names = FALSE,
  sep = "\t"
)
