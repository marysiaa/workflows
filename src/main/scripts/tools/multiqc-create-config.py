#!/usr/bin/env python3

import argparse
import json

__version__ = '1.0.1'

def load_json(input_json) -> dict:
    with open(input_json) as file:
        return json.load(file)

def create_sample_info_table(sample_info, info_dict):
    output = "report_header_info:"
    for info in sample_info:
        if info in info_dict:
            output = output + "\n - " + info_dict[info] + ": \"" + sample_info[info] + "\""
    return output

def create_config_file(type, table, report_title):
    with open('multiqc_config.yaml', 'w') as file:
        file.write(table)
        try:
            title = report_title.replace("_", " ")
            file.write("\ntitle: \"" + title + "\"")

        except AttributeError:
            pass

        file.write("""
custom_plot_config:
  picard_percentage_target_bases:
    xmax: 750""")

        if type in ("genome", "exome"):
            file.write("""
show_hide_buttons:
    - Hide reference samples
show_hide_mode:
    - hide
show_hide_patterns:
    - ref_sample_""")

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Creates multiqc config file for including')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--sample-info', '-s', type=str, required=False, metavar='sample_info', help='Sample info json')
    parser.add_argument('--info-dict', '-d', type=str, required=False, metavar='sample_info', help='Sample info dictionary')
    parser.add_argument('--type', '-t', type=str, required=False, metavar='type', help='Analysis type: exome or genome or target')
    parser.add_argument('--title', '-n', type=str, required=False, help='String with report title (_ will be converted to spaces)')
    args = parser.parse_args()

    if args.sample_info is not None:
        sample_info_table = create_sample_info_table(load_json(args.sample_info), load_json(args.info_dict))
    else:
        sample_info_table = ""

    if args.type is not None:
        analysis_type = args.type
    else:
        analysis_type = ""

    create_config_file(analysis_type, sample_info_table, args.title)


