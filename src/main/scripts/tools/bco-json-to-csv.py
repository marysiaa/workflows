#!/usr/bin/python3
import sys
import argparse
import pandas as pd
import json
import os

__version__ = "0.0.5"
AUTHOR = "gitlab.com/olaf.tomaszewski"

parser = argparse.ArgumentParser(description='converts bco.json to bco.csv')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-bco-json', '-i', metavar='input_bco_json', type=str, required=True, help='bco file in json format')
parser.add_argument('--output-csv', '-o', metavar='output_csv', type=str, required=True, help='bco file in csv format')
args = parser.parse_args()


def split_list(domain, interval):
   return [domain[i:i+interval] for i in range(0, len(domain), interval)]


def get_json(input_bco_json):
    with open(input_bco_json) as file:
        bco_json = json.load(file)
        return bco_json


def get_bco_df(grouped_tasks):
    bco_df = pd.DataFrame(columns=['Task name', 'Cpu', 'Memory [GB]', 'Time [s]', 'Docker image',
                                   'Docker image size', 'Module name'])

    for task_params in grouped_tasks:
        bco_dict = dict()
        bco_dict['Task name'] = task_params[0]['name']
        bco_dict['Cpu'] = task_params[0]['value']
        bco_dict['Memory [GB]'] = task_params[1]['value']
        bco_dict['Time [s]'] = task_params[2]['value']
        bco_dict['Docker image'] = task_params[3]['value']
        bco_dict['Docker image size'] = task_params[4]['value']
        bco_dict['Module name'] = task_params[0].get('module_name', task_params[0]['name'])

        bco_df = bco_df.append(bco_dict, ignore_index=True)

    return bco_df


if __name__ == "__main__":
    bco_json = get_json(args.input_bco_json)

    domain = bco_json['parametric_domain']
    grouped_tasks = split_list(domain, 5)

    bco_df = get_bco_df(grouped_tasks)
    bco_df = bco_df.sort_values(by=["Module name", "Task name"])
    bco_df = bco_df.set_index('Task name')
    bco_df.to_csv(args.output_csv + ".csv")
