#!/usr/bin/env python3

import argparse
from signal import signal, SIGPIPE, SIG_DFL
import pysam
from numpy import *
signal(SIGPIPE, SIG_DFL)

__version__ = '1.0.1'

def coveragetoletter(input_bam, verbose):
    #line under does the same as: samtools depth -r "chrY:2786989-2787603" input_bam
    coverage=pysam.depth("-r", "chrY:2786989-2787603", input_bam).splitlines()
    list=[]
    for line in coverage:
        list.append(int(line.split()[2]))

    if list==[]:
        min=0
        max=0
        av=0
    else:
        min=amin(list)
        max=amax(list)
        av=average(list)

    if verbose:
        if min>=4 and max>15 and av>=8:
            return "Biological sex: male (M). Coverage minimum: %s, maximum: %s, average: %s" % (min, max, av)
        else:
            return "Biological sex: female (F). Coverage minimum: %s, maximum: %s, average: %s" % (min, max, av)
    else:
        if min>=4 and max>15 and av>=8:
            sex="M"
        elif min<4 and max<=15 and av<8:
            sex="F"
        else :
            sex="cannot be determined by this method"
        return "{\"method\":\"SRY gene coverage\\n(chrY:2786989-2787603)\", \"result\": \""+sex+"\", \"measure_type\" : \"Coverage statistics of SRY gene region\", \"measure_value\": \"Average: "+str(round(av,2))+",\\nminimum: "+str(min)+",\\nmaximum: "+str(max)+".\", \"thresholds\":\"Male: av >= 8, min >= 4, max > 15.\\nFemale: av < 8, min < 4, max <= 15.\"}"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
                                 """
            _                ___       _.--._
            \`.|\..----...-'`   `-._.-' _..  |
            /  ' `         ,       __.-'   '-'
            )/' _/     \   `-_,   /
            `-'" `"\_  ,_.-;_.-\_ ',
                _.-'_./   {_.'   ; /
               {_.-``-'         {_/
     ============================================
     -----------Is it a boy or a girl?-----------
     ============================================

     Script prints out if a sample is male (M) or
     female (F).
     * It calculates depth coverage of SRY gene
       region (GRCH38: chrY:2786989-2787603),
     * Takes into account max/minimum and average
       coverage (the tresholds were obtained by
       checking ~200 known samples).     """)

    parser.add_argument('INPUT_BAM', type=str, help="path to input bam file")
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-verbose', action="store_true", help="if true: print human-readable, full answer")
    #parser.add_argument('-c', type=float, default=150, help="change mean target coverage, default: 150X")
    #parser.add_argument('-wgs', type=str, help="use when analysing WGS")

    args = parser.parse_args()
    print(coveragetoletter(args.INPUT_BAM, args.verbose))
