#!/usr/bin/python3
import argparse
import sys
from Bio import SeqIO

VERSION="0.0.1"
AUTHOR = "gitlab.com/olaf.tomaszewski"

if sys.argv[1] == "--version":
    print("fasta-filter.py version " + VERSION)
    exit(0)

parser = argparse.ArgumentParser(description='annotates vcf with tsv.gz')
parser.add_argument('--input-fasta', '-i', metavar='input_fasta', type=str, required=True, help='input fasta file')
parser.add_argument('--input-txt', '-j', metavar='input_txt', type=str, required=True, help='input txt containing list of chromosomes')
parser.add_argument('--output-fasta', '-o', metavar='output_fasta', type=str, required=True, help='output filtered fasta file')
args = parser.parse_args()


def get_chromosomes_from_txt_file():
    with open(args.input_txt, "r") as file:
        return list(map(lambda x: x.strip('\n'), file.readlines()))


def filter_fasta_file():
    chromosomes = get_chromosomes_from_txt_file()
    input_fasta_sequences = SeqIO.parse(open(args.input_fasta), "fasta")

    output_fasta_sequences = [fasta for fasta in input_fasta_sequences if fasta.id in chromosomes]

    SeqIO.write(output_fasta_sequences, args.output_fasta, "fasta")


if __name__ == "__main__":
    filter_fasta_file()

