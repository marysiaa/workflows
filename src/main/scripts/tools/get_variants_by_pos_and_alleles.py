#!/usr/bin/python3

import pysam
import argparse

__version__ = '0.0.1'

AUTHOR = "gitlab.com/kattom"


def check_position_and_alleles(line, tsv_file, no_chr):

    chrom = line.contig
    if no_chr:
        chrom = "chr" + chrom
    pos = int(line.pos)
    ref = line.ref
    alt = line.alts[0]

    result = 0
    try:
        for row in tsv_file.fetch(chrom, pos - 1, pos):
            # print(str(row))
            split_line = str(row).strip().split("\t")
            if ref == split_line[2] and alt == split_line[3] and chrom == split_line[0] and pos == int(split_line[1]):
                result += 1
        return result == 1
    except ValueError:
        return False


def main(vcf_name, vcf_writer_name):
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    header_vcf = vcf_reader.header
    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        if check_position_and_alleles(record, tsv, no_chr):
            vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Selects variants from vcf, uses list of with position and alleles')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--variant-tsv', '-t', metavar='variant_tsv', type=str, required=True,
                        help='tsv file with variant list, chrom, pos, ref, alt, bgzipped and indexed')
    parser.add_argument('--no-chr-prefix', '-n', action='store_true',
                        help='Input vcf with Beagle style chromosome names? (without chr prefix)')

    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    tsv = pysam.TabixFile(args.variant_tsv)
    no_chr = args.no_chr_prefix
    main(in_vcf, out_vcf)
