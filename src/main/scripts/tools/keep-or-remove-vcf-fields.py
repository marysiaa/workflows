#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(description='Keeps or removes only specified fields in INFO section of VCF file, optionally may '
                                             'remove FORMAT column and genotype INFO. Names of fields to keep or remove can be '
                                             'supplied as a file or via command line as a comma delimited list. Default mode: keep fields.')

parser.add_argument('-v', '--version', action='version', version=__version__)
parser.add_argument('-g', '--remove-format-and-genotype', dest='remove_genotypes', action='store_true', help="Remove FORMAT column and genotype info")
parser.add_argument('-k', '--keep-empty-info-lines', dest='remove_empty_line', action='store_true', help="Do not print line, if after removing redundant fields, INFO colum is empty.")
parser.add_argument('-r', '--remove-fields', dest='remove_mode', action='store_true', help="Supply fields names to remove rather than to keep")

group = parser.add_mutually_exclusive_group()
group.add_argument('-f', '--txt-file-with-fields', dest='path_to_field_names_file', type=str, help="Path to a TXT file with names of INFO fields to keep or remove (one field name per line)")
group.add_argument('-l', '--comma-delimited-list-of-fields', dest='list_of_field_names', type=str, help="Comma delimited list of names of INFO fields to keep or remove.")

args = parser.parse_args()

remove_genotypes = args.remove_genotypes
path_to_field_names_file = args.path_to_field_names_file
list_of_field_names = args.list_of_field_names
remove_empty_line = args.remove_empty_line
remove_mode = args.remove_mode

fields_names_list = []

if path_to_field_names_file:

    for field_name in open(path_to_field_names_file, 'r'):
        fields_names_list.append(field_name.strip())

if list_of_field_names:
    fields_names_list = list_of_field_names.strip().split(',')

fields_names_tuple_equal_sign = tuple([x + '=' for x in fields_names_list])

for line in stdin:

    if line.startswith('##INFO='):

        if remove_mode:

            print_line = True

            for field_name in ['##INFO=<ID=' + x + ',' for x in fields_names_list]:
                if line.count(field_name) > 0:
                    print_line = False
                    break

            if print_line:
                print(line.strip())

        else:

            for field_name in ['##INFO=<ID=' + x + ',' for x in fields_names_list]:
                if line.count(field_name) > 0:
                    print(line.strip())

    elif line.startswith('##'):

        print(line.strip())

    elif line.startswith('#C'):

        if remove_genotypes:
            print('\t'.join(line.strip().split()[0:8]))
        else:
            print(line.strip())

    else:

        line = line.strip().split()
        INFO = line[7].split(';')
        new_INFO = []

        if remove_mode:

            for info in INFO:

                if not (info.startswith(fields_names_tuple_equal_sign) or info in fields_names_list):
                    new_INFO.append(info)

        else:
            for info in INFO:

                if info.startswith(fields_names_tuple_equal_sign) or info in fields_names_list:
                    new_INFO.append(info)

        line[7] = ';'.join(new_INFO)

        if not line[7]:
            line[7] = '.'

        if not remove_empty_line:

            if remove_genotypes:
                print('\t'.join(line[0:8]))
            else:
                print('\t'.join(line))

        else:

            if line[7] != '.':

                if remove_genotypes:
                    print('\t'.join(line[0:8]))
                else:
                    print('\t'.join(line))
