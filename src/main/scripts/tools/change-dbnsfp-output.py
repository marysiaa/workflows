#!/usr/bin/env python3
from sys import stdin
import re
from signal import signal, SIGPIPE, SIG_DFL
import argparse

__version__ = '1.0.3'

parser = argparse.ArgumentParser(description='Changes dbNSFP output')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('-d', '--database_version', required=True, type=str,
                    help='dbNSFP database version, for example v4.2c')
args = parser.parse_args()

signal(SIGPIPE, SIG_DFL)
info_add = ". Commas in original dbNSFP field are replaced by colons. " \
           "More detailed description can be found in" \
           " https://sites.google.com/site/jpopgen/dbNSFP\",Source=\"dbNSFP\",Version=\"{}\">".format(
    args.database_version)

old_eigen = "raw_coding,Number=A,Type=String"
new_eigen = "raw_coding,Number=A,Type=Float"
old_bayas = "BayesDel_addAF_score,Number=A,Type=String"
new_bayas = "BayesDel_addAF_score,Number=A,Type=Float"
old_dann = "DANN_score,Number=A,Type=String"
new_dann = "DANN_score,Number=A,Type=Float"
old_deogen2 = "DEOGEN2_score,Number=A,Type=Float"
new_deogen2 = "DEOGEN2_score,Number=A,Type=String"
old_list2 = "LIST_S2_score,Number=A,Type=Float"
new_list2 = "LIST_S2_score,Number=A,Type=String"
old_mvp = "MVP_score,Number=A,Type=Float"
new_mvp = "MVP_score,Number=A,Type=String"
old_mcap = "M_CAP_score,Number=A,Type=String"
new_mcap = "M_CAP_score,Number=A,Type=Float"
old_mutass = "MutationAssessor_score,Number=A,Type=Float"
new_mutass = "MutationAssessor_score,Number=A,Type=String"
old_muttas = "MutationTaster_score,Number=A,Type=Float"
new_muttas = "MutationTaster_score,Number=A,Type=String"
old_primateai = "PrimateAI_score,Number=A,Type=String"
new_primateai = "PrimateAI_score,Number=A,Type=Float"
old_sift = "SIFT4G_score,Number=A,Type=Float"
new_sift = "SIFT4G_score,Number=A,Type=String"
old_fathmm = "fathmm_MKL_coding_score,Number=A,Type=String"
new_fathmm = "fathmm_MKL_coding_score,Number=A,Type=Float"
old_aloft_dom = "Aloft_prob_Dominant,Number=A,Type=Float"
new_aloft_dom = "Aloft_prob_Dominant,Number=A,Type=String"
old_aloft_rec = "Aloft_prob_Recessive,Number=A,Type=Float"
new_aloft_rec = "Aloft_prob_Recessive,Number=A,Type=String"
old_aloft_tol = "Aloft_prob_Tolerant,Number=A,Type=Float"
new_aloft_tol = "Aloft_prob_Tolerant,Number=A,Type=String"

for raw_line in stdin:
    if raw_line.startswith("#"):
        if raw_line.startswith("##INFO=<ID=dbNSFP"):
            print((re.sub("\">", info_add,
                re.sub(old_bayas, new_bayas,
                re.sub(old_dann, new_dann,
                re.sub(old_deogen2, new_deogen2,
                re.sub(old_list2, new_list2,
                re.sub(old_eigen, new_eigen,
                re.sub(old_mvp, new_mvp,
                re.sub(old_mcap, new_mcap,
                re.sub(old_mutass, new_mutass,
                re.sub(old_muttas, new_muttas,
                re.sub(old_primateai, new_primateai,
                re.sub(old_sift, new_sift,
                re.sub(old_fathmm, new_fathmm,
                re.sub(old_aloft_dom, new_aloft_dom,
                re.sub(old_aloft_rec, new_aloft_rec,
                re.sub(old_aloft_tol, new_aloft_tol,
                re.sub("Number=.", "Number=A",
                re.sub("Type=Character", "Type=String",
                raw_line)))))))))))))))))).strip()))
        else:
            print(raw_line.strip())
    else:
        line = raw_line.split("\t", 8)
        INFO = line[7].strip()
        INFO_colon = []
        for info in INFO.split(';'):
            if info.startswith("dbNSFP"):
                INFO_colon.append(info.replace(",", ":"))
            else:
                INFO_colon.append(info)

        line[7] = ";".join(INFO_colon)

        print(("\t".join(line)).strip())
