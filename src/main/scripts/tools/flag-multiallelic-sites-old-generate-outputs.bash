flag_multiallelic_sites_py="/home/ltw/Intelliseq/workflows/src/main/python/tools/flag-multiallelic-sites.py" 
ref_fasta="$TASK_TEST_RESOURCES/reference-genomes/grch38-no-alt-analysis-set/GRCh38_no_alt_analysis_set.fa"

zcat 268_318_322-test.vcf.gz | bcftools norm --fasta-ref ${ref_fasta} --multiallelics +any | python3 ${flag_multiallelic_sites_py} | bcftools norm --fasta-ref ${ref_fasta} --multiallelics -any | bgzip > ../outputs/268_318_322-test.normalized.vcf.gz
tabix -p vcf ../outputs/268_318_322-test.normalized.vcf.gz 

zcat chr22.268_318_322-test.vcf.gz | bcftools norm --fasta-ref ${ref_fasta} --multiallelics +any | python3 ${flag_multiallelic_sites_py} | bcftools norm  --fasta-ref ${ref_fasta} --multiallelics -any | bgzip > ../outputs/chr22.268_318_322-test.normalized.vcf.gz

tabix -p vcf ../outputs/chr22.268_318_322-test.normalized.vcf.gz

