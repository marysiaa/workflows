#!/usr/bin/env python3

import argparse
import sys
import gzip
from pysam import VariantFile
import ntpath
import re

#class Error(Exception):
#    pass

__version__ = '1.0.0'
AUTHOR = "moni.krzyz"

def create_output(args):
    content = []
    if args.query_vcf:
        for vcf in args.query_vcf:
            content.append(combine_vcf_content(vcf))
    if args.query_bed:
        for bed in args.query_bed:
            content.append(combine_bed_content(bed))

    return annotations_to_conf(content)

def get_vcf_info_fields(filename: str) -> list:
    vcf_in = VariantFile(filename, index_filename=filename + '.tbi')
    return list(vcf_in.header.info)

def vcf_create_annotation_names(list_info_fields: list, filename: str) -> str:
    return [name_in_path(filename) + "_" + x for x in list_info_fields]

def create_ops_array(fields: list) -> str:
    return ["self" for x in fields]

def annotations_to_conf(content: list) -> str:
    output = ""
    with open("conf.toml", "a") as toml:
        for i in range(len(content)):
            output += content[i]
    return output

def name_in_path(filename: str) -> str:
    return ntpath.basename(filename).replace(".vcf.gz", "").replace(".bed.gz", "")

def combine_vcf_content(filename: str) -> str:
    file = 'file="{}"'.format(filename)
    info = get_vcf_info_fields(filename)
    fields = "fields={}".format(info)
    ops = "ops={}".format(create_ops_array(info))
    names = "names={}".format(vcf_create_annotation_names(info, filename))
    return "\n".join(["[[annotation]]", file, str(fields), str(ops), str(names), "\n"])

def bed_create_annotation_names(columns: list, filename: str) -> list:
    return [  name_in_path(filename) +  "_bed_" + str(column) for column in columns]

def bed_annotation_columns(filename: str) -> list:
    with gzip.open(filename, "rt", encoding='utf-8') as bed:
        line = bed.readline()
        list_of_columns = line.split("\t")
        if line.find(" ") != -1:
            print("WARNING: Query bed file: {0} contains whiteholds. White spaces are replaced to tabs to define number of columns: \n ".format(filename))
            print(line.replace(" ", " WHITEHOLDER ").replace("\t", " TAB "))
            list_of_columns = re.split('[\t ]' , line)
        if len(list_of_columns) <= 3:
            print("WARNING: Query bed file: {0} has less than 4 columns, no annotations exist to annotate".format(filename))
            return []
           
        #raise Error("Query bed file: {0} has less than 4 columns, no annotations exist to annotate.".format(filename))
    return [x + 1 for x in list(range(len(list_of_columns)))][3:]

def combine_bed_content(filename: str) -> str:
    file = 'file = "{}"'.format(filename)
    columns_list = bed_annotation_columns(filename)
    columns = 'columns={}'.format(str(columns_list))
    names = 'names={}'.format(str(bed_create_annotation_names(columns_list, filename)))
    ops = 'ops={}'.format(str(create_ops_array(columns_list)))
    return "\n".join(["[[annotation]]", file, columns, names, ops, "\n"])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Create conf.toml with definition of vcf annotation.")
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument("--query_vcf", "-q", action="append", help="Vcf with annotations to insert.")
    parser.add_argument("--query_bed", "-b", action="append", help="Bed file with annotations")
    parser.add_argument("--output_path", "-o", help="Path to output text file with configurations, usually conf.toml", default="/tmp/conf.toml")

    args = parser.parse_args()

    with open(args.output_path, 'w') as f:
        #try:
        f.write(create_output(args))
        #except Error as err:
        #    print("Error happened: " + str(err))
        #    sys.exit(1)
