#!/usr/bin/python3
__version__ = '0.0.1'

from math import ceil
import argparse
from typing import List



def get_header(interval_file:[str]) -> str:
 header = ""
 for line in open(interval_file, 'r'):
     if line.startswith("@"):
         header += line
     else:
         break
 return header

def calculate_total_length(interval_file:[str]) -> int:
 total_length = 0
 for line in open(interval_file, 'r'):
     if line.startswith("@"):
         pass
     else:
         start = int(line.split("\t")[1])
         end = int(line.split("\t")[2])
         total_length += end - start
 return total_length


def calculate_length_of_one_piece(no_pieces:[int], total_length:[int]) -> int:
    return int(ceil(float(total_length)/no_pieces))


def get_str_with_no_piece(piece_number:[int], no_pieces_len:[int])-> str:
 piece_number_str = str(piece_number)
 for i in range(no_pieces_len - len(piece_number_str)):
     piece_number_str = "0" + piece_number_str
 return piece_number_str


def calculate_final_number_of_pieces(interval_file:[str], approximate_upper_limit_of_piece_length:[int]) -> int:
 piece_number = 1
 piece_length = 0
 for line in open(interval_file, 'r'):
     if line.startswith("@"):
         pass
     else:
         start = int(line.split("\t")[1])
         end = int(line.split("\t")[2])
         piece_length += (end - start)
         if piece_length >= approximate_upper_limit_of_piece_length:
             piece_number += 1
             piece_length = 0
 final_no_pieces = piece_number
 return final_no_pieces


def print_piece(piece_lines:[str], piece_number:[int], interval_file_name:[str], header:[str], no_pieces:[int]) -> None:
 piece_number_str = get_str_with_no_piece(piece_number, len(str(no_pieces)))
 with open(piece_number_str + "_" + interval_file_name, "w") as text_file:
     text_file.write(header + piece_lines)


def create_list_file(interval_file_name:[str], no_pieces:[int]) -> None:
 paths = ""
 no_pieces_len = len(str(no_pieces))
 for piece_number in [x + 1 for x in range(no_pieces)]:
     piece_number_str = get_str_with_no_piece(piece_number, no_pieces_len)
     paths += piece_number_str + "_" + interval_file_name + "\n"
     print(piece_number)
     print(paths)
     print(piece_number_str)
     with open("scattered_" + str(no_pieces) + "_" + interval_file_name.replace("interval_list", "") + "interval_list_files", "w") as text_file:
         text_file.write(paths)


def print_list_of_interval_files(interval_file_name:[str], no_pieces:[int]) -> None:
 no_pieces_len = len(str(no_pieces))
 for piece_number in [x + 1 for x in range(no_pieces)]:
     piece_number_str = get_str_with_no_piece(piece_number, no_pieces_len)
     print(piece_number_str + "_" + interval_file_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='splits interval file into given number of aproximately same size parts')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--interval_file', '-f', type=str, nargs=1, help='interval file to be splitted ')
    parser.add_argument('--no_pieces', '-n', type=int, nargs='?', default=3, help='input file will be splitted into that many files')
    parser.add_argument('--do_create_list_file', '-l', action="store_true", help=' create list file' )
    parser.add_argument('--print_absolute_paths_of_created_interval_files', '-p', action="store_true", help=' print absolute paths of created interval files' )
    # more arguments
    arguments = parser.parse_args()

#interval_file = "sureselect-human-all-exon-v7.covered-padded-200.interval_list"

    interval_file_name = arguments.interval_file[0] + ".intervals"


    total_length = calculate_total_length(arguments.interval_file[0])
    approximate_upper_limit_of_piece_length = calculate_length_of_one_piece(arguments.no_pieces, total_length)
    header = get_header(arguments.interval_file[0])
    no_pieces = calculate_final_number_of_pieces(arguments.interval_file[0], approximate_upper_limit_of_piece_length)



    piece_number = 1
    piece_length = 0
    piece_lines = ""

    for line in open(arguments.interval_file[0], 'r'):
     if line.startswith("@"):
         pass
     else:
         start = int(line.split("\t")[1])
         end = int(line.split("\t")[2])
         piece_length += end - start
         piece_lines += line
         if piece_length >= approximate_upper_limit_of_piece_length:
             print_piece(piece_lines, piece_number, interval_file_name, header, arguments.no_pieces)
             piece_number += 1
             piece_length = 0
             piece_lines = ""

    if piece_lines != "":
     print_piece(piece_lines, piece_number, interval_file_name, header, arguments.no_pieces)

    if arguments.do_create_list_file:
     create_list_file(interval_file_name, arguments.no_pieces)

    if arguments.print_absolute_paths_of_created_interval_files:
     print_list_of_interval_files(interval_file_name, arguments.no_pieces)
