#!/usr/bin/python3
import sys
import copy
import json
import pysam
import argparse
import warnings
from typing import Set
from typing import Dict
from typing import List
from typing import Tuple
from typing import Union
from typing import Iterable

__version__ = '1.1.1'


def get_variant_record(record: pysam.libcbcf.VariantRecord, info_subfields_to_be_parsed: Dict[str, Iterable[str]],
                       default_value: Union[str, None]) -> Dict[str, Union[str, tuple, dict, None]]:
    ret_dict = {
        "CHROM": record.chrom,
        "POS": record.pos,
        "ID": record.id,
        "REF": record.ref,
        "ALT": ','.join(record.alts),
        "QUAL": record.qual,
        "FILTER": tuple(record.filter)
    }
    ret_dict.update({k: v for k, v in record.info.items() if not k in info_subfields_to_be_parsed})
    ret_dict.update(
        {k: parse_info_subfield(v, info_subfields_to_be_parsed[k].keys(), default_value) for k, v in record.info.items()
         if k in info_subfields_to_be_parsed})
    return ret_dict


def get_columns_descriptions(header):
    columns_descriptions = dict()
    for item in header.formats:
        columns_descriptions[header.formats[item].name] = header.formats[item].description
    for item in header.info:
        columns_descriptions[header.info[item].name] = header.info[item].description
    return columns_descriptions


def process_file(in_path: str, out_path_without_ext: str, output_extensions: List[str], default_value: Union[str, None],
                 info_fields_to_be_parsed: Set[str], separator: str = '\t') -> None:
    template_dict = create_empty_info_format_dict(in_path, default_value, info_fields_to_be_parsed)
    valid_info_fields_to_be_parsed = validate_fields_to_be_parsed(template_dict, info_fields_to_be_parsed)
    out_handles = {extension: open(f'{out_path_without_ext}.{extension}', 'w') for extension in output_extensions}
    with pysam.VariantFile(in_path) as vcf_file:
        # get column descriptions from VCF header
        if 'json' in out_handles:
            header = vcf_file.header
            columns_descriptions = get_columns_descriptions(header)
        try:
            sample_names = vcf_file.header.samples
            if 'json' in out_handles:
                out_handles['json'].write('{\n')
                out_handles['json'].write('"variants":\n')
                out_handles['json'].write('[\n')
                last_read_object = ''
            if 'tsv' in out_handles:
                out_handles['tsv'].write(separator.join(record_dict_to_field_names(template_dict)))
                out_handles['tsv'].write('\n')
            for record in vcf_file.fetch():
                parsed_variant_record = get_variant_record(record,
                                                           {k: template_dict[k] for k in valid_info_fields_to_be_parsed},
                                                           default_value)
                sample_dict = {name: dict(record.samples[name]) for name in sample_names}
                record_dict = update_template_dict(template_dict, parsed_variant_record, sample_dict)
                if 'json' in out_handles:
                    if last_read_object:
                        out_handles['json'].write(last_read_object)
                        out_handles['json'].write(',\n')
                    last_read_object = record_dict_to_json(record_dict)
                if 'tsv' in out_handles:
                    out_handles['tsv'].write(separator.join(record_dict_to_values(record_dict, info_fields_to_be_parsed)))
                    out_handles['tsv'].write('\n')
            if 'json' in out_handles:
                out_handles['json'].write(last_read_object)
                out_handles['json'].write('\n],\n')
        finally:
            if 'json' in out_handles:
                out_handles['json'].write('"columns_descriptions":\n')
                out_handles['json'].write(json.dumps(columns_descriptions))
                out_handles['json'].write('\n}')
            for _, handle in out_handles.items():
                handle.close()


def validate_fields_to_be_parsed(header_fields_dictionary: Dict, info_fields_to_be_parsed: Set[str]) -> Set[str]:
    out_set = set()
    for field in info_fields_to_be_parsed:
        if not field in header_fields_dictionary:
            warnings.warn("No {} in the file's header".format(field))
        else:
            out_set.add(field)
    return out_set


def update_template_dict(template_dict: Dict[str, Union[str, None, tuple, Dict]],
                         parsed_variant_record: Dict[str, Union[str, None, tuple, Dict]],
                         sample_dict: Dict[str, Union[str, None, tuple, Dict]]) -> Dict[
    str, Union[str, None, tuple, Dict]]:
    record_dict = copy.deepcopy(template_dict)
    record_dict.update(parsed_variant_record)
    for k, v in sample_dict.items():
        record_dict[k].update(v)
    return record_dict


def create_empty_info_format_dict(vcf_path: str, value: Union[str, None], info_fields_to_be_parsed: Set[str]) -> Dict:
    with pysam.VariantFile(vcf_path) as vcf_file:
        header = vcf_file.header
        out = {
            "CHROM": value,
            "POS": value,
            "ID": value,
            "REF": value,
            "ALT": value,
            "QUAL": value,
            "FILTER": value
        }
        out.update({key: {x: value for x in
                          parse_info_header_subfield(header.info, key)} if key in info_fields_to_be_parsed else value
                    for key in header.info})
        out.update({sample: {key: value for key in header.formats.keys()} for sample in header.samples})
        return out


def record_dict_to_json(input: Dict[str, Union[str, None, List]]) -> str:
    return json.dumps(input, indent=4)


def record_dict_to_field_names(input: Dict[str, Union[str, None, tuple, Dict]], field_name_separator: str = '__') -> \
List[str]:
    fields = []
    for k, v in input.items():
        if isinstance(v, dict):
            fields.extend(['{}{}{}'.format(sub_k, field_name_separator, k) for sub_k in v])
        else:
            fields.append(k)
    return fields


def record_dict_to_values(input: Dict[str, Union[str, None, Tuple, Dict]], info_fields_to_be_parsed: Set[str]) -> List[
    str]:
    field_values = []
    for k, v in input.items():
        if isinstance(v, dict) or isinstance(v, tuple) or isinstance(v, list):
            field_values.extend(deal_with_complex_values(k, v, info_fields_to_be_parsed))
        else:
            field_values.append(str(v))
    return field_values


def deal_with_complex_values(key: Union[str, None], value: Union[Dict, Tuple, List],
                             info_fields_to_be_parsed: Set[str]) -> List[str]:
    if isinstance(value, dict):
        if isinstance(list(value.values())[0], dict):
            return flatten_list(
                [deal_with_complex_values(sub_key, sub_value, info_fields_to_be_parsed) for sub_key, sub_value in
                 value.items()])
        else:
            return [str(x) for x in value.values()]
    elif key in info_fields_to_be_parsed:
        if len(value) != 1:
            warnings.warn(
                '{} elements in {}. This script is not designed to support multiple {} per record. The result tsv file may be incorrect'.format(
                    len(value), key, key))
        return flatten_list([deal_with_complex_values(None, sub_val, info_fields_to_be_parsed) for sub_val in value])
    elif isinstance(value, tuple):
        if len(value) != 1:
            warnings.warn('{} elements in {}'.format(len(value), key))
        return [str(value)]
    else:
        raise TypeError()


def flatten_list(list_of_lists: List[List]) -> List:
    return [item for sublist in list_of_lists for item in sublist]


def parse_info_header_subfield(info_field: pysam.libcbcf.VariantHeaderMetadata, field_name: str) -> Union[
    List[str], None]:
    try:
        subfield = info_field[field_name]
    except KeyError:
        warnings.warn('No field_name {} in INFO section of the header'.format(field_name))
        return None
    try:
        description = subfield.record['Description']
    except KeyError:
        warnings.warn('No Description in {} field of the INFO header'.format(field_name))
        return None
    return parse_description(description)


def parse_description(description_string: str, separator: str = '|') -> List[str]:
    _, field_names_str = description_string.split(':')
    field_names = field_names_str.strip(" '\"").split(separator)
    return [x.strip() for x in field_names]


def parse_info_subfield(subfield_to_be_parsed: Union[str, Tuple[str]], keys: Iterable[str],
                        default_value: Union[str, None]) -> Union[Dict[str, str], List[Dict[str, str]]]:
    if isinstance(subfield_to_be_parsed, str):
        return dict(zip(keys, parse_subfield(subfield_to_be_parsed, default_value)))
    elif isinstance(subfield_to_be_parsed, tuple):
        return [parse_info_subfield(x, keys, default_value) for x in subfield_to_be_parsed]
    else:
        raise TypeError('Wrong data type: {}'.format(type(subfield_to_be_parsed)))


def parse_subfield(subfield_record_str: str, default_value: Union[str, None], separator: str = '|') -> List[str]:
    fields = [x.strip() for x in subfield_record_str.split(separator)]
    return [x if x else default_value for x in fields]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert vcf to csv')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-name', '-o', type=str, required=True, help='output file name (without extension')
    parser.add_argument('--output-extensions', '-x', nargs='+', type=str, required=True, help='output file extension(s) indicating output format(s)')
    parser.add_argument('--default-value', '-d', type=str, default=None, help='default for lacking values in vcf line')
    parser.add_argument('--split-ANN-field', action='store_true', default=False)
    parser.add_argument('--split-CSQ-field', action='store_true', default=False)
    parser.add_argument('--split-ISEQ_REPORT_ANN-field', action='store_true', default=False)
    parser.add_argument('--split-ISEQ_MANE_ANN-field', action='store_true', default=False)
    args = parser.parse_args()

    output_extensions = [extension.lower() for extension in args.output_extensions]
    for extension in output_extensions:
        if not extension in ['json', 'tsv']:
            raise RuntimeError("The script outputs tsv and json file formats only. Correct the extension and run the script again.")

    fields_to_be_parsed = set()
    if args.split_ANN_field:
        fields_to_be_parsed.add('ANN')
    if args.split_CSQ_field:
        fields_to_be_parsed.add('CSQ')
    if args.split_ISEQ_REPORT_ANN_field:
        fields_to_be_parsed.add('ISEQ_REPORT_ANN')
    if args.split_ISEQ_MANE_ANN_field:
        fields_to_be_parsed.add('ISEQ_MANE_ANN')

    process_file(args.input_vcf, args.output_name, output_extensions, args.default_value, fields_to_be_parsed)
