#!/usr/bin/python3

import os
import json
import pandas as pd
import argparse
from pandas.errors import EmptyDataError

from sqlalchemy import false, null, true

__version__ = '1.1.5'
AUTHOR = "gitlab.com/MateuszMarynowski"

parser = argparse.ArgumentParser(description='Concat jsons')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input_syndromes', type=str, required=False, nargs='+', help='input syndromes tsv files')
parser.add_argument('--input_syndromes_json', type=str, required=False, nargs='+', help='input syndromes json files')
parser.add_argument('--input_sample_info', type=str, required=False, help='input sample info file')
parser.add_argument('--input_qc', type=str, required=False, help='input qc json')
parser.add_argument('--input_clinvar', type=str, required=False, nargs='+', help='input clinvar tsv files')
parser.add_argument('--input_wsx_run_status', type=str, required=False, nargs='+', help='input WisecondorX run status files')
parser.add_argument('--input_aberrations', type=str, required=False, nargs='+', help='input aberrations bed files')
parser.add_argument('--input_statistics', type=str, required=False, nargs='+', help='input statistics txt files')
parser.add_argument('--input_illumina_plots', type=str, required=False, nargs='+', help='input illumina plots files')
parser.add_argument('--input_illumina_report', type=str, required=False, nargs='+', help='input illumina NCV values')
parser.add_argument('--input_plots', type=str, required=False, nargs='+', help='input plots files')
parser.add_argument('--input_results', type=str, required=False, nargs='+', help='input results files (models)')
parser.add_argument('--is_nipt', action='store_true', default=False)
parser.add_argument('--output_file', type=str, required=False, help='output filename')
args = parser.parse_args()


def save_to_file(data: dict, filename: str):
    with open(filename, "w") as f:
        json.dump(data, f, indent=2)


def get_basename(filename: str) -> str:
    return "_".join(os.path.basename(filename).split("_")[0:-1])


def load_csv(tsv_path: str) -> dict:
    try:
        return pd.read_csv(tsv_path, sep="\t", comment="#", low_memory=False)
    except EmptyDataError:
        return None

def load_json(json_path: str) -> dict:
    with open(json_path, "r") as f:
        try:
            return json.load(f)
        except:
            return None

def df_to_json(dataframe):
    return dataframe.to_json(orient="records")

def read_status_files(status_files: list, final: dict) -> dict:
    for f in status_files:
        basename = get_basename(f)
        with open(f) as status:
            result = status.readline()
            result = result.strip()
            if basename not in final["samples"]:
                final["samples"][basename] = {"results":{}}
            if "results" in final["samples"][basename]:
                final["samples"][basename]["results"]["wisecondorx_run_status"] = result
            else:
                final["samples"][basename]["results"] = {"wisecondorx_run_status": result}
            
    return final


def tsv_files_to_json(tsv_files: list, final: dict, name: str, key: str = null) -> dict:
    if not tsv_files: 
        return final
    for file in tsv_files:
        basename = get_basename(file)
        df = load_csv(file)
        if df is not None:
            df_json = df_to_json(df)
            if key is not null:
                loaded_json = {}
                for item in json.loads(df_json):
                    if key == "chr":
                        if len(item[key]) <= 2:
                            loaded_json["chr" + item[key]] = item
                    else:
                        loaded_json[item[key]] = item
            else:
                loaded_json = json.loads(df_json)
            if name == "aneuploidy_report":
                loaded_json = loaded_json[0]    
            if basename not in final["samples"]:
                final["samples"][basename] = {"results":{}}
            if "results" in final["samples"][basename]:
                final["samples"][basename]["results"][name] = loaded_json
            else:
                final["samples"][basename]["results"] = {name: loaded_json}
        else:
            continue        
    return final


def create_property_if_not_exits(root: dict, property: str):
    if property not in root:
        root[property] = {}


def json_files_to_json(json_files: list, final: dict, name: str) -> dict:
    for file in json_files:
        basename = get_basename(file)
        df = load_json(file)
        if df is not None:
            if "results" in final["samples"][basename]:
                final["samples"][basename]["results"][name] = df
            else:
                final["samples"][basename]["results"] = {name: df}
        else:
            continue        
    return final


def get_plots_path(plots_path: list, plot_type: str, set_key: bool = false) -> dict:
    list_of_plots = [os.path.basename(plot) for plot in plots_path]
    plots_dict = {}
    for item in list_of_plots:
        if plot_type == "illumina_plots":
            key = "_".join(item.split("_")[0:-1])
            chromosome_key = item.split("_")[-1].split(".")[0]
            item = {chromosome_key: item}
        else:
            key = item.split("_wsx-")[0]
            chromosome_key = item.split(key + "_wsx-")[1].split(".")[0]
            if chromosome_key != "empty":
                item = {chromosome_key: item}
            else:
                item = None 
        if key in plots_dict:
            if item:
                plots_dict[key].append(item)
        else:
            if item:
                plots_dict[key] = [item]
    return plots_dict


def add_plots_to_dict(plots_dict: dict, final: dict, plot_type: str) -> dict:
    if plot_type == "illumina_plots":
        for key, value in plots_dict.items():
            for k, v in final["samples"].items():
                if key in k:
                    create_property_if_not_exits(final["samples"], k)
                    create_property_if_not_exits(final["samples"][k], "plots")
                    final["samples"][k]["plots"][plot_type] = value
    else:
        for key, value in plots_dict.items():
            create_property_if_not_exits(final["samples"], key)
            create_property_if_not_exits(final["samples"][key], "plots")
            final["samples"][key]["plots"] = {plot_type: value}
    return final


def prepare_qc_microarray(qc_path: str, final: dict) -> dict:
    qc_json = load_json(qc_path)
    for item in qc_json:
        if item["id"] in final["samples"]:
            final["samples"][item["id"]]["qc"] = item["qc"]
        else:
            final["samples"][item["id"]] = {"qc": item["qc"]}
    return final


def prepare_qc_nipt(qc_dict: dict, final: dict) -> dict:
    for key, value in qc_dict.items():
        new_key = "_".join(key.split("_")[0:-3])
        final["samples"][new_key] = {"qc": value}
    return final


def add_sample_info_to_dict(info: dict, final: dict) -> dict:
    for item in info:
        for key, value in final["samples"].items():
            if item["name"] in key:
                final["samples"][key]["info"] = item
    return final


def models_to_json(models_path: list) -> dict:
    final = dict()
    for file in models_path:
        model = load_json(file)
        model = {"yml_method": model["description"]["model_name"], **model}
        sample_name = model["description"]["sample_name"]
        if sample_name in final:
            final[sample_name]["results"].append(model)
        else:
            final[sample_name] = {"results": [model]}
    return final


if __name__ == "__main__":
    if args.is_nipt:
        final_dict = dict()

        # qc
        qc = load_json(args.input_qc)
        final_dict["batch"] = {"qc": {"BCL_RunCompletionStatus": qc["BCL_RunCompletionStatus"]}}
        del qc["BCL_RunCompletionStatus"]
        final_dict["samples"] = {}
        final_dict = prepare_qc_nipt(qc, final_dict)

        # sample_info
        if args.input_sample_info:
            sample_info = load_json(args.input_sample_info)
            final_dict = add_sample_info_to_dict(sample_info, final_dict)

        # clinvar
        final_dict = tsv_files_to_json(args.input_clinvar, final_dict, "clinvar")

        # syndromes
        final_dict = tsv_files_to_json(args.input_syndromes, final_dict, "syndromes")

        # syndromes summary
        final_dict = json_files_to_json(args.input_syndromes_json, final_dict, "syndromes_summary")

        # aberrations
        final_dict = tsv_files_to_json(args.input_aberrations, final_dict, "aberrations")

        # zscores
        final_dict = tsv_files_to_json(args.input_statistics, final_dict, "zscores", "chr")

        # illumina NCV
        final_dict = tsv_files_to_json(args.input_illumina_report, final_dict, "aneuploidy_report")

        # wisecondorx status
        final_dict = read_status_files(args.input_wsx_run_status, final_dict)

        # plots
        plots = get_plots_path(args.input_plots, "wisecondorx_plots")
        final = add_plots_to_dict(plots, final_dict, "wisecondorx_plots")

        # illumina plots
        illumina_plots = get_plots_path(args.input_illumina_plots, "illumina_plots")
        final = add_plots_to_dict(illumina_plots, final_dict, "illumina_plots")

        
        if args.output_file:
            save_to_file(final_dict, args.output_file)
        else:
            print(final)
    else:
        final_dict = dict()
        final_dict["batch"] = {}

        # results
        final_dict["samples"] = models_to_json(args.input_results)

        # qc
        if args.input_qc:
            final_dict = prepare_qc_microarray(args.input_qc, final_dict)

        # sample_info
        if args.input_sample_info:
            sample_info = load_json(args.input_sample_info)
            final_dict = add_sample_info_to_dict(sample_info, final_dict)

        save_to_file(final_dict, args.output_file)
