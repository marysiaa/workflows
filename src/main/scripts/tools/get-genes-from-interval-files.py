#!/usr/bin/env python3

import argparse
import pandas as pd
import json

__version__ = '0.0.1'


def get_interval_file_as_dataframe(interval_files):
    columns = ["chrom", "pos_start", "pos_end", "turn", "genes"]
    data_concat = pd.DataFrame(columns=columns)
    for file in interval_files:
        try:
            data = pd.read_csv(file, sep="\t", comment='@', header=None)
            data.columns = columns
            data_concat = data_concat.append(data)
        except:
            pass
    return data_concat


def get_genes_from_dataframe(dataframe):
    list_of_genes = []
    for genes in dataframe["genes"]:
        for gene in genes.split("|"):
            gene_dict = dict()
            gene_dict['name'] = gene
            list_of_genes.append(gene_dict)
    return list_of_genes


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get genes from interval file, return list of genes in json format')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--interval-files', required=True, nargs='+')
    parser.add_argument('--outfile', type=str, required=True)

    arguments = parser.parse_args()

    # read intervals to dataframes and concat dataframes
    data_concat = get_interval_file_as_dataframe(arguments.interval_files)

    # get genes from concatenated dataframes
    list_of_genes = get_genes_from_dataframe(data_concat)

    # write list of objects to json
    with open(arguments.outfile, 'w') as outfile:
        json.dump(list_of_genes, outfile, indent=1)
