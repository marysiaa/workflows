#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
from signal import signal, SIGPIPE, SIG_DFL
from sys import stdin

signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
 """
 Create a tab-delimited dictionary from two columns of a delimiter-separated file, piped as *stdin*.

 Values of a key in a dictionary are by default
   - uniquified,
   - alphabetically sorted,
   - comma delimited,
   - whitespaces are replaced by underscores
   - all letters are changed to lower case
   - commas within values are erased.

 Output is printed to *stdout*.
 """)

parser.add_argument('-v', '--version', action='version', version=__version__)
parser.add_argument('-f', type=str, default='\t', help="specify delimiter in an input file (Default: \\t)")
parser.add_argument('-d', type=str, default=',', help="specify delimiter in an output dictionary (Default: ,)")
parser.add_argument('-w', action='store_true', help="do not replace whitespaces with underscores")
parser.add_argument('-c', action='store_true', help="preserve capitalization")
parser.add_argument('-e', action='store_true', help="do not erase commas within values")
parser.add_argument('-k', type=str, default=None, help="use if column from which keys are to be "
                                                       "extracted contains multiple records. "
                                                       "specify delimiting characters")
parser.add_argument('-l', type=str, default=None, help="use if column from which values are to be "
                                                       "extracted contains multiple records. "
                                                       "specify delimiting characters")
parser.add_argument('KEY_INDEX', type=int, help="index of column containing keys (column numeration is 0 based)")
parser.add_argument('VALUES_INDEX', type=int, help="index of column containing VALUES (column numeration is 0 based)")

args = parser.parse_args()

key_index = args.KEY_INDEX
values_index = args.VALUES_INDEX
files_delimiter = args.f
dictionary_delimiter = args.d
key_column_delimiting_character = args.k
value_column_delimiting_character = args.l
do_not_replace_whitespace = args.w
preserve_capitalization = args.c
do_not_erase_commas_within_values = args.e

dictionary = {}

for line in stdin:

    if not line.startswith('#'):

        record = [x.strip() for x in line.split(files_delimiter)]

        if key_column_delimiting_character:
            keys = [x.strip() for x in record[key_index].split(key_column_delimiting_character)]
        else:
            keys = [record[key_index]]

        if value_column_delimiting_character:
            values = [x.strip() for x in record[values_index].split(value_column_delimiting_character)]
        else:
            values = [record[values_index]]

        for key in keys:

            if key not in dictionary.keys():
                dictionary[key] = values

            else:
                dictionary[key] = dictionary[key] + values

dictionary.pop('', None)

for key in sorted(dictionary):

    values = sorted([x.strip() for x in list(set(dictionary[key]) - {''})])

    if not do_not_erase_commas_within_values:
        values = [x.replace(',', '') for x in values]

    if not do_not_replace_whitespace:
        values = [x.replace(' ', '_') for x in values]

    if not preserve_capitalization:
        values = [x.lower() for x in values]

    if values:
        print(key + '\t' + dictionary_delimiter.join(values))
