#!/usr/bin/python3
"""Select fields from JSON based on CSV"""

import json
import argparse
import pandas as pd


__version__ = '0.0.4'


def load_json(json_path: str) -> json:
    """Load JSON

    Args:
        json_path (str): Path to json file

    Returns:
        json: Loaded JSON file
    """
    with open(json_path, 'r', encoding="UTF-8") as json_file:
        return json.load(json_file)


def load_csv(csv_path: str) -> pd.DataFrame:
    """Load csv

    Args:
        csv_path (str): Path to csv file

    Returns:
        pd.DataFrame: Loaded csv file into pandas dataframe
    """
    return pd.read_csv(csv_path, header=[0])


def add_selected_field_to_dict(key: str, item: dict) -> str:
    """Add selected field to dict

    Args:
        key (str): Key to add
        item (dict): One variant

    Returns:
        str: Selected field value
    """
    if "__ISEQ_REPORT_ANN" in key:
        field_from_key = key.split("__")[0] # example Gene_Name
        return item["ISEQ_REPORT_ANN"][0][field_from_key]
    elif "__ISEQ_MANE_ANN" in key:
        field_from_key = key.split("__")[0] # example Gene_Name
        return item["ISEQ_MANE_ANN"][0][field_from_key]
    if key in ('GT', 'AD'):
        try:
            return str(item[list(item.keys())[-1]][key][0]) + "/" + str(item[list(item.keys())[-1]][key][1])
        except TypeError:
            return item[list(item.keys())[-1]][key]
    if key == "FILTER":
        return "; ".join(item[key])
    if isinstance(item[key], list):
        return item[key][0]
    return item[key]


def add_main_fields_description() -> dict:
    """Add main fields description (like CHROM, POS, etc.)

    Returns:
        dict: Dict with added main fields description
    """
    main_fields = {
        "CHROM": {
            "description": "CHROM - chromosome: An identifier from the reference genome or an angle-bracketed ID String (“<ID>”) pointing to a contig in the assembly file",
            "default": True
        },
        "POS": {
            "description": "POS - position: The reference position, with the 1st base having position 1. Positions are sorted numerically, in increasing order, within each reference sequence CHROM.",
            "default": True
        },
        "REF": {
            "description": "REF - reference base(s): Each base must be one of A,C,G,T,N (case insensitive).",
            "default": True
        },
        "ALT": {
            "description": "ALT - alternate base(s): Comma separated list of alternate non-reference alleles.",
            "default": True
        },
        "QUAL": {
            "description": "QUAL - quality: Phred-scaled quality score for the assertion made in ALT.",
            "default": True
        }
    }
    descriptions = {}
    for key, value in main_fields.items():
        descriptions[key] = value
    return descriptions


def add_other_fields_description(key: str, descriptions: dict, default: bool) -> dict:
    """Add other fields description (like ISEQ_ACMG_SUMMARY_CLASSIFICATION, ISEQ_DISEASE_HPO, etc.)

    Args:
        key (str): Key from which to get the description
        descriptions (dict): Whole columns descriptions dict
        default (bool): Whether a field is default or not

    Returns:
        dict: Dict with added other fields description
    """
    if ("__ISEQ_REPORT_ANN" or "__ISEQ_MANE_ANN") in key:
        field_from_key = key.split("__")[0] # example Gene_Name
        return {"description": f"SnpEff ANN subfield (transcript) that will be displayed in report. Format: {field_from_key}", "default": default}
    return {"description": descriptions[key], "default": default}


def save_dict_to_json(output_filename: str, variants_selected: dict, columns_selected: dict):
    """Save dict to JSON

    Args:
        output_filename (str): Output filename
        variants_selected (dict): Variants dict with selected fields
        columns_selected (dict): Columns descriptions dict with selected fields
    """
    final_json = {}
    final_json["variants"] = variants_selected
    final_json["columns"] = columns_selected
    with open(output_filename, "w", encoding="UTF-8") as file:
        json.dump(final_json, file, indent=2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Select fields from JSON based on CSV')
    parser.add_argument('--json', type=str, required=True,
                        help='Json file containing variants and columns description')
    parser.add_argument('--csv', type=str, required=True,
                        help='CSV with columns to be selected')
    parser.add_argument('--output_filename', type=str, required=True, help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')
    args = parser.parse_args()

    # load json
    vcf_json = load_json(args.json)
    variants = vcf_json["variants"]
    columns_descriptions = vcf_json["columns_descriptions"]


    # load csv
    df = load_csv(args.csv)
    fields = df["column_name"].tolist()
    defaults = df["Default"].tolist()


    # select variants fields in JSON based on fields from csv
    selected_variants = []
    for variant in variants:
        selected_fields = {}
        for field in fields:
            try:
                selected_fields[field] = add_selected_field_to_dict(field, variant)
            except KeyError:
                continue
        selected_variants.append(selected_fields)


    # Add main fields description
    selected_columns_descriptions = add_main_fields_description()
    for idx, field in enumerate(fields):
        try:
            selected_columns_descriptions[field] = add_other_fields_description(field, columns_descriptions, defaults[idx])
        except KeyError:
            continue


    # save dict to JSON
    save_dict_to_json(args.output_filename, selected_variants, selected_columns_descriptions)
