#!/usr/bin/env python3

import json
import argparse
from typing import List

__version__ = '0.0.1'


def merge_models(models: List[str]):
    merged_models = dict()
    for file in models:
        with open(file) as f:
            model = json.load(f)
        if merged_models:
            merged_models[model["descriptions"]["id"]] = model
        else:
            merged_models = {model["descriptions"]["id"]: model}
    return merged_models


def save_to_json(model: dict, output_dir: str):
    with open(f"{output_dir}/all_models.json", "w") as f:
        json.dump(model, f, indent=2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Make pairs (if needed), return sample names')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--models', required=True, nargs='+')
    parser.add_argument('--output-dir', type=str, required=True)

    arguments = parser.parse_args()

    # merge models
    all_models = merge_models(arguments.models)
    save_to_json(all_models, arguments.output_dir)
