#!/user/bin/env python3

import argparse
import pysam

__version__ = "0.0.1"


def make_id(record):
    chrom = record.contig
    chrom = chrom.replace("chr", "")
    pos = str(record.pos)
    ref = record.ref
    alt = record.alts[0]
    return "-".join([chrom, pos, ref, alt])


def main(in_vcf_name, out_vcf_name):
    in_vcf = pysam.VariantFile(filename=in_vcf_name)
    items = [('ID', "ISEQ_GNOMAD_ID"), ('Number', "1"), ('Type', "String"),
             ('Description',
              "GnomAD style variant ID")]
    in_vcf.header.add_meta("INFO", items=items)

    out_vcf = pysam.VariantFile(out_vcf_name, 'w', header=in_vcf.header)

    for record in in_vcf.fetch():
        record.info["ISEQ_GNOMAD_ID"] = make_id(record)
        out_vcf.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Adds annotations useful for inheritance analysis')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True,
                        help='Input vcf file (may be bgzipped)')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True,
                        help='Output vcf file (will be bgzipped - if .gz suffix present)')

    args = parser.parse_args()
    main(args.input_vcf, args.output_vcf)
