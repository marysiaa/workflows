#!/usr/bin/env python3

import argparse
from operator import itemgetter

__version__="0.0.1"

def read_gene_list(gene_file):
    gene_list = []
    with open(gene_file) as g:
        for line in g:
            gene_list.extend(line.strip().split())
    return gene_list


def get_chosen_genes_from_bed(bed_file, chosen_genes):
    chosen_intervals = []
    with open(bed_file) as b:
        for line in b:
            splitted_line = line.strip().split("\t")
            formatted_list = [splitted_line[0],int(splitted_line[1]), int(splitted_line[2]), splitted_line[3]]
            gene = formatted_list[-1]
            if gene in chosen_genes:
                chosen_intervals.append(formatted_list)
    final_intervals = sorted(chosen_intervals, key=itemgetter(0,1))
    return final_intervals


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Gets needed gene intervals from Ensembl biomart data bed')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--gene-list', '-g', metavar='gene_list', type=str, required=True,
                        help='File with white space separated list of genes')
    parser.add_argument('--ensembl-bed', '-e', metavar='ensembl_bed', type=str, required=True,
                        help='Bed with positions of all genes')
    parser.add_argument('-o', '--output', type=str, required=True, help="Path to output bed")
    args = parser.parse_args()

    genes = read_gene_list(args.gene_list)
    interval_list = get_chosen_genes_from_bed(args.ensembl_bed, genes)
    with open(args.output, "w") as bed:
        for item in interval_list:
            item_to_write = "\t".join([str(i) for i in item]) + "\n"
            bed.write(item_to_write)

