#!/usr/bin/env python3

import argparse

__version__ = '0.0.1'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Make json with name, value, error, warning and description. This json will be used to generate report from coverage statistics and it is the output of task collect-hs-metrics (simple_json)')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument("final_json", help="Path to output from task collect-hs-metrics: final_json.json (metrics form gatk and samtools), with 2 fields: name, value ")
    parser.add_argument("json_with_errors", help="Path to json with metrics chosen from final_json, error, warnings and description - to make human readeable report. More info about its structure: https://gitlab.com/intelliseq/workflows/blob/dev/src/main/wdl/modules/coverage-statistics/readme.md")
    args = parser.parse_args()

# make simple json with warnings if provided
import json
import os

data = json.load(open(args.final_json, 'r') )
metadata = json.load(open(args.json_with_errors, 'r') )

# Add measure, value, error and warnings to simple_json
result = []
for name in metadata:
    for record in data:
        if record["name"] == name:
        # add warning to the jason if warning field is not "not_provided"
            warning = "null" if metadata[name]["warning"] == "not_provided" else metadata[name]["warning"]
        # add error to the json warning field is not "not_provided"
            error = "null" if metadata[name]["error"] == "not_provided" else metadata[name]["error"]
            Test_działa = True
            result.append({
                "name": metadata[name]["name"],
                "description" : metadata[name]["description"],
                "value": round(float(record["value"]),2),
                "warning": warning,
                "error": error, 
                "comment": metadata[name]["comment"]
        })
            break

print(json.dumps(result, indent=2))


