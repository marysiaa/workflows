#!/usr/bin/env python3
import argparse
import json
import pandas as pd
from io import StringIO
import os

__version__ = '0.0.2'

def args_parser_init():
    parser = argparse.ArgumentParser(description='Compares sample ids from two files: .csv and .json')
    parser.add_argument('--input-sample-csv', '-c', type=str,
                        help='SampleSheet.csv found in raw_data')
    parser.add_argument('--input-sample-json', '-j', type=str,
                        help='sample_info.json used as input to nipt pipeline')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()
    return args


def read_sample_csv(path):
    with open(path) as f:
        lines = f.read()
        
        data_filtered = lines.split('[Data]')[1]
        data_filtered = data_filtered.lstrip(",")
        #print(data_filtered)
        return pd.read_csv(StringIO(data_filtered))


def get_and_sort_sample_csv_ids(path):
    sample_csv = read_sample_csv(path)
    sample_csv_ids = sample_csv['Sample_ID'].tolist()
    sample_csv_ids.sort()
    return sample_csv_ids


def get_and_sort_sample_json_ids(path):
    sample_json = pd.read_json(path)
    sample_json_ids = sample_json['name'].tolist()
    sample_json_ids.sort()
    return sample_json_ids


if __name__ == "__main__":
    args = args_parser_init()
    sample_csv_ids = get_and_sort_sample_csv_ids(args.input_sample_csv)
    sample_json_ids = get_and_sort_sample_json_ids(args.input_sample_json)

    only_in_csv = [id for id in sample_csv_ids if id not in sample_json_ids]
    only_in_json = [id for id in sample_json_ids if id not in sample_csv_ids]

    csv_name = os.path.basename(args.input_sample_csv)
    json_name = os.path.basename(args.input_sample_json)

    if only_in_csv:
        print(f"WARNING: Sample ids found only in {csv_name}: {only_in_csv}")
    if only_in_json:
        print(f"WARNING: Sample ids found only in {json_name}: {only_in_json}")
