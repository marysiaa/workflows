#!/usr/bin/python3

import argparse
import pysam
import json
import pandas as pd

__version__ = '1.1.0'


def load_csv(csv_path: str) -> dict:
    return pd.read_csv(csv_path, sep="\t", comment="#", header=None, low_memory=False)


def select_columns(df, columns: list):
    return df[columns]


def rename_columns(df, column_names: list):
    df.columns = column_names
    return df


def drop_dup(df):
    return df.drop_duplicates()


def merge_dfs(df1, df2):
    merged_df = pd.merge(df1, df2, on="DatabaseID")
    merged_df["DiseaseName"] = merged_df["DiseaseName"] + " (HPO - " + merged_df["DatabaseID"] + ")"
    return merged_df


def groupby_and_save_to_dict(df) -> dict:
    return df.groupby('entrez-gene-symbol')['DiseaseName'].apply(list).to_dict()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotate vcf with disease names derived from HPO')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input_vcf', '-i', type=str, required=True, help='input vcf file')
    parser.add_argument('--input_phenotype_hpoa', type=str, help='input phenotype.hpoa file',
                        default="/resources/phenotype.hpoa")
    parser.add_argument('--input_phenotype_to_genes', type=str, help='input phenotype_to_genes.txt file',
                        default="/resources/phenotype_to_genes.txt")
    parser.add_argument('--output_vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf file')
    parser.add_argument('--sv', action='store_true', help='Give this flag if the input vcf is a SV file')
    args = parser.parse_args()

    # load and prepare diseases
    phenotype_hpoa = load_csv(args.input_phenotype_hpoa)
    diseases = select_columns(phenotype_hpoa, [0, 1])
    diseases = rename_columns(diseases, ["DatabaseID", "DiseaseName"])
    diseases = drop_dup(diseases)

    # load and prepare genes
    phenotype_to_genes = load_csv(args.input_phenotype_to_genes)
    genes = select_columns(phenotype_to_genes, [6, 7])
    genes = rename_columns(genes, ["DatabaseID", "entrez-gene-symbol"])
    genes = drop_dup(genes)

    # merge diseases with genes
    merged = merge_dfs(diseases, genes)

    # group and save to dict
    gene_diseases = groupby_and_save_to_dict(merged)
    # replace whitespaces with "_"
    for key, value in gene_diseases.items():
        gene_diseases[key] = [v.replace(" ", "^").replace(",", "*") for v in value]

    # annotate vcf
    vcf_reader = pysam.VariantFile(filename=args.input_vcf)
    vcf_reader.header.info.add("ISEQ_DISEASE_HPO", "1", "String",
                               "Genes annotated with disease names derived from HPO")
    vcf_writer = pysam.VariantFile(args.output_vcf, 'w', header=vcf_reader.header)

    for record in vcf_reader.fetch():
        try:
            if args.sv:
                genes=record.info["Gene_name"][0].split("|")
                diseases = []
                for gene in genes:
                    if gene in gene_diseases:
                        diseases.append('_'.join(gene_diseases[gene]))
                    else:
                        diseases.append('.')
                check = bool([i for i in diseases if i != '.'])
                if check:        
                    record.info["ISEQ_DISEASE_HPO"] = '|'.join(diseases) 
                else:
                    continue     
            else:                 
                splitted_for_colon = record.info["ISEQ_GENES_NAMES"][0].split(":")[0]
                if splitted_for_colon in gene_diseases:
                    record.info["ISEQ_DISEASE_HPO"] = '_'.join(gene_diseases[splitted_for_colon])
                else:
                    continue
        except:
            continue
        finally:
            vcf_writer.write(record)
