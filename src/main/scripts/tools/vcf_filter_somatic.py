#!/usr/bin/python3

import argparse
import pysam
import sys


__version__ = '0.0.1'


def check_civic(record):
    try:
        return bool(record.info["ISEQ_CIVIC_EVIDENCE_INFO"])
    except KeyError:
        return False


def check_cosmic(record):
    try:
        return len(record.info["ISEQ_COSMIC_ID"]) > 4

    except:
        return False


def read_oncogenes(oncogenes_file):
    oncogenes_list = []
    with open(oncogenes_file) as f:
        for line in f:
            line = line.strip()
            oncogenes_list.append(line)
    return oncogenes_list


def check_if_pathogenic(record):
    try:
        acmg = record.info["ISEQ_ACMG_SUMMARY_CLASSIFICATION"]
        if acmg in ["Pathogenic", "Likely^Pathogenic"]:
            acmg_test = True
        else:
            acmg_test = False
    except KeyError:
        print("WARNING: ACMG annotations not present", file=sys.stderr)
        acmg_test = False
    try:
        clinvar = record.info["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0]
        if clinvar in ["pathogenic", "pathogenic/likely_pathogenic", "likely_pathogenic",
                       "pathogenic_low_penetrance", "likely_pathogenic_low_penetrance"]:
            clinvar_test = True
        else:
            clinvar_test = False
        #print(clinvar_test)
    except:
        clinvar_test = False
    return acmg_test or clinvar_test


def check_if_oncogene(record, genes_list):
    try:
        genes = record.info["ISEQ_GENES_NAMES"][0].split(":")
        return bool(list(set(genes) & set(genes_list)))
    except (KeyError):
        return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Filter for somatic report (civic, cosmic and oncogenes)')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input_vcf', '-i', type=str,
                        required=True, help='input vcf file')
    parser.add_argument('--output_vcf', '-o', type=str,
                        required=True, help='output vcf file')
    parser.add_argument('--oncogenes', '-l', type=str,
                        required=True, help='List of oncogenes (one per line)')
    parser.add_argument('--sample', '-s', type=str,
                        required=True, help='Tumor sample name')
    parser.add_argument('--target_analysis', '-t', required=False, action='store_true',
                        help='Targeted analysis? (skip filtering on oncogenes)')

    args = parser.parse_args()

    vcf_reader = pysam.VariantFile(args.input_vcf)
    try:
        vcf_reader.subset_samples([args.sample])
    except ValueError:
        print("WARNING: Wrong sample name given", file=sys.stderr)
    vcf_writer = pysam.VariantFile(
        args.output_vcf, 'w', header=vcf_reader.header)
    if not args.target_analysis:
        oncogenes = read_oncogenes(args.oncogenes)

    for record in vcf_reader.fetch():
        #print("CIVIC")
        #print(check_civic(record))
        #print("COSMIC")
        #print(check_cosmic(record))
        #print("ONCOGENE")
        #print(check_if_oncogene(record, oncogenes))
        #print("PATHOGENIC")
        #print(check_if_pathogenic(record))
        if not args.target_analysis:
            if check_civic(record) or check_cosmic(record) or (
                    check_if_oncogene(record, oncogenes) and check_if_pathogenic(record)):
                vcf_writer.write(record)
        else:
            if check_civic(record) or check_cosmic(record) or check_if_pathogenic(record):
                vcf_writer.write(record)
