#!/usr/bin/python3

from numpy.testing import assert_equal
from vcf_anno_ge_panels import *


def test_index_panels_by_gene():
    table = {
                "panel_name": "ACMG Incidental Findings",
                "genes": [
                    "ACTA2"
                ]
            },{
                "panel_name": "COVID-19 research",
                "genes": [
                    "ACTA2",
                    "ACTC1"
                ]}
    result = index_panels_by_gene(table)
    expected = {"ACTA2": "ACMG_Incidental_Findings^COVID-19_research", "ACTC1": "COVID-19_research"}
    assert_equal(result, expected)


def test_index_panel_all():
    panels = load_json("../../../../resources/gene-panels/104/genomics-england-panels.json")
    result = index_panels_by_gene(panels)
    assert "ACTA2" in result
    