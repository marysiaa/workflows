#!/user/bin/env python3

import argparse
import pysam

__version__ = "0.0.2"


def create_dict(data_file):
    out_dict = {}
    with open(data_file) as f:
        for line in f:
            split_line = line.split("\t")
            out_dict[split_line[0]] = int(split_line[1])
    return out_dict


def get_info_data(line, info_field, separator):
    try:
        return line.info[info_field][0].split(separator)
    except IndexError:
        return line.info[info_field].split(separator)
    except KeyError:
        return None


def inheritance_test(inheritance_list, inheritance_mode):
    inheritance_terms = [1 for i in inheritance_list if inheritance_mode in i]
    return sum(inheritance_terms) > 0


def main(in_vcf_name, out_vcf_name):
    in_vcf = pysam.VariantFile(filename=in_vcf_name)
    items = [('ID', "ISEQ_PER_GENE_VARIANT_COUNT"), ('Number', "1"), ('Type', "Integer"),
             ('Description',
              "Number of non-benign variants in the first gene listed in the ISEQ_GENES_NAMES field (for the fist sample)")]
    in_vcf.header.add_meta("INFO", items=items)

    items = [('ID', "ISEQ_PER_GENE_ALT_COUNT"), ('Number', "1"), ('Type', "Integer"),
             ('Description',
              "Number of non-benign alt alleles in the first gene from the ISEQ_GENES_NAMES field (for the first sample)")]
    in_vcf.header.add_meta("INFO", items=items)

    items = [('ID', "ISEQ_GENOTYPE_TO_INHERITANCE_MATCH"), ('Number', "1"), ('Type', "String"),
             ('Description', "Match between mode of inheritance and genotype (includes also compound heterozygotes)")]
    in_vcf.header.add_meta("INFO", items=items)

    items = [('ID', "ISEQ_COMPOUND_HET"), ('Number', "0"), ('Type', "Flag"),
             ('Description', "Possible compound heterozygote")]
    in_vcf.header.add_meta("INFO", items=items)

    out_vcf = pysam.VariantFile(out_vcf_name, 'w', header=in_vcf.header)

    for record in in_vcf.fetch():
        sample = record.samples[0]
        # print(sample['GT'])
        ## get first gene name
        try:
            first_gene = get_info_data(record, "ISEQ_GENES_NAMES", ":")[0]
            # print(first_gene)
        except TypeError:
            pass
        else:
            if first_gene not in [".", None]:
                gene_variants_number = gene_variants_count[first_gene]
                gene_alt_alleles_number = gene_alt_count[first_gene]
                record.info["ISEQ_PER_GENE_VARIANT_COUNT"] = gene_variants_number
                record.info["ISEQ_PER_GENE_ALT_COUNT"] = gene_alt_alleles_number
                if record.contig == "chrM":
                    record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "mitochondrial"
                else:
                    gt = sample["GT"]
                    is_hom = gt[0] == gt[1]

                    ## get list of inheritance modes, for first gene only
                    try:
                        inheritance = get_info_data(record, "ISEQ_HPO_INHERITANCE", ":")[0].split("^")
                    except TypeError:
                        inheritance = None
                    # print(inheritance)
                    if inheritance is not None:
                        dominant = inheritance_test(inheritance, "dominant")
                        recessive = inheritance_test(inheritance, "recessive")
                    else:
                        dominant = False
                    ## more than one ono-benign allele per gene (one or more variants)
                    if gene_alt_alleles_number > 1:
                        ## GT: hom and het
                        if dominant:
                            if not recessive:
                                record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "dominant"
                            elif recessive and not is_hom:
                                record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "possibly_dominant"
                            elif recessive and is_hom:
                                record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "recessive"
                        ## GT hom and inheritance recessive or not given
                        elif not dominant and is_hom:
                            record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "recessive"
                        ## GT: het and inheritance recessive or not given
                        elif not is_hom and not dominant:
                            record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "recessive"
                            record.info.__setitem__('ISEQ_COMPOUND_HET', 1)
                    ## only one non-benign allele
                    else:
                        if dominant:
                            if not recessive:
                                record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "dominant"
                            else:
                                record.info['ISEQ_GENOTYPE_TO_INHERITANCE_MATCH'] = "possibly_dominant"

        # print(record.info['ISEQ_COMPOUND_HET'])
        out_vcf.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Adds annotations useful for inheritance analysis')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True,
                        help='Input vcf file (may be bgzipped)')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True,
                        help='Output vcf file (will be bgzipped - if .gz suffix present)')
    parser.add_argument('--gene-variant-count', '-m', metavar='gene_variant_count', type=str, required=True,
                        help='TSV file with non-benign variant per gene counts')
    parser.add_argument('--gene-alt-count', '-a', metavar='gene_alts', type=str, required=True,
                        help='TSV file with non-benign alt allele per gene counts')
    args = parser.parse_args()

    gene_variants_count = create_dict(args.gene_variant_count)
    gene_alt_count = create_dict(args.gene_alt_count)
    main(args.input_vcf, args.output_vcf)
