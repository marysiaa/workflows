require(dplyr)
require(jsonlite)
require(argparse)
require(readr)

version <- '0.0.2'

parser <-
  ArgumentParser(description = 'Prepares syndrome summary jsons')

parser$add_argument('-v',
                    '--version',
                    action = 'version',
                    version = paste(sub(".*=.*/", "", commandArgs()[4]), version))
parser$add_argument('--patient_data', type = 'character', help = 'Path to the patient tsv file  - result of the bedtools intersect (clinvar or syndromes)')
parser$add_argument('--database',
                    type = 'character',
                    default = "decipher",
                    help = 'Which tsv file: clinvar or decipher')
parser$add_argument('--decipher',
                    default = "/resources/decipher.cnv",
                    type = 'character',
                    help = 'Path to the decipher syndromes tsv file')
parser$add_argument('--output_dir',
                    type = 'character',
                    help = 'Path to the output directory',
                    default = ".")
args <- parser$parse_args()


syndromes_patient_data <-
  read.delim(args$patient_data,
             header = TRUE,
             comment.char = "#")


database <- args$database

syndromes_filtered <-
  syndromes_patient_data %>%
  filter(overlap..percent.of.the.database.variant. > 50) %>%
  mutate(
    "size" = end - start,
    "oversize.percent" = 100 * (size - overlap..bp.) / overlap..bp.
  ) %>%
  filter(oversize.percent < 500)

if (database == "decipher") {
  all_syndromes <- read.delim(args$decipher,
                              header = TRUE,
                              comment.char = "#")
  all_syndromes <- all_syndromes %>%
    select(Syndrome.Name, Pse.Status) %>%
    rename("Syndrome_Name" = Syndrome.Name) %>%
    mutate(Syndrome_Name = as.factor(Syndrome_Name))
  
  syndromes_filtered <-
    syndromes_filtered %>%
    rename("Syndrome_Name" = syndrome.name) %>%
    mutate(Syndrome_Name = as.factor(Syndrome_Name)) %>%
    left_join(., all_syndromes, by = "Syndrome_Name") %>%
    group_by(chr, start, end, Pse.Status) %>%
    arrange(desc(overlap..percent.of.the.database.variant.),
            oversize.percent) %>%
    mutate(n = row_number()) %>%
    filter(n == 1)
} else {
  syndromes_filtered <-
    syndromes_filtered %>%
    group_by(chr, start, end) %>%
    arrange(desc(overlap..percent.of.the.database.variant.),
            oversize.percent) %>%
    mutate(n = row_number()) %>%
    filter(n == 1)
}
syndromes_filtered <-
  syndromes_filtered %>%
  mutate(
    "overlap.start" = max(start, database.variant.start),
    "overlap.end" = min(end, database.variant.end),
    "Location" = paste(
      gsub("chr", "", chr),
      ":",
      as.character(start) ,
      "-",
      as.character(end),
      sep = ""
    ),
    "Overlap_Region" = paste(
      chr,
      ":",
      as.character(overlap.start),
      "-",
      as.character(overlap.end),
      sep = ""
    ),
    "Size" = if_else(
      size >= 1000000,
      paste(as.character(round(size / 1000000, 1)), "Mb", sep = " "),
      paste(as.character(round(size / 1000, 2)), "kb", sep = " ")
    ),
    "Overlap_Percentage" = paste(as.character(
      round(overlap..percent.of.the.database.variant., 3)
    ), "%", sep = ""),
    "Oversize_Percentage" = paste(as.character(round(oversize.percent, 3)), "%", sep = ""),
    "Zscore" = as.character(round(zscore, 2)),
    "Match" = TRUE,
    "StartBand" = "",
    "EndBand" = ""
  ) %>%
  mutate(
    Size = if_else(grepl("\\.[0-9] ", Size), Size, sub(' ', '.0 ', Size)),
    Overlap_Percentage = if_else(
      grepl("\\.[0-9]", Overlap_Percentage),
      Overlap_Percentage,
      sub('%', '.0%', Overlap_Percentage)
    ),
    Oversize_Percentage = if_else(
      grepl("\\.[0-9]", Oversize_Percentage),
      Oversize_Percentage,
      sub('%', '.0%', Oversize_Percentage)
    )
  ) %>%
  ungroup() %>% as.data.frame()

if (database == "clinvar") {
  syndromes_filtered <- syndromes_filtered %>%
    filter(
      CLNSIG %in% c(
        "Pathogenic",
        "Likely pathogenic",
        "Pathogenic/Likely_pathogenic",
        "Uncertain",
        "Conflicting_interpretations_of_pathogenicity",
        "not_provided"
      )
    ) %>%
    mutate(
      "Clinical_Relevance" = if_else(
        CLNSIG %in% c(
          "Pathogenic",
          "Likely_pathogenic",
          "Pathogenic/Likely_pathogenic"
        ),
        gsub("_", " ", as.character(CLNSIG)),
        if_else(end - start > 3000000, "Pathogenic by size", "")
      ),
      "Syndrome_Name" = paste(as.character(CLNHGVS), " (AlleleID ", ALLELEID, ")", sep =
                                ""),
      "Genotype_Class" = CLNVC
    )
} else{
  syndromes_filtered <- syndromes_filtered %>%
    mutate(
      "Clinical_Relevance" = if_else(end - start > 3000000, "Pathogenic by size", ""),
      "Genotype_Class" = genotype.class
    )
  
  
}
syndromes_filtered <-
  syndromes_filtered  %>%
  select(
    Syndrome_Name,
    Location,
    StartBand,
    EndBand,
    Zscore,
    Size,
    Clinical_Relevance,
    Overlap_Percentage,
    Oversize_Percentage,
    Overlap_Region,
    Genotype_Class,
    Match
  ) %>% as.data.frame()

if (database == "decipher") {
  pse <- all_syndromes %>%
    filter(Pse.Status == "pse") %>%
    select(Syndrome_Name)
  
  nopse <- all_syndromes %>%
    filter(Pse.Status == "no_pse") %>%
    select(Syndrome_Name)
  
  pse <-
    pse %>% left_join(., syndromes_filtered, by = "Syndrome_Name") %>%
    mutate(Match = if_else(is.na(Match), FALSE, TRUE)) 
  
  matched_syndromes <- pse %>%
    filter(Match == TRUE) %>%
    mutate("matched_syndromes" = paste(Syndrome_Name, collapse = ", "))
  
  matched_syndromes <- matched_syndromes$matched_syndromes[1]
  matched_syndromes <- sub(",([^,]*)$", " and\\1", matched_syndromes)
  matched_syndromes_upper <- sub(",([^,]*)$", " AND\\1", matched_syndromes)
  print(matched_syndromes)
  if (is.na(matched_syndromes)) {
    SUMCNV_MICRODEL <- c("LOW RISK FOR TESTED COPY NUMBER VARIANTS SYNDROMES.")
    PPVCNVARIANT <- c("")
    CNVARIANT <- c("LOW RISK FOR TESTED COPY NUMBER VARIANTS")
    INTCNVARIANT <- c("Result consistent with no deletions or duplications detected in the regions covered by the test.")
    
  } else {
    PPVCNVARIANT <- c("N/A**")
    CNVARIANT <- c("HIGH RISK FOR TESTED COPY NUMBER VARIANTS")
    intcnvariant_text <- paste("Result consistent with", matched_syndromes, "covered by the test.", sep = " ")
    INTCNVARIANT <- c(intcnvariant_text)
    sumcnv_microdel_text = paste("HIGH RISK FOR ", matched_syndromes_upper, ".", sep = "")
    SUMCNV_MICRODEL <- c(sumcnv_microdel_text)  
      }
  
  cnv_summary <- as.data.frame(cbind(SUMCNV_MICRODEL, PPVCNVARIANT, CNVARIANT, INTCNVARIANT))
  
  #print(head(cnv_summary))
  write_tsv(cnv_summary,
        paste(args$output_dir,
              "/",
              "cnv-summary.tsv", sep = ""))  
    
  nopse <-
    nopse %>% left_join(., syndromes_filtered, by = "Syndrome_Name") %>%
    mutate(Match = if_else(is.na(Match), FALSE, TRUE))
  
  
  pse_json <- jsonlite::toJSON(pse, pretty = TRUE, na = "null")
  write(pse_json,
        paste(args$output_dir,
              "/",
              "syndromes_summary_pse.json",
              sep = ""))
  
  nopse_json <- jsonlite::toJSON(pse, pretty = TRUE, na = "null")
  write(nopse_json,
        paste(args$output_dir,
              "/",
              "syndromes_summary_nopse.json",
              sep = ""))
} else {
  database_json <- jsonlite::toJSON(syndromes_filtered, pretty = TRUE)
  write(database_json,
        paste(args$output_dir, "/",  "clinvar_summary.json", sep = ""))
  
}  