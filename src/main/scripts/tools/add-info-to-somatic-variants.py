#!/usr/bin/python3

import argparse
import json
import re
import pandas as pd
import math


__version__ = "0.0.1"


def load_json(filename: str) -> dict:
    with open(filename, "r") as f:
        return json.load(f)


def load_tsv(filename: str) -> dict:
    return pd.read_csv(filename, sep="\t")


def add_report_info(variants: list, df: pd.DataFrame):
    for variant in variants:
        variant["CancerTypes"] = None
        variant["Function"] = None
        variant["Known_mutation_types"] = None
        matched = df[df["GeneSymbol"] == variant["ISEQ_GENES_NAMES"]]
        if not matched.empty:
            variant["CancerTypes"] = {
                "Somatic": prepare_cancer_types(matched["TumourTypesSomatic"].iloc[0]),
                "Hereditary": prepare_cancer_types(matched["TumourTypesGermline"].iloc[0]),
                "Syndromes": prepare_cancer_types(matched["CancerSyndrome"].iloc[0])
            }
            variant["Function"] = matched["Function"].iloc[0]
            variant["Known_mutation_types"] = matched["Mutationtranslocationype"].iloc[0]


def prepare_cancer_types(tumour_types: str):
    diseases = []
    if not(type(tumour_types) == float and math.isnan(tumour_types)):
        for disease in re.split(r';\s*(?![^()]*\))', tumour_types):
            diseases.append(re.sub('([a-zA-Z])', lambda x: x.groups()[0].upper(), disease, 1))
    return diseases


def save_json(variants: list, filename: str):
    with open(filename, "w") as f:
        json.dump(variants, f, indent=4)


def main():
    parser = argparse.ArgumentParser(description="Add info to somatic variants")
    parser.add_argument("--variants", type=str, required=True, help="Path to a JSON file containing the variants")
    parser.add_argument("--info", type=str, required=True, help="Path to a TSV file containing the cancer genes census")
    parser.add_argument("--output-filename", type=str, required=True, help="Path to output JSON file")
    args = parser.parse_args()

    # Load the JSON files
    variants = load_json(args.variants)
    df = load_tsv(args.info)

    # Add the report info
    add_report_info(variants, df)

    # Save JSON
    save_json(variants, args.output_filename)


if __name__ == "__main__":
    main()
