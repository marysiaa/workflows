#!/usr/bin/env python3

import argparse
from typing import Union  # do klasy

__version__ = '0.0.1'
Num = Union[int, float]  # do klasy


# dwie przykładowe klasy do testowania obrazujące różne podejście do testów
# w przypadku klasy Rectangle nie obchodzi as za bardzo, które pole ma jaką  wartość, ważne żeby zrwacała
# poprawnie pole i obwód
# w przypadku klasy RsInfo ważne, żeby odpowiednia wartoć trafiła do odpowiedniego pola
# żeby np nie zamienić ref z alt


class Rectangle(object):
    def __init__(self, a: Num, b: Num):
        assert (isinstance(a, int) or isinstance(a, float)) and not isinstance(a, bool)
        assert (isinstance(b, int) or isinstance(b, float)) and not isinstance(b, bool)
        assert a >= 0
        assert b >= 0
        super().__init__()
        self.a = a
        self.b = b

    def perimeter(self) -> Num:
        return 2 * self.a + 2 * self.b

    def area(self) -> Num:
        return self.a * self.b


class RsInfo(object):

    def __init__(self, rsid: str, ref: str, alt: str):
        super().__init__()
        assert isinstance(rsid, str)
        assert isinstance(ref, str)
        assert isinstance(alt, str)
        self.rsid = rsid
        self.ref = ref
        self.alt = alt


def fib(n: int) -> int:  # przykładowa funcja do testowania
    """
    Funkcja licząca n-ty wyraz ciągu Fibonacciego.
        Zmodyfikowany przykład z https://python101.readthedocs.io/pl/latest/podstawy/przyklady/przyklad04.html
    """
    assert isinstance(n, int) and not isinstance(n, bool)
    assert n >= 0
    if n < 1:
        return 0
    if n < 2:
        return 1
    return fib(n - 1) + fib(n - 2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='The script calculates LRR and BAF for multisample VCF')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    # more arguments
    arguments = parser.parse_args()
    # more code
