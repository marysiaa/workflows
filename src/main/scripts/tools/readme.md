## TOOLS

This directory contain scripts that perform processing of VCF, FASTQ and other typical formats containing genetic data.

---

## Table of contents

**Tools:**
  * [Annotate SnpEff VCF with genes symbol (.py)](#annotate-snpeff-vcf-with-genes-symbols)
  * [Annotate VCF with a dictionary (.py)](#annotate-vcf-with-a-dictionary)
  * annotate-vcf-with-tsv
  * [Create a dictionary from flat file with two columns (.py)](#create-a-dictionary-from-flat-file-with-two-columns)
  * [Cut samples from VCF (.py)](#cut-samples-from-vcf)
  * exon-coverage
  * [Filter VCF by gene panel (.py)](#filter-vcf-by-gene-panel)
  * [Flag multiallelic sites in VCF (.py)](#flag-multiallelic-sites-in-vcf)
  * [Genotype gtc2vcf (.py)](#genotype-gtc2vcf)  
  * [Group calling intervals (.py)](#group-calling-intervals)
  * [Keep or remove VCF fields (.py)](#keep-or-remove-vcf-fields)
  * quality-check
  * [Simple removal of specified format field from VCF (.py)](#simple-removal-of-specified-format-field-from-vcf)
  * [Sort INFO columns in VCF (.py)](#sort-info-columns-in-vcf)
  * [Split FASTQ by lines (.py)](#split-fastq-by-lines)
  * [Split VCF file (.py)](#split-vcf-file)
  * [Translate valueas of a dictionary (.py)](#translate-valueas-of-a-dictionary)
  * vcftocsv
  * vcftojson
  * [Samtools flagstats to json (.py)](#samtools-flagstats-to-json)  

[Return to: scripts ](./../readme.md)

---

### Annotate SnpEff VCF with genes symbols

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**annotate-snpeff-vcf-with-genes-symbols.py 0.0.4**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/annotate-snpeff-vcf-with-genes-symbols.py)

```
usage: annotate-snpeff-vcf-with-genes-symbols.py [-h] [-v]
                                                 [-t {MODERATE,LOW,MODIFIER,HIGH}]
                                                 [-g GENES_FIELD_NAME]
                                                 [-i IMPACT_FIELD_NAME]
                                                 [-s SNPEFF_VERSION]

The script annotates the VCF provided as stdin:
  1. with the highest SnpEff impact (within protein coding transcripts with no warnings and errors)  
  2. with symbols of genes, that a given variant have specified or greater than specified threshold SnpEff impact (only protein coding transcripts without errors/warnings are checked)  
  3. with ISEQ_REPORT_ANN field, which contains ANN subfield (transcrpt) that will be displayed in report  
Input VCF must be previously annotated by SnpEff.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -t {MODERATE,LOW,MODIFIER,HIGH}, --impact {MODERATE,LOW,MODIFIER,HIGH}
                        IMPACT threshold
  -g GENES_FIELD_NAME, --genes-field-name GENES_FIELD_NAME
                        Name of a field with genes symbols in output VCF file
  -i IMPACT_FIELD_NAME, --impact-field-name IMPACT_FIELD_NAME
                        Name of a field with highest impact in output VCF file
  -s SNPEFF_VERSION, --snpeff-version SNPEFF_VERSION
                        Version of SnpEff

```

**Examples**:

Input file:

```
##fileformat=VCFv4.2
(...)
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	sample_ID
chr14	18974908	.	A	T	762.19	PASS	ANN=T|missense_variant|MODERATE|POTEM|ENSG00000222036|transcript|ENST00000552966.5|nonsense_mediated_decay|4/12|c.958A>T|p.Ile320Phe|1010/2073|958/969|320/322||,T|upstream_gene_variant|MODIFIER|RNU6-1239P|ENSG00000212612|transcript|ENST00000391310.1|snRNA||n.-3939A>T|||||3939|,T|downstream_gene_variant|MODIFIER|CTD-2311B13.5|ENSG00000275563|transcript|ENST00000613990.1|antisense||n.*2272T>A|||||2272|,T|intron_variant|MODIFIER|POTEM|ENSG00000222036|transcript|ENST00000547889.5|protein_coding|3/10|c.811-1141A>T||||||,T|intron_variant|MODIFIER|POTEM|ENSG00000222036|transcript|ENST00000616847.1|nonsense_mediated_decay|3/11|c.811-1141A>T||||||	GT:AD:DP:GQ:PL	0/1:24,4:28:2:2,0,819
```
Command 1:
```bash
zcat example_1.vcf.gz | \
python3 annotate-snpeff-vcf-with-genes-symbols.py \
	-t MODIFIER \
	-g ISEQ_GENES_NAMES \
	-i ISEQ_HIGHEST_IMPACT
```
Output 1:

```
##fileformat=VCFv4.2
(...)
##INFO=<ID=ISEQ_GENES_NAMES,Number=A,Type=String,Description="Names of genes the variant has at least MODIFIER impact on. Format: gene_symbol : (...) : gene_symbol",Source="SnpEff",Version="4.3t">
##INFO=<ID=ISEQ_HIGHEST_IMPACT,Number=A,Type=String,Description="The highest impact of an allele.",Source="SnpEff",Version="4.3t">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	sample_ID
chr14	18974908	.	A	T	762.19	PASS	(...);ISEQ_GENES_NAMES=POTEM:RNU6-1239P:CTD-2311B13.5;ISEQ_HIGHEST_IMPACT=MODERATE	GT:AD:DP:GQ:PL	0/1:24,4:28:2:2,0,819
```

Command 2:
```bash
zcat example_1.vcf.gz | \
python3 annotate-snpeff-vcf-with-genes-symbols.py \
	-t MODERATE \
	-g ISEQ_GENES_NAMES \
	-i ISEQ_HIGHEST_IMPACT
```

Output 2:
```
##fileformat=VCFv4.2
(...)
##INFO=<ID=ISEQ_GENES_NAMES,Number=A,Type=String,Description="Names of genes the variant has at least MODERATE impact on. Format: gene_symbol : (...) : gene_symbol",Source="SnpEff",Version="4.3t">
##INFO=<ID=ISEQ_HIGHEST_IMPACT,Number=A,Type=String,Description="The highest impact of an allele.",Source="SnpEff",Version="4.3t">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	sample_ID
chr14	18974908	.	A	T	762.19	PASS	(...);ISEQ_GENES_NAMES=POTEM;ISEQ_HIGHEST_IMPACT=MODERATE	GT:AD:DP:GQ:PL	0/1:24,4:28:2:2,0,819
```

[Return to the table of contents](#table-of-contents)

***

### Annotate VCF with a dictionary

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**annotate-vcf-with-a-dictionary.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/annotate-vcf-with-a-dictionary.py@0.0.1/src/main/scripts/tools/annotate-vcf-with-a-dictionary.py)

```
usage: annotate-vcf-with-a-dictionary.py [-h] [-v]
                                         [-k INPUT_VCF_KEYS_DELIMETER]
                                         [-d DICTIONARY_VALUES_DELIMETER]
                                         [-o OUTPUT_VCF_VALUES_DELIMETER]
                                         DICTIONARY_PATH KEY_COLUMN_NAME
                                         FIELD_NAME HEADER

Annotates VCF with a dictionary.

**Dictionary** is defined as a flat file consisted of a VCF-style header (however "Number" field is meaningless
for dictionaries, so it's value should always be a ".") and body with lines consisting of tab delimited pairs
of keys and values.

  + Dictionary format:

      ##INFO=<ID=${KEY_FIELD_NAME},Number=.,Type=(...),Description="(...)",Source="(...)",Version="(...)">
      ##INFO=<ID=${VALUES_FIELD_NAME},Number=.,Type=(...),Description="(...)",Source="(...)",Version="(...)">
      KEY_1   value_1_1,value_1_2,(...),value_1_n
      KEY_2   value_2_1,value_2_2,(...),value_2_k
      (...)
      KEY_N   value_N_1,value_N_2,(...),value_N_m

   + Characters forbidden in keys and values: ^ , ~ # : | ;

**Input VCF** file must contain an INFO field with keys. Allowed format is:

  + one set of colon-delimited keys per allele ("Number=A"):

    INFO_FIELD_WITH_KEYS=key:key:(...):key,key:key:(...):key,(...),key:key:(...):key

**Output VCF** will contain field the follwing field

  + each key is substituted for "^" delimited list of values from the dictionary

  + if the key is absent in the dictionary, that key is substituted with a dot (".")

    FIELD_NAME=value^value^(...)^value:value^value^(...)^value^(...),(...),value^value^(...)^value:value^value^(...)^value:(...):value^value^(...)^value

positional arguments:
  DICTIONARY_PATH       path to a dictionary
  KEY_COLUMN_NAME       specify name of a VCF INFO field containing keys
  FIELD_NAME            specify name of a field that will contain annotation
  HEADER                specify header line corresponding to a field
                        containing annotation

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -k INPUT_VCF_KEYS_DELIMETER, --input-vcf-keys-delimeter INPUT_VCF_KEYS_DELIMETER
                        specify delimiter in input VCF INFO field containing
                        keys (Default:":")
  -d DICTIONARY_VALUES_DELIMETER, --dictionary-values-delimeter DICTIONARY_VALUES_DELIMETER
                        specify delimiter in dictionary delimiting values
                        (Default:",")
  -o OUTPUT_VCF_VALUES_DELIMETER, --output-vcf-values-delimeter OUTPUT_VCF_VALUES_DELIMETER
                        specify delimiter in output VCF delimiting values in
                        field with annotation (Default:"^")
```

[Return to the table of contents](#table-of-contents)

***


***
### Create a dictionary from flat file with two columns

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**create-a-dictionary-from-flat-file-with-two-columns.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/create-a-dictionary-from-flat-file-with-two-columns.py@0.0.1/src/main/scripts/tools/create-a-dictionary-from-flat-file-with-two-columns.py)

```
usage: create-a-dictionary-from-flat-file-with-two-columns.py
       [-h] [-v] [-f F] [-d D] [-w] [-c] [-e] [-k K] [-l L]
       KEY_INDEX VALUES_INDEX

Create a tab-delimited dictionary from two columns of a delimiter-separated file, piped as *stdin*.

Values of a key in a dictionary are by default
  - uniquified,
  - alphabetically sorted,
  - comma delimited,
  - whitespaces are replaced by underscores
  - all letters are changed to lower case
  - commas within values are erased.

Output is printed to *stdout*.


positional arguments:
  KEY_INDEX      index of column containing keys (column numeration is 0
                 based)
  VALUES_INDEX   index of column containing VALUES (column numeration is 0
                 based)

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
  -f F           specify delimiter in an input file (Default: \t)
  -d D           specify delimiter in an output dictionary (Default: ,)
  -w             do not replace whitespaces with underscores
  -c             preserve capitalization
  -e             do not erase commas within values
  -k K           use if column from which keys are to be extracted contains
                 multiple records. specify delimiting characters
  -l L           use if column from which values are to be extracted contains
                 multiple records. specify delimiting characters
```
[Return to the table of contents](#table-of-contents)

***

### Cut samples from VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**cut-samples-from-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/cut-samples-from-vcf.py@0.0.1/src/main/scripts/tools/cut-samples-from-vcf.py)

```
usage: cut-samples-from-vcf.py [-h] [-f] [-r] [-x] [-v] SAMPLES_LIST

Print VCF containing genotypes only for specified samples or for all samples except those specified.

positional arguments:
  SAMPLES_LIST   Comma-separated list of samples IDs

optional arguments:
  -h, --help     show this help message and exit
  -v, --version         show program's version number and exit
  -f             Provide list of samples IDs in a file rather that a comma-
                 separated list of samples IDs (one sample ID per line)
                 (Default: cut all the samples except selected)
  -r             Cut out selected samples (Default: cut all the samples except
                 selected)
  -x             Do not exclude lines with no alternative genotype (Default:
                 print only lines with at least one non-reference allele
```

[Return to the table of contents](#table-of-contents)

***

### Filter VCF by gene panel

**Author**: Maria Paleczny

**Language**: python3

**Current version**: [**filter-vcf-by-gene-panel.py 0.0.2**](https://gitlab.com/intelliseq/workflows/-/blob/filter-vcf-by-gene-panel.py@0.0.2/src/main/scripts/tools/filter-vcf-by-gene-panel.py)

```
usage: filter-vcf-by-gene-panel.py [-h] [--skip_filter] [-v] GENE_PANEL_JSON

FILTERING VCF BY GENE PANEL
        _
      _<_/_
   __/    _>
  '\  '  |
    \___/
    /+++ o=|..|..|
   | o/..|
0==|+++++|
 0======/

The script takes input VCF file as stdin and gene panel file in JSON format as argument and perform the following actions:
  - filtering VCF file, so only variant in genes specified in genes panel remains (this may be switch off by adding --skip_filter flag)
  - annotating VCF file with ISEQ_GENE_PANEL_SCORE and ISEQ_GENE_PANEL_TYPE field

This gene panel JSON should be formatted as follows:

    [
        {"name":"<gene_symbol_1>","score":<score>,"type":"<gene_panel_type_a_gene_originates>"},
        {"name":"<gene_symbol_2>","score":<score>,"type":"<gene_panel_type_a_gene_originates>"},
        (...)
        {"name":"<gene_symbol_n>","score":<score>,"type":"<gene_panel_type_a_gene_originates>"}
    ]

 where:

  + score should be in the range from 0 to 100
  + posible gene panels types are:
      - phenotypes panel - type should be "phenotype"
      - user-defined panel - type should be "phenotype" and score should be "100"
      - diseases panel = type should be "<disease_name>" or "<group_of_diseases_name>"

 USE EXAMPLE:
   zcat example.vcf.gz | python3 filter-vcf-by-gene-panel.py gene-panel.json [--skip_filter] | bgzip > example.filtered.vcf.gz

 

positional arguments:
  GENE_PANEL_JSON  path to input gene panel file, it should be json with
                   structure {name: WNT5A, score: 20.0},

optional arguments:
  -h, --help       show this help message and exit
  --skip_filter    Use this flag to ommit filtering step
  -v, --version    show program's version number and exit
```

[Return to the table of contents](#table-of-contents)

***

### Flag multiallelic sites in VCF


**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**flag-multiallelic-sites.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/flag-multiallelic-sites.py@0.0.1/src/main/scripts/tools/flag-multiallelic-sites.py)

```
usage: flag-multiallelic-sites.py [-h] [-v]

  Annotates VCF file with the following fields:
    +  MULTIALLELIC_SITE - Multiallelic site. Determined by counting ALT alleles observed in samples in input VCF
    +  ALTS - ALT alleles observed on this site. Determined by counting ALT alleles observed in samples in input VCF
    +  ALTS_pos - Position of ALT alleles observed on this sites in input VCF
    +  ALTS_indices - Index of a corresponding ALT allele from ALTS field (1-based)

 Takes as an stdin VCF file normalized with command: bcftools norm -m +both.

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

***

### Genotype gtv2vcf vcf


**Author**:  Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**: [**genotype-gtc2vcf 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/genotype-gtc2vcf.py)

```
usage: genotype-gtc2vcf.py [-h] [-v] --input-vcf input_vcf --output-vcf
                           output_vcf [--ci-multiplier ci_multiplier]

Fills genotypes in freeseek gtc2vcf output

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  --input-vcf input_vcf, -i input_vcf
                        Path to input vcf file (vcf can be bgzipped)
  --output-vcf output_vcf, -o output_vcf
                        Path to output vcf file (vcf can be bgzipped)
  --ci-multiplier ci_multiplier, -m ci_multiplier
                        This option decides how wide should be THETA CI for
                        genotypes: theta dev times the multiplier. Default:
                        3.0
```

[Return to the table of contents](#table-of-contents)


### Group calling intervals

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**group-calling-intervals.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/group-calling-intervals.py@0.0.1/src/main/scripts/tools/group-calling-intervals.py)

```
usage: group-calling-intervals.py [-h] [-v] [-d D] INTERVAL_FILE MAX_NO_GROUPS

Takes an interval_list file (Picard style) as an input and outputs a directory with smaller
interval_list files that have similar sums of lenght.

positional arguments:
  INTERVAL_FILE  path to an interval_file
  MAX_NO_GROUPS  maximum number of output interval_list files

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
  -d D           output directory (Default: the directory containing
                 INTERVAL_FILE)
```

[Return to the table of contents](#table-of-contents)

***

### Keep or remove VCF fields

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**keep-or-remove-vcf-fields.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.py)

```
usage: keep-or-remove-vcf-fields.py [-h] [-v] [-g] [-k] [-r]
                                    [-f TXT_FILE_WITH_FIELDS | -l COMMA_DELIMITED_LIST_OF_FIELDS]

Keeps or removes only specified fields in INFO section of VCF file, optionally
may remove FORMAT column and genotype INFO. Names of fields to keep or remove
can be supplied as a file or via command line as a comma delimited list.
Default mode: keep fields.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -g, --remove-format-and-genotype
                        Remove FORMAT column and genotype info
  -k, --keep-empty-info-lines
                        Do not print line, if after removing redundant fields,
                        INFO colum is empty.
  -r, --remove-fields   Supply fields names to remove rather than to keep
  -f TXT_FILE_WITH_FIELDS, --txt-file-with-fields TXT_FILE_WITH_FIELDS
                        Path to a TXT file with names of INFO fields to keep
                        or remove (one field name per line)
  -l COMMA_DELIMITED_LIST_OF_FIELDS, --comma-delimited-list-of-fields COMMA_DELIMITED_LIST_OF_FIELDS
                        Comma delimited list of names of INFO fields to keep
                        or remove.

```

[Return to the table of contents](#table-of-contents)

***

### Simple removal of specified format field from VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**simple-removal-of-specified-format-field-from-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/simple-removal-of-specified-format-field-from-vcf.py@0.0.1/src/main/scripts/tools/simple-removal-of-specified-format-field-from-vcf.py)

```
usage: simple-removal-of-specified-format-field-from-vcf.py
       [-h] [-v] FIELD_NAME

Simple removal of specified FORMAT field from VCF.

positional arguments:
  FIELD_NAME     Name of FORMAT field to remove from VCF.

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

***

### Sort INFO columns in VCF

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**sort-info-columns-in-vcf.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/sort-info-columns-in-vcf.py@0.0.1/src/main/scripts/tools/sort-info-columns-in-vcf.py)

```
usage: sort-info-columns-in-vcf.py [-h] [-v]

Sorts INFO column fields of VCF file to appear in the same order as in the
header.

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```


[Return to the table of contents](#table-of-contents)

***

### Split FASTQ by lines

**Author**:  Maria Paleczny (https://gitlab.com/marysiaa)

**Language**: python3

**Current version**: [**split-fastq-by-lines.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/split-fastq-by-lines.py@0.0.1/src/main/scripts/tools/split-fastq-by-lines.py

```
usage: split-fastq-by-lines.py [-h] [-v] [-id [ID]] [-n [N]] [-f [F]] FASTQ

SPLITTING FASTQ BY FLOWCELL LINES

  .|||||||||.          .|||||||||.
 |||||||||||||        |||||||||||||
|||||||||||' .\      /. `|||||||||||
`||||||||||_,__o    o__,_||||||||||'

positional arguments:
  FASTQ          path to input fastq file; should be in format id_1.fq.gz or
                 id_2.fq.gz, if different use options -id and -n

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
  -id [ID]       sample ID or name
  -n [N]         pair number
  -f [F]         path to directory to save output (create if doesn't exist)
```

[Return to the table of contents](#table-of-contents)

***

### Split VCF file

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**split-vcf-file.py 0.0.2**](https://gitlab.com/intelliseq/workflows/-/blob/split-vcf-file.py@0.0.2/src/main/scripts/tools/split-vcf-file.py)


```
usage: split-vcf-file.py [-h] [-v] [-d OUTPUT_DIRECTORY] [-n BASENAME]
                         VCF_GZ NO_PIECES

    
    Takes as inputs bgzipped VCF file and:
      - creates smaller bgzipped VCF files 
      - files are saved to specified directory
    

positional arguments:
  VCF_GZ                path to VCF file
  NO_PIECES             number of output VCF files

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -d OUTPUT_DIRECTORY, --output-directory OUTPUT_DIRECTORY
                        output directory (Default: the working directory)
  -n BASENAME, --basename BASENAME
                        output vcf basename (Default: input VCF basename)
```

[Return to the table of contents](#table-of-contents)

---

### Translate values of a dictionary

**Author**:  Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**translate-values-of-a-dictionary.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/translate-values-of-a-dictionary.py@0.0.1/src/main/scripts/tools/translate-values-of-a-dictionary.py)

```
usage: translate-values-of-a-dictionary.py [-h] [-d D] [-t T] [-o O] [-k] [-v]
                                           DICTIONARY_PATH

Translate values of a tab-delimited dictionary using another dictionary (a dictionary in which those values are keys).
Output is printed to *stdout*.



positional arguments:
  DICTIONARY_PATH  path to a dictionary you want to translate with

optional arguments:
  -h, --help       show this help message and exit
  -d D             specify delimiter in a dictionary (Default: ,)
  -t T             specify delimiter in a dictionary that will be used to
                   translate (Default: ,)
  -o O             specify delimiter in an output dictionary (Default: ,)
  -k               keep untranslated values (Default: untranslated values are
                   removed)
  -v, --version    show program's version number and exit

```

[Return to the table of contents](#table-of-contents)

***
### Samtools flagstats to json  

**Author**:  Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**: [**samtools-flagstats-to-json.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/mapping-stats/src/main/scripts/tools/samtools-flagstats-to-json.py)

```
usage: samtools-flagstats-to-json.py [-h] [-v]
                                     [--percent [PERCENT [PERCENT ...]]]
                                     --input INPUT --output OUTPUT
                                     [--warnings [WARNINGS]]

Changes samtools flagstat output file to json, calculates percentages

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  --percent [PERCENT [PERCENT ...]], -p [PERCENT [PERCENT ...]]
                        Name (or names) of mapping characteristic for which
                        percent should be calculated. Spaces must be converted
                        to "_", for example: "properly_paired singletons"; if
                        set as "all" percent will be calculated for all
                        statistics, except read1 and read2
  --input INPUT, -i INPUT
                        Input file with statistics from samtools flagstat
  --output OUTPUT, -o OUTPUT
                        Output json file
  --warnings [WARNINGS], -w [WARNINGS]
                        Input file with statistics descriptions and warnings

```
[Return to the table of contents](#table-of-contents)   
