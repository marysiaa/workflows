# !/usr/bin/env python3

import argparse
import pysam
import csv


parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description="""Gene panel coverage """)

parser.add_argument('BAM', type=str, help="path to input bam file")
parser.add_argument('GENES', type=str, help="path to input bed file filtered from refGene")
parser.add_argument('cov_thresholds', type=int, nargs='+', help="coverage thresholds, e.g. 10 20 30")
parser.add_argument('-id', type=str, nargs='?', help="sample ID")
parser.add_argument('-p', type=str, nargs='?', help="gene panel name")

args = parser.parse_args()

if args.id is None:
    args.id = args.BAM.split("/")[-1].split(".")[0]
if args.p is None:
    args.p = args.GENES.split("/")[-1].split(".")[0]

N = len(args.cov_thresholds)

def cov_count(chrom, start, end, t=args.cov_thresholds):
    coverage = pysam.depth("-r", str(chrom + ":" + start + "-" + end), "-a", args.BAM).splitlines()
    sum = [0] * (len(t) + 1)
    for line in coverage:
        bc = int(line.split()[2])
        sum[0] += bc
        for i in range(len(t)):
            if bc >= t[i]: sum[i + 1] += 1
    sum[len(t) + 1:] = sum[:len(t) + 1]
    sum.extend([(int(end) - int(start) + 1)])
    sum[:len(t) + 1] = [x / (int(end) - int(start) + 1) for x in sum[:len(t) + 1]]
    return sum;


exon_table = [["gene_name", "transcript", "exon", "average"]]
exon_table[0].extend([str(x) + 'x' for x in args.cov_thresholds])
exon_table[0].extend(["sum"])
exon_table[0].extend([str(x) + 'x_count' for x in args.cov_thresholds])
exon_table[0].extend(["length"])

j = 1
gene_name = ""

for rawline in open(args.GENES, "r"):

    line = rawline.strip().split("\t")
    starts = line[9].split(",")
    ends = line[10].split(",")

    if gene_name == line[12]: j += 1
    else: j = 1
    gene_name = line[12]

    for i in range(int(line[8])):
        exon_table.append([gene_name, j, i + 1] + cov_count(line[2], starts[i], ends[i]))

with open(args.id + "." + args.p + ".full-coverage.txt","w") as out:
    wr = csv.writer(out,delimiter="\t")
    wr.writerows(exon_table)
