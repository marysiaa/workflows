#!/usr/bin/env python3

import os
import locale
import argparse
from typing import List

__version__ = '0.0.1'

locale.setlocale(locale.LC_COLLATE, "C")


def get_sample_names(f_names: List[str], suffixes: List[str]) -> List[str]:
    out_set = set()
    suffixes_sorted_from_shortest = sorted(suffixes, key=lambda x: len(x))
    for name in f_names:
        for suffix in suffixes_sorted_from_shortest:
            if name.endswith(suffix):
                out_set.add(os.path.basename(name).replace(suffix, ''))
                break
    return [x.rstrip('_.') for x in sorted(out_set, key=locale.strxfrm)]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Make pairs (if needed), return sample names')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--fastqs', required=True, nargs='+')
    parser.add_argument('--possible_suffixes', nargs='+',
                        default=['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz'])

    arguments = parser.parse_args()

    possible_suffixes = sorted(arguments.possible_suffixes, key=lambda x: len(x), reverse=True)
    sample_names = get_sample_names(arguments.fastqs, possible_suffixes)
    print('\n'.join(sample_names))