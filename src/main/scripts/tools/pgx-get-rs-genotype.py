#!/usr/bin/env python3

import argparse
import json
import pandas as pd


__version__ = "0.0.3"


def load_tsv(filename: str) -> pd.DataFrame:
    return pd.read_csv(filename, sep='\t')


def explode_df(df: pd.DataFrame) -> pd.DataFrame:
    df = df.assign(**{'RS':df['RS'].str.split(',')})
    return df.explode(['RS'])


def replace_gt(df: pd.DataFrame, sign: str) -> pd.DataFrame:
    df["GT"] = [gt.replace("|", sign).replace("/", sign) if len(gt) == 3 else gt.replace("|", "/") for gt in df["GT"]]
    return df


def filter_df_on_list(df: pd.DataFrame, values: list) -> pd.DataFrame:
    filtered = df.loc[df['RS'].isin(values)]
    return filtered.reset_index(drop=True)


def merge_dfs(df1: pd.DataFrame, df2: pd.DataFrame) -> pd.DataFrame:
    return pd.merge(df1, df2, on=['RS','GT'], how='left')


def drop_columns(df: pd.DataFrame, columns: list) -> pd.DataFrame:
    return df.drop(columns=columns)


def drop_rows(df: pd.DataFrame) -> pd.DataFrame:
    return df[(df.GT != './.') & (df.GT != '.|.')]


def drop_na(df: pd.DataFrame) -> pd.DataFrame:
    return df.dropna()


def drop_duplicates(df: pd.DataFrame) -> pd.DataFrame:
    return df.drop_duplicates(subset=['RS'], keep='last')


def convert_df_to_json(df: pd.DataFrame, index: str, column: str):
    df_to_json = df.set_index(index)[column].to_json()
    return json.loads(df_to_json)


def save_json(rs_to_gt: dict, filename: str):
    with open(filename, 'w') as f:
        json.dump(rs_to_gt, f, indent=2)


def main():
    parser = argparse.ArgumentParser(description='Create JSON for PharmGKB queries from a TSV file')
    parser.add_argument('--rs-from-vcf', type=str, required=True, help='Path to a TSV file with RS IDs from VCF')
    parser.add_argument('--rs-to-filter', type=str, required=True, help='Path to a TSV file with RS IDs to filter')
    parser.add_argument('--query-for-openpgx', action='store_true', default=False)
    parser.add_argument('--output-filename', type=str, required=True, help='Path to output JSON file')
    args = parser.parse_args()

    # Load TSV files
    rs_from_vcf = load_tsv(args.rs_from_vcf)
    rs_to_filter = load_tsv(args.rs_to_filter)

    # Drop rows when ./. or .|.
    rs_from_vcf = drop_rows(rs_from_vcf)

    # Drop duplicates
    rs_from_vcf = drop_duplicates(rs_from_vcf)

    # Explode RS IDs
    rs_from_vcf = explode_df(rs_from_vcf)

    if args.query_for_openpgx:
        # Replace "|" or "/" with "/" for openpgx
        rs_from_vcf = replace_gt(rs_from_vcf, sign="/")
    else:
        # Replace "|" or "/" with "" for pharmgkb
        rs_from_vcf = replace_gt(rs_from_vcf, sign="")

    # Filter RS IDs
    rs_from_vcf = filter_df_on_list(rs_from_vcf, rs_to_filter['RS'].tolist())

    if args.query_for_openpgx:
        # Merge dataframes
        rs_from_vcf = merge_dfs(rs_from_vcf, rs_to_filter)

        # Drop columns
        rs_from_vcf = drop_columns(rs_from_vcf, ["RS", "GT"])

        # Drop NA
        rs_from_vcf = drop_na(rs_from_vcf)

    # Convert to JSON
    column = "Encoding" if args.query_for_openpgx else "GT"
    index = "Gene" if args.query_for_openpgx else "RS"
    rs_to_gt = convert_df_to_json(rs_from_vcf, index=index, column=column)

    # Save JSON
    save_json(rs_to_gt, args.output_filename)


if __name__ == '__main__':
    main()
