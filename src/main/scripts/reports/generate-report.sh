#!/bin/bash
VERSION=1.0.0
DIR=$(dirname "$0")

#!/bin/bash

function help() {
  printf "  \n"
  printf "  NAME\n"
  printf "      generate-report\n"
  printf "  \n"
  printf "  SYNPOSIS\n"
  printf "      generate-report.sh --json name1=JSON_FILE1,name2=JSON_FILE2 --template FILE1\n"
  printf "  \n"
  printf "  DESCRIPTION\n"
  printf "  Creates task directory. Creates test directory. Prepares templates\n"
  printf "  \n"
  printf "      -v|--version                prints script version\n"
  printf "      -h|--help                   prints help\n"
  printf "      -j|--json                   list of json files with their respective names that will be used \n"
  printf "      -t|--template               template file \n"
  printf "      -n|--name                   output file name \n"
  printf "      -o|--output-dir             output directory \n"
  printf "  \n"
}

export -f help

export SRC_DIR="src/main/wdl/tasks/"
export REPOSITORY="intelliseqngs/"

POSITIONAL=()
while [[ $# -gt 0 ]] ;do
    key="$1"

    case $key in
        -v|--version)
        printf "generate-report.sh $VERSION\n"
        exit 0
        ;;
        -h|--help)
        help
        exit 0
        shift # past argument
        ;;
        -j|--json)
        export JSON=$JSON",""$2"
        shift # past argument
        shift # past value
        ;;
        -t|--template)
        export TEMPLATE="$2"
        shift # past argument
        shift # past value
        ;;
        -d|--odt-template)
        export ODT_TEMPLATE="$2"
        shift # past argument
        shift # past value
        ;;
        -n|--name)
        export NAME="$2"
        shift # past argument
        shift # past value
        ;;
        -o|--output-dir)
        export OUTDIR="$2"
        shift # past argument
        shift # past value
        ;;
        -p|--pictures)
        export PICTURES="$2"
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if [ -z "$JSON" ]; then
  help
  printf "  ERROR:\n"
  printf "  Json parameter must be specified:\n"
  printf "    -j|--json \"data1=file1.json --json data2=file2.json\"\n"
  printf "  \n"
  exit
fi
export JSON=$(echo $JSON | sed 's/,$//' | sed 's/^,//')
printf "JSON: $JSON\n"

if [ -z "$TEMPLATE" ]; then
  help
  printf "  ERROR:\n"
  printf "  Template parameter must be specified:\n"
  printf "    -t|--template \"path-to-template-file\"\n"
  printf "  \n"
  exit
fi

if [ -z "$NAME" ]; then
  help
  printf "  ERROR:\n"
  printf "  Name parameter must be specified:\n"
  printf "    -n|--name \"output file name\"\n"
  printf "  \n"
  exit
fi

if [ -z "$ODT_TEMPLATE" ]; then
  ODT_TEMPLATE="$DIR/templates/template.odt"
fi

if [ -z "$OUTDIR" ]; then
  OUTDIR=$(pwd)
fi

  SOURCE="${BASH_SOURCE[0]}"

#set -x
echo "Filling template..."
python $DIR/generate-report.py --json $JSON --template $TEMPLATE > $OUTDIR/content.xml
echo "Copying source odt..."
cp "$ODT_TEMPLATE" "$OUTDIR/$NAME.odt"
echo "Updating odt..."
cd $OUTDIR && zip -u $NAME.odt content.xml
if [ -n "$PICTURES" ]; then
  echo "Updating odt with pictures..."
  echo "Extracting META-INF..."
  cd $OUTDIR && unzip -j $NAME.odt "META-INF/manifest.xml" -d "META-INF/"
  echo "Parsing json with pictures"
  PICTURES_FILES=$(cat $PICTURES | jq .[].path --raw-output)
  cd $OUTDIR && for file in $PICTURES_FILES; do
    mkdir -p Pictures
    cp $file Pictures/$(basename $file)
    zip -u $NAME.odt Pictures/$(basename $file);
    sed -n -i 'p;3a  <manifest:file-entry manifest:full-path="Pictures/'$(basename $file)'" manifest:media-type="image/jpeg"/>' "META-INF/manifest.xml"
  done
  cd $OUTDIR && zip -u $NAME.odt "META-INF/manifest.xml"
fi
echo "Converting to docx..."
libreoffice --headless --convert-to "docx:Office Open XML Text" $OUTDIR/$NAME.odt
echo "Converting to html..."
libreoffice --headless --convert-to "html:HTML:EmbedImages" $OUTDIR/$NAME.odt
echo "Converting to pdf..."
wkhtmltopdf "file://"$OUTDIR/$NAME.html $OUTDIR/$NAME-fromhtml.pdf
libreoffice --headless --convert-to "pdf:writer_pdf_Export" $OUTDIR/$NAME.odt
echo "Finished..."
#set +x




#rm $OUTDIR/content.xml

#libreoffice --headless --convert-to "docx:Office Open XML Text" myfile.html
#zip -u myfile.odt styles.xml
#zip -u myfile.odt styles.xml
#unzip -p myfile2.docx word/document.xml | sed "s/<w:tblStyle w:val=\"TableNormal\"/<w:tblStyle w:val=\"BlueTableStyle\"/g" > word/document.xml
#zip -u myfile2.docx word/document.xml
