#!/usr/bin/python3

import os
import json
import argparse
from typing import List
from typing import Dict
from typing import Union

__version__ = '0.0.1'


def load_mobigen_res(path: str) -> List[Dict]:
    with open(path) as f:
        return json.load(f)


def get_model_name(module_name: str) -> str:
    return module_name.rsplit('.', 1)[-1]


def find_picture_for_model_name(model_name: str, pictute_paths: List[str]) -> str:
    for pictute_path in pictute_paths:
        if model_name in pictute_path:
            return os.path.abspath(pictute_path)


def match_proper_pictures_with_mobigen_results(mobigen_res: List[Dict], picture_paths: List[str]) -> Dict[str, str]:
    ret = {}
    for model in mobigen_res:
        module_name = model['module']
        model_name = get_model_name(module_name)
        ret[module_name] = find_picture_for_model_name(model_name, picture_paths)
    return ret


def write_json(data: Union[Dict, List], path:str):
    with open(path, 'w') as f:
        json.dump(data, f, indent=4)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='jakiś sensowny opis, co robi skrypt')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--mobigen_res', type=str, required=True, help='Path to the mobigen result json file.')
    parser.add_argument('--pictures', nargs='+', required=True, help='Paths to the pictures')
    parser.add_argument('--outfile', type=str, required=True, help="Output path")
    arguments = parser.parse_args()

    mobigen_res = load_mobigen_res(arguments.mobigen_res)
    report_data = match_proper_pictures_with_mobigen_results(mobigen_res, arguments.pictures)
    write_json(report_data, arguments.outfile)

