export function markSafari() {
  const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

  if (isSafari) {
    document.querySelector('html').classList.add('isSafari');
  }
}
