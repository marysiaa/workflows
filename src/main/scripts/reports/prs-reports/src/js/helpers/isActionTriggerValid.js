import { ACTION } from '../enums/data';

export default function isActionTriggerValid(el, triggerName) {
  return el.target.dataset[ACTION] === triggerName;
}
