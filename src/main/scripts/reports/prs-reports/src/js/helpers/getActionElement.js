import { ACTION } from '../enums/data';

export default function getActionElement(name, all, parent) {
  const handler = `[data-${ACTION}=${name}]`;
  const parentEl = parent || document;

  if (all) {
    return parentEl.querySelectorAll(handler);
  }

  return parentEl.querySelector(handler);
}
