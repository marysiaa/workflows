import { initializeExpandableContent } from './components/expandableContent/expandableContent';
import { initializeExpandableGenes } from './components/expandableContent/expandableGenes';
import { initializeTabs } from './components/tabs/tabs';
import { initializeAsideTabs } from './components/tabs/asideTabs';
import { initializeAccordion } from './components/accordion/accordion';
import { initializeExpandableRisks } from './components/risksScrollable/expandAllRisks';
import { initializeFiltering } from './components/risksScrollable/filters';
import { initializeRisksScrolling } from './components/risksScrollable/scrollRisks';
import { markSafari } from './helpers/isSafari';

function onLoad() {
  initializeExpandableGenes();
  initializeExpandableContent();
  initializeExpandableRisks();
  initializeTabs();
  initializeAsideTabs();
  initializeAccordion();
  initializeFiltering();
  initializeRisksScrolling();
  markSafari();
}

window.addEventListener('load', onLoad);
