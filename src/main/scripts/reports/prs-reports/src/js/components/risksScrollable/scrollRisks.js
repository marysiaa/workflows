import {
  RISKS_SCROLLER_TRIGGER,
  SCROLLER_DOT,
  RISK_SCROLLER_CLASS_NAME,
  RISK_SCROLLABLE_CONTAINER_CLASS_NAME,
  RISK_BOX_CLASS_NAME,
} from './enums';
import getActionElement from '../../helpers/getActionElement';
import isActionTriggerValid from '../../helpers/isActionTriggerValid';
import getTargetElement from '../../helpers/getTargetElement';

const scrollerTrigger = getActionElement(RISKS_SCROLLER_TRIGGER, true);
const boxesPerSlide = 3;
const boxesMinCount = 3;
const gap = 30;
const simpleBoxWidth = 269;
const slideSize = boxesPerSlide * (simpleBoxWidth + gap);
let scrollContainerWidth;

function setFullScrollWidthData() {
  document.querySelectorAll(`.${RISK_SCROLLER_CLASS_NAME}`).forEach((container) => {
    const containerEl = container;
    containerEl.dataset.fullScrollWidth = containerEl.offsetWidth;
  });
}

function getFullScrollWidth(container) {
  return container.dataset.fullScrollWidth;
}

function scrollRisks(e) {
  e.preventDefault();

  if (isActionTriggerValid(e, SCROLLER_DOT)) {
    const dotEl = e.target;
    const fullScroll = document.getElementById(getTargetElement(dotEl));
    const fullScrollWidth = getFullScrollWidth(fullScroll);
    const { slideId } = dotEl.dataset;
    const newOffset = slideId * slideSize;
    const newOffsetForLast = newOffset - (scrollContainerWidth - (fullScrollWidth - newOffset) - 100); // eslint-disable-line max-len
    // 100 is main container left padding

    if (fullScrollWidth - newOffset < scrollContainerWidth) {
      fullScroll.style.transform = `translateX(-${newOffsetForLast}px)`;
    } else {
      fullScroll.style.transform = `translateX(-${newOffset}px)`;
    }

    fullScroll.closest(`.${RISK_SCROLLABLE_CONTAINER_CLASS_NAME}`).querySelector('.dots__link--isActive').classList.remove('dots__link--isActive');
    dotEl.classList.add('dots__link--isActive');
  }
}

function initializeVariables() {
  scrollContainerWidth = document.querySelector('.tab').offsetWidth;
  setFullScrollWidthData();
}

export function refreshInitialVariables() {
  setFullScrollWidthData();
}

function prepareDot(dotIndex, target) {
  const li = document.createElement('li');
  const a = document.createElement('a');

  li.classList.add('dots__dot');
  a.href = `#${target}`;
  a.dataset.action = SCROLLER_DOT;
  a.dataset.slideId = dotIndex;
  a.classList.add('dots__link');

  if (dotIndex === 0) {
    a.classList.add('dots__link--isActive');
  }

  li.appendChild(a);

  return li;
}

function prepareDots() {
  document.querySelectorAll(`.${RISK_SCROLLER_CLASS_NAME}`).forEach((container) => {
    const slidesCount = Math.ceil(getFullScrollWidth(container) / slideSize);
    const dotsContainer = container.closest(`.${RISK_SCROLLABLE_CONTAINER_CLASS_NAME}`).querySelector('.dots');

    dotsContainer.textContent = '';

    for (let i = 0; i < slidesCount; i += 1) {
      dotsContainer.appendChild(prepareDot(i, container.id));
    }
  });
}

function cleanSlidesOffset() {
  document.querySelectorAll(`.${RISK_SCROLLER_CLASS_NAME}`).forEach((container) => {
    const containerEl = container;

    containerEl.style.transform = 'translateX(0px)';
  });
}

export function refreshDots() {
  document.querySelectorAll('.dots').forEach((dotsContainer) => {
    const dotsContainerEl = dotsContainer;

    dotsContainerEl.textContent = '';
  });
  prepareDots();
}

function addEventListeners() {
  scrollerTrigger.forEach((trigger) => trigger.addEventListener('click', scrollRisks));
  window.addEventListener('resize', cleanSlidesOffset);
  window.addEventListener('resize', refreshInitialVariables);
  window.addEventListener('resize', prepareDots);
}

function showScrollingHandlers() {
  document.querySelectorAll(`.${RISK_SCROLLER_CLASS_NAME}`).forEach((container) => {
    const boxesCount = container.querySelectorAll(`.${RISK_BOX_CLASS_NAME}`).length;

    if (boxesCount > boxesMinCount) {
      container.closest(`.${RISK_SCROLLABLE_CONTAINER_CLASS_NAME}`)
        .classList.remove(`${RISK_SCROLLABLE_CONTAINER_CLASS_NAME}--isInactive`);
    }
  });
}

export function initializeRisksScrolling() {
  if (scrollerTrigger.length > 0) {
    showScrollingHandlers();
    initializeVariables();
    prepareDots();
    addEventListeners();
  }
}
