import { IS_ACTIVE } from '../../enums/states';
import {
  ACCORDION_CONTAINER,
  ACCORDION_ITEM_CLASS_NAME,
  ACCORDION_TRIGGER,
  ARROW_DOWN,
  ARROW_UP,
  ARROW_DOWN_SOLID,
  ARROW_UP_SOLID,
} from './enums';
import isActionTriggerValid from '../../helpers/isActionTriggerValid';
import getActionElement from '../../helpers/getActionElement';

const activeAccordionSectionClassName = `${ACCORDION_ITEM_CLASS_NAME}--${IS_ACTIVE}`;
const accordionContainer = getActionElement(ACCORDION_CONTAINER);

function hideAccordionSection() {
  document.querySelectorAll(`.${activeAccordionSectionClassName}`).forEach((el) => {
    el.classList.remove(activeAccordionSectionClassName);
  });
}

function showAccordionSection(sectionEL) {
  sectionEL.closest(`.${ACCORDION_ITEM_CLASS_NAME}`).classList.add(activeAccordionSectionClassName);
}

function toggleIcon(e) {
  const el = e.target;

  if (el.classList.contains(ARROW_DOWN)) {
    document.querySelector(`.${ACCORDION_ITEM_CLASS_NAME} .${ARROW_UP}`)
      .classList.replace(ARROW_UP, ARROW_DOWN);
    el.classList.replace(ARROW_DOWN, ARROW_UP);
  } else {
    document.querySelector(`.${ACCORDION_ITEM_CLASS_NAME} .${ARROW_UP_SOLID}`)
      .classList.replace(ARROW_UP_SOLID, ARROW_DOWN_SOLID);
    el.classList.replace(ARROW_DOWN_SOLID, ARROW_UP_SOLID);
  }
}

function toggleAccordion(e) {
  if (isActionTriggerValid(e, ACCORDION_TRIGGER)) {
    e.preventDefault();

    toggleIcon(e);
    hideAccordionSection();
    showAccordionSection(e.target);
  }
}

function addEventListeners() {
  accordionContainer.addEventListener('click', toggleAccordion);
}

export function initializeAccordion() {
  if (accordionContainer) {
    addEventListeners();
  }
}
