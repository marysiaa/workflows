import getActionElement from '../../helpers/getActionElement';
import {
  ASIDE_TABS_CONTAINER,
  ASIDE_TAB_TRIGGER,
  ASIDE_TAB_CLASS_NAME,
  ASIDE_TAB_CONTENT_CLASS_NAME,
} from './enums';
import { IS_ACTIVE, IS_VISIBLE } from '../../enums/states';
import isActionTriggerValid from '../../helpers/isActionTriggerValid';
import getTargetElement from '../../helpers/getTargetElement';

const asideTabsContainer = getActionElement(ASIDE_TABS_CONTAINER, true);
const activeAsideTabClassName = `${ASIDE_TAB_CLASS_NAME}--${IS_ACTIVE}`;
const visibleAsideTabContentClassName = `${ASIDE_TAB_CONTENT_CLASS_NAME}--${IS_VISIBLE}`;

function hideAsideTab(tabGroupId) {
  const tabGroup = document.getElementById(tabGroupId);

  tabGroup.querySelectorAll(`.${activeAsideTabClassName}`).forEach((el) => {
    el.classList.remove(activeAsideTabClassName);
  });

  tabGroup
    .querySelectorAll(`.${visibleAsideTabContentClassName}`)
    .forEach((el) => {
      el.classList.remove(visibleAsideTabContentClassName);
    });
}

function showAsideTab(tabId) {
  document
    .querySelector(`[data-tab="${tabId}"]`)
    .classList.add(activeAsideTabClassName);
  document.getElementById(tabId).classList.add(visibleAsideTabContentClassName);
}

function toggleAsideTabs(e) {
  if (isActionTriggerValid(e, ASIDE_TAB_TRIGGER)) {
    e.preventDefault();

    const tabId = getTargetElement(e.target);
    const tabGroupId = e.target.dataset.tabGroup;

    hideAsideTab(tabGroupId);
    showAsideTab(tabId);
  }
}

export function initializeAsideTabs() {
  if (
    Object.prototype.isPrototypeOf.call(NodeList.prototype, asideTabsContainer)
  ) {
    asideTabsContainer.forEach((tab) => tab.addEventListener('click', toggleAsideTabs));
  } else if (asideTabsContainer) {
    asideTabsContainer.addEventListener('click', toggleAsideTabs);
  }
}
