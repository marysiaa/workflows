import {
  FILTERS_TRIGGER,
  FILTER_TRIGGER,
  FILTER_DATA,
  EXPAND_BLOCKS_TRIGGER,
  RISK_SCROLLABLE_CONTAINER_CLASS_NAME,
} from './enums';
import { IS_HIDDEN } from '../../enums/states';
import { LINK_OFF_CLASS_NAME } from '../../enums/elements';
import { COUNT } from '../../enums/data';
import { refreshInitialVariables, refreshDots } from './scrollRisks';
import { setDefaultState } from './utils';
import getActionElement from '../../helpers/getActionElement';
import getTargetElement from '../../helpers/getTargetElement';
import isActionTriggerValid from '../../helpers/isActionTriggerValid';

const filterTrigger = getActionElement(FILTERS_TRIGGER, true);

function toggleBoxesByFilter(e) {
  const filterLink = e.target;
  const filterType = filterLink.dataset[FILTER_DATA];
  const boxesContainer = document.getElementById(getTargetElement(filterLink));

  setDefaultState(boxesContainer);

  boxesContainer.querySelectorAll(`[data-${FILTER_DATA}=${filterType}]`).forEach((el) => {
    const boxEl = el;

    if (!boxEl.classList.contains(IS_HIDDEN)) {
      boxEl.classList.add(IS_HIDDEN);
    } else {
      boxEl.classList.remove(IS_HIDDEN);
    }
  });

  refreshInitialVariables();
  refreshDots();
}

function checkDoesListIsEmpty(parent) {
  const container = parent;
  const showAllLink = getActionElement(EXPAND_BLOCKS_TRIGGER, false, container);

  if (parseInt(showAllLink.dataset[COUNT], 10) === 0) {
    container.classList.add('risksScrollable--isEmpty');
  } else {
    container.classList.remove('risksScrollable--isEmpty');
  }
}

function toggleFilterLink(e) {
  const filterLink = e.target;
  const filterCount = parseInt(filterLink.dataset[COUNT], 10);
  const showAllLink = getActionElement(EXPAND_BLOCKS_TRIGGER, false, filterLink.closest(`.${RISK_SCROLLABLE_CONTAINER_CLASS_NAME}`));
  const currentAllCount = parseInt(showAllLink.dataset[COUNT], 10);

  if (filterLink.classList.contains(LINK_OFF_CLASS_NAME)) {
    filterLink.classList.remove(LINK_OFF_CLASS_NAME);
    showAllLink.dataset[COUNT] = currentAllCount + filterCount;
  } else {
    filterLink.classList.add(LINK_OFF_CLASS_NAME);
    showAllLink.dataset[COUNT] = currentAllCount - filterCount;
  }

  checkDoesListIsEmpty(filterLink.closest('.risksScrollable'));
}

function updateVisibleBoxes(container) {
  const boxesContainer = container;
  const DISPLAY_LIMIT = 3;

  boxesContainer.querySelectorAll('.overcount').forEach((el) => {
    el.classList.remove('overcount');
  });

  boxesContainer.querySelectorAll('.riskBox:not(.isHidden)').forEach((el, index) => {
    if (index + 1 > DISPLAY_LIMIT) {
      el.classList.add('overcount');
    }
  });
}

function filterBoxes(e) {
  e.preventDefault();
  const filterLink = e.target;
  const boxesContainer = document.getElementById(getTargetElement(filterLink));

  if (isActionTriggerValid(e, FILTER_TRIGGER)) {
    toggleBoxesByFilter(e);
    toggleFilterLink(e);
    updateVisibleBoxes(boxesContainer);
  }
}

function addEventListeners() {
  filterTrigger.forEach((trigger) => trigger.addEventListener('click', filterBoxes));
}

export function initializeFiltering() {
  if (filterTrigger.length > 0) {
    document.querySelectorAll('.riskScroller').forEach((container) => {
      updateVisibleBoxes(container);
    });
    addEventListeners();
  }
}
