export const TABS_CONTAINER = 'tabs';
export const TAB_TRIGGER = 'tabTrigger';
export const TAB_PREV_TRIGGER = 'tabPrev';
export const TAB_NEXT_TRIGGER = 'tabNext';
export const LINK_TAB_TRIGGER = 'linkTabTrigger';
export const SUBTAB_TRIGGER = 'subTabTrigger';
export const LINK_SUBTAB_TRIGGER = 'linkSubTabTrigger';

export const TABS_WRAPPER_CLASS_NAME = 'tabs__container';
export const TABS_SCROLLERS_CONTAINER_CLASS_NAME = 'tabs__scrollers';
export const TAB_CLASS_NAME = 'tabs__tab';
export const TAB_CONTENT_CLASS_NAME = 'tab';
export const TABS_CONTAINER_CLASS_NAME = 'tabsContent';
export const SUBTAB_CLASS_NAME = 'subtabMenu__item';
export const SUBTAB_CONTENT_CLASS_NAME = 'subTab';
export const INACTIVE_SCROLL_TRIGGER_CLASS_NAME = 'tabs__scroller--isInactive';

export const ASIDE_TABS_CONTAINER = 'asideTabs';
export const ASIDE_TAB_TRIGGER = 'asideTabTrigger';

export const ASIDE_TAB_CLASS_NAME = 'asideTabs__tab';
export const ASIDE_TAB_CONTENT_CLASS_NAME = 'asideTab';
