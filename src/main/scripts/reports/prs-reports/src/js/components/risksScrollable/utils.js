export function setDefaultState(target) {
  const el = target;

  el.style.transform = 'translateX(0px)';

  if (el.querySelector('.dots__link')) {
    el.querySelector('.dots__link--isActive').classList.remove('dots__link--isActive');
    el.querySelectorAll('.dots__link')[0].classList.add('dots__link--isActive');
  }
}
