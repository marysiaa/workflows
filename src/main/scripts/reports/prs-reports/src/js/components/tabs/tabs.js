import { IS_ACTIVE, IS_VISIBLE, IS_HIDDEN } from '../../enums/states';
import {
  ACTION,
  EXTRA_ACTION,
  EXTRA_ACTION_CAMELCASE,
  EXTRA_ACTION_CLASS_CAMELCASE,
} from '../../enums/data';
import {
  LINK_TAB_TRIGGER,
  LINK_SUBTAB_TRIGGER,
  TAB_TRIGGER,
  TABS_CONTAINER,
  TAB_CLASS_NAME,
  TAB_CONTENT_CLASS_NAME,
  TABS_CONTAINER_CLASS_NAME,
  TAB_PREV_TRIGGER,
  TAB_NEXT_TRIGGER,
  SUBTAB_TRIGGER,
  SUBTAB_CLASS_NAME,
  SUBTAB_CONTENT_CLASS_NAME,
  TABS_WRAPPER_CLASS_NAME,
  TABS_SCROLLERS_CONTAINER_CLASS_NAME,
  INACTIVE_SCROLL_TRIGGER_CLASS_NAME,
} from './enums';
import getActionElement from '../../helpers/getActionElement';
import getTargetElement from '../../helpers/getTargetElement';
import isActionTriggerValid from '../../helpers/isActionTriggerValid';

const tabsContainer = getActionElement(TABS_CONTAINER);
const tabsWrapper = document.querySelector(`.${TABS_WRAPPER_CLASS_NAME}`);
const activeTabClassName = `${TAB_CLASS_NAME}--${IS_ACTIVE}`;
const visibleTabContentClassName = `${TAB_CONTENT_CLASS_NAME}--${IS_VISIBLE}`;
const activeSubTabClassName = `${SUBTAB_CLASS_NAME}--${IS_ACTIVE}`;
const visibleSubTabContentClassName = `${SUBTAB_CONTENT_CLASS_NAME}--${IS_VISIBLE}`;
const scrollersContainerEl = document.querySelector(
  `.${TABS_SCROLLERS_CONTAINER_CLASS_NAME}`,
);
const prevTrigger = getActionElement(TAB_PREV_TRIGGER);
const nextTrigger = getActionElement(TAB_NEXT_TRIGGER);
let tabsWidth;
let tabsContainerWidth;
let scrollersWidth;

function hideTab(tabType, parentTab) {
  const tabsContainerEl = document.querySelector(`[data-tab='${parentTab}']`)
    || document.querySelector(`.${TABS_CONTAINER_CLASS_NAME}`);
  const tabsContentContainerEl = document.querySelector(
    `.${TABS_CONTAINER_CLASS_NAME}`,
  );
  const tabEl = tabType === SUBTAB_TRIGGER ? activeSubTabClassName : activeTabClassName;
  const tabContent = tabType === SUBTAB_TRIGGER
    ? visibleSubTabContentClassName
    : visibleTabContentClassName;

  tabsContainerEl.querySelectorAll(`.${tabEl}`).forEach((el) => {
    el.classList.remove(tabEl);
  });

  if (tabType === SUBTAB_TRIGGER) {
    tabsContentContainerEl
      .querySelectorAll(`.${tabContent}[data-tab-group='${parentTab}']`)
      .forEach((el) => {
        el.classList.remove(tabContent);
      });
  } else {
    tabsContentContainerEl.querySelectorAll(`.${tabContent}`).forEach((el) => {
      el.classList.remove(tabContent);
    });
  }
}

function showTab(tabId, tabType) {
  const tabEl = tabType === SUBTAB_TRIGGER ? activeSubTabClassName : activeTabClassName;
  const tabContent = tabType === SUBTAB_TRIGGER
    ? visibleSubTabContentClassName
    : visibleTabContentClassName;

  document.querySelector(`[data-tab='${tabId}']`).classList.add(tabEl);
  document.getElementById(tabId).classList.add(tabContent);
}

function toggleExtras(relatedTab) {
  const toggleMark = 'toggleTab:';
  const extraActionElements = document.querySelectorAll(
    `[data-${EXTRA_ACTION}^='${toggleMark}']`,
  );

  extraActionElements.forEach((el) => {
    const relatedTabContent = el.dataset[EXTRA_ACTION_CAMELCASE].split(':').pop();
    const className = el.dataset[EXTRA_ACTION_CLASS_CAMELCASE]
      ? el.dataset[EXTRA_ACTION_CLASS_CAMELCASE]
      : IS_HIDDEN;

    if (relatedTabContent === relatedTab) {
      el.classList.remove(className);
    } else {
      el.classList.add(className);
    }
  });
}

function toggleTab(e, tabId, tabType, parentTab) {
  e.preventDefault();

  window.scrollTo(0, 0);
  hideTab(tabType, parentTab);
  showTab(tabId, tabType);
  toggleExtras(tabId);
}

function toggleTabByTab(e) {
  const targetEl = getTargetElement(e.target);
  const parentEl = e.target.closest('.tabs__tab[data-tab]').dataset.tab;

  if (isActionTriggerValid(e, TAB_TRIGGER)) {
    toggleTab(e, targetEl, TAB_TRIGGER);
  } else if (isActionTriggerValid(e, SUBTAB_TRIGGER)) {
    toggleTab(e, targetEl, SUBTAB_TRIGGER, parentEl);
  }
}

function getTabsContainerWidth() {
  tabsContainerWidth = tabsContainer.offsetWidth;
}

function getTabsWidth() {
  tabsWidth = tabsWrapper.offsetWidth;
}

function markHiddenTabs() {
  const visibleTabsArea = tabsContainerWidth - scrollersWidth;

  document.querySelectorAll(`.${TAB_CLASS_NAME}`).forEach((el) => {
    const tabEl = el;
    const elRect = tabEl.getBoundingClientRect();

    tabEl.dataset.notVisible = elRect.left < 0 || elRect.right >= visibleTabsArea;
  });
}

function cleanScrollerOffset() {
  tabsWrapper.style.marginLeft = '';
  prevTrigger.classList.add(INACTIVE_SCROLL_TRIGGER_CLASS_NAME);
  nextTrigger.classList.remove(INACTIVE_SCROLL_TRIGGER_CLASS_NAME);
}

function setVisibleTabs() {
  getTabsContainerWidth();

  if (tabsWidth > tabsContainerWidth) {
    scrollersContainerEl.classList.add('tabs__scrollers--isVisible');
    scrollersWidth = scrollersContainerEl.offsetWidth;
  } else {
    scrollersContainerEl.classList.remove('tabs__scrollers--isVisible');
    cleanScrollerOffset();
  }

  markHiddenTabs();
}

function selectPrevVisibleOnScrollTab() {
  const visibleTabs = document.querySelectorAll("[data-not-visible='false']");
  const prevVisibleTabIndex = parseInt(visibleTabs[0].dataset.tabId, 10) - 1;
  const lastVisibleTab = visibleTabs[visibleTabs.length - 1];
  const prevVisibleTab = document.querySelector(
    `.${TAB_CLASS_NAME}[data-tab-id='${prevVisibleTabIndex}']`,
  );

  if (prevVisibleTab) {
    const lastRect = prevVisibleTab.getBoundingClientRect();
    const tabsWrapperRect = tabsWrapper.getBoundingClientRect();

    if (prevVisibleTabIndex === 1) {
      prevTrigger.classList.add(INACTIVE_SCROLL_TRIGGER_CLASS_NAME);
    }

    nextTrigger.classList.remove(INACTIVE_SCROLL_TRIGGER_CLASS_NAME);
    tabsWrapper.style.marginLeft = `${tabsWrapperRect.left + lastRect.width}px`;
    lastVisibleTab.dataset.notVisible = 'true';
    prevVisibleTab.dataset.notVisible = 'false';
  }
}

function selectNextVisibleOnScrollTab() {
  const visibleTabs = document.querySelectorAll("[data-not-visible='false']");
  const nextVisibleTabIndex = parseInt(visibleTabs[visibleTabs.length - 1].dataset.tabId, 10) + 1;
  const firstVisibleTab = visibleTabs[0];
  const nextVisibleTab = document.querySelector(
    `.${TAB_CLASS_NAME}[data-tab-id='${nextVisibleTabIndex}']`,
  );

  if (nextVisibleTab) {
    const firstRect = firstVisibleTab.getBoundingClientRect();
    const tabsWrapperRect = tabsWrapper.getBoundingClientRect();
    const tabsLength = document.querySelectorAll('.tabs__tab').length;

    if (nextVisibleTabIndex === tabsLength) {
      nextTrigger.classList.add(INACTIVE_SCROLL_TRIGGER_CLASS_NAME);
    }

    prevTrigger.classList.remove(INACTIVE_SCROLL_TRIGGER_CLASS_NAME);
    tabsWrapper.style.marginLeft = `${
      tabsWrapperRect.left - firstRect.width
    }px`;
    firstVisibleTab.dataset.notVisible = 'true';
    nextVisibleTab.dataset.notVisible = 'false';
  }
}

function scrollTabs(e) {
  if (isActionTriggerValid(e, TAB_PREV_TRIGGER)) {
    selectPrevVisibleOnScrollTab(e);
  }
  if (isActionTriggerValid(e, TAB_NEXT_TRIGGER)) {
    selectNextVisibleOnScrollTab(e);
  }
}

function setIndexForTabTriggers() {
  document.querySelectorAll(`.${TAB_CLASS_NAME}`).forEach((el, index) => {
    const item = el;
    item.dataset.tabId = `${index + 1}`;
  });
}

function toggleTabByLink(e) {
  const link = e.target.closest(`[data-${ACTION}='${LINK_TAB_TRIGGER}']`);
  const tabId = getTargetElement(link);

  toggleTab(e, tabId);
}

function toggleSubtabTabByLink(e) {
  const link = e.target.closest(`[data-${ACTION}='${LINK_SUBTAB_TRIGGER}']`);
  const targets = getTargetElement(link).split(':');
  const tabId = targets[0];
  const subtabId = targets[1];

  toggleTab(e, tabId, TAB_TRIGGER);
  toggleTab(e, subtabId, SUBTAB_TRIGGER, tabId);
}

function addEventListeners() {
  getActionElement(LINK_TAB_TRIGGER, true).forEach((el) => {
    el.addEventListener('click', toggleTabByLink);
  });
  getActionElement(LINK_SUBTAB_TRIGGER, true).forEach((el) => {
    el.addEventListener('click', toggleSubtabTabByLink);
  });
  window.addEventListener('resize', setVisibleTabs);
  window.addEventListener('resize', cleanScrollerOffset);
  tabsContainer.addEventListener('click', toggleTabByTab);
  tabsContainer.addEventListener('click', scrollTabs);
}

export function initializeTabs() {
  if (tabsContainer) {
    getTabsWidth();
    setVisibleTabs();
    cleanScrollerOffset();
    setIndexForTabTriggers();
    addEventListeners();
  }
}
