import { EXPAND_GENES_TRIGGER, GENES_CONTENT_CLASS_NAME, SHOW } from './enums';
import getActionElement from '../../helpers/getActionElement';
import getTargetElement from '../../helpers/getTargetElement';

const genesExpander = getActionElement(EXPAND_GENES_TRIGGER, true);

function toggleContent(target) {
  if (target.classList.contains(GENES_CONTENT_CLASS_NAME)) {
    target.classList.remove(GENES_CONTENT_CLASS_NAME);
  } else {
    target.classList.add(GENES_CONTENT_CLASS_NAME);
  }
}

function toggleLink(trigger) {
  const triggerEl = trigger;

  if (triggerEl.classList.contains(SHOW.MORE_CLASS_NAME)) {
    triggerEl.classList.remove(SHOW.MORE_CLASS_NAME);
    triggerEl.classList.add(SHOW.LESS_CLASS_NAME);
    triggerEl.textContent = SHOW.LESS_LABEL;
  } else {
    triggerEl.classList.remove(SHOW.LESS_CLASS_NAME);
    triggerEl.classList.add(SHOW.MORE_CLASS_NAME);
    triggerEl.textContent = SHOW.MORE_LABEL;
  }
}

function toggleGenesList(e) {
  e.preventDefault();

  const trigger = e.target;
  const target = document.getElementById(getTargetElement(trigger));

  toggleContent(target);
  toggleLink(trigger);
}

function addEventListeners() {
  genesExpander.forEach((genes) => genes.addEventListener('click', toggleGenesList));
}

export function initializeExpandableGenes() {
  if (genesExpander) {
    addEventListeners();
  }
}
