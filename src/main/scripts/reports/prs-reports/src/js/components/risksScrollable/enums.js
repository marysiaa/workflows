export const FILTERS_TRIGGER = 'filters';
export const FILTER_TRIGGER = 'filtersCase';
export const EXPAND_BLOCKS_TRIGGER = 'expandBlocks';
export const RISKS_SCROLLER_TRIGGER = 'risksScroll';
export const SCROLLER_DOT = 'dot';

export const FILTER_DATA = 'filter';

export const RISK_BOX_CLASS_NAME = 'riskBox';
export const RISK_SCROLLER_CLASS_NAME = 'riskScroller';
export const EXPANDED_RISKS_CLASS_NAME = 'riskScroller--isExpanded';
export const RISK_SCROLLABLE_CONTAINER_CLASS_NAME = 'risksScrollable';
