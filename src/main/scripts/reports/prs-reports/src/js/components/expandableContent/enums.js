export const EXPAND_CONTENT = 'expandContent';
export const EXPAND_CONTENT_BLOCK = 'expandContentBlock';
export const EXPAND_GENES_TRIGGER = 'expandGenes';

export const SHOW = {
  MORE_CLASS_NAME: 'icon-arrowDownSolid',
  LESS_CLASS_NAME: 'icon-arrowUpSolid',
  MORE_LABEL: 'Show more',
  LESS_LABEL: 'Show less',
};

export const READ = {
  MORE_CLASS_NAME: 'icon-arrowDown',
  LESS_CLASS_NAME: 'icon-arrowUp',
  MORE_LABEL: 'Read more',
  LESS_LABEL: 'Read less',
};

export const EXPANDED_CONTENT_CLASS_NAME = 'expandableContent--isExpanded';
export const GENES_CONTENT_CLASS_NAME = 'genesBox--isExpanded';
