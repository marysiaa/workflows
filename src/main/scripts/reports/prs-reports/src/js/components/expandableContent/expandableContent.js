import {
  EXPAND_CONTENT,
  EXPAND_CONTENT_BLOCK,
  EXPANDED_CONTENT_CLASS_NAME,
  SHOW,
  READ,
} from './enums';
import { ACTION } from '../../enums/data';
import getActionElement from '../../helpers/getActionElement';
import isActionTriggerValid from '../../helpers/isActionTriggerValid';

const textExpanders = getActionElement(EXPAND_CONTENT, true);

function toggleLink(trigger, type) {
  const triggerEl = trigger;
  const expandableType = type;

  if (triggerEl.classList.contains(expandableType.MORE_CLASS_NAME)) {
    triggerEl.classList.remove(expandableType.MORE_CLASS_NAME);
    triggerEl.classList.add(expandableType.LESS_CLASS_NAME);
    triggerEl.textContent = expandableType.LESS_LABEL;
  } else {
    triggerEl.classList.remove(expandableType.LESS_CLASS_NAME);
    triggerEl.classList.add(expandableType.MORE_CLASS_NAME);
    triggerEl.textContent = expandableType.MORE_LABEL;
  }
}

function handleTextContentExpand(e) {
  if (isActionTriggerValid(e, EXPAND_CONTENT)) {
    e.preventDefault();

    const trigger = e.target;
    const targetBlock = trigger.closest(`[data-${ACTION}="${EXPAND_CONTENT_BLOCK}"]`);
    const triggerLabel = trigger.textContent;
    const type = triggerLabel.toLowerCase().includes('show') ? SHOW : READ;

    if (targetBlock.classList.contains(EXPANDED_CONTENT_CLASS_NAME)) {
      targetBlock.classList.remove(EXPANDED_CONTENT_CLASS_NAME);
    } else {
      targetBlock.classList.add(EXPANDED_CONTENT_CLASS_NAME);
    }

    toggleLink(trigger, type);
  }
}

function addEventListeners() {
  document.addEventListener('click', handleTextContentExpand);
}

export function initializeExpandableContent() {
  if (textExpanders.length > 0) {
    addEventListeners();
  }
}
