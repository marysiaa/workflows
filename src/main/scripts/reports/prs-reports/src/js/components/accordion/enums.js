export const ACCORDION_CONTAINER = 'accordion';
export const ACCORDION_TRIGGER = 'accordionTrigger';

export const ACCORDION_ITEM_CLASS_NAME = 'accordion__section';
export const ARROW_UP_SOLID = 'icon-arrowUpSolid';
export const ARROW_DOWN_SOLID = 'icon-arrowDownSolid';
export const ARROW_UP = 'icon-arrowUp';
export const ARROW_DOWN = 'icon-arrowDown';
