import {
  EXPAND_BLOCKS_TRIGGER,
  EXPANDED_RISKS_CLASS_NAME,
} from './enums';
import { IS_INVISIBLE } from '../../enums/states';
import { EXTRA_ACTION } from '../../enums/data';
import { setDefaultState } from './utils';
import getActionElement from '../../helpers/getActionElement';
import getTargetElement from '../../helpers/getTargetElement';

const blockExpander = getActionElement(EXPAND_BLOCKS_TRIGGER, true);

function toggleContent(target) {
  const targetEl = target;
  const extraEl = document.querySelector(`[data-${EXTRA_ACTION}="toggle:${targetEl.id}"]`);

  setDefaultState(targetEl);

  if (targetEl.classList.contains(EXPANDED_RISKS_CLASS_NAME)) {
    targetEl.classList.remove(EXPANDED_RISKS_CLASS_NAME);
    extraEl.classList.remove(IS_INVISIBLE);
  } else {
    targetEl.classList.add(EXPANDED_RISKS_CLASS_NAME);
    extraEl.classList.add(IS_INVISIBLE);
  }
}

function toggleLink(trigger) {
  const triggerEl = trigger;
  const contentContainer = document.getElementById(getTargetElement(triggerEl));

  if (contentContainer.classList.contains(EXPANDED_RISKS_CLASS_NAME)) {
    triggerEl.textContent = 'Show less';
  } else {
    triggerEl.textContent = 'Show all';
  }
}

function expandRisks(e) {
  e.preventDefault();

  const trigger = e.target;
  const target = document.getElementById(getTargetElement(trigger));

  toggleContent(target);
  toggleLink(trigger);
}

function addEventListeners() {
  blockExpander.forEach((block) => block.addEventListener('click', expandRisks));
}

export function initializeExpandableRisks() {
  if (blockExpander.length > 0) {
    addEventListeners();
  }
}
