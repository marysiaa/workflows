export const ACTION = 'action';
export const EXTRA_ACTION = 'action-extra';
export const EXTRA_ACTION_CAMELCASE = 'actionExtra';
export const EXTRA_ACTION_CLASS_CAMELCASE = 'actionExtraClass';
export const COUNT = 'count';
