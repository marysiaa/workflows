export const IS_ACTIVE = 'isActive';
export const IS_VISIBLE = 'isVisible';
export const IS_HIDDEN = 'isHidden';
export const IS_INVISIBLE = 'isInvisible';
