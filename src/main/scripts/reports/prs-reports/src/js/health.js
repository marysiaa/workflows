import { initializeExpandableContent } from './components/expandableContent/expandableContent';
import { initializeExpandableGenes } from './components/expandableContent/expandableGenes';
import { initializeTabs } from './components/tabs/tabs';
import { initializeAsideTabs } from './components/tabs/asideTabs';
import { initializeAccordion } from './components/accordion/accordion';

function onLoad() {
  initializeExpandableContent();
  initializeExpandableGenes();
  initializeTabs();
  initializeAsideTabs();
  initializeAccordion();
}

window.addEventListener('load', onLoad);
