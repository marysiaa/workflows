# Polygenic Risk Score - reports

## Tool to generate wellness/health HTML report from JSON file.

-----

### How to add new model

1. Find specific model on GBE, PGS or Biobank UK websites.
2. Add info about model [here](https://docs.google.com/spreadsheets/d/1udFOZJOGFp4Bxew2eYzMYf2EudPoBM-QkPgFaGRqkxA/edit#gid=0).
3. Create a sheet on [Google Drive](https://drive.google.com/drive/u/1/folders/1Ew2MNRnr0AH_ympsropaMtlAWz7sAu9b) for the model in the health or wellness directory based on [example](https://docs.google.com/spreadsheets/d/19qpLmoCYn3riPoNbhHGxbgt00mzoJdmE03nb-k9rSgI/edit#gid=1486435868).
4. Create a raw model using the examples for [BMI](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/wellness-intelliseq#body-mass-index-bmi) and [CAD](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/health-intelliseq#coronary-artery-disease-cad) (comment `## create raw model`).
5. Add raw model to [health](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/health-intelliseq/raw-models) or [wellness](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/wellness-intelliseq/raw-models) directory.
6. Add description to raw model from created sheet in step 3 using the examples for [BMI](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/wellness-intelliseq#body-mass-index-bmi) and [CAD](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/health-intelliseq#coronary-artery-disease-cad) (comment `## add description`).
7. Add prepared model to [health](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/health-intelliseq) or [wellness](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/wellness-intelliseq) directory.
8. Update readme in [health](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/health-intelliseq/readme.md) or [wellness](https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/models/wellness-intelliseq/readme.md) directory.
9. Update `task_prs-models` docker and `prs-models` wdl. 
10. Update `prs-models` wdl version in `prs` pipeline.

-----
### How to create report

1. Create JSON from `yml` files using `mobigen-models` wdl by adding models to input `models` in `src/test/wdl/tasks/mobigen-models/test.json` file.

2. Merge all models in JSON format to one file using `all_models_to_one_json.py` in `src/main/scripts/tools` directory (you can use test for script).

3. Install required dependencies and run server:
```
npm install - install required dependencies 
npm run build - build public folder with css, js and html files 
npm run watch - watch file changes and run server
```

4. Type `localhost:8080` in browser to see reports.

```
http://localhost:8080/
```

5. Fill html templates:

- Wellness

```
python3 fill-html-template.py \
                    --json models=/path/to/merged/models.json,
                    content=resources/content.json,
                    sample_info=resources/sample-info.json \
                    --template src/views/wellness-template.html \
                    --output-filename wellness.html
```

- Health

```
python3 fill-html-template.py \
                    --json models=/path/to/merged/models.json,
                    content=resources/content.json,
                    sample_info=resources/sample-info.json \
                    --template src/views/health-template.html \
                    --output-filename health.html
```

-----