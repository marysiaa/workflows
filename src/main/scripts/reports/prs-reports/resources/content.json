{
    "wellness_description": "Based on your genes we are finding more and more relevant genetic associations to aid you in guiding lifestyle, diet, and supplement choices with the goal of improving your overall health. Discover more about yourself and personalize your daily routine with DNA insights!",
    "glossary": [
        {
            "title": "Genotype",
            "description": "The sequence of DNA that co-determines (together with environmental factors) individual traits of an organism. It can refer to a DNA region at a particular place (e.g. a single-nucleotide polymorphism) or the whole personal set of genetic variants."
        },
        {
            "title": "Enviromental factors",
            "description": "Non-genetic factors that influence organism behavior including diet, lifestyle, stress, air pollution, exposure to toxins, pathogens, radiation as well as chemicals in personal-care products and household cleaners."
        },
        {
            "title": "DNA (deoxyribonucleic acid)",
            "description": "A molecule composed of two strands coiled around each other. It carries genetic instructions for the development and functioning of organisms. Within human cells, most of DNA is organized into 23 pairs of long structures called chromosomes."
        },
        {
            "title": "Gene",
            "description": "DNA region that contains the biological instructions for the production of a component of a protein or the whole protein."
        },
        {
            "title": "Nucleotides",
            "description": "The molecules (\"subunits\") that build your DNA. Each nucleotide is composed of a sugar, phosphoric acid, and a nitrogenous base. Four types of bases can be found in DNA: adenine (A), cytosine (C), guanine (G) and thymine (T). Composition and order in which nucleotides are arranged within strands of DNA determine genetic instructions."
        },
        {
            "title": "SNP (single nucleotide polymorphism) or single-nucleotide variant",
            "description": "A genetic variation at a single nucleotide between individuals. Apart from SNPs, other types of small genetic variations include insertions and deletions of a number of bases."
        },
        {
            "title": "Genome",
            "description": "The complete genetic material of an organism, including genes that encode proteins (2%) and the non-coding sequences (98%)."
        },
        {
            "title": "Phenotype",
            "description": "Specific characteristics of an individual."
        }
    ],
    "high_medium_risk_disclaimer": {
        "title": "It doesn't mean you will get sick",
        "text": [
            "Talk to your healthcare professional about your results and additional required test that he or she can prescribe."
        ]
    },
    "what_does_the_genetic_predisposition_mean": {
        "title": "What does the genetic predisposition mean?",
        "text": "The vast majority of differences between humans is caused by variations in their DNA, usually by alterations within a single nucleotide (refer to “SNP, single nucleotide polymorphism” section in the glossary below). Traits related to your health and well-being are influenced by a large number of such SNPs scattered throughout the genome and affecting many genes. Importantly, while each SNP impacts the particular trait only minimally, together they may tip the odds toward the disadvantageous phenotype (e.g. increased risk of a specific disease). Therefore, we determined your genotype at all genomic positions, which are known to influence the traits included in this report. A score obtained this way is called a polygenic risk score. It allows us to estimate the joint effect of all the SNPs in the context of your particular trait. As a result, it is possible to assess your genetic predisposition to a certain phenotype."
    },
    "risk_info": {
        "low": {
            "name": "Low",
            "description": "“Low” means that your score is lower than the scores obtained by 90% of the population so you have a very poor chance to develop the specific phenotype."
        },
        "medium": {
            "name": "Average",
            "description": "“Average” means that your score is close to the population average score so your chances of developing the specific phenotype are close to the chances of an average person in the population."
        },
        "high": {
            "name": "High",
            "description": "“High” means that your score exceeds values obtained by 90% of the population so you are in a high risk group – your chances for developing the specific phenotype are bigger than the population’s average."
        }
    },
    "talk_to_doctor": [
        "This report should be one of many aspects used by the healthcare provider to help with the diagnosis and treatment plan, but it is not the diagnosis itself.",
        "Talk to your doctor or genetic counselor."
    ],
    "how_to_read_results": "For each trait, we did extensive literature search. We compared your polygenic risk scores to risk scores calculated for >1000 individuals (usually more than 10,000) of European ancestry and categorize your results as “low”, “average” or “high” in reference to each trait in this report. ",
    "whats_next": "Now that you know more about your DNA, there are things you can do to protect your health. Here the resources you need to learn more and connect with professionals who can help guide you."
}