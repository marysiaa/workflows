#!/usr/bin/python3
import os
import json
import jinja2
import argparse

__version__ = '1.0.1'


def create_dict_from_json_files(json_files: str) -> dict:
    json_dict = {}
    for json_file in json_files:
        json_name, json_path = json_file.split('=')
        with open(json_path, 'r') as jsonInput:
            json_dict[json_name] = json.load(jsonInput)
    return json_dict


def save_html_to_file(path: str, filename: str, text: str):
    with open(f'{path}/{filename}', 'w') as file:
        file.write(text)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate mobigen report')
    parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument("--json", help='Comma delimited list of json files with names name1=file1,name2=file2')
    parser.add_argument("--template", help='Template file')
    parser.add_argument('--analysis-run', type=str, required=False, help='Analysis run', default="IntelliseqFlow")
    parser.add_argument("--output-filepath", required=False, help='Output filepath')
    parser.add_argument("--output-filename", help='Output filename')
    args = parser.parse_args()

    # create json dictionary from input json files
    json_dictionary = create_dict_from_json_files(json_files=args.json.split(','))

    # Analysis run to json_dictionary
    json_dictionary['analysis_run'] = args.analysis_run

    # render template
    templatesPath = os.path.dirname(os.path.realpath(args.template))
    templateLoader = jinja2.FileSystemLoader(searchpath=templatesPath)
    templateEnv = jinja2.Environment(loader=templateLoader)
    templateFile = os.path.basename(args.template)
    template = templateEnv.get_template(templateFile)
    outputText = template.render(json_dictionary)

    outputFilePath = args.output_filepath if args.output_filepath else templatesPath
    # save output to file
    save_html_to_file(path=outputFilePath, filename=args.output_filename, text=outputText)
