const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PostCSSPresetEnv = require('postcss-preset-env');
const SvgToMiniDataURI = require('mini-svg-data-uri');

const config = {
  devtool: 'eval',
  entry: {
    health: [
      './src/js/health.js',
      './src/scss/health.scss',
    ],
    wellness: [
      './src/js/wellness.js',
      './src/scss/wellness.scss',
    ]
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].[hash:8].js',
  },
  resolve: {
    modules: [__dirname, 'node_modules'],
    extensions: ['*', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => [
                PostCSSPresetEnv(),
              ],
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.svg$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              generator: (content) => SvgToMiniDataURI(content.toString()),
            },
          },
        ],
      },
      {
        test: /\.jpg|png$/i,
        use: [
          {
            loader: 'url-loader',
          },
        ],
      },
      {
        test: /\.woff$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              publicPath: '',
              name: './fonts/[name].[ext]',
              limit: 1000,
            },
          },
        ],
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'wellness.html',
      template: './src/views/wellness.html',
      inlineSource: '.(js|css)$',
      chunks: ['wellness'],
    }),
    new HtmlWebpackPlugin({
      filename: 'health.html',
      template: './src/views/health.html',
      inlineSource: '.(js|css)$',
      chunks: ['health'],
    }),
    new HtmlWebpackInlineSourcePlugin(HtmlWebpackPlugin),
    
    new MiniCssExtractPlugin({
      filename: '[name].[hash:8].css',
    }),
  ],
};

exports.default = config;
