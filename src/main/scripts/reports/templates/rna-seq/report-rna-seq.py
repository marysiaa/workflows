#!/usr/bin/python3
from docxtpl import DocxTemplate
import json
import argparse

__version__ = "1.0.1"


def load_bco_json(bco_json):
    with open(bco_json) as json_file:
        return json.load(json_file)


def get_tools_version(tools: list, final: dict, auto_or_manual: int, split: bool) -> dict:
    for tool in tools:
        for item in bco["execution_domain"]:
            for key, value in item["software_prerequisites"][auto_or_manual].items():
                if tool in key:
                    if split:
                        final[f"{tool}_version"] = value.split("+")[0]
                    else:
                        final[f"{tool}_version"] = value
    return final


def convert_stranded_to_int(strand: str) -> int:
    stranded_to_int_dict = {"unstranded": 0, "stranded": 1, "reversely_stranded": 2}
    return stranded_to_int_dict.get(strand)


def convert_stranded_to_argument(strand: str) -> str:
    stranded_to_arg_dict = {"unstranded": "", "stranded": "--fr-stranded", "reversely_stranded": "--rf-stranded"}
    return stranded_to_arg_dict.get(strand)


def generate_final_json(template_json, organism, number_of_samples, ensembl_version, analysis_id, analysis_name,
                        workflow_name, workflow_version, reference_genome, is_paired, stranded, fragment_length,
                        fragment_length_sd):
    keys = ('organism', 'number_of_samples', 'ensembl_version', 'analysis_id', 'analysis_name', 'workflow_name',
            'workflow_version', 'reference_genome', 'is_paired', 'fragment_length', 'fragment_length_sd')
    for item in keys:
        template_json[item] = locals()[item]
    template_json['stranded_to_int'] = convert_stranded_to_int(stranded)
    template_json['stranded_argument'] = convert_stranded_to_argument(stranded)
    return template_json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates rna-seq report')
    parser.add_argument('--template', type=str, required=True, help='BCO docx template')
    parser.add_argument('--bco', type=str, required=True, help='Json from bco module')
    parser.add_argument('--organism', type=str, required=True, help='Json from bco module')
    parser.add_argument('--number_of_samples', type=str, required=True, help='Json from bco module')
    parser.add_argument('--ensembl_version', type=str, required=True, help='Json from bco module')
    parser.add_argument('--analysis_id', type=str, required=True, help='Json from bco module')
    parser.add_argument('--analysis_name', type=str, required=True, help='Json from bco module')
    parser.add_argument('--workflow_name', type=str, required=True, help='Json from bco module')
    parser.add_argument('--workflow_version', type=str, required=True, help='Json from bco module')
    parser.add_argument('--reference_genome', type=str, required=True, help='Json from bco module')
    parser.add_argument('--is_paired', type=str, required=True, help='Json from bco module')
    parser.add_argument('--stranded', type=str, required=True, help='Json from bco module')
    parser.add_argument('--fragment_length', type=str, required=False, help='Json from bco module')
    parser.add_argument('--fragment_length_sd', type=str, required=False, help='Json from bco module')
    parser.add_argument('--output_filename', type=str, required=True, help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    doc = DocxTemplate(args.template)

    bco = load_bco_json(args.bco)
    basic_template_json = dict()

    # get versions of tools installed automatically
    tools_installed_automatically = ["star", "subread", "kallisto"]
    basic_template_json = get_tools_version(tools=tools_installed_automatically, final=basic_template_json,
                                            auto_or_manual=0, split=True)

    # get versions of tools installed manually
    tools_installed_manually = ["fastqc", "gatk"]
    basic_template_json = get_tools_version(tools=tools_installed_manually, final=basic_template_json,
                                            auto_or_manual=1, split=False)

    final_json = generate_final_json(basic_template_json, args.organism, args.number_of_samples, args.ensembl_version,
                                     args.analysis_id, args.analysis_name, args.workflow_name, args.workflow_version,
                                     args.reference_genome, args.is_paired, args.stranded, args.fragment_length,
                                     args.fragment_length_sd)
    doc.render(final_json)
    doc.save(f'{args.output_filename}')
