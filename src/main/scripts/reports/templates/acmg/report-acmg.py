#!/usr/bin/python3
from docxtpl import DocxTemplate, RichText, InlineImage
from docx.shared import Pt
import argparse
import sys
import os

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils

__version__ = "2.1.3"


def prepare_var_for_report(vcf_dict: dict, classification: str, field: str) -> int:
    """Prepare variant for report

    Args:
        vcf_dict (dict): vcf dict
        variant (str): variant
        classification (str): classification (Pathogenic, Likely^Pathogenic or Uncertain)

    Returns:
        var_num: number of variants
    """
    var_num = 0
    for item in vcf_dict:
        if item[field] == classification:
            var_num += 1
    return var_num


def generate_final_json(template_json: dict, vcf_dict: dict, cnv_dict: dict, others_dict: dict, analysis_type: str, analysis_start: str,
                        analysis_group: str, analysis_description: str, analysis_name: str, analysis_version: str,
                        analysis_run: str, sample_id: str, content: dict, var_call_tool: str, germline_analysis: bool, sv_calling_module: bool) -> dict:
    """Generate final json for report

    Args:
        template_json (dict): template json with obligatory fields
        vcf_dict (dict): vcf dict
        cnv_dict (dict): cnv dict
        others_dict (dict): others dict (like panel, panel_inputs, images_width)
        analysis_type (str): analysis type
        analysis_start (str): analysis start
        analysis_group (str): analysis group
        analysis_description (str): analysis description
        analysis_name (str): analysis name
        analysis_run (str): analysis run
        sample_id (str): sample id
        content (dict): content dict
        var_call_tool (str): variant calling tool
        germline_analysis (bool): is germline_analysis (for iontorrent)
        sv_calling_module (bool): is sv_calling_module

    Returns:
        dict: final json to report
    """
    for category, values in others_dict["images_width"].items():
        for name, width in values.items():
            template_json['images'].update({name: template_utils.get_image(doc, analysis_group, name, category, width)})
    
    # logo
    for name, parameters in others_dict["logos"][analysis_run].items():
        template_json['images'].update({name: template_utils.get_logo(doc, analysis_run, parameters["width"], parameters["height"])})

    # Number of variants
    num_var = len(vcf_dict) + len(cnv_dict)
    num_path = prepare_var_for_report(vcf_dict, "Pathogenic", "ISEQ_ACMG_SUMMARY_CLASSIFICATION") + \
        prepare_var_for_report(cnv_dict, "Pathogenic", "CLASS")
    num_likely_path = prepare_var_for_report(vcf_dict, "Likely^Pathogenic", "ISEQ_ACMG_SUMMARY_CLASSIFICATION") + \
        prepare_var_for_report(cnv_dict, "Likely^pathogenic", "CLASS")
    num_uncertain = prepare_var_for_report(vcf_dict, "Uncertain", "ISEQ_ACMG_SUMMARY_CLASSIFICATION") + \
        prepare_var_for_report(cnv_dict, "Uncertain", "CLASS")


    # Prepare genes panel
    genes_panel = template_utils.set_precision_and_split_genes(others_dict["panel"], 7, analysis_group, analysis_type) \
        if others_dict["panel"] else template_utils.get_genes_from_vcf(vcf_dict, 7)
    len_gene_panel = len([item for sublist in genes_panel for item in sublist]) - 1
    panel_inputs = others_dict["panel_inputs"] if "panel_inputs" in others_dict else []
    genes_bed_target = template_utils.split_genes_target(others_dict["genes_bed_target"], 7) \
        if others_dict["genes_bed_target"] else template_utils.get_genes_from_vcf(vcf_dict, 7)
    len_genes_bed_target = len([item for sublist in genes_bed_target for item in sublist]) - 1

    # versions_of_tools_and_databases
    content["versions_of_tools_and_databases"] = template_utils.prepare_version_of_tools_and_database_description(content, analysis_name, analysis_version)

    template_json.update({
        'variants': vcf_dict,
        'cnv_variants': cnv_dict,
        'igv_images': template_utils.get_igv_images(others_dict["picture_generate_report_sh"], doc, var_call_tool),
        'cnv_igv_images': template_utils.get_cnv_igv_images(others_dict["picture_generate_report_cnv_sh"], doc),
        'analysis_type': analysis_type,
        'analysis_start': analysis_start,
        'analysis_group': analysis_group,
        'analysis_name': analysis_name,
        'scope_of_analysis': analysis_description if analysis_description else "",
        'content': content,
        'unused_genes_desc': content["description_of_unused_or_changed_genes"].format(sample_id=sample_id),
        'num_path': RichText(num_path if num_path else "0", color='ffffff', size='36pt', font='Muli', bold=True),
        'num_like': RichText(num_likely_path if num_likely_path else "0", color='ffffff', size='36pt', font='Muli', bold=True),
        'num_unc': RichText(num_uncertain if num_uncertain else "0", color='ffffff', size='36pt', font='Muli', bold=True),
        'num_var': RichText(num_var if num_var else "0", color='ffffff', size='36pt', font='Muli',
                            bold=True),
        'num_variants': num_var,
        'genes_panel': genes_panel,
        'panel_inputs': panel_inputs,
        'gene_panel_logs': others_dict["gene_panel_logs"] if "gene_panel_logs" in others_dict else [],
        'genes_bed_target': genes_bed_target,
        'num_genes': len_gene_panel,
        'num_genes_bed': len_genes_bed_target,
        'germline_analysis': germline_analysis,
        'sv_calling_module': sv_calling_module,
        'gain': others_dict["gain"],
        'loss': others_dict["loss"]
    })
    return template_json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates clinical report from template and given json files')
    parser.add_argument('--template', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--variants-json', type=str, required=True,
                        help='Json file containing variants')
    parser.add_argument('--cnv-variants-json', type=str, required=False,
                        help='Json file containing cnv variants')
    parser.add_argument('--other-jsons', type=str, required=True, nargs='+',
                        help='Other json files (etc. sample_info, content, genes_panel')
    parser.add_argument('--analysis-type', type=str, required=True,
                        help='Analysis type (genome, exome or target)')
    parser.add_argument('--analysis-start', type=str, required=True,
                        help='Analysis start (fastq or vcf)')
    parser.add_argument('--analysis-group', type=str, required=True,
                        help='Analysis group (etc. germline, carrier_screening)')
    parser.add_argument('--analysis-description', type=str, required=False,
                        help='Analysis description')
    parser.add_argument('--analysis-workflow-name', type=str, required=False,
                        help='Analysis name')
    parser.add_argument('--analysis-workflow-version', type=str, required=False,
                        help='Analysis version')
    parser.add_argument('--analysis-run', type=str, required=False,
                        help='Analysis run', default="IntelliseqFlow")
    parser.add_argument('--sample-id', type=str, required=False,
                        help='Sample id')
    parser.add_argument('--var-call-tool', type=str, required=False, default="haplotypeCaller",
                        help='Variant calling tool (haplotypeCaller or deepVariant)')
    parser.add_argument('--aligner-tool', type=str, required=False, default="bwa-mem",
                        help='Aligner tool (bwa-mem or dragmap)')
    parser.add_argument('--germline-analysis', action='store_true',
                        help='Germline analysis (for iontorrent), if false (somatic analysis) then no Inheritance Pattern Match in report')
    parser.add_argument('--sv-calling-module', action='store_true',
                        help='Run in sv-calling module')
    parser.add_argument('--output-filename', type=str, required=True,
                        help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    doc = DocxTemplate(args.template)

    # Dict with jsons with pathogenicity from vcf
    vcf_dict = template_utils.prepare_data(
        template_utils.get_json_as_dict(args.variants_json), args.var_call_tool, args.aligner_tool)

    # Dict with jsons with pathogenicity from vcf
    if args.cnv_variants_json:
        cnv_dict = template_utils.prepare_cnv_data(template_utils.get_json_as_dict(args.cnv_variants_json))
    else:
        cnv_dict = {}

    # Dict with other jsons (etc. sample_info, content, genes_panel)
    others_dict = template_utils.prepare_dict_for_report(args.other_jsons)

    # Get basic template json
    basic_template_json = template_utils.get_basic_template_json(
        sample_info=others_dict["sample_info"],
        doc=doc
    )

    final_json = generate_final_json(
        template_json=basic_template_json,
        vcf_dict=vcf_dict,
        cnv_dict=cnv_dict,
        others_dict=others_dict,
        analysis_type=args.analysis_type,
        analysis_start=args.analysis_start,
        analysis_group=args.analysis_group,
        analysis_description=args.analysis_description,
        analysis_name=args.analysis_workflow_name,
        analysis_version=args.analysis_workflow_version,
        analysis_run=args.analysis_run,
        sample_id=args.sample_id,
        content=others_dict["content"],
        var_call_tool=args.var_call_tool,
        germline_analysis = args.germline_analysis,
        sv_calling_module = args.sv_calling_module
    )

    doc.render(final_json, autoescape=True)
    doc.save(f'{args.output_filename}')
