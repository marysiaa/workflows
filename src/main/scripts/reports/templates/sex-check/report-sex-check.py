#!/usr/bin/python3
from docxtpl import DocxTemplate, RichText, InlineImage
from docx.shared import Pt
import json
import argparse
import ntpath
import sys
import os

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils

__version__ = "1.2.0"


def get_tables(sc_jsons):
    tables = list()
    for sc_json in sc_jsons:
        filename = ntpath.basename(sc_json)
        with open(sc_json) as json_file:
            report = json.load(json_file)
            table_prefix = filename.split('-')[0]
            table = {'filename': filename, 'report': report, 'measure_type': RichText(report['measure_type'], color='000000', size='8pt', font='Muli', bold=True), 'result' : RichText("Male" if report['result'] == "M" else "Female" if report['result'] == "F" else 'Cannot be determined by this method', color='000000', size='8pt', font='Muli')}
            tables.append(table)
    return tables


def generate_final_json(template_json, tables, language_dict, analysis_run):
    template_json['language_dict'] = {
        'analysis_name': RichText(language_dict['analysis_name'], color='01275B', size='12pt', font='Muli', bold=True),
        'table_method': RichText(language_dict['table_method'], color='000000', size='8pt', font='Muli', bold=True),
        'table_result': RichText(language_dict['table_result'], color='000000', size='8pt', font='Muli', bold=True),
        'table_thresholds': RichText(language_dict['table_thresholds'], color='000000', size='8pt', font='Muli', bold=True),
        'analysis_info': language_dict['analysis_info']
    }
    # logo
    logos_dict = template_utils.get_json_as_dict("/resources/images/logos.json")
    for name, parameters in logos_dict[analysis_run].items():
        template_json['images'].update({name: template_utils.get_logo(doc, analysis_run, parameters["width"], parameters["height"])})
    template_json['tables'] = tables

    return template_json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates sexcheck report from inputed json files and specific template')
    parser.add_argument('--input-template', '-t', metavar='input_template', type=str, required=True,
                        help='Sexcheck docx template')
    parser.add_argument('--input-sample-info-json', '-s', metavar='input_sample_info_json', type=str, required=True,
                        help='Json file containing sample info')
    parser.add_argument('--input-dict-json', '-d', metavar='input_dict_json', type=str, required=True,
                        help='Dictionary json')
    parser.add_argument('--input-sex-check-jsons', '-j',  nargs="+", metavar='input_sex_check_jsons', type=str, required=True,
                        help='Jsons from sex check module')
    parser.add_argument('--analysis-run', type=str, required=False,
                        help='Analysis run', default="IntelliseqFlow")
    parser.add_argument('--output-filename', '-o',metavar='output_filename', type=str, required=True,
                    help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    doc = DocxTemplate(args.input_template)

    sample_info = template_utils.get_json_as_dict(args.input_sample_info_json)
    tables = get_tables(args.input_sex_check_jsons)
    language_dict = template_utils.get_json_as_dict(args.input_dict_json)

    basic_template_json = template_utils.get_basic_template_json(sample_info, doc)
    final_json = generate_final_json(basic_template_json, tables, language_dict, args.analysis_run)
    doc.render(final_json)
    doc.save(f'{args.output_filename}')
