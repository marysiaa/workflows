#!/usr/bin/env bash

set -eo pipefail 
function version() {
  printf "prepare-pictures-json.sh 1.0.0\n"
}

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -v|--version)
    version
    exit 0
    shift # past argument
    ;;
    -i|--pictures-gz)
    export pictures_gz="$2"
    shift # past argument
    shift # past value
    ;;
    -d|--directory)
    export directory="$2"
    shift # past argument
    shift # past value
    ;;
    -n|--filename)
    export filename="$2"
    shift # past argument
    shift # past value
    ;;
esac
done

if [ -z "$pictures_gz" ] ; then
    echo '[]' > "$filename"
else
    tar xvzf $pictures_gz -C $directory

    echo "[" > $filename
    for png in $directory/*.png
    do
        # Create png_name extracting chr and pos from png file name (png_name="chr15-48497301")
        png_name=$(echo $png | sed 's/.*\(chr[0-9XYM]*-[0-9]*\).*-igv-screenshot.png/\1/');
        # Get the path to png file
        png_path=$(realpath $png);
        # Save png_name and path to picture-generate-report-sh.json ({"chr15-48497301":"/home/test/test/Pictures/test_chr15-48497301-igv-screenshot.png"})
        echo "{\"$png_name\" : \""$png_path"\"},">> $filename
    done

    sed -i '$ s/.$//' $filename
    echo "]" >> $filename
fi
