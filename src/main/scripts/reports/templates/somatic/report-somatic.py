#!/usr/bin/python3
from docxtpl import DocxTemplate, RichText, InlineImage
from docx.shared import Pt
import argparse
import sys
import os

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils


__version__ = "1.0.0"


def count_civic_variants(vcf_dict: dict) -> int:
    """Count CIViC variants

    Args:
        vcf_dict (dict): vcf dict

    Returns:
        var_num: number of variants
    """
    var_num = 0
    for item in vcf_dict:
        if item["ISEQ_CIVIC_EVIDENCE_INFO"]:
            var_num += 1
    return var_num


def prepare_evidence(vcf_dict: dict):
    for variant in vcf_dict:
        evidence = list()
        if variant["ISEQ_CIVIC_EVIDENCE_INFO"]:
            evidence_info = variant["ISEQ_CIVIC_EVIDENCE_INFO"].split("_")
            evidence_info = [evidence.replace("^", " ").replace("*", ",") for evidence in evidence_info]
            evidence_description = variant["ISEQ_CIVIC_EVIDENCE_DESCRIPTION"].split("_")
            evidence_description = [evidence.replace("^", " ").replace("*", ",").replace("&", ";") for evidence in evidence_description]
            for index, item in enumerate(evidence_info):
                evidence.append({"info": evidence_info[index], "description": evidence_description[index]})
        variant["EVIDENCE"] = evidence


def generate_final_json(template_json: dict, vcf_dict: dict, others_dict: dict, analysis_type: str,
                        analysis_description: str, analysis_name: str, analysis_version: str,
                        analysis_run: str, sample_id: str, content: dict, var_call_tool: str) -> dict:
    """Generate final json for report

    Args:
        template_json (dict): template json with obligatory fields
        vcf_dict (dict): vcf dict
        others_dict (dict): others dict (like panel, panel_inputs, images_width)
        analysis_type (str): analysis type
        analysis_start (str): analysis start
        analysis_group (str): analysis group
        analysis_description (str): analysis description
        analysis_name (str): analysis name
        analysis_run (str): analysis run
        sample_id (str): sample id
        content (dict): content dict
        var_call_tool (str): variant calling tool

    Returns:
        dict: final json to report
    """
    for category, values in others_dict["images_width"].items():
        for name, width in values.items():
            template_json['images'].update({name: template_utils.get_image(doc, "germline", name, category, width)})
    
    # logo
    for name, parameters in others_dict["logos"][analysis_run].items():
        template_json['images'].update({name: template_utils.get_logo(doc, analysis_run, parameters["width"], parameters["height"])})

    # Number of variants
    num_var = len(vcf_dict)
    num_civic = count_civic_variants(vcf_dict)
    num_others = num_var - num_civic

    # Prepare genes panel
    genes_panel = template_utils.set_precision_and_split_genes(others_dict["panel"], 7, "germline", analysis_type) \
        if others_dict["panel"] else template_utils.get_genes_from_vcf(vcf_dict, 7)
    len_gene_panel = len([item for sublist in genes_panel for item in sublist]) - 1
    panel_inputs = others_dict["panel_inputs"] if "panel_inputs" in others_dict else []
    genes_bed_target = template_utils.split_genes_target(others_dict["genes_bed_target"], 7) \
        if others_dict["genes_bed_target"] else template_utils.get_genes_from_vcf(vcf_dict, 7)
    len_genes_bed_target = len([item for sublist in genes_bed_target for item in sublist]) - 1

    # versions_of_tools_and_databases
    content["versions_of_tools_and_databases"] = template_utils.prepare_version_of_tools_and_database_description(content, analysis_name, analysis_version)

    # Prepare evidence
    prepare_evidence(vcf_dict)

    template_json.update({
        'variants': vcf_dict,
        'igv_images': template_utils.get_igv_images(others_dict["picture_generate_report_sh"], doc, var_call_tool),
        'analysis_type': analysis_type,
        'analysis_name': analysis_name,
        'scope_of_analysis': analysis_description if analysis_description else "",
        'content': content,
        'unused_genes_desc': content["description_of_unused_or_changed_genes"].format(sample_id=sample_id),
        'num_civic': RichText(num_civic if num_civic else "0", color='ffffff', size='36pt', font='Muli', bold=True),
        'num_others': RichText(num_others if num_others else "0", color='ffffff', size='36pt', font='Muli', bold=True),
        'num_var': RichText(num_var if num_var else "0", color='ffffff', size='36pt', font='Muli',
                            bold=True),
        'num_variants': num_var,
        'genes_panel': genes_panel,
        'panel_inputs': panel_inputs,
        'gene_panel_logs': others_dict["gene_panel_logs"] if "gene_panel_logs" in others_dict else [],
        'genes_bed_target': genes_bed_target,
        'num_genes': len_gene_panel,
        'num_genes_bed': len_genes_bed_target
    })
    return template_json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates somatic report from template and given json files')
    parser.add_argument('--template', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--variants-json', type=str, required=True,
                        help='Json file containing variants')
    parser.add_argument('--other-jsons', type=str, required=True, nargs='+',
                        help='Other json files (etc. sample_info, content, genes_panel')
    parser.add_argument('--analysis-type', type=str, required=True,
                        help='Analysis type (genome, exome or target)')
    parser.add_argument('--analysis-description', type=str, required=False,
                        help='Analysis description')
    parser.add_argument('--analysis-workflow-name', type=str, required=False,
                        help='Analysis name')
    parser.add_argument('--analysis-workflow-version', type=str, required=False,
                        help='Analysis version')
    parser.add_argument('--analysis-run', type=str, required=False,
                        help='Analysis run', default="IntelliseqFlow")
    parser.add_argument('--sample-id', type=str, required=False,
                        help='Sample id')
    parser.add_argument('--output-filename', type=str, required=True,
                        help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    doc = DocxTemplate(args.template)

    # Dict with jsons with pathogenicity from vcf
    vcf_dict = template_utils.prepare_data(
        template_utils.get_json_as_dict(args.variants_json), "mutec", "bwa-mem", is_somatic=True)

    # Dict with other jsons (etc. sample_info, content, genes_panel)
    others_dict = template_utils.prepare_dict_for_report(args.other_jsons)

    # Get basic template json
    basic_template_json = template_utils.get_basic_template_json(
        sample_info=others_dict["sample_info"],
        doc=doc
    )

    # Get final json
    final_json = generate_final_json(
        template_json=basic_template_json,
        vcf_dict=vcf_dict,
        others_dict=others_dict,
        analysis_type=args.analysis_type,
        analysis_description=args.analysis_description,
        analysis_name=args.analysis_workflow_name,
        analysis_version=args.analysis_workflow_version,
        analysis_run=args.analysis_run,
        sample_id=args.sample_id,
        content=others_dict["content"],
        var_call_tool="mutec"
    )

    # Render template
    doc.render(final_json, autoescape=True)

    # Save report
    doc.save(f'{args.output_filename}')
