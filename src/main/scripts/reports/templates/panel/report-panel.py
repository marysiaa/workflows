#!/usr/bin/python3
from docxtpl import DocxTemplate
import json
import argparse
import ntpath
import sys
import os

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils

__version__ = "0.1.0"


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates report fq qc from template and given json files')
    parser.add_argument('--input-template', '-t', metavar='input_template', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--input-sample-info-json', '-s', metavar='input_sample_info_json', type=str, required=True,
                        help='Json file containing sample info')
    parser.add_argument('--input-panel-json', '-p', metavar='input_panel_json', type=str, required=True,
                        help='Json file containing genes panel information')
    parser.add_argument('--input-panel-inputs-json', '-i', metavar='input_panel_inputs_json', type=str, required=True,
                        help='Json file containing user inputs to genes panel')
    parser.add_argument('--input-name', '-n', metavar='input_name', type=str, required=True,
                        help='Report name')
    parser.add_argument('--output-dir', '-l', metavar='output_dir', type=str, required=True,
                        help='Directory where goes the output')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()
    return args


def get_json_as_dict(json_path):
    with open(json_path) as json_file:
        return json.load(json_file)


def generate_final_json(template_json, genes_panel, panel_inputs):
    template_json['genes_panel'] = genes_panel
    template_json['panel_inputs'] = panel_inputs

    return template_json


if __name__ == "__main__":
    args = args_parser_init()
    doc = DocxTemplate(args.input_template)

    sample_info = get_json_as_dict(args.input_sample_info_json)
    panel = get_json_as_dict(args.input_panel_json)
    panel_inputs = get_json_as_dict(args.input_panel_inputs_json)

    genes_panel = template_utils.set_precision_and_split_genes(panel, 7)

    basic_template_json = template_utils.get_basic_template_json(sample_info, doc)
    final_json = generate_final_json(basic_template_json, genes_panel, panel_inputs)

    filename = ntpath.basename(args.input_template)

    doc.render(final_json)
    doc.save(f'{args.output_dir}/{args.input_name}.docx')
