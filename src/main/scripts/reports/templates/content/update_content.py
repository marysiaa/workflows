#!/usr/bin/env python3

import json
import argparse


__version__ = "1.0.8"


def load_json(file_name):
    with open(file_name, 'r') as f:
        return json.load(f)


def save_json(file_name: str, content: dict):
    with open(file_name, 'w') as f:
        json.dump(content, f, indent=2)


def set_multiallelic_qual(analysis_start: str, aligner: str, caller: str) -> int:
    if analysis_start == "vcf":
        return 0
    elif aligner == 'bwa-mem' and caller == 'haplotypeCaller':
        return 300
    elif aligner == 'dragmap' and caller == 'haplotypeCaller':
        return 70
    return 30


def set_multiallelic_desc(multiallelic_qual: int):
    if multiallelic_qual > 0:
        return f", as well as low quality multiallelic variants (QUAL < {multiallelic_qual}) were then removed"
    return " were then removed"


def prepare_name(content: dict, analysis_group: str) -> str:
    return content[analysis_group]


def prepare_name_somatic(content: dict, tumor_only: bool) -> str:
    if tumor_only:
        return content["somatic"]["tumor_only"]
    return content["somatic"]["tumor_normal"]


def prepare_methods(content: dict, analysis_group: str, aligner: str, caller: str, exome_kit: str, padding: str, 
                    qual: str, af: str, impact: str, multiallelic_desc: str, sample_id: str,
                    min_qual: str, del_cov_max: str, dup_cov_min: str, het_max: str, bf_threshold: str) -> dict:
    methods = dict()
    content_methods = content["methods"]
    variant_calling = "variant_calling_and_spurious_variant_removing"
    variant_annotation = "variant_annotation_and_annotation_based_filtering"
    methods["fastq_quality_check"] = content_methods["fastq_quality_check"]
    methods["alignment_and_bam_files_processing"] = prepare_alignment_methods(
        content_methods["alignment_and_bam_files_processing"], aligner, caller)
    methods[variant_calling + "_snp"] = prepare_variant_calling_methods_snp(
        content_methods[variant_calling]["snp"], caller, aligner, exome_kit, padding)
    methods[variant_annotation + "_snp"] = prepare_variant_annotation_methods_snp(
        content_methods[variant_annotation]["snp"], analysis_group, qual, af, impact, multiallelic_desc, sample_id)
    if content_methods[variant_calling].get("cnv"):
        methods[variant_calling + "_cnv"] = prepare_variant_calling_methods_cnv(content_methods[variant_calling]["cnv"],
            min_qual, del_cov_max, dup_cov_min, het_max, bf_threshold)    
        methods[variant_annotation + "_cnv"] = prepare_variant_annotation_methods_cnv(
            content_methods[variant_annotation]["cnv"])
    return methods


def prepare_alignment_methods(content: dict, aligner: str, caller: str) -> str:
    bqsr_description = ""
    if aligner == 'bwa-mem' and (caller == 'haplotypeCaller' or caller == 'mutec'):
        bqsr_description = content["bqsr_description"]
    return content["text"].format(aligner=aligner, BQSR_description=bqsr_description)


def prepare_variant_calling_methods_snp(content: dict, caller: str, aligner: str, exome_kit: str, padding: str) -> str:
    content["text"][0] = content["text"][0].format(
        caller=content["caller"][caller], 
        exome_version=exome_kit,
        padding=padding)
    if caller != "mutec":
        vqsr_description = ""
        if content.get("vqsr_description", None) and aligner == 'bwa-mem' and caller == 'haplotypeCaller':
            vqsr_description = content["vqsr_description"]
        content["text"][1] = content["text"][1].format(
            caller_genotyping=content["caller_genotyping"][caller],
            vqsr_description=vqsr_description)
    if caller != "deepVariant":
        content["text"][2] = content["text"][2].format(
            hard_filtering=content["hard_filtering"][aligner])
    else:
        del content["text"][2]
    return content["text"]


def prepare_variant_calling_methods_cnv(content: dict, min_qual: str, del_cov_max: str,
                                        dup_cov_min: str, het_max: str, bf_threshold: str) -> str:
    # for genome
    if len(content["text"]) == 5:
        content["text"][2] = content["text"][2].format(min_qual=min_qual)
        content["text"][3] = content["text"][3].format(del_cov_max=del_cov_max, het_max=het_max)
        content["text"][4] = content["text"][4].format(dup_cov_min=dup_cov_min)
        return content["text"]
    # for exome, target
    content["text"][3] = content["text"][3].format(bf_threshold=bf_threshold)
    return content["text"]    


def prepare_variant_calling_methods_somatic(content: dict, exome_kit: str) -> str:
    content["text"][0] = content["text"][0].format(exome_version=exome_kit)
    return content["text"]       


def prepare_variant_annotation_methods_snp(content: dict, analysis_group: str, qual: str, 
                                       af: str, impact: str, multiallelic_desc: str, sample_id: str) -> str:
    if qual:
        qual_desc = f"Variants with QUAL < {qual} were removed. " if qual > 0 else ""
    else:
        qual_desc = ""
    content_text = content[analysis_group]["text"]
    content_text[0] = content_text[0].format(
        QUAL=qual_desc, 
        AF=af, 
        IMPACT=impact, 
        MULTIALLELIC_DESC=multiallelic_desc,
        SAMPLE_ID=sample_id)
    content_text[1] = content_text[1].format(SAMPLE_ID=sample_id)
    return content_text


def prepare_variant_annotation_methods_cnv(content: dict) -> str:
    return content["text"]


def prepare_variant_annotation_methods_somatic(content: dict) -> str:
    return content["text"]


def prepare_igv_plot_description(content: dict, analysis_start: str, caller: str) -> str:
    igv_content = content['text'][0].format(
        igv_plot_description=content[analysis_start]['panel_description'])
    if analysis_start == "fastq":
        caller_content = content[analysis_start]["caller"][caller]
        igv_content = igv_content.format(caller=caller_content)
    return igv_content


def prepare_cnv_igv_plot_description(content: dict) -> str:
    return content['text'][0]


def prepare_somatic_igv_plot_description(content: dict, tumor_only: bool) -> str:
    if tumor_only:
        return content["text"][0].format(igv_plot_description=content["tumor_only"]["panel_description"])
    return content["text"][0].format(igv_plot_description=content["tumor_normal"]["panel_description"])


def prepare_somatic_methods(content: dict, exome_kit: str) -> dict:
    methods = dict()
    content_methods = content["methods"]
    variant_calling = "variant_calling_and_spurious_variant_removing"
    variant_annotation = "variant_annotation_and_annotation_based_filtering"
    methods["fastq_quality_check"] = content_methods["fastq_quality_check"]
    methods["alignment_and_bam_files_processing"] = prepare_alignment_methods(
        content_methods["alignment_and_bam_files_processing"], "bwa-mem", "mutec")
    methods[variant_calling] = prepare_variant_calling_methods_somatic(content_methods[variant_calling]["somatic"], exome_kit)
    methods[variant_annotation] = prepare_variant_annotation_methods_somatic(content_methods[variant_annotation]["somatic"])
    return methods


def main():
    parser = argparse.ArgumentParser(description='Generates content json file based on analysis type')
    parser.add_argument('--basic-content', type=str, required=True,
                        help='Basic content json file')
    parser.add_argument('--content', type=str, required=True,
                        help='Content json file based on analysis type')
    parser.add_argument('--analysis-start', type=str, required=False, default="fastq",
                        help='Analysis start (fastq or vcf)')
    parser.add_argument('--analysis-group', type=str, required=True,
                        help='Analysis group (etc. germline, carrier_screening)')
    parser.add_argument('--kit-short-name', type=str, required=False,
                        nargs='?', const='', help='A short name for an exome kit (for example exome-v6)')
    parser.add_argument('--padding', type=str, required=False, 
                        help='Padding for variant calling')
    parser.add_argument('--af', type=float, required=False,
                        help='Allele frequency')
    parser.add_argument('--impact', type=str, required=False,
                        help='Impact')
    parser.add_argument('--qual', type=float, required=False,
                        help='Filtered QUAL')
    parser.add_argument('--multiallelic-qual', type=float, required=False,
                        help='Filtered Multiallelic QUAL')                        
    parser.add_argument('--min-qual', type=float, required=False,
                        help='Filtered QUAL (CNV)') 
    parser.add_argument('--del-cov-max', type=float, required=False,
                        help='Maximal coverage of valid deletion')  
    parser.add_argument('--dup-cov-min', type=float, required=False,
                        help='Minimal coverage of valid duplication') 
    parser.add_argument('--het-max', type=float, required=False,
                        help='Maximal heterozygosity of valid deletion') 
    parser.add_argument('--bf-threshold', type=float, required=False,
                        help='Minimal BF of valid variant')                                                                                                    
    parser.add_argument('--var-call-tool', type=str, required=False, default="haplotypeCaller",
                        help='Variant calling tool (haplotypeCaller or deepVariant)')
    parser.add_argument('--aligner-tool', type=str, required=False, default="bwa-mem",
                        help='Aligner tool (bwa-mem or dragmap)')
    parser.add_argument('--sample-id', type=str, required=False, default="DEMO",
                        help='Sample id')
    parser.add_argument('--tumor-only', action='store_true',
                        help='Tumor only analysis (for somatic)')
    parser.add_argument('--output-filename', type=str, required=True,
                        help='Output json filename')
    parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')
    args = parser.parse_args()

    # Load content jsons
    basic_content = load_json(args.basic_content)
    content = load_json(args.content)

    # Initiate final content
    final_content = dict()

    # Prepare name
    if args.analysis_group == "somatic":
        final_content["name"] = prepare_name_somatic(content["name"], args.tumor_only)
        final_content["methods"] = prepare_somatic_methods(
            content=content, 
            exome_kit=args.kit_short_name)
        # Prepare igv plot description
        final_content["igv_plot_description"] = prepare_somatic_igv_plot_description(
            content=content["somatic_igv_plot_description"], 
            tumor_only=args.tumor_only)
    else:
        final_content["name"] = prepare_name(content["name"], args.analysis_group)

        # Set multiallelic qual
        if args.multiallelic_qual:
            multiallelic_qual = args.multiallelic_qual
        else:
            multiallelic_qual = set_multiallelic_qual(args.analysis_start, args.aligner_tool, args.var_call_tool)
            multiallelic_desc = set_multiallelic_desc(multiallelic_qual)

        # Prepare methods
        final_content["methods"] = prepare_methods(
            content=content, 
            analysis_group=args.analysis_group, 
            aligner=args.aligner_tool, 
            caller=args.var_call_tool, 
            exome_kit=args.kit_short_name,
            padding=args.padding,
            qual=args.qual, 
            af=args.af, 
            impact=args.impact, 
            multiallelic_desc=multiallelic_desc,
            sample_id=args.sample_id,
            min_qual=args.min_qual,
            del_cov_max=args.del_cov_max,
            dup_cov_min=args.dup_cov_min,
            het_max=args.het_max,
            bf_threshold=args.bf_threshold)

        # Prepare cnv igv plot description
        final_content["cnv_igv_plot_description"] = prepare_cnv_igv_plot_description(
            content=content["cnv_igv_plot_description"])

        # Prepare igv plot description
        final_content["igv_plot_description"] = prepare_igv_plot_description(
            content=content["igv_plot_description"], 
            analysis_start=args.analysis_start, 
            caller=args.var_call_tool)


    # Add basic content to final content
    final_content.update(basic_content)

    save_json(args.output_filename, final_content)


if __name__ == "__main__":
    main()
