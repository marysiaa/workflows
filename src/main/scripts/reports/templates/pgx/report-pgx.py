#!/usr/bin/python3
from docxtpl import DocxTemplate
import json
import argparse
import sys
import os
import pandas as pd
from itertools import groupby
import operator


REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils


__version__ = "0.0.3"


def read_tsv(tsv_file: str) -> dict:
    return pd.read_csv(tsv_file, sep='\t')


def explode_df(df: pd.DataFrame) -> pd.DataFrame:
    df = df.assign(**{'GENE':df['GENE'].str.split(',')})
    df = df.assign(**{'RS':df['RS'].str.split(',')})
    for index, row in df.iterrows():
        if len(row["GENE"]) != len(row["RS"]):
            row["RS"] = [row["RS"] for i in range(len(row["GENE"]))]
            row["RS"] = [item for sublist in row["RS"] for item in sublist]
    return df.explode(['GENE', 'RS'])


def df_to_json(df: pd.DataFrame) -> dict:
    result = df.to_json(orient='records')
    return json.loads(result)


def sort_dict(dict_to_sort: dict) -> dict:
    dict_to_sort.sort(key=lambda variant: variant['GENE'].split(":")[0])


def group_by_gene(dict_to_group: dict) -> groupby:
    return groupby(dict_to_group, lambda variant: variant['GENE'].split(":")[0])


def replace_in_genotype(genotype: str) -> str:
    return genotype.replace(".|.", "No call").replace("./.", "No call")


def get_allele_match_data(grouped_dict: groupby) -> dict:
    allele_match_data = dict()
    for gene, group in grouped_dict:
        gene_content = []
        for content in group:
            info = dict()
            info["position"] = content["GNOMAD_ID"]
            info["rs"] = content["RS"]
            info["call"] = replace_in_genotype(content["GT"])
            gene_content.append(info)
        allele_match_data[gene] = gene_content
    return allele_match_data


def add_allele_match_data(tsv_from_vcf: pd.DataFrame) -> dict:
    json_from_tsv = df_to_json(tsv_from_vcf)
    sort_dict(json_from_tsv)
    grouped_by_gene = group_by_gene(json_from_tsv)
    return get_allele_match_data(grouped_by_gene)


def get_drug_phenotype(main_dict: dict) -> dict:
    drug_phenotypes = dict()
    for gene, drugs in main_dict.items():
        for drug, drug_info in drugs.items():
            phenotypes = None
            if drug_info.get("openpgx"):
                phenotypes_list = []
                for info in drug_info["openpgx"]:
                    if info.get("database") == "cpic":
                        for gene, factor in info["factors"].items(): 
                            phenotypes_list.append(gene + " " + factor)
                phenotypes = "; ".join(phenotypes_list)
            if drug_phenotypes.get(drug):
                drug_phenotypes[drug]["phenotypes"].append(phenotypes)
            else:
                drug_phenotypes[drug] = {"phenotypes": [phenotypes]}
            drug_phenotypes[drug]["phenotypes"] = [i for i in drug_phenotypes[drug]["phenotypes"] if i is not None]
            drug_phenotypes[drug]["phenotypes"] = list(dict.fromkeys(drug_phenotypes[drug]["phenotypes"]))

            # add therapeutic area and drug class
            drug_phenotypes[drug]["therapeutic_area"] = drug_info["therapeutic_area"]
            drug_phenotypes[drug]["drug_class"] = drug_info["drug_class"]
    return drug_phenotypes


def add_drug_phenotypes(recommendations: dict) -> dict:
    return get_drug_phenotype(recommendations)


def get_drug_recommendations(main_dict: dict) -> dict:
    drug_recommendations = dict()
    for gene, drugs in main_dict.items():
        for drug, drug_info in drugs.items():
            recommendations = {
                "openpgx": lambda : openpgx_drug_recommendations(drug_info["openpgx"], drug_recommendations, drug),
                "clinical_annotation": lambda : clinical_annotation_drug_recommendations(drug_info["clinical_annotation"], drug_recommendations, drug),
                "variant_annotation": lambda : variant_annotation_drug_recommendations(drug_info["variant_annotation"], drug_recommendations, drug)
            }
            key = list(drug_info.keys())[0]
            recommendations.get(key, lambda : "ERROR: Invalid Operation")()
    return drug_recommendations


def openpgx_drug_recommendations(openpgx_recommendations: dict, final_dict: dict, drug: str):
    for info in openpgx_recommendations:
        drug_informations = dict()
        drug_informations["genotypes"], drug_informations["phenotypes"] = info["report_genotype"], info["report_phenotype"]
        drug_informations["source_informations"] = ["Database: " + info["database"]]
        if info.get("strength"):
            drug_informations["source_informations"].append("Strength: " + info["strength"])
        drug_informations["recommendation"] = info["recommendation"]
        drug_informations["guideline"] = info["guideline"]
        add_to_final_dict(final_dict, drug, drug_informations, "openpgx")


def clinical_annotation_drug_recommendations(clinical_annotation_recommendations: dict, final_dict: dict, drug: str):
    for info in clinical_annotation_recommendations:
        drug_informations = dict()
        drug_informations["genotype"] = info["report_genotype"]
        drug_informations["source_informations"] = ["Database: PharmGKB"]
        drug_informations["source_informations"].append("PharmGKB level: " + info["Level_of_Evidence"])
        drug_informations["source_informations"].append("PharmGKB ID: " + str(info["Clinical_Annotation_ID"]))
        drug_informations["source_informations"].append("Phenotype category: " + info["Phenotype_Category"])
        drug_informations["recommendation"] = info["Annotation_Text"]
        if info.get("Phenotype(s)"):
            drug_informations["source_informations"].append("Phenotypes: " + ", ".join(info["Phenotype(s)"]))
        add_to_final_dict(final_dict, drug, drug_informations, "clinical_annotation")


def variant_annotation_drug_recommendations(variant_annotation_recommendations: dict, final_dict: dict, drug: str):
    for info in variant_annotation_recommendations:
        drug_informations = dict()
        drug_informations["genotype"] = info["report_genotype"]
        drug_informations["source_informations"] = ["Database: PharmGKB"]
        drug_informations["source_informations"].append("PharmGKB ID: " + str(info["Variant_Annotation_ID"]))
        drug_informations["source_informations"].append("PMID: " + str(info["PMID"]))
        drug_informations["recommendation"] = info["Sentence"]
        add_to_final_dict(final_dict, drug, drug_informations, "variant_annotation")


def add_to_final_dict(final_dict: dict, drug: str, drug_info: dict, database: str):
    if final_dict.get(drug):
        if final_dict[drug].get(database):
            final_dict[drug][database].append(drug_info)
        else:
            final_dict[drug][database] = [drug_info]
    else:
        final_dict[drug] = {database: [drug_info]}


def add_drug_recommendations(recommendations: dict) -> dict:
    return get_drug_recommendations(recommendations)


def prioritize_drug_recommendations(recommendations: dict) -> dict:
    order = ["openpgx", "clinical_annotation", "variant_annotation"]
    prioritized = dict()
    for drug, drug_infos in recommendations.items():
        prioritized[drug] = {k: drug_infos[k] for k in order if k in drug_infos}
    return prioritized


def sort_drugs_by_own_order(recommendations: dict, drugs: list) -> list:
    dict_keys = list(recommendations.keys())
    drugs_list = drugs + dict_keys
    return {k: recommendations[k] for k in drugs_list if k in recommendations}


def main():
    parser = argparse.ArgumentParser(description='Generates PGX report from input json file and specific template')
    parser.add_argument('--input-template', type=str, required=True, help='PGX docx template')
    parser.add_argument('--input-recommendations', type=str, required=True, help='Json file containing recommendations')
    parser.add_argument('--input-tsv-from-vcf', type=str, required=True, help='Tsv file containing allele match data')
    parser.add_argument('--input-sample-info-json', type=str, required=True, help='Json file containing sample info')
    parser.add_argument('--drug-order', type=str, required=True, help='Semicolon separated list of drugs')
    parser.add_argument('--output-filename', type=str, required=True, help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    doc = DocxTemplate(args.input_template)

    # Add sample info to final_json
    sample_info = template_utils.get_json_as_dict(args.input_sample_info_json)
    final_json = template_utils.get_basic_template_json(sample_info, doc)

    # Load recommendations JSON
    recommendations = template_utils.get_json_as_dict(args.input_recommendations)

    # Add drug_phenotypes dict to final_json
    final_json['drug_phenotypes'] = add_drug_phenotypes(recommendations)

    # Add drug_recommendations dict to final_json
    final_json['drug_recommendations'] = add_drug_recommendations(recommendations)

    # Prioritize drug_recommendations
    final_json['drug_recommendations'] = prioritize_drug_recommendations(final_json['drug_recommendations'])

    # Sort drugs by own order
    final_json['drug_recommendations'] = sort_drugs_by_own_order(final_json['drug_recommendations'], drugs=args.drug_order.split(";"))

    # Load tsv file
    tsv_file = read_tsv(args.input_tsv_from_vcf)

    # Explode df
    exploded_df = explode_df(tsv_file)

    # Add allele_match_data dict to final_json
    final_json['allele_match_data'] = add_allele_match_data(exploded_df)

    # Render template
    doc.render(final_json)

    # Save output
    doc.save(f'{args.output_filename}')


if __name__ == "__main__":
    main()
