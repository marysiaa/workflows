#!/usr/bin/python3
from docxtpl import DocxTemplate, InlineImage
from docx.shared import Pt
import json
import argparse
import ntpath
import sys
import os

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils

__version__ = "1.1.0"


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates detection chance report from template and given json files')
    parser.add_argument('--input-template', '-t', metavar='input_template', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--input-sample-info-json', '-s', metavar='input_sample_info_json', type=str, required=True,
                        help='Json file containing sample info')
    parser.add_argument('--input-detection-stats-json', '-d', metavar='input_detection_stats_json', type=str, required=True,
                        help='Json file containing detection statistics')
    parser.add_argument('--analysis-run', type=str, required=False,
                        help='Analysis run', default="IntelliseqFlow")
    parser.add_argument('--output-filename', type=str, required=True, help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()
    return args


def get_json_as_dict(json_path):
    with open(json_path) as json_file:
        return json.load(json_file)



if __name__ == "__main__":
    args = args_parser_init()
    doc = DocxTemplate(args.input_template)

    sample_info = get_json_as_dict(args.input_sample_info_json)
    stats = get_json_as_dict(args.input_detection_stats_json)

    template_json = template_utils.get_basic_template_json(sample_info, doc)
    template_json['stats'] = stats
    # logo
    logos_dict = template_utils.get_json_as_dict("/resources/images/logos.json")
    for name, parameters in logos_dict[args.analysis_run].items():
        template_json['images'].update({name: template_utils.get_logo(doc, args.analysis_run, parameters["width"], parameters["height"])})

    filename = ntpath.basename(args.input_template)

    doc.render(template_json)
    doc.save(f'{args.output_filename}')
