#!/usr/bin/env bash

set -eo pipefail 
function version() {
  printf "prepare-sample-info-json.sh 1.1.0\n"
}

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -v|--version)
    version
    exit 0
    shift # past argument
    ;;
    -i|--sample-info-json)
    export sample_info_json="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--timezoneDifference)
    export timezoneDifference=$2
    shift # past argument
    shift # past value
    ;;
    -a|--sampleID)
    export sampleID="$2"
    shift # past argument
    shift # past value
    ;;
esac
done

# Create empty json file as input to python script if were not provided in wdl inputs.
[ -z "$sample_info_json" ] && echo "{}" > sample_info.json || cat $sample_info_json > sample_info.json

# Complete the sample-info.json file with the missing keys
touch tmp.json
for i in "name" "surname" "sex" "birth_date" "patient_id" "ID" "source" "sequencing_type" "received_date" "sequencing_date" "report_date" "ordering_physician" "patient_description"
do
    if [ $(grep -F -c "\""$i"\"" sample_info.json) -eq 0 ] ; then
        echo "\""$i"\": \"\"," >> tmp.json
    fi
done
if [ -s tmp.json ] ; then
    sed -i '$s/,//' tmp.json
    sed -i '$s/}//;s/"$/",/' sample_info.json
    cat tmp.json >> sample_info.json
    echo "}"  >> sample_info.json
fi

# Counting time difference between zones and entering the report date to the sample-info.json file
LENGTH_DATE=$(jq '.report_date | length' sample_info.json)
if [[ "$LENGTH_DATE" == 0 ]]
then
    CURRENT_TIME=$(echo $(($(date +%s%N)/1000000)))
    DIFFERENCE=$(((CURRENT_TIME+($timezoneDifference))/1000))
    DATE=$(date -d @$DIFFERENCE +"%m/%d/%Y")
    jq -r --arg REPORT_DATE "$DATE" '.report_date = $REPORT_DATE' sample_info.json > sample_info.json.tmp && cat sample_info.json.tmp > sample_info.json
fi

# Entering the analysis specifier to the sample-info.json file
LENGTH_ID=$(jq '.ID | length' sample_info.json)
if ([ ! -z "$sampleID" ] && [[ "$LENGTH_ID" == 0 ]])
then
    jq -r --arg ID "$sampleID" '.ID = $ID' sample_info.json > sample_info.json.tmp && cat sample_info.json.tmp > sample_info.json
fi

echo "SCRIPT"
