#!/usr/bin/python3
from docxtpl import DocxTemplate, RichText
import json
import argparse
import sys
import os
import re

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils

__version__ = "1.0.0"


def load_bco_json(bco_json):
    with open(bco_json) as json_file:
        bco = json.load(json_file)
    return bco


def parametric_domain_json(bco_json, step: int, parametric_domain: list):
    table = dict()
    table["task_name"] = bco_json["parametric_domain"][step * 5]["name"]
    table["cpu"] = bco_json["parametric_domain"][step * 5]["value"]
    table["memory"] = bco_json["parametric_domain"][step * 5 + 1]["value"]
    table["time"] = bco_json["parametric_domain"][step * 5 + 2]["value"]
    table["docker"] = bco_json["parametric_domain"][step * 5 + 3]["value"]
    table["docker_size"] = bco_json["parametric_domain"][step * 5 + 4]["value"]
    try:
        table["module_name"] = bco_json["parametric_domain"][step * 5]["module_name"]
    except KeyError:
        table["module_name"] = ""
    parametric_domain.append(table)


def execution_domain_json(bco_json, step: int, execution_domain: list):
    table = dict()
    table["task_name"] = bco_json["execution_domain"][step]["name"]
    # check if last element is digit (example vcf_anno_freq_3), if 'yes' remove last underscore and digit/number
    # needed so that there are no duplicate rows in the resulting file
    if table["task_name"].split("_")[-1].isdigit():
        table["task_name"] = re.sub("_\d", "", table["task_name"])
        table["task_name"] = re.sub("\d", "", table["task_name"])
    table["packages"] = bco_json["execution_domain"][step]["software_prerequisites"][0]
    table["tools"] = bco_json["execution_domain"][step]["software_prerequisites"][1]
    table["scripts"] = bco_json["execution_domain"][step]["software_prerequisites"][2]
    table["system"] = bco_json["execution_domain"][step]["software_prerequisites"][3]
    table["resources"] = bco_json["execution_domain"][step]["external_data_endpoints"]
    # needed for sorting
    try:
        table["module_name"] = bco_json["execution_domain"][step]["module_name"]
    except KeyError:
        table["module_name"] = ""
    table["time"] = bco_json["parametric_domain"][step * 5 + 2]["value"]
    execution_domain.append(table)


def sort_domain(domain, sort_by: list):
    return sorted(domain, key=lambda k: (k[sort_by[0]], k[sort_by[1]]))


def generate_final_json(template_json, parametric_domain, execution_domain):
    template_json['parametric_domain'] = parametric_domain
    template_json['execution_domain'] = execution_domain
    return template_json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates bco report from input json file and specific template')
    parser.add_argument('--input-template', type=str, required=True, help='BCO docx template')
    parser.add_argument('--input-sample-info-json', type=str, required=True, help='Json file containing sample info')
    parser.add_argument('--input-bco-json', type=str, required=True, help='Json from bco module')
    parser.add_argument('--is-rna-seq', type=bool, required=False, default=False, help='Is it analysis for RNA-Seq')
    parser.add_argument('--output-filename', type=str, required=True, help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    doc = DocxTemplate(args.input_template)

    sample_info = template_utils.get_json_as_dict(args.input_sample_info_json)
    basic_template_json = template_utils.get_basic_template_json(sample_info, doc)

    if args.is_rna_seq:
        basic_template_json["sample_id"] = sample_info["ID"]

    bco = load_bco_json(args.input_bco_json)
    parametric_domain = []
    execution_domain = []
    steps_number = len(bco["provenance_domain"]["steps"])
    for step in range(steps_number):
        parametric_domain_json(bco, step, parametric_domain)
        execution_domain_json(bco, step, execution_domain)

    parametric_domain = sort_domain(parametric_domain, ["module_name", "time"])
    execution_domain = {each["task_name"]: each for each in execution_domain}.values()
    execution_domain = sort_domain(execution_domain, ["module_name", "time"])
    final_json = generate_final_json(basic_template_json, parametric_domain, execution_domain)
    doc.render(final_json)
    doc.save(f'{args.output_filename}')
