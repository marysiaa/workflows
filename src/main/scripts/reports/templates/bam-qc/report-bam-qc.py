#!/usr/bin/python3
from docxtpl import DocxTemplate, RichText
import argparse
import ntpath
import os
import sys

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils

__version__ = "0.1.0"


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates report fq qc from template and given json files')
    parser.add_argument('--input-template', '-t', metavar='input_template', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--input-sample-info-json', '-s', metavar='input_sample_info_json', type=str, required=True,
                        help='Json file containing sample info')
    parser.add_argument('--input-coverage-stats-json', '-c', metavar='input_coverage_stats_json', type=str, required=True,
                        help='Json file containing coverage stats')
    parser.add_argument('--input-name', '-n', metavar='input_name', type=str, required=True,
                        help='Report name')
    parser.add_argument('--output-dir', '-l', metavar='output_dir', type=str, required=True,
                        help='Directory where goes the output')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()
    return args


def split_metric_classes(coverage_stats):
    split_coverage_stats = dict()
    split_coverage_stats['reads'] = list()
    split_coverage_stats['bases'] = list()
    split_coverage_stats['coverage'] = list()

    for coverage_stat in coverage_stats:
        split_coverage_stats[coverage_stat['metric_class']].append(coverage_stat)

    return split_coverage_stats


def generate_final_json(template_json, split_coverage_stats):
    template_json['analysis_name'] = RichText('BAM quality check', color='01275B', size='12pt', font='Muli', bold=True)
    template_json['split_coverage_stats'] = split_coverage_stats
    template_json['table_title'] = {
            'bases_metrics': RichText('Bases metrics', color='FFFFFF', size='12pt', font='Muli', bold=True),
            'coverage_metrics': RichText('Coverage metrics', color='FFFFFF', size='12pt', font='Muli', bold=True),
            'reads_metrics': RichText('Reads metrics', color='FFFFFF', size='12pt', font='Muli', bold=True),
            'metric': RichText('Metric', color='000000', size='8pt', font='Muli', bold=True),
            'computed_value': RichText('Computed value', color='000000', size='8pt', font='Muli', bold=True),
            'expected_value': RichText('Expected value', color='000000', size='8pt', font='Muli', bold=True),
            'expected_description': RichText('Description', color='000000', size='8pt', font='Muli', bold=True)
            }
    return template_json


if __name__ == "__main__":
    args = args_parser_init()
    doc = DocxTemplate(args.input_template)

    sample_info = template_utils.get_json_as_dict(args.input_sample_info_json)
    coverage_stats = template_utils.get_json_as_dict(args.input_coverage_stats_json)
    split_coverage_stats = split_metric_classes(coverage_stats)

    basic_template_json = template_utils.get_basic_template_json(sample_info, doc)
    final_json = generate_final_json(basic_template_json, split_coverage_stats)

    filename = ntpath.basename(args.input_template)

    doc.render(final_json)
    doc.save(f'{args.output_dir}/{args.input_name}.docx')
