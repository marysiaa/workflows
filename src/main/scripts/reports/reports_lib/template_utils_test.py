#!/usr/bin/python3

from template_utils import *


def test_round_value():
    assert round_value(value=2.22222) == 2.22


def test_set_precision_and_split_genes():
    panel = [{'name': 'ADAMTS2', 'score': 100.00, 'type': 'Ehlers-Danlos'},
             {'name': 'AEBP1', 'score': 100.00, 'type': 'Ehlers-Danlos'},
             {'name': 'B3GALT6', 'score': 100.00, 'type': 'Ehlers-Danlos'}]
    output = [[{'name': 'Gene name', 'score': 'phenotype match', 'type': 'type'},
               {'name': 'ADAMTS2', 'score': '100.00', 'type': 'Ehlers-Danlos'}],
              [{'name': 'AEBP1', 'score': '100.00', 'type': 'Ehlers-Danlos'},
               {'name': 'B3GALT6', 'score': '100.00', 'type': 'Ehlers-Danlos'}]]
    assert set_precision_and_split_genes(panel=panel, interval=2) == output


def test_get_basic_template_json():
    sample_info = {
        'sequencing_platform': 'Illumina HiSeq X',
        'material': 'saliva',
        'sending_date': '26-09-2017',
        'sex': 'M',
        'ID': 'SIJA-WES',
        'sequencing_type': 'Whole exome',
        'name': '',
        'surname': '',
        'birth_date': '',
        'patient_id': '',
        'source': '',
        'received_date': '',
        'sequencing_date': '',
        'report_date': '04/14/2021',
        'ordering_physician': '',
        'patient_description': ''
    }
    doc_germline = DocxTemplate("../templates/acmg/report-acmg-template.docx")
    doc_carrier_screening = DocxTemplate("../templates/acmg/report-carrier-screening-template.docx")
    results_germline = get_basic_template_json(sample_info=sample_info, doc=doc_germline)
    results_carrier_screening = get_basic_template_json(sample_info=sample_info, doc=doc_carrier_screening)
    assert type(results_germline) == dict
    assert "page_break" in (results_germline and results_carrier_screening)
    assert "images" in (results_germline and results_carrier_screening)


def test_all():
    test_round_value()
    test_set_precision_and_split_genes()
    test_get_basic_template_json()


test_all()
