#!/usr/bin/python3
"""Template utils"""

import json
from docxtpl import DocxTemplate, RichText, InlineImage
from docx.shared import Pt
import math
import re
import os


def get_json_as_dict(json_path: str) -> json:
    """Load json"""
    with open(json_path) as json_file:
        return json.load(json_file)


def get_logo(doc, analysisRun: str, width: int, height: int) -> InlineImage:
    """Get logo path

    Args:
        analysisRun (str): analysis run (IntelliseqFlow or T-Systems)
        width (int): logo width
        height (int): logo height

    Returns:
        InlineImage: InlineImage object with image
    """
    path = f"/resources/images/{analysisRun}-logo.png"
    return InlineImage(doc, path, width=Pt(width), height=Pt(height))


def set_precision_and_split_genes(panel: dict, interval: int, analysis_group: str = "germline",
                                  analysis_type: str = "fastq") -> list:
    """Set precision and split genes into list"""
    for gene in panel:
        gene['score'] = format(gene['score'], '.2f')

    if analysis_group == "carrier_screening" or analysis_type == "target":
        panel = sorted(panel, key=lambda k: (k['name']))
    else:
        panel = sorted(panel, key=lambda k: (-(float(k['score'])), k['name']))
    example_cell = {"name": "Gene name", "score": "phenotype match", "type": "type"}
    panel.insert(0, example_cell)

    return [panel[i:i + interval] for i in range(0, len(panel), interval)]


def get_genes_from_vcf(vcf_dict: dict, interval: int) -> list:
    """Get genes from vcf"""
    genes = []
    for item in vcf_dict:
        genes.append(item["Gene_Name__ISEQ_REPORT_ANN"])
    genes = list(set(genes))
    panel = []
    for gene in genes:
        gene_name = dict()
        gene_name["name"] = gene
        panel.append(gene_name)
    panel = sorted(panel, key=lambda k: (k['name']))
    example_cell = {"name": "Gene name"}
    panel.insert(0, example_cell)
    return [panel[i:i + interval] for i in range(0, len(panel), interval)]


def round_value(value: float) -> float:
    """Round value of float"""
    if value == 0.0:
        return 0.0
    return round(value, 3 - int(math.floor(math.log10(abs(value)))) - 1)


def prepare_basic_images(sex: str, doc):
    """Prepare (load) basic images"""
    return InlineImage(doc, f"/resources/images/human-{sex}.png", width=Pt(12), height=Pt(12))


def prepare_het_images(het: str, doc, width):
    """Prepare (load) het images"""
    return InlineImage(doc, f"/resources/images/{het}.png", width=Pt(width), height=Pt(14))


def prepare_sample_info_field(sample_info: dict, field: str):
    """Prepare sample info field"""
    size = '8pt'
    bold = False
    if field in ["name", "surname"]:
        size = '12pt'
        bold = True
    return RichText(sample_info[field], color='01275B', size=size, font='Muli', bold=bold)


def prepare_sample_info_dict(sample_info: dict) -> dict:
    """Prepare sample info dict"""
    results = dict()
    sample_info["sex"] = sample_info["sex"].capitalize()
    results["sex_image"] = sample_info["sex"]
    for key in sample_info.keys():
        results[key] = prepare_sample_info_field(sample_info, key)
    results["fullname"] = RichText(sample_info['name'] + " " + sample_info['surname'],
                                   color='01275B', size='8pt', font='Muli')
    return results


def get_basic_template_json(sample_info: dict, doc) -> dict:
    """Get basic template json"""
    basic_template_json = {
        'page_break': RichText('\f'),
        'images': {
            'Male': prepare_basic_images("male", doc),
            'Female': prepare_basic_images("female", doc),
            'compound_het': prepare_het_images("compound_het", doc, width=69),
            'recessive': prepare_het_images("recessive", doc, width=52),
            'dominant': prepare_het_images("dominant", doc, width=53),
            'possibly_dominant': prepare_het_images("possibly_dominant", doc, width=78),
            'none': prepare_het_images("none", doc, width=35),
            'mitochondrial': prepare_het_images("mitochondrial", doc, width=68)
        },
        'sample_info': prepare_sample_info_dict(sample_info)
    }
    return basic_template_json


def get_igv_images(igv_pngs_json: dict, doc, var_call_tool: str) -> dict:
    """Get IGV images"""
    images = dict()
    var_call_dict = {
        "deepVariant": 550,
        "haplotypeCaller": 450,
        "mutec": 500,
        "unknown": 450
    }
    for igv_png in igv_pngs_json:
        for key in igv_png.keys():
            images[key] = InlineImage(doc, igv_png[key], width=Pt(var_call_dict[var_call_tool]), height=Pt(400))
    return images


def get_cnv_igv_images(igv_pngs_json: dict, doc) -> dict:
    """Get cnv IGV images"""
    images = dict()
    for igv_png in igv_pngs_json:
        for key in igv_png.keys():
            images[key] = InlineImage(doc, igv_png[key], width=Pt(500), height=Pt(400))
    return images


def variant_information(variant: dict, var_call_tool: str, aligner_tool: str, is_somatic=False) -> list:
    """Get variant information"""
    rs = variant["ISEQ_DBSNP_RS"] if variant["ISEQ_DBSNP_RS"] else None
    chrom_pos = variant["ISEQ_GNOMAD_ID"]
    GT = variant["GT"]
    try:
        zygosity = "Heterozygote" if "0" in GT else "Homozygote" if "0" not in GT else "Unknown zygosity"
    except TypeError:
        zygosity = "Unknown zygosity"

    feature_ID = variant["Feature_ID__ISEQ_REPORT_ANN"]
    if variant.get("Feature_ID__ISEQ_MANE_ANN", None):
        feature_ID = feature_ID + " (MANE transcript: " + variant["Feature_ID__ISEQ_MANE_ANN"] + ")"
    HGVS_c = variant["HGVS.c__ISEQ_REPORT_ANN"]
    HGVS_p = variant["HGVS.p__ISEQ_REPORT_ANN"]
    if not is_somatic:
        try:
            depth = "Allele depth (ref/alt): " + variant["AD"]
        except:
            depth = "Allele depth (ref/alt): N/A"
        try:
            quality_value = round_value(variant["QUAL"])
            if var_call_tool != "unknown":
                if var_call_tool == "deepVariant":
                    quality_level = "Low" if quality_value < 10 else "Dubious" if 10 <= quality_value < 20 \
                        else "Acceptable" if 20 <= quality_value < 30 else "High"
                elif var_call_tool == "haplotypeCaller" and aligner_tool == "bwa-mem":
                    quality_level = "Low" if quality_value < 100 else "Dubious" if 100 <= quality_value < 200 \
                        else "Acceptable" if 200 <= quality_value < 300 else "High"
                elif var_call_tool == "haplotypeCaller" and aligner_tool == "dragmap":
                    quality_level = "Low" if quality_value < 40 else "Dubious" if 40 <= quality_value < 50 \
                        else "Acceptable" if 50 <= quality_value < 70 else "High"
                quality = quality_level + f" quality call (QC: {str(quality_value)})"
            else:
                quality = f"QC: {quality_value}"
        except:
            quality = "QC: N/A"

        # check if variant is penetrance (low_penetrance in ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE classification)    
        penetrance = ""
        if variant.get("ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE", None):
            penetrance = "Low penetrance" if "low_penetrance" in variant["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"] else ""
        variant_info = [i for i in [rs, chrom_pos, zygosity, feature_ID, HGVS_c, HGVS_p, depth, quality, penetrance] if i]
    else:
        try:
            ad = variant["AD"].split("/")
            percent = round_value((int(ad[1]) / (int(ad[0]) + int(ad[1]))) * 100)
            mutation_percent = f"Mutation percent: {percent}% (ref {ad[0]}/alt {ad[1]})"
        except:
            mutation_percent = "Mutation percent: N/A"
        function_types = "Function: " + variant["Function"] if variant["Function"] else ""
        known_mutation_types = "Known mutation types: " + variant["Known_mutation_types"] if variant["Known_mutation_types"] else ""
        cosmic_reads = "Number of COSMIC records: " + str(len(variant["ISEQ_COSMIC_ID"].split(','))) if variant["ISEQ_COSMIC_ID"] else ""
        alignment_artifact = "Marked as alignment artifact" if "alignment" in variant["FILTER"].split("; ") else ""
        variant_info = [i for i in [rs, chrom_pos, zygosity, feature_ID, HGVS_c, HGVS_p, mutation_percent, 
                                    function_types, known_mutation_types, cosmic_reads, alignment_artifact] if i]
    return variant_info


def set_predicted_impact(item):
    """Set predicted impact"""
    items_to_change = {
        "ISEQ_SIFT4G_MIN": "SIFT",
        "PhastCons100way": "PhastCons",
        "ISEQ_GERP": "GERP",
        "ISEQ_M_CAP": "MCAP"
    }
    for key, value in items_to_change.items():
        item[value] = item[key] if item[key] else None
        item[value] = round_value(item[value]) if item[value] else None

    round_item_fields(item)


def set_predicted_impact_somatic(item):
    """Set predicted impact"""
    items_to_change = {
        "dbNSFP_BayesDel_addAF_score": "BayesDel",
        "dbNSFP_DANN_score": "DANN",
        "dbNSFP_fathmm_MKL_coding_score": "FATHMM_MKL"
    }
    for key, value in items_to_change.items():
        item[value] = item[key] if item[key] else None
        item[value] = round_value(item[value]) if item[value] else None
    item["Aloft"] = set_aloft(item)
    round_value(item[value]) if item[value] else None
    
    round_item_fields(item)


def set_aloft(item):
    """Set Aloft"""
    try:
        predictor = item["dbNSFP_Aloft_pred"].split(":")
        predictor = [i for i in predictor if i != "."][0]
        probability = item[f"dbNSFP_Aloft_prob_{predictor}"].split(":")
        return [i for i in probability if i != "."][0]
    except:
        return None


def set_inheritance(item):
    """Set inheritance"""
    inheritance = []
    inh_to_remove = ["Autosomal_dominant_somatic_cell_mutation", "Polygenic_inheritance", "Somatic_mosaicism",
                     "Somatic_mutation", "Sporadic"]
    inh_to_remove_x_linked = ["X-linked_dominant_inheritance", "X-linked_inheritance", "X-linked_recessive_inheritance"]
    try:
        for inh in item["ISEQ_HPO_INHERITANCE"].split(":")[0].split("^"):
            if inh != ".":
                if inh in inh_to_remove or (inh == "Mitochondrial" and item["CHROM"] != "chrMT") or \
                        (inh in inh_to_remove_x_linked and item["CHROM"] != "chrX") or \
                        (inh == "Y-linked_inheritance" and item["CHROM"] != "chrY"):
                    continue
                inheritance.append(inh.replace("_", " "))
        item['INHERITANCE'] = inheritance
    except (TypeError, AttributeError):
        item['INHERITANCE'] = inheritance


def prepare_disease(disease: str):
    """Prepare diseases"""
    return re.sub('([a-zA-Z])', lambda x: x.groups()[0].upper(), disease, 1)


def set_diseases(item):
    """Set diseases"""
    diseases = []
    if "ISEQ_DISEASE_HPO" in item and item["ISEQ_DISEASE_HPO"]:
        for disease in item["ISEQ_DISEASE_HPO"].split("_"):
            disease = prepare_disease(disease)
            diseases.append(disease.replace("^", " ").replace("*", ","))
    elif item["ISEQ_DISEASE_GHR"]:
        for disease in item["ISEQ_DISEASE_GHR"].split("_"):
            disease = prepare_disease(disease)
            diseases.append(disease.replace("^", " ") + " (GHR)")
    # sort A-Z
    diseases.sort()
    item['DISEASES'] = diseases


def set_inheritance_pattern_match(item):
    """Set inheritance pattern match"""
    genotype_to_inh = item["ISEQ_GENOTYPE_TO_INHERITANCE_MATCH"]
    compound_het = item["ISEQ_COMPOUND_HET"]
    pattern_match = "inheritance_pattern_match"
    if genotype_to_inh == "recessive" and compound_het:
        item[pattern_match] = "compound_het"
    elif genotype_to_inh == "recessive" and not compound_het:
        item[pattern_match] = "recessive"
    elif genotype_to_inh == "dominant" and not compound_het:
        item[pattern_match] = "dominant"
    elif not genotype_to_inh and not compound_het:
        item[pattern_match] = "none"
    elif genotype_to_inh == "mitochondrial":
        item[pattern_match] = "mitochondrial"
    elif genotype_to_inh == "possibly_dominant":
        item[pattern_match] = "possibly_dominant"


def set_clinvar_classification(item):
    """Set set clinvar classification"""
    agg_significance = "ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"
    item[agg_significance] = item[agg_significance] \
        if (item[agg_significance] and
            item[agg_significance] != 'not_provided') else "undefined"
    others_classifications = ["affects", "association", "association_not_found", "drug_response", "not_provided",
                              "other", "protective", "risk_factor"]
    if item[agg_significance] in others_classifications:
        item[agg_significance] = "others"


def prepare_data(classification: dict, var_call_tool: str, aligner_tool: str, is_somatic=False) -> dict:
    """Prepare data"""
    for item in classification:
        # VARIANT
        item['VARIANT'] = variant_information(item, var_call_tool, aligner_tool, is_somatic)
        set_variant_frequency(item)
        set_clinvar_classification(item)
        if is_somatic:
            set_predicted_impact_somatic(item)
        else:
            set_inheritance(item)
            set_diseases(item)
            set_predicted_impact(item)
            set_inheritance_pattern_match(item)
        # Gene
        item["GENE"] = item["Gene_Name__ISEQ_REPORT_ANN"]

    return classification


def set_variant_frequency(item):
    """Set variant frequency"""
    wes_af = "WES_AF"
    wgs_af = "WGS_AF"
    
    item[wes_af] = item.get("ISEQ_GNOMAD_EXOMES_AF_nfe", None)
    item[wes_af] = round_value(item[wes_af]) if item[wes_af] else None
    GENOMES_AF_nfe = item.get("ISEQ_GNOMAD_GENOMES_AF_nfe", None)
    GENOMES_V3_AF_nfe = item.get("ISEQ_GNOMAD_GENOMES_V3_AF_nfe", None)
    WGS_AF = [i for i in [GENOMES_AF_nfe, GENOMES_V3_AF_nfe] if i]
    item[wgs_af] = max(WGS_AF) if WGS_AF else None
    item[wgs_af] = round_value(item[wgs_af]) if item[wgs_af] else None

    popmax_to_set = ["ISEQ_GNOMAD_EXOMES_popmax", "ISEQ_GNOMAD_GENOMES_popmax", "ISEQ_GNOMAD_GENOMES_V3_popmax"]
    valuesPOP = append_each_if_not_none(item, popmax_to_set)

    AF_suffix = "_AF"
    popmax_AF_to_set = [popmax + AF_suffix for popmax in popmax_to_set]
    valuesAF = append_each_if_not_none(item, popmax_AF_to_set)

    try:
        item["maxAF"] = max([i for i in valuesAF if i])
        item["maxAF"] = round_value(item["maxAF"]) if item["maxAF"] else None
        maxAF_index = valuesAF.index(max([i for i in valuesAF if i]))
        item["maxPOP"] = valuesPOP[maxAF_index].upper()
    except ValueError:
        item["maxAF"] = None
        item["maxPOP"] = None


def round_item_fields(item):
    """Round item fields"""
    for field in item:
        if re.findall('SCORE', field) and field != "ISEQ_GENE_PANEL_SCORE":
            try:
                item[field] = round(item[field], 3 - int(math.floor(math.log10(abs(item[field])))) - 1)
            except ValueError:
                continue


def append_each_if_not_none(item, to_set):
    """Append if not none"""
    out_list = list()
    for popmax in to_set:
        popmax_value = item.get(popmax, None)
        out_list.append(popmax_value)
    return out_list


def prepare_cnv_data(cnv_data: dict) -> dict:
    """Prepare cnv data"""
    # Check CNV variant for only DUP and DEL variants
    cnv_data = [i for i in cnv_data if i['SVTYPE'] in ["DUP", "DEL"]]
    for item in cnv_data:
        item["ZYGOSITY"] = set_zygosity(item)
        set_loss_or_gain_source(item)
        set_loss_or_gain_coord(item)
        set_overlap(item)
        item["GENES"] = set_panel_genes(item)
        item["BF_or_QUAL"] = set_bf_or_qual(item)
        item['VARIANT'] = [item["ZYGOSITY"], item["GENES"], item["BF_or_QUAL"]]
        item['VARIANT'] = [i for i in item['VARIANT'] if i]
        item['ID_SVTYPE'] = item["ID"] + "_" + item["SVTYPE"]
        item['CNV_SCORE'] = round_value(item["AnnotSV_ranking_score"][0])
        item["CLASS"] = item["ACMG_class"][0]
        item['DISEASES'] = set_cnv_diseases(item)
        item['SPLITTED_CRITERIA'] = split_ranking_criteria(item)
    return cnv_data


def set_zygosity(cnv_variant: dict) -> str:
    """Set zygosity"""
    try:
        GT = str(cnv_variant[list(cnv_variant.keys())[-1]]["GT"][0]) + "/" + str(cnv_variant[list(cnv_variant.keys())[-1]]["GT"][1])
        return "Heterozygote" if "0" in GT else "Homozygote" if "0" not in GT else "Unknown zygosity"
    except TypeError:
        return "Unknown zygosity"


def set_loss_or_gain_source(cnv_variant: dict) -> str:
    """Set loss or gain source (P_loss_source, P_gain_source, B_loss_source, B_gain_source)"""

    sources = {
        "DUP": ["P_gain_source", "B_gain_source"],
        "DEL": ["P_loss_source", "B_loss_source"]
    }

    for source in sources.get(cnv_variant["SVTYPE"], []):
        splitted_source = source.split("_")
        cnv_variant[splitted_source[0] + "_" + splitted_source[2]] = []
        if cnv_variant.get(source, None):
            cnv_variant[splitted_source[0] + "_" + splitted_source[2]] = cnv_variant[source][0].split("|")


def set_loss_or_gain_coord(cnv_variant: dict) -> str:
    """Set loss or gain coord (P_loss_coord, P_gain_coord, B_loss_coord, B_gain_coord)"""

    coords = {
        "DUP": ["P_gain_coord", "B_gain_coord"],
        "DEL": ["P_loss_coord", "B_loss_coord"]
    }

    for coord in coords.get(cnv_variant["SVTYPE"], []):
        splitted_coord = coord.split("_")
        cnv_variant[splitted_coord[0] + "_" + splitted_coord[2]] = []
        if cnv_variant.get(coord, None):
            cnv_variant[splitted_coord[0] + "_" + splitted_coord[2]] = cnv_variant[coord][0].split("|")


def set_overlap(cnv_variant: dict) -> str:
    """Set loss or gain overlap"""

    coords = ["P_coord", "B_coord"]
    variant_pos = str(cnv_variant["SV_start"][0]) + "-" + str(cnv_variant["SV_end"][0])
    for coord in coords:
        gain_or_loss = coord.split("_coord")[0]
        cnv_variant[f"{gain_or_loss}_mutation_overlap"] = []
        cnv_variant[f"{gain_or_loss}_known_variant_overlap"] = []
        if cnv_variant.get(coord, None):
            for known_variant_pos in cnv_variant[coord]:
                mutation_overlap, known_variant_overlap = overlap_value(variant_pos, known_variant_pos.split(":")[1])
                cnv_variant[f"{gain_or_loss}_mutation_overlap"].append(mutation_overlap)
                cnv_variant[f"{gain_or_loss}_known_variant_overlap"].append(known_variant_overlap)


def split_pos(pos: str) -> int:
    return int(pos.split("-")[0]), int(pos.split("-")[1])


def overlap_value(variant_pos, known_variant_pos):
    start_variant, end_variant = split_pos(variant_pos)
    start_known_variant, end_known_variant = split_pos(known_variant_pos)

    overlap = max(0, min(end_variant, end_known_variant) - max(start_variant, start_known_variant))
    
    length_variant = end_variant - start_variant
    length_known_variant = end_known_variant - start_known_variant

    return round_value(overlap/length_variant*100), round_value(overlap/length_known_variant*100)


def merge_source_and_coord(source: list, coord: list) -> list:
    source_coord = []
    for i in range(len(source)):
        source_coord.append(source[i] + " " + coord[i])
    return source_coord


def set_panel_genes(cnv_variant: dict) -> str:
    """Set panel genes"""
    if cnv_variant.get("Gene_name", None):
        return cnv_variant["Gene_name"][0].replace("|", ", ")
    return None


def set_bf_or_qual(cnv_variant: dict) -> str:
    """Set BF (Bayes factor) or QUAL if BF not defined"""
    if cnv_variant.get("BF", None):
        BF = round_value(cnv_variant["BF"])
        return f"Bayes factor (from ExomeDepth tool): {BF}"
    elif cnv_variant.get("QUAL", None):
        QUAL = round_value(cnv_variant["QUAL"])
        return f"QUAL (from lumpy tool): {QUAL}"
    return None


def set_cnv_diseases(cnv_variant: dict):
    """Set CNV diseases"""

    if cnv_variant.get("Gene_name", None):
        genes_list = cnv_variant["Gene_name"][0].split("|")

    diseases_hpo = []
    if cnv_variant.get("ISEQ_DISEASE_HPO", None):
        diseases_hpo_list = cnv_variant["ISEQ_DISEASE_HPO"].split("|")
        diseases_hpo = dict(zip(genes_list, diseases_hpo_list))
        for key in list(diseases_hpo.keys()):
            if diseases_hpo[key] == ".":
                del diseases_hpo[key]
                continue
            diseases_list = []
            for disease in diseases_hpo[key].split("_"):
                disease = prepare_disease(disease)
                diseases_list.append(disease.replace("^", " ").replace("*", ","))
            diseases_hpo[key] = diseases_list

    diseases_ghr = []
    if cnv_variant.get("ISEQ_DISEASE_GHR", None):
        diseases_ghr_list = cnv_variant["ISEQ_DISEASE_GHR"].split("|")
        diseases_ghr = dict(zip(genes_list, diseases_ghr_list))
        for key in list(diseases_ghr.keys()):
            if diseases_ghr[key] == ".":
                del diseases_ghr[key]
                continue
            diseases_list = []
            for disease in diseases_ghr[key].split("_"):
                disease = prepare_disease(disease)
                diseases_list.append(disease.replace("^", " ").replace("*", ",") + " (GHR)")
            diseases_ghr[key] = diseases_list

    if diseases_hpo and diseases_ghr:
        return mergeDictionary(diseases_hpo, diseases_ghr)
    elif diseases_hpo:
        return diseases_hpo
    elif diseases_ghr:
        return diseases_ghr
    return {}


def mergeDictionary(dict_1, dict_2):
    dict_3 = {**dict_1, **dict_2}
    for key, value in dict_3.items():
        if key in dict_1 and key in dict_2:
            dict_3[key] = [value, dict_1[key]]
            # flatten list and sort
            dict_3[key] = [item for sublist in dict_3[key] for item in sublist]
            dict_3[key].sort()
    return dict_3


def split_ranking_criteria(cnv_variant: dict) -> list:
    splitted_criteria = cnv_variant["AnnotSV_ranking_criteria"][0].split("|")
    return [item.split("^")[0] for item in splitted_criteria]


def get_image(doc, analysis_group: str, name: str, clinvar_or_acmg: str, width: int):
    """Get image path"""
    pathogenic_dict = {
        "pathogenic/likely_pathogenic": "pathogenic",
        "pathogenic_low_penetrance": "pathogenic",
        "likely_pathogenic_low_penetrance": "likely_pathogenic",
        "benign/likely_benign": "likely_benign"
    }
    name = name.replace("^", "_").lower()
    if pathogenic_dict.get(name, None):
        name = pathogenic_dict[name]
    path = f"/resources/images/{analysis_group}/{name}{clinvar_or_acmg}.png"
    return InlineImage(doc, path, width=Pt(width), height=Pt(14))


def prepare_dict_for_report(jsons: list) -> dict:
    """Prepare dict for report (json -> dict)"""
    result = dict()
    for json_file in jsons:
        name = os.path.basename(json_file).split(".json")[0].replace("-", "_")
        result[name] = get_json_as_dict(json_file)
    return result


def split_genes_target(panel: dict, interval: int) -> list:
    """Split genes for target analysis"""
    panel = sorted(panel, key=lambda k: (k['name']))
    example_cell = {"name": "Gene name"}
    panel.insert(0, example_cell)
    return [panel[i:i + interval] for i in range(0, len(panel), interval)]


def prepare_version_of_tools_and_database_description(content: dict, analysis_name: str, analysis_version: str) -> str:
    return content["versions_of_tools_and_databases"].format(analysis_name=analysis_name, analysis_version=analysis_version)
