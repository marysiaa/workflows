workflow report_somatic_workflow
{
    call report_somatic
}

task report_somatic {

    File? var_json
    File? sample_info_json
    File? pictures_gz
    File? panel_json
    File? panel_inputs_json
    File? genes_bed_target_json
    File? gene_panel_logs
    Boolean tumor_only = false

    Int timezoneDifference = 0

    String output_name = "somatic"
    String sample_id = "no_id_provided"
    String genome_or_exome = "exome"
    String? kit
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    String analysisEnvironment = "IntelliseqFlow"
    String outputs = "outputs"

    String task_name = "report_somatic"
    String task_version = "1.0.2"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/report_somatic:1.0.3"

    command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ${outputs}

    # Prepare sample-info.json
    bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                      --timezoneDifference ${timezoneDifference} \
                                                      --sampleID "${sample_id}"

    # Prepare picture.json with paths to png files
    mkdir pictures
    bash /intelliseqtools/prepare-pictures-json.sh  --pictures-gz "${pictures_gz}" \
                                                    --directory "pictures" \
                                                    --filename "picture-generate-report-sh.json"
                                                
    # change input variants filename to variants.json
    if [ -z "${var_json}" ] ; then
        echo '[]' > variants.json
    else
        cat ${var_json} > variants.json
    fi

    if [ -z "${panel_json}" ] ; then
        echo '[]' > panel.json
    else
        cat ${panel_json} > panel.json
    fi

    if [ -z "${panel_inputs_json}" ] ; then
        echo '[]' > panel_inputs.json
    else
        cat ${panel_inputs_json} > panel_inputs.json
    fi

    if [ -z "${genes_bed_target_json}" ] ; then
        echo '[]' > genes_bed_target.json
    else
        cat ${genes_bed_target_json} > genes_bed_target.json
    fi

    if [ -z "${gene_panel_logs}" ] ; then
        echo '[]' > gene_panel_logs.json
    else
        cat ${gene_panel_logs} > gene_panel_logs.json
    fi

    # add info to somatic variants
    python3 /intelliseqtools/tools/add-info-to-somatic-variants.py --variants "variants.json" \
                                                                   --info "/resources/cancer-gene-census-filtered.tsv" \
                                                                   --output-filename "variants.json"


    # create content json based on analysis_type (genome, exome, target or iontorrent)
    python3 /intelliseqtools/reports/templates/script/update_content.py --basic-content "/resources/basic-content.json" \
                                                                        --content "/resources/${genome_or_exome}-content.json" \
                                                                        --analysis-group "somatic" \
                                                                        --kit-short-name "${kit}" \
                                                                        --var-call-tool "mutec" \
                                                                        ${true="--tumor-only" false="" tumor_only} \
                                                                        --output-filename "content.json"                                                


    # create report
    python3 /intelliseqtools/reports/templates/script/report-somatic.py --template "/resources/report-somatic-template.docx" \
                                                                        --variants-json "variants.json" \
                                                                        --other-jsons "sample_info.json" \
                                                                                      "panel.json" \
                                                                                      "panel_inputs.json" \
                                                                                      "genes_bed_target.json" \
                                                                                      "gene_panel_logs.json" \
                                                                                      "picture-generate-report-sh.json" \
                                                                                      "content.json" \
                                                                                      "/resources/images/images-width.json" \
                                                                                      "/resources/images/logos.json" \
                                                                        --analysis-type "${genome_or_exome}" \
                                                                        --analysis-description "${analysisDescription}" \
                                                                        --analysis-workflow-name "${analysisWorkflow}" \
                                                                        --analysis-workflow-version "${analysisWorkflowVersion}" \
                                                                        --analysis-run "${analysisEnvironment}" \
                                                                        --sample-id "${sample_id}" \
                                                                        --output-filename "${outputs}/${sample_id}_${output_name}-report.docx"

    soffice --headless \
      --convert-to pdf ${outputs}/${sample_id}_${output_name}-report.docx \
      --outdir ${outputs}

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                                --task-name-with-index ${task_name_with_index} \
                                                --task-version ${task_version} \
                                                --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: 1
        maxRetries: 2

    }

    output {

        File somatic_report_ang_pdf = "${outputs}/${sample_id}_${output_name}-report.pdf"
        File somatic_report_ang_docx = "${outputs}/${sample_id}_${output_name}-report.docx"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
