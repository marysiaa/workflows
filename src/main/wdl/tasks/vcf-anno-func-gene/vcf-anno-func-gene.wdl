workflow vcf_anno_func_gene_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "annotations", "HPO"]}'
    name: 'VCF annotation with HPO data'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Annotates VCF with variant level annotation from the HPO database'
    changes: '{"1.3.6": "ensembl-resources docker update", "1.3.5": "ad delimiter input", "1.3.4": "docker update", "1.3.3": "docker update (uniprot, not related)", "1.3.2": "docker update", "1.3.1": "Docker update, now files are from hpo-resources docker", "1.3.0": "HPO annotation only, resources updated", "1.2.2":"resources fix","1.2.1": "set-e -o pipefail fix", "1.2.0": "latest dir removed, resources update"}'

    input_vcf_gz: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"], "description": "Chromosome for analysis"}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID"}'

    output_hpo_annotated_vcf_gz : '{"name": "HPO annotated vcf", "type": "File", "description": "HPO annotated vcf"}'
    output_hpo_annotated_vcf_gz_tbi : '{"name": "Annotated vcf index","type": "File", "description": "Vcf index"}'

    output_stdout_log: '{"name": "Standard out", "type": "File",  "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File",  "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "description": "Biocompute object"}'
  }

  call vcf_anno_func_gene

}

task vcf_anno_func_gene {

  String task_name = "vcf_anno_func_gene"
  String task_version = "1.3.6"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-func-gene:1.3.4"


  # Input VCF file must: be bgzipped
  #                      have left-normalized indels (bcftools norm)
  #                      multiallelic sites split (bcftools norm -m -any)
  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename = "no_id_provided"

  String? chromosome
  Boolean chromosome_defined = defined(chromosome)
  String vcf_prefix = if chromosome_defined then chromosome + "_" + vcf_basename else vcf_basename

  
# Tools runtime settings, paths etc.
  String hpo_gene_to_pheno_dict = "`ls /resources/human-phenotype-ontology/*/gene-symbol-to-phenotypes.tsv`"
  String hpo_gene_to_inheritance_dict = "`ls /resources/human-phenotype-ontology/*/gene-symbol-to-modes-of-inheritance.tsv`"

  String genes_symbol_field_name = "ISEQ_GENES_NAMES"
  String hpo_gene_to_phenotypes_field_name = "ISEQ_HPO_PHENOTYPES"
  String hpo_gene_to_inheritance_field_name = "ISEQ_HPO_INHERITANCE"
  String hpo_version = "13-01-2022"
  String field_delimiter = ":"

  String hpo_gene_to_phenotypes_description = "Names of phenotypes associated with genes that overlap the allele (source: Human Phenotye Ontology (HPO)). Phenotypes associated with a given gene are '^' - delimited. Groups of phenotypes associated with genes are ':' - delimited. The order of those groups is the same as the order of genes in " + genes_symbol_field_name + " field."
  String hpo_gene_to_inheritance_description = "Modes of inheritance associated with genes that overlap the allele (source: Human Phenotye Ontology (HPO)). Modes of inheritance associated with a given gene are '^' - delimited. Groups of modes of inheritance associated with genes are ':' - delimited. The order of those groups is the same as the order of genes in " + genes_symbol_field_name + " field."

  String annotate_vcf_with_a_dictionary_py = "/intellisetools/annotate-vcf-with-a-dictionary.py"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # HPO - phenotypes
  HPO_PHENOTYPES_HEADER='##INFO=<ID=${hpo_gene_to_phenotypes_field_name},Number=A,Type=String,Description="'"${hpo_gene_to_phenotypes_description}"'",Source="HPO",Version="'"${hpo_version}"'">'

  # HPO - inheritance
  HPO_INHERITANCE_HEADER='##INFO=<ID=${hpo_gene_to_inheritance_field_name},Number=A,Type=String,Description="'"${hpo_gene_to_inheritance_description}"'",Source="HPO",Version="'"${hpo_version}"'">'

  zcat ${vcf_gz} \
      | python3 ${annotate_vcf_with_a_dictionary_py} \
           ${hpo_gene_to_pheno_dict} \
            ${genes_symbol_field_name}  \
            ${hpo_gene_to_phenotypes_field_name} \
            -k ${field_delimiter} \
            "$HPO_PHENOTYPES_HEADER" \
              | python3 ${annotate_vcf_with_a_dictionary_py} \
              ${hpo_gene_to_inheritance_dict} \
              ${genes_symbol_field_name}  \
              ${hpo_gene_to_inheritance_field_name} \
              -k ${field_delimiter} \
              "$HPO_INHERITANCE_HEADER" \
                | bgzip > ${vcf_prefix}_annotated-with-hpo.vcf.gz

  tabix -p vcf ${vcf_prefix}_annotated-with-hpo.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File hpo_annotated_vcf_gz = "${vcf_prefix}_annotated-with-hpo.vcf.gz"
    File hpo_annotated_vcf_gz_tbi = "${vcf_prefix}_annotated-with-hpo.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
