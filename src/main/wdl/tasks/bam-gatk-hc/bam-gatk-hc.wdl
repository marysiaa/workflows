workflow bam_gatk_hc_workflow {

  meta {
    keywords: '{"keywords": ["variant calling", "bam", "gvcf"]}'
    name: 'Gatk haplotype caller'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.2.6": "add dragstr_model file if alignment with dragmap", "1.2.5": "add dragen-mode", "1.2.4": "ignore soft clipped bases option", "1.2.3": "changed ram"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Identifier of sample"}'
    input_index: '{"name": "index", "type" :"Int", "description" : "Sequential number of the analyzed interval."}'
    input_recalibrated_markdup_bam: '{"name": "Recalibrated bam", "type": "File", "description": "Recalibrated bam file with marked duplicated reads."}'
    input_recalibrated_markdup_bai: '{"name": "Bam index", "type": "File", "description": "Index for the recalibrated bam file."}'
    input_interval_list: '{"name": "Inteval list", "type": "File", "description": "File with list of genome intervals to be analyzed"}'
    input_contamination: '{"name": "Contamination", "type": "Float", "description": "Fraction of contaminated reads to be filtered out."}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "description": "Reference genome, possible values; hg38, grch38-no-alt."}'

    output_gvcf_gz:'{"name": "Gvcf gz","type": "File", "description": "Bgzipped gVCF file, for one sample (and, if specified, for one genome interval)"}'
    output_gvcf_gz_tbi: '{ "name": "Gvcf index", "type": "File", "description": "gVCF file index"}'
    output_realigned_bam: '{"name": "Realigned bam", "type": "File", "description": "Realigned bam file, for one sample (and, if specified, for one genome interval"}'
    output_realigned_bai: '{"name": "Realigned bai", "type": "File", "description": "Index for the realigned bam file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bam_gatk_hc
}

task bam_gatk_hc {

  String task_name = "bam_gatk_hc"
  String task_version = "1.2.6"
  String reference_genome = "hg38"
  Int? index
  String index_out = if defined(index) then index + "-"  else ""
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.4.1-" +reference_genome+ ":1.0.0"
  File recalibrated_markdup_bam
  File recalibrated_markdup_bai
  String sample_id = "no_id_provided"
  File interval_list
  Float contamination = 0
  Boolean ignore_soft_clips = true
  # dragen-mode
  File? dragstr_model
  Boolean dragen_mode = true

  String java_mem = "-Xms4g -Xmx63g"
  String java_opt = select_first(["-XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10"])

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  ref_genome=$(ls /resources/*/*/*.fa)

  gatk --java-options "${java_mem} ${java_opt}" \
  HaplotypeCaller \
    -R $ref_genome \
    -I ${recalibrated_markdup_bam} \
    -L ${interval_list} \
    -O ${index_out}${sample_id}.g.vcf.gz \
    -ERC GVCF \
    --bam-output ${index_out}${sample_id}_realigned-haplotypecaller.bam \
    ${true="--dont-use-soft-clipped-bases" false="" ignore_soft_clips} \
    -contamination ${contamination} \
    ${if defined(dragstr_model) then "--dragstr-params-path " + dragstr_model else ""} \
    ${true="--dragen-mode" false="" dragen_mode}
    
  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "64G"
    cpu: "8"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File gvcf_gz = "${index_out}${sample_id}.g.vcf.gz"
    File gvcf_gz_tbi = "${index_out}${sample_id}.g.vcf.gz.tbi"
    File realigned_bam = "${index_out}${sample_id}_realigned-haplotypecaller.bam"
    File realigned_bai = "${index_out}${sample_id}_realigned-haplotypecaller.bai"
  }

}
