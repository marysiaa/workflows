workflow rna_seq_bam_metrics_workflow
{
  call rna_seq_bam_metrics
}

task rna_seq_bam_metrics {

  File genome
  File bam
  File bai
  String sample_id = basename(bam, ".bam")

  String task_name = "rna_seq_bam_metrics"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      set -e -o pipefail

      gatk CollectAlignmentSummaryMetrics \
        R=${genome} \
        I=${bam} \
        O=${sample_id}.txt

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File bam_metrics = "${sample_id}.txt"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
