workflow meta_test_linked_inputs_workflow {

    call meta_test_linked_inputs

}

task meta_test_linked_inputs {

    String? input1
    Int? input2
    Int? input3
    Int? input4
    Array[Int]? input5
  
    String task_name = "meta_test_linked_inputs"
    String task_version = "1.0.8"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.6"

    command <<<
        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


        bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
        --task-name-with-index ${task_name_with_index} \
        --task-version ${task_version} \
        --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: "1"
        maxRetries: 2

    }

    output {

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
