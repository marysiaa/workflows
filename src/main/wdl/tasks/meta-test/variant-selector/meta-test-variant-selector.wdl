workflow meta_test_variant_selector_workflow
{
  call meta_test_variant_selector
}

task meta_test_variant_selector {

  File selected_json
  File? sample_info_json
  String genome_or_exome = "exome"
  String analysis_group = "germline"
  String sample_id = "no_id_provided"

  String task_name = "meta_test_variant_selector"
  String task_version = "1.0.8"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.5"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    echo "SAMPLE_INFO_JSON:"
    cat ${sample_info_json}

    # cat file to output_file
    cat ${selected_json} > ${sample_id}.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File final_json = "${sample_id}.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
