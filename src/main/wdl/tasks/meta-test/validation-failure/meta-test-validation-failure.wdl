workflow meta_test_validation_failure_workflow {

    call meta_test_validation_failure

}

task meta_test_validation_failure {

    String? sample_id
    File? file

    String task_name = "meta_test_validation_failure"
    String task_version = "1.0.1"

    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.4"

    command <<<

        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

        bash /intelliseqtools/bco-before-finish.sh  --task-name ${task_name} \
                                                    --task-name-with-index ${task_name_with_index} \
                                                    --task-version ${task_version} \
                                                    --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: 1
        maxRetries: 2

    }

    output {

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }
}
