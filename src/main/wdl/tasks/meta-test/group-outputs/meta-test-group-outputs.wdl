workflow meta_test_group_outputs_workflow {

    call meta_test_group_outputs

}

task meta_test_group_outputs {

    String? input1
    String? input2

    String task_name = "meta_test_group_outputs"
    String task_version = "1.0.1"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.6"

    command <<<
        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

            echo ${input1} > test.txt

        bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
        --task-name-with-index ${task_name_with_index} \
        --task-version ${task_version} \
        --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: "1"
        maxRetries: 2

    }

    output {

        File test_txt = "test.txt"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
