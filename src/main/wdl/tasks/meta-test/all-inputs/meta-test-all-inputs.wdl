workflow meta_test_all_inputs_workflow {

    call meta_test_all_inputs

}

task meta_test_all_inputs {

    String? input1
    String? input2
    String? input3
    String? input4
    String? input5
    String? input6
    String? input7
    String? input8
    String? input9

    Array[String]? input10
    Array[String]? input11
    Array[String]? input12
    Array[String]? input13
    Array[String]? input14
    Array[String]? input15
    Array[String]? input16

    File? input17
    File? input18
    File? input19

    Array[File]? input20
    Array[File]? input21
    Array[File]? input22

    Boolean? input23
    Boolean? input24

    Float? input25
    Float? input26
    Float? input27
    Float? input28

    Array[Float]? input29
    Array[Float]? input30
    Array[Float]? input31

    Int? input32
    Int? input33
    Int? input34
    Int? input35
    Int? input36
    Int? input37

    Array[Int]? input38
    Array[Int]? input39
    Array[Int]? input40
    Array[Int]? input41

    # Inputs un group 1
    String? input1_in_group1
    String? input2_in_group1
    String? input3_in_group1

    # Inputs un group 1
    String? input1_in_group2
    String? input2_in_group2
    String? input3_in_group2
    String? input4_in_group2

    String task_name = "meta_test_all_inputs"
    String task_version = "1.0.0"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.6"

    command <<<
        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


        bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
        --task-name-with-index ${task_name_with_index} \
        --task-version ${task_version} \
        --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: "1"
        maxRetries: 2

    }

    output {

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
