workflow genotype_gvcf_position_workflow {

  meta {
    name: 'genotype_gvcf_position'
    author: 'https://gitlab.com/mremre'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## genotype_gvcf_position \n Generic text for task'
    changes: '{latest: "no changes"}'
  }

  call genotype_gvcf_position

}

task genotype_gvcf_position {

  # @Input(description="", required="false")
    String task_name = "genotype_gvcf_position"
    String task_version = "latest"
    String docker_image = "intelliseqngs/gatk:4.1.2.0_hg38_v1.0"


    String sample_id
    Float? contamination
    File gvcf_z_haplotype
    File gvcf_z_haplotype_index

    String? interval_list
    String intervals_arg = select_first([interval_list, ""])

    String? interval_padding
    Boolean interval_padding_defined = defined(interval_padding)
    String interval_padding_arg = if interval_padding_defined then "--interval-padding " + interval_padding else ""


    String gatk_path = "/gatk/gatk"
    String java_mem = "-Xms3000m"
    String java_opt = select_first(["-XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10"])


  command <<<
  task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
  #source <(curl -s https://gitlab.com/intelliseq/workflows/raw/master/src/main/scripts/bco/v1/after-start.sh)

  set -e

  ${gatk_path} --java-options "${java_mem} ${java_opt}" \
    GenotypeGVCFs ${intervals_arg} ${interval_padding_arg} \
    --include-non-variant-sites \
    -V  ${gvcf_z_haplotype} \
    -R /resources/reference-genome/Homo_sapiens_assembly38.fa \
    --lenient \
    -O ${sample_id}.vcf.gz  2>&1 | tee genotype-gvcf.stdout.stderr.log

    JAVA_VER=`java -version 2>&1 | awk '/openjdk version/ {print $3}' | tr -d '"'`
    JAVA=`echo '"java_openjdk":"'$JAVA_VER',GPLv2"'`
    CONDA_VER=`conda info | awk '/conda version/ {print $4}'`
    CONDA=`echo '"miniconda":"'$CONDA_VER',BSD 3-Clause"'`
    UBUNTU_VER=`awk '/PRETTY_NAME/ {print $2}' /etc/*release`
    UBUNTU=`echo '"ubuntu":"'$UBUNTU_VER',GPL"'`
    REFGEN=`echo '"reference_genome":"GRCh38"'`

    echo "\""${task_name}"\""":{\"task-version\":\""${task_version}"\","$JAVA","$CONDA","$UBUNTU","$REFGEN"}" > task-info.json


  #source <(curl -s https://gitlab.com/intelliseq/workflows/raw/master/src/main/scripts/bco/v1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "500M"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File gvcf_gz = "${sample_id}.vcf.gz"
    File gvcf_gz_tbi = "${sample_id}.vcf.gz.tbi"

    # @Output(keep=true, outdir="/genotype_gvcf_position", filename="stdout.log", description="logs")
    File stdout_log = stdout()
    # @Output(keep=true, outdir="/genotype_gvcf_position", filename="stderr.log")
    File stderr_log = stderr()
    # @Output(keep=true, outdir="/genotype_gvcf_position", description="")
    #File bco = "bco.json"

  }

}
