workflow pgx_merge_genotypes_workflow {

  meta {
    keywords: '{"keywords": ["pgx", "pharmacogenomics", "CYP2D6", "CYP2C19", "PharmGKB", "CPIC", "PharmVar", "PharmCat", "Stargazer"]}'
    name: 'pgx_merge_genotypes'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Merge genotyping data from pgx tools: astrolabe, pharmcat, aldy (stargazer implemented in python3), cyrius (does not work well but is here to test if will work at all in some case...). Data is returned in tsv and json format.'
    changes: '{"1.0.0": "First version: merge data from 4 tools"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_astrolabe_tsv: '{"name": "astrolabe_tsv", "extension": [".tsv"], type": "File", "description": "Output from pgx tool: astrolabe"}'
  input_aldy_tsv: '{"name": "aldy_tsv", "extension": [".tsv"], "type": "File", "description": "Output from pgx tool: aldy"}'
  input_pharmcat_json: '{"name": "pharmcat_json", "extension": [".json"], "type": "File", "description": "Output from pgx tool: PharmCat"}'
  input_cyrius_tsv: '{"name": "cyrius_tsv", "extension": [".tsv"],  "type": "File", "description": "Output from pgx tool: Cyrius from Illumina"}'
  
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call pgx_merge_genotypes

}

task pgx_merge_genotypes {

  String task_name = "pgx_merge_genotypes"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-merge-genotypes:1.2.0"

  String sample_id = "no_id_provided"
  File? astrolabe_tsv
  File? aldy_tsv
  File? pharmcat_json
  File? cyrius_tsv

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  python3 /intelliseqtools/pgx-merge-genotypes.py \
    ${"-c " + cyrius_tsv} \
    ${"-p " + pharmcat_json} \
    ${"-s " + astrolabe_tsv} \
    ${"-l " + aldy_tsv} \
    -i ${sample_id}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File tsv = "${sample_id}_merged-diplotypes.tsv"
    File json = "${sample_id}_merged-diplotypes.json"

  }

}
