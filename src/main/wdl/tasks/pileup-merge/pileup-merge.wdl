workflow pileup_merge_workflow
{
  call pileup_merge
}

task pileup_merge {

  String task_name = "pileup_merge"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pileup-merge:1.0.1"

  String sample_id = "no_id_provided"
  Array[File] pileups
  String java_mem = "-Xmx3500m"
  String ref_dict_path = "/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.dict"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   gatk --java-options "${java_mem}" GatherPileupSummaries \
        --sequence-dictionary ${ref_dict_path} \
        -I ${sep=' -I ' pileups} \
        -O ${sample_id}_merged.tsv


   ## check if data lines present
   if grep -q '^chr' ${sample_id}_merged.tsv;then
       echo "true" > non-empty-pileup.txt
   else
       echo "false" > non-empty-pileup.txt
   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File merged_table = "${sample_id}_merged.tsv"
    Boolean non_empty_pileup = read_boolean("non-empty-pileup.txt")
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
