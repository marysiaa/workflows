workflow sv_report_workflow {

  meta {
    keywords: '{"keywords": ["SV", "report"]}'
    name: 'SV report'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Creates structural variants report'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "Sample identifier"}'
    input_sv_tsv: '{"name": "SV table", "type": "File", "description": "Table with sv calling results"}'
    input_sv_json: '{"name": "SV json", "type": "File", "description": "Json with sv calling results"}'
    input_sample_sex: '{"name": "Sample sex", "values": ["M","F","U"], "type": "String", "description": "Sample sex"}'

    output_report_pdf: '{"name": "Report pdf", "type": "File", "copy": "True", "description": "SV report in pdf format"}'
    output_report_html: '{"name": "Report html", "type": "File", "copy": "True", "description": "SV report in html format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_report

}

task sv_report {

  String task_name = "sv_report"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_sv-report:1.0.0"
  ##ubuntu-minimal-20.04:3.0.3"

  File sv_table
  String sample_sex = "F"
  String sample_id = "no-id"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   ## Plot

   # 1. find GT field name
   gt=$( head -1 ${sv_table} | sed 's/\t/\n/g' | grep  '^GT__' )

   # 2. Prepare dataframe for R: extract columns, leave only normal chromosomes, modify GT field and SVLEN field
    cat ${sv_table} | awk -F'\t' -vcols=CHROM,POS,SVTYPE,END,"$gt",SVLEN \
    '(NR==1){n=split(cols,cs,",");for(c=1;c<=n;c++){for(i=1;i<=NF;i++)if($(i)==cs[c])ci[c]=i}}{for(i=1;i<=n;i++)printf "%s" FS,$(ci[i]);printf "\n"}' \
    | awk '{gsub(/\(|\)|\,/,"")}1' \
    | awk -F '\t' -v chroms=CHROM,chr1,chr2,chr3,chr4,chr5,chr6,chr7,chr8,chr9,chr10,chr11,chr12,chr13,chr14,chr15,chr16,chr17,chr18,chr19,chr20,chr21,chr22,chrX,chrY \
    '{n=split(chroms,cs,",");for(c=1;c<=n;c++) if ($1==cs[c]) {print $0}}' \
    | awk -F '\t' '{if($5~"0" && $5!~"GT") {$5=0.5} else if( $5!~"GT") {$5=1} else {$5="GT"} {print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}}' \
    |  awk -F '\t' -v sex=${sample_sex} '{if (sex=="M" && ($1=="chrX" || $1=="chrY")) {$5=0.5}{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}} '  > sv.df

   # 3. Create plot

   Rscript /intelliseqtools/sv-plot.R --input sv.df --output sv.svg --sex ${sample_sex}

   # 4. Create picture json
   echo "[" > picture.json
   picturepath=$( realpath sv.svg )
   echo "{\"path\" : \"$picturepath\"}">> picture.json
   echo "]" >> picture.json

   # 5. Create empty json (generate-report script needs it)
   echo '[]' > empty.json

   # 6. Create report
   /intelliseqtools/generate-report.sh \
   --template /intelliseqtools/templates/sv-v1/content.jinja \
   --name ${sample_id}_sv-report \
   --pictures picture.json \
   --json empty=empty.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File report_pdf = "${sample_id}_sv-report.pdf"
    File report_html = "${sample_id}_sv-report.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
