workflow pharmgkb_get_annotations_workflow
{
  call pharmgkb_get_annotations
}

task pharmgkb_get_annotations {

  File genotypes
  String sample_id = "no_id_provided"

  String task_name = "pharmgkb_get_annotations"
  String task_version = "1.0.4"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pharmgkb-get-annotations:1.0.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # create PharmGKB database
    create_database

    # Get clinical annotations
    get_clinical_annotation --queries ${genotypes} --output-filename ${sample_id}-clinical_annotations.json

    # Get variant annotations
    get_variant_annotation --queries ${genotypes} --output-filename ${sample_id}-variant_annotations.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File clinical_annotations = "${sample_id}-clinical_annotations.json"
    File variant_annotations = "${sample_id}-variant_annotations.json"
    File variants_with_unknown_genotype = "variants_with_unknown_genotype.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}