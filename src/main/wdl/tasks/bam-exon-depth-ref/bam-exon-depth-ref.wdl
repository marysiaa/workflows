workflow bam_exon_depth_ref_workflow
{
  call bam_exon_depth_ref
}

task bam_exon_depth_ref {

  String task_name = "bam_exon_depth_ref"
  String task_version = "1.0.4"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-exon-depth:1.0.4"

  File? custom_bed  ## must be exactly the same, as bed that will be used in exome cnv analyses
  String? kit = "exome-v8" # Possible values "exome-v6", "exome-v7", "exome-v8"
  String exome_bed = if (defined(kit) && !defined(custom_bed)) then "/resources/" + kit + "/covered.bed" else ""
  Array[File] bams
  Array[File] bais

  String batch_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   ln -s ${sep=" " bams} .
   ln -s ${sep=" " bais} .

   Rscript /intelliseqtools/exon-depth.R \
      --exon-bed ${exome_bed} ${custom_bed} \
      --bam-dir . \
      --prepare-ref \
      --outfile ${batch_id}_exome-depth-reference.tsv


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 2
    maxRetries: 2

  }

  output {

    File cnv_reference_counts = "${batch_id}_exome-depth-reference.tsv"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
