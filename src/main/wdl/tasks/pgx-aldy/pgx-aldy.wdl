workflow pgx_aldy_workflow {

  meta {
    name: 'pgx_aldy'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    changes: '{"1.1.3":"update aldy in docker to 3.3", "1.1.2":"update aldy in docker - fix for cyp1a2", "1.1.1": "Nomenclature", "1.1.0": "New genes in aldy", "1.0.0": "no changes"}'
    input_bam: '{"name": "bam", "type": "File", "extension": [".bam"], "description": "Bam file, mardup.recalibrated"}'
   input_bai: '{"name": "bai", "type": "File", "extension": [".bai"], "description": "Index for bam file"}'
  }

  call pgx_aldy

}

task pgx_aldy {

  String task_name = "pgx_aldy"
  String task_version = "1.1.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-aldy:1.1.0"

  File bam
  File bai
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   pgx_genes="CYP2D6 CYP2A6 CYP2C19 CYP2C8 CYP2C9 CYP3A4 CYP3A5 CYP4F2 TPMT DPYD SLCO1B1 CYP2J2 CYP2W1 CYP3A7 CYP3A43 CYP2E1 CYP2R1 NUDT15 CYP1A1 CYP1A2 CYP4F2 CYP2B6 CYP2A13 CYP2F1 CYP2S1 G6PD"
   for i in $pgx_genes
    do 
      aldy genotype -p illumina \
      -g $i \
      -o $i.aldy \
      -l $i.log \
      --debug DEBUG \
      ${bam}  >> out.txt
      cat $i.aldy >> out.aldy
      cat $i.log >> out.log
    done

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File out_txt = "out.txt"
    File out_aldy = "out.aldy"
    File out_log = "out.log"
  }

}
