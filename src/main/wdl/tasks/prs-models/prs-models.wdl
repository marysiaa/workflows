workflow prs_models_workflow
{
  call prs_models
}

task prs_models {

  File? sample_info
  String analysisRun = "IntelliseqFlow"

  String task_name = "prs_models"
  String task_version = "1.0.6"
  Int? index
  String model_type = "health"

  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = if analysisRun == "Vitalleo" then "intelliseqngs/task_prs-vitalleo-models:1.0.0" else "intelliseqngs/task_prs-models:1.0.5"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir outputs
    cp -r /models/${model_type}-*/*.yml outputs/.

    if [ "${analysisRun}" != "Vitalleo" ]; then
        if [ "${model_type}" == "wellness" ]; then
            if test -f "${sample_info}"; then
                is_male=$(jq '.sex == "male"' ${sample_info})
                if ! $is_male; then
                    rm outputs/ukb-2395-pattern-balding.yml
                fi
            else
                rm outputs/ukb-2395-pattern-balding.yml
            fi
        fi
    fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
    Array[File] yml_models = glob("outputs/*.yml")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
