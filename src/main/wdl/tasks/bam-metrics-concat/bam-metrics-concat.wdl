workflow bam_metrics_concat_workflow {

  meta {
    keywords: '{"keywords": ["bam quality check", "pickard", "gatk", "coverage", "sequencing depth"]}'
    name: 'bam_metrics_concat'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Collects simple-final-csv files created in bam-metrics and concatenates them respectively to chromosomes'
    changes: '{"1.2.0": "kit_json is in docker container and as optional input, prevoius versions not documented in changes", "0.0.8": "no changes"}'

    input_final_json: '{"name": "input_final_json", "type": "String", "description": "Array of json files with results of pickard quality check analysis, each json is for one chromosome."}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    output_metrics_json: '{"name": "metrics json", "type": "String", "description": "Json with all metrics concatenated and caluclated, with additinale metadata neede to create quality-check report."}'
  }

  call bam_metrics_concat

}

task bam_metrics_concat {

  String task_name = "bam_metrics_concat"
  String task_version = "1.0.0"
  Int? index
  String sample_id = "no_id_provided"
  String? kit = "exome-v7"# genome, exome-v7, exome-v6, target
  File? kit_json
  Boolean kit_json_defined = defined(kit_json)

  Array[File] final_json

  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-metrics-concat:1.0.0"

  command <<<

  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if ${kit_json_defined}; then
    kit_json=${kit_json}
  else
    kit_json="/${kit}/${kit}.json"
  fi

    python3 /intelliseqtools/bam-metrics-concat.py  \
      ${sep=' ' prefix('--final_json ', final_json)} \
      --kit_json $kit_json \
      --output_name "${sample_id}_metrics.json"                                           
            
    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                                --task-name-with-index \ ${task_name_with_index} \
                                                --task-version ${task_version} \
                                                --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File metrics_json = "${sample_id}_metrics.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
