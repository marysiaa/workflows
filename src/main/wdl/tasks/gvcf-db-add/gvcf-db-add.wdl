workflow gvcf_db_add_workflow {

  meta {
    keywords: '{"keywords": ["genomics db", "database", "gvcf"]}'
    name: 'gvcf_db_add'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Create genomics.db from gvcfs or add gvcf(s) to existing genomics.db'
    changes: '{"1.0.2": "add inputs memory and java-options","1.0.1": "update meta"}'

    input_gvcf_gz:'{"name": "Gvcfs gz", "type": "Array[File]", "extension": [".g.vcf.gz", ".gvcf.gz"], "description": "Array of gVCF files"}'
    input_gvcf_gz_tbi: '{ "name": "Gvcfs indexes", "type": "Array[File]", "extension": [".g.vcf.gz.tbi", ".gvcf.gz.tbi"], "description": "Array of gVCF files indexes"}'
    input_genomics_db_tar:'{"name": "GenomicsDB tar folder","type": "File", "description": "Existing GenomicsDB datastore. Provide to add gvcfs to this GenomicsDB."}'
    input_new_genomics_db_name:'{"name": "Name of new GenomicsDB","type": "String", "description": "Fill only when creating new database."}'

    input_interval_file:  '{"name": "Interval file for GenomicsDB", "type": "File", "description": "GenomicsDB will be created for intervals provided in interval file. Exactly one of two inputsmust be provided: chromosome or interval_file. WARNING: if using with resources_kit task use version 1.0.8 or newer."}'
    input_chromosome:  '{"name": "Chromosome interval for GenomicsDB", "type": "String", "description": "GenomicsDB will be created for chromosome or interval provided. Use whole chromosome (e.g. chr1) or part of it (e.g. chr1:372-253399). Exactly one of two inputs must be provided: chromosome or interval_file."}'

    output_genomics_db: '{"name": "Output GenomicsDB tar folder", "type": "File", "copy": "True", "description": "GenomicsDB created from input gvcfs and/or input GenomicsDB"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call gvcf_db_add

}

task gvcf_db_add {

  Array[File] gvcf_gz
  Array[File] gvcf_gz_tbi

  File? genomics_db_tar
  Boolean is_genomics_db_tar_provided = defined(genomics_db_tar)
  String? new_genomics_db_name
  String filename = if is_genomics_db_tar_provided then basename(genomics_db_tar, ".tar") else if defined(new_genomics_db_name) then new_genomics_db_name else "GenomicsDB"

  String? chromosome
  File? interval_file
  Boolean is_chromosome_provided = defined(chromosome)

  String memory = "9G"
  String java_options = "-Xms8g -Xmx8g"

  String task_name = "gvcf_db_add"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0:1.0.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if ${is_genomics_db_tar_provided}
  then
      tar -xf ${genomics_db_tar}
      gatk --java-options "${java_options}" \
      GenomicsDBImport \
        -V ${sep=' -V ' gvcf_gz} \
        --genomicsdb-update-workspace-path ${filename} \
        --reader-threads 5 \
        --merge-input-intervals

  elif ${is_chromosome_provided}
  then
      gatk --java-options "${java_options}" \
      GenomicsDBImport \
        --intervals ${chromosome} \
        -V ${sep=' -V ' gvcf_gz} \
        --genomicsdb-workspace-path ${filename}\
        --reader-threads 5 \
        --merge-input-intervals

  else
      gatk --java-options "${java_options}" \
      GenomicsDBImport \
        --intervals ${interval_file} \
        -V ${sep=' -V ' gvcf_gz} \
        --genomicsdb-workspace-path ${filename}\
        --reader-threads 5 \
        --merge-input-intervals
  fi


  tar -cf ${filename}.tar ${filename}


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: memory
    cpu: "4"
    maxRetries: 2

  }

  output {

    File genomics_db = "${filename}.tar"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
