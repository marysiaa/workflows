workflow sv_cov_plot_workflow
{
  call sv_cov_plot
}

task sv_cov_plot {

  String task_name = "sv_cov_plot"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_sv-cov-plot:1.0.0"

  File vcf
  File tbi
  File bam
  File bai
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    ## prepare variant list
    bcftools query -i 'INFO/SVTYPE ="DEL,DUP"'  ${vcf}  -f '%CHROM\t%POS\t%INFO/END\t%ID\t%INFO/SVTYPE\n' \
    | awk '{len=int(0.3 *($3 - $2)); if(len > 500) {$6 = $2 - len; $7 = $3 + len} else {$6 = $2 - 500; $7 = $3 + 500} {print $4"_"$5"\t"$1":"$6"-"$7"\t"$2"\t"$3}}' \
    > variants-ranges-list.tsv

    ## check if variant list is not empty
    if grep -q '^'; then
        ## create plots

        while read f; do
            range=$( echo $f | cut -f2 -d ' ')
            name=$(echo $f | cut -f1 -d ' ')
            samtools depth -a ${bam} -r $range > $name-depth.tsv
            variant_start=$(echo $f | cut -f3 -d ' ')
            variant_end=$(echo $f | cut -f4 -d ' ')
            Rscript /intelliseqtools/coverage-plot.R \
                --depth $name-depth.tsv \
                --variant_start $variant_start \
                --variant_end $variant_end \
                --outfile ${sample_id}_"$name".svg
            done<variants-ranges-list.tsv

       ## prepare tar archive
       tar -zcvf ${sample_id}_coverage-plots.tar.gz ${sample_id}*.svg

   else
       touch ${sample_id}_coverage-plots.tar.gz
   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File coverage_plots = "${sample_id}_coverage-plots.tar.gz"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
