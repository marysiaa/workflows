workflow igv_screenshots_workflow {

  meta {
    keywords: '{"keywords": ["IGV"]}'
    name: 'IGV screenshots'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Generates igv pictures'
    changes: '{"3.2.2": "bug fix", "3.2.1": "report name change", "3.2.0": "ReadGroups in bams replaced to filename with AddOrReplaceReadGroups Gatk tool; bams_bais input deleted", "3.1.0": "jigv update to 0.1.8 (no more server needed); html with all positions as output", "3.0.1": "fixed track height", "3.0.0": "changed optional/required inputs and outputs", "2.0.0": "Images are now created with jigv", "1.3.3": "IGV version updated", "1.3.0": "Waiting time as argument","1.2.0": "optional outputs ad arrays"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Identifier of sample"}'
    input_positions_list: '{"name": "Position list", "type": "File",  "description": "Output from report_variants_from_vcf or sv_breaks_pos tasks"}'
    input_bams: '{"name": "Other bams", "type": "Array[File]",  "extension": [".bam"], "description": "Bams files used for igv-screenshots comparison"}'
    input_softclip: '{"name": "Soft clip?", "default": "false", "type": "Boolean", "description": "Decides whether show soft clipped bases"}'
    input_range: '{"name": "Range", "default": "1000", "type": "Int", "description": "Determines width of the variant surrounding genomic region that will be shown on picture (in bp)"}'
    input_picture_height: '{"name": "Height", "default": "1280", "type": "Int", "description": "Determines picture height"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "true", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "true", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "true", "description": "Biocompute object"}'
    output_igv_pngs: '{"name": "Igv screenshots images", "type": "File", "description": "Set of images with igv screenshots generated for a list of alignment positions"}'
    output_igv_screenshots: '{"name": "Igv screenshots", "type": "File", "description": "Set of html files with igv screenshots generated for a list of alignment positions."}'
  }


  call igv_screenshots

}

task igv_screenshots {

  String task_name = "igv_screenshots"
  String task_version = "3.2.2"
  File positions_list
  Array[File] bams = []

  String sample_id = "no_id_provided"
  Int range = 1000
  Boolean softclip = false
  Boolean view_as_pairs = false
  Int picture_width = 1200
  Int track_height = 550
  Int base_height = 250
  String dollar = "$"
  Int max_picture_processing_time_in_ms = 20000

  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/igv:3.0.1"

  # determine number of tracks in the result picture
  Int num_of_tracks = length(bams)

  command <<<
    # JIGV documentation https://github.com/brentp/jigv#automated-screenshots
    # chromium CLI switches (I was not able to find documentation for google chrome but this one seems relevant): https://peter.sh/experiments/chromium-command-line-switches/
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    function create_index_if_not_present {
      # create indices if not present. If present, rename to *bam.bai (JIGV needs this kind of name)
      bai_path=`echo $1 | sed "s/.$/i/"`
      if [[ -f $bai_path ]]; then
        mv $bai_path $1.bai
      else
        echo "can't find $bai_path, creating index for $1"
        samtools index $1 $1.bai
      fi
    }
    export -f create_index_if_not_present

    n=$( cat ${positions_list} | wc -l )

    # check if position list is not empty
    if [ $n -gt 0 ];then
      # due to an error in JIGV, related to the number of `sample_names` in the BAM header (additional sample_name from HC),
      # replace all Read-Group sample name to SM:file_name
      for bam in ${sep=' ' bams}; do
       file_name=$(basename $bam)
       java -jar /usr/bin/picard.jar AddOrReplaceReadGroups \
         I=$bam \
         O=OUTPUT.bam \
         RGID=4 \
         RGLB=lib1 \
         RGPL=ILLUMINA \
         RGPU=unit1 \
         RGSM=$file_name \
         VALIDATION_STRINGENCY=SILENT
        mv OUTPUT.bam $bam
      done

      set -e -o pipefail
      # deal with indices
      for other_bam in ${sep=' ' bams}; do
        create_index_if_not_present $other_bam
      done

      # change {positions_list} txt file to .bed
      sort ${positions_list} | uniq | awk -v OFS="\t" '{print $1,($2 - ${range} / 2),($2 + ${range} / 2),$3}' > list_of_positions.bed

      # create html
      nohup jigv -g hg38 --sites list_of_positions.bed ${sep=' ' bams} > ${sample_id}_main-report-igv-variants.html
      path_to_html=$(realpath ${sample_id}_main-report-igv-variants.html)

      # make picture for each position
      picture_height=`expr ${base_height} + ${track_height} \* ${num_of_tracks}`
      sort ${positions_list} | uniq | \
      while read -r line; do
        words=( $line );
        chrom="${dollar}{words[0]}";
        position="${dollar}{words[1]}"
        name="${dollar}{words[2]}"
        floor=`expr 1 + $position - ${range} / 2`
        ceil=`expr $position + ${range} / 2`
        region=$chrom:$floor-$ceil

        # try to create the picture in a given time. When it fails, increase the time by 5 seconds and try again
        wait_time=${dollar}((${max_picture_processing_time_in_ms}/1000+5))
        exit_code=1
        echo "creating picture for region $region"
        set +e # exiting immediatedly after error temporarily turned off
        retries=0
        while (( exit_code != 0 && retries < 3 )); do
          echo "timeout for a picture set to $wait_time seconds"
          timeout $wait_time bash -c "google-chrome --window-size=${picture_width},$picture_height \
                                                    --virtual-time-budget=${max_picture_processing_time_in_ms} \
                                                    --headless \
                                                    --disable-gpu \
                                                    --run-all-compositor-stages-before-draw \
                                                    --screenshot=${sample_id}_${dollar}{chrom}-${dollar}{position}_$name-igv-screenshot.png \
                                                    --no-sandbox \
                                                    'file://$path_to_html#$region'"
          exit_code=$?
          ((wait_time+=10))
          ((retries++))
        done
        set -e # exiting immediatedly after error turned on again
        echo "created screenshot for region: $region"
      done

      echo "Created screenshots:"
      ls ${sample_id}*-igv-screenshot.png

      echo "Creating archive:"
      tar -zcvf ${sample_id}_igv-images.tar.gz ${sample_id}*-igv-screenshot.png

    fi

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    # This is trick to allow for optional outputs in gcloud
    Array[File] compressed_igv_pngs = glob("${sample_id}_igv-images.tar.gz")
    Array[File] jigv_html = glob("${sample_id}_main-report-igv-variants.html")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}