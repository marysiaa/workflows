workflow imputing_quality_workflow
{
  call imputing_quality
}

task imputing_quality {

  String task_name = "imputing_quality"
  String task_version = "0.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_imputing-quality:0.0.2"

  File imputed_vcf # sorted vcf
  File initial_vcf # sorted vcf

  command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # WARNING! this eliminates indels from the intersection
    # IMP field in vcf produced by beagle means that the marker was not transfered from the input file (in this case
    # output of eagle), but imputed by beagle. If the imputed file was produced by beagle from initial (unphased) file,
    # we could simply grep for IMP in imputed_vcf

    if echo ${initial_vcf} | grep -q '\.gz$';then
        tabix -p vcf ${initial_vcf}
    fi

    if echo ${imputed_vcf} | grep -q '\.gz$';then
        tabix -p vcf ${imputed_vcf}
    fi

    bcftools filter -i 'FORMAT/DP[0-] > 9'  -S . ${initial_vcf} -o high-cov-initial.vcf.gz -O z
    bcftools filter -i 'INFO/IMP = 1' ${imputed_vcf} -o imputed-sites.vcf.gz -O z

    zgrep "^#CH" imputed-sites.vcf.gz -m1 > intersection
    zgrep "^#CH" high-cov-initial.vcf.gz -m1 >> intersection

    bedtools intersect \
                      -a high-cov-initial.vcf.gz \
                      -b imputed-sites.vcf.gz \
                      -sorted \
                      -wa \
                      -wb \
                      -r \
                      -f 1 >> intersection

    # the intersection file consists of lines merged from both files. Example:
    # #CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  415
    # #CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  415
    # 19      200352  rs532817504     G       A       .       PASS    DR2=0.00;AF=0.0000;IMP  GT:DS   0|0:0   19      200352  rs532817504     G       A,C     .       PASS    RS_INFO=rs532817504:G:A^C;DP=5  GT:DP:RGQ 0/0:5:15
    python /intelliseqtools/imputing_quality.py -i intersection -o quality_info.json

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File quality_info = "quality_info.json"

  }

}
