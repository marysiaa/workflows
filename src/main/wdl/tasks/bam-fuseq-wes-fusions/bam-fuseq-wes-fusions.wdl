workflow bam_fuseq_wes_fusions_workflow
{
  call bam_fuseq_wes_fusions
}

task bam_fuseq_wes_fusions {

  String task_name = "bam_fuseq_wes_fusions"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/fuseq-wes:1.1.0"

  String sample_id = "no_id_provided"
  String ensembl_release = "107"
  String fuseq_wes_dir = "/tools/fuseq-wes/v1.0.0/FuSeq_WES_v1.0.0/"
  File bam 
  File bai

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  gtf=`ls /resources/grch38-ensembl${ensembl_release}-wes-r150.json`
  sqlite=`ls /resources/grch38-ensembl${ensembl_release}-wes-r150.sqlite`

  mkdir outputs

    

  # extract split and discordanr reads
  python3 ${fuseq_wes_dir}/fuseq_wes.py \
      --bam ${bam} \
      --gtf $gtf \
      --mapq-filter \
      --outdir outputs

  # find fusions
  Rscript ${fuseq_wes_dir}/process_fuseq_wes.R \
      in=outputs \
      sqlite=$sqlite\
      fusiondb=${fuseq_wes_dir}/Data/Mitelman_fusiondb.RData \
      out=outputs \
      paralogdb=/resources/ensmbl_paralogs_grch38.RData 

      
          
  for i in outputs/FuSeq_WES*;do
      name=$(basename $i | sed 's/[A-Z]/\L&/g' | sed 's/_/-/g' | sed 's/final//' | sed 's/txt$/tsv/')
      mv $i outputs/${sample_id}_"$name"
  done

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File fusion_tsv = "outputs/${sample_id}_fuseq-wes-fusion.tsv"
    File fusion_mr_fge_tsv = "outputs/${sample_id}_fuseq-wes-mr-fge.tsv"
    File fusion_mr_fge_fdb_tsv = "outputs/${sample_id}_fuseq-wes-mr-fge-fdb.tsv"
    File fusion_sr_fge_tsv = "outputs/${sample_id}_fuseq-wes-sr-fge.tsv"
    File fusion_sr_fge_fdb_tsv = "outputs/${sample_id}_fuseq-wes-sr-fge-fdb.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
