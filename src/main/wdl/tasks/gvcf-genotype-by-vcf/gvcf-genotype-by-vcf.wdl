workflow gvcf_genotype_by_vcf_workflow {

  meta {
    keywords: '{"keywords": ["genotyping", "intervals"]}'
    name: 'gvcf_genotype_by_vcf'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Genotypes gvcf on positions given in the interval vcf file'
    changes: '{"2.0.6" : "Split xargs to make exit code work", "2.0.5" : "Handled case where the variants we genotype are outside the intervals used for HC", "2.0.4" : "genotyping in parallel", "2.0.3": "docker update and changes for the polygenic pipeline", "2.0.2": "added bcftools error control", "2.0.1": "interval_bed input added","2.0.0": "new docker image, task fails if any of the commands fail, removed genotyping_databese variable"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_sample_gvcf_gz: '{"name": "sample_gvcf_gz", "type": "File", "extension": [".gvcf.gz", ".g.vcf.gz"], "description": "Sample data"}'
    input_sample_gvcf_gz_tbi: '{"name": "sample_gvcf_gz_tbi", "type": "File", "extension": [".g.vcf.gz.tbi", ".gvcf.gz.tbi"], "description": "Index for sample data"}'
    input_interval_vcf_gz: '{"name": "interval_vcf_gz", "type": "File", "description": "Interval file with annotations info and loci to genotype", "extension": [".vcf.gz"]}'
    input_interval_vcf_gz_tbi: '{"name": "interval_vcf_gz_tbi", "type": "File", "description": "Index for interval_vcf_gz", "extension": [".vcf.gz.tbi"]}'
    input_interval_bed_gz: '{"name": "interval_bed_gz", "type": "File", "description": "Bed interval file - optional.", "extension": [".bed.gz"], "required": false}'
    input_interval_bed_gz_tbi: '{"name": "interval_bed_gz_tbi", "type": "File", "description": "Index for interval_bed_gz.", "extension": [".bed.gz.tbi"], "required": false}'

    output_genotyped_vcf_gz: '{"name": "genotyped_vcf_gz", "type": "File", "copy": "True", "description": "Vcf genotyped by interval vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call gvcf_genotype_by_vcf

}

task gvcf_genotype_by_vcf {

  String task_name = "gvcf_genotype_by_vcf"
  String task_version = "2.0.6"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0-grch38-no-alt:1.0.0"
  Boolean small_task = true
  Int ram = if small_task then 10 else 64
  String genotyping_java_options = "-Xmx9g -Xms4g"
  String concat_java_options = if small_task then "-Xmx9g -Xms4g" else "-Xmx63g -Xms4g"
  String gatk_path = "/gatk/gatk"

  String sample_id = "no_id_provided"

  # vcf used for genotyping (if no bed provided) and merging
  # important: if this file does not contain genotype data, then it must have multiallelic variants in one line (bcftools norm -m +any)!!!
  File interval_vcf_gz
  File interval_vcf_gz_tbi

  # bed used for genotyping (needed in the pgx module only)
  File? interval_bed_gz
  File? interval_bed_gz_tbi

  Boolean defined_bed = if defined(interval_bed_gz) then true else false
  String suffix = if defined_bed then "bed" else "vcf"
  File intervals = select_first([interval_bed_gz, interval_vcf_gz])

  # sample gvcf
  File sample_gvcf_gz
  File sample_gvcf_gz_tbi

  # bcftools command fixing variant ids: use in the polygenic pipeline only
  # explanation: after multiallelic variant split IDs are getting from INFO/RS_ID field - this is because
  # INFO field with Number=A allows to assign id to specific alt allele, this is not possible for the ID vcf field
  Boolean fix_id = false
  String id_fix_command = if fix_id then "| bcftools annotate --set-id '%INFO/RS_ID'" else ""

  # Handled case where the variants we genotype are outside the intervals used for HC. Now we get the format and genotype . and .
  # Conversion from sed to GT and ./. solving a problem
  Boolean format_and_genotype_fix = false
  String format_and_genotype_fix_command = if format_and_genotype_fix then "| sed 's|\\t\\.\\t\\.$|\\tGT\\t\\./.|'" else ""
  String ref_fasta = "`ls /resources/reference-genomes/*/*.fa`"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  mkdir sample-files
  mkdir intervals
  #ref_fasta=`ls /resources/reference-genomes/*/*.fa`

  # 1. SPLIT intervals (bed_gz and vcf_gz) and sample-gvcf into chromosome-wise files
  chroms=$( tabix -l ${intervals} )
  echo $chroms | sed 's/ /\n/g' | xargs -i -n 1 -P 24  bash -c "tabix -h ${interval_vcf_gz} {} | \
    bgzip > intervals/{}.intervals.vcf.gz"
  echo $chroms | sed 's/ /\n/g' | xargs -i -n 1 -P 24  bash -c "tabix -h ${sample_gvcf_gz} {} | \
    bgzip > sample-files/{}.sample.gvcf.gz"
  ls sample-files/*sample.gvcf.gz | xargs -i -n 1 -P 24  bash -c "tabix -p vcf {}"
  ls intervals/*intervals.vcf.gz | xargs -i -n 1 -P 24  bash -c "tabix -p vcf {}"

  if  ${defined_bed};then
    echo $chroms | sed 's/ /\n/g' | xargs -i -n 1 -P 24  bash -c "tabix -h ${interval_bed_gz} {} | \
    bgzip > intervals/{}.intervals.bed.gz"
    ls intervals/*intervals.bed.gz | xargs -i -n 1 -P 24  bash -c "tabix -p bed {}"
  fi


  # 2. PARALEL GENOTYPING sample_gvcfs WITH chromosome-wise interval files
  echo $chroms | sed 's/ /\n/g' | \
    xargs -i -n 1 -P 6 bash -c '\
      ${gatk_path} --java-options "${genotyping_java_options}" GenotypeGVCFs \
        --allow-old-rms-mapping-quality-annotation-data \
        --lenient \
        --include-non-variant-sites \
        --intervals intervals/{}.intervals.${suffix}.gz \
        --variant sample-files/{}.sample.gvcf.gz \
        --output sample-files/{}.genotyped-sample.vcf.gz \
        --reference ${ref_fasta}'

  # 3 FIND sample name (in the input gvcf file)
  sample_name=$(bcftools query -l ${sample_gvcf_gz})


  # 4 MERGING interval vcf files WITH genotyped-norm-sample.vcf.gz:
  # TO CHANGE REF/REF sites alleles from dot to nucleotide
  # and to add annotations (if present in the interval vcf file - ID and INFO columns),
  # SPLITTING multiallelic sites, NORMALIZING indels, REMOVING samples added from the interval vcf
  # and optionally fixing IDs (neccessary in the polygenic pipeline)
  echo $chroms | sed 's/ /\n/g' | \
    xargs -i -n 1 -P 24 bash -c "\
    bcftools merge -m all intervals/{}.intervals.vcf.gz sample-files/{}.genotyped-sample.vcf.gz \
    -R intervals/{}.intervals.${suffix}.gz -o sample-files/{}.merged.vcf.gz -O z"
    
  echo $chroms | sed 's/ /\n/g' | \
    xargs -i -n 1 -P 24 bash -c "\
    bcftools norm -m -any -f ${ref_fasta} sample-files/{}.merged.vcf.gz \
    ${id_fix_command} \
    ${format_and_genotype_fix_command} \
    | bcftools view -s $sample_name -o sample-files/{}.genotyped-by-vcf.vcf.gz -O z"

  ls sample-files/*.genotyped-by-vcf.vcf.gz | xargs -i -n 1 -P 24  bash -c "tabix -p vcf {}"


  # 5 CONCATENATE chromosome-wise genotyped vcf files
  files_to_concat=$(ls sample-files/*.genotyped-by-vcf.vcf.gz | sed 's/^/-I /' )
  ${gatk_path} --java-options "${concat_java_options}" \
    MergeVcfs $files_to_concat \
      -O ${sample_id}_genotyped-by-vcf.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory:  ram + "G"
    cpu: 12
    maxRetries: 2

  }

  output {

    File genotyped_vcf_gz = "${sample_id}_genotyped-by-vcf.vcf.gz"
    File genotyped_vcf_gz_tbi = "${sample_id}_genotyped-by-vcf.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
