workflow gvcf_genotype_by_vcf_workflow {

  meta {
    keywords: '{"keywords": ["GenotypeGVCFs", "GATK", "clinvar"]}'
    name: 'gvcf_genotype_by_vcf'
    author: 'https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'gvcf_genotype_by_vcf: genotype gvcf.gz using vcf file instead of interval_file, merge output vcf with interval vcf'
    changes: '{"latest": "no changes"}'

    input_sample_gvcf_gz: '{"name": "sample_gvcf_gz", "type": "File", "extension": [".gvcf.gz", ".g.vcf.gz"], "description": "Sample data"}'
    input_sample_gvcf_gz_tbi: '{"name": "sample_gvcf_gz_tbi", "type": "File", "extension": [".g.vcf.gz.tbi", ".gvcf.gz.tbi"], "description": "Index for sample data"}'
    input_interval_vcf_gz: '{"name": "interval_vcf_gz", "type": "File", "description": "Interval file with annotations info and loci to genotype", "extension": [".vcf.gz"]}'
    input_interval_vcf_gz_tbi: '{"name": "interval_vcf_gz_tbi", "type": "File", "extension": [".vcf.gz.tbi"]}'
    input_interval_bed_gz: '{"name": "interval_bed_gz", "type": "File", "description": "Bed file for interval_bed_gz", "optional": "true", "extension": [".bed.gz"]}'
    input_interval_bed_gz_tbi: '{"name": "interval_bed_gz_tbi", "type": "File", "description": "Index for interval_vcf_gz", "extention": [".bed.gz.tbi"], "optional": "true"}'
    
    output_genotyped_vcf_gz: '{"name": "genotyped_vcf_gz", "type": "File", "copy": "True", "description": "Vcf genotyped by interval vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call gvcf_genotype_by_vcf

}

task gvcf_genotype_by_vcf {

  String task_name = "gvcf_genotype_by_vcf"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.6.0-hg38:1.0.0"
  String genotype_gvcfs_java_options = "-Xmx5g -Xms5g"
  String gatk_path = "/gatk/gatk"

  String sample_id = "no_id_provided"

  # vcf used for genotyping (for example clinvar)
  String genotyping_databese = "clinvar"
  File interval_vcf_gz # needed for STEP 4.

  # this files are optional - can be created in this task if not provided, but to save calculation time is better to provide them already inside docker image. 
  # IN PROGRESS
  File? interval_bed_gz # needed for STEP 3.
  File? interval_bed_gz_tbi
  Boolean is_interval_bed_gz_not_defined = !defined(interval_bed_gz)
  String merge_annotations = "true"


  # sample gvcf
  File sample_gvcf_gz
  File sample_gvcf_gz_tbi

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)


  # 1. CREATING BED FILE AND ITS INDEX FOR interval_vcf_gz (PREVIOUS MODULE DOES NOT PROVIDE)

  if [ ${is_interval_bed_gz_not_defined} ]; then
    zcat ${interval_vcf_gz} | awk '{print $1"\t"($2 - 1)"\t"$2}' | sort -k1,1d -k2,2n | grep -v "^#" | bgzip > interval.bed.gz
    tabix -p bed interval.bed.gz
    interval_bed_gz="interval.bed.gz"
  else
   interval_bed_gz=${interval_bed_gz}
  fi

  # 2. GENOTYPING sample_gvcf_gz WITH interval_bed_gz
  ${gatk_path} --java-options "${genotype_gvcfs_java_options}" \
      GenotypeGVCFs \
      --allow-old-rms-mapping-quality-annotation-data \
      --lenient \
      --include-non-variant-sites \
      --intervals $interval_bed_gz \
      --variant ${sample_gvcf_gz} \
      --output genotyped-sample.vcf.gz \
      --reference /resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa

  if [ "${merge_annotations}" = "true" ]; then
  # 3. MERGING interval_vcf_gz WITH genotyped-sample.vcf.gz (TO CONTAIN ISEQ_CLINVAR_GENE_INFO)
    tabix -p vcf ${interval_vcf_gz}
    bcftools merge genotyped-sample.vcf.gz ${interval_vcf_gz} | bgzip > temp.vcf.gz
    
  # 4. REMOVING irrelevant sample from interval_vcf_gz (syntetic_homo_sample IN CLINVAR)
    sample_name=$(bcftools query -l ${sample_gvcf_gz})
    bcftools view -s $sample_name temp.vcf.gz | bgzip > ${sample_id}_genotyped-by-${genotyping_databese}.vcf.gz
  else 
    cp genotyped-sample.vcf.gz ${sample_id}_genotyped-by-${genotyping_databese}.vcf.gz

  fi

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  # VARIANTY WYKRYTE JAKO SYNTHETIC SAMPLE NIE MAJA ALLELIC DEPTH I DLATEGO NIE MA ICH W STATYSTYKACH

  runtime {

    docker: docker_image
    memory: "6G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File genotyped_vcf_gz = "${sample_id}_genotyped-by-${genotyping_databese}.vcf.gz"
  
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
