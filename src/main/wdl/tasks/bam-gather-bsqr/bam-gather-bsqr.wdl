workflow bam_gather_bsqr_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'bam_gather_bsqr'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Gather BQSR Reports \n   Implements GATK Gather BQSR Reports.'
    changes: '{"latest": "no changes"}'
    input_partial_recalibration_reports: '{"name": "Partial recalibration reports array", "type": "Array[File]", "extension": [".csv"], "description": ""}'
    output_recalibration_report: '{"name": "Recalibration report", "type": "File", "description": ""}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bam_gather_bsqr

}

task bam_gather_bsqr {

  Array[File] partial_recalibration_reports

  String sample_id = "no_id_provided"

  String gatk_path = "/gatk/gatk"
  String java_opt = "-Xms3000m"

  String task_name = "bam_gather_bsqr"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0:1.0.0"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    ${gatk_path} --java-options ${java_opt} \
      GatherBQSRReports \
      -I ${sep=' -I ' partial_recalibration_reports} \
      -O ${sample_id}_recal-data.csv

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File recalibration_report = "${sample_id}_recal-data.csv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
