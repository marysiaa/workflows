workflow rna_seq_cuffquant_workflow {

  meta {
    keywords: '{"keywords": ["cuffquant"]}'
    name: 'rna_seq_cuffquant'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.0.4": "more memory"}'

    input_gtf_file: '{"name": "gtf_file", "type": "File", "description": "gtf file"}'
    input_bam_file: '{"name": "bam_file", "type": "File", "description": "bam_file"}'

    output_abundances_file: '{"name": "abundances_file", "type": "File", "copy": "True", "description": "abundances file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_cuffquant

}

task rna_seq_cuffquant {

  File gtf_file
  File bam_file

  String sample_id = "no_id_provided"
  Int num_cpu = 4

  String task_name = "rna_seq_cuffquant"
  String task_version = "1.0.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/cufflinks:1.1.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    cuffquant -v -p ${num_cpu} --no-effective-length-correction -o cuffquant \
     ${gtf_file} ${bam_file}

    mv cuffquant/abundances.cxb cuffquant/${sample_id}.cxb

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: num_cpu
    maxRetries: 2

  }

  output {

    File abundances_file = "cuffquant/${sample_id}.cxb"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
