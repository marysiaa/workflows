workflow vcf_beagle_postproc_workflow
{
  call vcf_beagle_postproc
}

task vcf_beagle_postproc {

  String task_name = "vcf_beagle_postproc"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-beagle-postproc:1.1.0"

  File beagle_vcf
  File? beagle_vcf_tbi
  File? sites_tsv
  File? sites_tsv_tbi
  Boolean gtc_sites_only = if defined(sites_tsv) then true else false
  Boolean create_index = if defined(beagle_vcf_tbi) then false else true

  String ref_path = "/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa"
  String fai_path = "/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa.fai"
  String map_file_path = "/resources/reference-genomes/map-file/map.file"
  String? chrom
  String vcf_basename_with_colon = basename(beagle_vcf, ".vcf.gz")
  String vcf_basename = sub(vcf_basename_with_colon, ":", ".")

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   if ${create_index}; then
       tabix -p vcf ${beagle_vcf}
   fi

   ## create empty (as it is created only optionally)
   touch ${vcf_basename}imputed-reformatted.vcf.gz

   ## Change chromosome names, check reference concordance for the imputed vcf
   ## Remove also contig fields from the header:
   ## contigs (only those present in the vcf body) are added by the bcftools, as beagle does not write them
   ## I'm removing contig info from the header and will refill it later - just to make sure that in headers of
   ## all the vcfs in the scatter, the same contigs are placed in the same order (which is required by gatk MergeVcfs)
   bcftools annotate --rename-chrs ${map_file_path}  ${beagle_vcf} |\
   bcftools norm -m -any -c x -f ${ref_path} --do-not-normalize | \
   grep -v '##contig=' | bgzip -c > ${vcf_basename}imputed-reformatted-all-variants.vcf.gz
   tabix -p vcf ${vcf_basename}imputed-reformatted-all-variants.vcf.gz


   ## Extract from beagle vcf only sites present in the gtc_vcf (in two steps, to make it faster)
   if ${gtc_sites_only};then

       # Tbi could be in different directory than vcf file, the symlink below fixes it
       tsv_dir=$(dirname "${sites_tsv}")
       tbi_name=$(basename "${sites_tsv_tbi}")
       if [ ! -f $tsv_dir/$tbi_name ]; then
           ln -s ${sites_tsv_tbi} $tsv_dir/$tbi_name
       fi

       ## Extract sites by contig and variant position
       bcftools view ${vcf_basename}imputed-reformatted-all-variants.vcf.gz -R ${sites_tsv} -o tmp1.vcf.gz -O z

       ## Check also alleles
       python3 /intelliseqtools/get_variants_by_pos_and_alleles.py \
           --input-vcf tmp1.vcf.gz \
           --output-vcf tmp2.vcf.gz \
           --variant-tsv ${sites_tsv}

       ## Remove contigs from the header - one more time
       zcat  tmp2.vcf.gz | grep -v '##contig=' | bgzip -c > ${vcf_basename}imputed-reformatted.vcf.gz
   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File all_sites_out_vcf = "${vcf_basename}imputed-reformatted-all-variants.vcf.gz"
    File all_sites_out_vcf_tbi = "${vcf_basename}imputed-reformatted-all-variants.vcf.gz.tbi"
    File out_vcf = "${vcf_basename}imputed-reformatted.vcf.gz"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
