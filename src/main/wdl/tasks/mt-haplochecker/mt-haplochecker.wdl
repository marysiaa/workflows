workflow mt_haplochecker_workflow {

  meta {
    keywords: '{"keywords": ["mitochondrion", "haplotype"]}'
    name: 'Mitochondrial haplotype check'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Check mitochondrial haplotypes, may be used to estimate contamination of the NGS data'
    changes: '{"1.0.3": "additional outputs", "1.0.2": "new docker", "1.0.1": "works with empty chrM bam"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_mt_bam: '{"name": "Mitochondrial bam", "type": "File","extension": [".bam"], "description": "Mitochondrial bam (realigned to normal chrM fasta)"}'
    input_mt_bai: '{"name": "Mitochondrial bai", "type": "File","extension": [".bai"], "description": "Mitochondrial bam index"}'
    input_qual: '{"name": "Qual", "type": "Int", "default": 20, "description": "Per base minimal qual of read to be considered"}'
    input_map_qual: '{"name": "Mapping qual", "type": "Int", "default": 30, "description": "Minimal mapping quality of read to be considered"}'
    input_vaf: '{"name": "VAF", "type": "Float", "default": 0.01, "description": "Minimal variant frequency"}'

    output_contamination_file: '{"name": "Contamination file", "type": "File", "copy": "True", "description": "File containing haplochecker contamination report"}'
    output_minor_level: '{"name": "Minor level", "type": "Float", "copy": "True", "description": "Haplochecker minor level statistics (may be used as contamination estimate)"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call mt_haplochecker

}

task mt_haplochecker {

  String task_name = "mt_haplochecker"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/mitolib:1.1.0"

  String sample_id = "no_id_provided"
  File mt_bam
  File mt_bai
  String basename = basename(mt_bam, ".bam")

  Int qual = 20
  Int map_qual = 30
  Float vaf = 0.01

  String mitolib_version = "0.1.2"
  String broad_resources_version = "v0"
  String ref_fasta = "/resources/broad-institute-hg38-mt-reference/"+ broad_resources_version + "/Homo_sapiens_assembly38.chrM.fasta"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   java -jar /tools/mitolib/${mitolib_version}/mitolib.jar haplochecker \
    --in ${mt_bam} \
    --ref ${ref_fasta} \
    --out haplochecker_out \
    --QUAL ${qual} \
    --MAPQ ${map_qual} \
    --VAF ${vaf}


   if grep -q -v 'SampleID' haplochecker_out/${basename}.contamination.txt; then
      awk -F "\t" 'BEGIN{OFS="\t"}; NR==1 { for (i=1; i<=NF; i++) { f[$i] = i }} NR==2 { print $(f["MinorLevel"])}' \
      haplochecker_out/${basename}.contamination.txt > ${sample_id}_minor-level.txt
   else
      echo '0.0' > ${sample_id}_minor-level.txt
   fi

   sed "s/${basename}/${sample_id}/" haplochecker_out/${basename}.contamination.txt > ${sample_id}_contamination.tsv
   sed "s/${basename}/${sample_id}/" haplochecker_out/${basename}.haplogrep.txt > ${sample_id}_haplogrep.tsv
   cp haplochecker_out/${basename}.pileup ${sample_id}_haplogrep.pileup


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File contamination_file = "${sample_id}_contamination.tsv"
    File haplogrep_tsv = "${sample_id}_haplogrep.tsv"
    File haplogrep_pileup = "${sample_id}_haplogrep.pileup"
    Float minor_level = read_float("${sample_id}_minor-level.txt")
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
