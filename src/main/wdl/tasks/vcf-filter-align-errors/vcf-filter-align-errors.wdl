workflow vcf_filter_align_errors_workflow
{
  call vcf_filter_align_errors
}

task vcf_filter_align_errors {

  String task_name = "vcf_filter_align_errors"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-filter-align-errors:1.1.0"


  File vcf
  File vcf_tbi
  File bamout
  File bamout_bai

  String sample_id = "no_id_provided"
  String ref_fasta = "`ls /resources/reference-genomes/*/*.fa`"
  String ref_fasta_image = "`ls /resources/reference-genomes/*/*.fa.img`"
  String java_mem = "-Xmx11g"

  ## to check possible options see:
  ## https://gatk.broadinstitute.org/hc/en-us/articles/360050814972-FilterAlignmentArtifacts-EXPERIMENTAL-
  ## some arguments are listed below (with defaults)
  ## --drop-ratio 0.2; --indel-start-tolerance 5; --max-reasonable-fragment-length 100000 (?);
  ## --min-aligner-score-difference-per-base 0.2; --min-mismatch-difference-per-base 0.02
  ## --num-regular-contigs 250000 (?);  -split-factor 0.5
  String realignment_extra_args = "--num-regular-contigs 194 --max-reasonable-fragment-length 2000 --drop-ratio 0.1 --indel-start-tolerance 8"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk --java-options "${java_mem}" FilterAlignmentArtifacts \
       -R ${ref_fasta} \
       -V ${vcf} \
       -I ${bamout} \
       --bwa-mem-index-image ${ref_fasta_image} \
       ${realignment_extra_args} \
       -O ${sample_id}_align-error-filtered-m2.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "12G"
    cpu: 4
    maxRetries: 2

  }

  output {

    File artifact_filtered_vcf = "${sample_id}_align-error-filtered-m2.vcf.gz"
    File artifact_filtered_vcf_tbi = "${sample_id}_align-error-filtered-m2.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
