workflow vcf_anno_ge_panels_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "Genomics England panels", "vcf annotation"]}'
    name: 'vcf_anno_ge_panels'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Annotate vcf with Genomics England panels'
    changes: '{"1.2.5": "ensembl-resources docker update", "1.2.4": "docker update", "1.2.3": "docker update (uniprot, not related)", "1.2.2": "docker update", "1.2.1": "Panels and docker update", "1.2.0": "New data structure of panels data", "1.0.0": "no changes"}'

    input_vcf_gz: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf tabix index", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'

    output_annotated_with_ge_panels_vcf_gz: '{"name": "Annotated vcf", "type": "File", "copy": "True", "description": "Vcf annotated with Genomics England panels"}'
    output_annotated_with_ge_panels_vcf_gz_tbi: '{"name": "Annotated vcf tabix index", "type": "File", "copy": "True", "description": "Index for vcf annotated with Genomics England panels"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_ge_panels

}

task vcf_anno_ge_panels {

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename = "no_id_provided"

  String task_name = "vcf_anno_ge_panels"
  String task_version = "1.2.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-ge-panels:1.3.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   python3 /intelliseqtools/vcf_anno_ge_panels.py \
    --input_vcf ${vcf_gz} \
    --output_vcf ${vcf_basename}_anno-ge-panels.vcf.gz

   tabix -p vcf ${vcf_basename}_anno-ge-panels.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_with_ge_panels_vcf_gz = "${vcf_basename}_anno-ge-panels.vcf.gz"
    File annotated_with_ge_panels_vcf_gz_tbi = "${vcf_basename}_anno-ge-panels.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
