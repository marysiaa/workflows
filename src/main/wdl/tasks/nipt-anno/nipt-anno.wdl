workflow nipt_anno_workflow
{
  call nipt_anno
}

task nipt_anno {

  String task_name = "nipt_anno"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_nipt-anno:1.2.0"
  File variant_bed
  String sample_id = basename(variant_bed, "_aberrations.bed")

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   printf "hgvs\n" | paste <(head -1 ${variant_bed}) - > header
   tail -n +2  ${variant_bed} | sed 's/^/chr/' > tmp.variant.bed
   cut -f 1-3,6 tmp.variant.bed | sed 's/chr//' > tmp.hgvs.txt

   ## add hgvs
   while read f; do
      old=$(echo $f | cut -f1 -d ' '); new=$(echo $f | cut -f2 -d ' ')
      sed -i "s/^$old\t/$new:g./" tmp.hgvs.txt
   done</resources/mapping-file.txt
   sed 's/\t/_/' tmp.hgvs.txt | sed 's/\tgain/dup/' | sed 's/\tloss/del/' > hgvs.txt
   paste  tmp.variant.bed hgvs.txt > variant.bed

   awk -F '\t' '{if ($6 == "gain") print $0}' variant.bed > gain.bed
   awk -F '\t' '{if ($6 == "loss") print $0}' variant.bed > loss.bed

   bedtools intersect -wao -a gain.bed -b /resources/clinvar.gain.bed.gz \
      | awk -F '\t' 'BEGIN {OFS = "\t"} $0 = $0 {if ($12 != 0 ) {$13 = ($12 * 100)/($3 -$2); $14 = ($12 * 100)/($10 -$9)} else {$11 = ".;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;." ; $13 = 0; $14 = 0}} {print $0}' \
      > gain-clinvar.bed

    bedtools intersect -wao -a loss.bed -b /resources/clinvar.loss.bed.gz \
      | awk -F '\t' 'BEGIN {OFS = "\t"} $0 = $0 {if ($12 != 0 ) {$13 = ($12 * 100)/($3 -$2); $14 = ($12 * 100)/($10 -$9)} else {$11 = ".;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;.;." ; $13 = 0; $14 = 0}} {print $0}' \
      > loss-clinvar.bed

   bedtools intersect -wao -a gain.bed -b /resources/decipher.gain.cnv.bed.gz \
    | awk -F '\t' 'BEGIN {OFS = "\t"} $0 = $0 {if ($12 != 0 ) {$13 = ($12 * 100)/($3 -$2); $14 = ($12 * 100)/($10 -$9)} else {$11 = ".;.;.;."; $13 = 0; $14 = 0}} {print $0}' \
      > gain-decipher.bed

  bedtools intersect -wao -a loss.bed -b /resources/decipher.loss.cnv.bed.gz \
    | awk -F '\t' 'BEGIN {OFS = "\t"} $0 = $0 {if ($12 != 0 ) {$13 = ($12 * 100)/($3 -$2); $14 = ($12 * 100)/($10 -$9)} else {$11 = ".;.;.;."; $13 = 0; $14 = 0}} {print $0}' \
    > loss-decipher.bed

  cat /resources/header.clinvar1.txt > ${sample_id}_abberations-anno-clinvar.tsv
  paste header /resources/header.clinvar2.txt >> ${sample_id}_abberations-anno-clinvar.tsv
  cat  loss-clinvar.bed gain-clinvar.bed \
      | sed 's/;/\t/g' | sort -k1,1V -k2,2n -k3,3n >> ${sample_id}_abberations-anno-clinvar.tsv

  paste header /resources/decipher.header.txt > ${sample_id}_abberations-anno-syndromes.tsv
  cat  loss-decipher.bed gain-decipher.bed \
      | sed 's/;/\t/g' | sort -k1,1V -k2,2n -k3,3n >> ${sample_id}_abberations-anno-syndromes.tsv

  sed  '/^chr\t/ { s,decipher,database,g }' ${sample_id}_abberations-anno-syndromes.tsv > tmp.syndromes.tsv
  sed  '/^chr\t/ { s,ClinVar,database,g }' ${sample_id}_abberations-anno-clinvar.tsv > tmp.clinvar.tsv

  Rscript /intelliseqtools/summarize-syndromes.R \
      --patient_data tmp.syndromes.tsv \
       --database decipher \
       --decipher /resources/decipher.cnv

  
  Rscript /intelliseqtools/summarize-syndromes.R \
      --patient_data tmp.clinvar.tsv \
      --database clinvar 
     
  
  jq -n 'reduce inputs as $s (.; (.[input_filename|rtrimstr(".json")]) += $s)' syndromes_summary_pse.json syndromes_summary_nopse.json clinvar_summary.json \
  > ${sample_id}_syndromes.json
  #jq '.syndromes_summary_pse[] | select(.Match == true)' tmp.json  > PSE_TEST
  #jq '.syndromes_summary_nopse[] | select(.Match == true)' tmp.json  > NOPSE_TEST
  

  

  #if  grep -q '^' <(cat PSE_TEST NOPSE_TEST); then
  #   jq '. += {"CNVARIANT": "HIGH RISK FOR TESTED COPY NUMBER VARIANTS"}' tmp.json | \
  #   jq '. += {"INTCNVARIANT": "Result consistent with deletions or duplications detected in the regions covered by the test."}'  \
  #   > ${sample_id}_syndromes.json
  #else
  #    jq '. += {"CNVARIANT": "LOW RISK FOR TESTED COPY NUMBER VARIATIONS"}' tmp.json | \
  #    jq '. += {"INTCNVARIANT": "Result consistent with no deletions or duplications detected in the regions covered by the test."}' \
  #     > ${sample_id}_syndromes.json    
  #fi  

  #rm tmp*json tmp*tsv

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File clinvar_tsv = "${sample_id}_abberations-anno-clinvar.tsv"
    File syndromes_tsv = "${sample_id}_abberations-anno-syndromes.tsv"
    File syndromes_json = "${sample_id}_syndromes.json"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
