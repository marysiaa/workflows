
workflow bam_bsqr_workflow {

  meta {
  keywords: '{"keywords": []}'
  name: 'Bam bsqr'
  author: 'https://gitlab.com/lltw'
  copyright: 'Copyright 2019 Intelliseq'
  description: 'Make bqsr reports \n  Implements GATK BaseRecalibrator.'
  changes: '{"1.1.3": "docker gatk update", "1.1.2": "new docker"}'

  input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
  input_markdup_bam: '{"name": "Markdup bam", "type": "File", "extension": [".bam"], "description": ""}'
  input_markdup_bai: '{"name": "Markdup bai", "type": "File", "extension": [".bai"], "description": ""}'

  output_partial_recalibration_report: '{"name": "Partial recalibration report", "type": "File", "description": ""}'
  output_base_recalibrator_stdout_stderr_log: '{"name": "Base recalibrator stdout stderr_log", "type": "File"}'
  output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
  output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
  output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bam_bsqr 
  }

task bam_bsqr {

  String task_name = "bam_bsqr"
  String task_version = "1.1.3"
  Int? index = 1 
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-bsqr:1.2.1"

  File markdup_bam
  File markdup_bai

  # Inputs with defaults
  Array[String] sequence_group_interval = ["chr11"]
  String sample_id = "no_id_provided"

  # Tools runtime settings, paths etc.
  String mills_and_1000g_gold_standard_indels_vcf_gz_path = "/resources/broad-institute-resources-for-bqsr-and-vqsr/indels-known-sites/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz"
  String homo_sapiens_assembly38_known_indels_vcf_gz_path = "/resources/broad-institute-resources-for-bqsr-and-vqsr/indels-known-sites/Homo_sapiens_assembly38.known_indels.vcf.gz"
  String gatk_path = "/gatk/gatk"
  String java_opt = "-Xms6000m"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    echo  ${sep=" " sequence_group_interval} | sed 's/ /\n/g' | sed 's/\t/\n/g' > my.intervals

    # Run BaseRecalibrator
    ${gatk_path} --java-options ${java_opt} \
      BaseRecalibrator \
      -R /resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa \
      -I ${markdup_bam} \
      --use-original-qualities \
      -O ${index}-${sample_id}_recal-data.csv  \
      --known-sites ${mills_and_1000g_gold_standard_indels_vcf_gz_path} \
      --known-sites ${homo_sapiens_assembly38_known_indels_vcf_gz_path} \
      -L my.intervals

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File partial_recalibration_report = "${index}-${sample_id}_recal-data.csv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
