workflow cnv_tsv_read_workflow
{
  call cnv_tsv_read
}

task cnv_tsv_read {

  String task_name = "cnv_tsv_read"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.6"
  File sample_mapping

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  cut -f1 ${sample_mapping} | sort | uniq > samples.txt

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    Array[String] sample_ids = read_lines("samples.txt")
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
