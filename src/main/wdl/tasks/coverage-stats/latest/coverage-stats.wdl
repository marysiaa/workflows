# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: coverage_stats
#  authors:
#    - https://gitlab.com/Gleb Lavrukevich,
#  copyright: Copyright 2019 Intelliseq
#  description: >
#    This section requires developer to fill in details about the purpose of the task
#  changes:
#    latest:
#      - no changes
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow coverage_stats_workflow { call coverage_stats }

task coverage_stats {


  File intervals
  File bam_file
  File bai_file
  String task_name = "coverage_stats"
  String task_version = "latest"
  String docker_image = "intelliseqngs/ubuntu-toolbox:18.04_v0.8"
  String stats_name = "coverage"
  String output_file = "answer.json"




  command <<<


  ### start time
  starttime=$(date +%s)

  ### bioobject
  if [ -e "/resources.bioobject.json" ]; then RESOURCES=$(cat /resources.bioobject.json); else RESOURCES="[]"; fi
  if [ -e "/tools.bioobject.json" ]; then TOOLS=$(cat /tools.bioobject.json); else TOOLS="[]"; fi
  CPU=$(lscpu | grep '^CPU(s)' | grep -o '[0-9]*')
  MEMORY=$(cat /proc/meminfo | grep MemTotal | grep -o '[0-9]*' |  awk '{ print $1/1024/1024 ; exit}')
  finishtime=$(date +%s)
  printf TASKTIME=$((finishtime-starttime))



  printf "{\
    \"task-name\":\"${task_name}\",\
    \"task-version\":\"${task_version}\",\
    \"docker-image\":\"${docker_image}\",\
    \"cpu\":\"$CPU\",\
    \"memory\":\"$MEMORY\",\
    \"resources\":$RESOURCES,\
    }" | sed 's/ //g' > bioobject.json


  cat ${intervals} | grep -v "@SQ" | grep -v "HD" >  bed_file.bed
  wget https://github.com/brentp/mosdepth/releases/download/v0.2.6/mosdepth && chmod +x ./mosdepth && ./mosdepth -h

  ./mosdepth --fast-mode -t 1 -T 1,2,3,5,10,15,20,30,50,100 --by bed_file.bed ${stats_name} ${bam_file}
  gzip -d ${stats_name}.thresholds.bed.gz


  python3 << CODE

  import pandas as pd
  import json


  df = pd.read_csv("coverage.thresholds.bed", sep="\t")
  tresholds = df.filter(regex="X$")

  lines_number = df.shape[0]
  stats_dict = {}

  for treshold in tresholds:
    nonzero_lines = df[str(treshold)].to_numpy().nonzero()
    nonzero_length = nonzero_lines[0].shape[0]
    stats_dict[str(treshold)] = round(nonzero_length / lines_number, 4)

  json_list = []
  json_list.append(stats_dict)
  final_json = {"coverage-statistics": json_list}
  with open('/home/answer.json', 'w') as outfile:
    json.dump(final_json, outfile, indent=4)

  CODE

  cat /home/answer.json > "${output_file}"






>>>


  runtime {

    docker: docker_image
    memory: "500M"
    cpu: "1"
    maxRetries: 2

  }

  output {

    # @Output(required=true,directory="/coverage_stats",filename="stdout.log")
    File stdout_log = stdout()
    # @Output(required=true,directory="/coverage_stats",filename="stderr.log")
    File stderr_log = stderr()
    # @Output(required=true,directory="/coverage_stats",filename="bioobject.json")
    File bioobject = "bioobject.json"

    File dest = "${output_file}"

  }

}
