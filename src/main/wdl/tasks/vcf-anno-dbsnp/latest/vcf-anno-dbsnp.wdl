workflow vcf_anno_dbsnp_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "dbsnp"]}'
    name: 'Vcf annotation with dbsnp rsID'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'The task adds a VCF field ISEQ_DBSNP_RS, containing dbsnp rsIDs (per allele)'
    changes: '{"latest": "no changes"}'

    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file, should have splitted multiallelic sites and normalized indels."}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID"}'
    input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]} , "description": ""}'

    output_annotated_with_dbsnp_rsids_vcf_gz : '{"name": "Annotated with rsids vcf", "type": "File", "description": "Vcf file with rsiDs added."}'
    output_annotated_with_dbsnp_rsids_vcf_gz_tbi : '{"name": "Annotated with rsids vcf tbi", "type": "File", "description": "Index for the output vcf file."}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_dbsnp

}

task vcf_anno_dbsnp {

  String task_name = "vcf_anno_dbsnp"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String chromosome
  String docker_image = "intelliseqngs/task_vcf-anno-dbsnp:1.0.0-" + chromosome

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename="no_id_provided"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  set -e pipefail

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  vcf_dir=$(dirname "${vcf_gz}")
  tbi_name=$(basename "${vcf_gz_tbi}")
  if [ ! -f $vcf_dir/$tbi_name ]; then
    ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
  fi

  dbsnp_database=$( ls /resources/*/*/${chromosome}*.vcf.gz )

  bcftools annotate --columns INFO --annotation $dbsnp_database ${vcf_gz} 2>> bcftools.stderr_log \
    | bgzip > ${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz 2>> bcftools.stderr_log

  cat bcftools.stderr_log >&2

  # Bcftools annotate returns exit 0 even if it has failed. The code below fixes it
  #if grep -q "^\[E::" bcftools.stderr_log; then
  #  exit 1
  #fi

  tabix -p vcf ${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "2"
    maxRetries: 2

  }

  output {
    File annotated_with_dbsnp_rsids_vcf_gz = "${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz"
    File annotated_with_dbsnp_rsids_vcf_gz_tbi = "${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
