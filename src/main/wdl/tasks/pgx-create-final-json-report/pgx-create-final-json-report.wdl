workflow pgx_create_final_json_report_workflow
{
    call pgx_create_final_json_report
}

task pgx_create_final_json_report {

    File merged_models
    String analysis_name = "PGx Cannabinoid report"
    String analysis_version = "1.0.0"
    String sample_id = "no_id_provided"

    String task_name = "pgx_create_final_json_report"
    String task_version = "1.0.1"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/task_pgx-create-final-json-report:1.0.1"

    command <<< 
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # Create final JSON report
    /intelliseqtools/pgx-create-final-json-report.py --models ${merged_models} \
                                                    --diplotype-phenotype-table "/resources/Diplotype_Phenotype_Table.xlsx" \
                                                    --analysis-name "${analysis_name}" \
                                                    --analysis-version "${analysis_version}" \
                                                    --output-filename "${sample_id}_PGx_Cannabinoid_report.json"

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                                --task-name-with-index ${task_name_with_index} \
                                                --task-version ${task_version} \
                                                --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: 1
        maxRetries: 2

    }

    output {

        File final_json_report = "${sample_id}_PGx_Cannabinoid_report.json"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
