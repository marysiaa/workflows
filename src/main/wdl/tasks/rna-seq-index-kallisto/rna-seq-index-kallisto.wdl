workflow rna_seq_index_kallisto_workflow
{
  call rna_seq_index_kallisto
}

task rna_seq_index_kallisto {

  File transcriptome
  String analysis_id = "no_id_provided"

  String task_name = "rna_seq_index_kallisto"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/kallisto:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    kallisto index -i ${analysis_id}-kallisto_index ${transcriptome}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 1
    maxRetries: 2

  }

  output {
    File transcriptome_index = "${analysis_id}-kallisto_index"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
