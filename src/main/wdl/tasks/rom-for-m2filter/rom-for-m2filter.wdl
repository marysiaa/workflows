workflow rom_for_m2filter_workflow
{
  call rom_for_m2filter
}

task rom_for_m2filter {

  String task_name = "rom_for_m2filter"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0:1.0.0"

  Array[File] f1r2_tar_gz ## files produced by mutect2
  String sample_id = "no_id_provided"
  String java_mem = "-Xmx3500m"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk --java-options "${java_mem}" LearnReadOrientationModel \
         -I ${sep=" -I " f1r2_tar_gz} \
         -O ${sample_id}_artifact-priors.tar.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File artifact_priors = "${sample_id}_artifact-priors.tar.gz"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
