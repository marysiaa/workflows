workflow vcf_acmg_bp4_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "BP4", "variant", "benign"]}'
    name: 'Vcf ACMG BP4'
    author: 'https://gitlab.com/gleblavr , https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Evaluates whether variant fulfils ACMG BP4 criterion, adds annotation'
    changes: '{"2.0.2": "update ubuntu and pysam", "2.0.1": "pysam and cython update", "2.0.0": "dbNSFP predictors quorum", "1.2.0": "Added splicing checking (dbscSNV)", "1.1.0": "Python script reformatted, latest dir removed"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf"], "description": "Input annotated vcf"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True", "type": "File", "description": "Vcf with ACMG BP4 annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_acmg_bp4

}

task vcf_acmg_bp4 {

  String task_name = "vcf_acmg_bp4"
  String task_version = "2.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-bp4:2.1.0"
  File vcf
  String sample_id = "no-id"

  command <<<

  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   python3 /intelliseqtools/acmg-bp4.py \
         --input-vcf ${vcf} \
         --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz \
         --predictor /intelliseqtools/predictor-data.json


  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
