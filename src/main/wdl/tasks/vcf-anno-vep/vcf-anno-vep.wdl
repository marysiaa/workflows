workflow vcf_anno_vep_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "VEP", "loftee"]}'
    name: 'Vcf vep annotation'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Annotates vcf with vep and loftee'
    changes: '{"1.4.0": "update Ensembl to release107", "1.3.0": "dbscSNV splicing predictor added", "1.2.0": "new Ensembl database (104)", "1.1.0": "new Ensembl database, latest removed"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "Vcf", "type": "File", "extension": [".vcf.gz"], "description": "Input vcf file"}'
    input_vcf_gz_tbi: '{"name": "Vcf tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Index for the input vcf file"}'
    input_add_loftee: '{"name": "Add loftee?", "type": "Boolean", "description": "This option defines whether to add loftee annotation"}'
    input_add_dbscsnv: '{"name": "Add dbscSNV?", "type": "Boolean", "description": "This option defines whether to add dbscSNV splice sites annotation"}'

    output_anno_vcf_gz: '{"name": "Annotated vcf", "type": "File", "description": "Vcf file with vep annotation added"}'
    output_anno_vcf_gz_tbi: '{"name": "Annotated vcf tbi", "type": "File", "description": "Index for the output vcf file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True",   "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call vcf_anno_vep

}

task vcf_anno_vep {

  String task_name = "vcf_anno_vep"
  String task_version = "1.4.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  String sample_id = "no-id"
  File vcf_gz
  File vcf_gz_tbi

  String chromosome
  String docker_image = "intelliseqngs/vep:1.2.0-" + chromosome


  Boolean add_loftee = true
  Boolean add_dbscsnv = true

  String other_options = "--no_intergenic --hgvs --symbol --biotype --uniprot --total_length"
  String transcript_filtering = "--exclude_predicted --gencode_basic"
  String transcript_info = "--appris --tsl --xref_refseq --mane"

  ## loftee and other plugins
  String plugins = "/opt/vep/.vep/Plugins"
  String ancestor_fa = "/opt/vep/.vep/human_ancestor_seq/human_ancestor.fa.gz"
  String conservation_file = "/opt/vep/.vep/conservation/loftee.sql"
  String gerp_data = "/opt/vep/.vep/conservation/gerp_conservation_scores.homo_sapiens.GRCh38.bw"
  String loftee_command = "--plugin LoF,loftee_path:" + plugins +",human_ancestor_fa:" + ancestor_fa +",conservation_file:" + conservation_file +",gerp_bigwig:" + gerp_data
  String loftee = if (add_loftee) then loftee_command else ""

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # check if input file is not empty


  /opt/vep/src/ensembl-vep/vep \
       --cache \
       --offline \
       --format vcf --vcf \
       --force_overwrite \
       --dir_cache /opt/vep/.vep/ \
       --dir_plugins ${plugins} \
       --input_file ${vcf_gz} \
       --compress_output bgzip \
       --assembly GRCh38 \
       --output_file tmp.vcf.gz \
       ${other_options} ${transcript_filtering} ${transcript_info} ${loftee} \
       ${true="--plugin dbscSNV,/opt/vep/.vep/dbscSNV/dbscSNV1.1_GRCh38.txt.gz" false="" add_dbscsnv}

  tabix -p vcf tmp.vcf.gz

  if ${add_dbscsnv}; then
      bcftools +split-vep \
           tmp.vcf.gz \
           -c rf_score:Float,ada_score:Float \
           -p ISEQ_ \
           -s worst \
           -o ${chromosome}-${sample_id}_annotated-with-vep.vcf.gz -O z

      tabix -p vcf ${chromosome}-${sample_id}_annotated-with-vep.vcf.gz
  else
      mv tmp.vcf.gz ${chromosome}-${sample_id}_annotated-with-vep.vcf.gz
      mv tmp.vcf.gz.tbi ${chromosome}-${sample_id}_annotated-with-vep.vcf.gz.tbi
  fi

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File anno_vcf_gz = "${chromosome}-${sample_id}_annotated-with-vep.vcf.gz"
    File anno_vcf_gz_tbi = "${chromosome}-${sample_id}_annotated-with-vep.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
