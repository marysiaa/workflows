workflow fq_dragmap_workflow
{
  call fq_dragmap
}

task fq_dragmap {

  File fastq_1
  File? fastq_2
  Boolean paired = if defined(fastq_2) then true else false
  String platform = "Illumina" ## or IONTORRENT

  String sample_id = 'no_id_provided'

  String? chromosome
  String chromosome_defined = select_first([chromosome, ""])
  String reference_genome = "grch38-no-alt" # another option: String reference_genome = "hg38"
  String dragmap_reference_bin = "/resources/reference.bin"
  String dragmap_hash_table_cfg_bin = "/resources/hash_table.cfg.bin"
  String dragmap_hash_table_cmp = "/resources/hash_table.cmp"

  # Tools runtime settings, paths etc.
  Int no_threads = 16

  String dollar = "$"

  String task_name = "fq_dragmap"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = if defined(chromosome) then "intelliseqngs/dragmap-" + reference_genome + "-chr-wise:" + "1.0.0-" + chromosome_defined else "intelliseqngs/dragmap-" + reference_genome + ":1.0.0"

  command <<<
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    if ${paired}; then
        # prepare basename for bam file (intersection of basename1 and basename2)
        basename1=$( basename "${fastq_1}" ); basename2=$( basename "${fastq_2}" )
        if [ $basename1 == $basename2 ]; then
          echo 'ERROR: The filenames are identical.' >/dev/stderr
          exit 1
        fi
        fn=''; n=0; while [[ ${dollar}{basename1:n:1} == ${dollar}{basename2:n:1} ]]; do fn+=${dollar}{basename1:n:1}; ((n++)); done
        filename=${dollar}{fn::-1}

    else
        # prepare basename for bam file (intersection from basename1)
        filename=$( basename ${fastq_1} | sed 's/\.gz$//' | sed 's/\.f.*q$//' | sed 's/IonXpress_//')
    fi

    set -e -o pipefail

    ## check whether files are compressed to use proper programm (grep vs zgrep)
    if echo ${fastq_1} | grep -q '\.gz$';then
        regex_command="zgrep"
    else
        regex_command="grep"
    fi

    if $regex_command -q '^' ${fastq_1}; then

      # move dragmap_reference files
      mkdir dragmap_reference
      mv ${dragmap_reference_bin} ${dragmap_hash_table_cfg_bin} ${dragmap_hash_table_cmp} dragmap_reference

      # set reading groups
      if [ ${platform} = Illumina ]; then
          $regex_command -m1  '^@' ${fastq_1} | cut -d ':' -f 3,4 | sed 's/:/\./g' > rg_id
      else
          $regex_command -m1  '^@' ${fastq_1} | cut -d ':' -f 1 | sed 's/@//' > rg_id
      fi

      RG_ID=`cat rg_id`
      RG_SM="${sample_id}"
      fastq_2="${fastq_2}"

      # run dragmap
      dragen-os \
        --ref-dir dragmap_reference \
        --num-threads ${no_threads} \
        --preserve-map-align-order true \
        --fastq-file1 ${fastq_1} \
        ${true="--fastq-file2 $fastq_2" false="" paired}\
        --RGID "$RG_ID" \
        --RGSM "$RG_SM" \
        2> >(tee dragmap.stderr.log >&2) \
        | samtools view -h -O BAM - > ${sample_id}-$filename"_unsorted.bam"
        
    else
        echo 'ERROR: Input files are empty.' >/dev/stderr
        exit 1
    fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "64G"
    cpu: 16
    maxRetries: 2

  }

  output {

    File dragmap_unsorted_bam = glob("${sample_id}-*_unsorted.bam")[0]

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
