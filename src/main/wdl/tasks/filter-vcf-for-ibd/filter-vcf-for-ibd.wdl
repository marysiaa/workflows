workflow filter_vcf_for_ibd_workflow {

  meta {
    keywords: '{"keywords": ["IBD", "variant-filtering"]}'
    name: 'Filter vcf for IBD analysis'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Filters vcf file for IBD regions identification'
    changes: '{"latest": "no changes"}'

    input_vcf_gz : '{"name": "Vcf gz", "type": "File", "description": "VCF file, bgzipped and indexed"}'
    input_vcf_gz_tbi : '{"name": "Index for the input vcf", "type": "File", "description": "Tabix index for input vcf file"}'
    input_vcf_basename : '{"name": "Vcf file basename", "type": "String", "description": "Basename of the output file"}'
    input_chr : '{"name": "Chromosome", "type": "String", "values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX"], "description": "Restrict analysis to a chosen chromosome."}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_filtered_for_ibd_vcf_gz: '{"name": "Filtered for ibd vcf", "type": "File", "copy": "True", "description": "Filtered vcf, needed for ibd regions identicitation"}'
    output_filtered_for_ibd_vcf_gz_tbi:'{"name": "Index for the filtered vcf file", "type": "File", "copy": "True", "description": "filtered vcf index"}'
  }

  call filter_vcf_for_ibd

}

task filter_vcf_for_ibd {

  String task_name = "filter_vcf_for_ibd"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  
  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename = "no-id"
  String chr
  String pseudoautosomal_regions = if chr == "chrX" then "-t ^chrX:10001-2781479,chrX:155701383-156030895" else ""
  
  String docker_image = "intelliseqngs/task_vcf-anno-freq-genome:0.2.0-" + chr


  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  vcf_dir=$(dirname "${vcf_gz}")
  tbi_name=$(basename "${vcf_gz_tbi}")
  if [ ! -f $vcf_dir/$tbi_name ]; then
    ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
  fi

  ### removal of multiallelic sites 
  bcftools view -M 2  ${vcf_gz} ${pseudoautosomal_regions} -o ${chr}.filtered1.vcf.gz -O z
  tabix -p vcf ${chr}.filtered1.vcf.gz

  ### annotation with gnomad genomes v3 frequencies
  frequencies_vcf=$( ls /resources/frequencies/merged-frequencies-genome/*/${chr}.frequencies.vcf.gz )
  bcftools annotate \
           --columns INFO/ISEQ_GNOMAD_GENOMES_V3_AF \
           --annotation $frequencies_vcf ${chr}.filtered1.vcf.gz \
           -O z -o ${chr}.filtered1-with-freq.vcf.gz

  tabix -p vcf ${chr}.filtered1-with-freq.vcf.gz  

  ### filtering frequencies and coverage
  bcftools filter -i 'FORMAT/DP[0] > 4 && INFO/ISEQ_GNOMAD_GENOMES_V3_AF > 0.04 && INFO/ISEQ_GNOMAD_GENOMES_V3_AF < 0.96' \
                  ${chr}.filtered1-with-freq.vcf.gz -O z -o ${chr}-${vcf_basename}_filtered-for-ibd.vcf.gz


  tabix -p vcf ${chr}-${vcf_basename}_filtered-for-ibd.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                          --task-name-with-index ${task_name_with_index} \
                                          --task-version ${task_version} \
                                          --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File filtered_for_ibd_vcf_gz = "${chr}-${vcf_basename}_filtered-for-ibd.vcf.gz"
    File filtered_for_ibd_vcf_gz_tbi = "${chr}-${vcf_basename}_filtered-for-ibd.vcf.gz.tbi"

  }

}
