workflow rna_seq_ensembl_anno_workflow {

  meta {
    keywords: '{"keywords": ["ensembl", "rna", "seq"]}'
    name: 'rna_seq_ensembl_anno'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: ''

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_organism_name: '{"name": "Organism name", "type": "String", "description": "Organism name"}'
    input_ensembl_version: '{"name": "Ensembl version", "type": "String", "description": "Version of ensembl database"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call rna_seq_ensembl_anno

}

task rna_seq_ensembl_anno {

  String task_name = "rna_seq_ensembl_anno"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-ensembl-anno:1.1.1"

  String organism_name = "Human"
  String ensembl_version = '104'
  Array[String]? genes_anno_list
  Array[String]? transcripts_anno_list

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ensembl-rna-seq

  python3 /intelliseqtools/rna-seq-ensembl-anno.py \
    --input-organism-name '${organism_name}' \
    --input-version '${ensembl_version}' \
    --input-genes-anno-list "${sep='" "' genes_anno_list}" \
    --input-transcripts-anno-list "${sep='" "' transcripts_anno_list}" \
    --input-versions-json "/resources/ensembl-versions.json" \
    --output-dir 'ensembl-rna-seq'



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: 1
    maxRetries: 2

  }

  output {
    File genes_ensembl_tsv = "ensembl-rna-seq/${ensembl_version}/genes-ensembl.tsv"
    File transcripts_ensembl_tsv = "ensembl-rna-seq/${ensembl_version}/transcripts-ensembl.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
