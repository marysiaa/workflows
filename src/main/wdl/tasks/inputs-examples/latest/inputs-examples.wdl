workflow inputs_examples_workflow {

  meta {
    name: 'inputs_examples'
    author: 'https://gitlab.com/Dżesika Hoinkis'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## inputs_examples \n Generic text for task'
    changes: '{latest: "no changes"}'

    input_input1: '{name: "String", type: "String"}'
    input_input2: '{name: "String with values", type: "String", constraints: {values: ["Val1", "Val2"]}}'
    input_input3: '{name: "Array of strings", type: "Array[String]"}'
    input_input4: '{name: "File", type: "File", "extension": [".fq.gz", ".fastq.gz"]}'
    input_input5: '{name: "File genepanes", type: "File", "constraints": {"extension": [".json"], "subtype": "genepanel"}}'
    input_input6: '{name: "Array of files ", type: "Array[File]", "extension": [".json"]}'
    input_input7: '{name: "Integer", type: "Int", constraints: {min: "1", max: "7"}}'
    input_input8: '{name: "Array of integer", type: "Array[Int]", constraints: {min: "1", max: "7"}}'

    output_stdout_log: '{name: "Standard out", type: "File", copy: "True", description: "Console output"}'
    output_stderr_err: '{name: "Standard err", type: "File", copy: "True", description: "Console stderr"}'
    output_bco: '{name: "Biocompute object", type: "File", copy: "True", description: "Biocompute object"}'
  }

  call inputs_examples

}

task inputs_examples {

  String input1 = "test"
  String input2 = "Val1"
  Array[String] input3 = ["test1", "test2"]
  File input4
  File input5
  Array[File] input6
  Int input7 = 3
  Array[Int] input8 = [2, 4]


  String task_name = "inputs_examples"
  String task_version = "latest"
  String docker_image = "intelliseqngs/ubuntu-minimal:18.04_v0.2"

  command <<<
  task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/after-start.sh)



  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
