workflow pgx_merge_jsons_query_workflow
{
  call pgx_merge_jsons_query
}

task pgx_merge_jsons_query {

  Array[File] queries

  String task_name = "pgx_merge_jsons_query"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-merge-jsons-query:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir queries/
    cp ${sep=" " queries} queries/

    /intelliseqtools/pgx-merge-jsons-query.py --input-directory "queries" \
                                              --output-directory "."

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File pharmgkb_query = "pharmgkb.json"
    File openpgx_query = "openpgx.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
