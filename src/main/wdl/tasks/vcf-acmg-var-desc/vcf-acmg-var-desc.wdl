workflow vcf_acmg_var_desc_workflow {

  meta {
    keywords: '{"keywords": ["vcf",  "acmg"]}'
    name: 'Vcf variant description'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## Gathers ACMG descriptions'
    changes: '{"latest": "no changes"}'

    input_acmg_vcf: '{"name": "ACMG vcf", "type": "File", "extension": [".vcf"], "description": "Annotated vcf"}'
    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Identifier of sample"}'

    output_annotated_acmg_vcf_gz: '{"name": "annotated vcf", "type": "Files", "description": "Vcf file annotated with full ACMG description"}'
    output_annotated_acmg_vcf_gz_tbi: '{"name": "tbi file for annotated vcf", "type": "Files", "description": "Tbi file for annotated vcf"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_acmg_var_desc

}

task vcf_acmg_var_desc {


  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no_id_provided"

  String task_name = "vcf_acmg_var_desc"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/vcf-acmg-var-desc:1.0.0"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  python3 /intelliseqtools/variant-descriptor.py -i ${vcf_gz} > ${sample_id}_annotated-with-acmg.vcf
  bgzip -c ${sample_id}_annotated-with-acmg.vcf > ${sample_id}_annotated-with-acmg.vcf.gz
  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File annotated_vardescription_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_vardescription_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
