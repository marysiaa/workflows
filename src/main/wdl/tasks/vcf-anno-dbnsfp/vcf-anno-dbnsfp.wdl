workflow vcf_anno_dbnsfp_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "vcf-annoattion", "dbnsbp", "snpSift"]}'
    name: 'vcf_anno_dbnsfp'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Annotating vcf with dbnsnp database'
    changes: '{"1.4.0": "dbNSFP database updated to v4.2c, bug fix for all needed fields (the same vcf header Number for all chromosomes)", "1.3.0": "needed fields defined", "1.2.1": "bug fix: 1000g AC field is now described as int for all chromosomes", "1.2.0": "new database (dbNSFP4.1c), latest dir rm, set-e -o pipeline added"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_vcf_gz: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"], "description": "Chromosome for analysis"}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "no_id_provided", "description": "Sample ID"}'

    output_annotate_vcf_with_dbnsfp_vcf_gz :'{"name": "annotated vcf", "type": "File"}'
    output_annotate_vcf_with_dbnsfp_vcf_gz_tbi :'{"name": "annotated vcf index", "type": "File"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
   
  }

  call vcf_anno_dbnsfp

}

task vcf_anno_dbnsfp {

  String task_name = "vcf_anno_dbnsfp"
  String task_version = "1.4.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  
  # Input VCF file must:
  # - be bgzipped
  # - have left-normalized indels (bcftools norm)
  # - multiallelic sites split (bcftools norm --multiallelics -any)")
  File vcf_gz
  File vcf_gz_tbi

  String database_version = "dbNSFP4.2c"
  String chromosome
  String docker_image = "intelliseqngs/task_vcf-anno-dbnsfp:" + "1.2.0-" + chromosome

  String vcf_basename = "no_id_provided"

  String python_out_mod = "/intelliseqtools/change-dbnsfp-output.py"
  String vcf_prefix = chromosome + "-" + vcf_basename
  String fields_to_add1 = "BayesDel_addAF_score,DANN_score,DEOGEN2_score,fathmm-MKL_coding_score,LIST-S2_score,M-CAP_score"
  String fields_to_add2 = "MVP_score,PrimateAI_score,MutationAssessor_score,SIFT4G_score,Eigen-raw_coding,Eigen-PC-raw_coding,MutationTaster_score"
  String fields_to_add4 = "Aloft_Fraction_transcripts_affected,Aloft_prob_Tolerant,Aloft_prob_Recessive,Aloft_prob_Dominant,Aloft_pred,Aloft_Confidence"
  String fields_to_add3 = "APPRIS,GENCODE_basic"

  String snpsift_java_mem = "-Xmx4g"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  db_version=$( echo ${database_version} | sed 's/dbNSFP/v/')

  java -jar ${snpsift_java_mem} /tools/snpEff/SnpSift.jar dbnsfp -v  \
    -f ${fields_to_add1},${fields_to_add2},${fields_to_add3},${fields_to_add4} -db \
  \/resources/dbnsfp/${database_version}/${database_version}_variant.${chromosome}.gz ${vcf_gz} \
  | python3 ${python_out_mod} -d $db_version | bgzip > ${vcf_prefix}_annotated-with-dbnsfp.vcf.gz

  tabix -p vcf ${vcf_prefix}_annotated-with-dbnsfp.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "5G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    
    File annotate_vcf_with_dbnsfp_vcf_gz = "${vcf_prefix}_annotated-with-dbnsfp.vcf.gz"
    File annotate_vcf_with_dbnsfp_vcf_gz_tbi = "${vcf_prefix}_annotated-with-dbnsfp.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
