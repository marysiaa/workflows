workflow pgx_polygenic_haplotype_workflow
{
  call pgx_polygenic_haplotype
}

task pgx_polygenic_haplotype {

  File vcf
  Array[String]? selected_genes
  String sample_id = "no_id_provided"
  String log_file_name = "pgx-polygenic.log"

  String task_name = "pgx_polygenic_haplotype"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-polygenic-haplotype:1.0.1"

  command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      # Index VCF
      tabix -p vcf ${vcf}
      # Create output directory
      mkdir output/

      # Run polygenic for all models in /resources/models/ directory
      
      if [ -n "${sep=" " selected_genes}" ]; then
        lower_case_genes=$(echo "${sep="|" selected_genes}" | tr '[:upper:]' '[:lower:]')
        export testmodels=$(find /resources/models/ -maxdepth 1 -type f -not -path '*/\.*' | sort | grep -wE "$lower_case_genes" | tr '\n' ' ')
      else
        export testmodels=$(find /resources/models/ -maxdepth 1 -type f -not -path '*/\.*' | sort | tr '\n' ' ')
      fi

      pgstk --log-file ${log_file_name} pgs-compute \
            --vcf ${vcf} \
            --model $testmodels \
            --output-directory output
      
      # Create JSONs for OpenPGx and PharmGKB queries and merge all input models
      python3 /intelliseqtools/pgx-polygenic-create-jsons.py --input-directory "output" \
                                                             --output-directory "."


    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File merged_models = "merged_models.json"
    File openpgx_queries = "openpgx_queries.json"
    File pharmgkb_queries = "pharmgkb_queries.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
