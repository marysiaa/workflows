workflow vcf_acmg_ba1_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "BA1", "variant", "benign"]}'
    name: 'Vcf ACMG BA1'
    author: 'https://gitlab.com/gleblavr https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Evaluates whether variant fulfils ACMG BA1 criterion, adds annotation'
    changes: '{"1.2.2": "update ubuntu and pysam", "1.2.1": "update cython and pysam", "1.2.0": "uses gnomAD popmax fields", "1.1.0": "Script updated, set -e added, latest removed"}'
    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Identifier of sample"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf"], "description": "Input annotated vcf"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True", "type": "File", "description": "Vcf with ACMG BA1 annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_acmg_ba1

}

task vcf_acmg_ba1 {

  String task_name = "vcf_acmg_ba1"
  String task_version = "1.2.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-ba1:1.2.0"

  File vcf
  String sample_id = "no-id"

  command <<<
  set -e -o pipefail

  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  #acmg annotation

  python3 /intelliseqtools/acmg-ba1.py  \
         --input-vcf ${vcf}  \
         --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz



  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}

  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }
}
