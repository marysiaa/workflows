workflow mt_anno_workflow
{
  call mt_anno
}

task mt_anno {

  String task_name = "mt_anno"
  String task_version = "1.0.1"

  File vcf
  File tbi
  String sample_id = "no_id_provided"
  String broad_resources_version = "v0"
  String ref_fasta = "/resources/broad-institute-hg38-mt-reference/*/Homo_sapiens_assembly38.chrM.fasta"
  String dbsnp = "/resources/dbsnp/build*/dbsnp-mt.vcf.gz"  

  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_mt-anno:1.0.0"

  command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    ## normalize vcf and add rsID
    bcftools norm -m -any -f ${ref_fasta} ${vcf} -o tmp.vcf.gz -O z
    tabix -p vcf tmp.vcf.gz
    
    bcftools annotate tmp.vcf.gz -c "INFO/ISEQ_DBSNP_RS" -a ${dbsnp} \
    -o ${sample_id}_norm-dbsnp.vcf.gz -O z
    tabix -p vcf ${sample_id}_norm-dbsnp.vcf.gz

    rm tmp.vcf.gz*

    ## prepare table
    bcftools query -f "%CHROM\t%POS\t%REF\t%ALT\t%FILTER\t%INFO/ISEQ_DBSNP_RS[\t%AF][\t%AD]\n" ${sample_id}_norm-dbsnp.vcf.gz \
    | sed 's/,/\t/' > tmp.tsv
    printf "CHROM\tPOS\tREF\tALT\tFILTER\tRSID\tALT ALLELE FRACTION\tREF ALLELE DEPTH\tALT ALLELE DEPTH\n" \
    | cat - tmp.tsv >  ${sample_id}.tsv


    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File annotated_vcf = "${sample_id}_norm-dbsnp.vcf.gz"
    File annotated_vcf_tbi = "${sample_id}_norm-dbsnp.vcf.gz.tbi"
    File annotated_tsv = "${sample_id}.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
