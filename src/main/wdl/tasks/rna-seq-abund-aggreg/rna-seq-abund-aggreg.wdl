workflow rna_seq_abund_aggreg_workflow
{
  call rna_seq_abund_aggreg
}

task rna_seq_abund_aggreg {

  Array[File] htseq_count_gene
  String htseq_count_gene_tbl_suffix = "-gene-level"
  Array[File] htseq_count_exon
  String htseq_count_exon_tbl_suffix = "-exon-level"
  String analysis_id = "no_id_provided"

  String task_name = "rna_seq_abund_aggreg"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-abund-aggreg:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    python3 /intelliseqtools/abund_aggreg.py --count-files ${sep=" " htseq_count_gene} \
                                             --tbl-suffix "gene" \
                                             --output-file-name ${analysis_id}_htseq-count${htseq_count_gene_tbl_suffix}.tsv

    python3 /intelliseqtools/abund_aggreg.py --count-files ${sep=" " htseq_count_exon} \
                                             --tbl-suffix "exon" \
                                             --output-file-name ${analysis_id}_htseq-count${htseq_count_exon_tbl_suffix}.tsv

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File htseq_count_gene_level = "${analysis_id}_htseq-count${htseq_count_gene_tbl_suffix}.tsv"
    File htseq_count_exon_level = "${analysis_id}_htseq-count${htseq_count_exon_tbl_suffix}.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
