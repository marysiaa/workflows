workflow fq_qc_mapping_stat_workflow {

  meta {
    name: 'fq-qc-mapping-stats'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Aligning first 100000 reads of fastq_1 and fastq_2 with BWA MEM, calculating data from samtools flagstat and checking percentage of reads with mate mapped to a different chromosome (can indicates quality). \\n Output lines of ${sample_id}.${filename}.estimation-of-mapping-statistics.tx [number of reads...]:'
    changes: '{latest: "no changes"}'
    input_fastq_1: '{name: "fastq 1", type: "File", constraints: {extension: ["fq.gz"]}, description: "first fastq file"}'
    input_fastq_2: '{name: "fastq 2", type: "File", constraints: {extension: ["fq.gz"]}, description: "second fastq file"}'
    input_sample_id: '{name: "sample id", type: "String", description: "identifier of sample"}'
    output_stdout_log: '{name: "Standard out", type: "File", copy: "True", description: "Console output"}'
    output_stderr_err: '{name: "Standard err", type: "File", copy: "True", description: "Console stderr"}'
    output_bco: '{name: "Biocompute object", type: "File", copy: "True", description: "Biocompute object"}'
    output_fastqc_files: '{name: "fastqc report in html", type: "Files", description: "html reports and tar archive for fastq_1 and fastq_2"}'
  }

  call fq_qc_mapping_stats
}

task fq_qc_mapping_stats {

  File fastq_1
  File fastq_2

  String lane = 0

  # Inputs with defaults
  String PRAC = '85'
  String sample_id = "no-id"

  # Docker image
  String docker_image = "intelliseqngs/bwa-mem:v1.4"

  # Task name and task version
  String task_name = "estimation_of_mapping_statistics"
  String task_version = "v0.1"

  command <<<

      task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
      source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/after-start.sh)

    # Task info #
    # task_info first, because `set -o pipefail` trigger error in `bwa 2>&1 ...`

    # Task info # <-- end

    #//  1. take first 100000 reads of fastq_1 and fastq_2
  	zcat -f ${fastq_1}  | head -400000 > fastq_1_part.fq
    zcat -f ${fastq_2}  | head -400000 > fastq_2_part.fq

    #//  2. prepare reference genome
    gunzip /resources/Homo_sapiens_assembly/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz

    #//  3. align with BWA MEM
    set -o pipefail
    set -e

    bwa mem \
      -t 2 \
      -K 10000000 \
      -v 3 \
      -Y /resources/Homo_sapiens_assembly/broad-institute-hg38/Homo_sapiens_assembly38.fa \
      fastq_1_part.fq fastq_2_part.fq  \
      | samblaster  \
      | samtools sort -@ 2 - > ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.markdup.bam

    #//  4. Calculate statistics
    samtools flagstat ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.markdup.bam > ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt

    #//  5. Extra stats: properly mapped: The number of reads that mapped to the reference genome and within the expected insert size (percentage).

        # under construction #
    #//  6. Checking the quality - percentage of reads with mate mapped to a different chromosome
    ALL=`awk '{print $1}' ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt | head -1` #how many reads in total
    BAD=`awk '{print $1}' ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt | tail -2 | head -1` #how many reads with mate mapped to a different chromosome
    STAT=`echo $BAD $ALL | awk '{print 100*(1-$1/$2)}'` #percentage*100 of reads with mate mapped to the same chromosome
    DIFF=`echo ${PRAC} $STAT | awk '{print $2-$1}'`

    if [[ $DIFF > 0 ]]; then
        echo "PASSED" > estimation_status
    else
        echo "FAILED" > estimation_status
    fi

    #//  7. print statistics in json file

    #// 7. create json with statistics
    cat ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt                          |awk '{print $1 " failed=" $3}' > basic.txt
    first=`cat ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt | sed -n '5p'     |awk '{print $5 " failed=" $7}' | sed 's/[()]//g' `
    second=`cat ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt | sed -n '9p'   |awk '{print $6 " failed=" $8}' | sed 's/[()]//g' `
    third=`cat ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt | sed -n '11p'   |awk '{print $5 " failed=" $7}' | sed 's/[()]//g' `
    sed -n -i "p;5a $first"  basic.txt
    sed -n -i "p;10a $second"  basic.txt
    sed -n -i "p;15a $second"  basic.txt
    #for i in {1..16}; do  "stat$i"=`cat basic.txt | sed -n "$i p"`; done
    #nie wiem czemu ta pętla nie działa więc pod spodem jest spaghetti code który robi to dobrze. declare?

     stat1=`cat basic.txt | sed -n "1p"`
     stat2=`cat basic.txt | sed -n "2p"`
     stat3=`cat basic.txt | sed -n "3p"`
     stat4=`cat basic.txt | sed -n "4p"`
     stat5=`cat basic.txt | sed -n "5p"`
     stat6=`cat basic.txt | sed -n "6p"`
     stat7=`cat basic.txt | sed -n "7p"`
     stat8=`cat basic.txt | sed -n "8p"`
     stat9=`cat basic.txt | sed -n "9p"`
     stat10=`cat basic.txt | sed -n "10p"`
     stat11=`cat basic.txt | sed -n "11p"`
     stat12=`cat basic.txt | sed -n "12p"`
     stat13=`cat basic.txt | sed -n "13p"`
     stat14=`cat basic.txt | sed -n "14p"`
     stat15=`cat basic.txt | sed -n "15p"`
     stat16=`cat basic.txt | sed -n "16p"`

    echo \[ >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="in total" passed=$stat1 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="secondary" passed=$stat2 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="supplementary" passed=$stat3 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="duplicates" passed=$stat4 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="mapped" passed=$stat5 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="mapped%" passed=$stat6 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="paired in sequencing" passed=$stat7 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="read1" passed=$stat8 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="read2" passed=$stat9 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="properly paired" passed=$stat10 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="properly paired%" passed=$stat11 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="with itself and mate mapped" passed=$stat12 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="singletons" passed=$stat13 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="singletons%" passed=$stat14 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="with mate mapped to a different chr" passed=$stat15 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="with mate mapped to a different chr%" passed=$stat16 >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    jo name="percentage of reads with mate mapped to the same chromosome" value=$STAT >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    echo \] >>"${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"



#    cat ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt| sed 's/+/\n/' | sed 's/:/\n/' | sed 's/mapped (/\n/' | sed 's/paired (/\n/' | sed 's/singletons (/\n/' |sed 's/read[1,2]//'| sed 's/>=5//' | sed 's/[a-zA-Z]//g'| sed 's/[(,),+]//g' | sed 's/[\-]//g' | sed 's/\//N\/A/g' | sed 's/ //g' >  ${sample_id}_lane_${lane}.estimation-of-mapping-statistics_reformated.statistics.txt
#    echo $STAT >> ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt
#    awk 'FNR==NR{a[FNR]=$0;next}{print a[FNR],$0}' names_for_statistics.txt ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt |sed 's/= /=/' | jo -p >  ${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/before-finish.sh)

  >>>

  runtime {

    maxRetries: 3
    docker: docker_image
    memory: "1G"
    cpu: "1"

  }


  output {

    #String estimation_status = read_string("estimation_status")
    File estimation_of_mapping_statistics_txt = "${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.txt"
    File estimation_of_mapping_statistics_json = "${sample_id}_lane_${lane}.estimation-of-mapping-statistics.statistics.json"
    # zamienić na json
    File stderr_log = stderr()
    File stdout_log = stdout()
    File bco = "bco.json"

 }
}
