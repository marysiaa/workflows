workflow sv_annot_filter_workflow {

  meta {
    keywords: '{"keywords": ["SV", "panel", "effect", "frequency", "filtering"]}'
    name: 'Annotated SV vcf filtering'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Filters annotated SV. Applies gene-panel, frequency and ISEQ rank filters.'
    changes: '{"1.2.2": "Removed freq filter inputs from meta", "1.2.1": "bug fixed (wrong vcf field name)", "1.2.0": "frequency filtering removed, changed panel filter","1.1.0": "panel annotation moved to other task", "1.0.2": "dealing with empty vcf - without annotSV annotations", "1.0.1": "bcftools command fixed"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "SV vcf gz", "type": "File", "description": "Annotated SV vcf"}'
    input_vcf_gz_tbi: '{"name": "SV vcf gz", "type": "File", "description": "Index for the annotated SV vcf"}'
    input_apply_panel_filter: '{"name": "Apply filtering on gene panel?", "type": "Boolean", "default": "true", "description": "Determines whether panel filter should be applied"}'
    input_panel_filter_type: '{"name": "Panel filter type", "type": "String", "constraints": {"values": ["panel_affected", "panel_overlap"]}, "default": "panel_overlap", "description": "Determines filter to apply"}'
    input_apply_rank_filter: '{"name": "Apply rank filtering?", "type": "Boolean", "description": "Determines whether SV variants should be filtered depending on their predicted effect"}'
    input_rank_threshold: '{"name": "Rank threshold", "type": "String", "constraints": {"values": ["likely_benign", "uncertain", "likely_pathogenic", "pathogenic"]}, "default": "uncertain", "description": "SV with rang equal to given value or more severe will be kept"}'

    output_filtered_vcf_gz: '{"name": "Filtered SV vcf gz", "type": "File", "copy": "True", "description": "Filtered SV vcf"}'
    output_filtered_vcf_gz_tbi: '{"name": "Filtered SV vcf gz", "type": "File", "copy": "True", "description": "Index for the filtered SV vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
     }

  call sv_annot_filter

}

task sv_annot_filter {

  String task_name = "sv_annot_filter"
  String task_version = "1.2.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.4"

  String sample_id = "no-id"
  File vcf_gz
  File vcf_gz_tbi

  Boolean apply_panel_filter = true
  Boolean apply_rank_filter = false
  String rank_threshold = "uncertain"

   command <<<

   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   set -e -o pipefail

   if zgrep -q -v '^#' ${vcf_gz}; then

     # filter on gene panel (or remove SV without any gene overlap)
     if ${apply_panel_filter}; then
        bcftools filter -e 'INFO/ISEQ_GENE_PANEL_OVERLAP="."' \
                       ${vcf_gz} \
                      -O z -o tmp1.vcf.gz

        tabix -p vcf tmp1.vcf.gz

     else

          mv ${vcf_gz} tmp1.vcf.gz
          mv ${vcf_gz_tbi} tmp1.vcf.gz.tbi
     fi

     # Variant classification filter
     if ${apply_rank_filter};then

        if [ ${rank_threshold} = likely_benign ]; then
           bcftools filter -i 'INFO/ACMG_class = "Likely^benign,Uncertain,Likely^pathogenic,Pathogenic"' tmp1.vcf.gz -O z -o ${sample_id}_annotated-filtered-sv.vcf.gz
        elif [ ${rank_threshold} = uncertain ]; then
           bcftools filter -i 'INFO/ACMG_class = "Uncertain,Likely^pathogenic,Pathogenic"' tmp1.vcf.gz -O z -o ${sample_id}_annotated-filtered-sv.vcf.gz
        elif [ ${rank_threshold} = likely_pathogenic ]; then
           bcftools filter -i 'INFO/ACMG_class = "Likely^pathogenic,Pathogenic"' tmp1.vcf.gz -O z -o ${sample_id}_annotated-filtered-sv.vcf.gz
        elif [ ${rank_threshold} = pathogenic ]; then
           bcftools filter -i 'INFO/ACMG_class = "Pathogenic"' tmp1.vcf.gz -O z -o ${sample_id}_annotated-filtered-sv.vcf.gz
        else
           mv tmp1.vcf.gz ${sample_id}_annotated-filtered-sv.vcf.gz
           echo "Rank filtering not performed: bad rank_threshold value"
        fi
        tabix -p vcf ${sample_id}_annotated-filtered-sv.vcf.gz
     else
          mv tmp1.vcf.gz ${sample_id}_annotated-filtered-sv.vcf.gz
          mv tmp1.vcf.gz.tbi ${sample_id}_annotated-filtered-sv.vcf.gz.tbi
     fi

   else
      mv ${vcf_gz} ${sample_id}_annotated-filtered-sv.vcf.gz
      mv ${vcf_gz_tbi} ${sample_id}_annotated-filtered-sv.vcf.gz.tbi
   fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File filtered_vcf_gz = "${sample_id}_annotated-filtered-sv.vcf.gz"
    File filtered_vcf_gz_tbi = "${sample_id}_annotated-filtered-sv.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}