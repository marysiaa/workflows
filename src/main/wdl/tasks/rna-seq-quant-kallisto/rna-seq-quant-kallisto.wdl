workflow rna_seq_quant_kallisto_workflow
{
  call rna_seq_quant_kallisto
}

task rna_seq_quant_kallisto {

  File transcriptome_index
  File fastq_1
  File? fastq_2
  File ensembl_gtf
  File chromosomes

  Boolean is_paired_end = true

  String stranded = "unstranded" # Possible values include: unstranded, stranded and reversely stranded
  String stranded_argument = if stranded == "unstranded" then "" else if stranded == "stranded" then "--fr-stranded" else "--rf-stranded"

  # for single_end
  Float fragment_length = 200
  Float fragment_length_sd = 20

  Int num_threads = 4
  String sample_id = "no_id_provided"

  String task_name = "rna_seq_quant_kallisto"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/kallisto:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      mkdir results

      if [ "${is_paired_end}" = true ] ; then
          kallisto quant -t ${num_threads} -i ${transcriptome_index} -o results ${stranded_argument} \
            --genomebam --gtf ${ensembl_gtf} --chromosomes ${chromosomes} \
            ${fastq_1} ${fastq_2}
      else
          kallisto quant -t ${num_threads} -i ${transcriptome_index} -o results ${stranded_argument} \
            --single --fragment-length 200 --sd 20 \
            --genomebam --gtf ${ensembl_gtf} --chromosomes ${chromosomes} \
            ${fastq_1}
      fi
      mv results/abundance.tsv results/${sample_id}.tsv
      awk '(NR==1 && $3=="eff_length"){sub("eff_length","${sample_id}",$0);} 1' results/${sample_id}.tsv | cut -f 1,3 > results/${sample_id}_eff_length.tsv
      awk '(NR==1 && $4=="est_counts"){sub("est_counts","${sample_id}",$0);} 1' results/${sample_id}.tsv | cut -f 1,4 > results/${sample_id}_est_counts.tsv
      awk '(NR==1 && $5=="tpm"){sub("tpm","${sample_id}",$0);} 1' results/${sample_id}.tsv | cut -f 1,5 > results/${sample_id}_tpm.tsv

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File eff_length_file = "results/${sample_id}_eff_length.tsv"
    File est_counts_file = "results/${sample_id}_est_counts.tsv"
    File tpm_file = "results/${sample_id}_tpm.tsv"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
