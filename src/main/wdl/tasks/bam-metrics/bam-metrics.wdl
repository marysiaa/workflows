workflow bam_metrics_workflow {

  meta {
    keywords: '{"keywords": ["gatk", "samtools", "coverage metrics"]}'
    name: 'bam_metrics'
    author: 'https://gitlab.com/mremre, https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Collects coverage metrics using gatk and samtools to 2 jsons (with whole statistics and with human readable statistics with some additional info).'
    changes: '{"1.4.3": "fix dup metrics bam name", "1.4.2": "deal with empty dup metrics file", "1.4.1": "new docker", "1.4.0-bam-metrics-bug-fix": "change read length in collectwgsmetrics", "1.4.0": "added insert size analysis, output tables for multiqc", "1.3.0": "target possible", "1.2.7": "possible genome choice", "1.2.6": "changed ram"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_reference_genome: '{"name": "reference genome, "type": "String", "constraints": {"values": ["hg38", "grch38-no-alt"]}, "description": "Reference fasta file"}'
    input_intervals: '{"name": "intervals", "type": "File", "extension": [".interval_list"], "description": "interval list (needed for WGS analysis)"}'
    input_custom_intervals: '{"name": "Custom intervals", "type": "File", "extension": [".interval_list"], "description": "interval list (needed for target analysis)"}'
    input_target: '{"name": "target", "type": "File", "extension": [".interval_list"], "description": "target"}'
    input_bait: '{"name": "bait", "type": "File", "extension": [".interval_list"], "description": "bait"}'
    input_bam: '{"name": "bam", "type": "File", "extension": [".bam"], "description": "Alignment result"}'
    input_bai: '{"name": "bai", "type": "File", "extension": [".bai"], "description": "Index for the bam file"}'
    input_kit: '{"name": "Kit", "type": "String",  "constraints": {"values": ["exome-v6", "exome-v7", "genome", "target"]}, "description": ""}'
    input_dupl_metrics: '{"name": "Duplication metrics", "type": "File", "extension": [".txt", ".tsv"], "description": "Picard MarkDuplicates metrics file"}'
    input_use_fast_algorithm: '{"name": "Use fast algorithm?", "type": "Boolean", "description": "Decides whether use fast algorithm (CollectWGSMetrics)"}'

    output_bam_metrics: '{"name": "final_json.csv", "type": "Array[File]", "description": "Metrics files"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File",  "description": "Biocompute object"}'

  }

  call bam_metrics

}

task bam_metrics {

  File? intervals
  File? custom_intervals
  File? target
  File? bait
  File? dupl_metrics
  Boolean dupl_metrics_empty = false
  Boolean dupl_metrics_not_given = if defined(dupl_metrics) then false else true
  Boolean use_fast_algorithm = true

  File bam
  File? bai

  String kit
  File? intervals_to_use  = if (kit == "genome") then intervals else custom_intervals

  String reference_genome = "grch38-no-alt"
  String ref_path = "`ls /resources/reference-genomes/*/*.fa`"
  String sample_id = "no_id_provided"
  String java_options = "-Xms4g -Xmx62g"
  String bam_name = "variant-calling-ready"

  String task_name = "bam_metrics"
  String task_version = "1.4.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0-"+ reference_genome + ":1.1.0"

  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  mkdir results

  #check if the input bam is not empty (multiqc fails on empty metrics files)
  if samtools view ${bam} | grep -q '^'; then
      read_length=$( samtools view ${bam}  | head -100000 | awk '{len[length($10)] = len[length($10)] + 1} END {max_key = 0; for(key in len) {if(len[max_key] < len[key]) max_key = key} print max_key}' )

      set -e -o pipefail

      # for WES, target and  WGS #
      gatk CollectAlignmentSummaryMetrics \
          --java-options "${java_options}" \
          -R ${ref_path} \
          --INPUT ${bam} \
          --OUTPUT results/${sample_id}-CollectAlignmentSummaryMetrics.txt \
          --VALIDATION_STRINGENCY SILENT

      gatk CollectInsertSizeMetrics \
          --java-options "${java_options}" \
          --INPUT ${bam} \
          --OUTPUT results/${sample_id}-CollectInsertSizeMetrics.txt \
          -H ${sample_id}-histogram.pdf \
          --VALIDATION_STRINGENCY SILENT


      if ${dupl_metrics_not_given} || ${dupl_metrics_empty}; then
          gatk MarkDuplicates \
              --java-options "${java_options}" \
              -R ${ref_path} \
              --INPUT ${bam} \
              --OUTPUT to-remove-${sample_id}-MarkDuplicates.bam \
              -M results/${sample_id}-MarkDuplicates.txt \
              --VALIDATION_STRINGENCY SILENT

          rm to-remove-${sample_id}-MarkDuplicates.bam
      else
          sed "s/--INPUT .*.bam /--INPUT ${sample_id}_${bam_name}.bam /g" ${dupl_metrics} \
          > results/${sample_id}-MarkDuplicates.txt
      fi

      if [ ${kit} = "genome" ] || [ ${kit} = "target" ]; then
          gatk CollectWgsMetrics \
              --java-options "${java_options}" \
              -R ${ref_path} \
              --INPUT ${bam} \
              --READ_LENGTH $read_length \
              --OUTPUT results/${sample_id}-CollectWgsMetrics.txt \
              --INTERVALS ${intervals_to_use} \
              ${true="--USE_FAST_ALGORITHM true" false="--USE_FAST_ALGORITHM false" use_fast_algorithm} \
              --VALIDATION_STRINGENCY SILENT

          gatk CollectQualityYieldMetrics \
              --java-options "${java_options}" \
              -R ${ref_path} \
              --INPUT ${bam} \
              --OUTPUT results/${sample_id}-CollectQualityYieldMetrics.txt \
              --VALIDATION_STRINGENCY SILENT


      elif [ ${kit} = "exome-v7" ] || [ ${kit} = "exome-v6" ] || [ ${kit} = "exome-v8" ]; then
          gatk CollectHsMetrics \
              --java-options "${java_options}" \
              --INPUT ${bam} \
              --OUTPUT results/${sample_id}-CollectHsMetrics.txt \
              -R ${ref_path} \
              -BI ${bait} \
              -TI ${target} \
              --VALIDATION_STRINGENCY SILENT

      fi
  fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    disks: "local-disk 1500 LOCAL"
    docker: docker_image
    memory: "64G"
    cpu: "8"
    maxRetries: 2

  }

  output {
    Array[File] bam_metrics = glob("results/*.txt")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }
}
