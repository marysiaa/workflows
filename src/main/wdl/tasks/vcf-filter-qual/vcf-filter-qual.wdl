workflow vcf_filter_qual_workflow {
    call vcf_filter_qual
}

task vcf_filter_qual {

    File vcf_gz
    File vcf_gz_tbi
    String vcf_basename = "no_id_provided"
    Float quality_threshold = 300
    Int qual = floor(quality_threshold)

    String task_name = "vcf_filter_qual"
    String task_version = "1.0.1"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.7"

    command <<<
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
    set -e -o pipefail

        # Tbi could be in different directory than vcf file, the symlink below fixes it
        vcf_dir=$(dirname "${vcf_gz}")
        tbi_name=$(basename "${vcf_gz_tbi}")
        if [ ! -f $vcf_dir/$tbi_name ]; then
            ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
        fi
        if [ ${qual} -gt 0 ]; then
            bcftools filter ${vcf_gz} -e 'QUAL <= ${qual}' -o ${vcf_basename}_filtered-by-qual.vcf.gz -O z
            tabix -p vcf ${vcf_basename}_filtered-by-qual.vcf.gz
        else
            cp ${vcf_gz} ${vcf_basename}_filtered-by-qual.vcf.gz
            tabix -p vcf ${vcf_basename}_filtered-by-qual.vcf.gz
        fi


    bash /intelliseqtools/bco-before-finish.sh  --task-name ${task_name} \
                                                --task-name-with-index ${task_name_with_index} \
                                                --task-version ${task_version} \
                                                --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: "1"
        maxRetries: 2
    }

    output {

        File filtered_by_qual_vcf_gz = "${vcf_basename}_filtered-by-qual.vcf.gz"
        File filtered_by_qual_vcf_gz_tbi = "${vcf_basename}_filtered-by-qual.vcf.gz.tbi"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
