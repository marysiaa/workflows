workflow genotype_gvcf_on_dbsnp_positions_workflow {

  meta {
    keywords: '{"keywords": ["dbsnp", "genotyping", "gvcf"]}'
    name: 'Genotype gvcf on dbsnp positions'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Genotypes gvcf on dbsnp positions'
    changes: '{"1.0.2": "docker gatk update", "1.0.1": "more processors to comply with GCE custom machine requirements, slightly more max memory used by JVM"}'
    input_gvcf_gz: '{"name": "gVCF file (bgzipped)", "type": "File", "extension": [".g.vcf.gz"], "description": "Bgzipped gVCF file containing information for one sample."}'
    input_gvcf_gz_tbi: '{"name": "gVCF file (bgzipped) index", "type": "File", "extension": [".g.vcf.gz.tbi"], "description": "Tabix index of bgzipped gVCF file"}'
    input_basename: '{"name": "Files basename", "type": "String", "description": "Basename of output files"}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "constraints":{"values":["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]}, "description": "Restrict analysis to a chosen chromosome."}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "description": "Reference genome, possible values; hg38, grch38-no-alt."}'

    output_genotyped_on_dbsnp_vcf_gz: '{"name": "Output VCF (bgzipped)", "type": "Files", "description": "Output VCF file (bgzipped) containing genotyped positions from dbSNP. Lines with ./. genotypes are filtered out."}'
    output_genotyped_on_dbsnp_vcf_gz_tbi: '{"name": "Index of output VCF (bgzipped)", "type": "Files", "description": "Index of output VCF file (bgzipped)."}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call genotype_gvcf_on_dbsnp_positions

}

task genotype_gvcf_on_dbsnp_positions {

  File gvcf_gz
  File gvcf_gz_tbi
  String basename
  String chromosome
  String reference_genome = "hg38"
  Boolean old_gvcf = false

  # Tools runtime settings, paths etc.

  String dbsnp_after_merge_postprocessing_py = "/intelliseqtools/dbsnp-after-merge-postprocessing.py"

  String keep_or_remove_vcf_fields_py = "/intelliseqtools/keep-or-remove-vcf-fields.py"
  String gatk_path = "/gatk/gatk"
  String genotype_gvcfs_java_options = "-Xmx9g -Xms5g"

  String task_name = "genotype_gvcf_on_dbsnp_positions"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_genotype-gvcf-on-dbsnp-positions-" + reference_genome + ":1.0.1-" + chromosome

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  chromosome_reference_genome_fa_gz=$( ls /resources/reference-genome/*/${chromosome}*.fa.gz )
  chromosome_dbsnp_interval_bed_gz=$( ls /resources/dbsnp-for-genotyping-purposes/*/${chromosome}.dbsnp-for-genotyping.bed.gz )
  chromosome_dbsnp_vcf_gz=$( ls /resources/dbsnp-for-genotyping-purposes/*/${chromosome}.dbsnp-for-genotyping.vcf.gz )
  simple_repeats_bed_gz=$( ls /resources/ucsc-simple-repeats/*/${chromosome}.simple-repeats-6bp-or-less.bed.gz )

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  gvcf_dir=$(dirname "${gvcf_gz}")
  tbi_name=$(basename "${gvcf_gz_tbi}")
  if [ ! -f $gvcf_dir/$tbi_name ]; then
    ln -s ${gvcf_gz_tbi} $gvcf_dir/$tbi_name
  fi

  if ${old_gvcf}; then
      bcftools norm -d none ${gvcf_gz} -o ${chromosome}-${basename}_unique.g.vcf.gz -O z
      tabix -p vcf ${chromosome}-${basename}_unique.g.vcf.gz
  else
      mv ${gvcf_gz} ${chromosome}-${basename}_unique.g.vcf.gz
      mv ${gvcf_gz_tbi} ${chromosome}-${basename}_unique.g.vcf.gz.tbi
  fi

  # Genotyping
  # ----------
  #  The command in this section genotype the gVCF on every position included
  #  in dbSNP interval BED file.
  #
  #  Notes:
  #    1. option 'allow-old-rms-mapping-quality-annotation-data' is needed to
  #       genotype gVCF produced by earlier versions of GATK
  #    2. option --lenient is needed, so the error:
  #       "java.lang.IllegalStateException: Key LowQual found in VariantContext
  #       field FILTER at <chr>:<pos> but this key isn't defined in the
  #       VCFHeader. We require all VCFs to have complete VCF headers by
  #       default." will not disrupt the GenotypeGVCFs when genotyping gVCF
  #       produced by earlier versions of GATK

    ${gatk_path} --java-options "${genotype_gvcfs_java_options}" \
      GenotypeGVCFs \
      --allow-old-rms-mapping-quality-annotation-data \
      --lenient \
      --include-non-variant-sites \
      --intervals $chromosome_dbsnp_interval_bed_gz \
      --variant ${chromosome}-${basename}_unique.g.vcf.gz \
      --output ${chromosome}-${basename}.vcf.gz \
      --reference $chromosome_reference_genome_fa_gz
  
  # Filtering out not genotyped sites
  # ---------------------------------
  #  The following command formats VCF file produced by previous step
  #  and removed not genotyped posiotions.
  #
  #  Notes:
  #    1. when genotyping gVCF produced by earlier versions of GATK, the VCF
  #       without definition of "LowQual" is produced. The following command
  #       fixes it.
  #    2. PASS filter is added
  #    3. genotypes with * or <NON_REF> in ALTs are deleted


  # awk finishes task earlier than zcat and zcat returns 141 code -> final pipe exit code is not 0 
  set +o pipefail

      zcat ${chromosome}-${basename}.vcf.gz \
        | awk '{if (/^##ALT=<ID=NON_REF/) {print "##FILTER=<ID=PASS,Description=\"All filters passed\">"; print "##FILTER=<ID=LowQual,Description=\"Low quality\">"}
                else if (/^##FORMAT=<ID=AD/) {print "##FORMAT=<ID=AD,Number=.,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">"}
                else if (/^#/) {print} else {exit}}' \
        | uniq | bgzip > ${chromosome}-${basename}-only-genotyped.vcf.gz


      zcat ${chromosome}-${basename}.vcf.gz \
        | grep -vE "^#|\.[\|/]\.|NON_REF|\*" \
        | awk -F "\t" '{OFS=FS}{if ($7 == ".") {$7 = "PASS"}; print $0}' \
        | bgzip >> ${chromosome}-${basename}-only-genotyped.vcf.gz

  set -o pipefail

      tabix -p vcf ${chromosome}-${basename}-only-genotyped.vcf.gz


  # Merging and postprocessing
  # --------------------------
  #  The command below merges dbSNP VCF database and VCF produced by previous
  #  command and performs postprocessing of resultinf VCF. This is necessary
  #  mainly to correctly annotate sites with 0/0 genotypes in genotyped VCF.
  #  In the postprocessing step:
  #   1. rs IDs from INFO columns are copied to ID column
  #   2. flags "LowQual", "ComplexGenotype" and "NovelAllele" are added
  #   3. sites that lie within simple repeats regions (as defined in file
  #      simple_repeats_bed_gz) are removed

      bcftools merge \
        --merge all \
        ${chromosome}-${basename}-only-genotyped.vcf.gz \
        $chromosome_dbsnp_vcf_gz \
          | grep -vP "\./\.\t1/1$" \
          | awk -F "\t" '{OFS=FS} {if (/^##/) {print} else {print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}}' \
          | python3 ${keep_or_remove_vcf_fields_py} -l DP,RS_INFO \
          | bcftools view --targets-file ^$simple_repeats_bed_gz \
          | python3 ${dbsnp_after_merge_postprocessing_py} \
          | bgzip > ${chromosome}-${basename}_genotyped-on-dbsnp.vcf.gz

      tabix -p vcf ${chromosome}-${basename}_genotyped-on-dbsnp.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                          --task-name-with-index ${task_name_with_index} \
                                          --task-version ${task_version} \
                                          --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "10G"
    cpu: 2
    maxRetries: 2

  }

  output {

    File genotyped_on_dbsnp_vcf_gz = "${chromosome}-${basename}_genotyped-on-dbsnp.vcf.gz"
    File genotyped_on_dbsnp_vcf_gz_tbi = "${chromosome}-${basename}_genotyped-on-dbsnp.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
