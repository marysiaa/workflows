workflow rna_seq_merge_abundances_workflow
{
  call rna_seq_merge_abundances
}

task rna_seq_merge_abundances {

  Array[File] est_counts_files
  Array[File] tpm_files
  File gene_level_tsv
  File genes_ensembl_tsv
  File transcripts_ensembl_tsv

  String analysis_id = "no_id_provided"

  String dollar = "$"
  String task_name = "rna_seq_merge_abundances"
  String task_version = "1.1.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-merge-abundances:1.1.3"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir results
    python3 /intelliseqtools/merge_abundances.py --tsv-files ${sep=" " est_counts_files} ${sep=" " tpm_files} \
                                                 --analysis-id ${analysis_id} \
                                                 --output-file-path results


    python3 /intelliseqtools/annotate-with-ensembl-data.py  --input-file-to-annotate "${gene_level_tsv}" \
                                                            --input-ensembl-anno-tsv "${genes_ensembl_tsv}" \
                                                            --ensembl-column-to-merge-on "Gene stable ID" \
                                                            --id-column-to-merge-on "Geneid" \
                                                            --output-file-annotated "results/${analysis_id}_gene-level-counts-annotated.tsv"

    for file in results/${analysis_id}_transcript-level-counts.tsv results/${analysis_id}_transcript-level-tpm.tsv
      do
        filename="$(basename $file)"
        filename="${dollar}{filename%.*}"

        python3 /intelliseqtools/annotate-with-ensembl-data.py  --input-file-to-annotate "$file" \
                                                                --input-ensembl-anno-tsv "${transcripts_ensembl_tsv}" \
                                                                --ensembl-column-to-merge-on "Transcript stable ID version" \
                                                                --id-column-to-merge-on "target_id" \
                                                                --output-file-annotated "results/$filename-annotated.tsv"
      done




   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File est_counts = "results/${analysis_id}_transcript-level-counts-annotated.tsv"
    File tpm = "results/${analysis_id}_transcript-level-tpm-annotated.tsv"
    File gene_level = "results/${analysis_id}_gene-level-counts-annotated.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
