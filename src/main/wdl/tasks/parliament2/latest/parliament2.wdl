workflow parliament2_workflow {

  meta {
    name: 'parliament2'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'parliament2 \n WDL implementation of Parliament2.'
    changes: '{"latest": "no changes"}'

    input_bam: '{"name": "BAM file", "type": "File", "constraints": {"extension": [".bam"]}, "description": "Input BAM file"}'
    input_bai: '{"name": "BAM file index", "type": "File", "constraints": {"extension": [".bai", ".bam.bai"]}, "description": "BAI index of input BAM file"}'
    input_sample_id: '{"name": "Sample ID", "type": "String", "description": "Sample ID"}'

    input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"values": ["broad-institute-hg38", "grch38-no-alt-analysis-set"]}, "description": "Version of reference genome an input BAM file is aligned to"}'

    input_filter_short_contigs: '{"name": "Filter short contigs", "type": "Boolean", "default": "false", "description": "If selected, SV calls will not be generated on contigs shorter than 1 MB."}'
    input_breakdance: '{"name": "Breakdancer", "type": "Boolean", "default": "true", "description": "If selected, the program Breakdancer will be one of the SV callers run."}'
    input_breakseq: '{"name": "BreakSeq2", "type": "Boolean", "default": "false", "description": "If selected, the program BreakSeq2 will be one of the SV callers run. May not work for reference genomes other than hs37d5."}'
    input_manta: '{"name": "Manta", "type": "Boolean", "description": "If selected, the program Manta will be one of the SV callers run."}'
    input_cnvnator: '{"name": "CNVnator", "type": "Boolean", "default": "true", "description": "If selected, the program CNVnator will be one of the SV callers run."}'
    input_lumpy: '{"name": "Lumpy", "type": "Boolean", "default": "true", "description": "If selected, the program Lumpy will be one of the SV callers run."}'
    input_delly_deletion: '{"name": "Delly2 deletion", "type": "Boolean", "default": "true", "description": "If selected, the deletion module of the program Delly2 will be one of the SV callers run."}'
    input_delly_insertion: '{"name": "Delly2 insertion", "type": "Boolean", "default": "true", "description": "If selected, the insertion module of the program Delly2 will be one of the SV callers run."}'
    input_delly_inversion: '{"name": "Delly2 inversion", "type": "Boolean", "default": "true", "description": "If selected, the inversion module of the program Delly2 will be one of the SV callers run."}'
    input_delly_duplication: '{"name": "Delly2 duplication", "type": "Boolean", "default": "true", "description": "If selected, the duplication module of the program Delly2 will be one of the SV callers run."}'
    input_genotype: '{"name": "Genotype", "type": "Boolean", "default": "true", "description": "If selected, candidate events determined from the individual callers will be genotyped and merged to create a consensus output."}'
    input_svviz: '{"name": "SVVIZ", "type": "Boolean", "default": "false", "description": "NOT RECOMMENDED - the SVVIZ component takes a very long time to complete. If selected, visualizations of genotyped SV events will be produced with SVVIZ, one screenshot of support per event. For this option to take effect Genotype must be selected."}'
    input_svviz_only_validated_candidates: '{"name": "SVVIZ", "type": "Boolean", "default": "true", "description": "Run SVVIZ only on validated candidates. For this option to be relevant, SVVIZ must be selected. NOT selecting this will make the SVVIZ component run longer."}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call parliament2

}

task parliament2 {

  File bam
  File bai
  String sample_id = "no_id_provided"

  String reference_genome = "broad-institute-hg38"

  Boolean? filter_short_contigs = false
  Boolean? breakdancer = true
  Boolean? breakseq = false
  Boolean? manta = true
  Boolean? cnvnator = true
  Boolean? lumpy = true
  Boolean? delly_deletion = true
  Boolean? delly_insertion = true
  Boolean? delly_inversion = true
  Boolean? delly_duplication = true
  Boolean? genotype = true
  Boolean? svviz = false
  Boolean? svviz_only_validated_candidates = true

  # Tools runtime settings, paths etc.
  String dnanexus_in_path = "/home/dnanexus/in"
  String dnanexus_out_path = "/home/dnanexus/out"
  String reference_genome_fasta_gz = "/home/dnanexus/in/resources/reference-genome/" + reference_genome + ".fa.gz"
  String reference_genome_fasta_fai = "/home/dnanexus/in/resources/reference-genome/" + reference_genome + ".fa.fai"
  String parliament2_py = "/home/dnanexus/parliament2.py"
  Boolean cleanup_workspace = true

  String docker_image = "intelliseqngs/parliament2:" + reference_genome + "_v0.1"
  String task_name = "parliament2"
  String task_version = "latest"

  command <<<
    task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
    source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/after-start.sh)

    mv ${bam} ${dnanexus_in_path}/${sample_id}.bam
    mv ${bai} ${dnanexus_in_path}/${sample_id}.bai

    ${parliament2_py} \
      --bam ${dnanexus_in_path}/${sample_id}.bam \
      --bai ${dnanexus_in_path}/${sample_id}.bai \
      --ref_genome ${reference_genome_fasta_gz} \
      --fai ${reference_genome_fasta_fai} \
      --prefix ${sample_id} \
      ${true='--filter_short_contigs' false='' filter_short_contigs} \
      ${true='--breakdancer' false='' breakdancer} \
      ${true='--breakseq' false='' breakseq} \
      ${true='--manta' false='' manta} \
      ${true='--cnvnator' false='' cnvnator} \
      ${true='--lumpy' false='' lumpy} \
      ${true='--delly_deletion' false='' delly_deletion} \
      ${true='--delly_insertion' false='' delly_insertion} \
      ${true='--delly_inversion' false='' delly_inversion} \
      ${true='--delly_duplication' false='' delly_duplication} \
      ${true='--genotype' false='' genotype} \
      ${true='--svviz' false='' svviz} \
      ${true='--svviz_only_validated_candidates' false='' svviz_only_validated_candidates} \

    tar -C ${dnanexus_out_path} -zcvf log-files.tar.gz log_files
    tar -C ${dnanexus_out_path} -zcvf sv-caller-results.tar.gz sv_caller_results

    if ${genotype}; then

      tar -C ${dnanexus_out_path} -zcvf svtyped-vcfs.tar.gz svtyped_vcfs

      cat ${dnanexus_out_path}/${sample_id}.combined.genotyped.vcf | vcf-sort -c | bgzip > ${sample_id}.combined.genotyped.vcf.gz
      tabix -p vcf ${sample_id}.combined.genotyped.vcf.gz

    fi

    if ${svviz}; then
      tar -C ${dnanexus_out_path} -zcvf svviz-outputs.tar.gz svviz_outputs
    fi

    ## cleanup workspace
    if ${cleanup_workspace}; then

      rm chr.[1-9].bam chr.[1-2][0-9].bam chr.[1-9].bam.bai chr.[1-2][0-9].bam.bai \
         output.rootchr[1-9] output.rootchr[1-2][0-9] output.rootchr[XY] \
         header.txt \
         contigs ref.fa ref.fa.fai \
         input.bam input.bam.bai \
         small_vcf.* vcf_entries.vcf \
         *.svp &> /dev/null

      rm -r breakdancer* *breakdancer* *breakdancer \
            breakseq* *breakseq* *breakseq \
            manta* *manta* *manta \
            cnvnator* *cnvnator* *cnvnator
            lumpy* *lumpy* *lumpy \
            delly* *delly* *delly \
            survivor* *survivor* *survivor \
            svtype_* &> /dev/null

    fi

    source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "14G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File? combined_genotyped_vcf_gz = "${sample_id}.combined.genotyped.vcf.gz"
    File? combined_genotyped_vcf_gz_tbi = "${sample_id}.combined.genotyped.vcf.gz.tbi"

    File? svtyped_vcfs_tar_gz = "svtyped-vcfs.tar.gz"
    File? svviz_outputs_tar_gz = "svviz-outputs.tar.gz"
    File sv_caller_results_tar_gz = "sv-caller-results.tar.gz"
    File log_files_tar_gz = "log-files.tar.gz"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
