workflow vcf_anno_custom_workflow {

  meta {
    keywords: '{"keywords": ["vcf annotation", "bed", "vcf", "vcfanno"]}'
    name: 'vcf_anno_custom'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'vcf.gz annotation by INFO fields from another vcf.gz and/or columns from bed.gz file.'
    changes: '{"latest": "no changes"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


    input_query_bed_gz_array: '{"name": "query_bed_gz_array", "type": "Array[File]", "required": "false", "extension": [".bed.gz"], "description": "Array of bed.gz files. Requires: -bgziped, -more columns than basic 3 - fourth and the next will be used to annotate, -tab separated. Important note: name of the file will be included at the beginning of annotation name"}'
    input_query_vcf_gz_array: '{"name": "query_vcf_gz_array", "type": "Array[File]", "required": "false", "description": "Array of vcf.gz files. Should contain some INFO fields to use to annotate. Important note: name of the file will be included at the beginning of annotation name", "extension": [".vcf.gz"]}'
    input_vcf_gz: '{"name": "vcf_gz", "type": "File", "required": "true", "description": "vcf.gz file - will be annotated by query_vcf_gz_array and/or query_bed_gz_array", "extension": [".vcf.gz"]}'
    input_vcf_gz_tbi: '{"name": "vcf_gz_tbi", "type": "File", "required": "true", "description": "index for input_vcf_gz", "extension": [".vcf.gz.tbi"]}'
    #input_conf_toml: '{"name": "conf_toml", "type": "File", "description": "to be implemented later, according to documentation: https://github.com/brentp/vcfanno", "required": "false", "extension": [".tmol"]}


  output_annotated_vcf_gz: '{"name": "annotated_vcf_gz", "type": "File", "description": "vcf.gz file annotated by query_bed_gz_array and/or query_vcf_gz_array"}'

  }

  call vcf_anno_custom

}

task vcf_anno_custom {

  String task_name = "vcf_anno_custom"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-custom:1.0.1"

  Array[File] query_vcf_gz_array = []
  Array[File] query_bed_gz_array = []

  #File? conf_toml
  File vcf_gz
  File vcf_gz_tbi

  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if [ ${length(query_vcf_gz_array)} != 0 ]; then
    for vcf_file in ${sep=' 'query_vcf_gz_array}; do
      tabix -p vcf $vcf_file
    done
  fi

  if [ ${length(query_bed_gz_array)} != 0 ]; then
    for bed_file in ${sep=' ' query_bed_gz_array}; do
      tabix -p bed $bed_file
    done
  fi

  python3 /intelliseqtools/vcf-anno-custom.py \
    --output_path conf.toml \
    ${sep=' ' prefix('--query_vcf ', query_vcf_gz_array)} \
    ${sep=' ' prefix('--query_bed ', query_bed_gz_array)} && \
    vcfanno conf.toml ${vcf_gz} | bgzip > ${sample_id}_annotated.vcf.gz

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File annotated_vcf_gz = "${sample_id}_annotated.vcf.gz"

  }

}
