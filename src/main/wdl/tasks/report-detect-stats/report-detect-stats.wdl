workflow report_detect_stats_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'report_detect_stats'
    author: 'https://gitlab.com/MonikaKrzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.3.5": "add analysisEnvironment (IntelliseqFlow, T-Systems) input to report", "1.3.4": "update name for report"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_detection_stats_json: '{"name": "detection_stats_json", "type": "File", "description": "Json with detection chance for each gene genotyped by intervals"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    output_detection_chance_report_pdf: '{"name": "detection_chance_report_pdf", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in pdf file"}'
    output_detection_chance_report_odt: '{"name": "detection_chance_report_odt", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in odt file"}'
    output_detection_chance_report_docx: '{"name": "detection_chance_report_docx", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in docx file"}'
    output_detection_chance_report_html: '{"name": "detection_chance_report_html", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in html file"}'
    
    
  }

  call report_detect_stats

}

task report_detect_stats {

  String task_name = "report_detect_stats"
  String task_version = "1.3.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_detection_chance:1.0.1"


  File detection_stats_json
  File? sample_info_json
  String analysisEnvironment = "IntelliseqFlow"

  Int timezoneDifference = 0

  String sample_id = "no_id_provided"
  String output_name = "sensitivity-report"



  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir outputs/

    # Prepare sample-info.json
    bash /intelliseqtools/prepare-sample-info-json.sh \
        --sample-info-json "${sample_info_json}" \
        --timezoneDifference ${timezoneDifference} \
        --sampleID "${sample_id}"

    python3 /intelliseqtools/reports/templates/script/report-detection-chance.py \
        --input-template "/resources/detection-chance-template.docx" \
        --input-sample-info-json "sample_info.json" \
        --input-detection-stats-json ${detection_stats_json} \
        --analysis-run "${analysisEnvironment}" \
        --output-filename "outputs/${sample_id}_${output_name}.docx"

soffice --headless \
        --convert-to pdf outputs/${sample_id}_${output_name}.docx \
        --outdir outputs/

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File detection_chance_report_pdf = "outputs/${sample_id}_${output_name}.pdf"
    File detection_chance_report_docx = "outputs/${sample_id}_${output_name}.docx"

  }

}
