workflow report_fq_qc_workflow {

  meta {
    keywords: '{"keywords": ["report", "fq-qc"]}'
    name: 'report_fq_qc'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task which generates report-fq-qc from .docx template'
    changes: '{"1.2.0": "change in reports template, docker and script", "1.1.0": "reports refactoring", "1.0.2": "fixed improper processing of filename .fastq.gz suffix"}'

    input_sample_info_json: '{"name": "Sample info json", "extension": [".json"], "type": "File", "description": "Json file with sample info"}'
    input_qc_jsons: '{"name": "QC jsons", "type": "Array[String]", "description": "Array containing jsons"}'
    input_qc_pngs: '{"name": "QC pngs", "type": "Array[String]", "description": "Array containing pngs"}'
    input_sample_id: '{"name": "Sample id", "type": "String", "description": "identifier of sample"}'

    output_quality_check_report_ang_docx: '{"name": "Fastq quality report docx in English", "type": "File", "copy": "True", "description": "Fastq quality check report in .docx format"}'
    output_quality_check_report_pl_docx: '{"name": "Fastq quality report docx in Polish", "type": "File", "copy": "True", "description": "Fastq quality check report in .docx format (PL)"}'
    output_quality_check_report_ang_pdf: '{"name": "Fastq quality report pdf in English", "type": "File", "copy": "True", "description": "Fastq quality check report in .pdf format"}'
    output_quality_check_report_pl_pdf: '{"name": "Fastq quality report pdf in Polish", "type": "File", "copy": "True", "description": "Fastq quality check report in .pdf format (PL)"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call report_fq_qc

}

task report_fq_qc {

  File? sample_info_json
  Array[File] qc_jsons
  Array[File] qc_pngs
  String sample_id = "no_id_provided"
  Array[String] languages = ["pl", "ang"]
  Int timezoneDifference = 0
  String outputs = "outputs"
  String output_name = "fq-qc-report"

  String task_name = "report_fq_qc"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_fq-qc:1.1.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ${outputs}

    bash /intelliseqtools/prepare-sample-info-json.sh \
      --sample-info-json "${sample_info_json}" \
      --timezoneDifference 0 \
      --sampleID "${sample_id}"

    for language in ${sep=' ' languages}
    do
      python3 /intelliseqtools/reports/templates/script/report-fq-qc.py \
        --input-template "/resources/fq-qc-template.docx" \
        --input-sample-info-json "sample_info.json" \
        --input-dict-json "/resources/dict-$language.json" \
        --input-qc-jsons ${sep=" " qc_jsons} \
        --input-qc-pngs ${sep=" " qc_pngs} \
        --input-name ${sample_id}_$language-${output_name} \
        --output-dir "${outputs}"

      soffice --headless \
      --convert-to pdf ${outputs}/${sample_id}_$language-${output_name}.docx \
      --outdir ${outputs}
    done

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
    File? report_fq_qc_ang_docx = "${outputs}/${sample_id}_ang-${output_name}.docx"
    File? report_fq_qc_pl_docx = "${outputs}/${sample_id}_pl-${output_name}.docx"

    File? report_fq_qc_ang_pdf = "${outputs}/${sample_id}_ang-${output_name}.pdf"
    File? report_fq_qc_pl_pdf = "${outputs}/${sample_id}_pl-${output_name}.pdf"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
