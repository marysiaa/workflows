workflow rna_seq_star_workflow
{
  call rna_seq_star
}

task rna_seq_star {

  File fastq_1
  File? fastq_2
  Array[File] ref_genome_index
  String sample_id = "no_id_provided"

  Int num_thread = 6
  String task_name = "rna_seq_star"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/star:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir genome
    mkdir results
    ln -s ${sep=" " ref_genome_index} genome

    STAR --genomeDir genome \
         --runThreadN ${num_thread} \
         --readFilesIn ${fastq_1} ${fastq_2}\
         --readFilesCommand zcat \
         --outFileNamePrefix results/${sample_id}_ \
         --outSAMtype BAM SortedByCoordinate \
         --outSAMunmapped Within \
         --outSAMattributes Standard

    # index bam
    mv results/*.bam results/${sample_id}.bam
    samtools index results/${sample_id}.bam results/${sample_id}.bam.bai

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: 6
    maxRetries: 2

  }

  output {

    File bam = "results/${sample_id}.bam"
    File bai = "results/${sample_id}.bam.bai"
    File summary = "results/${sample_id}_Log.final.out"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
