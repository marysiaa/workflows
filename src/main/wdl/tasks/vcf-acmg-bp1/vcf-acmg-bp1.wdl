workflow vcf_acmg_bp1_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "BP1", "variant", "benign"]}'
    name: 'Vcf ACMG BP1'
    author: 'https://gitlab.com/gleblavr , https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Evaluates whether variant fulfils ACMG BP1 criterion, adds annotation'
    changes: '{"1.5.0": "update ClinVar, ubuntu and pysam", "1.4.0": "update ClinVar and pysam/cython", "1.3.0": "ClinVar update", "1.2.0": "resources update", "1.1.2": "resources update, reformatted script"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf"], "description": "Input annotated vcf"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True", "type": "File", "description": "Vcf with ACMG BP1 annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_acmg_bp1

}

task vcf_acmg_bp1 {

  String task_name = "vcf_acmg_bp1"
  String task_version = "1.5.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-bp1:1.4.0"
  File vcf
  String sample_id = "no-id"

  command <<<
  set -e -o pipefail

  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  clinvar_lof_dictionary=$( ls /resources/clinvar/*/clinvar-lof-dictionary.json )
  clinvar_ms_dictionary=$( ls /resources/clinvar/*/clinvar-ms-dictionary.json )

  bgzip -d ${vcf} -c > tmp.vcf

  python3 /intelliseqtools/acmg-bp1.py \
    --input-vcf tmp.vcf \
    --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz \
    --clinvar-lof $clinvar_lof_dictionary \
    --clinvar-ms $clinvar_ms_dictionary


  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}

  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
