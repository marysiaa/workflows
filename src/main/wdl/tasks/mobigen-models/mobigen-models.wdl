workflow mobigen_models_workflow {
    call mobigen_models
}

task mobigen_models {

    String task_name = "mobigen_models"
    String task_version = "4.1.3"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

    File? assignments_json
    Array[File] vcfs_gz
    Array[File] models
    String log_file_name = "mobigen-models.log"
    String docker_image = "intelliseqngs/mobigen-models:5.3.4"
    Int num_of_processes = 8

    #todo $3 "!~ /;rs/" excludes lines containing valiants with multiple ids. They should be split instead of excluding
    command <<<
    set -e
    set -x
    task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir /models
    printf "${sep='\n' models}" | xargs -i bash -c "cp {} /models"

    mkdir /vcfs
    printf "${sep='\n' vcfs_gz}" | xargs -i bash -c "cp {} /vcfs"
    ls /vcfs/* | xargs -i bash -c "tabix -p vcf {}"

    mkdir output/

    # cat ${assignments_json} | jq -c '.assignments[] | {file, tests: .tests}' > tests
    cat ${assignments_json} | \
        jq -c '.assignments | to_entries | map( { (.key) : (.value + { "sample":.key }) } ) | .[] | add' > tests
    for index in $(seq -s ' ' $(wc -l tests | cut -d ' ' -f 1))
    do
        cat tests | sed -n "$index"p > samplework
        for test in $(cat samplework | jq -r .tests[])
        do
            export testmodels=$(cat ${assignments_json} | jq -r ".tests.$test.models[]" | tr -s '\n' ' ' | sed 's/ $//' | sed 's| | --model /models/|g' | sed 's|^|--model /models/|')
            export samplevcf=$(cat samplework | jq -r .file | tr -d '\n')
            polygenic \
            $testmodels \
            --sample-name $(cat samplework | jq -r .sample | tr -d '\n') \
            --output-name-appendix $test \
            --af /resources/gnomad/3.1/af.vcf.gz \
            --log-file ${log_file_name} \
            --output-directory output \
            --vcf /vcfs/$samplevcf
            echo "OUTPUT"
            ls output/
        done
    done

    if [ -d "output/" ]; then
        if [ "$(ls -A output/)" ]; then
            echo "Output is not Empty"
        else
            pgstk \
                --log-file ${log_file_name} pgs-compute \
                --model ${sep=' ' models} \
                --af /resources/gnomad/3.1/af.vcf.gz \
                --output-directory output \
                --vcf $(ls /vcfs/*.vcf.gz | head -1)
        fi
    else
        echo "Directory Output not found."
    fi

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: num_of_processes
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    Array[File] out_files = glob("output/*.json")

  }

}
