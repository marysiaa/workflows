workflow json_prioritize_var_somatic_workflow
{
  call json_prioritize_var_somatic
}

task json_prioritize_var_somatic {

  String task_name = "json_prioritize_var_somatic"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_json-prioritize-var-somatic:1.0.0"

  String sample_id = "no_id_provided"
  File vcf_json
  Int num_var = 50 #maximal number of variants to keep

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    python3 /intelliseqtools/select_variants_from_json_somatic.py \
        --input-json ${vcf_json} \
        --output-json ${sample_id}_report.json \
        --variant-number ${num_var}

    jq '.[] | .CHROM , .POS , .ISEQ_GENES_NAMES' ${sample_id}_report.json \
    | sed 's/\"//g' | paste -d ' ' - - - >> ${sample_id}_list-of-positions.txt    

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File report_json = "${sample_id}_report.json"
    File position_list = "${sample_id}_list-of-positions.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
