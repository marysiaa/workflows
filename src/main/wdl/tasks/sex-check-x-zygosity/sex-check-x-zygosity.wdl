workflow sex_check_x_zygosity_workflow {

  meta {
    keywords: '{"keywords": ["chrX", "sex", "biological sex", "zygosity"]}'
    name: 'sex_check_x_zygosity'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task performs biological sex recognition using zygosity on chromosome X excluding pseudoautosomal regions. The ratio of hetero- and homozygotes is computed and empirical criteria are used to assess biological sex: F or M. In some boundary cases "cannot be determined by this method" can be returned.'
    changes: '{"1.0.5":"update output json structure","1.0.3": "fix bug when no homozygotes are present", "1.0.2": "added error handling", "1.0.1": "added thresholds; fixed counting bug"}'


    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_vcf_gz: '{"name": "Vcf", "type": "File", "extension": [".vcf.gz"], "description": "Input vcf file with annotation added for variants in UCSC Simple Repeat region, by task vcf-anno-repeat.wdl (annotation id: ISEQ_SIMPLE_REPEAT)"}'
    input_vcf_gz_tbi: '{"name": "Vcf tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Index for the input vcf file"}'


    output_sex_report_json: '{"name": "Biological sex recognition report", "type": "File", "copy": "True", "description": "Sex recognition results report in json format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call sex_check_x_zygosity

}

task sex_check_x_zygosity {

  File vcf_gz
  File vcf_gz_tbi

  String task_name = "sex_check_x_zygosity"
  String task_version = "1.0.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.3"

  command <<<
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   #pseudoautosomal regions on X: 10001-2781479 and 155701383-156030895
   zcat ${vcf_gz} | grep -v "^#" | grep chrX | grep -v "ISEQ_SIMPLE_REPEAT=inSimpleRepeat" | awk '$2<10001 || ($2>2781479 && $2<155701383) ||	$2>156030895 { print $10 }' | awk -F: '{print $1}' | grep -F -v './.' | grep -F -v '0/0' > filtered_gt.txt

   set -e -o pipefail

   touch sex_recognition_x_zygosity.json
   if [ -s filtered_gt.txt ] ; then

      ALL=$(wc -l < filtered_gt.txt)
      HETEROZYGOTES=$(awk -F'[/|]' '{print $1!=$2}' filtered_gt.txt | grep -c 1 || [[ $? == 1 ]])
      PERCENT=$(echo $HETEROZYGOTES  $ALL | awk '{printf("%.2f", 100*$1/$2)}')

      SEX=$(echo $PERCENT | awk '{if ($1 < 15) print "M"; else if ($1 > 30) print "F"; else print "cannot be determined by this method"}')

      echo "{\"method\":\"Zygosity for chrX\",\"result\":\""$SEX"\",\"measure_type\":\"Percent of heterozygotes\",\"measure_value\":\""$PERCENT"%\",\"thresholds\":\"Male: percent < 15%.\nFemale: percent > 30%.\"}" >> sex_recognition_x_zygosity.json

   else

      echo "{\"method\":\"Zygosity for chrX\",\"result\":\"cannot be determined by this method\",\"measure_type\":\"Percent of heterozygotes\",\"measure_value\":\"no coverage on chrX\",\"thresholds\":\"Male: percent < 15%.\nFemale: percent > 30%.\"}" >> sex_recognition_x_zygosity.json

   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File sex_report_json = "sex_recognition_x_zygosity.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
