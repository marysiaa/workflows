workflow pgx_prioritize_anno_workflow
{
  call pgx_prioritize_anno
}

task pgx_prioritize_anno {

  File recommendations
  String sample_id = "no_id_provided"

  String task_name = "pgx_prioritize_anno"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-prioritize-anno:1.0.2"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    /intelliseqtools/pgx-prioritize-anno.py --recommendations ${recommendations} \
                                            --drugs-to-filter "/resources/drugs_to_filter.tsv" \
                                            --output-filename ${sample_id}-prioritized.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File prioritized = "${sample_id}-prioritized.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
