workflow bco_json_to_csv_workflow {

  meta {
    keywords: '{"keywords": ["bco", "csv", "json", "table"]}'
    name: 'bco_json_to_csv'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"2.0.1": "docker update"}'

    input_bco_json: '{"name": "bco json", "type": "File", "description": "Name of table containing bco values in json format"}'

    output_bco_table_csv: '{"name": "bco table csv", "type": "File", "copy": "True", "description": "Bco after conversion in csv format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call bco_json_to_csv

}

task bco_json_to_csv {

  File bco_json
  String bco_table_filename= basename(bco_json, ".json")
  String sample_id = "no_id_provided"

  String task_name = "bco_json_to_csv"
  String task_version = "2.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bco-json-to-csv:0.0.4"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   python3 /intelliseqtools/bco-json-to-csv.py --input-bco-json ${bco_json} \
                                               --output-csv ${sample_id}.bco_csv

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File bco_table_csv = "${sample_id}.bco_csv.csv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
