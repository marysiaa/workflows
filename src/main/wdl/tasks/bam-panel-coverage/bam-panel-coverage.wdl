workflow bam_panel_coverage_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'bam panel coverage'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Produces json with genes coverage from provided panel in provided sample'
    changes: '{"1.1.2": "ensembl-resources docker update", "1.1.1":"add created panel to outputs","1.1.0": "update docker, add creating panel from bed","1.0.5": "fixed bug with empty panels", "1.0.4": "changed meta", "1.0.3": "updated docker", "1.0.1": "result presented as percent, rounded"}'

    input_bam: '{"name": "bam file", "type": "File", "description": "Bam file with name containing: markdup.recalibrated or filtered -  outputs from: 1. sample-id_markdup.recalibrated.bam from fq_bwa_align module: recalibrated_markdup_bam = bam_concat_recalib.bam, 2. sample-id_filtered.bam from bam_filter_contam module: bam_remove_mates.filtered_bam, or 3. one of those is a germline output (if you start from fastqs): final_bam = bam_to_var_calling. NEVER USE sample-id_realigned-haplotypecaller.bam from bam_varcalling module: haplotype_caller_bam = bam_concat.bam, also a germline output: final_realigned_bam = bam_varcalling.haplotype_caller_bam.", "extension": [".bam"]}'
    input_bai: '{"name": "bai file", "type": "File", "description": "Bai index file for provided bam.", "extension": [".bai"]}'
    input_panel_json: '{"name": "panel json", "type": "File", "description": "Panel data in json format", "extension": [".json"]}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_depth: '{"name": "depth", "type": "Int", "description": "the output file contains the percentage of bases with coverage bigger than this value", "constraints": {"min": "0", "max": "100"}}'

    output_panel_coverage_json: '{"name": "panel coverage json", "type": "File", "copy": "True", "description": "Json with panel genes coverage in sample"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bam_panel_coverage

}

task bam_panel_coverage {

  File bam
  File bai
  Int depth = 10

  File? panel_json
  File? bed
  Boolean is_panel_defined = defined(panel_json)

  String task_name = "bam_panel_coverage"
  String task_version = "1.1.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-panel-coverage:1.0.1"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  awk 'NF==7' /resources/biomart-genes-exon-position.tsv | awk '$5 != "MT"' > biomart.tsv

  #create genes panel from bed or simplify input json panel
  if  ${is_panel_defined}; then
    jq -r '.[]|.name' ${panel_json} > panel.txt
  else
    tail -n +2 biomart.tsv | awk '{print "chr"$5"\t"$6"\t"$7"\t"$2}' | sort -k1,1V -k2,2n -k3,3n > tmp.tsv
    bedtools intersect -a tmp.tsv -b ${bed} | cut -f4 | sort | uniq > panel.txt
    rm tmp.tsv
  fi

  #filter biomart-genes-exon-position.csv for genes from panel
  awk 'NR==FNR {a[$1]; next} $2 in a' panel.txt biomart.tsv > biomart-genes-filtered.tsv
  rm biomart.tsv

  #create table with gene_name gene_id transcript_id number_of_positions number_of_positions_with_depth>10
  touch depth-for-interval.txt
  while IFS="$\n" read -r line; do
    INTERVAL=$(echo "$line" | awk '{print "chr" $5 ":" $6 "-" $7}')
    DEPTH=$(samtools depth -r $INTERVAL ${bam} | awk -v d=${depth} '$3 > d {c++} END {print c+0}')
    NAMES=$(echo "$line" |awk '{print $2 " " $3 " " $4 " "}')
    NUMBER_OF_POSITIONS=$(echo "$line" | awk '{print $7-$6+1}')
    echo  $NAMES $NUMBER_OF_POSITIONS $DEPTH >> depth-for-interval.txt
  done < biomart-genes-filtered.tsv

  #calculate percentage and change it into json
  echo '[' > panel_coverage.json
  awk '{all[ "\"gene_name\":\"" $1 "\",\"gene_id\":\"" $2 "\",\"transcript_id\":\""  $3]+=$4; \
  count["\"gene_name\":\"" $1 "\",\"gene_id\":\"" $2 "\",\"transcript_id\":\""  $3]+=$5;} \
  END {for (i in all) printf  "%s%s%.4f%s\n" , i , "\",\"percentage\":" , 100*count[i]/all[i] , ","}' depth-for-interval.txt \
  | sed 's/^"/{"/;s/,$/},/'>> panel_coverage.json
  sed -i '$ s/,$//' panel_coverage.json
  echo ']' >> panel_coverage.json

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}s
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File panel_coverage_json = "panel_coverage.json"
    File panel = "panel.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
