workflow pdf_merge_workflow {

  meta {
    keywords: '{"keywords": []}'
    name: 'merge_pdf'
    author: 'https://gitlab.com/mremre'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'merge_pdf \n Takes all pdf raports in target seq pipeline and merges them into one pdf'
    changes: '{"latest": "no changes"}'

    input_pdf: '{"name": "pdf", "type": "Array[File]", "extension": [".pdf"], "description": "array of reports in pdf"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'

    output_merged_pdf : '{"name": "merged_pdf", "type": "File", "description": "all reports merged into one file"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call pdf_merge

}

task pdf_merge {

  Array[File] pdf
  Array[File] ang_pdf

  String sample_id = "no-id-provided"

  String task_name = "pdf_merge"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pdf-merge:1.2.1"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/after-start.sh)

  python3 /intelliseqtools/merge-pdf.py   --sample_id ${sample_id} \
                                          --pdf_files ${sep=' ' ang_pdf} \
                                          --output ${sample_id}"_merged-report-ang.pdf"

  python3 /intelliseqtools/enumerate-pdf.py                       --pdf_file ${sample_id}"_merged-report-ang.pdf" \
                                              --output ${sample_id}"_full-report-ang.pdf"


  python3 /intelliseqtools/merge-pdf.py   --sample_id ${sample_id} \
                                          --pdf_files ${sep=' ' pdf} \
                                          --output ${sample_id}"_merged-report.pdf"
  python3 /intelliseqtools/enumerate-pdf.py          --pdf_file ${sample_id}"_merged-report.pdf" \
                              --output ${sample_id}"_full-report.pdf"

  rm ${sample_id}"_merged-report-ang.pdf"
  rm ${sample_id}"_merged-report.pdf"

  for file_ang in ${sep=' ' ang_pdf}; do
      python3 /intelliseqtools/enumerate-pdf.py --pdf_file $file_ang --output $(basename $file_ang); done

  for file in ${sep=' ' pdf}; do
    python3 /intelliseqtools/enumerate-pdf.py --pdf_file $file --output $(basename $file); done

  rm page_numbers.pdf

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    Array[File] all_reports_pdf = glob("*.pdf")
    File full_report_pdf = "${sample_id}_full-report.pdf"
    File full_report_ang_pdf = "${sample_id}_full-report-ang.pdf"

  }

}
