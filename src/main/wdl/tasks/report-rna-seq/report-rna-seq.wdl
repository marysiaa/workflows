workflow report_rna_seq_workflow
{
  call report_rna_seq
}

task report_rna_seq {

  File bco_json

  String organism = "Human"
  Int number_of_samples = 10
  String ensembl_version = "104"
  String analysis_id = "no_id_provided"
  String? analysisName
  String? analysisWorkflow
  String? analysisWorkflowVersion
  Boolean paired = true
  String stranded = "unstranded"
  # for single end
  Float? fragment_length
  Float? fragment_length_sd

  String task_name = "report_rna_seq"
  String task_version = "1.0.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_rna_seq:1.0.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # get reference_genome name
    wget -O species.tsv "http://ftp.ensembl.org/pub/release-${ensembl_version}/species_EnsemblVertebrates.txt"
    awk -F"\t" '$1=="${organism}" {print $5}' species.tsv > name.txt
    REFERENCE_GENOME=$(cut -f 2 name.txt)

    mkdir outputs

    python3 /intelliseqtools/reports/templates/script/report-rna-seq.py --template "/resources/rna-seq-template.docx" \
                                                                        --bco "${bco_json}" \
                                                                        --organism "${organism}" \
                                                                        --number_of_samples "${number_of_samples}" \
                                                                        --ensembl_version "${ensembl_version}" \
                                                                        --analysis_id "${analysis_id}" \
                                                                        --analysis_name "${analysisName}" \
                                                                        --workflow_name "${analysisWorkflow}" \
                                                                        --workflow_version "${analysisWorkflowVersion}" \
                                                                        --reference_genome "$REFERENCE_GENOME" \
                                                                        --is_paired "${paired}" \
                                                                        --stranded "${stranded}" \
                                                                        --fragment_length "${fragment_length}" \
                                                                        --fragment_length_sd "${fragment_length_sd}" \
                                                                        --output_filename "outputs/${analysis_id}-main-report.docx"

    soffice --headless \
      --convert-to pdf outputs/${analysis_id}-main-report.docx \
      --outdir outputs

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File rna_seq_report_ang_pdf = "outputs/${analysis_id}-main-report.pdf"
    File rna_seq_report_ang_docx = "outputs/${analysis_id}-main-report.docx"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
