workflow bam_filter_mismatch_workflow {

  meta {
    keywords: '{"keywords": ["bam", "filtering", "contamination"]}'
    name: 'Bam filter mismatches'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Removes reads with multiple mismatches and indels from bam file'
    changes: '{"3.1.0": "deals with iontorrent data - possible removal of reads with multiple indels and mate filtering skipping", "3.0.1": "gatk docker update","3.0.0": "Mates filtering added (works for query sorted bams only), possible filter choice, unmapped reads are now kept", "2.0.1": "list of reads to remove obtained differently, set -o pipefail added"}'

    input_bam: '{"name": "Bam", "type": "File", "extension": [".bam"], "description": "Bam to be filtered"}'
    input_filter_choice: '{"name": "Filter choice", "type": "String", "constraints": {"choices": ["1", "2", "3"]}, "default": "3", "description": "Which filters should be applied: 1 - removing reads with high fraction of mismatches only, 2 - removing reads soft-clipped from both ends and with mapped part shorter than 33, 3 - both filters"}'
    input_mismatch_threshold: '{"name": "Mismatch threshold (for filters 1 and 3)", "type": "Float", "constraints": {"min": 0.0, "max": 1.0}, "default": 0.1, "description": "Reads with fraction of mismatches below this value are accepted"}'

    output_filtered_bam: '{"name": "Filtered bam", "type": "File", "copy": "True", "description": "Filtered bam - without reads with multiple mismatches and indels"}'
    output_removed_reads: '{"name": "Removed reads", "type": "File", "copy": "True", "description": "File containing names of the filtered out reads"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_filter_mismatch

}

task bam_filter_mismatch {

  String task_name = "bam_filter_mismatch"
  String task_version = "3.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-filter-mismatch:1.4.0"

  File bam
  String basename = sub(basename(bam),".bam","")
  Boolean paired = true

  ## Decides which reads should be removed:
  ## 1 - with high fraction of mismatches
  ## 2 - very short and with soft-clip at both ends
  ## 3 - both types (1+2)
  ## 4 - 3 plus removing of reads with more than one indel: for iontorrent

  String filter_choice = "3"

  ## For filters 1 and 3
  Float mismatch_threshold = 0.1
  String java_opt = "-Xmx3g"

  command <<<
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    set -e -o pipefail


    ## set filtering threshold
    sed "s/THRESHOLD/${mismatch_threshold}/" /intelliseqtools/filtering-script${filter_choice}.js > filtering-script${filter_choice}.js


    ## filter out reads with the chosen filter
    gatk --java-options "${java_opt}" FilterSamReads \
         -I ${bam} \
         -O ${basename}-mismatch-filtered.bam \
         --JAVASCRIPT_FILE filtering-script${filter_choice}.js \
         --FILTER includeJavascript \
         --WRITE_READS_FILES


    ## creating lists of reads that were filtered out (usually one from pair) and checking if any
    sort ${basename}.reads | uniq > reads_sorted
    sort  ${basename}-mismatch-filtered.reads | uniq > mismatch_sorted
    sort -m mismatch_sorted reads_sorted | uniq -c | awk '{if ($1==1) {print $2}}' > ${basename}_removed-reads.txt

    if ${paired};then
        ## removing mates of the filtered out reads
        if grep -q '^'  ${basename}_removed-reads.txt
        then
            gatk --java-options "${java_opt}" FilterSamReads \
                -I ${bam} \
                -O ${basename}_filtered.bam \
                --FILTER excludeReadList \
                --READ_LIST_FILE  ${basename}_removed-reads.txt
        else
            mv ${bam} ${basename}_filtered.bam
        fi
    else
         mv ${basename}-mismatch-filtered.bam ${basename}_filtered.bam
    fi


    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File filtered_bam = "${basename}_filtered.bam"
    File removed_reads = "${basename}_removed-reads.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
