workflow task_fail_workflow {

    meta {
        keywords: '{"keywords": ["some", "keywords"]}'
        name: 'task_fail'
        author: 'https://gitlab.com/dzesikahoinkis'
        copyright: 'Copyright 2019 Intelliseq'
        description: 'Generic text for task'
        changes: '{"latest": "no changes"}'

        output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
        output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
        output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    }

    call task_fail

}

task task_fail {

    String task_name = "task_fail"
    String task_version = "1.0.0"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.3"

    command <<<
        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

        echo "Task has failed because of failure" 1>&2
        exit 1

        bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
        --task-name-with-index ${task_name_with_index} \
        --task-version ${task_version} \
        --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: "1"
        maxRetries: 0

    }

    output {

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
