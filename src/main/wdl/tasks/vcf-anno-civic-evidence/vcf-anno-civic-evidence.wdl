workflow vcf_anno_civic_evidence_workflow
{
    call vcf_anno_civic_evidence
}

task vcf_anno_civic_evidence {

    File vcf_gz
    File vcf_gz_tbi
    String vcf_basename = "no_id_provided"
    String evidence_level_threshold = "C" ## required minimal CIViC evidence level, possible levels A (very well supported), B, C, D, E (not well supported)
    Int evidence_rating_threshold = 3 ## required minimal CIViC rating, from 1 to 5, where 1 is not very reliable and 5 is very reliable 

    String task_name = "vcf_anno_civic_evidence"
    String task_version = "1.0.2"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/task_vcf-anno-civic-evidence:1.0.2"

    command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    /intelliseqtools/vcf-anno-civic-evidence.py --input_vcf ${vcf_gz} \
                                                --output_vcf ${vcf_basename}_annotated.vcf.gz \
                                                --minimal_evidence_level ${evidence_level_threshold} \
                                                --minimal_evidence_rating ${evidence_rating_threshold}

    tabix -p vcf ${vcf_basename}_annotated.vcf.gz

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                                --task-name-with-index ${task_name_with_index} \
                                                --task-version ${task_version} \
                                                --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: 1
        maxRetries: 2

    }

    output {

        File annotated_vcf_gz = "${vcf_basename}_annotated.vcf.gz"
        File annotated_vcf_gz_tbi = "${vcf_basename}_annotated.vcf.gz.tbi"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
