workflow fq_to_ubam_workflow
{
  call fq_to_ubam
}

task fq_to_ubam {

  String task_name = "fq_to_ubam"
  String task_version = "1.0.0"
  Int? index
  String sample_id = "no_id_provided"
  File fastq_1
  File? fastq_2

  String java_options = "-Xmx9G"
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.4.1:1.0.1"

  command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    ## check whether files are compressed to use proper programm (grep vs zgrep)
    if echo ${fastq_1} | grep -q '\.gz$';then
        regex_command="zgrep"
    else
        regex_command="grep"
    fi

    ## check if fastq not empty
    if $regex_command -q '^' ${fastq_1}; then

        $regex_command -m1  '^@' ${fastq_1} | cut -d ':' -f 3,4 | sed 's/:/\./g' > rg_id
        
        RG_ID=`cat rg_id`
        
        gatk --java-options "${java_options}" \
            FastqToSam \
            --FASTQ ${fastq_1} \
            ${"--FASTQ2 "  +  fastq_2} \
            --OUTPUT ${sample_id}_unmapped.bam \
            --READ_GROUP_NAME $RG_ID \
            --SAMPLE_NAME ${sample_id} \
            --LIBRARY_NAME ${sample_id}.library \
            --PLATFORM_UNIT "$RG_ID"".""${sample_id}" \
            --PLATFORM Illumina 
    else
        echo 'ERROR: Input files are empty.' >/dev/stderr
        exit 1        
    fi        

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "10G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File unmapped_bam = "${sample_id}_unmapped.bam"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
