workflow vcf_anno_cosmic_workflow {

  meta {
    keywords: '{"keywords": ["ensembl", "cosmic", "id", "phenotype"]}'
    name: 'vcf_anno_cosmic'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Get chromosomes with cosmic-id and phenotypes description'
    changes: '{"1.0.6": "docker gatk update", "1.0.5": "0-based and Number=. for multiallelic records", "1.0.1": "add returning tabix gz"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_vcf_gz: '{"name": "vcf_gz", "type": "File", "required": "true", "description": "vcf.gz file - will be annotated by query_vcf_gz_array and/or query_bed_gz_array", "extension": [".vcf.gz"]}'
    input_vcf_gz_tbi: '{"name": "vcf_gz_tbi", "type": "File", "required": "true", "description": "index for input_vcf_gz", "extension": [".vcf.gz.tbi"]}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_anno_cosmic

}

task vcf_anno_cosmic {
  String sample_id = "no_id_provided"
  String task_name = "vcf_anno_cosmic"
  String task_version = "1.0.6"
  Int? index
  File vcf_gz
  File vcf_gz_tbi

  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-cosmic:1.0.2"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    tabix -p bed /resources/cosmic-id-phenotype.bed.gz

#   change to Number=. in lines annotated with cosmic database cause of possible multiallelic records
    vcfanno /resources/conf.toml ${vcf_gz} \
      | sed '/ISEQ_COSMIC_PHENOTYPES/s/Number=1/Number=./g; /ISEQ_COSMIC_ID/s/Number=1/Number=./g' \
      | bgzip > ${sample_id}_annotated.vcf.gz

    tabix -p vcf ${sample_id}_annotated.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
    File annotated_vcf_gz = "${sample_id}_annotated.vcf.gz"
    File annotated_vcf_gz_tbi = "${sample_id}_annotated.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
