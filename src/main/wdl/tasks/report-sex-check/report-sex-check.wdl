workflow report_sex_check_workflow {

  meta {
    keywords: '{"keywords": ["report", "biological sex"]}'
    name: 'report_sex_check'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task generates table with results from several biological sex recognition tasks.'
    changes: '{"2.2.3": "add analysisEnvironment (IntelliseqFlow, T-Systems) input to report", "2.2.2": "remove language choose", "2.2.1": "report name update", "2.2.0": "change in reports template, docker and script", "2.1.0": "reports refactoring", "2.0.1": "new docker",2.0.0": "new report, new way of generating","1.0.1": "new docker with new templates"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_sample_info_json: '{"name": "Sample info json", "type": "File", "description": "Json file with sample info"}'
    input_sex_report_jsons: '{"name": "Biological sex recognition reports", "type": "Array[File]", "description": "Sex recognition results reports in json format"}'

    output_sex_check_report_pdf: '{"name": "sex_check_report_pdf", "type": "File", "copy": "true", "description": "biological sex recognition report in pdf file"}'
    output_sex_check_report_docx: '{"name": "sex_check_report_docx", "type": "File", "copy": "true", "description": "biological sex recognition report in docx file"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call report_sex_check

}

task report_sex_check {

  File? sample_info_json
  Array[File] sex_check_jsons
  String sample_id = "no_id_provided"
  String output_name = "sex-assessment"
  String outputs = "outputs"
  String analysisEnvironment = "IntelliseqFlow"

  Int timezoneDifference = 0

  String task_name = "report_sex_check"
  String task_version = "2.2.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_sex-check:1.2.1"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  mkdir ${outputs}

  bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                      --timezoneDifference ${timezoneDifference} \
                                                      --sampleID "${sample_id}"



    python3 /intelliseqtools/reports/templates/script/report-sex-check.py \
      --input-template "/resources/sex-check-template.docx" \
      --input-sample-info-json sample_info.json \
      --input-dict-json "/resources/dict-ang.json" \
      --input-sex-check-jsons ${sep=" " sex_check_jsons} \
      --analysis-run "${analysisEnvironment}" \
      --output-filename "${outputs}/${sample_id}_${output_name}-report.docx"

    soffice --headless \
      --convert-to pdf ${outputs}/${sample_id}_${output_name}-report.docx \
      --outdir ${outputs}



  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File report_sex_check_ang_docx = "${outputs}/${sample_id}_${output_name}-report.docx"
    File report_sex_check_ang_pdf = "${outputs}/${sample_id}_${output_name}-report.pdf"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
