workflow resources_polygenic_workflow
{
  call resources_polygenic
}

task resources_polygenic {

  String task_name = "resources_polygenic"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_resources-polygenic:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   cp /resources/polygenic.vcf.gz polygenic.vcf.gz
   cp /resources/polygenic.vcf.gz.tbi polygenic.vcf.gz.tbi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File polygenic_vcf_gz = "polygenic.vcf.gz"
    File polygenic_vcf_gz_tbi = "polygenic.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
