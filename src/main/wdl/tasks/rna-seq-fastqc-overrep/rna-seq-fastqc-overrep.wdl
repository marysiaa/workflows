workflow rna_seq_fastqc_overrep_workflow {

  meta {
    keywords: '{"keywords": ["Blast", "overrepresented sequences"]}'
    name: 'rna_seq_fastqc_overrep'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Getting overrepresented sequences'
    changes: '{"1.0.3": "Bux fixed in python script"}'

    input_zip_files: '{"name": "zip_files", "type": "Array[Array[File]]", "description": "zip files"}'

    output_overrepresented: '{"name": "overrepresented", "type": "File", "copy": "true", "description": "overrepresented in Excel"}'
    output_overrepresented_csv: '{"name": "overrepresented_csv", "type": "File", "copy": "true", "description": "overrepresented in CSV"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_fastqc_overrep

}

task rna_seq_fastqc_overrep {

  Array[Array[File]] zip_files
  Array[File] zip_files_flatten = flatten(zip_files)

  String task_name = "rna_seq_fastqc_overrep"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-fastqc-overrep:1.1.1"

  command <<<

  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    set -e pipefail

    ### create local swissprot database
    mkdir db
    wget -O db/swissprot.gz https://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/swissprot.gz
    gunzip db/swissprot.gz
    makeblastdb -dbtype prot -in db/swissprot

    mkdir zip
    ln -s ${sep=" " zip_files_flatten} zip
    unzip zip/\*.zip -d unzipped_fastqc
    cd unzipped_fastqc
    mkdir ../fastqc_data
    for dir in *     # list directories in the form "/unzipped_fastqc/sample/"
    do
        mv $dir/fastqc_data.txt ../fastqc_data/$dir.txt
    done
    cd ..

    ### Getting overrepresented sequences (part 1)
    python3 /intelliseqtools/fastqc-overrepresented-rna-seq-1.py --input-path-to-fastqc-files "fastqc_data" \
                                                                 --output-file-path "samples/"

    cd samples
    mkdir ../overrepresented_data
    for dir in *     # list directories in the form "/unzipped_fastqc/sample/"
    do
        FILE=$dir/'Overrepresented sequences.pkl'
        if [ -f "$FILE" ]; then
            mv $dir/'Overrepresented sequences.pkl' ../overrepresented_data/$dir.pkl
        else
            echo "File $FILE does not exist."
        fi
    done
    cd ..

    ### Getting overrepresented sequences (part 2)
    python3 /intelliseqtools/fastqc-overrepresented-rna-seq-2.py --input-path-to-overrepresented-files "overrepresented_data" \
                                                                 --output-file-name "overrepresented"

    ### Search protein databases using a translated nucleotide query using blastx
    python3 /intelliseqtools/fastqc-overrepresented-rna-seq-blast.py --input-overrepresented-file "overrepresented.xlsx" \
                                                                     --fasta-file-name "testfile.fasta" \
                                                                     --results-file-name "results.xml" \
                                                                     --database "db/swissprot" \
                                                                     --blast-excel-file-name "blast.xlsx" \
                                                                     --output-file-name "overrepresented"

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File overrepresented = "overrepresented.xlsx"
    File overrepresented_csv = "overrepresented.csv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}