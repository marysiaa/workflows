workflow vcf_vqsr_snp_recalib_workflow {

  meta {
    keywords: '{"keywords": ["SNP", "VQSR"]}'
    name: 'VQSLOD SNP recalibration'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Calculates VQSLOD tranches for SNPs using VariantRecalibrator.\nParameters set as in GATK4 pipeline (08-08-2020) https://github.com/gatk-workflows/gatk4-germline-snps-indels'
    changes: '{"1.0.4": "docker gatk update"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_sites_only_vcf_gz: '{"name": "Sites only vcf gz", "type": "File", "description": "Sites only vcf file (vcf without genotype columns)"}'
    input_sites_only_vcf_gz_tbi: '{"name": "Sites only vcf gz", "type": "File", "description": "Index for the sites only VCF file"}'
    input_use_allele_specific_annotations: '{"name": "Use allele specific annotations?", "type": "Boolean", "description": "Decides whether use allele specific annotations during VQSR recalibration"}'

    output_snps_recalibration_vcf: '{"name": "SNP recalibration table", "type": "File", "copy": "True", "description": "Vcf file with recalibration data"}'
    output_snps_recalibration_vcf_idx: '{"name": "SNP recalibration table index", "type": "File", "copy": "True", "description": "Index for recalibration vcf file"}'
    output_snps_recalibration_tranches: '{"name": "SNP recalibration tranches", "type": "File", "copy": "True", "description": "File with recalibration data"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_vqsr_snp_recalib

}

task vcf_vqsr_snp_recalib {

  String task_name = "vcf_vqsr_snp_recalib"
  String task_version = "1.0.4"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String ref_genome ="hg38"
  String docker_image = "intelliseqngs/task_vcf-vqsr-snp-recalib-" + ref_genome + ":1.1.1"


  # Known sites vcfs (paths to files in docker)
  String hapmap_vcf = "/resources/broad-institute-references-hg38/v0/hapmap_3.3.hg38.vcf.gz"
  String kg_omni_vcf = "/resources/broad-institute-references-hg38/v0/1000G_omni2.5.hg38.vcf.gz"
  String kg_phase1_vcf = "/resources/broad-institute-references-hg38/v0/1000G_phase1.snps.high_confidence.hg38.vcf.gz"
  String dbsnp_vcf = "/resources/broad-institute-references-hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf.gz"

  File sites_only_vcf_gz
  File sites_only_vcf_gz_tbi

  String sample_id = "no-id"
  String java_options = "-Xmx24g -Xms24g"
  ## gatk set max_gaussian parameter as 6 (but for high quality wgs data model failed to converge due to low variance)
  Int max_gaussians = 4

  # Recalibration parameters
  Boolean use_allele_specific_annotations = true
  Array[String] snp_recalibration_annotation_values = ["QD", "MQRankSum", "ReadPosRankSum", "FS", "MQ", "SOR", "DP"]
  Array[String] snp_recalibration_tranche_values = ["100.0", "99.95", "99.9", "99.8", "99.6", "99.5", "99.4", "99.3", "99.0", "98.0", "97.0", "90.0" ]

  command <<<
   # Note, that set -e -o pipefail is not given intentionally, as this task may fail which should not halt variant filtering module execution
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk --java-options "${java_options}" \
       VariantRecalibrator \
      -V ${sites_only_vcf_gz} \
      --trust-all-polymorphic \
      -tranche ${sep=' -tranche ' snp_recalibration_tranche_values} \
      -an ${sep=' -an ' snp_recalibration_annotation_values} \
      -mode SNP \
      --max-gaussians ${max_gaussians} \
      ${true='--use-allele-specific-annotations' false='' use_allele_specific_annotations} \
      -resource:hapmap,known=false,training=true,truth=true,prior=15 ${hapmap_vcf} \
      -resource:omni,known=false,training=true,truth=true,prior=12 ${kg_omni_vcf} \
      -resource:1000G,known=false,training=true,truth=false,prior=10 ${kg_phase1_vcf} \
      -resource:dbsnp,known=true,training=false,truth=false,prior=7 ${dbsnp_vcf} \
      -O ${sample_id}_snps-recalibration.vcf \
      --tranches-file ${sample_id}_snps-recalibration.tranches &> vqsr-log.txt


   if [ ! -f ${sample_id}_snps-recalibration.vcf ];then
      touch ${sample_id}_snps-recalibration.vcf
   fi

   if [ ! -f ${sample_id}_snps-recalibration.vcf.idx ];then
       touch ${sample_id}_snps-recalibration.vcf.idx
   fi

   if [ ! -f ${sample_id}_snps-recalibration.tranches ];then
       touch ${sample_id}_snps-recalibration.tranches
   fi

   if grep -q 'ERROR' vqsr-log.txt || ! grep -v '^#' ${sample_id}_snps-recalibration.vcf | grep -q '^'
   then
     mv ${sample_id}_snps-recalibration.tranches empty.tranches
   fi



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "26G"
    cpu: "2"
    maxRetries: 0

  }

  output {

    File snps_recalibration_vcf = "${sample_id}_snps-recalibration.vcf"
    File snps_recalibration_vcf_idx = "${sample_id}_snps-recalibration.vcf.idx"
    File snps_recalibration_tranches = glob("*.tranches")[0]

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
