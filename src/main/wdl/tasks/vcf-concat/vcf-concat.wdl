workflow vcf_concat_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "concatenation"]}'
    name: 'vcf_concat'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.4.2": "docker gatk update", "1.4.1": "output name change", "1.4.0": "Possible use of gatk or bcftools; vcf_gz_tbi input deleted due to a bug in MergeVcfs; update picard", "1.3.5": "merge versions 1.3.3 and 1.3.4", "1.3.4": "new docker", "1.3.3": "more memory when beagle_merge", "1.3.2": "Beagle version: dict changed", "1.3.1": "Added beagle version", "1.3.0": "changed vcf merge tool (bcftools -> MergeVcfs (picrad))", "1.2.4": "set -oe pipeline", "1.2.3": "Creates indexes for input vcfs, when not provided", "1.2.1": "delete the latest folder", "1.2.0": "new docker version (with new bco)"}'

    input_vcf_gz: '{"name": "VCF file (bgzipped)", "type": "Array[File]", "extension": [".vcf.gz"], "description": ""}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "no_id_provided", "description": "Sample ID"}'

    output_concatenated_vcf_gz: '{"name": "Concatenated VCF (bgzipped)", "type": "Files", "description": "Output VCF file (bgzipped) containing genotyped positions from dbSNP. Lines with ./. genotypes are filtered out."}'
    output_concatenated_vcf_gz_tbi: '{"name": "Concatenated VCF TBI index file", "type": "Files", "description": "Index of output VCF file (bgzipped)."}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_concat

}

task vcf_concat {

  Array[File] vcf_gz
  String vcf_basename = "no_id_provided"
  Boolean beagle_merge = false
  Boolean use_gatk = true
  String ref_dict_command = if (beagle_merge) then "-D /resources/reference-genome/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.dict" else ""

  String task_name = "vcf_concat"
  String task_version = "1.4.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = if (beagle_merge) then "intelliseqngs/task_vcf-concat-beagle:1.1.1" else "intelliseqngs/task_vcf-concat:1.0.2"

  command <<<
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
    set -e -o pipefail

    if ${use_gatk};then
        echo ${sep=" " vcf_gz} | sed 's/ /\n/g' > input_variant_files.list
        java -jar /resources/picard.jar MergeVcfs \
            -I input_variant_files.list \
            -O ${vcf_basename}.vcf.gz \
            ${ref_dict_command}
    else
        bcftools concat ${sep=" " vcf_gz} -o ${vcf_basename}.vcf.gz -O z
        tabix -p vcf ${vcf_basename}.vcf.gz
    fi

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: if beagle_merge then "32G" else "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File concatenated_vcf_gz = "${vcf_basename}.vcf.gz"
    File concatenated_vcf_gz_tbi = "${vcf_basename}.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
