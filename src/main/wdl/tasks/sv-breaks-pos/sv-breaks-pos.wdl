workflow sv_breaks_pos_workflow {

  meta {
    keywords: '{"keywords": ["SV", "breakends"]}'
    name: 'SV breaksends positions'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Creates list of SV breakend positions'
    changes: '{"1.0.1": "updated python script to work with gatk concat"}'

    input_vcf_gz: '{"name": "SV vcf", "type": "File", "description": "SV vcf file"}'
    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'

    output_position_list:  '{"name": "Position list", "copy": "True", "type": "File", "description": "Breakends positon list"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_breaks_pos

}

task sv_breaks_pos {

  String task_name = "sv_breaks_pos"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_sv-breaks-pos:1.0.2"

  File vcf_gz
  String sample_id = "no-id"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    bgzip -d ${vcf_gz} -c > tmp.vcf

    python3 /intelliseqtools/get-sv-pos.py \
        -i tmp.vcf \
        -o ${sample_id}_sv-breakends-positions.txt

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File position_list = "${sample_id}_sv-breakends-positions.txt"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
