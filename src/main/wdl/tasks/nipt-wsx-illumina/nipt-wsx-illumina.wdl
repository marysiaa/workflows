workflow nipt_wsx_illumina_workflow
{
  call nipt_wsx_illumina
}

task nipt_wsx_illumina {

  String task_name = "nipt_wsx_illumina"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_nipt-wsx-illumina:1.0.0"

  File illumina_csv
  Array[File] wsx_stats
  File? sample_info_json
  


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   mkdir wsx
   ln -s ${sep=" " wsx_stats} wsx/

   Rscript /intelliseqtools/wsx-illumina-plot.R \
       --wsx_dir wsx \
       --out_dir . \
       --cutoff /resources/AdjZCutOff.tsv \
       --illumina_nipt ${illumina_csv} \
       --example_samples /resources/cnv.tsv \
      ${"--sample_info " + sample_info_json} \
      --wsx_name_pattern "_.*_sorted-dup-marked_statistics.txt$"


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    Array[File] plots = glob("*.png")
    Array[File] reports = glob("*_report.tsv")
    File illumina_plot = glob("*.html")[0]

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
