workflow vcf_prepare_workflow {

  meta {
    keywords: '{"keywords": ["prepare vcf"]}'
    name: 'vcf_prepare'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.0.7": "add GT and sample number check", "1.0.6": "docker gatk update", "1.0.5": "added fixing of AN and AC tags and strange contigs removal", "1.0.4": "better error message to user", "1.0.3": "Reference check added, index input removed", "1.0.2": "pipefail restored, tbi is not required, input vcf may be uncompressed, removal of lines without any alt allele", "1.0.1": "pipefail set in the middle of the task", "1.0.0": "no changes"}'

    input_vcf: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz", ".vcf"], "description": "Vcf file to be annotated"}'

    output_prepared_vcf_gz: '{"name": "Prepared vcf", "type": "File", "copy": true, "description": "Vcf prepared"}'
    output_prepared_vcf_gz_tbi: '{"name": "Prepared vcf tabix index", "type": "File", "copy": true, "description": "Index for vcf prepared"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_prepare

}

task vcf_prepare {

  File vcf

  String map_file_path = "/resources/assembly_report/GRCh38.p13/map-file.tsv"
  String ref_path = "/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa"
  String vcf_basename = "no_id_provided"
  String vcf_file_name = basename(vcf)
  String error_message_to_user = "Coordinates in the input vcf file (" + vcf_file_name + ") correspond to reference genome other than GRCh38/hg38. Our workflows work only on vcf files in GRCh38/hg38 coordinates."

  String task_name = "vcf_prepare"
  String task_version = "1.0.7"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-prepare:1.0.2"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
   ## check if vcf file is bgzipped
   ## remove contig names from the header (to be sure that gatk checks reference and not header names)

   if [[ ${vcf} = *.vcf.gz ]];then
      zgrep -v "^##contig=" ${vcf} | bgzip  > ${vcf_basename}_input.vcf.gz

   elif [[ ${vcf} = *.vcf ]];then
      grep -v "^##contig=" ${vcf} | bgzip > ${vcf_basename}_input.vcf.gz
   fi
   tabix -p vcf ${vcf_basename}_input.vcf.gz

   ## check if file contains one sample only
   test `bcftools view -h ${vcf_basename}_input.vcf.gz | tail -1 | wc -w` -eq 10 ||  echo "Task failed with error code 86: VCF file should contain data for one sample only;" >&2 | exit 86

   ## check if file contains GT field
   bcftools view -h ${vcf_basename}_input.vcf.gz | grep -q 'FORMAT=<ID=GT' ||  echo "Task failed with error code 86: GT field must be present in the input VCF;" >&2 | exit 86 

   ## split multiallelic sites, rename chromosomes, fix AC and AN tags,
   ##remove "strange" contigs and remove lines with all ref/ref genotypes
   chroms="chr1,chr2,chr3,chr4,chr5,chr6,chr7,chr8,chr9,chr10,chr11,chr12,chr13,chr14,chr15,chr16,chr17,chr18,chr19,chr20,chr21,chr22,chrX,chrY,chrM"

   bcftools +fill-tags ${vcf_basename}_input.vcf.gz -- -t AC,AN \
   | bcftools annotate --rename-chrs ${map_file_path} \
   | bcftools norm -m -any  \
   | bcftools view --min-ac=1 -t $chroms -o  ${vcf_basename}_renamed-chrs.vcf.gz -O z
   tabix -p vcf ${vcf_basename}_renamed-chrs.vcf.gz

   ## remove ISEQ annotations
   annot_to_remove=$( tabix -H ${vcf_basename}_renamed-chrs.vcf.gz | grep -o 'ISEQ_[A-Za-z0-9_]*,\|ANN,\|NMD,\|CSQ,\|LOF,\|MITOMAP_[A-Za-z0-9_]*,\|Phylo[A-Za-z0-9_]*,\|Phast[A-Za-z0-9_]*,\|dbNSFP_[A-Za-z0-9_]*,' | sed 's/^/INFO\//' | tr '\n' ' ' | sed 's/ //g' | sed 's/,$//'  || [[ $? == 1 ]] )
   if [ -z $annot_to_remove ];then
       mv ${vcf_basename}_renamed-chrs.vcf.gz ${vcf_basename}_prepared.vcf.gz
       mv ${vcf_basename}_renamed-chrs.vcf.gz.tbi ${vcf_basename}_prepared.vcf.gz.tbi
   else
       bcftools annotate -x $annot_to_remove ${vcf_basename}_renamed-chrs.vcf.gz \
       | grep -v '##SnpEff' \
       | bgzip > ${vcf_basename}_prepared.vcf.gz; tabix -p vcf ${vcf_basename}_prepared.vcf.gz
   fi

   ## validate vcf file (format check only)
   gatk ValidateVariants \
      -V ${vcf_basename}_prepared.vcf.gz


   ## validate vcf file (add strict reference check)
   gatk ValidateVariants \
      -V ${vcf_basename}_prepared.vcf.gz \
      -R ${ref_path} \
      --validation-type-to-exclude CHR_COUNTS \
      --validation-type-to-exclude ALLELES \
      --validation-type-to-exclude IDS || echo "Task failed with error code 86: ${error_message_to_user};" >&2 | exit 86


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {
    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2
  }

  output {
    File prepared_vcf_gz = "${vcf_basename}_prepared.vcf.gz"
    File prepared_vcf_gz_tbi = "${vcf_basename}_prepared.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
  }
}