workflow pgx_astrolabe_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'pgx_astrolabe'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.1.1": "Change nomenclature", "1.1.0": "added bam input"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "Identifier of the sample"}'
    input_gvcf_gz: '{"name": "gvcf_gz", "type": "File", "extension": [".gvcf.gz", ".g.vcf.gz"], "description": "Sample data"}'
    input_bam: '{"name": "bam", "type": "File", "extension": [".bam"], "description": "Bam file, mardup.recalibrated"}'
   input_bai: '{"name": "bai", "type": "File", "extension": [".bai"], "description": "Index for bam file"}'

    output_cnv_tsv: '{"name": "CNV tsv", "type": "File", "description": "TSV text file with genotyping data from another pgx tool, specialised for Copy Number Variants (CNV)"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call pgx_astrolabe

}

task pgx_astrolabe {

  String task_name = "pgx_astrolabe"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/astrolabe:1.0.0"

  File gvcf_gz
  File? bam
  File? bai
  Boolean is_bam_defined = defined(bam)
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   if  ${is_bam_defined}; then
      /intelliseqtools/astrolabe-0.8.7.0/run-astrolabe.sh \
      -inputVCF ${gvcf_gz} \
      -inputBam ${bam} \
      -skipBamQC \
      -ref GRCh38 \
      -outFile ${sample_id}_astrolabe-out.tsv \
      -verboseFile ${sample_id}_astrolabe-verbose.txt \
      -novelFile ${sample_id}_astrolabe-novel.txt \
      -conf /intelliseqtools/astrolabe-0.8.7.0/astrolabe.ini
  else

    /intelliseqtools/astrolabe-0.8.7.0/run-astrolabe.sh \
      -inputVCF ${gvcf_gz} \
      -skipBamQC \
      -ref GRCh38 \
      -outFile ${sample_id}_astrolabe-out.tsv \
      -verboseFile ${sample_id}_astrolabe-verbose.txt \
      -novelFile ${sample_id}_astrolabe-novel.txt \
      -conf /intelliseqtools/astrolabe-0.8.7.0/astrolabe.ini
  fi 

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File cnv_tsv = "${sample_id}_astrolabe-out.tsv"
    File verbose_file = "${sample_id}_astrolabe-verbose.txt"
    File novel_file = "${sample_id}_astrolabe-novel.txt" 


  }

}
