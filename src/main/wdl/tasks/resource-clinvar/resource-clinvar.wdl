workflow resource_clinvar_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'resource_clinvar'
    author: 'https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task providing vcf from clinvar database, annotated with submission-summary.txt (clinical sgnificance based on clinical data) '
    changes: '{"1.0.4": "Update and reformat clinvar data (form multiallelic lines)", "1.0.3": "Remove latest directory for this task", "1.0.1": "Update clinvar data."}'
    output_clinvar_vcf_gz: '{"name": "clinvar_vcf_gz", "type": "File", "description": "vcf from clinvar database, annotated with submission-summary.txt"}'
    output_clinvar_vcf_gz_tbi: '{"name": "clinvar_vcf_gz_tbi", "type": "File"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call resource_clinvar

}

task resource_clinvar {

  String task_name = "resource_clinvar"
  String task_version = "1.0.4"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/resource-clinvar:0.1.4"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  cp /resources/clinvar-compare-acmg/*/* .
  

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File clinvar_vcf_gz = "clinvar-annotation-file.vcf.gz"
    File clinvar_vcf_gz_tbi = "clinvar-annotation-file.vcf.gz.tbi"
    File clinvar_bed_gz = "clinvar-annotation-file.bed.gz"
    File clinvar_bed_gz_tbi = "clinvar-annotation-file.bed.gz.tbi"

  }

}
