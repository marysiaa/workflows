workflow vcf_anno_repeat_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "simple repeats", "UCSC"]}'
    name: 'Vcf annotation with simple repeat'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Adds simple repeat annotation to vcf file.'
    changes: '{"1.0.2": "script fixed - proper repeat limits, removed latest dir"}'

    input_vcf_gz: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for the input vcf file"}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID"}'

    output_annotated_with_simple_repeats_vcf_gz :'{"name": "Annotated vcf", "type": "File", "copy": "True", "description": "Vcf file with simple repeat annotations added"}'
    output_annotated_with_simple_repeats_vcf_gz_tbi :'{"name": "Index file", "type": "File", "copy": "True", "description": "Index for the output vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_repeat

}

task vcf_anno_repeat {

  String task_name = "vcf_anno_repeat"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-repeat:1.0.1"

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename="no_id_provided"

  #String simple_repeat_bed_gz = "/resources/ucsc-simple-repeats/3-11-2019/simpleRepeat.bed.gz"

  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  set -e pipefail

  simple_repeat_bed_gz=$( ls /resources/*/*/simpleRepeat.bed.gz )

  python3 /intelliseqtools/simple-repeat.py  \
          --input ${ vcf_gz } \
          --repeats "$simple_repeat_bed_gz" \
           | bgzip > ${vcf_basename}_annotated-with-simple-repeats.vcf.gz

  tabix -p vcf ${vcf_basename}_annotated-with-simple-repeats.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_with_simple_repeats_vcf_gz = "${vcf_basename}_annotated-with-simple-repeats.vcf.gz"
    File annotated_with_simple_repeats_vcf_gz_tbi = "${vcf_basename}_annotated-with-simple-repeats.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
