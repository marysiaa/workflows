workflow rna_seq_custom_hisat_workflow {

  meta {
    keywords: '{"keywords": ["hisat2 index", "extract splice sites"]}'
    name: 'rna_seq_custom_hisat'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Reference genome indexing and splite sites extracting'
    changes: '{"1.0.1": "hisat2 newest version (2.2.1)"}'

    input_ref_genome: '{"name": "Fasta file", "type": "File", "copy": "True", "description": "Fasta file (reference genome)"}'
    input_gtf: '{"name": "GTF file", "type": "File", "copy": "True", "description": "GTF file"}'

    output_ref_genome_index: '{"name": "ref_genome_index", "type": "Array[File]", "copy": "True", "description": "Reference genome indexed"}'
    output_splicesites_file: '{"name": "splicesites_file", "type": "File", "copy": "True", "description": "Splicesites file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call rna_seq_custom_hisat

}

task rna_seq_custom_hisat {

  File ref_genome
  File gtf

  String genome_basename = "no_basename"
  String gtf_basename = "no_basename"

  String task_name = "rna_seq_custom_hisat"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/hisat2:1.2.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir genome
    cp ${ref_genome} genome
    gunzip genome/*
    mv genome/*.fa genome/${genome_basename}.fa

    hisat2-build genome/${genome_basename}.fa genome/${genome_basename}

    extract_splice_sites.py ${gtf} > ${gtf_basename}.splicesites

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    Array[File] ref_genome_index = glob("genome/*")
    File splicesites_file = "${gtf_basename}.splicesites"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
