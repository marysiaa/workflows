workflow vcf_acmg_ps3_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "PS3", "variant", "pathogenic"]}'
    name: 'Vcf ACMG PS3'
    author: 'https://gitlab.com/gleblavr , https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Evaluates whether variant fulfils ACMG PS3 criterion, adds annotation'
    changes: '{"1.5.0": "update UniProt , ubuntu and pysam", "1.4.0": "updates UniProt, pysam and cython", "1.3.0": "UniProt updated", "1.2.0": "Uniprot updated" ,"1.1.2": "Python script slightly changed, uniprot resources updated"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf"], "description": "Input annotated vcf"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True","type": "File", "description": "Vcf with ACMG PS3 annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_acmg_ps3

}

task vcf_acmg_ps3 {

  String task_name = "vcf_acmg_ps3"
  String task_version = "1.5.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-ps3:1.5.0"
  File vcf
  String sample_id = "no-id"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  bgzip -d ${vcf} -c > tmp.vcf

  uniprot_mutagenesis_data=$( ls /resources/uniprot/*/uniprot-mutagenesis-data-from-table.json )

  python3 /intelliseqtools/acmg-ps3.py \
        --input-vcf tmp.vcf \
        --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz \
        --uniprot-mut $uniprot_mutagenesis_data \
        --aa /intelliseqtools/aa-symbols.json


  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
