workflow rna_seq_multiqc_workflow
{
  call rna_seq_multiqc
}

task rna_seq_multiqc {

  Array[Array[File]] fastqc_zip_files
  Array[File] qc_results = flatten(fastqc_zip_files)
  Array[File] align_summaries
  Array[File]? bam_metrics
  File feature_count_gene_level_log
  Array[File]? infer_experiment_log
  String analysis_id = "no_id_provided"

  String task_name = "rna_seq_multiqc"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/multiqc:1.2.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      mkdir results
      mv ${sep=' ' qc_results} ${sep=' ' align_summaries} ${sep=' ' bam_metrics} ${feature_count_gene_level_log} ${sep=' ' infer_experiment_log} results/
      multiqc --exclude general_stats --filename ${analysis_id}_multiqc-report.html results/

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File report_html = "${analysis_id}_multiqc-report.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
