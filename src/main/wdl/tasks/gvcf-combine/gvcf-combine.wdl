workflow gvcf_combine_workflow {

  meta {
    keywords: '{"keywords": ["gvcf", "CombineGVCFs"]}'
    name: 'gvcf_combine'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Combine gvcfs using gatk CombineGVCFs'
    changes: '{"1.0.2" : "new docker - non-root permission to resources, add meta", "1.0.1": "fix bco error"}'

    input_gvcf_gz:'{"name": "Gvcfs gz", "type": "Array[File]", "description": "Array of gVCF files"}'
    input_gvcf_gz_tbi: '{ "name": "Gvcfs indexes", "type": "Array[File]", "description": "Array of gVCF files indexes"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "values": ["hg38", "grch38-no-alt"], "default": "hg38", "description": "Reference genome options: hg38 and grch38-no-alt"}'

    output_combined_gvcf_gz: '{"name": "Combined gzipped gvcf", "type": "File", "copy": "True", "description": "Output file from CombineGVCFs, GVCF combined from all the input samples"}'
    output_combined_gvcf_gz_tbi: '{"name": "Index of combined gzipped gvcf", "type": "File", "copy": "True", "description": "Index of the output file from CombineGVCFs"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call gvcf_combine

}

task gvcf_combine {

  Array[File] gvcf_gz
  Array[File] gvcf_gz_tbi

  String sample_id = "no_id_provided"
  String reference_genome = "hg38" # or grch38-no-alt

  File? intervals
  Boolean intervals_defined = defined(intervals)
  String intervals_arg = if intervals_defined then "--intervals " else ""

  String? interval_padding
  String interval_padding_defined = select_first([interval_padding, ""])
  String interval_padding_arg = if defined(interval_padding) then "--interval_padding " + interval_padding_defined else ""

  String java_options = "-Xmx5g -Xms5g"
  String gatk_path = "/gatk/gatk"

  String task_name = "gvcf_combine"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0-" + reference_genome + ":1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  ${gatk_path} --java-options "${java_options}" CombineGVCFs \
    ${intervals_arg} ${intervals} ${interval_padding_arg} \
    --reference /resources/Homo_sapiens_assembly38.fa \
    --variant ${sep=' --variant ' gvcf_gz} \
    --output ${sample_id}.combined.g.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File combined_gvcf_gz = "${sample_id}.combined.g.vcf.gz"
    File combined_gvcf_gz_tbi = "${sample_id}.combined.g.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
