workflow sex_check_sry_workflow {

  meta {
    keywords: '{"keywords": ["sex", "chromosome Y", "coverage"]}'
    name: 'sex_check_sry'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Biological sex recognition using depth coverage of SRY gene region'
    changes: '{"1.0.3":"update output json structure", "1.0.1": "update docker with bco"}'

    input_bam: '{"name": "bam", "type": "File", "description": "sample bam file", "extension": [".bam"]}'
    input_bai: '{"name": "bai", "type": "File", "description": "sample bai file", "extension": [".bai"]}'

    output_sex_report_json: '{"name": "Biological sex recognition report", "type": "File", "copy": "True", "description": "Sex recognition results report in json format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call sex_check_sry

}

task sex_check_sry {

  File bam
  File bai

  String task_name = "sex_check_sry"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/pysam:1.0.2"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  touch sex_recognition_sry_gene.json
  python3 /intelliseqtools/sex-check-SRY-gene.py ${bam} >> sex_recognition_sry_gene.json


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File sex_report_json = "sex_recognition_sry_gene.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
