# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  haplotype-caller
#  *********
#
#  Info:
#  -----
#    Name: haplotype_caller
#    Version: v0.1
#    Status: Prototype
#    Last edited: 26-07-19
#    Last edited by: <Macel Mroczek>
#    Author(s):
#      + <Katarzyna Kolanek> <katarzyna.kolanek@intelliseq.pl> <https://gitlab.com/lltw>
#    Maintainer(s):
#      + <Katarzyna Kolanek> <katarzyna.kolanek@intelliseq.pl> <https://gitlab.com/lltw>
#    Copyright: Copyright 2019 Intelliseq
#    Licence: All rights reserved
#
#  Description:
#  -----------
#    https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php
#
#  Changes from vy.y:  (OPTIONAL)
#  -------------------
#    <Changes introduced since the last version of the task>
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow haplotype_caller_workflow {
      call haplotype_caller
}

task haplotype_caller {

  File recalibrated_markdup_bam
  File recalibrated_markdup_bai
  String sample_id
  Float? contamination
  File interval_list
  # Inputs with defaults

  # Docker image
  String docker_image = "intelliseqngs/gatk:haplotype-caller_v0.1"

  # Tools runtime settings, paths etc.
  String gatk_path = "/gatk/gatk"
  String java_mem = "-Xms3000m"
  String java_opt = select_first(["-XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10"])

  # Task name and task version
  String task_name = "haplotype_caller"
  String task_ver = "v0.1"
  command <<<

    set -e

    gunzip /resources/reference-genome/Homo_sapiens_assembly38.fa.gz


          ${gatk_path} --java-options "${java_mem} ${java_opt}" \
          HaplotypeCaller \
          -ERC BP_RESOLUTION \
          -R /resources/reference-genome/Homo_sapiens_assembly38.fa \
          -I ${recalibrated_markdup_bam} \
          -L ${interval_list} \
          -O dbsnp_jakointerval.g.vcf.gz  2>&1 | tee -a ${sample_id}.haplotypecaller-gvcf.stdout.stderr.log

          JAVA_VER=`java -version 2>&1 | awk '/openjdk version/ {print $3}' | tr -d '"'`
          JAVA=`echo '"java_openjdk":"'$JAVA_VER',GPLv2"'`
          CONDA_VER=`conda info | awk '/conda version/ {print $4}'`
          CONDA=`echo '"miniconda":"'$CONDA_VER',BSD 3-Clause"'`
          UBUNTU_VER=`awk '/PRETTY_NAME/ {print $2}' /etc/*release`
          UBUNTU=`echo '"ubuntu":"'$UBUNTU_VER',GPL"'`
          REFGEN=`echo '"reference_genome":"GRCh38"'`

          echo "\""${task_name}"\""":{\"task-version\":\""${task_ver}"\","$JAVA","$CONDA","$UBUNTU","$REFGEN"}" > task-info.json


  >>>

  runtime {
    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2
  }

  output {
    File gvcf_gz = "dbsnp_jakointerval.g.vcf.gz"
    File gvcf_gz_tbi = "dbsnp_jakointerval.g.vcf.gz.tbi"
    # Logs
    File haplotypecaller_gvcf_stderr_log = "${sample_id}.haplotypecaller-gvcf.stdout.stderr.log"
    # Task info
    File task_info = "task-info.json"
  }

}
