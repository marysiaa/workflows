workflow vcf_sample_merge_workflow
{
  call vcf_sample_merge
}

task vcf_sample_merge {

  String task_name = "vcf_sample_merge"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.7"

  Array[File] vcfs
  Array[File] tbis
  String batch_id = "no_id_provided"
  Boolean missing_to_ref = true

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   bcftools merge \
       ${sep=" " vcfs}\
       ${true="--missing-to-ref" false="" missing_to_ref} \
        -o ${batch_id}_merged.vcf.gz -O z

   tabix -p vcf ${batch_id}_merged.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File merged_vcf = "${batch_id}_merged.vcf.gz"
    File merged_vcf_tbi = "${batch_id}_merged.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
