workflow array_split_workflow
{
  call array_split
}

task array_split {

  String task_name = "array_split"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.6"
  String sample_id
  File sample_mapping
  Array[File] fastqs

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
   
   #mkdir sample-files
   for fastq in ${sep=" " fastqs};do
     name=`basename $fastq`
     if grep -q -P "${sample_id}\t$name" ${sample_mapping}; then
        ln -s $fastq $name
     fi
   done  

    
   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    Array[File] sample_fastqs = glob("*f*q.gz")
    File stdout_log = stdout()
    File stderr_log = stderr() 
    File bco = "bco.json"

  }

}
