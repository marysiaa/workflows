workflow rna_seq_index_star_workflow
{
  call rna_seq_index_star
}

task rna_seq_index_star {

  File genome
  File gtf

  String task_name = "rna_seq_index_star"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/star:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir genome/

    STAR --runThreadN 10 \
         --runMode genomeGenerate \
         --genomeDir genome/ \
         --genomeFastaFiles ${genome} \
         --sjdbGTFfile ${gtf} \
         --sjdbOverhang 99

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "64G"
    cpu: 10
    maxRetries: 2

  }

  output {
    Array[File] ref_genome_index = glob("genome/*")
    File chromosomes = "genome/chrNameLength.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
