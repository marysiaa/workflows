workflow mobigen_plot_workflow {

  meta {
    keywords: '{"keywords": ["plot", "histogram", "genotype"]}'
    name: 'Plotting task for mobigen'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'This task plots results of mobigen-models task.'
    changes: '{"latest": "no changes"}'

    input_input_json: '{"name": "Input file", "type": "File", "description": "Output of mobigen-models task in json format"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_out_files: '{"name": "Output files", "type": "Array[File]", "copy": "True", "description": "Output images in SVG format"}'
  }

  call mobigen_plot

}

task mobigen_plot {

  String task_name = "mobigen_plot"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  String docker_image = "intelliseqngs/mobigen-plot:1.1.0"

  File input_json

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/after-start.sh)

  python3 /intelliseqtools/mobigen_plot.py -i ${ input_json }

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    Array[File] out_files = glob("*_model.svg")

  }

}
