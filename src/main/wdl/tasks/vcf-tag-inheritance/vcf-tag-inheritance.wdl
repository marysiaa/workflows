workflow vcf_tag_inheritance_workflow
{
  call vcf_tag_inheritance
}

task vcf_tag_inheritance {

  String task_name = "vcf_tag_inheritance"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-tag-inheritance:1.0.1"

  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   ## check if vcf is not empty
   if zgrep -q -v '^#' ${vcf_gz} ; then
       ## prepare per gene variant and alt allele counts data

       bcftools query -f '[%GT]\t%INFO/ISEQ_GENES_NAMES\n' ${vcf_gz} \
          | cut -f1 -d ':' | grep -v '\.$' | sed 's/0[/|]1/1/' | sed 's/1[/|]1/2/' > gt-gene.tsv

       awk '{arr[$2]+=$1 } END {for (key in arr) printf("%s\t%s\n", key, arr[key])}' gt-gene.tsv  | sort -k1,1 > gene-alt-count.tsv
       awk '{arr[$2]+=1 } END {for (key in arr) printf("%s\t%s\n", key, arr[key])}' gt-gene.tsv  | sort -k1,1 > gene-variant-count.tsv

   else
      touch gene-alt-count.tsv
      touch gene-variant-count.tsv
   fi

   python3 /intelliseqtools/add_inheritance_info.py \
      --input-vcf ${vcf_gz} \
      --output-vcf ${sample_id}_tagged.vcf.gz \
      --gene-variant-count gene-variant-count.tsv \
      --gene-alt-count gene-alt-count.tsv

   tabix -p vcf  ${sample_id}_tagged.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File tagged_vcf_gz = "${sample_id}_tagged.vcf.gz"
    File tagged_vcf_gz_tbi = "${sample_id}_tagged.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
