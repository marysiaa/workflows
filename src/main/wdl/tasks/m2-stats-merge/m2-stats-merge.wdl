workflow m2_stats_merge_workflow
{
  call m2_stats_merge
}

task m2_stats_merge {

  String task_name = "m2_stats_merge"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0:1.0.0"

  String sample_id = "no_id_provided"
  Array[File] m2_stats
  String java_mem = "-Xmx500m"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk --java-options "${java_mem}" MergeMutectStats \
            -stats ${sep=" -stats " m2_stats} \
             -O ${sample_id}_merged.stats


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File merged_m2_stats = "${sample_id}_merged.stats"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
