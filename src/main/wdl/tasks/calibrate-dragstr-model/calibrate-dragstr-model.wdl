workflow calibrate_dragstr_model_workflow
{
  call calibrate_dragstr_model
}

task calibrate_dragstr_model {

  File bam
  File bai
  String sample_id = "no_id_provided"

  String reference_genome = "grch38-no-alt" # another option: String reference_genome = "hg38"
  String reference_genome_whole_name = if (reference_genome == "hg38") then "broad-institute-" + reference_genome else reference_genome + "-analysis-set"
  String reference_genome_fasta_gz = "/resources/reference-genomes/" + reference_genome_whole_name + "/" + reference_genome_whole_name + ".fa.gz"
  String reference_genome_fasta = "/resources/reference-genomes/" + reference_genome_whole_name + "/" + reference_genome_whole_name + ".fa"
  String STRTableFile = "/resources/reference-genomes/" + reference_genome_whole_name + "/" + reference_genome_whole_name + ".str.zip"

  String task_name = "calibrate_dragstr_model"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_calibrate-dragstr-model-" + reference_genome + ":1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # unbgzip reference genome...
    gunzip ${reference_genome_fasta_gz}

    gatk CalibrateDragstrModel \
      -R ${reference_genome_fasta} \
      -I ${bam} \
      -str ${STRTableFile} \
      -O ${sample_id}.dragstr  


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File dragstr_model = "${sample_id}.dragstr"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
