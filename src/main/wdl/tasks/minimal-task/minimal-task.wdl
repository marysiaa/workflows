workflow minimal_task_workflow {

    call minimal_task

}

task minimal_task {

    String task_name = "minimal_task"
    String task_version = "2.1.1"

    Int exit_code = 0
    String? exit_info

    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.4"

    command <<<

        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

        if [ ${exit_code} -ne 0 ] ; then
            stderr_info="Task failed with error code ${exit_code}: ${exit_info};"
            echo $stderr_info >&2
            exit ${exit_code}
        fi

        bash /intelliseqtools/bco-before-finish.sh  --task-name ${task_name} \
                                                    --task-name-with-index ${task_name_with_index} \
                                                    --task-version ${task_version} \
                                                    --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: 1
        maxRetries: 2

    }

    output {

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }
}
