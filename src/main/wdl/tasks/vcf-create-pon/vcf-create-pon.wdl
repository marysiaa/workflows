workflow vcf_create_pon_workflow
{
  call vcf_create_pon
}

task vcf_create_pon {

  String task_name = "vcf_create_pon"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.4.1:1.0.0"

  File vcf_gz
  File vcf_gz_tbi
  String batch_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   gatk CreateSomaticPanelOfNormals \
        -V ${vcf_gz}\
        -O ${batch_id}_pon.vcf.gz




   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File pon_vcf_gz = "${batch_id}_pon.vcf.gz"
    File pon_vcf_gz_tbi = "${batch_id}_pon.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
