workflow vcf_split_chrom_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "gvcf", "chromosome-wise"]}'
    name: 'Vcf splitting to chromosomes'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Divides vcf (or gvcf) to chromosome wise vcf files.'
    changes: '{"1.2.0": "returns only vcfs with variants (or one empty for empty input vcf file)", "1.1.1": "y and the rest bug fix", "1.1.0": "latest removed, set -eo pipefail added", "1.0.4":"possible not to return y-and the rest (default to return) and splitting g.vcf files","1.0.3":"3G memory", "1.0.2": "works if not all contigs present - needed in sv-calling"}'

    input_vcf_basename: '{"name": "Vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID."}'
    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file"}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
    input_big_input : '{"name": "Big input file?", "type": "Boolean", "description": "Change to true if dealing with big input file (for example genomic gvcf)"}'
    input_gvcf_analysis : '{"name": "GVCF analysis", "type": "Boolean", "description": "Decides whether g will be added to output files extension"}'

    output_chromosomes_vcf_gz : '{"name": "Vcf files", "type": "Array[File]", "description": "vcf files divided by chromosomes"}'
    output_chromosomes_vcf_gz_tbi : '{"name": "Vcf files indexes", "type": "Array[File]", "description": "Vcf files indexes  divided by chromosomes"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_split_chrom

}

task vcf_split_chrom {

  String task_name = "vcf_split_chrom"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.5"

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename = "no_id_provided"
  String max_no_jobs_in_parallel = "24"
  Boolean return_y = true
  Boolean gvcf_analysis = false
  String gvcf_extension = if (gvcf_analysis) then ".g" else ""

  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  ## lists of contigs with variants
  tabix -l ${vcf_gz} | grep -w 'chr[1-9]\|chr[1-2][0-9]\|chrX' > normal_chroms
  tabix -l ${vcf_gz} | grep -wv 'chr[1-9]\|chr[1-2][0-9]\|chrX' > other_chroms

  set -e -o pipefail
  ## normal chromosomes
  if grep -q '^' normal_chroms;then
     cat normal_chroms | xargs -i -n 1 -P ${max_no_jobs_in_parallel}  bash -c \
     "tabix -h ${vcf_gz} {} \
    | bgzip > {}-${vcf_basename}${gvcf_extension}.vcf.gz"
  fi

  ## Y and the rest
  if ${return_y} && grep -q '^' other_chroms; then
      tabix -h ${vcf_gz}  `cat other_chroms | tr '\n' ' '` \
      | bgzip > "chrY-and-the-rest"-${vcf_basename}${gvcf_extension}.vcf.gz
  fi

  ##  files were not created - output one empty vcf for scatter
  if [[ `ls *vcf.gz | wc -w` -eq 0 ]];then
     mv ${vcf_gz} chr22-${vcf_basename}${gvcf_extension}.vcf.gz
  fi

  ## create indexes
  ls *vcf.gz | xargs -i -n 1 -P ${max_no_jobs_in_parallel} bash -c "tabix -f -p vcf {}"

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    Array[File] chromosomes_vcf_gz = glob("*.vcf.gz")
    Array[File] chromosomes_vcf_gz_tbi = glob("*.vcf.gz.tbi")

  }

}