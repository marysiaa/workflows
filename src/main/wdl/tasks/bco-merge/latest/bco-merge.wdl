workflow bco_merge_workflow {

  meta {
    keywords: '{"keywords": ["merging bco"]}'
    name: 'Merge .bco files'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Merges bco, stdout and stderr files from previous tasks'
    changes: '{"1.4.1": "No error with no keywords in the description domain", "1.4.0": "From now, bcos merging is done through a Python script"}'
    input_bco_array: '{"name": "Biocompute objects", "type": "Array[File]", "copy": "True", "description": "Biocompute objects"}'
    input_stdout_array: '{"name": "Standard out files", "type": "Array[File]", "copy": "True", "description": "Standard out files"}'
    input_stderr_array: '{"name": "Standard error files", "type": "Array[File]", "copy": "True", "description": "Standard out files"}'
    input_module_name: '{"name": "Module name", "type": "String", "copy": "True", "description": "Module name"}'
    input_module_version: '{"name": "Module version", "type": "String", "copy": "True", "description": "Module version"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bco_merge

}

task bco_merge {

  Array[File]? bco_array
  Array[File]? stdout_array
  Array[File]? stderr_array

  String module_name = "no_module_name_provided"
  String module_version = "no_module_version_provided"
  String? pipeline_name
  String? pipeline_version

  String task_name = "bco_merge"
  String task_version = "1.4.1"
  String docker_image = "intelliseqngs/task_bco-merge:1.0.1"

  command <<<

    mkdir bcos
    i=1; for file in ${sep=" " bco_array}; do mv $file bcos/bco_$i.json; let i=$i+1; done

    if [[ ! -z "${pipeline_name}" ]]
    then
        python3 /intelliseqtools/bco-merge.py --input-path "bcos" \
                                              --pipeline-name ${pipeline_name} \
                                              --pipeline-version ${pipeline_version} \
                                              --output-file "bco.json"
    else
        python3 /intelliseqtools/bco-merge.py --input-path "bcos" \
                                              --module-name ${module_name} \
                                              --module-version ${module_version} \
                                              --output-file "bco.json"
    fi

    cat ${sep=" " stdout_array} > merged.stdout
    cat ${sep=" " stderr_array} > merged.stderr

  >>>

  runtime {

    maxRetries: 3
    docker: docker_image
    memory: "1G"
    cpu: "1"

  }

  output {

    File stdout_log = "merged.stdout"
    File stderr_log = "merged.stderr"
    File bco = "bco.json"

  }
}