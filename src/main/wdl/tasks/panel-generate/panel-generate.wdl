workflow panel_generate_workflow {

  meta {
    keywords: '{"keywords": ["gene", "hpo"]}'
    name: 'Panel generate'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generates gene panel based on HPO terms and user defined genes'
    changes: '{"2.0.2": "change prod.api to localhost", "2.0.1": "small bug fix", "2.0.0": "Script refactor; changed: endpoint, input types, docker content", "1.8.3": "json adaptation to the same as from string-gp-to-json task", "1.8.2": "meta groupname minor change", "1.8.1": "fix apostrophes in diseases input", "1.8.0": "Fix letter bug in warnings (double by).","1.7.1": "fixed a bug with the phenotype description (removal of apostrophes)", "1.7.0": "Phenotypes as ", "1.6.10": "All panels added as output", "1.6.9": "Gene panels resources updated, now score is 75 for user defined genes, 50 for genes associated with the specified diseases and 30 for genes from panel. Moreover, if the score from the phenotype is higher, the score is increased", "1.6.8": "deleted None from panel_names values", "1.6.5": "panel names update"}'

    groups: '{"gene_panel": {"description": "Fill in at least 1 input below to generate the gene panel", "rules": "Fill at least one input", "hReadableName": "Gene panel", "min_inputs": 1}}'

    input_sample_id: '{"index": 1, "name": "Sample ID", "type": "Array[String]", "description": "Enter a sample name (or identifier)"}'
    input_hpo_terms: '{"index": 2, "name": "HPO terms", "type": "Array[String]", "groupname": "gene_panel", "description": "Enter HPO terms to narrow your search/analysis results (separate HPO terms with comma, for example: HP:0004942, HP:0011675)"}'
    input_genes: '{"index": 3, "name": "Genes names", "type": "Array[String]", "groupname": "gene_panel", "description": "Enter gene names to narrow your search/analysis results (separate gene names with comma, for example: HTT, FBN1)"}'
    input_diseases: '{"index": 4, "name": "Diseases", "type": "Array[String]", "groupname": "gene_panel", "description": "Enter disease names to narrow your search/analysis results (separate diseases names with comma; each disease name should be just a keyword, for example for Marfan Syndrome only Marfan should be written, for Ehlers-Danlos Syndrome: Ehlers-Danlos; other proper diseases names for example: Osteogenesis imperfecta, Tay-sachs, Hemochromatosis, Brugada, Canavan, etc.)"}'
    input_phenotypes_description: '{"index": 5, "name": "Description of patient phenotypes", "type": "String", "groupname": "gene_panel", "description": "Enter description of patient phenotypes"}'
    input_panel_names: '{"index": 6, "name": "Gene panel", "type": "Array[String]", "groupname": "gene_panel", "description": "Select gene panels", "constraints": {"values": ["ACMG_Incidental_Findings", "COVID-19_research", "Cancer_Germline", "Cardiovascular_disorders", "Ciliopathies", "Dermatological_disorders", "Dysmorphic_and_congenital_abnormality_syndromes","Endocrine_disorders", "Gastroenterological_disorders", "Growth_disorders","Haematological_and_immunological_disorders", "Haematological_disorders", "Hearing_and_ear_disorders", "Metabolic_disorders", "Neurology_and_neurodevelopmental_disorders","Ophthalmological_disorders", "Rare_Diseases", "Renal_and_urinary_tract_disorders","Respiratory_disorders", "Rheumatological_disorders", "Skeletal_disorders","Tumour_syndromes"], "multiselect": true}}'

    output_panel: '{"name": "panel", "type": "File", "copy": "True", "description": "Genes panel"}'
    output_phenotypes: '{"name": "phenotypes", "type": "File", "copy": "True", "description": "Hpo names taken from hpo ids"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call panel_generate

}

task panel_generate {

  String sample_id = "no_id_provided"
  Array[String]? hpo_terms
  Array[String]? genes
  Array[String]? diseases

  String dollar = "$"
  String? phenotypes_description
  Array[String]? panel_names

  String task_name = "panel_generate"
  String task_version = "2.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_panel-generate:2.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # -1. Run the service
    WORKDIR=$(pwd)
    cd /tools/explorare
    java -Xmx4G -jar app.jar > /tmp/explorare.log &
    ITER=0
    MAXITER=300
    while ! grep -m1 'Started ExplorareServer' < /tmp/explorare.log; do
        sleep 1
        echo "Waiting for explorare... $ITER"
        let "ITER=ITER+1"
        if [ "$ITER" -gt "$MAXITER" ]; then
            break
    fi
    done
    cd $WORKDIR

    ### Removal of apostrophes from the phenotype description
    DESCRIPTION_TO_BE_CHANGED="${phenotypes_description}"
    PHENOTYPES_DESCRIPTION=$(sed "s#'##g" <<< $DESCRIPTION_TO_BE_CHANGED)

    # 0. Get hpo terms from phenotypes description
    parsedDescription=$(curl --request POST \
     --url 'http://localhost:8080/parse-text' \
     --header 'content-type: application/json' \
     --data '{"query": "'"$PHENOTYPES_DESCRIPTION"'"}')

    hpoTermsFromDescription=$(echo $parsedDescription | jq -r '.hpoTerms[]? | .id')
    hpoTermsConcat="${sep=',' hpo_terms} $hpoTermsFromDescription"

    # 1. hpo terms
    hpo=$(echo $hpoTermsConcat | sed -r 's/(HP:[[:digit:]]+)[^[:digit:][:alpha:]]*/"\1",/g' | sed 's/,$//' | awk '{print "["$0"]"}')

    curl --request POST \
     --url 'http://localhost:8080/get-genes' \
     --header 'content-type: application/json' \
     --data '{"hpoTerms": '"$hpo"',"threshold": 0.25}' > curl.output

    if grep -q "error" curl.output; then
        cat curl.output >&2
        echo "Probably something wrong with HPO ids" $hpo >&2
        exit 1
    fi

    cat curl.output  | jq -c '[ .[] +{"type":"Phenotypes"} ]' > phenotype-panel.json

    # 2. validate and get user input genes
    raw_genes="${sep=',' genes}"

    if [ ! -z $raw_genes ]
    then
        array_genes=[\"${dollar}raw_genes\"]
        array_genes=$(echo $array_genes | sed 's/,/","/g')

        curl --request POST \
             --url 'http://localhost:8080/validate-genes' \
             --header 'content-type: application/json' \
             --data "${dollar}{array_genes}" | jq '.corrected' | jq -c '[ .[] +{"type":"Genes names"} ]' > user-panel.json
    fi

    # 3. diseases genes
    raw_diseases="${sep=',' diseases}"
    no_apo_s_diseases="$(sed "s#'s##g" <<< $raw_diseases)"
    no_apo_diseases=$(sed "s#'##g" <<< $no_apo_s_diseases)

    if [ ! -z "$raw_diseases" ]
    then
        array_diseases=[\"${dollar}no_apo_s_diseases\"]
        array_diseases=$(echo $array_diseases | sed 's/,/","/g')

        diseases_url='http://localhost:8080/get-genes-by-diseases-list'

        curl  --request POST \
              --url $diseases_url \
              --header 'content-type: application/json' \
              --data "${dollar}{array_diseases}" | jq -c '[ .[] +{"type":"Diseases"} ]' > diseases-panel.json
    fi


    # 4. Getting selected panels from explorare
    selected_panels=${sep=',' panel_names}

    if [ ! -z "$selected_panels" ]
    then
        selected_panels=$(echo $selected_panels | sed 's/_/%20/g')
        selected_panels_url=http://localhost:8080/get-panels-genes?panels=$selected_panels
        curl --request GET \
            --url $selected_panels_url \
            | jq -c '[ .[] +{"type":"Gene panels"} ]'  > selected-panel.json

        chmod 666 selected-panel.json
    fi

    # 5. merging panels
    jq -s 'add' *-panel.json > all_panels.json
    jq -s 'add' *-panel.json | jq -M 'sort_by(.score)|reverse|unique_by(.name)|sort_by(.score)|reverse' | jq -c . > not_validated_panel.json
    # 6. Getting hpo names from hpo terms

    unformattedHpoNames=$(curl --request POST \
     --url 'http://localhost:8080/get-hpo-names-by-id' \
     --header 'content-type: application/json' \
     --data '{"hpoTerms": '"$hpo"', "threshold": 0.25}')

    readarray -t hpoNames < <(echo $unformattedHpoNames | jq -r '.[].name' )
    readarray -t hpoIds < <(echo $unformattedHpoNames | jq -r '.[].id' )

    outputNameId="["
    for ((i=0;i<${dollar}{#hpoIds[@]};++i))
    do
      outputNameId=$outputNameId'"'${dollar}{hpoNames[i]}' '${dollar}{hpoIds[i]}'",'
    done
    outputNameId=$(echo $outputNameId] | sed 's/\(.*\),/\1/')

    echo '{"hpo_terms": "${sep=', ' hpo_terms}", "genes": "${sep=', ' genes}", "diseases": "'"$no_apo_diseases"'", "phenotypes_description": "'"$PHENOTYPES_DESCRIPTION"'", "panel_names": "${sep=',' panel_names}", "phenotypes": '"$outputNameId"'}' > panel_inputs.json
    jq '.panel_names |= split(",") | .diseases |= split(",")' panel_inputs.json > panel_inputs.json.tmp && mv panel_inputs.json.tmp panel_inputs.json

    python3 /intelliseqtools/synonym_panel_validation.py \
       --panel not_validated_panel.json \
       --panel_output "${sample_id}_panel.json" \
       --warning_output "${sample_id}_panel_logs.json"

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File panel = "${sample_id}_panel.json"
    File gene_panel_logs = "${sample_id}_panel_logs.json" 
    File inputs = "panel_inputs.json"
    File all_panels = "all_panels.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
