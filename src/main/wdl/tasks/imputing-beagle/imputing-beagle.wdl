workflow imputing_beagle_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "imputing"]}'
    name: 'Imputing with Beagle software'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Imputation of ungenotyped SNPs'
    changes: '{"2.1.0": "dbSNP 155 based reference", "2.0.0": "better reference data, ability to run the task with prodia-spesicic reference data", "1.0.1": "new docker image with fixed docker size in BCO and new Beagle version", "1.0.0": "resources: fixed location"}'

    input_phased_files: '{"name": "Phased files", "type": "Array[File]", "extension": [".vcf.gz"], "description": "Array of phased files corresponding to each chromosome"}'
    input_threads: '{"name": "Beagle threads", "type": "Int", "description": "Number of threads used by Beagle software", "required":false}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": true, "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": true, "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": true, "description": "Biocompute object"}'
    output_out_files: '{"name": "Imputed files", "type": "Array[File]", "copy": true, "description": "Array of imputed files corresponding to each chromosome"}'

  }

  call imputing_beagle

}

task imputing_beagle {

  String task_name = "imputing_beagle"
  String task_version = "2.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  Boolean prodia_reference = false
  String docker_image = if prodia_reference then "intelliseqngs/task_imputing-beagle:3.0.0-prodia-ref" else "intelliseqngs/task_imputing-beagle:3.1.0"

  Array [File] phased_files
  Array[String] chromosomes = ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX"]
  Int threads = 6

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  chroms_string=$( IFS=$''; echo "${sep=',' chromosomes}" )
  chroms_string_wihtout_chr=$( echo $chroms_string | sed "s/chr//g" )

  for file in ${sep=' ' phased_files}; do
        ln $file .
    done

  beagle_wrapper\
                `pwd` \
                `pwd` \
                `pwd` \
                --beagle_threads ${threads} \
                --chromosomes $chroms_string_wihtout_chr \
                --beagle_ref_data_dir /resources/ref_data/grch38 \
                --beagle_genetic_maps_dir /resources/genetic_maps/grch38 \
                --beagle_path /tools/beagle/5.1/beagle.18May20.d20.jar

  ### stderr logs
    for file in *_err; do
        >&2 echo "$file content:"
        cat $file >&2
        >&2 echo
    done

    ### stdout logs
    echo "imputing_beagle.log content:"
    cat imputing_beagle.log
    for file in *_out; do
        echo
        echo "#########################################################################################################"
        echo "$file content:"
        cat $file
    done

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: threads
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    Array[File] out_files = glob("*_imputed.vcf.gz")

  }

}