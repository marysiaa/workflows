workflow qc_fq_mapping_workflow {

  meta {
    keywords: '{"keywords": ["quality check", "fastq", "samtools flagstat"]}'
    name: 'Fastq mapping quality stats'
    author: 'https://gitlab.com/marysiaa https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Generates json with fastq mapping quality statistics.\n Analyses are based on bam file resulting from BWA MEM alignment of 1000000 reads from both fastq files.'
    changes: '{"1.0.3": "change cpu to 2", "1.0.2": "enlarge memory"}'

    input_fastq_1: '{"name": "Fastq 1", "type": "File", "extension": [".fq.gz"], "description": "First fastq file"}'
    input_fastq_2: '{"name": "Fastq 2", "type": "File", "extension": [".fq.gz"], "description": "Second fastq file"}'
    input_bam_warnings_file: '{"name": "File with bam warnings stats", "type": "File", "description": "Path to file with descriptions and warnings for bams, set as empty string, if not needed"}'
    input_percent: '{"name": "Percent", "type": "String", "description": "Controls for which statistics percent should be outputed in json file"}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"values": ["hg38", "grch38-no-alt"]}, "description": "Version of the reference genome to align reads to."}'

    output_mapping_stats_txt: '{"name": "Mapping quality statistics txt", "type": "File", "copy": "True", "description": "Output from the samtools flagstat."}'
    output_mapping_stats_json: '{"name": "Mapping quality statistics json", "type": "File", "copy": "True", "description": "Samtools flagstat output converted to json format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    }

  call qc_fq_mapping

}

task qc_fq_mapping {

  String task_name = "qc_fq_mapping"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String reference_genome = "hg38"
  String reference_genome_whole_name = if (reference_genome == "hg38") then "broad-institute-" + reference_genome else reference_genome + "-analysis-set"

  String docker_image = "intelliseqngs/task_qc-fq-mapping-" + reference_genome + ":1.0.0"

  File fastq_1
  File fastq_2

  String reference_genome_fasta_gz = "/resources/reference-genomes/" + reference_genome_whole_name + "/" + reference_genome_whole_name + ".fa.gz"
  String reference_genome_fasta = "/resources/reference-genomes/" + reference_genome_whole_name + "/" + reference_genome_whole_name + ".fa"
  String reference_genome_fasta_fai = "/resources/reference-genomes/" + reference_genome_whole_name + "/" + reference_genome_whole_name + ".fa.fai"

  String bam_warnings_file = "/intelliseqtools/bam-flagstats-description.txt" ## set as "", if not additional descriptions needed

  ## names of statistic for which percent should be calculated and outputed
  ## names should be exactly as output statistics from samtools flagstat txt file, but with spaces replaced by "_"
  ## for example 'in_total mapped properly_paired singletons', if "all" given, percentages will be calculated for all statistics
  ## except for read1 and read2
  String percent = 'all'

  String dollar = "$"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/after-start.sh)

    ## 1. prepare basename for output files (intesection of basename1 and basename2)
    basename1=$( basename "${fastq_1}" ); basename2=$( basename "${fastq_2}" )
    if [ $basename1 == $basename2 ]; then
      echo 'ERROR: The filenames are identical.' >/dev/stderr
      exit 1
    else
      echo 'rozne'
      fn=''; n=0; while [[ ${dollar}{basename1:n:1} == ${dollar}{basename2:n:1} ]]; do fn+=${dollar}{basename1:n:1}; ((n++)); done
    fi
    filename=${dollar}{fn::-1}


    ## 2. take first 100000 reads of fastq_1 and fastq_2
  	zcat -f ${fastq_1}  | head -400000 > fastq_1_part.fq
    zcat -f ${fastq_2}  | head -400000 > fastq_2_part.fq

    ## 3. prepare reference genome

    gunzip  ${reference_genome_fasta_gz}

    ##  4. align with BWA MEM
    set -o pipefail
    set -e


    bwa mem \
      -t 2 \
      -K 10000000 \
      -v 3 \
      -Y ${reference_genome_fasta} \
      fastq_1_part.fq fastq_2_part.fq  \
      | samblaster  \
      | samtools sort -@ 2 - > $filename"-mapping-stats-markdup.bam"

    ## 5. Calculate statistics
    samtools flagstat $filename"-mapping-stats-markdup.bam" > $filename"-mapping-stats.txt"

    ## 6. Prepare json
    python3 /intelliseqtools/samtools-flagstats-to-json.py \
            --input $filename"-mapping-stats.txt" \
            --output $filename"-mapping-stats.json" \
            --percent ${percent} --warnings ${bam_warnings_file}

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File mapping_stats_txt = glob("*-mapping-stats.txt")[0]
    File mapping_stats_json = glob("*-mapping-stats.json")[0]
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
