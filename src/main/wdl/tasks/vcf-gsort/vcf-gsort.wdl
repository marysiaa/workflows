workflow vcf_gsort_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "sort"]}'
    name: 'Vcf gsort'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Coordinate sorts vcf file, keeping contigs order specified by user '
    changes: '{"latest": "no changes"}'

    input_contig_tab: '{"name": "Contig tab", "type": "File", "description": "Tab delimited file with contigs names and lengths"}'
    input_vcf_gz: '{"name": "Vcf gz", "type": "File", "description": "Vcf file to sort"}'

    output_sorted_vcf_gz: '{"name": "Sorted vcf gz", "type": "File", "copy": "True", "description": "Coordinate sorted vcf"}'
    output_sorted_vcf_gz_tbi: '{"name": "Sorted vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the coordinate sorted vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_gsort

}

task vcf_gsort {

  String task_name = "vcf_gsort"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gsort:1.0.0"

  File vcf_gz
  File contig_tab
  String basename = basename(vcf_gz, ".vcf.gz")
  String memory = "--memory 1500"

  command <<<

   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   n=$( zcat ${vcf_gz} | grep -v '^#'| wc -l )

   set -e -o pipefail
   if [ $n -gt 0 ]; then
       printf 'sorting...\n'
       gsort ${memory} ${vcf_gz} ${contig_tab} | bgzip > ${basename}-sorted.vcf.gz
   else
       mv ${vcf_gz} ${basename}-sorted.vcf.gz
   fi
   tabix -p vcf ${basename}-sorted.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File sorted_vcf_gz = "${basename}-sorted.vcf.gz"
    File sorted_vcf_gz_tbi = "${basename}-sorted.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
