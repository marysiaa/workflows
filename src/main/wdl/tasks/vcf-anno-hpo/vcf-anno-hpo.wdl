workflow vcf_anno_hpo_workflow
{
  call vcf_anno_hpo
}

task vcf_anno_hpo {
    
  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename = "no_id_provided"
  Boolean sv = false
    
  String task_name = "vcf_anno_hpo"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-hpo:1.0.3"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   python3 /intelliseqtools/vcf_anno_hpo.py --input_vcf ${vcf_gz} \
                                            --output_vcf ${vcf_basename}_anno-hpo.vcf.gz ${true="--sv" false="" sv} 
                                            
   tabix -p vcf ${vcf_basename}_anno-hpo.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
      
    File annotated_with_hpo_vcf_gz = "${vcf_basename}_anno-hpo.vcf.gz"
    File annotated_with_hpo_vcf_gz_tbi = "${vcf_basename}_anno-hpo.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
