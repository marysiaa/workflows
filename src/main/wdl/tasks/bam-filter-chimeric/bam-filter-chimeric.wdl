workflow bam_filter_chimeric_workflow
{
  call bam_filter_chimeric
}

task bam_filter_chimeric {

  String task_name = "bam_filter_chimeric"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.7"

  File bam
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   ## find names of chimeric reads (and their mates, if paired sequencing)
   samtools view -e '[SA]' ${bam} | cut -f1 | sort | uniq > ${sample_id}_chimeric-reads.txt

   ## find names of all reads
   samtools view ${bam} | cut -f1 | sort | uniq > all_reads

   ## find names of reads to keep (present in all reads, but not in chimeras)
   sort -m all_reads  ${sample_id}_chimeric-reads.txt | uniq -c | awk '{if ($1==1) {print $2}}' > reads_to_keep

   ## keep only reads from the "reads_to_keep" list
   samtools view -h -N reads_to_keep ${bam} -o ${sample_id}_filtered.bam

   ## calculate percent of chimeric reads/pairs
   CHIMERAS=$( cat ${sample_id}_chimeric-reads.txt | wc -l)
   ALL=$(cat all_reads | wc -l)
   if [ $ALL -gt 0 ];then
       awk -v chim=$CHIMERAS -v all=$ALL 'BEGIN {printf "%.2f ", (chim * 100)/ all}' > chimeras_percent.txt
   else
       echo 0 > chimeras_percent.txt
   fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File filtered_bam = "${sample_id}_filtered.bam"
    File chimeric_reads = "${sample_id}_chimeric-reads.txt"
    Float chimeras_percent = read_float("chimeras_percent.txt")
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
