workflow prs_health_report_workflow {

  call prs_health_report

}

task prs_health_report {

  Array[File] models
  File? sample_info_json

  String sample_id = "no_id_provided"
  Int timezoneDifference = 0
  String analysisEnvironment = "IntelliseqFlow"

  String task_name = "prs_health_report"
  String task_version = "1.1.7"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report-prs:1.0.8"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      # Prepare sample-info.json
      bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                        --timezoneDifference ${timezoneDifference} \
                                                        --sampleID "${sample_id}"

      # merge models
      python3 /intelliseqtools/all_models_to_one_json.py --models ${sep=" " models} --output-dir .

      mkdir results/
      python3 /intelliseqtools/prs-reports/fill-html-template.py --json models=all_models.json,content=/intelliseqtools/prs-reports/resources/content.json,sample_info=sample_info.json \
                                                                 --template /intelliseqtools/prs-reports/src/views/health-template.html \
                                                                 --analysis-run "${analysisEnvironment}" \
                                                                 --output-filename health.html

      WORKING_DIR=$(pwd)

      cd /intelliseqtools/prs-reports/
      npm run build
      npm run lint:js
      npm run lint:style

      cd $WORKING_DIR

      cp /intelliseqtools/prs-reports/public/health.html results/${sample_id}-health.html

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File result_health_html = "results/${sample_id}-health.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
