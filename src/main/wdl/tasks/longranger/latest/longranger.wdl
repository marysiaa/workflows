workflow longranger_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'longranger'
    author: 'https://gitlab.com/mremre'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'longranger \n wgs analysis with 10xgenomics tool'
    changes: '{"latest": "no changes"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call longranger

}

task longranger {

  String task_name = "longranger"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/resources:longranger_v0.1"

  File fastq1
  File fastq2
  File fastq3
  File fastq4
  File fastq5
  File fastq6
  File fastq7
  File fastq8
  File fastq9
  File fastq10
  File fastq11
  File fastq12


  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"

  export PATH=/opt/tools/longranger-2.2.2:$PATH



    longranger wgs --id=PALMAR \
                   --reference=/resources/refdata-GRCh38-2.1.0 \
                   --fastqs=../inputs \
                   --vcmode="gatk:/gatk/gatk.jar" \
                   --sample=PALMAR-AK4772,PALMAR-AK4773,PALMAR-AK4774,PALMAR-AK4775

  >>>

  runtime {

    docker: docker_image
    memory: "5G"
    cpu: "5"
    maxRetries: 0

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()

  }

}
