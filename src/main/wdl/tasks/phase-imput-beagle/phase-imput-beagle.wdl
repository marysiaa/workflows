workflow phase_imput_beagle_workflow
{
  call phase_imput_beagle
}

task phase_imput_beagle {

  File vcf_gz
  String chromosome = "chrX"

  String phasing_bed = "/resources/phasing.bed"
  Int window = 13
  String beagle_path = "/tools/beagle/5.2/beagle.28Jun21.220.jar"
  String sample_id = "no_id_provided"

  String task_name = "phase_imput_beagle"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_phase-imput-beagle:2.0.0-" + chromosome

  command <<<
   #set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir input-vcf/
    #zcat ${vcf_gz} | awk '{gsub(/^chr/,""); print}' | bgzip -c > input-vcf/${sample_id}_input.vcf.gz

    cat ${phasing_bed} | xargs -L 1 -P 16 -i sh -c 'RANGE={} && \
      SHORT_CHR="$(echo $RANGE | cut -d: -f1)" && \
      java -jar ${beagle_path} \
          gt="${vcf_gz}" \
          chrom="chr"$RANGE \
          ap=true \
          gp=true \
          ref="/resources/ref_data/grch38/"$SHORT_CHR".38.vcf.gz" \
          map="/resources/genetic_maps/grch38/plink.chr"$SHORT_CHR".GRCh38.map" \
          window=${window} \
          out="${sample_id}.chr"$RANGE".nochr.phased"'

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: 1
    maxRetries: 2

  }

  output {

    Array[File] phase_imput_vcfs_gz = glob("*phased.vcf.gz")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
