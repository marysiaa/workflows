workflow quality_check_report_workflow { call quality_check_report {}}

task quality_check_report {


  File quality_check_json
  File patient_json

  String task_name = "quality_check_report"
  String task_version = "v0.1"
  String docker_image = "intelliseqngs/reports:v0.2"

  command <<<

  /opt/tools/generate-report.sh --json patient=${patient_json},qc=${quality_check_json} --template /opt/tools/templates/qc-template-v1/content.xml

  >>>

  runtime {

    docker: docker_image
    memory: "500M"
    cpu: "1"

  }

  output {
    File quality_check_report_pdf = "template.pdf"

    # @Output(Required=True,Directory="/quality_check",Name="stdout.log")
    File stdout_log = stdout()
    # @Output(Required=True,Directory="/quality_check",Name="stderr.log")
    File stderr_log = stderr()
    # @Output(Required=True,Directory="/quality_check",Name="bioobject.json")
    #File bioobject = "bioobject.json"

  }

}
