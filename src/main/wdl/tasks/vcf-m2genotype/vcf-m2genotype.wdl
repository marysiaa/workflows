workflow vcf_m2genotype_workflow
{
  call vcf_m2genotype
}

task vcf_m2genotype {

  String task_name = "vcf_m2genotype"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.7"

  File vcf
  File tbi
  Float no_variant_thereshold = 0.1
  Float homozygote_threshold = 0.85
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   bcftools filter -e 'FORMAT/AF < ${no_variant_thereshold}' ${vcf} \
   | bcftools filter -e 'FORMAT/AF >= ${homozygote_threshold}' --set-GTs . \
   | sed 's|\./\.|1/1|' | bgzip > ${sample_id}_genotyped-m2.vcf.gz

   tabix -p vcf  ${sample_id}_genotyped-m2.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File genotyped_vcf = "${sample_id}_genotyped-m2.vcf.gz"
    File genotyped_tbi = "${sample_id}_genotyped-m2.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
