workflow vcf_acmg_pvs1_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "PVS1", "variant", "pathogenic"]}'
    name: 'Vcf ACMG PVS1'
    author: 'https://gitlab.com/gleblavr , https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Evaluates whether variant fulfils ACMG PVS1 criterion, adds annotation'
    changes: '{"2.4.0": "update ClinVar, VEP, ubuntu and pysam", "2.3.0" : "update ClinVar resources, pysam and cython", "2.2.0": "ClinVar update", "2.1.1": "remove  semicolon from loftee descriptions", "2.1.0": "ClinVar updadte", "2.0.1": "script fixed (gene names in two LoF gene situation)", "2.0.0": "uses lotfee for LoF variant evaluation, requires VEP annotation","1.1.3": "Python script reformatted, clinvar resources updated "}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf.gz"], "description": "Input annotated vcf (bgzipped)"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True","type": "File", "description": "Vcf with ACMG PVS1 annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_acmg_pvs1

}

task vcf_acmg_pvs1 {

  String task_name = "vcf_acmg_pvs1"
  String task_version = "2.4.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-pvs1:1.5.0"
  File vcf
  String sample_id = "no-id"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  clinvar_lof_dictionary=$( ls /resources/clinvar/*/clinvar-lof-dictionary.json )
  gnomad_lof_dictionary=$( ls /resources/gnomad/*/gnomad-lof-dictionary.json )
  loftee_descriptions=$( ls /resources/loftee-descriptions/*/loftee-filters.json )

  python3 /intelliseqtools/acmg-pvs1.py \
       --input-vcf ${vcf} \
       --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz \
       --clinvar-lof $clinvar_lof_dictionary \
       --gnomad-lof $gnomad_lof_dictionary \
       --loftee-filters  $loftee_descriptions


  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}

  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
