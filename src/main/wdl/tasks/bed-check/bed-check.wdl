workflow bed_check_workflow
{
  call bed_check
}

task bed_check {

  String task_name = "bed_check"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.7"

  File bed
  String name = basename(basename(bed, ".bed.gz"), ".bed")

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  ## decompress if compressed
  if echo ${bed} | grep -q "\.gz$";then
      zcat ${bed} > temp1.bed
  else
     cp ${bed} temp1.bed
  fi

  ## check chromosome names, rename if without chr, select 1..22 only, sort
  if ! grep -q  -P "^chr[0-9]*\t" temp1.bed;then 
       grep -P "^[0-9]*\t" temp1.bed | sed 's/^/chr/' | sort -k1,1V -k2,2n -k3,3n > temp2.bed 
  else       
      grep -P "^chr[0-9]*\t" temp1.bed | sort -k1,1V -k2,2n -k3,3n > temp2.bed         
  fi

  ## check number of columns (must be 4)
  awk -F '\t' '{print $1"_"$2"_"$3}' temp2.bed > ids
  paste temp2.bed ids | cut -f1-4 > ${name}_cnv.bed
      

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File cnv_bed = "${name}_cnv.bed"

  }

}
