workflow rna_seq_custom_bwa_workflow {

  meta {
    keywords: '{"keywords": ["ensembl dataset", "bwa"]}'
    name: 'rna_seq_custom_bwa'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Preparation of data from ensembl dataset'
    changes: '{"1.0.0": "no changes"}'

    input_ref_genome: '{"name": "Fasta file", "type": "File", "copy": "True", "description": "Fasta file (reference genome)"}'

    output_ref_genome_index: '{"name": "ref_genome_index", "type": "Array[File]", "copy": "True", "description": "Reference genome indexed"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_custom_bwa

}

task rna_seq_custom_bwa {

  File ref_genome
  String genome_basename = "no_basename"

  String task_name = "rna_seq_custom_bwa"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/rna-bwa:1.1.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir genome
    cp ${ref_genome} genome
    gunzip genome/*
    mv genome/*.fa genome/${genome_basename}.fa

    bwa index -a bwtsw genome/${genome_basename}.fa

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    Array[File] ref_genome_index = glob("genome/*")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
