workflow vcf_split_multiallelic_workflow {

  meta {
    keywords: '{"keywords": ["vcf-normalisation", "multiallelic sites", "variants", "bcftools"]}'
    name: 'vcf_split_multiallelic'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Normalize indels mark and split multiallelic sites in vcf, removes low quality multiallelic variants.'
    changes: '{"1.2.1": "new docker", "1.2.0": "added removal of the low quality multiallelic sites and star alts" ,"1.1.0": "latest dir removed, set -eo pipefail added"}'

    input_vcf_basename: '{"name": "vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID."}'
    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file"}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"values": ["broad-institute-hg38", "grch38-no-alt-analysis-set"]}, "description": ""}'
    input_quality_threshold: '{"name": "Quality threshold", "type": "Float", "default": 300, "description": "Threshold QUAL value for the multiallelic variants to be removed as artifacts".}'

    output_normalized_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "vcf file"}'
    output_normalized_vcf_gz_tbi : '{"name": "Vcf file index", "type": "File", "description": "vcf file index"}'
  
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    
  }

  call vcf_split_multiallelic

}

task vcf_split_multiallelic {

  String task_name = "vcf_split_multiallelic"
  String task_version = "1.2.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  
  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename = "no_id_provided"
  Float quality_threshold = 300
  Array[String] reference_genome = ["grch38-no-alt", "GRCh38.no_alt_analysis_set"] # another option: Array[String] reference_genome = ["hg38", "Homo_sapiens_assembly38"]
  String docker_version = "1.2.0"

  # During updating dockerimage to this task remeber to make version for each reference genome and apply here
  String docker_image = "intelliseqngs/task_vcf-split-multiallelic-" + reference_genome[0] + ":" +  docker_version
  # Tools runtime settings, paths etc.
  String flag_multiallelic_sites_py = "/intelliseqtools/flag-multiallelic-sites.py"
  String reference_genome_fasta = "/resources/${reference_genome[1]}.fa"

  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  set -e -o pipefail

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  vcf_dir=$(dirname "${vcf_gz}")
  tbi_name=$(basename "${vcf_gz_tbi}")
  if [ ! -f $vcf_dir/$tbi_name ]; then
    ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
  fi

  bcftools norm  ${vcf_gz} --fasta-ref ${reference_genome_fasta} --multiallelics +any \
   | python3 ${flag_multiallelic_sites_py} \
   | bcftools norm --fasta-ref ${reference_genome_fasta} --multiallelics -any \
   | bcftools filter -e 'ALT[*] ="*"' \
   | bcftools filter -e 'INFO/MULTIALLELIC=1 & QUAL <= ${quality_threshold}' -o ${vcf_basename}_normalized.vcf.gz -O z

  tabix -p vcf ${vcf_basename}_normalized.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2
  }

  output {

    File normalized_vcf_gz = "${vcf_basename}_normalized.vcf.gz"
    File normalized_vcf_gz_tbi = "${vcf_basename}_normalized.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
