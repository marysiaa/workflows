workflow openpgx_workflow
{
  call openpgx
}

task openpgx {

  File genotype_json
  String sample_id = "no_id_provided"

  String task_name = "openpgx"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/openpgx:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    openpgx ${genotype_json}

    /intelliseqtools/openpgx-add-report-info.py --recommendations "recommendations.json" \
                                                --genotypes ${genotype_json} \
                                                --output-filename ${sample_id}-openpgx_recommendations.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File recommendation_json = "${sample_id}-openpgx_recommendations.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
