# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: postprocess_germline_cnv_calls
#  authors:
#    - https://gitlab.com/lltw,
#  copyright: Copyright 2019 Intelliseq
#  description: |
#    WDL implementation of GTAK4 PostprocessGermlineCNVCalls.
#    Overview:
#    Postprocesses the output of GermlineCNVCaller and generates VCFs and denoised copy ratios.
#  changes:
#    latest:
#      - no changes
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow postprocess_germline_cnv_calls_workflow { call postprocess_germline_cnv_calls }

task postprocess_germline_cnv_calls {

# @Input (name = "Sample ID")
  String sample_id

# @Input (name = "Sample index")
  String sample_index

# @Input (name = "Germline contig ploidy calls", desc = "Compressed directory containg contig ploidy calls. This file is an output of 'determine_germline_contig_ploidy_cohort_mode' or 'determine_germline_contig_ploidy_case_mode' task")
  File germline_contig_ploidy_calls_tar_gz

# @Input (name = "GermlineCNVcaller calls", desc = "Compressed directory containg GermlineCNVcaller calls. This file is an output of 'run_germline_cnv_caller_cohort_mode' or 'run_germline_cnv_caller_case_mode' task")
  Array[File] germline_cnv_caller_calls_tar_gz

# @Input (name = "GermlineCNVcaller model", desc = "Compressed directory containg GermlineCNVcaller calls model. This file is an output of 'run_germline_cnv_caller_cohort_mode' task")
  Array[File] germline_cnv_caller_model_tar_gz

  # Docker image
  String docker_image = "broadinstitute/gatk:4.1.3.0"

  # Tools runtime settings, paths etc.
  String gatk_path = "/gatk/gatk"
  String java_opt = "-Xms16000m"

  # Task name and task version
  String task_name = "postprocess_germline_cnv_calls"
  String task_version = "latest"

  command <<<

    ### start time
    starttime=$(date +%s)

    # unpack Germline contig ploidy calls
    mkdir germline-contig-ploidy-calls
    tar -xzf ${germline_contig_ploidy_calls_tar_gz} -C germline-contig-ploidy-calls --strip-components=1

    # unpack GermlineCNVcaller calls
    # create command
    INDEX=0
    CALLS_SHARD_PATH_COMMAND=""
    for tar_gz_archive in ${sep=' ' germline_cnv_caller_calls_tar_gz}; do
      mkdir germline-cnv-caller-calls-"$INDEX"
      tar -xzf $tar_gz_archive -C germline-cnv-caller-calls-"$INDEX" --strip-components=1
      CALLS_SHARD_PATH_COMMAND=$CALLS_SHARD_PATH_COMMAND"--calls-shard-path "germline-cnv-caller-calls-"$INDEX"" "
      INDEX=$((INDEX + 1))
    done

    # unpack GermlineCNVcaller model
    # create command
    INDEX=0
    MODEL_SHARD_PATH_COMMAND=""
    for tar_gz_archive in ${sep=' ' germline_cnv_caller_model_tar_gz}; do
      mkdir germline-cnv-caller-model-"$INDEX"
      tar -xzf $tar_gz_archive -C germline-cnv-caller-model-"$INDEX" --strip-components=1
      MODEL_SHARD_PATH_COMMAND=$MODEL_SHARD_PATH_COMMAND"--model-shard-path "germline-cnv-caller-model-"$INDEX"" "
      INDEX=$((INDEX + 1))
    done

    # run PostprocessGermlineCNVCalls
    ${gatk_path} --java-options ${java_opt} \
      PostprocessGermlineCNVCalls \
      --contig-ploidy-calls germline-contig-ploidy-calls \
      $CALLS_SHARD_PATH_COMMAND \
      $MODEL_SHARD_PATH_COMMAND \
      --sample-index ${sample_index} \
      --autosomal-ref-copy-number 2 \
      --allosomal-contig chrX \
      --allosomal-contig chrY \
      --output-genotyped-intervals ${sample_id}.genotyped-intervals.vcf \
      --output-genotyped-segments ${sample_id}.genotyped-segments.vcf \
      --output-denoised-copy-ratios ${sample_id}.denoised-copy-ratios.tsv

    bgzip ${sample_id}.genotyped-intervals.vcf
    bgzip ${sample_id}.genotyped-segments.vcf

    tabix -p vcf ${sample_id}.genotyped-intervals.vcf.gz
    tabix -p vcf ${sample_id}.genotyped-segments.vcf.gz

    ### bioobject
    if [ -e "/resources.bioobject.json" ]; then RESOURCES=$(cat /resources.bioobject.json); else RESOURCES="[]"; fi
    if [ -e "/tools.bioobject.json" ]; then TOOLS=$(cat /tools.bioobject.json); else TOOLS="[]"; fi
    CPU=$(lscpu | grep '^CPU(s)' | grep -o '[0-9]*')
    MEMORY=$(cat /proc/meminfo | grep MemTotal | grep -o '[0-9]*' |  awk '{ print $1/1024/1024 ; exit}')
    finishtime=$(date +%s)
    printf TASKTIME=$((finishtime-starttime))


    printf "{\
      \"task-name\":\"${task_name}\",\
      \"task-version\":\"${task_version}\",\
      \"docker-image\":\"${docker_image}\",\
      \"cpu\":\"$CPU\",\
      \"memory\":\"$MEMORY\",\
      \"task-processing-time\":\"$TASKTIME\",\
      \"resources\":$RESOURCES,\
      \"tools\":$TOOLS\
      }" | sed 's/ //g' > bioobject.json

  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File genotyped_intervals_vcf_gz = "${sample_id}.genotyped-intervals.vcf.gz"
    File genotyped_intervals_vcf_gz_tbi = "${sample_id}.genotyped-intervals.vcf.gz.tbi"

    File genotyped_segments_vcf_gz =  "${sample_id}.genotyped-segments.vcf.gz"
    File genotyped_segments_vcf_gz_tbi =  "${sample_id}.genotyped-segments.vcf.gz.tbi"

    File denoised_copy_ratios_tsv = "${sample_id}.denoised-copy-ratios.tsv"

    # @Output(required=true,directory="/postprocess_germline_cnv_calls",filename="stdout.log")
    File stdout_log = stdout()
    # @Output(required=true,directory="/postprocess_germline_cnv_calls",filename="stderr.log")
    File stderr_log = stderr()
    # @Output(required=true,directory="/postprocess_germline_cnv_calls",filename="bioobject.json")
    File bioobject = "bioobject.json"

  }

}
