workflow resources_pharmcat_workflow {

  meta {
    keywords: '{"keywords": ["resources", "pharmcat"]}'
    name: 'resources_pharmcat'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Outputs pharmcat vcf.gz'
    changes: '{"1.1.0": "update docker, create bed in task", 1.0.2": "add pharmcat_bed_gz to docker and output", "1.0.1": "add pharmcat_vcf_gz_tbi to output"}'

    output_pharmcat_vcf_gz: '{"name": "Pharmcat gzipped vcf", "type": "File", "copy": "True", "description": "Pharmcat gzipped vcf"}'
    output_pharmcat_bed_gz: '{"name": "Pharmcat gzipped bed", "type": "File", "copy": "True", "description": "Pharmcat gzipped bed"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call resources_pharmcat

}

task resources_pharmcat {

  String task_name = "resources_pharmcat"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/pgx-pharmcat:2.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   cp /resources/pgx.vcf.gz pharmcat.vcf.gz
   cp /resources/pgx.vcf.gz.tbi pharmcat.vcf.gz.tbi

   # Create bed
   zcat pharmcat.vcf.gz | awk '{print $1"\t"($2 - 1)"\t"$2}' | sort -k1,1d -k2,2n | grep -v "^#" | bgzip > pharmcat.bed.gz
   tabix -p bed pharmcat.bed.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File pharmcat_vcf_gz = "pharmcat.vcf.gz"
    File pharmcat_vcf_gz_tbi = "pharmcat.vcf.gz.tbi"

    File pharmcat_bed_gz = "pharmcat.bed.gz"
    File pharmcat_bed_gz_tbi = "pharmcat.bed.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
