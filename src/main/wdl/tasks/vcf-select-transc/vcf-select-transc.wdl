workflow vcf_select_transc_workflow {

  call vcf_select_transc

}

task vcf_select_transc {

  String task_name = "vcf_select_transc"
  String task_version = "1.0.1"
  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no_id_provided"
  String? chr
  String prefix = if defined(chr) then chr + "-" + sample_id else sample_id
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-select-transc:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   python3 /intelliseqtools/select_report_transcript.py \
      --input-vcf ${vcf_gz} \
      --output-vcf ${prefix}_annotated.vcf.gz

   tabix -p vcf ${prefix}_annotated.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File output_vcf_gz = "${prefix}_annotated.vcf.gz"
    File output_vcf_gz_tbi = "${prefix}_annotated.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
