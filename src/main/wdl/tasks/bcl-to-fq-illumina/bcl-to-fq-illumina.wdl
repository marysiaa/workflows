workflow bcl_to_fq_illumina_workflow
{
  call bcl_to_fq_illumina
}

task bcl_to_fq_illumina {

  File raw_data_tar_gz


  String task_name = "bcl_to_fq_illumina"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/bcl-to-fq-illumina:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      mkdir raw-data results
      tar --directory=raw-data -xvzf  ${raw_data_tar_gz}
      DIR="$(find raw-data -mindepth 1 -maxdepth 1 -type d)"

      /tools/bcl2fastq -o results -R $DIR --no-lane-splitting

      # extract QC metrics from RunCompletionStatus.xml
      RUN_STATUS="$(find -maxdepth 3 -type f -name "RunCompletionStatus.xml")"
      CD="$(cat $RUN_STATUS | grep "ClusterDensity" | sed 's/[^0-9.]//g')"
      CPF="$(cat $RUN_STATUS | grep "ClustersPassingFilter" | sed 's/[^0-9.]//g')"
      jq -n --arg CD "$CD" --arg CPF "$CPF" \
      '{"BCL_RunCompletionStatus": {ClusterDensity: $CD, ClustersPassingFilter: $CPF}}' > bcl_stats.json

      NIPT_DATA="$(find -maxdepth 3 -type f -name "*_NIPT_RESULTS.csv")"
      cp $NIPT_DATA .


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File bcl_stats = "bcl_stats.json"
    Array[File] fastqs = glob("results/*.fastq.gz")

    File illumina_csv = glob("*_NIPT_RESULTS.csv")[0]

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
