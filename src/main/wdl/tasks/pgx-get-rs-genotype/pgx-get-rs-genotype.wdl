workflow pgx_get_rs_genotype_workflow
{
  call pgx_get_rs_genotype
}

task pgx_get_rs_genotype {

  File vcf

  String task_name = "pgx_get_rs_genotype"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-get-rs-genotype:1.0.2"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # Query vcf for rsid and genotype
    bcftools query -f '%ISEQ_DBSNP_RS\t[%TGT]\n' ${vcf}> rs_from_vcf.tsv

    # add header to tsv
    echo -e "RS\tGT" | cat - rs_from_vcf.tsv > rs_vcf.tsv

    # Create PharmGKB query file
    /intelliseqtools/pgx-get-rs-genotype.py --rs-from-vcf "rs_vcf.tsv" \
                                            --rs-to-filter "/resources/rs_Ummar.tsv" \
                                            --output-filename "pharmgkb_rs_queries.json"

    # Create openpgx query file
    /intelliseqtools/pgx-get-rs-genotype.py --rs-from-vcf "rs_vcf.tsv" \
                                            --rs-to-filter "/resources/genes_encoding.tsv" \
                                            --query-for-openpgx \
                                            --output-filename "openpgx_rs_queries.json"

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File pharmgkb_rs_queries = "pharmgkb_rs_queries.json"
    File openpgx_rs_queries = "openpgx_rs_queries.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
