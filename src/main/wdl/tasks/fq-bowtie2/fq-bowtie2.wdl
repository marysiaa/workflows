workflow fq_bowtie2_workflow
{
  call fq_bowtie2
}

task fq_bowtie2 {

  File? fastq_single_end

  File? fastq_paired_1
  File? fastq_paired_2

  Int threads = 4
  String sample_id = 'no_id_provided'
  Boolean run_bowtie1 = false

  String task_name = "fq_bowtie2"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = if run_bowtie1 then "intelliseqngs/task_fq-bowtie:1.0.0" else "intelliseqngs/task_fq-bowtie2:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    if ${run_bowtie1}; then
        bowtie -S --threads ${threads} /resources/GRCh38.no_alt_analysis_set ${fastq_single_end} ${"-1 " + fastq_paired_1} ${"-2 " + fastq_paired_2} > ${sample_id}.sam 2> ${sample_id}.stderr
    else
        bowtie2 --threads ${threads} -x /resources/GRCh38.no_alt_analysis_set ${"-U " + fastq_single_end} ${"-1 " + fastq_paired_1} ${"-2 " + fastq_paired_2} -S ${sample_id}.sam 2> ${sample_id}.stderr
    fi

    cat ${sample_id}.stderr >&2

    samtools view -S -b ${sample_id}.sam > ${sample_id}.bam


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: threads
    maxRetries: 2

  }

  output {

    File bam = "${sample_id}.bam"
    File bowtie_stderr = "${sample_id}.stderr"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
