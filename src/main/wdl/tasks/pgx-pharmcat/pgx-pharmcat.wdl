workflow pgx_pharmcat_workflow {

  meta {
    keywords: '{"keywords": []}'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    changes: '{"1.5.0": "update pharmcat","1.4.4": "cnv_tsv optional"}'
    tag: 'Clinical WES/WGS'

    name: 'pgx_pharmcat'
    description: 'Identifies drug-related variants (according to the CPIC guidelines); generates clinical report which can be used to inform treatment decisions; for whole genome sequencing data (WGS).'

    input_sample_id: '{"index": 1, "name": "Sample id", "type": "String", "description": "Enter a sample name (or identifier)"}'
    input_sample_vcf_gz: '{"index": 2, "name": "VCF file", "type": "File", "extension": [".vcf.gz"], "description": "Select vcf file with variants"}'
    input_cnv_tsv: '{"name": "CNV tsv", "type": "File", "description": "tsv text file with genotyping data from another pgx tool, specialised for Copy Number Variants (CNV)", "extension": [".tsv"]}'
    
    output_pgx_report_html: '{"name": "PGx report", "type": "File", "description": "Pharmacogenomics report in html format"}'
    output_pgx_report_json: '{"name": "PGx report", "type": "File", "description": "Data form pharmacogenomics report in json format"}'
   
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    }

  call pgx_pharmcat

}

task pgx_pharmcat {

  String task_name = "pgx_pharmcat"
  String task_version = "1.5.1"
  Int? index = 1 
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/pgx-pharmcat:2.0.0"

  String sample_id = "no_id_provided"
  File sample_vcf_gz
  File? cnv_tsv
  Boolean is_cnv_tsv_defined = defined(cnv_tsv)
  

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  #tabix -p vcf /resources/pgx.vcf.gz
  # remove "inf" value in QUAl column

  zcat ${sample_vcf_gz} | awk -v OFS='\t' ' ( $6 == "inf" || $6 == "Infinity") { $6 = "1000" }1' > no_inf_${sample_id}.vcf

  if  ${is_cnv_tsv_defined}; then
     java -jar /tools/pharmcat/1.5.1/pharmcat.jar -vcf no_inf_${sample_id}.vcf -o sampledir -j pharmcat.json -k -f ${sample_id}_pgx -a ${cnv_tsv}
  else
     java -jar /tools/pharmcat/1.5.1/pharmcat.jar -vcf no_inf_${sample_id}.vcf -o sampledir -j pharmcat.json -k -f ${sample_id}_pgx
  fi



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File pgx_report_html = "sampledir/${sample_id}_pgx.report.html"
    File pgx_report_json = "sampledir/${sample_id}_pgx.report.json"
    File pgx_matcher_html = "sampledir/${sample_id}_pgx.matcher.html"
    File pgx_matcher_json = "sampledir/${sample_id}_pgx.matcher.json"
    
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
