workflow rna_seq_count_workflow {

  meta {

    keywords: '{"keywords": ["count reads"]}'
    name: 'rna_seq_count'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Count reads with htseq-count'
    changes: '{"1.0.3": "updating to a new template"}'

    input_gtf_file: '{"name": "gtf_file", "type": "File", "description": "gtf file"}'
    input_bam_file: '{"name": "bam_file", "type": "File", "description": "bam file"}'
    input_bam_bai_file: '{"name": "bam_bai_file", "type": "File", "description": "index for bam file"}'

    output_count_reads: '{"name": "count_reads", "type": "File", "copy": "True", "description": "htseq-count output"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_count

}

task rna_seq_count {

  File bam_file
  File bam_bai_file
  File gtf_file

  String sample_id = "no_id_provided"

  String task_name = "rna_seq_count"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/htseq-count:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    python3 -m HTSeq.scripts.count --nonunique all -t gene -i gene_id --additional-attr=Alias --additional-attr=Name ${bam_file} ${gtf_file} > ${sample_id}.tsv

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File count_reads = "${sample_id}.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
