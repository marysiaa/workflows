workflow interval_list_to_bed_workflow
{
  call interval_list_to_bed
}

task interval_list_to_bed {

  Array[File] calling_intervals
  String dollar = "$"

  String task_name = "interval_list_to_bed"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.4.1:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    for interval in ${sep=' ' calling_intervals}
    do
      filename="$(basename $interval)"
      filename="${dollar}{filename%.*}"
      gatk IntervalListToBed \
        -I $interval \
        -O $filename.bed
    done

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
    Array[File] converted_beds = glob("*.bed")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
