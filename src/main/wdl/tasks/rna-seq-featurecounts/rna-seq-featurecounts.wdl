workflow rna_seq_featurecounts_workflow
{
  call rna_seq_featurecounts
}

task rna_seq_featurecounts {

  Array[File] bams
  Array[File] bams_bais
  File genome
  File ensembl_gtf
  Boolean is_paired_end = true
  Int num_threads = 4
  String sample_id = "no_id_provided"

  String stranded = "unstranded" # Possible values include: 0 (unstranded), 1 (stranded) and 2 (reversely stranded)
  Int stranded_to_int = if stranded == "unstranded" then 0 else if stranded == "stranded" then 1 else 2

  String task_name = "rna_seq_featurecounts"
  String task_version = "2.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/featurecount:1.1.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if [ "${is_paired_end}" = true ] ; then
      featureCounts -p -O -B -C -T ${num_threads} -G ${genome} -t exon -g gene_id -s ${stranded_to_int} -a ${ensembl_gtf} -o ${sample_id}_gene-level-counts.tsv ${sep=" " bams}
  else
      featureCounts -O -T ${num_threads} -G ${genome} -t exon -g gene_id -s ${stranded_to_int} -a ${ensembl_gtf} -o ${sample_id}_gene-level-counts.tsv ${sep=" " bams}
  fi

  ### python code for renaming columns
    python3 /intelliseqtools/rna-seq-featurecounts.py --input-tsv "${sample_id}_gene-level-counts.tsv" \
                                                      --output-tsv "${sample_id}_gene-level-counts.tsv"




   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File sample_tbl_featurecount_gene_level = "${sample_id}_gene-level-counts.tsv"
    File gene_level_summary = "${sample_id}_gene-level-counts.tsv.summary"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
