workflow qc_fq_fastqc_workflow {

  meta {
    keywords: '{"keywords": ["fastqc", "quality check"]}'
    name: 'FastQC'
    author: 'https://gitlab.com/mremre'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Runs FastQC and generates quality statistics and plots.'
    changes: '{"1.0.3": "one fastq per iteration"}'
    input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Identifier of sample"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    output_quality_check_json: '{"name": "Quality check for fastq", "type": "File", "copy": "True", "description": "Json with fastqc report with additional statictics"}'
    output_quality_check_png: '{"name": "Quality of reads picture for fastq", "type": "File", "copy": "True", "description": "Per base quality picture for fastq"}'
  }

  call qc_fq_fastqc

}

task qc_fq_fastqc {
  File fastq
  String fastq_basename = basename(fastq)
  File? error_warning_json_qc

  String task_name = "qc_fq_fastqc"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String task_version = "1.0.3"
  String docker_image = "intelliseqngs/fastqc:1.0.0"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  #fastqc analysis
  mkdir fastqc-${index}

  fastqc -o fastqc-${index} ${fastq}

  unzip fastqc-${index}/\*.zip -d unzipped_${index}

  #ls /intelliseqtools > ls.txt
  #cat /intelliseqtools/add-error-warning-json.py > warni.txt
  #cat /intelliseqtools/quality-check.py > qc.txt

  #fastqc_data
  python3 /intelliseqtools/quality-check.py unzipped_${index}/*/fastqc_data.txt ${fastq_basename}.json
  python3 /intelliseqtools/add-error-warning-json.py \
  ${default="/intelliseqtools/fastq-quality-check-errors-template.json" error_warning_json_qc} \
  ${fastq_basename}.json \
  > ${fastq_basename}-quality-check.json
  mv unzipped_${index}/*/Images/per_base_quality.png ${fastq_basename}-qc.png

###
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "500M"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File fastqc_zip = glob("fastqc-${index}/*.zip")[0]

    File quality_check_json = "${fastq_basename}-quality-check.json"
    File quality_check_png = "${fastq_basename}-qc.png"

    String basename_out = "${fastq_basename}"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
