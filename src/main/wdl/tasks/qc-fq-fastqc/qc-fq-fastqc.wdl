workflow qc_fq_fastqc_workflow {

  meta {
    keywords: '{"keywords": ["fastqc", "quality check"]}'
    name: 'FastQC'
    author: 'https://gitlab.com/mremre'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Runs FastQC and generates quality statistics and plots.'
    changes: '{"2.0.1": "add optional sample_name input", "2.0.0": "Remove report inputs", "1.2.0": "report inputs optional", "1.1.0" : "latest removed", "1.0.3": "one fastq per iteration"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call qc_fq_fastqc

}

task qc_fq_fastqc {

  File fastq
  String? sample_name
  Boolean defined_sample_name = defined(sample_name)

  String task_name = "qc_fq_fastqc"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String task_version = "2.0.1"
  String docker_image = "intelliseqngs/fastqc:1.2.1"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  mkdir fastqc

  if ${defined_sample_name}; then
    ln -s ${fastq} ${sample_name}.fq.gz
    fastqc -o fastqc ${sample_name}.fq.gz
  else
    fastqc -o fastqc ${fastq}
  fi

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "500M"
    cpu: "1"
    maxRetries: 2

  }

  output {

    Array[File] fastqc_zip = glob("fastqc/*.zip")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
