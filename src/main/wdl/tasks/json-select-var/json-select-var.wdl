workflow json_select_var_workflow
{
  call json_select_var
}

task json_select_var {

  File variants_json
  String variants_list
  String sample_id = "no_id_provided"

  String task_name = "json_select_var"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_json-select-var:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    python3 /intelliseqtools/json_select_var.py \
      --variants-json "${variants_json}" \
      --variants-list "${variants_list}" \
      --output-filename "${sample_id}_selected.json"

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File selected_json = "${sample_id}_selected.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
