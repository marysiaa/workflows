workflow vcf_m2filter_workflow
{
  call vcf_m2filter
}

task vcf_m2filter {

  String task_name = "vcf_m2filter"
  String task_version = "1.0.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.4.1-hg38:1.0.0"

  String sample_id = "no_id_provided"

  File m2_vcf
  File m2_vcf_tbi
  Array[String] bad_filters = ["FAIL", "base_qual", "clustered_events", "contamination", "duplicate", "fragment", "low_allele_frac", "map_qual", "multiallelic", "n_ratio", "orientation", "panel_of_normals", "position", "possible_numt", "slippage", "strand_bias", "strict_strand", "weak_evidence"]

  ## stats files produced by mutect2, merged with MergeMutectStats (m2-stats-merge task)
  File? m2_stats

  ## Calculated with the CalculateContamination (pileup-cals-contam task); based on merged tumor (and optionally normal) pileups
  File? contamination_table

  ## Calculated with LearnReadOrientationModel (rom-for-m2filter task); based on Mutect2 f1r2 tars
  File? artifact_priors_tar_gz

  ## Calculated with the CalculateContamination (pileup-cals-contam task); based on merged tumor (and optionally normal) pileups
  File? maf_segments

  ## For more information see:
  ## https://gatk.broadinstitute.org/hc/en-us/articles/360051305471-FilterMutectCalls and
  ## https://github.com/broadinstitute/gatk/blob/master/docs/mutect/mutect.pdf
  ## --max-alt-allele-count 1; --max-events-in-region 2; --min-median-base-quality 30;
  ## --min-median-mapping-quality 30; --f-score-beta 1; --minimal-alt-read-count 1; --min-allele-fraction 0.0;
  ## --min-reads-per-strand false; --distance-on-haplotype 100;
  String? m2_extra_filtering_args = "--max-alt-allele-count 2 --max-events-in-region 3 --unique-alt-read-count 2 --min-allele-fraction 0.02 --min-reads-per-strand 1"
  Float indel_threshold = 0.0
  Boolean for_somatic_pipeline = true

  String java_mem = "-Xmx3500m"
  String ref = "`ls /resources/reference-genomes/*/*.fa`"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  gatk --java-options "${java_mem}" \
      FilterMutectCalls \
      -V ${m2_vcf} \
      -R ${ref} \
      -O ${sample_id}_m2.vcf.gz  \
      ${"--contamination-table " + contamination_table} \
      ${"--tumor-segmentation " + maf_segments} \
      ${"--ob-priors " + artifact_priors_tar_gz} \
      ${"-stats " + m2_stats} \
      --filtering-stats ${sample_id}_m2-filtering.stats \
      ${m2_extra_filtering_args}


  # Hard filter and correct invalid info description
  if ${for_somatic_pipeline};then
    bcftools filter -e 'FILTER~"${sep="\" | FILTER~\"" bad_filters}"' ${sample_id}_m2.vcf.gz \
    | sed 's/INFO=<ID=AS_SB_TABLE,Number=1/INFO=<ID=AS_SB_TABLE,Number=./' | bgzip > ${sample_id}_filtered-m2.vcf.gz

     bcftools filter -i 'FILTER~"${sep="\" | FILTER~\"" bad_filters}"' ${sample_id}_m2.vcf.gz \
    | sed 's/INFO=<ID=AS_SB_TABLE,Number=1/INFO=<ID=AS_SB_TABLE,Number=./' | bgzip > ${sample_id}_rejected-m2.vcf.gz

  else  

    bcftools filter -i 'FILTER="PASS" | (FILTER~"germline" & INFO/AS_FilterStatus="SITE" & FILTER!~"normal_artifact" & FILTER!~"panel_of_normals")' ${sample_id}_m2.vcf.gz \
    | bcftools filter -e 'TYPE="indel" & FORMAT/AF[0:0] < ${indel_threshold}' \
    | sed 's/INFO=<ID=AS_SB_TABLE,Number=1/INFO=<ID=AS_SB_TABLE,Number=./' | bgzip > ${sample_id}_filtered-m2.vcf.gz

    bcftools filter -e 'FILTER="PASS" | (FILTER~"germline" & INFO/AS_FilterStatus="SITE" & FILTER!~"normal_artifact" & FILTER!~"panel_of_normals")' ${sample_id}_m2.vcf.gz \
    | sed 's/INFO=<ID=AS_SB_TABLE,Number=1/INFO=<ID=AS_SB_TABLE,Number=./' | bgzip  > tmp1_rejected-m2.vcf.gz
    
    tabix -p vcf tmp1_rejected-m2.vcf.gz

    bcftools filter -i 'FILTER="PASS" | (FILTER~"germline" & INFO/AS_FilterStatus="SITE" & FILTER!~"normal_artifact" & FILTER!~"panel_of_normals")' ${sample_id}_m2.vcf.gz \
    | bcftools filter -i 'TYPE="indel" & FORMAT/AF[0:0] < ${indel_threshold}' \
    | sed 's/INFO=<ID=AS_SB_TABLE,Number=1/INFO=<ID=AS_SB_TABLE,Number=./' | bgzip > tmp2_rejected-m2.vcf.gz
    
    tabix -p vcf tmp2_rejected-m2.vcf.gz

    bcftools concat -a tmp1_rejected-m2.vcf.gz tmp2_rejected-m2.vcf.gz -o ${sample_id}_rejected-m2.vcf.gz -O z
  fi
  
  tabix -p vcf ${sample_id}_filtered-m2.vcf.gz
  tabix -p vcf ${sample_id}_rejected-m2.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File mutect2_vcf = "${sample_id}_m2.vcf.gz"
    File mutect2_vcf_tbi = "${sample_id}_m2.vcf.gz.tbi"

    File filtered_m2_vcf = "${sample_id}_filtered-m2.vcf.gz"
    File filtered_m2_vcf_tbi = "${sample_id}_filtered-m2.vcf.gz.tbi"

    File rejected_m2_vcf = "${sample_id}_rejected-m2.vcf.gz"
    File rejected_m2_vcf_tbi = "${sample_id}_rejected-m2.vcf.gz.tbi"

    File filtering_stats = "${sample_id}_m2-filtering.stats"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
