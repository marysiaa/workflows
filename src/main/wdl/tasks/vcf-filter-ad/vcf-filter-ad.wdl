workflow vcf_filter_ad_workflow {

  meta {
    keywords: '{"keywords": ["VCF", "filter", "allele depth"]}'
    name: 'VCF allele depth filter'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Filters heterozygous variants with unbalanced allele depths'
    changes: '{"1.0.1": "output vcf name change"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "Vcf gz", "type": "File", "description": "Vcf file (bgzipped)"}'
    input_vcf_gz_tbi: '{"name": "Vcf gz tbi", "type": "File", "description": "Vcf file index"}'
    input_ad_binom_threshold: '{"name": "AD binomal threshold", "type": "Float", "description": "Threshold probability for the AD binomal test"}'

    output_ad_marked_vcf_gz: '{"name": "AD marked vcf gz", "type": "File", "copy": "True", "description": "VCF file with marked sites that failed the binomal AD test (bgzipped)"}'
    output_ad_marked_vcf_gz_tbi: '{"name": "AD marked vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the AD marked vcf file"}'
    output_filtered_vcf_gz: '{"name": "Filtered vcf gz", "type": "File", "copy": "True", "description": "Binomal AD filtered vcf (bgzipped)"}'
    output_filtered_vcf_gz_tbi: '{"name": "Filtered vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the AD filtered vcf file"}'
    output_rejected_vcf_gz: '{"name": "Rejected vcf gz", "type": "File", "copy": "True", "description": "VCF file with variants that failed the binomal AD test (bgzipped)"}'
    output_rejected_vcf_gz_tbi: '{"name": "Rejected vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the rejected vcf file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_filter_ad

}

task vcf_filter_ad {

  String task_name = "vcf_filter_ad"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.4"

  File vcf_gz
  File vcf_gz_tbi
  Float ad_binom_threshold = 0.01
  String sample_id = "no-id"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   bcftools filter -e '(GT="het") & (binom(FORMAT/AD) < ${ad_binom_threshold}) & (FORMAT/AD[0:0] - FORMAT/AD[0:1] > 0)' -s "binomalAD" ${vcf_gz} -O v -o tmp.vcf
   sed 's/\\"het\\"/het/' tmp.vcf > ${sample_id}_ad-marked.vcf


   awk '/^#/||$7=="PASS"' ${sample_id}_ad-marked.vcf | bgzip >  ${sample_id}.vcf.gz
   awk '/^#/||$7!="PASS"' ${sample_id}_ad-marked.vcf | bgzip > ${sample_id}_ad-rejected.vcf.gz

   bgzip ${sample_id}_ad-marked.vcf
   tabix -p vcf ${sample_id}_ad-marked.vcf.gz
   tabix -p vcf ${sample_id}.vcf.gz
   tabix -p vcf ${sample_id}_ad-rejected.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File ad_marked_vcf_gz = "${sample_id}_ad-marked.vcf.gz"
    File ad_marked_vcf_gz_tbi = "${sample_id}_ad-marked.vcf.gz.tbi"

    File filtered_vcf_gz = "${sample_id}.vcf.gz"
    File filtered_vcf_gz_tbi = "${sample_id}.vcf.gz.tbi"

    File rejected_vcf_gz = "${sample_id}_ad-rejected.vcf.gz"
    File rejected_vcf_gz_tbi = "${sample_id}_ad-rejected.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
