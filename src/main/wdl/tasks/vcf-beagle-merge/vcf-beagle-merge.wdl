workflow vcf_beagle_merge_workflow {


  call vcf_beagle_merge

}

task vcf_beagle_merge {

  String task_name = "vcf_beagle_merge"
  String task_version = "1.0.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.5"

  File gtc_vcf
  File gtc_vcf_tbi
  File beagle_vcf
  File beagle_vcf_tbi
  Boolean gtc_sites_only = true
  String bcftools_sites_argument = if gtc_sites_only then "-R split-vcf-from-gtc-all/{}" else ""  
  String sample_id = "no_id_provided"
  String ref_path = "/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   ## Split both vcfs to create per sample vcfs:
   mkdir split-vcf-from-gtc-all
   bcftools +split ${gtc_vcf} -o split-vcf-from-gtc-all/ -O z
   ls split-vcf-from-gtc-all/*.vcf.gz |  xargs -i -n 1 -P 4 bash -c "tabix -p vcf -f {}"  

   mkdir split-vcf-from-gtc
   bcftools +split -e 'GT~"\."' ${gtc_vcf} -o split-vcf-from-gtc/ -O z
   ls split-vcf-from-gtc/*.vcf.gz |  xargs -i -n 1 -P 4 bash -c "tabix -p vcf -f {}"

   mkdir split-vcf-wo-genotypes
   bcftools +split -i 'GT~"\."' ${gtc_vcf} -o split-vcf-wo-genotypes/ -O z
   ls split-vcf-wo-genotypes/*.vcf.gz |  xargs -i -n 1 -P 4 bash -c "tabix -p vcf -f {}"

   mkdir split-vcf-from-imputing
   bcftools +split -e 'GT~"\."' ${beagle_vcf} -o split-vcf-from-imputing/ -O z
   ls split-vcf-from-imputing/*.vcf.gz |  xargs -i -n 1 -P 4 bash -c "tabix -p vcf -f {}"


   ## Concatenate per-sample vcfs and remove duplicated lines:
   ## Note that the order of the vcf files must be 1) genotyped from gtc 2) imputed 3) non-genotyped from gtc
   ## Only bcftools concat -a works this way (gatk MergeVcfs changes order of input files)
   mkdir concatenated-vcfs
   find split-vcf-from-gtc/ -name "*.vcf.gz" -exec basename \{} \; \
   | xargs -i -n 1 -P 4 bash -c \
   "bcftools concat -a split-vcf-from-gtc/{} ${bcftools_sites_argument} split-vcf-from-imputing/{} split-vcf-wo-genotypes/{} \
   | bcftools norm -d none -o concatenated-vcfs/{} -O z"
   ls concatenated-vcfs/*.vcf.gz |  xargs -i -n 1 -P 4 bash -c "tabix -p vcf -f {}"

   mkdir concatenated-phased-vcfs
   find split-vcf-from-gtc/ -name "*.vcf.gz" -exec basename \{} \; \
   | xargs -i -n 1 -P 4 bash -c \
   "bcftools annotate -a split-vcf-from-imputing/{} -c ID,FORMAT/GT concatenated-vcfs/{} -o concatenated-phased-vcfs/{} -O z"
   ls concatenated-phased-vcfs/*.vcf.gz |  xargs -i -n 1 -P 4 bash -c "tabix -p vcf -f {}"

   ## Final merge: per sample vcfs into one final multi-sample vcf
   if [[ `ls concatenated-phased-vcfs/*.vcf.gz | wc -l` -gt 1 ]];then
       bcftools merge `ls concatenated-phased-vcfs/*.vcf.gz | tr '\n' ' '` -m  none -o ${sample_id}_merged-imputed.vcf.gz -O z
   else
      cp `ls concatenated-phased-vcfs/*.vcf.gz` ${sample_id}_merged-imputed.vcf.gz
   fi
   tabix -p vcf ${sample_id}_merged-imputed.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "12G"
    cpu: 4
    maxRetries: 2

  }

  output {

    File merged_vcf = "${sample_id}_merged-imputed.vcf.gz"
    File merged_vcf_tbi = "${sample_id}_merged-imputed.vcf.gz.tbi"
    Array[File] vcfs_gz = glob("concatenated-phased-vcfs/*.vcf.gz")    
    Array[File] vcfs_gz_tbi = glob("concatenated-phased-vcfs/*.vcf.gz.tbi")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
