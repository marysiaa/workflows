workflow report_acmg_workflow
{
  call report_acmg
}

task report_acmg {

  File? var_json
  File? cnv_json
  File? sample_info_json
  File? pictures_gz
  File? pictures_cnv_gz
  File? panel_json
  File? panel_inputs_json
  File? genes_bed_target_json
  File? gene_panel_logs

  Int timezoneDifference = 0

  String output_name = "main"
  String sample_id = "no_id_provided"
  String genome_or_exome = "exome"
  String? kit
  String var_call_tool = "haplotypeCaller"
  String aligner_tool = "bwa-mem"
  Boolean germline_analysis = true

  # sv calling inputs to content
  Boolean sv_calling_module = false     
  Float min_qual = 30
  Float del_cov_max = 0.75
  Float dup_cov_min = 1.3
  Float het_max = 0.25
  Float bf_threshold = 0

  String analysis_start = "fastq"
  String analysis_group = "germline" # possible: "carrier_screening" and "germline"
  String? analysisDescription
  String? analysisName
  String? analysisWorkflow
  String? analysisWorkflowVersion
  String analysisEnvironment = "IntelliseqFlow"
  Float threshold_frequency = 0.05
  String impact_threshold = "MODERATE"
  Float quality_threshold = 0
  String outputs = "outputs"
  String template = if analysis_group == "germline" then "acmg" else "carrier-screening"

  String task_name = "report_acmg"

  String task_version = "2.4.17"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_acmg:1.1.49"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ${outputs}

    # Prepare sample-info.json
    bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                      --timezoneDifference ${timezoneDifference} \
                                                      --sampleID "${sample_id}"

    # Prepare picture.json with paths to png files
    mkdir pictures
    bash /intelliseqtools/prepare-pictures-json.sh  --pictures-gz "${pictures_gz}" \
                                                    --directory "pictures" \
                                                    --filename "picture-generate-report-sh.json"

    # Prepare picture-cnv.json with paths to png files
    mkdir pictures-cnv
    bash /intelliseqtools/prepare-pictures-json.sh  --pictures-gz "${pictures_cnv_gz}" \
                                                    --directory "pictures-cnv" \
                                                    --filename "picture-generate-report-cnv-sh.json"    


    # change input variants filename to variants.json
    if [ -z "${var_json}" ] ; then
        echo '[]' > variants.json
    else
        cat ${var_json} > variants.json
    fi

    # create empty files for report
    if [ -z "${cnv_json}" ] ; then
        echo '[]' > cnv_variants.json
    else
        cat ${cnv_json} > cnv_variants.json
    fi

    if [ -z "${panel_json}" ] ; then
        echo '[]' > panel.json
    else
        cat ${panel_json} > panel.json
    fi

    if [ -z "${panel_inputs_json}" ] ; then
        echo '[]' > panel_inputs.json
    else
        cat ${panel_inputs_json} > panel_inputs.json
    fi

    if [ -z "${genes_bed_target_json}" ] ; then
        echo '[]' > genes_bed_target.json
    else
        cat ${genes_bed_target_json} > genes_bed_target.json
    fi

    if [ -z "${gene_panel_logs}" ] ; then
        echo '[]' > gene_panel_logs.json
    else
        cat ${gene_panel_logs} > gene_panel_logs.json
    fi

    # create content json based on analysis_type (genome, exome, target or iontorrent)
    python3 /intelliseqtools/reports/templates/script/update_content.py --basic-content "/resources/basic-content.json" \
                                                                        --content "/resources/${genome_or_exome}-content.json" \
                                                                        --analysis-start "${analysis_start}" \
                                                                        --analysis-group "${analysis_group}" \
                                                                        --kit-short-name "${kit}" \
                                                                        --af ${threshold_frequency} \
                                                                        --impact "${impact_threshold}" \
                                                                        --qual ${quality_threshold} \
                                                                        --min-qual ${min_qual} \
                                                                        --del-cov-max ${del_cov_max} \
                                                                        --dup-cov-min ${dup_cov_min} \
                                                                        --het-max ${het_max} \
                                                                        --bf-threshold ${bf_threshold} \
                                                                        --var-call-tool "${var_call_tool}" \
                                                                        --aligner-tool "${aligner_tool}" \
                                                                        --sample-id "${sample_id}" \
                                                                        --output-filename "content.json"

    # create report
    python3 /intelliseqtools/reports/templates/script/report-acmg.py    --template "/resources/report-${template}-template.docx" \
                                                                        --variants-json "variants.json" \
                                                                        --cnv-variants-json "cnv_variants.json" \
                                                                        --other-jsons "sample_info.json" \
                                                                                      "panel.json" \
                                                                                      "panel_inputs.json" \
                                                                                      "genes_bed_target.json" \
                                                                                      "gene_panel_logs.json" \
                                                                                      "picture-generate-report-sh.json" \
                                                                                      "picture-generate-report-cnv-sh.json" \
                                                                                      "content.json" \
                                                                                      "/resources/images/images-width.json" \
                                                                                      "/resources/images/logos.json" \
                                                                                      "/resources/gain.json" \
                                                                                      "/resources/loss.json" \
                                                                        --analysis-type "${genome_or_exome}" \
                                                                        --analysis-start "${analysis_start}" \
                                                                        --analysis-group "${analysis_group}" \
                                                                        --analysis-description "${analysisDescription}" \
                                                                        --analysis-workflow-name "${analysisWorkflow}" \
                                                                        --analysis-workflow-version "${analysisWorkflowVersion}" \
                                                                        --analysis-run "${analysisEnvironment}" \
                                                                        --sample-id "${sample_id}" \
                                                                        --var-call-tool ${var_call_tool} \
                                                                        --aligner-tool ${aligner_tool} \
                                                                        ${true="--germline-analysis" false="" germline_analysis} \
                                                                        ${true="--sv-calling-module" false="" sv_calling_module} \
                                                                        --output-filename "${outputs}/${sample_id}_${output_name}-report.docx"

    soffice --headless \
      --convert-to pdf ${outputs}/${sample_id}_${output_name}-report.docx \
      --outdir ${outputs}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File variants_report_ang_pdf = "${outputs}/${sample_id}_${output_name}-report.pdf"
    File variants_report_ang_docx = "${outputs}/${sample_id}_${output_name}-report.docx"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}