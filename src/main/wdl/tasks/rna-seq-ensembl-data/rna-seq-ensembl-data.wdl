workflow rna_seq_ensembl_data_workflow {

  meta {
    keywords: '{"keywords": ["ensembl dataset", "hisat2", "extract splice sites"]}'
    name: 'rna_seq_ensembl_data'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Preparation of data from ensembl dataset'
    changes: '{"1.1.5": "gtf2bed added",  "1.1.4": "bug fix (wrong download)",  "1.1.3": "bug fix", "1.1.2": "input chromosome_name added for testing", "1.1.1": "new bco, set -e -o pipefail after wgets", "1.1.0": "changes in wget, tests"}'

    input_release_version: '{"name": "release_version", "type": "String", "description": "Ensembl release version"}'
    input_organism_name: '{"name": "organism_name", "type": "String", "description": "Name of the organism in English"}'

    output_ref_genome_index: '{"name": "ref_genome_index", "type": "Array[File]", "copy": "True", "description": "Reference genome indexed"}'
    output_gtf_file: '{"name": "gtf_file", "type": "File", "copy": "True", "description": "GTF file from ensembl dataset"}'
    output_splicesites_file: '{"name": "splicesites_file", "type": "File", "copy": "True", "description": "Splicesites file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "tye": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_ensembl_data

}

task rna_seq_ensembl_data {

  String release_version = "104"
  String organism_name
  String chromosome_name = "primary_assembly" # input for testing only
  String dollar = "$"

  String task_name = "rna_seq_ensembl_data"
  String task_version = "1.1.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-ensembl-data:1.0.1"

  command <<<
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

#    Check if release version exists
    README_PATH_TO_CHECK="ftp.ensembl.org/pub/release-${release_version}/README"
    wget -q --spider $README_PATH_TO_CHECK
    ENSEMBL_STATUS=$?

    if [ $ENSEMBL_STATUS -ne 0 ]
    then
      echo '[${release_version}] version README was not found. Check if this version exists in [ftp.ensembl.org/pub/release-${release_version}].' >&2
      exit 1
    fi

    wget -O species.tsv "http://ftp.ensembl.org/pub/release-${release_version}/species_EnsemblVertebrates.txt"
    awk -F"\t" '$1=="${organism_name}" {print $2}' species.tsv > name.txt
    ORGANISM_NAME=$(cut -f 2 name.txt)

    GENOME_FTP_PATH_PRIMARY_ASSEMBLY="ftp://ftp.ensembl.org/pub/release-${release_version}/fasta/$ORGANISM_NAME/dna/*.dna_sm.${chromosome_name}.fa.gz"
    GENOME_FTP_PATH_TOPLEVEL=$(echo $GENOME_FTP_PATH_PRIMARY_ASSEMBLY | sed -e 's/primary_assembly/toplevel/g')


    # check if ftp path exists (if exists exit status is equal to 8, if does not exist exit status is equal to 0)
    wget -q --spider $GENOME_FTP_PATH_PRIMARY_ASSEMBLY
    STATUS=$?

    if [ $STATUS -eq 0 ]
    then
        wget -q $GENOME_FTP_PATH_TOPLEVEL
    else
        wget -q $GENOME_FTP_PATH_PRIMARY_ASSEMBLY
    fi


    GTF_FTP_PATH="ftp://ftp.ensembl.org/pub/release-${release_version}/gtf/$ORGANISM_NAME/*.${release_version}.gtf.gz"
    TRANSCRIPTOME_FTP_PATH="ftp://ftp.ensembl.org/pub/release-${release_version}/fasta/$ORGANISM_NAME/cdna/*.cdna.all.fa.gz"

    wget -q $GTF_FTP_PATH
    wget -q $TRANSCRIPTOME_FTP_PATH

#   after wget because it caused problems
    set -e -o pipefail

    gunzip *.gz

    gtf_path=$(find "$PWD" -maxdepth 1 -type f -regex ".*\.gtf")

    gtf_file=$(basename -- "$gtf_path")
    gtf_file_without_extension="${dollar}{gtf_file%.*}"

    /gtfToGenePred -genePredExt -geneNameAsName2 -ignoreGroupsWithoutExons $gtf_file /dev/stdout | awk 'BEGIN { OFS="\t"} {print $12, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}' > ${dollar}{gtf_file_without_extension}.refflat

    # prepare bed file from gtf
    awk '{ if ($0 ~ "transcript_id") print $0; else print $0" transcript_id \"\";"; }' $gtf_file | gtf2bed - > ${dollar}{gtf_file_without_extension}.bed

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    Array[File] genome = glob("*.dna_sm.*.fa")
    Array[File] transcriptome = glob("*.cdna.all.fa")
    Array[File] gtf = glob("*.gtf")
    Array[File] refflat = glob("*.refflat")
    Array[File] bed = glob("*.bed")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
