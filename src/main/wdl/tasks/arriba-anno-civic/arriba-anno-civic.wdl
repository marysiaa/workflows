workflow arriba_anno_civic_workflow
{
  call arriba_anno_civic
}

task arriba_anno_civic {

  String task_name = "arriba_anno_civic"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_arriba-anno-civic:1.0.0"

  File arriba_tsv
  String sample_id = "no_id_provided"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  python3 /intelliseqtools/annot_arriba_tsv_with_civic.py \
      --arriba-tsv ${arriba_tsv} \
      --civic-fusions /resources/civic/*/civic-fusions.tsv \
      --output ${sample_id}_arriba-annot.tsv



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File annotated_tsv = "${sample_id}_arriba-annot.tsv"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
