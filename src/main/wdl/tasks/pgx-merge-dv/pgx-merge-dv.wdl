workflow pgx_merge_dv_workflow
{
  call pgx_merge_dv
}

task pgx_merge_dv {

  File vcf_gz
  File vcf_gz_tbi
  File pgx_polygenic_vcf
  File pgx_polygenic_vcf_tbi

  String ref_fasta = "`ls /resources/reference-genomes/*/*.fa`"

  String sample_id = "no_id_provided"

  String task_name = "pgx_merge_dv"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0-grch38-no-alt:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # Find sample name (in the input vcf file)
    sample_name=$(bcftools query -l ${vcf_gz})

    bcftools merge -m all ${pgx_polygenic_vcf} ${vcf_gz} \
    -R ${pgx_polygenic_vcf}  \
    | bcftools norm -m -any -f ${ref_fasta} \
    | bcftools view -s $sample_name -o "${sample_id}_pgx-merged.vcf.gz" -O z

    tabix -p vcf "${sample_id}_pgx-merged.vcf.gz"

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File pgx_merged_vcf = "${sample_id}_pgx-merged.vcf.gz"
    File pgx_merged_vcf_tbi = "${sample_id}_pgx-merged.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
