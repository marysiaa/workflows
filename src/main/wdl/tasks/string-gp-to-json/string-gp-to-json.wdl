workflow string_gp_to_json_workflow {
    call string_gp_to_json
}

task string_gp_to_json {

    String task_name = "string_gp_to_json"
    String task_version = "2.1.1"

    String sample_id = "no_id_provided"

    String panel_json

    Array[String]? hpo_terms
    Array[String]? genes
    Array[String]? diseases
    String? phenotypes_description
    Array[String]? panel_names

    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.5"

    command <<<
        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

        echo "Check panel_json"
        jq "." <<< '${panel_json}' > all_panels.json

        jq -M 'sort_by(.score)|reverse|unique_by(.name)|sort_by(.score)|reverse' all_panels.json | jq -c . > "${sample_id}-panel.json"

        echo '{"hpo_terms": "${sep=', ' hpo_terms}", "genes": "${sep=', ' genes}", "diseases": "${sep=',' diseases}", "phenotypes_description": "${phenotypes_description}", "panel_names": "${sep=',' panel_names}"}' | tr '\n' ' ' > ${sample_id}-panel_inputs.json
        jq '.panel_names |= split(",") | .diseases |= split(",")' ${sample_id}-panel_inputs.json > ${sample_id}-panel_inputs.json.tmp && mv ${sample_id}-panel_inputs.json.tmp ${sample_id}-panel_inputs.json

        bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
        --task-name-with-index ${task_name_with_index} \
        --task-version ${task_version} \
        --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: 1
        maxRetries: 2

    }

    output {
        File validated_panel_json = "${sample_id}-panel.json"
        File all_panels = "all_panels.json"
        File validated_panel_inputs_json = "${sample_id}-panel_inputs.json"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}