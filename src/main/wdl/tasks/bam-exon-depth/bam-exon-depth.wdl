workflow bam_exon_depth_workflow
{
  call bam_exon_depth
}

task bam_exon_depth {

  String task_name = "bam_exon_depth"
  String task_version = "1.0.4"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-exon-depth:1.0.4"
  File ref_counts
  File? custom_bed
  String? kit = "exome-v7" # Possible values "exome-v6", "exome-v7", "exome-v8"
  String exome_bed = if (defined(kit) && !defined(custom_bed)) then "/resources/" + kit + "/covered.bed" else ""
  File bam
  File bai
  Float bf_threshold = 0  # minimal variant bayes factor
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   ln -s ${bam} .
   ln -s ${bai} .

   Rscript /intelliseqtools/exon-depth.R \
      --exon-bed ${exome_bed} ${custom_bed} \
      --bam-dir . \
      --reference ${ref_counts} \
      --bf-threshold ${bf_threshold} \
      --outfile tmp.vcf 2>exon-depth.log

   grep 'Correlation between reference and tests count is' exon-depth.log | cut -f8 -d ' ' > ref-sample-fit.txt
   cat exon-depth.log >&2

   if grep -q -v '#CHROM' tmp.vcf; then
      echo true > vcf-not-empty.txt
   else
      echo false > vcf-not-empty.txt
   fi

   cat /intelliseqtools/header.txt tmp.vcf | sed "s/SAMPLE/${sample_id}/" | bgzip > ${sample_id}_cnv.vcf.gz
   tabix -p vcf ${sample_id}_cnv.vcf.gz



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 2
    maxRetries: 2

  }

  output {

    File cnv_vcf_gz = "${sample_id}_cnv.vcf.gz"
    File cnv_vcf_gz_tbi = "${sample_id}_cnv.vcf.gz.tbi"
    Float sample_to_ref_fit = read_float("ref-sample-fit.txt")
    Boolean vcf_not_empty = read_boolean("vcf-not-empty.txt")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
