workflow rna_seq_infer_experiment_workflow {
    call rna_seq_infer_experiment
}

task rna_seq_infer_experiment {

    File bam
    File bai
    File bed
    String bam_basename = basename(bam, ".bam")

    String task_name = "rna_seq_infer_experiment"
    String task_version = "1.0.1"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/task_rna-seq-infer-experiment:1.0.1"

    command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # infer_experiment (check how reads were stranded)
    infer_experiment.py -r ${bed} -i ${bam} > ${bam_basename}-infer_experiment.txt

    # python code for checking stranded
    python3 /intelliseqtools/check_stranded.py --input_rseqc_log "${bam_basename}-infer_experiment.txt" \
                                               --output_file "stranded.txt"

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: 1
        maxRetries: 2

    }

    output {

        String stranded = read_lines("stranded.txt")[0]
        File infer_experiment_log = "${bam_basename}-infer_experiment.txt"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
