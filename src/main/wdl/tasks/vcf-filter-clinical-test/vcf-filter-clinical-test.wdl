workflow vcf_filter_clinical_test_workflow
{
  call vcf_filter_clinical_test
}

task vcf_filter_clinical_test {

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename = "no_id_provided"

  String task_name = "vcf_filter_clinical_test"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-filter-clinical-test:1.0.2"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      python3 /intelliseqtools/vcf_filter_clinical.py --input-vcf ${vcf_gz} \
                                                      --output-vcf ${vcf_basename}_filtered-clinical-test.vcf.gz

      tabix -p vcf ${vcf_basename}_filtered-clinical-test.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File filtered_vcf_gz = "${vcf_basename}_filtered-clinical-test.vcf.gz"
    File filtered_vcf_gz_tbi = "${vcf_basename}_filtered-clinical-test.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
