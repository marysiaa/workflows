workflow pgx_merge_anno_workflow
{
  call pgx_merge_anno
}

task pgx_merge_anno {

  File openpgx_anno
  File pharmgkb_clinical_anno
  File pharmgkb_variant_anno
  String sample_id = "no_id_provided"

  String task_name = "pgx_merge_anno"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-merge-anno:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    /intelliseqtools/pgx-merge-anno.py  --openpgx-anno ${openpgx_anno} \
                                        --pharmgkb-clinical-anno ${pharmgkb_clinical_anno} \
                                        --pharmgkb-variant-anno ${pharmgkb_variant_anno} \
                                        --output-filename ${sample_id}-merged_anno.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File merged_anno = "${sample_id}-merged_anno.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
