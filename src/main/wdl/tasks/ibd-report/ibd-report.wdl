workflow ibd_report_workflow {

  meta {
    keywords: '{"keywords": ["report", "IBD"]}'
    name: 'IBD report'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Generates IBD plot and report'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "Sample identifier"}'
    input_roh_tables: '{"name": "ROH tables", "type": "Array[File]", "description": "Runs of homozygosity tables"}'

    output_ibd_report_pdf: '{"name": "IBD report pdf", "type": "File", "copy": "True", "description": "IBD report in pdf"}'
    output_ibd_report_html: '{"name": "IBD report html", "type": "File", "copy": "True", "description": "IBD report in html"}'
    output_ibd_report_odt: '{"name": "IBD report odt", "type": "File", "copy": "True", "description": "IBD report in odt"}'
    output_ibd_report_docx: '{"name": "IBD report docx", "type": "File", "copy": "True", "description": "IBD report in docx"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call ibd_report

}

task ibd_report {

  String task_name = "ibd_report"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_ibd-report:1.0.0"

  Array[File] roh_tables
  File? panel
  Boolean is_panel_defined = defined(panel)
  String template = if (is_panel_defined) then "content-with-table.jinja" else "content-without-table.jinja"
  Int tables_length = length(roh_tables)
  Float picture_height = (0.41 * tables_length) + 0.8
  Float frame_height = 70 * picture_height
  String sample_id = "no-id"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   printf "chr\tpos\troh\n" > ${sample_id}_roh-plot-data.txt
   cat ${sep=" " roh_tables} | awk '$1=="ST" && $6 > 30 {print $3"\t"$4"\t"$5}' | sort -k1,1 -k2,2n >> ${sample_id}_roh-plot-data.txt
   cat ${sep=" " roh_tables} | awk '$1=="RG"' | cut -f3-8 | sort -k1,1 -k2,2n >   ${sample_id}_roh.tab

   ensembl_exons=$( ls /resources/ensembl/*/ensembl-exons.tsv )

   if (${is_panel_defined}); then
       cat ${panel} | jq .[].name | uniq | sed 's/\"//g' > panel-genes.txt

       while read f
       do
          awk -F '\t' -v gene=$f '$4 == gene {print $0}' "$ensembl_exons" >> tmp.tsv
       done < panel-genes.txt

      sort -k1,1 -k2,2n tmp.tsv > panel-exons.tsv

      bedtools intersect -b panel-exons.tsv -a ${sample_id}_roh.tab -loj | cut -f1-6,10 | sort -k1,1 -k2,2n | uniq | awk -F '\t' '$7 != "."' \
      | sed 's/\t/:/' | sed 's/\t/-/' | awk  '$1==last {printf ",%s",$5; next} NR>1 {print "";} {last=$1; printf "%s",$0;} END{print "";}' \
      | jq -rRs 'split("\n")[0:-1] | map([split("\t")[]|split(",")] |  {"region":.[0][0],"length":.[1][0],"markers":.[2][0], "qual":.[3][0],"genes":.[4]})' \
      > ${sample_id}_roh.json

   else
      echo "{}" > ${sample_id}_roh.json
   fi

   bedtools intersect -b $ensembl_exons -a ${sample_id}_roh.tab -wb | cut -f10 | sort | uniq \
   | jq -rRs '{"genes": split("\n")[0:-1]}' > ${sample_id}_roh-genes.json


   Rscript /intelliseqtools/ibd-plot.R --input ${sample_id}_roh-plot-data.txt --output roh.svg  --height ${picture_height}

   echo "[" > picture.json
   picturepath=$( realpath roh.svg )
   echo "{\"path\" : \"$picturepath\"}">> picture.json
   echo "]" >> picture.json

   echo "[" > picture-height.json
   echo "{\"height\" : \"${frame_height}\"}">> picture-height.json
   echo "]" >> picture-height.json

  /intelliseqtools/generate-report.sh \
   --json roh_regions=${sample_id}_roh.json \
   --json roh_genes=${sample_id}_roh-genes.json \
   --json picture_height=picture-height.json \
   --template /intelliseqtools/templates/ibd-v1/${template} \
   --name ${sample_id}_ibd-report \
   --pictures picture.json


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File ibd_report_pdf = "${sample_id}_ibd-report.pdf"
    File ibd_report_odt = "${sample_id}_ibd-report.odt"
    File ibd_report_docx = "${sample_id}_ibd-report.docx"
    File ibd_report_html = "${sample_id}_ibd-report.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
