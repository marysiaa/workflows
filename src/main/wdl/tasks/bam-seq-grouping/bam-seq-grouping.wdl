workflow bam_seq_grouping_workflow {

  meta {
    keywords: '{"keywords": ["bam"]}'
    name: 'bam_seq_grouping'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Create sequence grouping files \n Create sequence grouping files for hg38 or grch38-no-alt reference genome'
    changes: '{"1.0.2": "new docker", "1.0.1": "fixed grouping files, latest dir removed"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_seq_grouping

}

task bam_seq_grouping {

  String task_name = "bam_seq_grouping"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String reference_genome = "grch38-no-alt"
  String docker_image = "intelliseqngs/task_bam-seq-grouping-"+ reference_genome +":1.1.0"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  cp /resources/sequence_grouping.txt  .
  cp /resources/sequence_grouping_with_unmapped.txt .

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                                --task-name-with-index ${task_name_with_index} \
                                                --task-version ${task_version} \
                                                --task-docker ${docker_image}

  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File sequence_grouping_txt = "sequence_grouping.txt"
    File sequence_grouping_with_unmapped_txt = "sequence_grouping_with_unmapped.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
