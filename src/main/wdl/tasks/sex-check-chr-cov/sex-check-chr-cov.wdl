workflow sex_check_chr_cov_workflow {

  meta {
    keywords: '{"keywords": ["sex", "biological sex", "coverage", "chrY", "chrX"]}'
    name: 'sex_check_chr_cov'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task computes mean coverage of chosen targeted regions of X and Y chromosomes and its ratio. Uses empirical criteria to assess biological sex: F or M. In some boundary cases ::cannot be determined by this method:: can be returned.'
    changes: '{"1.0.3":"update output json structure", "1.0.1": "added error handling"}'


    input_bam: '{"name": "bam", "type": "File", "description": "Sample bam file, use *markdup.recalibrated.bam or *markdup.realigned.recalibrated.bam or *filtered.bam, DO NOT USE *realigned-haplotypecaller.bam or *.markdup.bam", "extension": [".bam"]}'
    input_bai: '{"name": "bai", "type": "File", "description": "Sample bai file", "extension": [".bai"]}'



    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sex_check_chr_cov

}

task sex_check_chr_cov {

  File bam
  File bai

  String task_name = "sex_check_chr_cov"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_sex-check-chr-cov:1.0.0"

  String dollar = "$"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   grep chrX /resources/covered.bed > chrX.bed
   grep chrY /resources/covered.bed > chrY.bed

   samtools bedcov chrX.bed ${bam} | awk '{print $3-$2+1 " " $5}' > chrX.stats
   samtools bedcov chrY.bed ${bam} | awk '{print $3-$2+1 " " $5}' > chrY.stats

   XCOV=$(awk '{s+=$1; c+=$2} END {print c " " s}' chrX.stats)
   YCOV=$(awk '{s+=$1; c+=$2} END {print c " " s}' chrY.stats)

   touch sex_recognition_chr_cov.json
   if [ ${dollar}{YCOV:0:1} == 0 ] ; then

      echo "{\"method\":\"Ratio of chrX and chrY coverage\",\"result\":\"cannot be determined by this method\",\"measure_type\":\"Coverage ratio\",\"measure_value\":\"no coverage on chrY\",\"thresholds\":\"Male: ratio < 15.\nFemale: ratio > 50.\"}" >> sex_recognition_chr_cov.json

   else
      XMEANCOV=$(echo $XCOV | awk '{print $1/$2}')
      YMEANCOV=$(echo $YCOV | awk '{print $1/$2}')
      RATIO=$(echo $XMEANCOV $YMEANCOV | awk '{printf ("%.2f", $1/$2)}')

      SEX=$(echo $RATIO | awk '{if ($1 < 5) print "M"; else if ($1 > 50) print "F"; else print "cannot be determined by this method"}')

      echo "{\"method\":\"Ratio of chrX and chrY coverage\",\"result\":\""$SEX"\",\"measure_type\":\"Coverage ratio\",\"measure_value\":\""$RATIO"\",\"thresholds\":\"Male: ratio < 15.\nFemale: ratio > 50.\"}" >> sex_recognition_chr_cov.json

   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File sex_report_json = "sex_recognition_chr_cov.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
