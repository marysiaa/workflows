workflow mt_extract_reads_workflow {

  meta {
    keywords: '{"keywords": ["mitochondrion"]}'
    name: 'Extract mitochondrial reads'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Extract mitochondrial reads from genome wise bam'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_bam: '{"name": "Bam file", "type": "File", "extension": [".bam"], "description": "Genome bam file"}'
    input_bai: '{"name": "Bam index file", "type": "File", "extension": [".bai"], "description": "Index for the genome bam file"}'

    output_reverted_mt_bam:'{"name": "Reverted mitochondrial bam", "type": "File", "copy": "True", "description": "Unmapped bam file with mitochondrial reads"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call mt_extract_reads

}

task mt_extract_reads {

  String task_name = "mt_extract_reads"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.9.0:1.0.0"

  String sample_id = "no_id_provided"
  File bam
  File bai
  String java_options = "-Xmx1000m"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk PrintReads \
      -L chrM \
      --read-filter MateOnSameContigOrNoMappedMateReadFilter \
      --read-filter MateUnmappedAndUnmappedReadFilter \
      -I ${bam} \
      -O ${sample_id}-mt.bam


   gatk  --java-options "${java_options}" \
    RevertSam \
    -I ${sample_id}-mt.bam \
    --OUTPUT_BY_READGROUP false \
    -O ${sample_id}_reverted-mt.bam \
    --VALIDATION_STRINGENCY LENIENT \
    --ATTRIBUTE_TO_CLEAR FT \
    --ATTRIBUTE_TO_CLEAR CO \
    --SORT_ORDER queryname \
    --RESTORE_ORIGINAL_QUALITIES false


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File reverted_mt_bam = "${sample_id}_reverted-mt.bam"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
