workflow resources_panels_workflow {

  meta {
    keywords: '{"keywords": ["gene panel"]}'
    name: 'resources_panels'
    author: 'https://gitlab.com/mremre'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Generic text for task'
    changes: '{"latest": "no changes"}'

    input_panel_name: '{"name": "Panel", "type": "String", "required": "True", "constraints": {"values": ["acmg-recommendation-panel"]}, "description": ""}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call resources_panels

}

task resources_panels {

  String panel_name = "acmg-recommendation-panel"
  String phenotypes_name = "phenotypes"

  String task_name = "resources_panels"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_resources-panels:0.1.3"


  command <<<
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    cp /resources/${panel_name}.json panel.json
    chmod 666 panel.json

    cp /resources/${phenotypes_name}.json phenotypes.json
    chmod 666 phenotypes.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File panel = "panel.json"
    File phenotypes = "phenotypes.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
