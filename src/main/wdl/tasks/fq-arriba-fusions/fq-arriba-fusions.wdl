workflow fq_arriba_fusions_workflow
{
  call fq_arriba_fusions
}

task fq_arriba_fusions {

  String task_name = "fq_arriba_fusions"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/arriba:1.0.0"

  String sample_id = "no_id_provided" 
  Array[File] fastqs_1
  Array[File] fastqs_2
  Int num_thread = 6
  String ensembl_version = "104"
  String star_index_dir = "/resources/reference-genomes/grch38/STAR_index_GRCh38_ENSEMBL" + ensembl_version

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # run SRAR  
  STAR \
  --runThreadN ${num_thread} \
  --genomeDir ${star_index_dir} \
  --genomeLoad NoSharedMemory \
  --readFilesIn ${sep="," fastqs_1} ${sep="," fastqs_2} \
  --readFilesCommand zcat \
  --outStd BAM_Unsorted \
  --outSAMtype BAM Unsorted \
  --outSAMunmapped Within \
  --outBAMcompression 0 \
  --outFilterMultimapNmax 50 \
  --peOverlapNbasesMin 10 \
  --alignSplicedMateMapLminOverLmate 0.5 \
  --alignSJstitchMismatchNmax 5 -1 5 5 \
  --chimSegmentMin 10 \
  --chimOutType WithinBAM HardClip \
  --chimJunctionOverhangMin 10 \
  --chimScoreDropMax 30 \
  --chimScoreJunctionNonGTAG 0 \
  --chimScoreSeparation 1 \
  --chimSegmentReadGapMax 3 \
  --chimMultimapNmax 50 | \

  tee temp.sam  | \
  
  # run arriba
  arriba \
  -x /dev/stdin \
  -o ${sample_id}_fusions.tsv \
  -O ${sample_id}_fusions-discarded.tsv \
  -a /resources/GRCh38.fa \
  -g /resources/ENSEMBL${ensembl_version}.gtf \
  -b /database/blacklist_hg38_GRCh38_v2.3.0.tsv.gz \
  -k /database/known_fusions_hg38_GRCh38_v2.3.0.tsv.gz \
  -t /database/known_fusions_hg38_GRCh38_v2.3.0.tsv.gz \
  -p /database/protein_domains_hg38_GRCh38_v2.3.0.gff3  

  # sort and index the bam file
  samtools sort -@ ${num_thread} -o ${sample_id}.bam -O bam  temp.sam
  samtools index ${sample_id}.bam

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "12G"
    cpu: 6
    maxRetries: 2

  }

  output {

    File bam = "${sample_id}.bam"
    File bai = "${sample_id}.bam.bai"
    File fusions_tsv = "${sample_id}_fusions.tsv"
    File discarded_fusions_tsv = "${sample_id}_fusions-discarded.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
