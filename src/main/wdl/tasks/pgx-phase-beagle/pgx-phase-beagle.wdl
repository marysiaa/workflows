workflow pgx_phase_beagle_workflow
{
  call pgx_phase_beagle
}

task pgx_phase_beagle {

  File genotyped_vcf_gz
  File phasing_bed

  String sample_id = "no_id_provided"
  String beagle_path = "/tools/beagle/5.4-05May22.33a/beagle.05May22.33a.jar"
  String fai_path = "/resources/grch38-no-alt-analysis-set.fa.fai"

  String task_name = "pgx_phase_beagle"
  String task_version = "1.1.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/pgx-phase-beagle:1.0.2"

  String dollar = "$"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    bcftools sort -Oz -o ${sample_id}-sorted.vcf.gz ${genotyped_vcf_gz}
    tabix -p vcf ${sample_id}-sorted.vcf.gz

    mkdir phased-vcfs/

    # Phasing
    cat ${phasing_bed} | xargs -L 1 -P 16 -i sh -c '\
      RANGE="$(echo {} | cut -d \  -f1)" && \
      GENE="$(echo {} | cut -d \  -f2)" && \
      java -jar ${beagle_path} \
          gt=${sample_id}-sorted.vcf.gz \
          chrom=$RANGE \
          impute=false \
          ref="/resources/bref3-reference/"$GENE".bref3" \
          out="phased-vcfs/${sample_id}_"$GENE"-phased"'

    # Reheader (add contigs) and index VCFs files 
    for file in phased-vcfs/*phased.vcf.gz ; do
        bcftools reheader --fai ${fai_path} "${dollar}{file}" -o "${dollar}{file}"
        tabix -p vcf "${dollar}{file}"
    done

    # Concatenate VCFs
    bcftools concat -a phased-vcfs/*phased.vcf.gz | bcftools sort -Oz -o ${sample_id}-concat.vcf.gz
    tabix -p vcf ${sample_id}-concat.vcf.gz

    # Merge VCFs
    sample_name=$(bcftools query -l ${sample_id}-sorted.vcf.gz)
    bcftools merge --force-samples ${sample_id}-concat.vcf.gz ${sample_id}-sorted.vcf.gz | bcftools view -s $sample_name | bcftools sort -o ${sample_id}-phased.vcf.gz -Oz
    tabix -p vcf ${sample_id}-phased.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File vcf_phased = "${sample_id}-phased.vcf.gz"
    File vcf_phased_tbi = "${sample_id}-phased.vcf.gz.tbi"
    File vcf_sorted = "${sample_id}-sorted.vcf.gz"
    File vcf_sorted_tbi = "${sample_id}-sorted.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
