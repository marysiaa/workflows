workflow bam_deepvariant_workflow
{
  call bam_deepvariant
}

task bam_deepvariant {

  File recalibrated_markdup_bam
  File recalibrated_markdup_bai
  String sample_id = "no_id_provided"
  File bed_file
  
  # hg38 or grch38-no-alt
  String reference_genome = "hg38"
  String model_type = "WES"
  Int num_shards = 10
  Boolean filter_pass = true

  String task_name = "bam_deepvariant"
  String task_version = "1.0.9"
  Int? index
  String index_out = if defined(index) then index + "-"  else ""

  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/deepvariant-1.3.0-" +reference_genome+ ":1.0.3"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    ref_genome=$(ls /resources/*/*/*.fa)


    /opt/deepvariant/bin/run_deepvariant \
                                        --model_type=${model_type} \
                                        --ref=$ref_genome \
                                        --reads=${recalibrated_markdup_bam} \
                                        --regions=${bed_file} \
                                        --output_vcf=${index_out}${sample_id}.vcf.gz \
                                        --output_gvcf=${index_out}${sample_id}.g.vcf.gz \
                                        --num_shards=${num_shards} \
                                        --logging_dir=logs \
                                        --novcf_stats_report \
                                        --dry_run=false

    if [ ${filter_pass} = true ]; then
      bcftools filter -i 'FILTER="PASS"' -o tmp-${index_out}${sample_id}.vcf.gz -O z ${index_out}${sample_id}.vcf.gz
      mv tmp-${index_out}${sample_id}.vcf.gz ${index_out}${sample_id}.vcf.gz
    fi

    bcftools index -f ${index_out}${sample_id}.vcf.gz

  
   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: 8
    maxRetries: 2

  }

  output {

    File vcf_gz = "${index_out}${sample_id}.vcf.gz"
    File vcf_gz_tbi = "${index_out}${sample_id}.vcf.gz.tbi"    
    File gvcf_gz = "${index_out}${sample_id}.g.vcf.gz"
    File gvcf_gz_tbi = "${index_out}${sample_id}.g.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
