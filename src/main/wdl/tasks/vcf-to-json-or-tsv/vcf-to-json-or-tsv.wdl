workflow vcf_to_json_or_tsv_workflow {

  meta {
    keywords: '{"keywords": ["VCF", "json", "tsv"]}'
    name: 'vcf_to_json_or_tsv'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Converts vcf into tsv or json'
    changes: '{"1.0.10": "add new field (FILTER) in csv with selected columns" "1.0.9": "add new field (ISEQ_COSMID_ID) in csv with selected columns", "1.0.8": "add new fields in csv with selected columns", "1.0.7": "changes in scripts to deal with ISEQ_MANE_ANN field", "1.0.6": "add json_select_fields script", "1.0.5": "adding column_description to the resulting json", "1.0.4": "fixed misuse variable for iseq report", "1.0.3": "output name based on vcf_gz instead of sample_id", "1.0.2": "Meta fix, added empty json and tsv file to run on gc", "1.0.1": "fixed TypeError when processing empty (no variants) vcf", "1.0.0": "added possibility to split ISEQ_REPORT_ANN field and return json and tsv simultaneously", "0.0.4": "fixed missing commas in json output", "0.0.3": "small meta fix and g in sed", "0.0.2": "END tag renamed for sv vcf"}'

    input_vcf_gz: '{"index": 1, "name": "Vcf gz", "type": "File", "extension": [".vcf.gz"], "description": "Vcf file"}'
    input_vcf_gz_tbi: '{"index": 2, "name": "Vcf gz tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Tabix vcf index file"}'
    input_sample_id: '{"index": 3, "name": "Sample id", "type": "String", "description": "Identifier of sample"}'
    input_output_formats: '{"index": 4, "name": "Output format", "type": "String", "constraints": {"values": ["json", "tsv"]}, "description": "Output format"}'
    input_split_ANN_field: '{"index": 5, "name": "Split ANN field", "type": "Boolean", "description": "Whether to split ANN file or not"}'
    input_split_CSQ_field: '{"index": 6, "name": "Split CSQ field", "type": "Boolean", "description": "Whether to split CSQ file or not"}'
    input_split_ISEQ_REPORT_ANN_field: '{"index": 7, "name": "Split ISEQ_REPORT_ANN field", "type": "Boolean", "description": "Whether to split ISEQ_REPORT_ANN file or not"}'
    input_default_value: '{"index": 8, "name": "Default for missing values", "type": "String", "description": "Default for missing values"}'
    input_sv_analysis: '{"index": 9, "name": "SV analysis", "type": "Boolean", "description": "Whether vcf is from structural variant analysis"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_converted_file: '{"name": "Converted file", "type": "File", "copy": "True", "description": "Output file"}'

  }

  call vcf_to_json_or_tsv

}

task vcf_to_json_or_tsv {

  String task_name = "vcf_to_json_or_tsv"
  String task_version = "1.0.10"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-to-json-or-tsv:1.0.7"

  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no_id_provided"
  String output_formats = 'json tsv'
  Boolean split_ANN_field = true
  Boolean split_CSQ_field = true
  Boolean split_ISEQ_REPORT_ANN_field = true
  Boolean split_ISEQ_MANE_ANN_field = false
  Boolean sv_analysis = false
  Boolean select_columns = false
  String? default_value
  String default_value_string = if defined(default_value) then "--default-value " + default_value else ""
  String output_name_without_extension = basename(vcf_gz, ".vcf.gz")

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   if ${sv_analysis}; then
      zcat ${vcf_gz} | sed 's/\bEND\b/ISEQPYSAMEND/' | bgzip > tmp.vcf.gz
      tabix -p vcf tmp.vcf.gz
   else
      mv ${vcf_gz} tmp.vcf.gz
      mv ${vcf_gz_tbi} tmp.vcf.gz.tbi
   fi


   ## create empty files (optional outputs cause problems on gc)
   touch ${output_name_without_extension}.json
   touch ${output_name_without_extension}.tsv


   python3 /intelliseqtools/vcf_to_json_or_tsv.py --input-vcf tmp.vcf.gz \
                                                  --output-name ${output_name_without_extension} \
                                                  --output-extensions ${output_formats} \
                                                  ${default_value_string} \
                                                  ${true="--split-ANN-field" false="" split_ANN_field} \
                                                  ${true="--split-CSQ-field" false="" split_CSQ_field} \
                                                  ${true="--split-ISEQ_REPORT_ANN-field" false="" split_ISEQ_REPORT_ANN_field} \
                                                  ${true="--split-ISEQ_MANE_ANN-field" false="" split_ISEQ_MANE_ANN_field}

   if ${select_columns}; then
    python3 /intelliseqtools/json_select_fields.py --json "${output_name_without_extension}.json" \
                                                   --csv "/resources/column-list-to-json.csv" \
                                                   --output_filename "${output_name_without_extension}.json"
   fi                                           


   if ${sv_analysis}; then
    for extension in tsv json; do
      if [[ -f ${output_name_without_extension}.$extension ]]; then
       sed -i 's/ISEQPYSAMEND/END/g' ${output_name_without_extension}.$extension
      fi
    done
   fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File output_json = "${output_name_without_extension}.json"
    File output_tsv = "${output_name_without_extension}.tsv"
  }

}
