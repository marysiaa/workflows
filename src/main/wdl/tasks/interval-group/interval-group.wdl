workflow interval_group_workflow {

  meta {
  keywords: '{"keywords": ["some", "keywords"]}'
  name: 'interval_group'
  author: 'https://gitlab.com/kattom , https://gitlab.com/mremre'
  copyright: 'Copyright 2020 Intelliseq'
  description: 'Create grouped calling intervals'
  changes: '{"1.1.2" : "new docker with excluding /etc/ssl/private from du", "1.1.1" : "new docker - non-root permission to resources", "1.1.0" : "new docker with new bco"}'

  input_interval_file: '{"name": "inteval file", "type": "File", "extension": [".interval_list"], "description": "File with list of genome intervals to be analyzed"}'
  input_max_no_pieces: '{"name": "Max number of pieces", "type": "Int", "default": "3", "description": "Maximal number of pieces to scatter an interval file"}'

  output_grouped_calling_intervals: '{"name": "Grouped calling intervals", "type": "Array[File]", "description": "Array of intervals of similar length"}'

  output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
  output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
  output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }


  call interval_group

}

task interval_group {
  File interval_file
  Int max_no_pieces_to_scatter_an_interval_file = 3

  String task_name = "interval_group"
  String task_version = "1.1.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_interval-group:1.0.2"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  python3 /intelliseqtools/interval-group.py --interval_file ${interval_file} --no_pieces ${max_no_pieces_to_scatter_an_interval_file}  --output_dir .

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    Array[File] grouped_calling_intervals = glob("*.interval_list")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
