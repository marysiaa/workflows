workflow cromwell_test_task_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'cromwell_test_task'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.0.0": "no changes"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call cromwell_test_task

}

task cromwell_test_task {

  File? test_file
  String test_string = "test"
  Int sec_to_wait = 3600

  String task_name = "cromwell_test_task"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    sleep ${sec_to_wait}

    if [ -z "${test_file}" ] ; then
        echo ${test_string} > results.txt
    else
        cp ${test_file} results.txt
        echo ${test_string} >> results.txt
    fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
    File results = "results.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
