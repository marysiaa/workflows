workflow gtc_to_vcf_workflow {

  meta {
    keywords: '{"keywords": ["gtc", "genotyping", "vcf"]}'
    name: 'gtc_to_vcf'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Converts unclustered gtc file(s) to vcf'
    changes: '{"1.1.1": "return also splitted vcfs", "1.0.0": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_bpm_manifest_file: '{"name": "BPM manifest file", "type": "File", "extension": [".bpm"], "description": "BPM manifest file, needed for normalized intensities computation."}'
    input_csv_manifest_file: '{"name": "CSV manifest file", "type": "File", "extension": [".csv"], "description": "CSV manifest file, needed for indels analysis"}'
    input_egt_cluster_file: '{"name": "EGT cluster file", "type": "File", "extension": [".egt"], "description": "File with cluster information"}'
    input_gtc_files: '{"name": "Gtc files", "type": "Array[File]", "extension": [".gtc"], "description": "GTC files to analyze"}'
    input_adjust_clusters: '{"name": "Adjust_clusters?", "type": "Boolean", "description": "Decides whether use --adjust-cluster option (which will recenter the genotype clusters rather than using those provided in the EGT cluster file; not recommended for small number of gtc files analysis.)"}'
    input_sd_multiplier: '{"name": "SD multiplier", "type": "Float", "default": 3.0, "description": "Decides on genotype THETA CI (used in the genotyping step)"}'

    output_vcf_gz: '{"name": "Vcf file", "type": "File", "copy": "True", "description": "Genotyped vcf file (for all samples)"}'
    output_vcf_gz_tbi : '{"name": "Vcf tbi", "type": "File", "copy": "True", "description": "Index for the genotyped vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'



  }

  call gtc_to_vcf

}

task gtc_to_vcf {

  String task_name = "gtc_to_vcf"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_gtc-to-vcf-grch38-no-alt:1.1.0"

  File bpm_manifest_file
  File csv_manifest_file
  File egt_cluster_file
  Array[File] gtc_files
  Int array_length = length(gtc_files)
  Boolean adjust_clusters = true ## if (array_length) > 10 then true else false
  Float sd_multiplier = 3.0

  String ref_path = "/resources/reference-genomes/grch38-no-alt-analysis-set/grch38-no-alt-analysis-set.fa"
  String sample_id = "no_id_provided"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   mkdir gtc-folder
   ln -s ${sep=" " gtc_files} gtc-folder


   ## gtc files to one vcf
   bcftools +gtc2vcf \
       --no-version -Ou \
       --bpm ${bpm_manifest_file} \
       --csv ${csv_manifest_file} \
       --egt ${egt_cluster_file} \
       --gtcs gtc-folder \
       --fasta-ref ${ref_path} \
       ${true='--adjust-clusters' false='' adjust_clusters} \
       --extra ${sample_id}_gtc2vcf.tsv | \
       bcftools sort -Ou -T ./bcftools-sort.XXXXXX | \
       bcftools norm --no-version -c x -f ${ref_path} |\
       bcftools filter -e 'INFO/INTENSITY_ONLY=1' -Oz -o ${sample_id}_gtc2vcf.vcf.gz 
    tabix -p vcf ${sample_id}_gtc2vcf.vcf.gz
   
   ## add genotypes to the vcf
   python3 /intelliseqtools/genotype-gtc2vcf.py \
       --input-vcf ${sample_id}_gtc2vcf.vcf.gz \
       --output-vcf ${sample_id}_genotyped-gtc2vcf.vcf.gz \
       --ci-multiplier ${sd_multiplier}

   tabix -p vcf  ${sample_id}_genotyped-gtc2vcf.vcf.gz

   bcftools query -f '%CHROM\t%POS\t%ID\t%REF\t%ALT[\t%GT]\n' ${sample_id}_genotyped-gtc2vcf.vcf.gz > tmp.tsv
   bcftools view -h  ${sample_id}_genotyped-gtc2vcf.vcf.gz  | tail -1 | cut -f1,2,4,5,3,10- | sed 's/^#//' | cat - tmp.tsv > bcftools_query.tsv
   
   python3 /intelliseqtools/gt_source.py \
		--input-vcf ${sample_id}_genotyped-gtc2vcf.vcf.gz \
		--output-vcf ${sample_id}_genotyped-annot-gtc2vcf.vcf.gz \
		--bcftools-tsv bcftools_query.tsv

   tabix -p vcf ${sample_id}_genotyped-annot-gtc2vcf.vcf.gz 		

   mkdir per-sample-vcfs
   bcftools +split  ${sample_id}_genotyped-annot-gtc2vcf.vcf.gz -o per-sample-vcfs -O z
   ls per-sample-vcfs/*.vcf.gz | xargs -i -n 1 -P 6 bash -c "tabix -f -p vcf {}"


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: 4
    maxRetries: 2

  }

  output {

    File vcf_gz = "${sample_id}_genotyped-annot-gtc2vcf.vcf.gz"
    File vcf_gz_tbi = "${sample_id}_genotyped-annot-gtc2vcf.vcf.gz.tbi"
    Array[File] per_sample_vcfs_gz = glob("per-sample-vcfs/*.vcf.gz")
    Array[File] per_sample_vcfs_gz_tbi = glob("per-sample-vcfs/*.vcf.gz.tbi")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
