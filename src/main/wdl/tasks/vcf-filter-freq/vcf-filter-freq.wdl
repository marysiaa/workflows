workflow vcf_filter_freq_workflow {

    meta {
        keywords: '{"keywords": ["frequency", "filter", "vcf"]}'
        name: 'vcf_filter_freq'
        author: 'https://gitlab.com/lltw'
        copyright: 'Copyright 2019 Intelliseq'
        description: 'Generic text for task'
        changes: '{"1.3.1": "set -oe pipefail added"}'

        input_vcf_basename: '{"name": "vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID."}'
        input_genome_or_exome: '{"name": "genome or exome", "type": "String", "default": "genome", "constraints": {"values": ["exome", "genome"]}, "description": " "}'
        input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]},"required": "False" , "description": ""}'
        input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file"}'
        input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
        input_gnomad_af_popmax_threshold : '{"name": "gnomAD threshold", "type": "Float", "constraints": {"min": "0.0", "max": "1.0"}, "description": "Maximum AF across gnomAD populations"}'
        input_exac_af_popmax_threshold : '{"name": "ExAC threshold", "type": "Float", "constraints": {"min": 0.0, "max": 1.0}, "description": "Maximum AF across ExAC populations"}'
        input_mitomap_af_threshold : '{"name": "MITOMAP threshold", "type": "Float", "constraints": {"min": 0.0, "max": 1.0}, "description": "Maximum AF across MITOMAP populations"}'

        output_filtered_by_frequencies_vcf_gz : '{"name": "filtered by frequencies vcf files", "type": "File", "description": ""}'
        output_filtered_by_frequencies_vcf_gz_tbi : '{"name": "filtered by frequencies vcf files indexes", "type": "File", "description": ""}'
        output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
        output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
        output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    }

    call vcf_filter_freq

}

task vcf_filter_freq {

    File vcf_gz
    File vcf_gz_tbi

    String vcf_basename = "no_id_provided"
    String? chromosome
    Boolean chromosome_defined = defined(chromosome)
    String vcf_prefix = if chromosome_defined then chromosome + "-" + vcf_basename else vcf_basename

    Float gnomad_af_popmax_threshold = 0.05
    Float exac_af_popmax_threshold = 0.05
    Float mitomap_af_threshold = 0.05

    String gnomad_genomes_popmax_af_field = "ISEQ_GNOMAD_GENOMES_popmax_AF"
    String gnomad_genomes_v3_popmax_af_field = "ISEQ_GNOMAD_GENOMES_V3_popmax_AF"
    String gnomad_exomes_popmax_af_field = "ISEQ_GNOMAD_EXOMES_popmax_AF"
    String mitomap_af_field = "MITOMAP_AF"

    String task_name = "vcf_filter_freq"
    String task_version = "1.3.1"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.4"

    command <<<
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

        set -e -o pipefail

        # Tbi could be in different directory than vcf file, the symlink below fixes it
        vcf_dir=$(dirname "${vcf_gz}")
        tbi_name=$(basename "${vcf_gz_tbi}")
        if [ ! -f $vcf_dir/$tbi_name ]; then
            ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
        fi

        bcftools filter ${vcf_gz} --exclude "INFO/${gnomad_genomes_popmax_af_field} > ${gnomad_af_popmax_threshold}" \
          | bcftools filter --exclude "INFO/${gnomad_genomes_v3_popmax_af_field} > ${gnomad_af_popmax_threshold}"  \
          | bcftools filter --exclude "INFO/${gnomad_exomes_popmax_af_field} > ${gnomad_af_popmax_threshold}" \
          | bcftools filter --exclude "INFO/${mitomap_af_field} > ${mitomap_af_threshold}" -o ${vcf_prefix}_filt-freq.vcf.gz -O z


        tabix -p vcf ${vcf_prefix}_filt-freq.vcf.gz

        bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                                   --task-name-with-index ${task_name_with_index} \
                                                   --task-version ${task_version} \
                                                   --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "4G"
        cpu: "2"
        maxRetries: 2

    }

    output {

        File filtered_by_frequencies_vcf_gz = "${vcf_prefix}_filt-freq.vcf.gz"
        File filtered_by_frequencies_vcf_gz_tbi = "${vcf_prefix}_filt-freq.vcf.gz.tbi"

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}
