workflow report_mobigen_workflow {

  meta {
    keywords: '{"keywords": ["report", "polygenic traits"]}'
    name: 'Polygenic traits report'
    author: 'https://gitlab.com/marysiaa https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Report for polygenic traits'
    changes: '{"1.0.1": "add script for sample_info_json input, update template", 1.0.0": "own tag, change output name"}'

    input_json: '{"name": "json", "type": "File", "description":"Output of the polygenic risk score models", "constraints": {"extension": [".json"]}}'
    input_patient_json: '{"name": "Patient json", "type": "File", "description":"Patient data in json format, should include attributes (even if without values): name, surname, sex, birthdate and pesel", "constraints": {"extension": [".json"]}}'
    input_pictures: '{"name": "Images", "type": "Array[File]", "description":"Images to be placed in the report", "constraints": {"extension": [".svg"]}}'
    input_sample_id: '{"name": "Sample ID", "type": "String", "description": "Enter a sample name (or identifier)"}'
    input_sample_info_json: '{"advanced":"true", "name": "Patient and sample information", "type": "File", "extension": [".json"], "description": "Add patient and sample data in json format"}'

    output_mobigen_report_pdf: '{"name": "Report mobigen pdf", "type": "File", "copy": "True", "description": "Report of mobigen results in pdf file"}'
    output_mobigen_report_odt: '{"name": "Report mobigen odt", "type": "File", "copy": "True", "description": "Report of mobigen results in odt file"}'
    output_mobigen_report_docx: '{"name": "Report mobigen docx", "type": "File", "copy": "True", "description": "Report of mobigen results in docx file"}'
    output_mobigen_report_html: '{"name": "Report  mobigen html", "type": "File", "copy": "True", "description": "Report of mobigen results in docx file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call report_mobigen

}

task report_mobigen {

  String task_name = "report_mobigen"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report-mobigen:1.0.3"

  File json
  Array[File] pictures
  File? sample_info_json
  Int timezoneDifference = 0
  String sample_id = "no_id_provided"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  set -e
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  # Prepare sample_info.json
  bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                    --timezoneDifference ${timezoneDifference} \
                                                    --sampleID "${sample_id}"


  echo "[" > picture.json
  for picture in ${sep=' ' pictures}; do
    picturepath=$( realpath $picture)
    echo "{\"path\" : \""$picturepath"\"},">> picture.json
  done
  sed -i '$ s/.$//' picture.json
  echo "]" >> picture.json


  echo "{" > references.json
  for pmid in $(cat ${json} | jq .[].references.article_links | grep -v '\[\|\]' | sed 's/,//' | sort | uniq )
  do
    name=$(echo $pmid | sed 's/\"//g' )
    text=$( curl -o - "https://api.ncbi.nlm.nih.gov/lit/ctxp/v1/pubmed/?format=citation&id=$name" | jq .ama.orig )
    echo $pmid : $text, >> references.json
  done
  sed -i '$ s/.$//' references.json
  echo "}" >> references.json


  /intelliseqtools/generate-report.sh \
  --json mobigen=${json} \
  --json references=references.json \
  --json sample=sample_info.json \
  --template /intelliseqtools/templates/mobigen-v1/content.jinja \
  --name ${sample_id}_polygenic-score-report \
  --pictures picture.json


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File mobigen_report_pdf = "${sample_id}_polygenic-score-report.pdf"
    File mobigen_report_odt = "${sample_id}_polygenic-score-report.odt"
    File mobigen_report_docx = "${sample_id}_polygenic-score-report.docx"
    File mobigen_report_html = "${sample_id}_polygenic-score-report.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
