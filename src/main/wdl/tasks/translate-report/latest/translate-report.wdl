workflow translate_report_workflow {

  meta {
    keywords: '{"keywords": ["translate", "report"]}'
    name: 'translate_report'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'translate_report \n Pairs English-Polish translations from excel file and translates jinja files'
    changes: '{"latest": "no changes"}'
    input_excel_file: '{"name": "excel_file", "type": "File", "constraints": {"extension": "xlsx"}, "required": true, "description": "Excel file with paired English-Polish translations in nearby columns"}'
    input_jinja_files : '{"name": "jinja_files", "type": "Array[File]", "constraints": {"extension": "jinja"}, "required: true", "description": "Jinja files with English meanigs to translate them to Polish"}'
    output_translated_jinja_files: '{"name": "translated_jinja_files", "type": "Array[File]", "copy": "True", "description": "Jinja files translated to Polish"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call translate_report

}

task translate_report {
  Array[File] jinja_files
  File excel_file
  String translated_jinja= "out"

  String task_name = "translate_report"
  String task_version = "dir"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_translate-report:1.0.0"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/after-start.sh)

  mkdir dir
  ln -s ${sep=" " jinja_files} dir

  python3 /intelliseqtools/translate-report.py  --input-xlsx ${excel_file} \
                                                --input-jinja dir \
                                                --output-jinja ${translated_jinja}


  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    Array[File] translated_jinja_files = glob("dir/*.jinja")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
