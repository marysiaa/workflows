workflow gvcf_remove_tags_workflow
{
  call gvcf_remove_tags
}

task gvcf_remove_tags {

  File gvcf_gz
  Array[String] tags = ["GP", "PG"]
  String sample_id = "no_id_provided"

  String task_name = "gvcf_remove_tags"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.7"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    for item in ${sep=' ' tags}; do
      FORMAT_TAGS="$FORMAT_TAGS,FORMAT/$item"
    done
    FORMAT_TAGS=$(echo $FORMAT_TAGS | awk '{sub(",","")}1')

    bcftools annotate -x $FORMAT_TAGS ${gvcf_gz} | bgzip -c > ${sample_id}.g.vcf.gz
    tabix -p vcf ${sample_id}.g.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File gvcf_removed_tags_gz = "${sample_id}.g.vcf.gz"
    File gvcf_removed_tags_gz_tbi = "${sample_id}.g.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
