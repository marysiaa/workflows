workflow report_pgx_workflow
{
  call report_pgx
}

task report_pgx {

  File recommendations
  File vcf
  Array[File]? drug_order
  File? sample_info_json

  String output_name = "main"
  String sample_id = "no_id_provided"
  Int timezoneDifference = 0

  String task_name = "report_pgx"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_pgx:1.0.3"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
   
    mkdir outputs/

    # Query vcf for gene_names, gnomad_id, rsid and genotype
    bcftools query -f '%ISEQ_GENES_NAMES\t%ISEQ_GNOMAD_ID\t%ISEQ_DBSNP_RS\t[%TGT]\n' ${vcf} > rs_from_vcf.tsv

    # add header to tsv
    echo -e "GENE\tGNOMAD_ID\tRS\tGT" | cat - rs_from_vcf.tsv > rs_vcf.tsv

    # Prepare sample-info.json
    bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                      --timezoneDifference ${timezoneDifference} \
                                                      --sampleID ${sample_id}

    # Create report from template                                              
    python3 /intelliseqtools/reports/templates/script/report-pgx.py --input-template "/resources/pgx-template.docx" \
                                                                    --input-recommendations ${recommendations} \
                                                                    --input-tsv-from-vcf "rs_vcf.tsv" \
                                                                    --input-sample-info-json "sample_info.json" \
                                                                    --drug-order "${sep = ";" drug_order}" \
                                                                    --output-filename "outputs/${sample_id}-${output_name}.docx"

    # Convert docx to pdf
    soffice --headless \
            --convert-to pdf outputs/${sample_id}-${output_name}.docx \
            --outdir outputs/

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File pgx_report_pdf = "outputs/${sample_id}-${output_name}.pdf"
    File pgx_report_docx = "outputs/${sample_id}-${output_name}.docx"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
