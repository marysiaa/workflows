workflow vcf_uniq_workflow {

  meta {
    keywords: '{"keywords": ["gvcf uniq records"]}'
    name: 'vcf_uniq'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Check for duplicated records in gvcf'
    changes: '{"1.1.1": "gvcf name change", "1.1.0": "indexing before checking for duplicated records "}'

    input_vcf_gz: '{"name": "Input gvcf", "type": "String", "description": "gVCF file to be checked for duplicated records"}'

    output_gvcf_uniq_gz: '{"name": "Uniq gvcf", "type": "File", "copy": "True", "description": "gVCF checked for duplicated records"}'
    output_gvcf_uniq_gz_tbi: '{"name": "Uniq gvcf tabix index", "type": "File", "copy": "True", "description": "Index for gVCF checked for duplicated records"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_uniq

}

task vcf_uniq {

  File gvcf_gz
#  String basename = basename(gvcf_gz)
  String sample_id = "no_id_provided"

  String task_name = "vcf_uniq"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-uniq:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    ln -s ${gvcf_gz} tmp.vcf.gz
    tabix -p vcf tmp.vcf.gz
    vcfuniq tmp.vcf.gz | bgzip > ${sample_id}.g.vcf.gz
    tabix -p vcf ${sample_id}.g.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File gvcf_uniq_gz = "${sample_id}.g.vcf.gz"
    File gvcf_uniq_gz_tbi = "${sample_id}.g.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
