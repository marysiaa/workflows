workflow rna_seq_most_occur_stranded_workflow
{
  call rna_seq_most_occur_stranded
}

task rna_seq_most_occur_stranded {

  Array[String] stranded_array
  String dollar = "$"

  String task_name = "rna_seq_most_occur_stranded"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.5"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    stranded_array=(${sep=" " stranded_array})
    printf '%s\n' "${dollar}{stranded_array[@]}" | sort | uniq -c | sort -k1,1nr -k2 | awk '{print $2; exit}' > most_occurence.txt

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    String stranded = read_lines("most_occurence.txt")[0]

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
