workflow vcf_select_var_somatic_workflow
{
  call vcf_select_var_somatic
}

task vcf_select_var_somatic {

  String task_name = "vcf_select_var_somatic"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-select-var-somatic:1.0.0"

  File vcf
  File tbi
  String sample_id = "no_id_provided"
  String oncogenes_list = "/resources/oncogenes/*/oncogenes.txt"
  String tumor_sample_name = "tumor"
  Boolean target = false ##set true for targeted analysis, this will turn off filtering on oncogenes


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    
    python3 /intelliseqtools/vcf_filter_somatic.py \
        --input_vcf ${vcf} \
        --output_vcf ${sample_id}_filtered.vcf.gz \
        --oncogenes /${oncogenes_list} \
        --sample ${tumor_sample_name} \
        ${true="--target_analysis" false="" target}

    tabix -p vcf ${sample_id}_filtered.vcf.gz    


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File filtered_vcf = "${sample_id}_filtered.vcf.gz"
    File filtered_vcf_tbi = "${sample_id}_filtered.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"



  }

}
