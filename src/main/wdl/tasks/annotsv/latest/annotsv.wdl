workflow annotsv_workflow {

  meta {
    name: 'annotsv'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2020 Intelliseq'
    description: '## annotsv \n Annotates bgzipped VCF file containing structural variants information with the use of AnnotSV v2.3. Note to developers: some rarely used options (candidateGenesFile, -candidateGenesFiltering, -candidateSnvIndelFiles, -candidateSnvIndelsamples, -snvIndelFiles, -txFile) are not implemented, however, if the need arises their implementation should be rather straight-forward.'
    changes: '{latest: "no changes"}'

    input_sv_vcf_gz: '{"name": "Structural variants VCF (bgzipped)", "type": "File", "extension": [".vcf.gz"], "description": "Bgzipped VCF file containing structural variants information"}'
    input_sv_vcf_gz_tbi: '{"name": "Structural variants VCF (bgzipped) index", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Tabix index of bgzipped VCF file containing structural variants information"}'
    input_sv_vcf_basename: '{"name": "Files basename", "type": "String", "description": "Basename of output files"}'
    input_reference_genome:  '{"name": "Reference genome", "type": "String", "values": ["GRCh38"], "default": "GRCh38", "description": "Reference genome"}'

    input_hpo_terms_file:  '{"name": "HPO terms file", "type": "File", "required": "false", "description": "Flat text file containing HPO terms list describing the phenotype of the individual being investigated. Format: one HPO term per line."}'

    input_rank_output:  '{"name": "Rank output", "type": "Boolean", "default": "true", "description": "Save in an output file the decisions that explain the ranking of each SV."}'
    input_min_total_number:  '{"name": "Minimum number of individuals tested", "type": "Int", "min": "100", "max": "1000", "default": "500", "description": "Minimum number of individuals tested to consider a benign SV for the ranking"}'
    input_minimum_overlap:  '{"name": "Minimum overlap beetween SV and features", "type": "Int", "min": "0", "max": "100", "default": "70", "description": "Minimum overlap (%) between the features (DGV, DDD, promoter, TAD...) and the annotated SV to be reported."}'
    input_promoter_size:  '{"name": "Promoter size", "type": "Int", "min": "0", "default": "500", "description": "Number of bases upstream from the transcription start site."}'
    input_sv_minimum_size:  '{"name": "Minimum SV size", "type": "Int", "min": "0", "default": "50", "description": "SV minimum size (in bp)."}'
    input_type_of_annotation:  '{"name": "Type of annotation", "type": "String", "values": ["both", "full", "split"],  "default": "both", "description": "Description of the types of lines produced by AnnotSV."}'

    input_size_of_vcf_chunk: '{"name": "(Advanced) Size of VCF chunk", "type": "String" , "default": "200M", "description": "(Advanced) AnnotSV is able to proccess only VCFs with limited size, therefore the input VCF must be divided into smaller VCF chunks before annotating it with AnnotSV. If the task fails with error - max size for a Tcl value (2147483647 bytes) exceeded or - try to set the lower size. The size argument is an integer and optional unit (example: 10K is 10*1024). Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000)."}'

    output_annotated_with_annotsv_vcf_gz: '{"name": "Structural variants VCF (bgzipped) annotated with AnnotSV", "type": "File", "description": "Structural variants VCF (bgzipped) annotated with AnnotSV."}'
    output_annotated_with_annotsv_vcf_gz_tbi: '{"name": "Index of annotated VCF", "type": "File", "description": "Tabix index of structural variants VCF (bgzipped) annotated with AnnotSV."}'
    output_annotated_with_annotsv_vcf_gz: '{"name": "AnnotSV TSV OUTPUT (gzipped)", "type": "File", "description": "Structural variants TSV (bgzipped) - output of AnnotSV."}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call annotsv

}

task annotsv {

  File sv_vcf_gz
  File sv_vcf_gz_tbi

  String sv_vcf_basename

  # Inputs with defaults
  String reference_genome = "GRCh38"

  File? hpo_terms_file
  Boolean is_hpo_terms_file_provided = defined(hpo_terms_file)

  Boolean rank_output = true
  Int min_total_number = 500
  Int minimum_overlap = 70
  Int promoter_size = 500
  Int sv_minimum_size = 50
  String type_of_annotation = "both"

  String size_of_vcf_chunk = "200M"
  Int max_no_jobs_in_parallel = 8

  # Tools runtime settings, paths etc.
  String annotsv_commit = "commit-b5a65c1ddd71d24547f8eab521925f98ece10df4"
  String annotsv_tsv_to_vcf_py = "/intelliseqtools/annotsv-tsv-to-vcf.py"
  String annotsv_env_path = "/tools/AnnotSV/" + annotsv_commit
  String annotsv_path = "/tools/AnnotSV/" + annotsv_commit + "/bin/AnnotSV"
  String annotations_description_file_path = "/resources/annotsv-annotations-description/1.0.0/annotsv-annotations-description.csv"

  String ref_fasta_gz = "/tools/AnnotSV/" + annotsv_commit + "/share/AnnotSV/Annotations_Human/BreakpointsAnnotations/GCcontent/" + reference_genome + "/" + reference_genome + "_chromFa.fasta.gz"
  String phenotype_h2_db_gz = "/tools/AnnotSV/" + annotsv_commit + "/share/AnnotSV/Annotations_Exomiser/1902/1902_phenotype/1902_phenotype.h2.db.gz"

  String task_name = "annotsv"
  String task_version = "latest"
  String docker_image = "intelliseqngs/annotsv:" + reference_genome + "_1.0.0"

  command <<<
  task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/after-start.sh)

    # If HPO term file is provided - create hpo command
    if ${is_hpo_terms_file_provided }; then
      hpo_command=`(cat ${hpo_terms_file} | awk 'BEGIN{ORS=","} { gsub(/^[ \t]+|[ \t]+$/, ""); print }' | sed 's/,\s*$//')`
    fi

    # Unbgzip reference genome and
    printf "unbgzipping reference genome fasta...\n"
    gunzip ${ref_fasta_gz}

    # Ungzip 1902_phenotype.h2.db.gz (Exomiser database)
    printf "unzipping Exomiser database... \n"
    gunzip ${phenotype_h2_db_gz}
    export ANNOTSV=${annotsv_env_path}

    # Split input VCF into chunks not exceding 2GB
    printf "spliting input SV VCF into chunks... \n"
    zcat ${sv_vcf_gz} | awk '{if (/^#/) {print} else {exit}}' > header
    zcat ${sv_vcf_gz} | grep -v "^#" | split -C ${size_of_vcf_chunk} -d --additional-suffix="-chunk.vcf" --filter=' cat header - | bgzip > $FILE.gz'

    # Annotate chunks with AnnotSV
    printf "annotating chunks with AnnotSV and GNU parallel... \n"
    index=0
    indices=""
    for chunk_vcf_gz in x*-chunk.vcf.gz; do
      mkdir "$index"-chunk
      mv "$chunk_vcf_gz" "$index"-chunk/"$index".vcf.gz
      indices="$indices"" ""$index"
      index=$((index + 1))
    done

    parallel --keep-order -j ${max_no_jobs_in_parallel} \
      ${annotsv_path} \
        -genomeBuild ${reference_genome} \
        ${true='-hpo ' false='' is_hpo_terms_file_provided}"$hpo_command" \
        -rankOutput ${true='yes' false='no' rank_output} \
        -minTotalNumber ${min_total_number} \
        -overlap ${minimum_overlap} \
        -promoterSize ${promoter_size} \
        -SVminSize ${sv_minimum_size} \
        -typeOfAnnotation ${type_of_annotation} \
        -outputDir {1}-chunk/ \
        -SvinputFile {1}-chunk/{1}.vcf.gz \
        -outputFile {1}.annotated-with-annotsv.tsv \
    ::: $indices 2> annotsv.stderr > annotsv.stdout

    cat annotsv.stderr >&2
    cat annotsv.stdout

    annotsv_stdout_err=`(cat annotsv.stdout | grep "couldn't fork child process: not enough memory" | wc -l)`
    annotsv_stderr_err=`(cat annotsv.stderr | grep "max size for a Tcl value (2147483647 bytes) exceeded" | wc -l)`
    annotsv_err=$((annotsv_stdout_err + annotsv_stderr_err))

    if [ $annotsv_err -eq 0 ]; then

      # Add 'Ranking decision' column to AnnotSV TSV outputs
      if ${rank_output}; then

        function add_ranking_decision_column_to_annotsv_tsv() {
          index=$1
          cat "$index"-chunk/"$index".ranking-with-annotsv.tsv \
          | cut -f 5 \
          | paste "$index"-chunk/"$index".annotated-with-annotsv.tsv \
          > "$index"-chunk/tmp
          mv "$index"-chunk/tmp "$index"-chunk/"$index".annotated-with-annotsv.tsv
        }
        export -f add_ranking_decision_column_to_annotsv_tsv

        printf "adding 'Ranking decision' column to AnnotSV TSV outputs... \n"
        parallel --keep-order -j ${max_no_jobs_in_parallel} \
          add_ranking_decision_column_to_annotsv_tsv {1} \
        ::: $indices

      fi

      # Convert AnnotSV TSV outputs into VCF
      printf "converting AnnotSV TSV outputs into VCF and GNU parallel... \n"
      function convert_annotsv_tsv_into_vcf() {
        header=$1
        index=$2
        python3 ${annotsv_tsv_to_vcf_py} \
          "$index"-chunk/"$index".annotated-with-annotsv.tsv \
          "$header" \
          ${annotations_description_file_path} \
          | bgzip > "$index"-chunk/"$index".annotated-with-annotsv.vcf.gz
      }
      export -f convert_annotsv_tsv_into_vcf

      parallel --keep-order -j ${max_no_jobs_in_parallel} \
        convert_annotsv_tsv_into_vcf header {1} \
      ::: $indices

      # Concatenate outputs
      printf "concatenating outputs... \n"

      zcat 0-chunk/0.annotated-with-annotsv.vcf.gz | awk '{if (/^#/) {print} else {exit}}' | bgzip > ${sv_vcf_basename}.annotated-with-annotsv.vcf.gz
      cat 0-chunk/0.annotated-with-annotsv.tsv | head -1 | gzip > ${sv_vcf_basename}.annotated-with-annotsv.tsv.gz

      for index in $indices; do
        zcat "$index"-chunk/"$index".annotated-with-annotsv.vcf.gz | grep -v "^#" | bgzip >> ${sv_vcf_basename}.annotated-with-annotsv.vcf.gz
        cat "$index"-chunk/"$index".annotated-with-annotsv.tsv | sed 1d | gzip >> ${sv_vcf_basename}.annotated-with-annotsv.tsv.gz
      done
      zcat ${sv_vcf_gz} | grep -v "^#" | split -C ${size_of_vcf_chunk} -d --additional-suffix="-chunk.vcf" --filter=' cat header - | bgzip > $FILE.gz'

      # Create tabix index to output VCF...
      printf "creating tabix index to output VCF... \n"
      tabix -p vcf ${sv_vcf_basename}.annotated-with-annotsv.vcf.gz

      # Finish
      printf "Finish!\n"

    else

      printf "Task FAILED with errors:\n"
      cat annotsv.stdout | grep "couldn't fork child process: not enough memory" | awk '{print "    ERROR: \""$0"\""}'
      cat annotsv.stderr | grep "max size for a Tcl value (2147483647 bytes) exceeded" | awk '{print "    ERROR: \""$0"\""}'
      exit 1

    fi


  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "1"
    maxRetries: 0

  }

  output {

    File annotated_with_annotsv_tsv_gz = "${sv_vcf_basename}.annotated-with-annotsv.tsv.gz"
    File annotated_with_annotsv_vcf_gz = "${sv_vcf_basename}.annotated-with-annotsv.vcf.gz"
    File annotated_with_annotsv_vcf_gz_tbi = "${sv_vcf_basename}.annotated-with-annotsv.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
