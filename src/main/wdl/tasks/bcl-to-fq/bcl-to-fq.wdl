workflow bcl_to_fq_workflow {

  call bcl_to_fq

}

task bcl_to_fq {

  File raw_data_tar_gz
  File? raw_data_dir
  Boolean is_raw_data_dir_defined = defined(raw_data_dir)
  Int max_reads_per_tile = 1200000
  Boolean paired = false

  String task_name = "bcl_to_fq"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/bcl-to-fq:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if ! ${is_raw_data_dir_defined};then
    tar xvzf ${raw_data_tar_gz}
  fi
  
 # find paths to files:
  SAMPLE="$(find -maxdepth 2 -type f -name "SampleSheet.csv")"
  BASECALLS_DIR="$(find -maxdepth 4 -type d | grep -Z '/Data/Intensities/BaseCalls')"
  RUN_INFO="$(find -maxdepth 2 -type f -name "RunInfo.xml")"
  NIPT_DATA="$(find -maxdepth 2 -type f -name "*_NIPT_RESULTS.csv")"

  INDEX="$(cat $SAMPLE | grep -o index | wc -l)"
  # create samples and barcodes
  if [ $INDEX -eq 1 ]; then
    cat  $SAMPLE | awk 'BEGIN { FS=","; OFS="\t"; print "barcode_name", "barcode_sequence", "library_name" } $1 == "Sample_ID" { ok = 1; next }; ok { print $5,$6,$10 }' > barcodes.tsv
    cat  $SAMPLE | awk 'BEGIN { FS=","; OFS="\t"; print "OUTPUT_PREFIX",         "BARCODE_1" } $1 == "Sample_ID" { ok = 1; next }; ok { print $1, $6 }; END { print "unknown", "N" } ' > samples.tsv
  elif [ $INDEX -eq 2 ]; then
    cat  $SAMPLE | awk 'BEGIN { FS=","; OFS="\t"; print "OUTPUT_PREFIX", "BARCODE_1", "BARCODE_2" } $1 == "Sample_ID" && $6 == "index" && $8 == "index2" { ok = 1; next }; ok { print $1, $6, $8 }; END { print "unknown", "N", "N" } ' > samples.tsv
    cat  $SAMPLE | awk 'BEGIN { FS=","; OFS="\t"; print "barcode_name", "barcode_sequence", "barcode_name_2", "barcode_sequence_2", "library_name" } $1 == "Sample_ID" { ok = 1; next }; ok { print $5, $6, $7, $8, $10 }' >  barcodes.tsv

  fi

  # run tool fqbio to create file with variables from Illumina raw file:
  #java -jar /intelliseqtools/fqbio.jar ExtractIlluminaRunInfo -i $RUN_INFO -o variables.tsv
  java -jar /intelliseqtools/fqbio.jar ExtractIlluminaRunInfo -i $RUN_INFO -o variables.tsv

  # Extract variables from file variables.tsv needed for gatk tools
  FLOWCELL_BARCODE="$(cat variables.tsv | awk 'END{print $2}')"
  MACHINE_NAME="$(cat variables.tsv | awk 'END{print $3}')"
  READ_STRUCTURE="$(cat variables.tsv | awk 'END{print $5}')"
  RUN_BARCODE="$(cat variables.tsv | awk 'END{print $1}')"
  LANE="$(cat variables.tsv | awk 'END{print $6}')"

  # extract barcodes needed for bcl to fastq conversion
  gatk ExtractIlluminaBarcodes \
    --BASECALLS_DIR $BASECALLS_DIR \
    --LANE $LANE \
    --READ_STRUCTURE $READ_STRUCTURE \
    --BARCODE_FILE barcodes.tsv \
    --METRICS_FILE $BASECALLS_DIR/metrics_output.txt

  # convert bcl to fastq
  gatk IlluminaBasecallsToFastq \
    --BASECALLS_DIR $BASECALLS_DIR \
    --LANE $LANE \
    --READ_STRUCTURE $READ_STRUCTURE \
    --MULTIPLEX_PARAMS samples.tsv \
    --RUN_BARCODE $RUN_BARCODE \
    --FLOWCELL_BARCODE $FLOWCELL_BARCODE \
    --MACHINE_NAME $MACHINE_NAME \
    --COMPRESS_OUTPUTS true \
    --MAX_READS_IN_RAM_PER_TILE ${max_reads_per_tile}

  # divide outputs: unknown, fastqs and barcode_fastqs
  mkdir barcodes fastqs_1 fastqs_2 fastqs unknown
  mv *unknown*.fastq.gz unknown/
  mv *barcode*.fastq.gz barcodes/
  if ${paired}; then
    mv *1.fastq.gz fastqs_1/
    mv *2.fastq.gz fastqs_2/
  else
    mv *.fastq.gz fastqs/
  fi

   cp $NIPT_DATA .

  # extract QC metrics from RunCompletionStatus.xml
  RUN_STATUS="$(find -maxdepth 2 -type f -name "RunCompletionStatus.xml")"
  CD="$(cat $RUN_STATUS | grep "ClusterDensity" | sed 's/[^0-9.]//g')"
  CPF="$(cat $RUN_STATUS | grep "ClustersPassingFilter" | sed 's/[^0-9.]//g')"
  jq -n --arg CD "$CD" --arg CPF "$CPF"\
  '{"BCL_RunCompletionStatus": {ClusterDensity: $CD, ClustersPassingFilter: $CPF}}' > bcl_stats.json


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: 1
    maxRetries: 2

  }

  output {

    Array[File] unknown = glob("unknown/*.fastq.gz")
    Array[File] barcode_fastqs= glob("barcodes/*.fastq.gz")

    Array[File] fastqs_1 = glob("fastqs_1/*.fastq.gz")
    Array[File] fastqs_2 = glob("fastqs_2/*.fastq.gz")
    Array[File] fastqs = glob("fastqs/*.fastq.gz")

    File illumina_csv = glob("*_NIPT_RESULTS.csv")[0]
    File bcl_stats = "bcl_stats.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    }

}
