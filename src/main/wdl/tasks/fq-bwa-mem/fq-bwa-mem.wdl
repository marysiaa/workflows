workflow fq_bwa_mem_workflow {

  meta {
    keywords: '{"keywords": ["alignment", "bwa-mem"]}'
    name: 'fq_bwa_mem'
    author: 'https://gitlab.com/MateuszMarynowski, https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Aligns short reads to the reference'
    changes: '{"2.1.0": "works also with unpaired fastq, possible platform choice", "2.0.1": "set -e -o pipefail", "2.0.0": "Samblaster removed, output bam is not sorted, .alt added to hg38 docker (to allow for alt aware alignment for broad hg38 reference)"}'

    input_fastq_1: '{"name": "(bgzipped) FASTQ 1", "type": "File", "extension": [".fq.gz", ".fastq.gz"], "description": "First FASTQ file (bgzipped)."}'
    input_fastq_2: '{"name": "(bgzipped) FASTQ 2", "type": "File", "extension": [".fq.gz", ".fastq.gz"], "description": "Second FASTQ file (bgzipped)."}'
    input_sample_id: '{"name": "Sample ID", "type": "String", "description": "Sample identifier."}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"values": ["hg38", "grch38-no-alt"]}, "description": "Version of the reference genome to align reads to."}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "required": "false", "constraints": {"values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]}, "description": "Restrict reference genome to specified chromosomes (read will be simulated only from this chromosome)."}'
    input_no_threads: '{"name": "(Advanced) Number of threads", "type": "Int", "default": 8, "description": "(Advanced) Number of threads to run BWA MEM."}'
    input_bwa_mem_arguments: '{"name": "(Advanced) BWA MEM arguments", "type": "String", "required": "false", "default":"-K 100000000 -v 3 ", "description": "(Advanced) Overwrite BWA MEM arguments. Arguments \'-t number of threads\' and \'-R read group header line\' cannot be overwritten by this option."}'

    output_bwa_unsorted_bam: '{"name": "BWA unsorted bam", "type": "File", "copy": "True", "description": "BWA-MEM output alignment file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call fq_bwa_mem

}

task fq_bwa_mem {

  File fastq_1
  File? fastq_2
  Boolean paired = if defined(fastq_2) then true else false
  String platform = "Illumina" ## or IONTORRENT

  String sample_id = 'no_id_provided'

  String? chromosome
  String chromosome_defined = select_first([chromosome, ""])
  String reference_genome = "grch38-no-alt" # another option: String reference_genome = "hg38"
  String reference_genome_whole_name = if (reference_genome == "hg38") then "broad-institute-" + reference_genome else reference_genome + "-analysis-set"
  String reference_genome_scope = if defined(chromosome) then reference_genome_whole_name + "-" + chromosome_defined else reference_genome_whole_name

  Int? index

  # Tools runtime settings, paths etc.
  String reference_genome_fasta_gz = "/resources/reference-genomes/" + reference_genome_scope + "/" + reference_genome_scope + ".fa.gz"
  String reference_genome_fasta = "/resources/reference-genomes/" + reference_genome_scope + "/" + reference_genome_scope + ".fa"
  String reference_genome_fasta_fai = "/resources/reference-genomes/" + reference_genome_scope + "/" + reference_genome_scope + ".fa.fai"
  Int no_threads = 8
  String bwa_mem_arguments = "-K 100000000 -v 3 -Y "

  String RG_PL = platform

  String task_name = "fq_bwa_mem"
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String task_version = "2.1.0"
  String docker_image = if defined(chromosome) then "intelliseqngs/bwa-mem-" + reference_genome + "-chr-wise:" + "3.1.0-" + chromosome_defined else "intelliseqngs/bwa-mem-" + reference_genome + ":3.1.0"

  String dollar = "$"

  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if ${paired}; then
      # prepare basename for bam file (intersection of basename1 and basename2)
      basename1=$( basename "${fastq_1}" ); basename2=$( basename "${fastq_2}" )
      if [ $basename1 == $basename2 ]; then
          echo 'ERROR: The filenames are identical.' >/dev/stderr
          exit 1
      fi
      fn=''; n=0; while [[ ${dollar}{basename1:n:1} == ${dollar}{basename2:n:1} ]]; do fn+=${dollar}{basename1:n:1}; ((n++)); done
      filename=${dollar}{fn::-1}

  else
      # prepare basename for bam file (intersection from basename1)
      filename=$( basename ${fastq_1} | sed 's/\.gz$//' | sed 's/\.f.*q$//' | sed 's/IonXpress_//')
  fi

  set -e -o pipefail

  ## check whether files are compressed to use proper programm (grep vs zgrep)
  if echo ${fastq_1} | grep -q '\.gz$';then
      regex_command="zgrep"
  else
      regex_command="grep"
  fi

  if $regex_command -q '^' ${fastq_1}; then

      # unbgzip reference genome...
      gunzip ${reference_genome_fasta_gz}

      # set reading groups
      if [ ${platform} = Illumina ]; then
          $regex_command -m1  '^@' ${fastq_1} | cut -d ':' -f 3,4 | sed 's/:/\./g' > rg_id
      else
           $regex_command -m1  '^@' ${fastq_1} | cut -d ':' -f 1 | sed 's/@//' > rg_id
      fi

      RG_ID=`cat rg_id`
      RG_PU="$RG_ID"".""${sample_id}"
      RG_LB="${sample_id}"".library"
      RG_SM="${sample_id}"
      RG_PL="${RG_PL}"

      # run BWA MEM
      bwa mem \
          -t ${no_threads} \
          -R "@RG\tID:""$RG_ID""\tPU:""$RG_PU""\tPL:${RG_PL}\tLB:""$RG_LB""\tSM:""$RG_SM" \
          ${bwa_mem_arguments} \
          ${reference_genome_fasta} \
          ${fastq_1} ${fastq_2} \
          | bgzip > ${sample_id}-$filename"_unsorted.bam"

  else
      echo 'ERROR: Input files are empty.' >/dev/stderr
      exit 1
  fi

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                          --task-name-with-index ${task_name_with_index} \
                                          --task-version ${task_version} \
                                          --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: "8"
    maxRetries: 2

  }

  output {

    File bwa_unsorted_bam = glob("${sample_id}-*_unsorted.bam")[0]


    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
