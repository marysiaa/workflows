workflow sv_duphold_workflow {

  meta {
    keywords: '{"keywords": ["sv", "depth analysis"]}'
    name: 'sv_duphold'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Adds annotations about coverage depth and heterozygosity level to structural variant vcf'
    changes: '{"1.0.2": "new docker", "1.0.1": "6G memory"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"values": ["hg38", "grch38-no-alt"]}, "description": "Version of the reference genome used in alignment"}'
    input_bam: '{"name": "Bam", "type": "File", "description": "Bam file used for depth analysis"}'
    input_bai: '{"name": "Bai", "type": "File", "description": "Bam index file"}'
    input_svvcf_gz: '{"name": "SV vcf", "type": "File", "description": "Sorted vcf with putative structural variants"}'
    input_svvcf_gz_tbi: '{"name": "SV vcf index", "type": "File", "description": "Putative structural variants vcf index"}'
    input_snpvcf: '{"name": "SNP vcf", "type": "File", "description": "Vcf file with SNP and INDELs, used for heterozygosity analysis"}'

    output_duphold_vcf: '{"name": "Duphold svvcf", "type": "File", "copy": "True", "description": "Structural variant vcf with depth and heterozygosity information added"}'
    output_duphold_tbi: '{"name": "Duphold svvcf index", "type": "File", "copy": "True", "description": "Index for the structural variant vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_duphold

}

task sv_duphold {

  String task_name = "sv_duphold"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  # possible values: hg38, grch38-no-alt
  String reference_genome = "hg38"

  String reference_genome_whole_name = if (reference_genome == "hg38") then "broad-institute-" + reference_genome else reference_genome + "-analysis-set"

  String docker_image = "intelliseqngs/duphold-" + reference_genome + ":1.1.0"

  String sample_id = "no-id"
  File bam
  File bai
  File svvcf_gz
  File svvcf_gz_tbi
  File? snpvcf_gz
  File? snpvcf_gz_tbi

  String het_annot_option = if defined(snpvcf_gz) && defined(snpvcf_gz_tbi) then "--snp " else ""

    command <<<

    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
    set -e -o pipefail


    ref_fasta=$( ls /resources/reference-genomes/*/*.fa )

    if [ ${het_annot_option} != "" ]
    then
        snp_vcf=$( realpath ${snpvcf_gz} )
    else
        snp_vcf=""
    fi

    duphold \
        --vcf ${svvcf_gz} \
        --bam ${bam} \
        --fasta $ref_fasta \
        ${het_annot_option} $snp_vcf \
        --output ${sample_id}_duphold.vcf

    bgzip ${sample_id}_duphold.vcf
    tabix -p vcf ${sample_id}_duphold.vcf.gz

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "6G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File duphold_vcf = "${sample_id}_duphold.vcf.gz"
    File duphold_tbi = "${sample_id}_duphold.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
