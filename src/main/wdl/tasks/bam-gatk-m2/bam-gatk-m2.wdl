workflow bam_gatk_m2_workflow
{
  call bam_gatk_m2
}

task bam_gatk_m2 {

  String task_name = "bam_gatk_m2"
  String task_version = "1.1.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-gatk-m2:1.1.1"

  File? intervals ## grouped intervals from previous task
  File tumor_bam
  File tumor_bai
  File? normal_bam
  File? normal_bai

  ## user panel-of-normals file, if not given GATK resource file is used
  File? custom_pon_file
  File? custom_pon_index
  Boolean is_normal_bam_given = defined(normal_bam)
  String prefix = if defined(index) then index+"-" else ""
  Boolean genotype_germline_sites = true
  Boolean genotype_pon_sites = false
  Boolean ignore_soft_clips = true
  String pcr_indel_model = "CONSERVATIVE" ## or NONE, AGGRESSIVE, HOSTILE
  String sample_id = "no_id_provided"

  ## resources from docker
  String ref_fasta  = "`ls /resources/reference-genomes/*/*.fa`"
  String pon = if defined(custom_pon_file) then "" else "/resources/1000g-panel-of-normals/gatk-best-practices-somatic-hg38/1000g_pon.hg38.vcf.gz"
  String af_vcf = "/resources/gnomad-v2.1/gatk-best-practices-somatic-hg38/af-only-gnomad.hg38.vcf.gz"
  ## this file was created from the af_vcf:
  ## gatk SelectVariants -V af_vcf -select-type SNP -restrict-alleles-to BIALLELIC  -select "AF > 0.05" -O common_snps_vcf --lenient
  String common_snps_vcf = "/resources/gnomad-v2.1/gatk-best-practices-somatic-hg38/common-snps-gnomad.hg38.vcf.gz"
  ## true: run orientation bias filter run_ob_filter = run_ob_filter
  String java_mem = "-Xmx7500m"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    ## get sample names from normal bam
    if ${is_normal_bam_given}; then
        gatk --java-options "${java_mem}" \
            GetSampleName \
            -I ${normal_bam} \
            -O normal_name.txt -encode

        normal_command_line="-normal `cat normal_name.txt`"
    else
        normal_command_line=""
    fi

    ## get sample names from tumor  bam (for report)
    gatk --java-options "${java_mem}" \
        GetSampleName \
        -I ${tumor_bam} \
        -O tumor_name.txt -encode


   gatk --java-options "${java_mem}" Mutect2 \
          -R ${ref_fasta} \
          -I ${tumor_bam} \
           ${"-I " + normal_bam} \
           $normal_command_line \
          --germline-resource ${af_vcf} \
          -pon ${pon} ${custom_pon_file} \
           ${"-L " + intervals} \
          -O ${prefix}${sample_id}_m2.vcf.gz \
          --bam-output ${prefix}${sample_id}_m2.bam \
          --f1r2-tar-gz ${prefix}${sample_id}_f1r2.tar.gz \
           ${true='--genotype-germline-sites' false='' genotype_germline_sites} \
           ${true='--genotype-pon-sites' false='' genotype_pon_sites} \
           --pcr-indel-model ${pcr_indel_model} \
           ${true='--dont-use-soft-clipped-bases' false='' ignore_soft_clips}


   m2_exit_code=$?

   ### GetPileupSummaries

   # If the variants for contamination and the intervals for this scatter don't intersect, GetPileupSummaries
   # throws an error.  However, there is nothing wrong with an empty intersection for our purposes; it simply doesn't
   # contribute to the merged pileup summaries that we create downstream.  We implement this by with array outputs.
   # If the tool errors, no table is created and the glob yields an empty array.

   set +e

   gatk --java-options "${java_mem}" GetPileupSummaries \
         -R ${ref_fasta} \
         -I ${tumor_bam} \
         --interval-set-rule INTERSECTION \
         ${"-L " + intervals} \
         -V ${common_snps_vcf} \
         -L ${common_snps_vcf} \
         -O ${prefix}${sample_id}_tumor-pileups.table



   if ${is_normal_bam_given}; then
         gatk --java-options "${java_mem}" GetPileupSummaries \
         -R ${ref_fasta} \
         -I ${normal_bam} \
         --interval-set-rule INTERSECTION \
         ${"-L " + intervals} \
         -V ${common_snps_vcf} \
         -L ${common_snps_vcf} \
         -O ${prefix}${sample_id}_normal-pileups.table
   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}

   # the script only fails if Mutect2 itself fails
   exit $m2_exit_code

  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File unfiltered_vcf = "${prefix}${sample_id}_m2.vcf.gz"
    File unfiltered_vcf_tbi = "${prefix}${sample_id}_m2.vcf.gz.tbi"
    File bamOut = "${prefix}${sample_id}_m2.bam"
    File bamOut_bai = "${prefix}${sample_id}_m2.bai"

    File stats = "${prefix}${sample_id}_m2.vcf.gz.stats"
    File f1r2_counts = "${prefix}${sample_id}_f1r2.tar.gz"
    Array[File] tumor_pileups = glob("*tumor-pileups.table")
    Array[File] normal_pileups = glob("*normal-pileups.table") ## may be empty
    String tumor_sample_name = read_string("tumor_name.txt")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
