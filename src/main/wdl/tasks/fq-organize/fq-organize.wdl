workflow fq_organize_workflow {

  call fq_organize

}

task fq_organize {

  String task_name = "fq_organize"
  String task_version = "2.3.0"
  Int? index
  Int number_of_reads_per_file = 40000000
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_fq-organize:2.1.2"

  Array[File] fastqs
  Boolean paired
  Boolean split_files = true
  Boolean concat_files = false
  Array[String] possible_left_suffixes = ['1.fq', '1.fq.gz', '1.fastq','1.fastq.gz', 'R1_001.fastq', 'R1_001.fastq.gz', 'R1_001.fq', 'R1_001.fq.gz', '1.clean.fq','1.clean.fq.gz', '1_merged.fastq.gz', '1_merged.fq.gz']
  Array[String] possible_right_suffixes = ['2.fq', '2.fq.gz', '2.fastq','2.fastq.gz', 'R2_001.fastq','R2_001.fastq.gz', 'R2_001.fq','R2_001.fq.gz', '2.clean.fq' ,'2.clean.fq.gz', '2_merged.fastq.gz', '2_merged.fq.gz']
  Array[String] possible_unpaired_suffixes = ['fastq', 'fq', 'clean.fq', 'fq.gz', 'fastq.gz', 'clean.fq.gz', 'merged.fastq.gz', 'merged.fq.gz']
  Int num_of_processes = 4
  Int input_len = length(fastqs)
  Int input_size = if input_len >0 then ceil(size(fastqs[0], "GB")) else 1
  Float disk_size_multiplier = 1 # modify this value if disk size was not enough
  Int disk_size = round(disk_size_multiplier * (num_of_processes * input_len * input_size)) + 100
  String memory_string = if split_files then "62G" else "32G"

  String dollar = "$"
  String user_error_msg = "The uploaded fastq file/files seem to be incorrect. The most common reasons are: 1. the fastq files are corrupted; 2. the fastq files contain duplicated sequence; 3. the fastq files are incorrectly formatted; 4. the paired fastq files contain unpaired reads."

  command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
    
    if echo ${sep=" " fastqs} | grep -q '\.gz'; then
        concat=zcat
    else
        concat=cat    
    fi

    function split_files {
      if echo ${sep=" " fastqs} | grep -q '\.gz';then
          #echo `find ../$1 -name "*gz"`
          find ../$1 -name "*gz" | xargs -n 1 -P ${num_of_processes} bash -c \
          'zcat $0 | split -l ${number_of_reads_per_file} -e --additional-suffix=_1_`basename $0` --numeric-suffixes=1 --suffix-length=5'
      else
         find ../$1 -name "*f*q" | xargs -n 1 -P ${num_of_processes} bash -c \
         'cat $0 | split -l ${number_of_reads_per_file} -e --additional-suffix=_1_`basename $0` --numeric-suffixes=1 --suffix-length=5'
      fi
      for f in *f*q*; do
        mv $f `echo $f | sed 's/^x//' | sed 's/.gz${dollar}//'`
      done
      ls | xargs -i -P ${num_of_processes} -n 1 bash -c 'gzip -4 {}'
    }
    export -f split_files

    mkdir fastqs_1
    mkdir fastqs_2
    mkdir fastqs_1_split
    mkdir fastqs_2_split
    mkdir fastqs_1_concat
    mkdir fastqs_2_concat

    error_msg="Task failed with error code 86: ${user_error_msg};"

    if ${paired} ; then
      echo "Making pairs"
      python /intelliseqtools/make_pairs.py \
                    --fastqs ${sep=' ' fastqs} \
                    --outdir1 fastqs_1 \
                    --outdir2 fastqs_2 \
                    --possible_left_suffixes ${sep=' ' possible_left_suffixes} \
                    --possible_right_suffixes ${sep=' ' possible_right_suffixes}

      python /intelliseqtools/get_sample_names.py \
                    --fastqs fastqs_1/* \
                    --possible_suffixes ${sep=' ' possible_left_suffixes} > samples_ids

      fastqs_1_arr=(fastqs_1/*)
      fastqs_2_arr=(fastqs_2/*)
      files_number=${dollar}{#fastqs_1_arr[@]}

      for ((i=0;i<$files_number;i++)); do
        /fastq_utils-0.25.1/src/fastq_info ${dollar}{fastqs_1_arr[i]} ${dollar}{fastqs_2_arr[i]} || \
          echo "$error_msg" >&2 | exit 86
      done


    else
      for f in ${sep=" " fastqs}; do
        fname=$( basename $f )
        ln $f fastqs_1/"${dollar}{fname/fastq/fq}" # change suffix to .fq.gz
        /fastq_utils-0.25.1/src/fastq_info fastqs_1/"${dollar}{fname/fastq/fq}"  || \
          echo "$error_msg" >&2 | exit 86
      done
      python /intelliseqtools/get_sample_names.py \
                    --fastqs ${sep=' ' fastqs} \
                    --possible_suffixes ${sep=' ' possible_unpaired_suffixes} > samples_ids
    fi

    if ${split_files} ; then
      echo "Splitting files from fastqs_1"
      cd fastqs_1_split
       split_files fastqs_1
      cd ..

      if ${paired} ; then
        echo "Splitting files from fastqs_2"
        cd fastqs_2_split
        split_files fastqs_2
        cd ..
      fi
    
    elif ${concat_files} && [[ `ls fastqs_1/*.f*q* | wc -w` -gt 1 ]]; then
      echo "Concatenating files from fastqs_1"
      cd fastqs_1
      $concat *.f*q* | gzip -4 > ../fastqs_1_concat/concatenated_1.fq.gz
      cd ..
      ln fastqs_1_concat/* fastqs_1_split
      if ${paired} ; then
        echo "Concatenating files from fastqs_2"
        cd fastqs_2
        $concat *.f*q* | gzip -4 > ../fastqs_2_concat/concatenated_2.fq.gz
        cd ..
        ln fastqs_2_concat/* fastqs_2_split
      fi
         
    else
      ln fastqs_1/* fastqs_1_split
      if ${paired} ; then
        ln fastqs_2/* fastqs_2_split
      fi
    fi

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: memory_string
    cpu: num_of_processes
    maxRetries: 1
    disks: "local-disk " + disk_size + " LOCAL"
  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    Array[File] fastqs_1 = glob("fastqs_1_split/*.f*q*")
    Array[File]? fastqs_2 = glob("fastqs_2_split/*.f*q*")
    Array[String] samples_ids = read_lines("samples_ids")

  }

}
