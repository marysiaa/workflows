workflow report_bco_workflow
{
  call report_bco
}

task report_bco {

  File bco_json
  File? sample_info_json

  Int timezoneDifference = 0

  Boolean is_rna_seq = false
  String template = if is_rna_seq then "/resources/bco-template-rna-seq.docx" else "/resources/bco-template.docx"
  String output_name = "report-bco"
  String sample_id = "no_id_provided"

  String task_name = "report_bco"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_bco:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir outputs/

    # Prepare sample-info.json
    bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                      --timezoneDifference ${timezoneDifference} \
                                                      --sampleID "${sample_id}"

    python3 /intelliseqtools/reports/templates/script/report-bco.py \
            --input-template ${template} \
            --input-sample-info-json "sample_info.json" \
            --input-bco-json "${bco_json}" \
            --is-rna-seq ${is_rna_seq} \
            --output-filename "outputs/${sample_id}-${output_name}.docx"

    soffice --headless \
            --convert-to pdf outputs/${sample_id}-${output_name}.docx \
            --outdir outputs/

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File bco_report_pdf = "outputs/${sample_id}-${output_name}.pdf"
    File bco_report_docx = "outputs/${sample_id}-${output_name}.docx"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
