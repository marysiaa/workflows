workflow vcf_select_var_workflow
{
  call vcf_select_var
}

task vcf_select_var {

  String task_name = "vcf_select_var"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.5"

  String sample_id = "no_id_provided"
  File vcf_gz
  File vcf_gz_tbi

  ## severity threshold for acmg (variants of that severity+ will be selected)
  ## possible values "benign", "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"
  String acmg_threshold = "uncertain"

  ## severity threshold for ClinVar (variants of that severity+ will be selected)
  ## possible values "pathogenic", "likely_pathogenic", "uncertain"
  String clinvar_threshold = "likely_pathogenic"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   if [ ${acmg_threshold} = "pathogenic" ];then
      acmg_string="Pathogenic"
   elif [ ${acmg_threshold} = "likely_pathogenic" ];then
      acmg_string="Pathogenic,Likely^Pathogenic"
   elif [ ${acmg_threshold} = "uncertain" ];then
      acmg_string="Pathogenic,Likely^Pathogenic,Uncertain"
   elif [ ${acmg_threshold} = "likely_benign" ];then
      acmg_string="Pathogenic,Likely^Pathogenic,Uncertain,Likely^benign"
   else
      acmg_string="Pathogenic,Likely^Pathogenic,Uncertain,Likely^benign,Benign"
   fi

    if [ ${clinvar_threshold} = "pathogenic" ];then
      clinvar_string="pathogenic,pathogenic_low_penetrance"
   elif [ ${clinvar_threshold} = "likely_pathogenic" ];then
      clinvar_string="pathogenic,pathogenic/likely_pathogenic,likely_pathogenic,pathogenic_low_penetrance,likely_pathogenic_low_penetrance"
   elif [ ${clinvar_threshold} = "uncertain" ];then
      clinvar_string="pathogenic,pathogenic/likely_pathogenic,likely_pathogenic,pathogenic_low_penetrance,likely_pathogenic_low_penetrance,conflicting_interpretations_of_pathogenicity,uncertain_significance"
   fi


   # Tbi could be in different directory than vcf file, the symlink below fixes it
   vcf_dir=$(dirname "${vcf_gz}")
   tbi_name=$(basename "${vcf_gz_tbi}")
   if [ ! -f $vcf_dir/$tbi_name ]; then
     ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
   fi

   ## filter on ACMG and ClinVar significance
   bcftools filter ${vcf_gz} \
     -i "INFO/ISEQ_ACMG_SUMMARY_CLASSIFICATION = \"$acmg_string\" | INFO/ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE = \"$clinvar_string\" " \
     -o ${sample_id}_significance-filterd.vcf.gz -O z

   tabix -p vcf ${sample_id}_significance-filterd.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File filtered_vcf_gz = "${sample_id}_significance-filterd.vcf.gz"
    File filtered_vcf_gz_tbi = "${sample_id}_significance-filterd.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
