workflow mt_realign_workflow {

  meta {
    keywords: '{"keywords": ["mitochondrion", "alignment", "BWA MEM"]}'
    name: 'Alignment to chrM'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Aligns unmapped bam to chrM'
    changes: '{"1.0.2": "optional duplicated reads marking", "1.0.1": "new docker"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_reverted_mt_bam: '{"name": "Reverted mitochondrial bam", "type": "File", "extension": [".bam"], "description": "Sample identifier"}'
    input_shifted_alignment: '{"name": "Shifted alignment", "type": "Boolean", "default": false, "description": "Set as true if reads should be aligned to shifted chrM reference fasta"}'

    output_realigned_mt_bam: '{"name": "Realigned mitochondrial bam", "type": "File", "copy": "True", "description": "Realigned mitochondrial bam"}'
    output_realigned_mt_bai: '{"name": "Realigned mitochondrial bai", "type": "File", "copy": "True", "description": "Index for the realigned mitochondrial bam"}'
    output_metrics_file: '{"name": "Metrics file", "type": "File", "copy": "True", "description": "Duplicate metrics file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call mt_realign

}

task mt_realign {

  String task_name = "mt_realign"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_mt-realign:1.1.0"

  String sample_id = "no_id_provided"
  File reverted_mt_bam
  Boolean shifted_alignment = false
  String broad_resources_version = "v0"
  String reference_path = "/resources/broad-institute-hg38-mt-reference/"+ broad_resources_version
  String align_type = if (shifted_alignment) then "-shifted" else ""

  String bwa_options = "-K 100000000 -p -v 3 -t 2 -Y"
  Boolean mark_dup = true
  String sam_to_fastq_java_options = "-Xms5000m"
  String java_options =  "-Xms4000m"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   if ${shifted_alignment};then
       mt_fasta="${reference_path}/Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta"
   else
       mt_fasta="${reference_path}/Homo_sapiens_assembly38.chrM.fasta"
   fi

    touch ${sample_id}_mt${align_type}-dup-metrics.txt

    gatk --java-options "${sam_to_fastq_java_options}" \
      SamToFastq \
      -I ${reverted_mt_bam} \
      --FASTQ /dev/stdout \
      --INTERLEAVE true \
      --NON_PF true | \
    bwa mem ${bwa_options} $mt_fasta /dev/stdin | \
    gatk --java-options "${java_options}" \
      MergeBamAlignment \
      --VALIDATION_STRINGENCY SILENT \
      --EXPECTED_ORIENTATIONS FR \
      --ATTRIBUTES_TO_RETAIN X0 \
      --ATTRIBUTES_TO_REMOVE MD \
      --ALIGNED_BAM /dev/stdin \
      --UNMAPPED_BAM ${reverted_mt_bam} \
      --OUTPUT mt${align_type}.bam \
      --REFERENCE_SEQUENCE "$mt_fasta" \
      --SORT_ORDER "unsorted" \
      --IS_BISULFITE_SEQUENCE false \
      --ALIGNED_READS_ONLY false \
      --CLIP_ADAPTERS false \
      --MAX_RECORDS_IN_RAM 2000000 \
      --ADD_MATE_CIGAR true \
      --MAX_INSERTIONS_OR_DELETIONS -1 \
      --PRIMARY_ALIGNMENT_STRATEGY MostDistant \
      --UNMAPPED_READ_STRATEGY COPY_TO_TAG \
      --ALIGNER_PROPER_PAIR_FLAGS true \
      --UNMAP_CONTAMINANT_READS true \
      --ADD_PG_TAG_TO_READS false


    if ${mark_dup}; then
        gatk --java-options "${java_options}" \
            MarkDuplicates \
            -I mt${align_type}.bam \
            -O mt${align_type}-markdup.bam \
            --METRICS_FILE ${sample_id}_mt${align_type}-dup-metrics.txt  \
            --VALIDATION_STRINGENCY SILENT \
            --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
            --ASSUME_SORT_ORDER "queryname" \
            --CLEAR_DT false \
            --ADD_PG_TAG_TO_READS false
    else
        mv mt${align_type}.bam mt${align_type}-markdup.bam
    fi          


    gatk --java-options ${java_options} \
      SortSam \
      -I mt${align_type}-markdup.bam \
      -O ${sample_id}_mt${align_type}.bam \
      --SORT_ORDER "coordinate" \
      --CREATE_INDEX true \
      --MAX_RECORDS_IN_RAM 300000


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "6G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File realigned_mt_bam = "${sample_id}_mt${align_type}.bam"
    File realigned_mt_bai = "${sample_id}_mt${align_type}.bai"
    File metrics_file = "${sample_id}_mt${align_type}-dup-metrics.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
