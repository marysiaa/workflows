workflow microarray_qc_plots_workflow
{
  call microarray_qc_plots
}

task microarray_qc_plots {

  File vcf_gz
  String id = "no_id_provided"

  String task_name = "microarray_qc_plots"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_microarray-qc-plots:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   zcat ${vcf_gz} | grep -v ^## | sed 's/^#//' > in.vcf

   Rscript /intelliseqtools/microarray-qc-plots.R \
      --vcf "in.vcf" \
      --id ${id}


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File qc_plots = "${id}-plots.pdf"
    File qc_xmlx = "${id}-stats_quantiles.xlsx"
    File gentrain_score_json = "${id}-gentrain_score.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
