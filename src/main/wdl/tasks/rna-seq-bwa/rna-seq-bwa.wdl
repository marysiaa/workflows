workflow rna_seq_bwa_workflow {

  meta {

    keywords: '{"keywords": ["alignment"]}'
    name: 'rna_seq_bwa'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Alignment with bwa'
    changes: '{"1.0.3": "updating to a new template"}'

    input_fastq_1: '{"name": "fastq_1", "type": "File", "description": "Fastq file 1"}'
    input_ref_genome_index: '{"name": "ref_genome_index", "type": "Array[File]", "description": "reference genome indexed"}'

    output_bam_file: '{"name": "bam_file", "type": "File", "copy": "True", "description": "bam file"}'
    output_bam_bai_file: '{"name": "bam_bai_file", "type": "File", "copy": "True", "description": "bam bai file"}'
    output_stats: '{"name": "stats", "type": "File", "copy": "True", "description": "stats file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_bwa

}

task rna_seq_bwa {

  File fastq_1
  Array[File] ref_genome_index
  String genome_basename = "no_basename"

  Int num_cpu = 4
  String sample_id = "no_id_provided"

  String task_name = "rna_seq_bwa"
  String task_version = "1.0.4"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/rna-bwa:1.1.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ${genome_basename}
    cp ${sep=" " ref_genome_index} ${genome_basename}

    bwa aln -t 20 ${genome_basename}/${genome_basename}.fa ${fastq_1} > ${sample_id}.sai
    bwa samse ${genome_basename}/${genome_basename}.fa ${sample_id}.sai ${fastq_1} | samtools sort -O bam -@ 10 > ${sample_id}.bam
    samtools index ${sample_id}.bam
    samtools flagstat ${sample_id}.bam > ${sample_id}.stats

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: num_cpu
    maxRetries: 2

  }

  output {

    File bam_file = "${sample_id}.bam"
    File bam_bai_file = "${sample_id}.bam.bai"
    File stats = "${sample_id}.stats"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
