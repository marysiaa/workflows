workflow vcf_anno_scores_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "scores"]}'
    name: 'Vcf annotation with conservation scores'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Annotates vcf with conservation scores: M_CAP, GERP, SIFT4G, Phylop100way, PhastCoins100way'
    changes: '{"1.1.1": "new docker", "1.1.0": "latest dir removed, set-eo pipeline added"}'

    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file"}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index file"}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID"}'
    input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]}, "description": "Chromosome"}'

    output_annotated_with_scores_vcf_gz : '{"name": "Annotated with scores vcf", "type": "File", "description": "Vcf file annotated with conservation scores"}'
    output_annotated_with_scores_vcf_gz_tbi : '{"name": "Annotated with scores vcf tbi", "type": "File", "description": "Index file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_scores

}

task vcf_anno_scores {

  String task_name = "vcf_anno_scores"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  ## NOTES:
  ## Input VCF file must:
  ## 1) be bgzipped
  ## 2) have left-normalized indels (bcftools norm)
  ## 3) have multiallelic sites split (bcftools norm --multiallelics -any)

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename="no_id_provided"

  String chromosome
  String docker_image = "intelliseqngs/task_vcf-anno-scores:1.2.0-" + chromosome

  String sort_info_columns_in_vcf_py = "/intelliseqtools/sort-info-columns-in-vcf.py"

  String vcfbigwig_java_mem = "-Xmx4g"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if zgrep -q -v "^#" ${vcf_gz}; then
      all_vcf_scores_vcf_gz=$( ls /resources/*/*/${chromosome}.all-vcf-scores.vcf.gz )
      gerp_bw=$( ls /resources/*/*/${chromosome}.gerp.bw )
      phastcons_100way_bw=$( ls /resources/*/*//${chromosome}.phastcons-100-way.bw )
      phylop_100way_bw=$( ls /resources/*/*/${chromosome}.phylop-100-way.bw )
      vcfbigwig_jar=$( ls /tools/jvarkit/*/jvarkit/dist/vcfbigwig.jar )

      # annotate with all-scores
      bcftools annotate --columns INFO --annotation $all_vcf_scores_vcf_gz ${vcf_gz} \
          | awk '{if($1 ~/^##/) {print > "header"; print $0} else {print $0}}' \
          | sed 's/,Source.*>/>/g' | sed 's/,Version.*>/>/g' | (printf "##fileformat=VCFv4.0\n"; grep -v "##fileformat") \
          | bgzip > ${chromosome}-${vcf_basename}_annotated-with-all-vcf-scores.vcf.gz

      sed -i 's/Source=SIFT4G/Source=\"SIFT4G\"/' header
      sed -i 's/Source=M-CAP/Source=\"M-CAP\"/' header

      # GERP++, phastCons100way and PhyloP100way
      printf '##INFO=<ID=ISEQ_GERP,Number=A,Type=Float,Description="GERP++ score",Source="GERP++",Version="GERP++, lifted over with CrossMap.py v0.3.7">\n' >> header
      printf '##INFO=<ID=PhastCons100way,Number=A,Type=Float,Description="PhastCons100way conservation score",Source="PhastCons100way",Version="PhastCons100way">\n' >> header
      printf '##INFO=<ID=PhyloP100way,Number=A,Type=Float,Description="PhyloP100way score",Source="PhyloP100way",Version="PhyloP100way">\n' >> header
      zgrep -m1 "^#C" ${vcf_gz}  >> header

      java ${vcfbigwig_java_mem} -jar $vcfbigwig_jar -B $gerp_bw -T ISEQ_GERP ${chromosome}-${vcf_basename}_annotated-with-all-vcf-scores.vcf.gz | bgzip > ${chromosome}-${vcf_basename}_gerp.vcf.gz
      java ${vcfbigwig_java_mem} -jar $vcfbigwig_jar -B $phastcons_100way_bw -T PhastCons100way ${chromosome}-${vcf_basename}_gerp.vcf.gz | bgzip > ${chromosome}-${vcf_basename}_phastcons.vcf.gz
      java ${vcfbigwig_java_mem} -jar $vcfbigwig_jar -B $phylop_100way_bw -T PhyloP100way ${chromosome}-${vcf_basename}_phastcons.vcf.gz \
        | grep -v "^#" | cat header - | python3 ${sort_info_columns_in_vcf_py} | bgzip > ${chromosome}-${vcf_basename}_annotated-with-scores.vcf.gz

  else

      zcat ${vcf_gz} | grep '^##' > header
      printf '##INFO=<ID=ISEQ_M_CAP,Number=A,Type=Float,Description="M-CAP score",Source="M-CAP",Version="v1.4, lifted over to hg38 with CrossMap v0.3.7">\n' >> header
      printf '##INFO=<ID=ISEQ_M_CAP_Sensitivity,Number=A,Type=Float,Description="Interpretable M-CAP sensitivity score. Sensitivity score ≤ 0.95 is possibly pathogenic.",Source="M-CAP",Version="v1.4, lifted over to hg38 with CrossMap v0.3.7">\n' >> header
      printf '##INFO=<ID=ISEQ_SIFT4G_MIN,Number=A,Type=Float,Description="Minimal SIFT4G score for a variant",Source="SIFT4G",Version="SIFT4G, GRCh38.83">\n' >> header
      printf '##INFO=<ID=ISEQ_SIFT4G,Number=A,Type=String,Description="SIFT4G score annotations. Format: Transcript_Id | Gene_Name | Region | HGVS.p | SIFT_score : (...) : Transcript_Id | Gene_Name | Region | HGVS.p | SIFT_score",Source="SIFT4G",Version="SIFT4G, GRCh38.83">\n' >> header
      printf '##INFO=<ID=ISEQ_GERP,Number=A,Type=Float,Description="GERP++ score",Source="GERP++",Version="GERP++, lifted over with CrossMap.py v0.3.7">\n' >> header
      printf '##INFO=<ID=PhastCons100way,Number=A,Type=Float,Description="PhastCons100way conservation score",Source="PhastCons100way",Version="PhastCons100way">\n' >> header
      printf '##INFO=<ID=PhyloP100way,Number=A,Type=Float,Description="PhyloP100way score",Source="PhyloP100way",Version="PhyloP100way">\n' >> header
      zgrep -m1 "^#C" ${vcf_gz}  >> header
      cat header | bgzip > ${chromosome}-${vcf_basename}_annotated-with-scores.vcf.gz


  fi

  tabix -p vcf ${chromosome}-${vcf_basename}_annotated-with-scores.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File annotated_with_scores_vcf_gz = "${chromosome}-${vcf_basename}_annotated-with-scores.vcf.gz"
    File annotated_with_scores_vcf_gz_tbi = "${chromosome}-${vcf_basename}_annotated-with-scores.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
