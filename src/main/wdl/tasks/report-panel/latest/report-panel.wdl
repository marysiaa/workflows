workflow report_panel_workflow {


  meta {
    keywords: '{"keywords": ["genes", "hpo", "phenotype"]}'
    name: 'report_panel'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task generates table with information about genes udes in analysis, in contetn of pdf, html, docx, odt'
    changes: '{"1.2.5": "new brand report generated from .docx template"}'
    input_sample_id: '{"name": "Sample ID", "type": "String", "description": "Identifier of sample"}'
    input_panel: '{"name": "Panel", "type": "File", "description": "JSON format genes with scores"}'
    input_panel_inputs: '{"name": "Panel inputs", "type": "File", "description": "JSON format inputs provided by user"}'


    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_panel_report_pdf: '{"name": "Panel report PDF", "type": "File", "description": "PDF report created from panel"}'
    output_panel_report_docx: '{"name": "Panel report Docx", "type": "File", "description": "Docx report created from panel"}'

  }

  call report_panel

}

task report_panel {
  String task_name = "report_panel"
  String task_version = "1.2.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_panel:1.0.0"

  File? sample_info_json
  Int timezoneDifference = 0
  String? analysisSpecifier
  String outputs = "outputs"
  File panel_json
  String sample_id = "no_id_provided"
  String output_name = "panel-report"
  File panel_inputs_json

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.0/after-start.sh)

  mkdir ${outputs}

    [ -z "${sample_info_json}" ] && echo "{}" > sample_info.json || cat ${sample_info_json} > sample_info.json

    touch tmp.json
      for i in "name" "surname" "sex" "birthdate" "pesel" "ID" "material" "sequencing_type" "sending_date" "doctor_surname" "doctor_name"
      do
        if [ $(grep -F -c "\""$i"\"" sample_info.json) -eq 0 ] ; then
          echo "\""$i"\": \"\"," >> tmp.json
        fi
      done
      if [ -s tmp.json ] ; then
        sed -i '$s/,//' tmp.json
        sed -i '$s/}//;s/"$/",/' sample_info.json
        cat tmp.json >> sample_info.json
        echo "}"  >> sample_info.json
      fi

    LENGTH_DATE=$(jq '.raport_date | length' sample_info.json)
    if [[ "$LENGTH_DATE" == 0 ]]
    then
        CURRENT_TIME=$(echo $(($(date +%s%N)/1000000)))
        DIFFERENCE=$(((CURRENT_TIME+(${timezoneDifference}))/1000))
        DATE=$(date -d @$DIFFERENCE +"%m/%d/%Y")
        jq -r --arg RAPORT_DATE "$DATE" '.raport_date = $RAPORT_DATE' sample_info.json > sample_info.json.tmp && cat sample_info.json.tmp > sample_info.json
    fi


    LENGTH_ID=$(jq '.ID | length' sample_info.json)
      if ([ ! -z "${analysisSpecifier}" ] && [[ "$LENGTH_ID" == 0 ]])
      then
          jq -r --arg ID "${analysisSpecifier}" '.ID = $ID' sample_info.json > sample_info.json.tmp && cat sample_info.json.tmp > sample_info.json
      fi


    python3 /intelliseqtools/report-panel.py \
        --input-template "/resources/panel-template.docx" \
        --input-sample-info-json "sample_info.json" \
        --input-panel-json "${panel_json}" \
        --input-panel-inputs-json "${panel_inputs_json}" \
        --input-name "${sample_id}-${output_name}" \
        --output-dir "${outputs}"

      soffice --headless \
      --convert-to pdf ${outputs}/${sample_id}-${output_name}.docx \
      --outdir ${outputs}



  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.0/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File panel_report_docx = "${outputs}/${sample_id}-${output_name}.docx"
    File panel_report_pdf = "${outputs}/${sample_id}-${output_name}.pdf"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
  }

}
