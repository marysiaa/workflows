workflow report_panel_workflow {

  meta {
    keywords: '{"keywords": ["genes", "hpo", "phenotype"]}'
    name: 'report_panel'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task generates table with information about genes udes in analysis, in contetn of pdf, html, docx, odt'
    changes: '{"1.4.2": "report update for task string-gp-to-json", "1.4.1": "sort genes", "1.4.0": "change in reports template, docker and script", "1.3.0": "refactored reports", "1.2.6": "rewriting a repetitive part of code into a bash script", "1.2.5": "new brand report generated from .docx template", "1.2.4": "update task to new template"}'

    input_panel: '{"name": "Panel", "type": "File", "description": "JSON format genes with scores"}'
    input_panel_inputs: '{"name": "Panel inputs", "type": "File", "description": "JSON format inputs provided by user"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_panel_report_pdf: '{"name": "Panel report PDF", "type": "File", "description": "PDF report created from panel"}'
    output_panel_report_docx: '{"name": "Panel report Docx", "type": "File", "description": "Docx report created from panel"}'

  }

  call report_panel

}

task report_panel {

  File panel_json
  File panel_inputs_json
  File? sample_info_json

  Int timezoneDifference = 0

  String outputs = "outputs"
  String output_name = "panel-report"
  String sample_id = "no_id_provided"
  String task_name = "report_panel"
  String task_version = "1.4.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_panel:1.1.2"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ${outputs}

    # Prepare sample-info.json
    bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                      --timezoneDifference ${timezoneDifference} \
                                                      --sampleID "${sample_id}"

    python3 /intelliseqtools/reports/templates/script/report-panel.py \
        --input-template "/resources/panel-template.docx" \
        --input-sample-info-json "sample_info.json" \
        --input-panel-json "${panel_json}" \
        --input-panel-inputs-json "${panel_inputs_json}" \
        --input-name "${sample_id}-${output_name}" \
        --output-dir "${outputs}"

      soffice --headless \
      --convert-to pdf ${outputs}/${sample_id}-${output_name}.docx \
      --outdir ${outputs}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File panel_report_docx = "${outputs}/${sample_id}-${output_name}.docx"
    File panel_report_pdf = "${outputs}/${sample_id}-${output_name}.pdf"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
