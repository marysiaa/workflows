workflow vcf_anno_ghr_workflow {

  meta {
    keywords: '{"keywords": ["annotate with ghr"]}'
    name: 'vcf_anno_ghr'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Annotate vcf with disease names derived from Genetics Home Reference'
    changes: '{"1.0.5": "modify script to work also with sv vcf", "1.0.4": "update script", "1.0.3": "Updated resources and bug fix in python script", "1.0.1": "Updated resources and adaptation to the new bco version"}'

    input_vcf_gz: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf tabix index", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'

    output_annotated_with_ghr_vcf_gz: '{"name": "Annotated vcf", "type": "File", "copy": "True", "description": "Vcf annotated with ghr"}'
    output_annotated_with_ghr_vcf_gz_tbi: '{"name": "Annotated vcf tabix index", "type": "File", "copy": "True", "description": "Index for vcf annotated with ghr"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_anno_ghr

}

task vcf_anno_ghr {

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename = "no_id_provided"
  Boolean sv = false

  String task_name = "vcf_anno_ghr"
  String task_version = "1.0.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-ghr:1.1.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   python3 /intelliseqtools/vcf-anno-ghr.py --input-vcf ${vcf_gz} \
                                            --input-json /resources/ghr-summaries.json \
                                            --output-vcf ${vcf_basename}_anno-ghr.vcf.gz\
                                            ${true="--sv" false="" sv}

   tabix -p vcf ${vcf_basename}_anno-ghr.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_with_ghr_vcf_gz = "${vcf_basename}_anno-ghr.vcf.gz"
    File annotated_with_ghr_vcf_gz_tbi = "${vcf_basename}_anno-ghr.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
