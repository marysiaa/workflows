workflow vcf_add_id_workflow
{
  call vcf_add_id
}

task vcf_add_id {

  String task_name = "vcf_add_id"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-add-id:1.0.0"
  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   python3 /intelliseqtools/add_gnomad_id.py \
      -i ${vcf_gz} \
      -o ${sample_id}_annotated.vcf.gz

   tabix -p vcf ${sample_id}_annotated.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File annotated_vcf_gz = "${sample_id}_annotated.vcf.gz"
    File annotated_vcf_gz_tbi = "${sample_id}_annotated.vcf.gz.tbi"

  }

}
