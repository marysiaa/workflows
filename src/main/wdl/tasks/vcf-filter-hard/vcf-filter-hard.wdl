workflow vcf_filter_hard_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "variant", "filtering"]}'
    name: 'vcf_filter_hard'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Applies hard filters to vcf'
    changes: '{"1.0.4": "add dragmap aligner tool", "1.0.3": "removed QUAL filtering, updated gatk", "1.0.2": "output vcf name change"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "VCF gz", "type": "File", "description": "Variant file"}'
    input_vcf_gz_tbi: '{"name": "VCF gz tbi", "type": "File", "description": "Variant file index"}'

    output_hard_filtered_vcf_gz: '{"name": "Hard filtered vcf", "type": "File", "copy": "True", "description": "VCF with variants that passed all hard filters (bgzipped)"}'
    output_hard_filtered_vcf_gz_tbi: '{"name": "Hard filtered vcf tbi", "type": "File", "copy": "True", "description": "Index for the hard filtered VCF"}'
    output_hard_filter_rejected_vcf_gz: '{"name": "Hard filter rejected vcf", "type": "File", "copy": "True", "description": "VCF file with variants that failed applied filters (bgzipped)"}'
    output_hard_filter_rejected_vcf_gz_tbi: '{"name": "SNP rejected vcf", "type": "File", "copy": "True", "description": "Index for the hard filter rejected vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }
  call vcf_filter_hard

 }

task vcf_filter_hard {

  String task_name = "vcf_filter_hard"
  String task_version = "1.0.4"
  String reference_genome = "hg38"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.4.1-" +reference_genome+ ":1.0.0"

  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no-id"
  String aligner_tool = "bwa-mem"
  #String java_opt = "-Xms3000m"

  ## Thresholds (based on GATK recommendations)
  ## https://gatk.broadinstitute.org/hc/en-us/articles/360037499012-I-am-unable-to-use-VQSR-recalibration-to-filter-variants
  ## https://gatk.broadinstitute.org/hc/en-us/articles/360035531112--How-to-Filter-variants-either-with-VQSR-or-by-hard-filtering
  Float QD_threshold = 2.0
  Float FS_threshold = 60.0
  Float SOR_threshold = 3.0
  Float MQ_threshold = 40.0
  Float MQRankSum_threshold = -12.5
  Float ReadPosRankSum_threshold = -8.0


  Float QD_threshold_INDEL = 2.0
  Float FS_threshold_INDEL = 200.0
  Float ReadPosRankSum_threshold_INDEL = -20.0
  Float SOR_threshold_INDEL = 10.0
  

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   ref_genome=$(ls /resources/*/*/*.fa)

  if [ "${aligner_tool}" = "bwa-mem" ]; then
    # selecting and filtering snps
    gatk SelectVariants \
        -R $ref_genome \
        -V ${vcf_gz} \
        --select-type-to-include SNP \
        -O snp.vcf

    gatk VariantFiltration \
        -R $ref_genome \
        -V snp.vcf \
        -O filtered_snp.vcf \
        -filter "QD < ${QD_threshold}" --filter-name "QD_SNP"  \
        -filter "FS > ${FS_threshold}" --filter-name "FS_SNP" \
        -filter "MQ < ${MQ_threshold}" --filter-name "MQ_SNP" \
        -filter "MQRankSum < ${MQRankSum_threshold}" --filter-name "MQRankSum_SNP" \
        -filter "ReadPosRankSum < ${ReadPosRankSum_threshold}" --filter-name "ReadPosRankSum"


    awk '/^#/||$7=="PASS"' filtered_snp.vcf > snp-pass.vcf
    awk '/^#/||$7!="PASS"' filtered_snp.vcf > snp-rejected.vcf

    # selecting and filtering indels
    gatk SelectVariants \
        -R $ref_genome \
        -V ${vcf_gz} \
        --select-type-to-include INDEL \
        --select-type-to-include MIXED \
        -O indel.vcf

    gatk VariantFiltration \
        -R $ref_genome \
        -V indel.vcf \
        -O filtered_indel.vcf \
        -filter "QD < ${QD_threshold_INDEL}" --filter-name "QD_INDEL" \
        -filter "FS > ${FS_threshold_INDEL}" --filter-name "FS_INDEL" \
        -filter "ReadPosRankSum < ${ReadPosRankSum_threshold_INDEL}"  --filter-name "ReadPosRankSum_INDEL" \
        -filter "SOR > ${SOR_threshold_INDEL}" --filter-name "SOR_INDEL" 
        


    awk '/^#/||$7=="PASS"' filtered_indel.vcf > indel-pass.vcf
    awk '/^#/||$7!="PASS"' filtered_indel.vcf > indel-rejected.vcf

    # merging filtered (good) snps and indels
    gatk MergeVcfs  \
        -I snp-pass.vcf \
        -I indel-pass.vcf  \
        -O ${sample_id}.vcf.gz

    # merging rejected (bad) snps and indels
    gatk MergeVcfs  \
        -I snp-rejected.vcf \
        -I indel-rejected.vcf  \
        -O ${sample_id}_hard-filter-rejected.vcf.gz

  elif [ "${aligner_tool}" = "dragmap" ]; then
    gatk VariantFiltration \
      -R $ref_genome \
      -V ${vcf_gz} \
      --filter-expression "QUAL < 10.4139" \
      --filter-name "DRAGENHardQUAL" \
      -O filtered.vcf

    awk '/^#/||$7=="PASS"' filtered.vcf > ${sample_id}.vcf
    awk '/^#/||$7!="PASS"' filtered.vcf > ${sample_id}_hard-filter-rejected.vcf
    
    bgzip ${sample_id}.vcf
    bgzip ${sample_id}_hard-filter-rejected.vcf

    tabix -p vcf ${sample_id}.vcf.gz
    tabix -p vcf ${sample_id}_hard-filter-rejected.vcf.gz

  fi


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File hard_filtered_vcf_gz = "${sample_id}.vcf.gz"
    File hard_filtered_vcf_gz_tbi = "${sample_id}.vcf.gz.tbi"

    File hard_filter_rejected_vcf_gz = "${sample_id}_hard-filter-rejected.vcf.gz"
    File hard_filter_rejected_vcf_gz_tbi = "${sample_id}_hard-filter-rejected.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
