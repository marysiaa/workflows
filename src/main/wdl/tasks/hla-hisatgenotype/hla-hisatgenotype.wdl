workflow hla_hisatgenotype_workflow
{
  call hla_hisatgenotype
}

task hla_hisatgenotype {

  String task_name = "hla_hisatgenotype"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/hisat-genotype:test"

  ## define loci that will be genotyped
  ## present in db:
  ## "A","B","C","DMA","DMB","DOA","DOB","DPA1","DPB1","DPB2","DQA1","DQB1","DRA","DRB","DRB1","DRB3","DRB4","DRB5","E","F","G","H","HFE","J","K","L","MICA","MICB","P","TAP1","TAP2","V"
  ## DRB3, DRB4, DRB5 - not working
  Array[String] hla_loci = ["A","B","C","DPA1","DPB1","DQA1","DQB1","DRA","DRB1"]
  
  ## Allele name starts with a gene name followed by an asterisk and at least two colonseparated numeric fields (e.g., HLA-A*02:02:01:02)
  ## 1 - show only first numeric field (allele families, often correspond to serologically defined antigens)
  ## 2 - 1 plus nonsynonimous changes
  ## 3 - 1 plus 2 pus synonymous changes
  ## 4 - all, 1 plus 2 plus 3 plus changes in UTR regions
  Int trim_level = 4 

  ## unaligned or aligned reads as input
  File fastq_1
  File fastq_2
  
  
  String sample_id = "no_id_provided"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  ln -s ${fastq_1} ${sample_id}.1.fq.gz
  ln -s ${fastq_2} ${sample_id}.2.fq.gz
       

   hisatgenotype \
       --keep-extract \
       --base hla \
       --locus-list ${sep="," hla_loci} \
       -1 ${sample_id}.1.fq.gz -2 ${sample_id}.2.fq.gz \
       --out-dir output \
       -p 8

   hisatgenotype_toolkit parse-results \
       --in-dir output \
       --trim ${trim_level} \
       --csv \
       --output-file ${sample_id}_hla.tsv

    mv output/assembly_graph*.report ${sample_id}_hla-genotyping.report
    
        
       
     

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: 8
    maxRetries: 0

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File out_csv = "${sample_id}_hla.tsv"
    File genotyping_report = "${sample_id}_hla-genotyping.report"
  }

}
