workflow vcfs_merge_workflow
{
  call vcfs_merge
}

task vcfs_merge {

  String task_name = "vcfs_merge"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.5"
  Array[File] vcfs_gz

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
   ls ${sep=" " vcfs_gz} | xargs -i bash -c "bcftools index -f {}"
   if [ $(ls -1q ${sep=" " vcfs_gz} | wc -l) == 1 ]; then
     zcat ${sep=" " vcfs_gz} | bgzip -c > merged.vcf.gz
   else
     bcftools merge --force-samples ${sep=" " vcfs_gz} | bgzip -c > merged.vcf.gz
   fi
   bcftools index --tbi merged.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: 1
    maxRetries: 1

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File vcf_gz = "merged.vcf.gz"
    File vcf_gz_tbi = "merged.vcf.gz.tbi"

  }

}
