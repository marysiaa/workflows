import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.2.0/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.8/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-metrics@1.4.3/src/main/wdl/tasks/bam-metrics/bam-metrics.wdl" as bam_metrics_task
import "https://gitlab.com/intelliseq/workflows/raw/qc-multiqc@3.0.0/src/main/wdl/tasks/qc-multiqc/qc-multiqc.wdl" as qc_multiqc_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow bam_qc {

  String module_name = "bam_qc"
  String module_version = "3.0.8"
  String sample_id = "no_id_provided"
  String reference_genome = "grch38-no-alt" #or hg38
  String bam_name = "variant-calling-ready"

  File bam
  File bai

  ## Bam metrics inputs
  File? dupl_metrics
  Boolean dupl_metrics_empty = false
  Boolean use_fast_algorithm = true
  String bam_metrics_java_options = "-Xms4g -Xmx63g" # use on anakin "-Xms4g -Xmx8g"
  String kit = "exome-v7" # Possible values "exome-v6", "exome-v7", "genome", "target"
  # for target
  File? custom_interval_list
  # for wgs
  File? interval_list
  # for wes
  File? target
  File? bait

  ## To prepare intervals (if not given)
  # for bed to interval list
  File? bed
  Array[String]? gene_list
  Int padding = 1000
  # for wes and wgs
  Boolean run_resources_kit_for_genome = (kit == "genome" && !defined(interval_list))
  Boolean run_resources_kit_for_exome = ((kit == "exome-v7" || kit == "exome-v6" || kit == "exome-v8") && ( !defined(target) || !defined(bait)))
  Boolean run_bed_to_intervals = (kit == "target" && !defined(custom_interval_list))

  ## Multiqc
  File? sample_info_json
  String? report_title
  String analysis_type = sub(kit, "-v.*", "")

  #1a. Prepare intervals for genome or exome
  if(run_resources_kit_for_genome || run_resources_kit_for_exome) {
       call resources_kit_task.resources_kit {
           input:
              kit = kit,
              reference_genome = reference_genome
      }

      if (kit == "genome"){
          File interval_resources = resources_kit.interval_list[0]
      }

      if(kit == "exome-v7" || kit == "exome-v6" || kit == "exome-v8"){
          File target_resources = resources_kit.target[0]
          File bait_resources = resources_kit.bait[0]
      }
  }

  #1b. Prepare interval list for targeted analysis
  if (run_bed_to_intervals) {
      call bed_to_interval_list_task.bed_to_interval_list {
          input:
              bed = bed,
              gene_list = gene_list,
              reference_genome = reference_genome,
              padding = padding,
              sample_id = sample_id
      }
      File prepared_custom_interval_list = bed_to_interval_list.custom_interval_file
  }

  if (defined(interval_list) || defined(interval_resources)){
      File interval_file = select_first([interval_list, interval_resources])
  }

  if((defined(target_resources) || defined(target)) && (defined(bait_resources) || defined(bait))) {
      File target_file = select_first([target, target_resources])
      File bait_file = select_first([bait, bait_resources])
  }

  if (defined(custom_interval_list) || defined(prepared_custom_interval_list)){
      File custom_interval_file = select_first([custom_interval_list, prepared_custom_interval_list])
  }

  #2. Bam metrics
  call bam_metrics_task.bam_metrics as bam_stats {
      input:
          sample_id = sample_id,
          intervals = interval_file,
          custom_intervals = custom_interval_file,
          target = target_file,
          bait = bait_file,
          kit = kit,
          bam = bam,
          bai = bai,
          reference_genome = reference_genome,
          java_options = bam_metrics_java_options,
          dupl_metrics = dupl_metrics,
          dupl_metrics_empty = dupl_metrics_empty,
          use_fast_algorithm = use_fast_algorithm,
          bam_name = bam_name
      }

  #3. MultiQC
  if (length(bam_stats.bam_metrics) != 0) {
      call qc_multiqc_task.qc_multiqc {
          input:
              bam_metrics = bam_stats.bam_metrics,
              sample_id = sample_id,
              analysis_type = analysis_type,
              sample_info_json = sample_info_json,
              report_title = report_title,
              output_name = "bam-quality-check"
      }
  }
  #Merge bco, stdout, stderr files

  Array[File] bco_tasks = select_all([resources_kit.bco, bed_to_interval_list.bco, bam_stats.bco, qc_multiqc.bco ])
  Array[File] stdout_tasks = select_all([resources_kit.stdout_log, bed_to_interval_list.stdout_log, bam_stats.stdout_log, qc_multiqc.stdout_log])
  Array[File] stderr_tasks = select_all([resources_kit.stderr_log, bed_to_interval_list.stderr_log, bam_stats.stderr_log, qc_multiqc.stderr_log])


  call bco_merge_task.bco_merge {
      input:
          bco_array = bco_tasks,
          stdout_array = stdout_tasks,
          stderr_array = stderr_tasks,
          module_name = module_name,
          module_version = module_version
  }

  output {
    File? data_json = qc_multiqc.multiqc_data_json
    File? report_html = qc_multiqc.report_html
    File? general_stats_txt = qc_multiqc.multiqc_general_stats_txt
    File? multiqc_sources = qc_multiqc.multiqc_sources

    File stdout_log = bco_merge.stdout_log
    File bco = bco_merge.bco
    File stderr_log = bco_merge.stderr_log
  }
}
