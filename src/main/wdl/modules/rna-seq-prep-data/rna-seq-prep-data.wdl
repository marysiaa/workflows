import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-ensembl-data@1.1.5/src/main/wdl/tasks/rna-seq-ensembl-data/rna-seq-ensembl-data.wdl" as rna_seq_ensembl_data_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-index-star@1.0.1/src/main/wdl/tasks/rna-seq-index-star/rna-seq-index-star.wdl" as  rna_seq_index_star_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-index-kallisto@1.0.0/src/main/wdl/tasks/rna-seq-index-kallisto/rna-seq-index-kallisto.wdl" as  rna_seq_index_kallisto_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-ensembl-anno@1.1.1/src/main/wdl/tasks/rna-seq-ensembl-anno/rna-seq-ensembl-anno.wdl" as  rna_seq_ensembl_anno_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.1/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow rna_seq_prep_data {

    String release_version = "104"
    String organism_name
    String chromosome_name = "primary_assembly" # input for testing only

    Array[String]? genes_anno_list
    Array[String]? transcripts_anno_list

    String analysis_id = "no_id_provided"
    String module_name = "rna_seq_prep_data"
    String module_version = "1.1.1"


  #1 first task to call
    call rna_seq_ensembl_data_task.rna_seq_ensembl_data {
        input:
            release_version = release_version,
            organism_name = organism_name,
            chromosome_name = chromosome_name
    }

    File gtf_from_ensembl_data = rna_seq_ensembl_data.gtf[0]
    File genome_file = rna_seq_ensembl_data.genome[0]
    File transcriptome = rna_seq_ensembl_data.transcriptome[0]
    File refflat_from_ensembl_data = rna_seq_ensembl_data.refflat[0]
    File bed_file = rna_seq_ensembl_data.bed[0]

    call rna_seq_index_star_task.rna_seq_index_star {
        input:
            genome = genome_file,
            gtf = gtf_from_ensembl_data
    }

    call rna_seq_index_kallisto_task.rna_seq_index_kallisto {
        input:
            transcriptome = transcriptome,
            analysis_id = analysis_id
    }

    call rna_seq_ensembl_anno_task.rna_seq_ensembl_anno {
        input:
            organism_name = organism_name,
            ensembl_version = release_version,
            genes_anno_list = genes_anno_list,
            transcripts_anno_list = transcripts_anno_list
    }

    # after all regular tasks merge bco
    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = [rna_seq_ensembl_data.bco, rna_seq_index_star.bco, rna_seq_index_kallisto.bco, rna_seq_ensembl_anno.bco]
    Array[File] stdout_tasks = [rna_seq_ensembl_data.stdout_log, rna_seq_index_star.stdout_log, rna_seq_index_kallisto.stdout_log, rna_seq_ensembl_anno.stdout_log]
    Array[File] stderr_tasks = [rna_seq_ensembl_data.stderr_log, rna_seq_index_star.stderr_log, rna_seq_index_kallisto.stderr_log, rna_seq_ensembl_anno.stderr_log]

    Array[Array[File]] bco_scatters = [bco_tasks]
    Array[Array[File]] stdout_scatters = [stdout_tasks]
    Array[Array[File]] stderr_scatters = [stderr_tasks]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {
        Array[File] ref_genome_index = rna_seq_index_star.ref_genome_index
        File chromosomes = rna_seq_index_star.chromosomes
        File transcriptome_index = rna_seq_index_kallisto.transcriptome_index
        File gtf = gtf_from_ensembl_data
        File genome = genome_file
        File refflat = refflat_from_ensembl_data
        File bed = bed_file
        File genes_ensembl_tsv = rna_seq_ensembl_anno.genes_ensembl_tsv
        File transcripts_ensembl_tsv = rna_seq_ensembl_anno.transcripts_ensembl_tsv

        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco
    }

}
