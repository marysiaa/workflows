import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.3/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-arriba-fusions@1.0.0/src/main/wdl/tasks/fq-arriba-fusions/fq-arriba-fusions.wdl" as fq_arriba_fusions_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-fuseq-wes-fusions@1.1.0/src/main/wdl/tasks/bam-fuseq-wes-fusions/bam-fuseq-wes-fusions.wdl" as bam_fuseq_wes_fusions_task
import "https://gitlab.com/intelliseq/workflows/raw/arriba-anno-civic@1.0.0/src/main/wdl/tasks/arriba-anno-civic/arriba-anno-civic.wdl" as arriba_anno_civic_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow fusions {


  String module_name = "fusions"
  String module_version = "1.1.0"
  String sample_id = "no_id_provided"
  Array[File] fastqs
  String ensembl_release = "107"
   

  ## organize fastq_files
  call fq_organize_task.fq_organize {
      input:
            fastqs = fastqs,          
            paired = true,
            split_files = false
  }

  ## align fastqs with STAR and call fusions with arriba
  call fq_arriba_fusions_task.fq_arriba_fusions {
      input:
            fastqs_1 = fq_organize.fastqs_1,          
            fastqs_2 = fq_organize.fastqs_2,
            sample_id = sample_id,
            ensembl_version = "104" ## 107 not available
  } 

  ## call fusions with FuSeq_WES
  call bam_fuseq_wes_fusions_task.bam_fuseq_wes_fusions {
      input:
          bam = fq_arriba_fusions.bam,
          bai = fq_arriba_fusions.bai,
          ensembl_release = ensembl_release,
          sample_id = sample_id           
  }

  ## annotate arriba output
  call arriba_anno_civic_task.arriba_anno_civic {
      input:
          arriba_tsv = fq_arriba_fusions.fusions_tsv,
          sample_id = sample_id  
  }        
  
  
  # Merge bco, stdout, stderr files
  Array[File] bco_tasks = [fq_organize.bco, fq_arriba_fusions.bco, bam_fuseq_wes_fusions.bco, fq_arriba_fusions.bco]
  Array[File] stdout_tasks = [fq_organize.stdout_log, fq_arriba_fusions.stdout_log, bam_fuseq_wes_fusions.stdout_log, fq_arriba_fusions.stdout_log]
  Array[File] stderr_tasks = [fq_organize.stderr_log, fq_arriba_fusions.stderr_log, bam_fuseq_wes_fusions.stderr_log, fq_arriba_fusions.stderr_log]


  call bco_merge_task.bco_merge {
     input:
          bco_array = bco_tasks,
          stdout_array = stdout_tasks,
          stderr_array = stderr_tasks,
          module_name = module_name,
          module_version = module_version
 }



  output {
    
  # arriba outputs
  File bam = fq_arriba_fusions.bam
  File bai = fq_arriba_fusions.bai
  File arriba_fusions_tsv = fq_arriba_fusions.fusions_tsv
  File arriba_discarded_fusions_tsv = fq_arriba_fusions.discarded_fusions_tsv
  File annotated_tsv = arriba_anno_civic.annotated_tsv

  # fuseq outputs
  File fuseq_fusion_tsv = bam_fuseq_wes_fusions.fusion_tsv
  File fuseq_fusion_mr_fge_tsv = bam_fuseq_wes_fusions.fusion_mr_fge_tsv
  File fuseq_fusion_mr_fge_fdb_tsv = bam_fuseq_wes_fusions.fusion_mr_fge_fdb_tsv
  File fuseq_fusion_sr_fge_tsv = bam_fuseq_wes_fusions.fusion_sr_fge_tsv  ## this one is interesting
  File fuseq_fusion_sr_fge_fdb_tsv = bam_fuseq_wes_fusions.fusion_sr_fge_fdb_tsv

  File stdout_log = bco_merge.stdout_log
  File stderr_log = bco_merge.stderr_log
  File bco = bco_merge.bco

  }

}
