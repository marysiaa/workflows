import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-featurecounts@2.1.1/src/main/wdl/tasks/rna-seq-featurecounts/rna-seq-featurecounts.wdl" as rna_seq_featurecounts_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-quant-kallisto@1.0.0/src/main/wdl/tasks/rna-seq-quant-kallisto/rna-seq-quant-kallisto.wdl" as rna_seq_quant_kallisto_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-merge-abundances@1.1.2/src/main/wdl/tasks/rna-seq-merge-abundances/rna-seq-merge-abundances.wdl" as rna_seq_merge_abundances_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.1/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow rna_seq_abundance {

    # featurecounts inputs
    Array[File] bams
    Array[File] bams_bais
    File ensembl_gtf
    File genome

    # kallisto quant inputs
    Array[File] fastqs_1
    Array[File]? fastqs_2
    Array[File] fastqs_2_defined = select_first([fastqs_2, []])
    File transcriptome_index
    Array[String] samples_ids
    File chromosomes
    # for single_end
    Float fragment_length = 200
    Float fragment_length_sd = 20 #sd - standard deviation

    File genes_ensembl_tsv
    File transcripts_ensembl_tsv

    Boolean is_paired_end = true
    String stranded = "unstranded" # Possible values include: unstranded, stranded and reversely stranded
    String analysis_id = "no_id_provided"

    String module_name = "rna_seq_abundance"
    String module_version = "3.1.2"

    #1
    call rna_seq_featurecounts_task.rna_seq_featurecounts {
        input:
            bams = bams,
            bams_bais = bams_bais,
            ensembl_gtf = ensembl_gtf,
            genome = genome,
            is_paired_end = is_paired_end,
            stranded = stranded,
            sample_id = analysis_id
    }

    #2
    if(is_paired_end) {
        scatter (index in range(length(fastqs_1))) {
            call rna_seq_quant_kallisto_task.rna_seq_quant_kallisto {
                input:
                    transcriptome_index = transcriptome_index,
                    fastq_1 = fastqs_1[index],
                    fastq_2 = fastqs_2_defined[index],
                    sample_id = samples_ids[index],
                    ensembl_gtf = ensembl_gtf,
                    chromosomes = chromosomes,
                    is_paired_end = is_paired_end,
                    stranded = stranded,
                    fragment_length = fragment_length,
                    fragment_length_sd = fragment_length_sd,
                    index = index
            }
        }
    }
    if(!is_paired_end) {
        scatter (index in range(length(fastqs_1))) {
            call rna_seq_quant_kallisto_task.rna_seq_quant_kallisto as rna_seq_quant_kallisto_se {
                input:
                    transcriptome_index = transcriptome_index,
                    fastq_1 = fastqs_1[index],
                    sample_id = samples_ids[index],
                    ensembl_gtf = ensembl_gtf,
                    chromosomes = chromosomes,
                    is_paired_end = is_paired_end,
                    stranded = stranded,
                    fragment_length = fragment_length,
                    fragment_length_sd = fragment_length_sd,
                    index = index
            }
        }
    }
#   unused
#    Array[File] eff_length_files = select_first([rna_seq_quant_kallisto.eff_length_file, rna_seq_quant_kallisto_se.eff_length_file])
    Array[File] est_counts_files = select_first([rna_seq_quant_kallisto.est_counts_file, rna_seq_quant_kallisto_se.est_counts_file])
    Array[File] tpm_files = select_first([rna_seq_quant_kallisto.tpm_file, rna_seq_quant_kallisto_se.tpm_file])

    call rna_seq_merge_abundances_task.rna_seq_merge_abundances {
        input:
            est_counts_files = est_counts_files,
            tpm_files = tpm_files,
            gene_level_tsv = rna_seq_featurecounts.sample_tbl_featurecount_gene_level,
            genes_ensembl_tsv = genes_ensembl_tsv,
            transcripts_ensembl_tsv = transcripts_ensembl_tsv,
            analysis_id = analysis_id
    }

    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = [rna_seq_featurecounts.bco, rna_seq_merge_abundances.bco]
    Array[File] stdout_tasks = [rna_seq_featurecounts.stdout_log, rna_seq_merge_abundances.stdout_log]
    Array[File] stderr_tasks = [rna_seq_featurecounts.stderr_log, rna_seq_merge_abundances.stderr_log]

    Array[Array[File]] bco_scatters = [bco_tasks, select_first([rna_seq_quant_kallisto.bco, rna_seq_quant_kallisto_se.bco])]
    Array[Array[File]] stdout_scatters = [stdout_tasks, select_first([rna_seq_quant_kallisto.stdout_log, rna_seq_quant_kallisto_se.stdout_log])]
    Array[Array[File]] stderr_scatters = [stderr_tasks, select_first([rna_seq_quant_kallisto.stderr_log, rna_seq_quant_kallisto_se.stderr_log])]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

        # annotated with attributes from ensembl featurecount table for gene level
        File gene_level = rna_seq_merge_abundances.gene_level
        File gene_level_summary = rna_seq_featurecounts.gene_level_summary

        # merged tables from the quant kallisto tool and annotated with attributes from ensembl
        File est_counts_tsv = rna_seq_merge_abundances.est_counts
        File tpm_tsv = rna_seq_merge_abundances.tpm

        # stdout, stderr, bco
        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco

    }

}
