import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.1/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task
import "https://gitlab.com/intelliseq/workflows/-/raw/report-bco@1.1.0/src/main/wdl/tasks/report-bco/report-bco.wdl" as report_bco_task
import "https://gitlab.com/intelliseq/workflows/-/raw/bco-json-to-csv@2.0.1/src/main/wdl/tasks/bco-json-to-csv/bco-json-to-csv.wdl" as bco_json_to_csv_task

workflow bco {

  Array[File]? bco_array
  Array[File]? stdout_array
  Array[File]? stderr_array

  File? sample_info_json
  Int timezoneDifference = 0
  Boolean is_rna_seq = false

  String sample_id = "no_id_provided"
  String? module_name
  String? module_version
  String? pipeline_name
  String? pipeline_version

  if(defined(module_name)) {
    call bco_merge_task.bco_merge as bco_merge_module {
      input:
          bco_array = bco_array,
          stdout_array = stdout_array,
          stderr_array = stderr_array,
          module_name = module_name,
          module_version = module_version
    }
  }

  if(defined(pipeline_name)) {
    call bco_merge_task.bco_merge as bco_merge_pipeline {
      input:
          bco_array = bco_array,
          stdout_array = stdout_array,
          stderr_array = stderr_array,
          pipeline_name = pipeline_name,
          pipeline_version = pipeline_version
    }
  }

  File bco = select_first([bco_merge_module.bco, bco_merge_pipeline.bco])
  File stdout = select_first([bco_merge_module.stdout_log, bco_merge_pipeline.stdout_log])
  File stderr = select_first([bco_merge_module.stderr_log, bco_merge_pipeline.stderr_log])

  call report_bco_task.report_bco {
    input:
        bco_json = bco,
        sample_info_json = sample_info_json,
        timezoneDifference = timezoneDifference,
        is_rna_seq = is_rna_seq,
        sample_id = sample_id
  }

  call bco_json_to_csv_task.bco_json_to_csv {
    input:
        bco_json = bco,
        sample_id = sample_id
  }

  output {

    #bco, stdout, stderr
    File bco_merged = bco
    File stdout_log = stdout
    File stderr_log = stderr

    #bco report (pdf, docx)
    File bco_report_pdf = report_bco.bco_report_pdf
    File bco_report_docx = report_bco.bco_report_docx

    #bco table (csv)
    File bco_table_csv = bco_json_to_csv.bco_table_csv

  }

}
