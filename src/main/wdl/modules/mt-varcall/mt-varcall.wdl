import "https://gitlab.com/intelliseq/workflows/raw/mt-extract-reads@1.0.0/src/main/wdl/tasks/mt-extract-reads/mt-extract-reads.wdl" as mt_extract_reads_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-realign@1.0.1/src/main/wdl/tasks/mt-realign/mt-realign.wdl" as mt_realign_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-haplochecker@1.0.3/src/main/wdl/tasks/mt-haplochecker/mt-haplochecker.wdl" as mt_haplochecker_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-varcall-mutect2@1.0.2/src/main/wdl/tasks/mt-varcall-mutect2/mt-varcall-mutect2.wdl" as mt_varcall_mutect2_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-merge-mutect2-outputs@1.0.3/src/main/wdl/tasks/mt-merge-mutect2-outputs/mt-merge-mutect2-outputs.wdl" as mt_merge_mutect2_outputs_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-m2-vcf-filter@1.0.4/src/main/wdl/tasks/mt-m2-vcf-filter/mt-m2-vcf-filter.wdl" as mt_m2_vcf_filter_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.4.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-concat@1.1.2/src/main/wdl/tasks/bam-concat/bam-concat.wdl" as bam_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task



workflow mt_varcall {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'Mitochondrial variant calling'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Calls and filters mitochondrial variants'
    changes: '{"1.0.10": "update haplochecker and outputs merging tasks", "1.0.9": "bam from hc not necessary to run vcf and bam concat", "1.0.8": "docker gatk update", "1.0.7": "name of outputs", "1.0.6": "vcf-concat updated", "1.0.5": "final vcf name fixed", "1.0.4": "dockers updates", "1.0.3": "new version of the bam-concat and mutect2 filter tasks", "1.0.2": "merge with hc bam and vcf added to module","1.0.1": "works with empty bam (new mt-haplochecker version)"}'

    input_sample_id: '{"index": 1, "name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_bam: '{"index": 2, "required": true, "name": "Bam file", "type": "File", "extension": [".bam"], "description": "Genome bam file"}'
    input_bai: '{"index": 3, "required": true, "name": "Bam index file", "type": "File", "extension": [".bai"], "description": "Index for the genome bam file"}'
    input_mt_intervals: '{"index": 4, "name": "MT target intervals", "advanced": true, "type": "File", "extension": [".interval_list"], "description": "Custom mitochondrial intervals, needed only for targeted sequencing version"}'
    input_genome_or_exome: '{"index": 5, "name": "Genome or exome", "type": "String", "constraints": {"choices": ["genome", "exome", "target"]},"default": "genome", "description": "Analysis type: Use genome for WGS, exome for WES and target for panel sequencing"}'
    input_max_alt_allele_count: '{"index": 6, "name": "Max alt allele count", "advanced": true, "type": "Int", "default": 4, "description": "Maximal number of alleles per site"}'
    input_vaf_filter_threshold: '{"index": 7, "name": "VAF filter threshold", "advanced": true, "type": "Float", "advanced": true, "default": 0.05, "description": "Minimal frequency of the mitochondrial variant to be outputed"}'
    input_f_score_beta: '{"index": 8, "name": "F score beta", "type": "Float", "advanced": true, "default": 1.0, "description": "The relative weight of recall to precision"}'
    input_min_alt_reads: '{"index": 9, "name": "Minimum alt reads", "type": "Int", "advanced": true, "default": 2, "description": "Minimum unique (i.e. deduplicated) reads supporting the alternate mitochondrial allele"}'
    input_hc_vcf_gz: '{"name": "HC vcf gz", "type": "File", "hidden": true, "extension": [".vcf.gz"], "description": "Haplotype caller vcf file with nuclear variants"}'
    input_hc_vcf_gz_tbi: '{"name": "HC vcf gz tbi", "type": "File", "hidden": true, "extension": [".vcf.gz.tbi"], "description": "Index for the HC vcf file"}'
    input_hc_bam: '{"name": "HC bam", "type": "File", "hidden": true, "extension": [".bam"], "description": "Haplotype caller bam (with assembled nuclear haplotypes and locally realigned reads)"}'

    output_mt_filtered_vcf_gz: '{"name": "Filtered MT vcf gz", "type": "File", "copy": "True", "description": "Filtered mitochondrial vcf"}'
    output_mt_filtered_vcf_gz_tbi: '{"name": "Filtered MT vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the filtered mitochondrial vcf"}'
    output_mt_rejected_vcf_gz: '{"name": "Rejected MT vcf gz", "type": "File", "copy": "True", "description": "Vcf with mitochondrial variants that did not pass applied filters"}'
    output_mt_rejected_vcf_gz_tbi: '{"name": "Rejected MT vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the rejected mitochondrial vcf"}'
    output_mutect2_mt_bam: '{"name": "Mutec2 MT bam", "type": "File", "copy": "True", "description": "Mutect2 bam (with assembled mitochondrial haplotypes and locally realigned reads)"}'
    output_mutect2_mt_bai: '{"name": "Mutec2 MT bai", "type": "File", "copy": "True", "description": "Index for the mutect2 MT bam"}'
    output_realigned_mt_bam: '{"name": "Realigned mitochondrial bam", "type": "File", "copy": "True", "description": "Realigned mitochondrial bam"}'
    output_realigned_mt_bai: '{"name": "Realigned mitochondrial bai", "type": "File", "copy": "True", "description": "Index for the realigned mitochondrial bam"}'
    output_contamination_report: '{"name": "Contamination report", "type": "File", "copy": "True", "description": "File containing haplochecker contamination report"}'
    output_duplication_metrics: '{"name": "Duplication metrics file", "type": "File", "copy": "True", "description": "Duplicate metrics file"}'
    output_allcontigs_vcf_gz: '{"name": "All contigs vcf gz", "type": "File", "copy": "True", "description": "Vcf file with nuclear and mitochondrial variants"}'
    output_allcontigs_vcf_gz_tbi: '{"name": "All contigs vcf gz tbi", "type": "File", "copy": "True", "description": "Index for all contigs vcf"}'
    output_allcontigs_bamout:  '{"name": "All contigs bamout", "type": "File", "copy": "True", "description": "Merged bam from HC and Mutect2 (with assembled haplotypes and locally realigned reads)"}'
    output_allcontigs_bamout_bai: '{"name": "All contigs bamout bai", "type": "File", "copy": "True", "description": "Index for the all contigs bamout file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

 }

  String module_name = "mt_varcall"
  String module_version = "1.0.10"
  String sample_id = "no_id_provided"
  String genome_or_exome = "genome" # other options: exome, target

  File bam
  File bai
  String bam_concat_name = "allcontigs-bamout"
  File? mt_intervals
  File? hc_vcf_gz
  File? hc_vcf_gz_tbi
  File? hc_bamout


  if (defined(mt_intervals) && (genome_or_exome == "target")) {
  File? target_mt_intervals = mt_intervals
  }

  Int max_alt_allele_count = 4
  Float vaf_filter_threshold = 0.05
  Float f_score_beta = 1.0
  Int min_alt_reads = 2

  # 1. Extract reads mapped to mitochondrion from bam
  call mt_extract_reads_task.mt_extract_reads {
      input:
          bam = bam,
          bai = bai,
          sample_id = sample_id
  }

  ## NORMAL REFERENCE
  # 2a. Realign extracted reads to normal mitochondrial fasta
  call mt_realign_task.mt_realign as normal_alignment {
      input:
          reverted_mt_bam = mt_extract_reads.reverted_mt_bam,
          shifted_alignment = false,
          sample_id = sample_id
  }

  # 3a. Call variants aligned to normal mitochondrial fasta (everything except for control region)
  call mt_varcall_mutect2_task.mt_varcall_mutect2 as normal_varcall {
      input:
          mt_bam = normal_alignment.realigned_mt_bam,
          mt_bai = normal_alignment.realigned_mt_bai,
          shifted_bam = false,
          sample_id = sample_id
  }

  ## SHIFTED REFERENCE
  # 2b. Realign extracted reads to shifted mitochondrial fasta
  call mt_realign_task.mt_realign as shifted_alignment {
      input:
          reverted_mt_bam = mt_extract_reads.reverted_mt_bam,
          shifted_alignment = true,
          sample_id = sample_id
  }

   # 3b. Call variants aligned to shifted mitochondrial fasta (control region only); liftover outputs (vcf and m2 bam)
   call mt_varcall_mutect2_task.mt_varcall_mutect2 as shifted_varcall {
       input:
           mt_bam = shifted_alignment.realigned_mt_bam,
           mt_bai = shifted_alignment.realigned_mt_bai,
           shifted_bam = true,
           sample_id = sample_id
   }

  # 4. Merge mutect2 outputs
  call mt_merge_mutect2_outputs_task.mt_merge_mutect2_outputs as merge_mt_outputs {
     input:
         mutect_vcfs = [shifted_varcall.mt_vcf_gz, normal_varcall.mt_vcf_gz],
         mutect_bams = [shifted_varcall.mutec2_bam, normal_varcall.mutec2_bam],
         mutect_stats = [shifted_varcall.mutec2_stats, normal_varcall.mutec2_stats],
         sample_id = sample_id,
         output_name = "mt-variants-sites-only"
  }

  # 5. Estimate contamination (haplochecker)
  call  mt_haplochecker_task.mt_haplochecker {
      input:
          mt_bam = normal_alignment.realigned_mt_bam,
          mt_bai = normal_alignment.realigned_mt_bai,
          sample_id = sample_id
  }

  # 6. Filter variants
  call mt_m2_vcf_filter_task.mt_m2_vcf_filter {
      input:
          m2_raw_vcf = merge_mt_outputs.merged_mt_vcf_gz,
          m2_raw_vcf_tbi = merge_mt_outputs.merged_mt_vcf_gz_tbi,
          mutect2_stats = merge_mt_outputs.merged_m2_stats,
          mt_target_intervals = target_mt_intervals,
          contamination = mt_haplochecker.minor_level,
          max_alt_allele_count = max_alt_allele_count,
          vaf_filter_threshold = vaf_filter_threshold,
          f_score_beta = f_score_beta,
          min_alt_reads = min_alt_reads,
          sample_id = sample_id
  }

  if (defined(hc_vcf_gz)) {
      call vcf_concat_task.vcf_concat as merge_hc_and_m2_vcfs {
         input:
             vcf_gz = select_all([hc_vcf_gz, mt_m2_vcf_filter.filtered_mt_vcf_gz]),
             vcf_basename = sample_id,
             use_gatk = false
      }
      call bam_concat_task.bam_concat {
         input:
              interval_bams = select_all([hc_bamout, merge_mt_outputs.merged_m2_mt_bam]),
              sample_id = sample_id,
              output_name = bam_concat_name,
              just_gather = false,
              merge_seq_dict = true,
              validation_level = "SILENT"
      }
  }


  # 7. Merge bco, stdout, stderr
  Array[File] bco_tasks = select_all([mt_extract_reads.bco, normal_alignment.bco, normal_varcall.bco,
                                      shifted_alignment.bco, shifted_varcall.bco, merge_mt_outputs.bco,
                                      mt_haplochecker.bco, mt_m2_vcf_filter.bco, merge_hc_and_m2_vcfs.bco, bam_concat.bco
                                     ])
  Array[File] stdout_tasks = select_all([mt_extract_reads.stdout_log, normal_alignment.stdout_log, normal_varcall.stdout_log,
                                        shifted_alignment.stdout_log, shifted_varcall.stdout_log, merge_mt_outputs.stdout_log,
                                        mt_haplochecker.stdout_log, mt_m2_vcf_filter.stdout_log, merge_hc_and_m2_vcfs.stdout_log,
                                        bam_concat.stdout_log
                                        ])
  Array[File] stderr_tasks = select_all([mt_extract_reads.stderr_log, normal_alignment.stderr_log, normal_varcall.stderr_log,
                                         shifted_alignment.stderr_log, shifted_varcall.stderr_log, merge_mt_outputs.stderr_log,
                                         mt_haplochecker.stderr_log, mt_m2_vcf_filter.stderr_log, merge_hc_and_m2_vcfs.stderr_log,
                                          bam_concat.stderr_log
                                        ])


  call bco_merge_task.bco_merge {
      input:
          bco_array = bco_tasks,
          stdout_array = stdout_tasks,
          stderr_array = stderr_tasks,
          module_name = module_name,
          module_version = module_version
  }


  output {

    # Final all contigs vcf
    File? allcontigs_vcf_gz = merge_hc_and_m2_vcfs.concatenated_vcf_gz
    File? allcontigs_vcf_gz_tbi = merge_hc_and_m2_vcfs.concatenated_vcf_gz_tbi

    # Final all contigs bamout
    File? allcontigs_bamout = bam_concat.bam
    File? allcontigs_bamout_bai = bam_concat.bai

    # Final mt vcf
    File mt_filtered_vcf_gz = mt_m2_vcf_filter.filtered_mt_vcf_gz
    File mt_filtered_vcf_gz_tbi = mt_m2_vcf_filter.filtered_mt_vcf_gz_tbi

    # Rejected variants vcf
    File mt_rejected_vcf_gz = mt_m2_vcf_filter.rejected_mt_vcf_gz
    File mt_rejected_vcf_gz_tbi = mt_m2_vcf_filter.rejected_mt_vcf_gz_tbi

    # Mutect2 bam with artificial haplotypes
    File mutect2_mt_bam = merge_mt_outputs.merged_m2_mt_bam
    File mutect2_mt_bai = merge_mt_outputs.merged_m2_mt_bai

    # Bam from repeated alignment to "normal" MT reference
    File realigned_mt_bam = normal_alignment.realigned_mt_bam
    File realigned_mt_bai = normal_alignment.realigned_mt_bai

    # Haplochecker contamination report
    File contamination_report = mt_haplochecker.contamination_file

    # Duplicates metrics
    File duplication_metrics = normal_alignment.metrics_file

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}
