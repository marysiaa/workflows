import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.0.14/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/interval-group@1.1.1/src/main/wdl/tasks/interval-group/interval-group.wdl" as interval_group_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-gatk-hc@1.2.6/src/main/wdl/tasks/bam-gatk-hc/bam-gatk-hc.wdl" as bam_gatk_hc_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-merge@1.0.1/src/main/wdl/tasks/gvcf-merge/latest/gvcf-merge.wdl" as gvcf_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-uniq@1.1.1/src/main/wdl/tasks/vcf-uniq/vcf-uniq.wdl" as vcf_uniq_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-remove-tags@1.0.0/src/main/wdl/tasks/gvcf-remove-tags/gvcf-remove-tags.wdl" as gvcf_remove_tags_task 
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-call-variants@1.1.4/src/main/wdl/tasks/gvcf-call-variants/gvcf-call-variants.wdl" as gvcf_call_variants_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-concat@1.1.2/src/main/wdl/tasks/bam-concat/bam-concat.wdl" as bam_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.1.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow bam_varcalling {
  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'Variant calling'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Workflow for variant calling.'
    changes: '{"1.3.12": "add remove_tags input for PGx", "1.3.11": "add task gvcf-remove-tag if dragmap", "1.3.10": "change boolean dragen_mode", "1.3.9": "add dragstr_model to HC", "1.3.8": "add dragen-mode to HC", "1.3.7": "ignore soft clipped bases option for HC", "1.3.6": "gvcf name change", "1.3.5": "Two tasks optional -  not needed in pgx.wdl pipeline (run_bam_concat_task and run_gvcf_call_variants_task)","1.3.4": "new resources kit added", "1.3.3": "gatk hc java_mem as argument", "1.3.2": "tag increment for bam-gatk-hc"}'

    input_bam: '{"name": "Bam", "type": "File", "description": "Add bam file with marked duplicated reads"}'
    input_bai: '{"name": "Bam index", "type": "File", "description": "Add index for the bam file"}'
    input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Enter a sample name (or identifier)"}'
    input_interval_list: '{"hidden":"true", "name": "Inteval file", "type": "File", "extension": [".interval_list"], "description": "Add genomic intervals"}'
    input_kit: '{"name": "Kit", "type": "String", "constraints": {"values": ["exome-v6", "exome-v7", "genome"]}, "description": "Choose comprehensive exome kit from exome-v6 for sure-select exome v6 or exome-v7 for sure-select exome v7. For WGS sample choose genome"}'
    input_ignore_soft_clips: '{"name": "Ignore soft clips?", "type": "Boolean", "default": true, "description": "Decides whether HC should analyse soft clipped bases"}'

    output_gvcf_gz: '{"name": "Gvcf gz", "type": "File", "description": "Merged bgzipped gVCF file"}'
    output_gvcf_gz_tbi: '{ "name": "Gvcf index", "type": "File", "description": "Merged gVCF file index"}'
    output_vcf_gz: '{"name": "Vcf gz","type": "File", "description": "Merged bgzipped VCF file"}'
    output_vcf_gz_tbi: '{ "name": "Vcf index", "type": "File", "description": "Merged VCF file index"}'
    output_haplotype_caller_bam: '{"name": "Bam from HaplotypeCaller", "type": "File", "description": "Realigned bam file produced by HC"}'
    output_haplotype_caller_bai: '{"name": "Bam index", "type": "File", "description": "Index file for the realigned bam"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  String module_name = "bam_varcalling"
  String module_version = "1.3.12"

  File input_bam
  File input_bai
  String bam_concat_name = 'realigned-haplotypecaller'

  # dragen-mode
  File? dragstr_model
  Boolean dragen_mode = if defined(dragstr_model) then true else false
  Boolean remove_tags = false

  String sample_id = "no_id_provided"
  String reference_genome = "grch38-no-alt" # or hg38

  File? interval_list
  String kit = "exome-v7" # or "exome-v6" or "genome"
  Boolean is_interval_list_not_defined = !defined(interval_list)
  Int max_no_pieces_to_scatter_an_interval_file = 2
  String haplotype_caller_java_mem = "-Xms4g -Xmx63g" # use "-Xms7g -Xmx7g" on anakin
  Boolean ignore_soft_clips = true
  Float contamination = 0.0

  # Tasks not needed to pgx.wdl pipeline
  Boolean run_bam_concat_task = true
  Boolean run_gvcf_call_variants_task = true
  
  ### fills interval_list if not declared ###
  if(is_interval_list_not_defined) {
    call resources_kit_task.resources_kit {
      input:
      kit = kit,
      reference_genome = reference_genome
    }
    File interval_resources = resources_kit.interval_list[0]
  }

  File interval_file = select_first([interval_list, interval_resources])

  ##########################################
  # 1. Variant calling and genotyping gVCF #
  ##########################################

  # i. Create grouped calling intervals
  call interval_group_task.interval_group {
    input:
      interval_file = interval_file,
      max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file
  }

  # ii. Call variants in parallel over grouped calling intervals
  scatter (index in range(length(interval_group.grouped_calling_intervals))) {
  call bam_gatk_hc_task.bam_gatk_hc {
    input:
      index = index,
      recalibrated_markdup_bam = input_bam,
      recalibrated_markdup_bai = input_bai,
      sample_id = sample_id,
      interval_list = interval_group.grouped_calling_intervals[index],
      java_mem = haplotype_caller_java_mem,
      ignore_soft_clips = ignore_soft_clips,
      contamination = contamination,
      reference_genome = reference_genome,
      dragen_mode = dragen_mode,
      dragstr_model = dragstr_model
    }
  }

  # iii. Merge per-interval GVCFs
  call gvcf_merge_task.gvcf_merge {
    input:
      gvcf_gzs = bam_gatk_hc.gvcf_gz,
      gvcf_gz_tbis = bam_gatk_hc.gvcf_gz_tbi,
      sample_id = sample_id
  }

  # iv. Check for duplicated records in gVCF
  call vcf_uniq_task.vcf_uniq {
    input:
      gvcf_gz = gvcf_merge.gvcf_gz,
      sample_id = sample_id
  }

  # v. Remove tags (GP and PG) from gVCF if dragen (workaround for bug in GATK https://bit.ly/3bJXkpl)
  if ((dragen_mode) || (remove_tags)) {
    call gvcf_remove_tags_task.gvcf_remove_tags {
      input:
        tags = ["GP", "PG"],
        gvcf_gz = vcf_uniq.gvcf_uniq_gz,
        sample_id = sample_id
    }
  }

  File gvcf_uniq_or_removed_tags_gz = select_first([gvcf_remove_tags.gvcf_removed_tags_gz, vcf_uniq.gvcf_uniq_gz])
  File gvcf_uniq_or_removed_tags_gz_tbi = select_first([gvcf_remove_tags.gvcf_removed_tags_gz_tbi, vcf_uniq.gvcf_uniq_gz_tbi])

  # vi. Genotype GVCF
  if (run_gvcf_call_variants_task) {
    call gvcf_call_variants_task.gvcf_call_variants {
      input:
        gvcf_gz = gvcf_uniq_or_removed_tags_gz,
        gvcf_gz_tbi = gvcf_uniq_or_removed_tags_gz_tbi,
        sample_id = sample_id
    }
  }

  # vii. Concatenate recalibrated bam files
  if (run_bam_concat_task) {
    call bam_concat_task.bam_concat {
      input:
        interval_bams = bam_gatk_hc.realigned_bam,
        #interval_bais = bam_gatk_hc.realigned_bai,
        sample_id = sample_id,
        output_name = bam_concat_name,
        need_sort = "yes"
    }
  }

  # Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([resources_kit.bco, interval_group.bco, gvcf_merge.bco, bam_concat.bco, gvcf_call_variants.bco, vcf_uniq.bco, gvcf_remove_tags.bco])
  Array[File] stdout_tasks = select_all([resources_kit.stdout_log, interval_group.stdout_log, gvcf_merge.stdout_log, bam_concat.stdout_log, gvcf_call_variants.stdout_log, vcf_uniq.stdout_log, gvcf_remove_tags.stdout_log])
  Array[File] stderr_tasks = select_all([resources_kit.stderr_log, interval_group.stderr_log, gvcf_merge.stderr_log, bam_concat.stderr_log, gvcf_call_variants.stderr_log, vcf_uniq.stderr_log, gvcf_remove_tags.stderr_log])

  Array[Array[File]] bco_scatters = [bco_tasks, bam_gatk_hc.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, bam_gatk_hc.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, bam_gatk_hc.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  ###########
  # Outputs #
  ###########

  output {

    Array[File] grouped_calling_intervals = interval_group.grouped_calling_intervals

    File gvcf_gz = gvcf_uniq_or_removed_tags_gz
    File gvcf_gz_tbi = gvcf_uniq_or_removed_tags_gz_tbi

    File? vcf_gz = gvcf_call_variants.vcf_gz
    File? vcf_gz_tbi = gvcf_call_variants.vcf_gz_tbi

    File? haplotype_caller_bam = bam_concat.bam
    File? haplotype_caller_bai = bam_concat.bai

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}
