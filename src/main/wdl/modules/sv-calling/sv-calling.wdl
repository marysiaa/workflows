import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.8/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-get-contigs@1.0.0/src/main/wdl/tasks/bam-get-contigs/bam-get-contigs.wdl" as bam_get_contigs_task
import "https://gitlab.com/intelliseq/workflows/raw/bed-check@1.0.0/src/main/wdl/tasks/bed-check/bed-check.wdl" as bed_check_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-smoove@1.0.3/src/main/wdl/tasks/sv-smoove/sv-smoove.wdl" as sv_smoove_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-gsort@1.0.2/src/main/wdl/tasks/vcf-gsort/vcf-gsort.wdl" as vcf_gsort_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-duphold@1.0.2/src/main/wdl/tasks/sv-duphold/sv-duphold.wdl" as sv_duphold_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-typer@1.0.0/src/main/wdl/tasks/sv-typer/sv-typer.wdl" as sv_typer_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-filter@1.0.2/src/main/wdl/tasks/sv-filter/sv-filter.wdl" as sv_filter_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-exon-depth@1.0.4/src/main/wdl/tasks/bam-exon-depth/bam-exon-depth.wdl" as bam_exon_depth_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-annotsv@2.0.4/src/main/wdl/tasks/sv-annotsv/sv-annotsv.wdl" as sv_annotsv_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-chrom@1.2.0/src/main/wdl/tasks/vcf-split-chrom/vcf-split-chrom.wdl" as vcf_split_chrom_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-vep@1.3.0/src/main/wdl/tasks/vcf-anno-vep/vcf-anno-vep.wdl" as vcf_anno_vep_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.4.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-hpo@1.1.1/src/main/wdl/tasks/vcf-anno-hpo/vcf-anno-hpo.wdl" as vcf_anno_hpo_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-func-gene@1.3.6/src/main/wdl/tasks/vcf-anno-func-gene/vcf-anno-func-gene.wdl" as vcf_anno_func_gene_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-ghr@1.0.5/src/main/wdl/tasks/vcf-anno-ghr/vcf-anno-ghr.wdl" as vcf_anno_ghr_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-anno-panel@1.1.1/src/main/wdl/tasks/sv-anno-panel/sv-anno-panel.wdl" as sv_anno_panel_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-annot-filter@1.2.2/src/main/wdl/tasks/sv-annot-filter/sv-annot-filter.wdl" as sv_annot_filter_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-breaks-pos@1.0.1/src/main/wdl/tasks/sv-breaks-pos/sv-breaks-pos.wdl" as sv_breaks_pos_task
import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@2.0.2/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/string-gp-to-json@2.1.1/src/main/wdl/tasks/string-gp-to-json/string-gp-to-json.wdl" as string_gp_to_json_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@1.0.4/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/raw/igv-screenshots@3.2.1/src/main/wdl/tasks/igv-screenshots/igv-screenshots.wdl" as igv_screenshots_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-cov-plot@1.0.3/src/main/wdl/tasks/sv-cov-plot/sv-cov-plot.wdl" as sv_cov_plot_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-report@1.0.0/src/main/wdl/tasks/sv-report/sv-report.wdl" as sv_report_task
import "https://gitlab.com/intelliseq/workflows/-/raw/report-acmg@2.4.17/src/main/wdl/tasks/report-acmg/report-acmg.wdl"  as report_acmg_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow sv_calling {
    String module_name = "sv_calling"
    String module_version = "1.9.4"
    String sample_id = "no_id_provided"
    String genome_or_exome = "genome" # possible values genome, exome, target
    Boolean run_as_wgs = if genome_or_exome == "genome" then true else false

    File bam
    File bai
    File? all_gene_panel
    File? gene_panel

    ## File with info on short variants may be used by duphold to add info about heterozygous sites
    ## Use if possible, as it allows for spurious deletion removal (by duphold, outside smoove task)
    File? vcf_gz
    File? vcf_gz_tbi

    ## Low complexity regions may be excluded from smoove analysis,
    ## Two files with such regions are provided (in docker) and can be chosen "hall-lab" or "btu356"
    ## "hall-lab" (https://github.com/hall-lab/speedseq/raw/master/annotations/exclude.cnvnator_100bp.GRCh38.20170403.bed)
    ## "btu356" (https://doi.org/10.1093/bioinformatics/btu356, Supplementary materials)
    Boolean exclude_lcr = true
    String lcr_bed_source = "hall-lab"

    ## Some chromosomes may be excluded from smoove analysis
    Boolean exclude_chroms = true # This option decides whether exclude non canonical chromosomes from lumpy/smoove analysis
    String chroms_string = "chrM,~chrUn,~random,~:,~decoy,~_alt,chrEBV,hs38d1,chrMT" # Comma-delimited list of chromosomes - SVs with either end in chromosomes from this list will be ignored. If chromosome name starts with ~ it is treated as a regular expression to exclude. Note, that all contigs which are not present in the chosen reference genome fasta file (but are present in analyzed bam) must be excluded. Note, that this option works only if exclude_chroms option is set as true
    Boolean run_svtyper_by_smoove = true
    Boolean run_duphold_by_smoove = if (defined(vcf_gz) && defined(vcf_gz_tbi)) then false else true

    ## genome used for alignment ("hg38" or "grch38-no-alt"), used in sv-duphold and smoove tasks
    String reference_genome = "grch38-no-alt"

    ## exome and target depth analysis inputs 
    File? ref_counts ## must be provided for ExomeDepth analysis
    ## For exome or targeted seq kit or custom bed must be provided
    File? custom_bed
    String? kit = "exome-v7" # possible choices exome-v8, exome-v7, exome-v6

    ## Filtering options
    Float min_qual = 30 # Minimal qual of valid variant (sv-filter)
    Float del_cov_max = 0.7 # Maximal coverage of valid deletion (compared to +/-1000bp flanking regions and to genome bins of similar GC content)(sv-filter)
    Float dup_cov_min = 1.3 # Minimal coverage of valid duplication (compared to +/-1000bp flanking regions and to genome bins of similar GC content) (sv-filter)
    Int min_snp_count = 4 # "Minimal number of SNP/INDELs in deletion region to apply heterozygosity filter (sv-filter)
    Float het_max = 0.25 # Maximal heterozygosity of valid deletion (sv-filter)
    Float bf_threshold = 0 # Minimal BF of valid variant (sv-filter)

    ## AnnotSV options and files
    Int promoter_size = 500
    Int sv_minimum_size = 50
    Float panel_threshold1 = 30.0
    Float panel_threshold2 = 45.0
    String include_ci = "yes"   # This option decides whether variant should be extended by its confidence intervals; possible values: "yes", "no"

    ## VEP options
    String other_options = "--no_intergenic --coding_only --hgvs --symbol --biotype --uniprot --total_length --gene_phenotype --overlaps --protein --domains"
    String transcript_filtering = "--exclude_predicted --gencode_basic --pick_allele_gene --pick_order mane,appris,biotype,canonical,tsl,ccds,length --transcript_filter 'biotype match protein_coding'"
    String transcript_info = "--appris --tsl --xref_refseq --mane"

    ## Generate panel inputs
    Array[String]? hpo_terms
    Array[String]? genes
    Array[String]? diseases
    String? phenotypes_description
    Array[String]? panel_names

    Boolean create_panel = (defined(hpo_terms) || defined(genes) || defined(diseases) || defined(phenotypes_description) || defined(panel_names))
    String? panel_json
    Boolean require_panel = (create_panel || defined(panel_json) || defined(gene_panel))

    ## Annotation filtering options
    ## Task sv-anno-panel adds panel annotations to vcf
    ## Task sv-annot-filter filters vcf on panel (turned off) and impact (turned on)
    Boolean apply_panel_filter = true
    Boolean apply_rank_filter = true # Determines whether SV variants should be filtered depending on their predicted effect
    String rank_threshold = "likely_pathogenic"

    ## IGV options
    Boolean create_pictures = true
    Int range = 300
    Boolean softclip = false
    Int igv_track_height = 345
    Int max_igv_picture_processing_time_in_ms = 15000
    Boolean view_as_pairs = true

    ## Coverage plot options
    Boolean create_coverage_plots = true

    ## Report options
    Boolean create_report = true
    String sample_sex = "U"

    # Patient information
    File? sample_info_json

    # Report information (time zone difference and ID)
    Int timezoneDifference = 0
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    String analysisEnvironment = "IntelliseqFlow"

    # 0. Bed as json for report
    if (genome_or_exome == "target") {
        call bed_to_interval_list_task.bed_to_interval_list {
            input:
                bed = custom_bed,
                reference_genome = reference_genome,
                sample_id = sample_id
        }
        File genes_bed_target_json = bed_to_interval_list.genes_bed_target_json
    }

    # i. Extract contig info from bam
    call bam_get_contigs_task.bam_get_contigs {
        input:
            bam = bam,
            bai = bai
    }

    if (run_as_wgs) {
        # ii. Call variants with smoove/lumpy
        call sv_smoove_task.sv_smoove {
            input:
                bam = bam,
                bai = bai,
                exclude_lcr = exclude_lcr,
                lcr_bed_source = lcr_bed_source,
                exclude_chroms = exclude_chroms,
                chroms_string = chroms_string,
                run_duphold = run_duphold_by_smoove,
                run_svtyper = run_svtyper_by_smoove,
                sample_id = sample_id,
                reference_genome = reference_genome
        }

        # iii. Run duphold (adds annotations useful for filtering spurious variants)
        if (!run_duphold_by_smoove) {
            call sv_duphold_task.sv_duphold {
                input:
                    bam = bam,
                    bai = bai,
                    svvcf_gz = sv_smoove.sv_vcf_gz,
                    svvcf_gz_tbi = sv_smoove.sv_vcf_gz_csi,
                    snpvcf_gz = vcf_gz,
                    snpvcf_gz_tbi = vcf_gz_tbi,
                    reference_genome = reference_genome,
                    sample_id = sample_id
            }
        }

        # iv. Run svtyper (added to allow genotyping after running other callers)
        if (!run_svtyper_by_smoove) {
            call sv_typer_task.sv_typer {
                input:
                    bam = bam,
                    bai = bai,
                    svvcf_gz = select_first([sv_duphold.duphold_vcf, sv_smoove.sv_vcf_gz]),
                    svvcf_gz_tbi = select_first([sv_duphold.duphold_tbi, sv_smoove.sv_vcf_gz_csi]),
                    sample_id = sample_id
            }

            # v. Sort svtyper vcf
            call vcf_gsort_task.vcf_gsort as svtyper_gsort {
                input:
                    vcf_gz = sv_typer.gt_svvcf_gz,
                    contig_tab = bam_get_contigs.contig_tab
            }
        }

        # vi. Remove low confidence variants
        call sv_filter_task.sv_filter {
            input:
                svvcf = select_first([svtyper_gsort.sorted_vcf_gz, sv_duphold.duphold_vcf, sv_smoove.sv_vcf_gz]),
                svvcf_tbi = select_first([svtyper_gsort.sorted_vcf_gz_tbi, sv_duphold.duphold_tbi, sv_smoove.sv_vcf_gz_csi]),
                min_qual = min_qual,
                del_cov_max = del_cov_max,
                dup_cov_min = dup_cov_min,
                del_cov_gcbin_max = del_cov_max,
                dup_cov_gcbin_min = dup_cov_min,
                min_snp_count = min_snp_count,
                het_max = het_max,
                sample_id = sample_id
        }
        Boolean wgs_variants_present = sv_filter.vcf_not_empty
    }

    if (!run_as_wgs){
        if (defined(custom_bed)) {
            call bed_check_task.bed_check {
                input:
                    bed = custom_bed
            }
        }

        call bam_exon_depth_task.bam_exon_depth{
            input:
                ref_counts = ref_counts,
                custom_bed = bed_check.cnv_bed,
                kit = kit,
                bam = bam,
                bai = bai,
                bf_threshold = bf_threshold,
                sample_id = sample_id
        }

        Boolean wes_variants_present = bam_exon_depth.vcf_not_empty
    }

    Boolean variants_present = select_first([wes_variants_present, wgs_variants_present])

    if (variants_present) {
        File vcf_to_annotation = select_first([sv_filter.sv_vcf_gz, bam_exon_depth.cnv_vcf_gz])
        File tbi_to_annotation = select_first([sv_filter.sv_vcf_gz_tbi, bam_exon_depth.cnv_vcf_gz_tbi])

        if (require_panel) {
            if (!defined(gene_panel)) {
                # vii. Create panel (optionally)
                if(!defined(panel_json)) {
                    call panel_generate_task.panel_generate {
                        input:
                            hpo_terms = hpo_terms,
                            genes = genes,
                            diseases = diseases,
                            phenotypes_description = phenotypes_description,
                            panel_names = panel_names,
                            sample_id = sample_id
                    }
                }

                if(defined(panel_json)) {
                    call string_gp_to_json_task.string_gp_to_json {
                        input:
                            panel_json = panel_json,
                            hpo_terms = hpo_terms,
                            genes = genes,
                            diseases = diseases,
                            phenotypes_description = phenotypes_description,
                            panel_names = panel_names,
                            sample_id = sample_id
                    }
                }
                File panel_ready_or_created = select_first([panel_generate.panel, string_gp_to_json.validated_panel_json])
                File panel_inputs_ready_or_created = select_first([panel_generate.inputs, string_gp_to_json.validated_panel_inputs_json])
                File all_panels_json_ready_or_created = select_first([panel_generate.all_panels, string_gp_to_json.all_panels])
            }
            File? panel_to_use = select_first([gene_panel, panel_ready_or_created])
            File? full_panel = select_first([all_gene_panel, all_panels_json_ready_or_created])
        }

        #viii. Split vcf into chromosome wise vcf files ## update version of this task
        call vcf_split_chrom_task.vcf_split_chrom {
            input:
                vcf_basename = sample_id,
                vcf_gz = vcf_to_annotation,
                vcf_gz_tbi = tbi_to_annotation
        }

        # ix. Add vep annotations
        Array[File] chromosome_vcfs = vcf_split_chrom.chromosomes_vcf_gz
        Array[File] chromosome_tbis = vcf_split_chrom.chromosomes_vcf_gz_tbi

        scatter (index in range(length(chromosome_vcfs))) {
            String string_to_remove = "-" + sample_id + ".*"
            String chrom_from_file_name = sub(basename(chromosome_vcfs[index]), string_to_remove, "")
            call vcf_anno_vep_task.vcf_anno_vep {
                input:
                    index = index,
                    chromosome = chrom_from_file_name,
                    sample_id = sample_id,
                    vcf_gz = chromosome_vcfs[index],
                    vcf_gz_tbi = chromosome_tbis[index],
                    add_loftee = false,
                    add_dbscsnv = false,
                    other_options = other_options,
                    transcript_filtering = transcript_filtering,
                    transcript_info = transcript_info
            }
        }

        # x. Concatenate chromosome vcf file
        call vcf_concat_task.vcf_concat {
            input:
                vcf_gz = vcf_anno_vep.anno_vcf_gz,
                vcf_basename = sample_id,
                beagle_merge = !run_as_wgs
        }

        # xi. AnnotSV annotation
        call sv_annotsv_task.sv_annotsv {
            input:
                sv_vcf_gz = vcf_concat.concatenated_vcf_gz,
                sv_vcf_gz_tbi = vcf_concat.concatenated_vcf_gz_tbi,
                panel_file = full_panel,
                promoter_size = promoter_size,
                sv_minimum_size = sv_minimum_size,
                panel_threshold1 = panel_threshold1,
                panel_threshold2 = panel_threshold2,
                include_ci = include_ci,
                sample_id = sample_id
        }

        # xii. Sort AnnotSV vcf
        call vcf_gsort_task.vcf_gsort as annotsv_gsort {
            input:
                vcf_gz = sv_annotsv.annotated_vcf_gz,
                contig_tab = bam_get_contigs.contig_tab
        }

        # xiii. Annotate with hpo
        call vcf_anno_hpo_task.vcf_anno_hpo {
            input:
                vcf_gz = annotsv_gsort.sorted_vcf_gz,
                vcf_gz_tbi = annotsv_gsort.sorted_vcf_gz_tbi,
                sv = true,
                vcf_basename = sample_id
        }

        # xiii. Annotate with hpo phenotypes and inheritance
        call vcf_anno_func_gene_task.vcf_anno_func_gene {
            input:
                vcf_gz = vcf_anno_hpo.annotated_with_hpo_vcf_gz,
                vcf_gz_tbi = vcf_anno_hpo.annotated_with_hpo_vcf_gz_tbi,
                genes_symbol_field_name = "Gene_name",
                field_delimiter = "\\|",
                vcf_basename = sample_id
        }

        # xiii. Annotate with ghr diseases
        call vcf_anno_ghr_task.vcf_anno_ghr {
            input:
                vcf_gz = vcf_anno_func_gene.hpo_annotated_vcf_gz,
                vcf_gz_tbi = vcf_anno_func_gene.hpo_annotated_vcf_gz_tbi,
                sv = true,
                vcf_basename = sample_id
        }


        # xiv. Annotate with panel scores
        call sv_anno_panel_task.sv_anno_panel {
            input:
                vcf_gz = vcf_anno_ghr.annotated_with_ghr_vcf_gz,
                vcf_gz_tbi = vcf_anno_ghr.annotated_with_ghr_vcf_gz_tbi,
                gene_panel = panel_to_use,
                sample_id = sample_id
        }

        # xv. Apply filters (gene panel, frequency, ISEQ classification)
        call sv_annot_filter_task.sv_annot_filter {
            input:
                vcf_gz = sv_anno_panel.panel_anno_vcf_gz,
                vcf_gz_tbi = sv_anno_panel.panel_anno_vcf_gz_tbi,
                apply_panel_filter = (defined(panel_to_use) && apply_panel_filter),
                apply_rank_filter = apply_rank_filter,
                rank_threshold = rank_threshold,
                sample_id = sample_id
        }

        # xvi. generate json from annotated and filtered vcf
        call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv as create_filtered_json {
            input:
                vcf_gz = sv_annot_filter.filtered_vcf_gz,
                vcf_gz_tbi = sv_annot_filter.filtered_vcf_gz_tbi,
                sample_id = sample_id,
                output_formats = 'json',
                sv_analysis = true,
                split_ANN_field = false,
                split_CSQ_field = false,
                split_ISEQ_REPORT_ANN_field = true
        }

        if (create_pictures && run_as_wgs) { 
            # xvii. Create list of positions for IGV
            call sv_breaks_pos_task.sv_breaks_pos {
                input:
                    vcf_gz = sv_annot_filter.filtered_vcf_gz,
                    sample_id = sample_id
            }

            # xviii. IGV screenshots
            call igv_screenshots_task.igv_screenshots {
                input:
                    positions_list = sv_breaks_pos.position_list,
                    bams = [bam],
                    sample_id = sample_id,
                    range = range,
                    softclip = softclip,
                    track_height = igv_track_height,
                    max_picture_processing_time_in_ms = max_igv_picture_processing_time_in_ms,
                    view_as_pairs = view_as_pairs
            }

            Int igv_output_length = length(igv_screenshots.compressed_igv_pngs)
            if(igv_output_length != 0 ) {
                File pictures_gz = igv_screenshots.compressed_igv_pngs[0]
            }
        }

        # xix. create coverage plots
        if (create_coverage_plots && run_as_wgs) {
            call sv_cov_plot_task.sv_cov_plot {
                input:
                    vcf = sv_annot_filter.filtered_vcf_gz,
                    tbi = sv_annot_filter.filtered_vcf_gz_tbi,
                    bam = bam,
                    bai = bai,
                    sample_id = sample_id
            }
        }
    }

    # xx. generate csv from full annotated vcf/ or empty genotyped vcf
    call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv as create_full_tsv {
        input:
            vcf_gz = select_first([sv_anno_panel.panel_anno_vcf_gz, sv_filter.sv_vcf_gz, bam_exon_depth.cnv_vcf_gz]),
            vcf_gz_tbi = select_first([sv_anno_panel.panel_anno_vcf_gz_tbi, sv_filter.sv_vcf_gz_tbi, bam_exon_depth.cnv_vcf_gz_tbi]),
            sample_id = sample_id,
            output_formats = 'tsv',
            sv_analysis = true,
            split_ANN_field = false,
            split_CSQ_field = false,
            split_ISEQ_REPORT_ANN_field = true,
            default_value = "NA"
    }

    if (create_report) {
        # xxi Generate report
        call sv_report_task.sv_report {
            input:
                sv_table = create_full_tsv.output_tsv,
                sample_id = sample_id,
                sample_sex = sample_sex
        }
    }

    # xxii. Generate pdf and docx report
    call report_acmg_task.report_acmg {
        input:
            cnv_json = create_filtered_json.output_json,
            sample_info_json = sample_info_json,
            panel_json = panel_to_use,
            panel_inputs_json = panel_inputs_ready_or_created,
            genes_bed_target_json = genes_bed_target_json,
            gene_panel_logs = panel_generate.gene_panel_logs,
            timezoneDifference = timezoneDifference,
            sample_id = sample_id,
            genome_or_exome = genome_or_exome,
            kit = kit,
            analysisDescription = analysisDescription,
            analysisName = analysisName,
            analysisWorkflow = analysisWorkflow,
            analysisWorkflowVersion = analysisWorkflowVersion,
            analysisEnvironment = analysisEnvironment,
            sv_calling_module = true,
            min_qual = min_qual,
            del_cov_max = del_cov_max,
            dup_cov_min = dup_cov_min,
            het_max = het_max,
            bf_threshold = bf_threshold
    }

    # xxi. Merge bco, stdout, stderr files
    Array[File] bco_tasks = select_all([bam_get_contigs.bco, sv_smoove.bco,  sv_duphold.bco, sv_typer.bco, svtyper_gsort.bco,
            sv_filter.bco, bam_exon_depth.bco, vcf_split_chrom.bco, vcf_concat.bco, sv_annotsv.bco,
            annotsv_gsort.bco, panel_generate.bco, sv_anno_panel.bco, sv_annot_filter.bco,
            create_filtered_json.bco, bed_check.bco, vcf_anno_hpo.bco, vcf_anno_func_gene.bco, vcf_anno_ghr.bco,
            sv_breaks_pos.bco, igv_screenshots.bco, sv_cov_plot.bco, create_full_tsv.bco, sv_report.bco, report_acmg.bco])
    Array[File] stdout_tasks = select_all([bam_get_contigs.stdout_log, sv_smoove.stdout_log, sv_duphold.stdout_log, sv_typer.stdout_log,
            svtyper_gsort.stdout_log, sv_filter.stdout_log, bam_exon_depth.stdout_log,
            vcf_split_chrom.stdout_log, vcf_concat.stdout_log, sv_annotsv.stdout_log,
            annotsv_gsort.stdout_log, panel_generate.stdout_log, sv_anno_panel.stdout_log,
            sv_annot_filter.stdout_log, create_filtered_json.stdout_log, bed_check.stdout_log,
                sv_breaks_pos.stdout_log, igv_screenshots.stdout_log, sv_cov_plot.stdout_log, vcf_anno_ghr.stdout_log,
                create_full_tsv.stdout_log, sv_report.stdout_log, vcf_anno_hpo.stdout_log,vcf_anno_func_gene.stdout_log, report_acmg.stdout_log])
    Array[File] stderr_tasks = select_all([bam_get_contigs.stderr_log, sv_smoove.stderr_log, sv_duphold.stderr_log,
            sv_typer.stderr_log, svtyper_gsort.stderr_log, sv_filter.stderr_log, bed_check.stderr_log,
            bam_exon_depth.stderr_log, vcf_split_chrom.stderr_log, vcf_concat.stderr_log,
            sv_annotsv.stderr_log, annotsv_gsort.stderr_log, panel_generate.stderr_log,
            sv_anno_panel.stderr_log, sv_annot_filter.stderr_log, create_filtered_json.stderr_log,
            sv_breaks_pos.stdout_log, igv_screenshots.stderr_log, sv_cov_plot.stderr_log, vcf_anno_ghr.stderr_log,
            create_full_tsv.stderr_log, sv_report.stderr_log, vcf_anno_hpo.stderr_log, vcf_anno_func_gene.stderr_log, report_acmg.stderr_log])

    Array[Array[File]] bco_scatters = select_all([bco_tasks, vcf_anno_vep.bco])
    Array[Array[File]] stdout_scatters = select_all([stdout_tasks, vcf_anno_vep.stdout_log])
    Array[Array[File]] stderr_scatters = select_all([stderr_tasks, vcf_anno_vep.stderr_log])

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

    File ang_docx_report = report_acmg.variants_report_ang_docx
    File ang_pdf_report = report_acmg.variants_report_ang_pdf    

    File sv_vcf = select_first([sv_filter.sv_vcf_gz, bam_exon_depth.cnv_vcf_gz])
    File sv_vcf_tbi = select_first([sv_filter.sv_vcf_gz_tbi, bam_exon_depth.cnv_vcf_gz_tbi])
    File? bam_stats_json = sv_typer.bam_stats_json
    Float? ref_to_sample_fit = bam_exon_depth.sample_to_ref_fit

    File? annotated_sv_vcf_gz = sv_anno_panel.panel_anno_vcf_gz
    File? annotated_sv_vcf_gz_tbi = sv_anno_panel.panel_anno_vcf_gz_tbi

    Array[File]? annotsv_tsv_gz = sv_annotsv.full_annotsv_tsv_gz

    File? annotated_filtered_sv_vcf_gz = sv_annot_filter.filtered_vcf_gz
    File? annotated_filtered_sv_vcf_gz_tbi = sv_annot_filter.filtered_vcf_gz_tbi

    File? filtered_json = create_filtered_json.output_json
    File? tsv_full_report = create_full_tsv.output_tsv

    File? igv_pngs = pictures_gz

    File? sv_report_pdf = sv_report.report_pdf
    File? sv_report_html = sv_report.report_html

    File? coverage_plots = sv_cov_plot.coverage_plots

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

    }

}
