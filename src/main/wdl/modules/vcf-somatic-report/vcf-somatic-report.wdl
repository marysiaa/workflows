import "https://gitlab.com/intelliseq/workflows/-/raw/vcf-select-var-somatic@1.0.0/src/main/wdl/tasks/vcf-select-var-somatic/vcf-select-var-somatic.wdl" as vcf_select_var_somatic_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@1.0.10/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/-/raw/json-prioritize-var-somatic@1.0.1/src/main/wdl/tasks/json-prioritize-var-somatic/json-prioritize-var-somatic.wdl" as json_prioritize_var_somatic_task
import "https://gitlab.com/intelliseq/workflows/-/raw/igv-screenshots@3.2.2/src/main/wdl/tasks/igv-screenshots/igv-screenshots.wdl" as igv_screenshots_task
import "https://gitlab.com/intelliseq/workflows/-/raw/report-somatic@1.0.2/src/main/wdl/tasks/report-somatic/report-somatic.wdl"  as report_somatic_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.1/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow vcf_somatic_report {

    String module_name = "vcf_somatic_report"
    String module_version = "1.1.2"

    File vcf_gz
    File vcf_gz_tbi
    Array[File]? bams
    Boolean is_bams_defined = defined(bams)
    File? panel_json
    Boolean provided_panel_json = if defined(panel_json) then true else false
    File? sample_info_json
    File? panel_inputs_json
    File? genes_bed_target_json
    File? gene_panel_logs
    Boolean tumor_only = false

    Int timezoneDifference = 0
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    String analysisEnvironment = "IntelliseqFlow"
    String genome_or_exome = "exome"
    String sample_id = "no_id_provided"
    String? kit
    Int max_igv_picture_processing_time_in_ms = 100000
    Boolean view_as_pairs = false
    Int track_height = if tumor_only then 550 else 250
    Boolean target = false   

    ## tumor sample name
    String tumor_sample_name = "tumor"

    # 1. Select variants
    call vcf_select_var_somatic_task.vcf_select_var_somatic {
        input:
            vcf = vcf_gz,
            tbi = vcf_gz_tbi,
            sample_id = sample_id,
            tumor_sample_name = tumor_sample_name,
            target = target
    }

    # 2. Convert VCF to JSON
    call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv {
        input:
            vcf_gz = vcf_select_var_somatic.filtered_vcf,
            vcf_gz_tbi = vcf_select_var_somatic.filtered_vcf_tbi,
            split_ANN_field = false,
            split_CSQ_field = false,
            split_ISEQ_REPORT_ANN_field = true,
            split_ISEQ_MANE_ANN_field = true,
            output_formats = "json",
            select_columns = true,
            sample_id = sample_id
    }
    
    # 3. Prioritize variants
    call json_prioritize_var_somatic_task.json_prioritize_var_somatic {
        input:
            vcf_json = vcf_to_json_or_tsv.output_json,
            num_var = 50,
            sample_id = sample_id
            
    }

    # 4. Generate IGV screenshots
    if (is_bams_defined) {
        call igv_screenshots_task.igv_screenshots {
            input:
                positions_list = json_prioritize_var_somatic.position_list,
                bams = bams,
                sample_id = sample_id,
                max_picture_processing_time_in_ms = max_igv_picture_processing_time_in_ms,
                view_as_pairs = view_as_pairs,
                track_height = track_height
        }
        Int igv_output_length = length(igv_screenshots.compressed_igv_pngs)
        if(igv_output_length != 0 ) {
            File pictures_gz = igv_screenshots.compressed_igv_pngs[0]
            File jigv_html = igv_screenshots.jigv_html[0]
        }
    }

    # 5. Generate report
    call report_somatic_task.report_somatic {
        input:
            var_json = json_prioritize_var_somatic.report_json,
            sample_info_json = sample_info_json,
            pictures_gz = pictures_gz,
            panel_json = panel_json,
            panel_inputs_json = panel_inputs_json,
            genes_bed_target_json = genes_bed_target_json,
            gene_panel_logs = gene_panel_logs,
            tumor_only = tumor_only,
            timezoneDifference = timezoneDifference,
            sample_id = sample_id,
            genome_or_exome = genome_or_exome,
            kit = kit,
            analysisDescription = analysisDescription,
            analysisName = analysisName,
            analysisWorkflow = analysisWorkflow,
            analysisWorkflowVersion = analysisWorkflowVersion,
            analysisEnvironment = analysisEnvironment
    }

    Array[File] bco_array = select_all([vcf_select_var_somatic.bco, vcf_to_json_or_tsv.bco, json_prioritize_var_somatic.bco, 
                                        igv_screenshots.bco, report_somatic.bco])
    Array[File] stdout_array = select_all([vcf_select_var_somatic.stdout_log, vcf_to_json_or_tsv.stdout_log, json_prioritize_var_somatic.stdout_log, 
                                        igv_screenshots.stdout_log, report_somatic.stdout_log])
    Array[File] stderr_array = select_all([vcf_select_var_somatic.stderr_log, vcf_to_json_or_tsv.stderr_log, json_prioritize_var_somatic.stderr_log, 
                                        igv_screenshots.stderr_log, report_somatic.stderr_log])

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

        File ang_docx_report = report_somatic.somatic_report_ang_docx
        File ang_pdf_report = report_somatic.somatic_report_ang_pdf
        File? igv_screenshots_tar_gz = pictures_gz
        File? jigv_html = jigv_html
        File? json_report = vcf_to_json_or_tsv.output_json
        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco
    }

}
