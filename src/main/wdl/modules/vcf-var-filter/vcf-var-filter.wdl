import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-qual@1.0.1/src/main/wdl/tasks/vcf-filter-qual/vcf-filter-qual.wdl" as vcf_filter_qual_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-site-only@1.0.0/src/main/wdl/tasks/vcf-split-site-only/vcf-split-site-only.wdl" as vcf_split_site_only_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-vqsr-snp-recalib@1.0.4/src/main/wdl/tasks/vcf-vqsr-snp-recalib/vcf-vqsr-snp-recalib.wdl" as vcf_vqsr_snp_recalib_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-vqsr-indel-recalib@1.0.3/src/main/wdl/tasks/vcf-vqsr-indel-recalib/vcf-vqsr-indel-recalib.wdl" as vcf_vqsr_indel_recalib_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-vqsr-apply@1.0.0/src/main/wdl/tasks/vcf-vqsr-apply/vcf-vqsr-apply.wdl" as vcf_vqsr_apply_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-hard@1.0.4/src/main/wdl/tasks/vcf-filter-hard/vcf-filter-hard.wdl" as vcf_filter_hard_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-ad@1.0.1/src/main/wdl/tasks/vcf-filter-ad/vcf-filter-ad.wdl" as vcf_filter_ad_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.4.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-varcall-metrics@1.0.2/src/main/wdl/tasks/vcf-varcall-metrics/vcf-varcall-metrics.wdl" as vcf_varcall_metrics_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.1.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task



workflow vcf_var_filter {

  meta {
    keywords: '{"keywords": ["variant", "filtering", "VQSR", "hard-filtering"]}'
    name: 'Variant filtering'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Variant filtering. Applies VQSR for WGS variants and hard filters to WES variants'
    changes: '{"1.0.8": "add var_call_tool and make running vqsr dependent on aligner_tool (bwa-mem) and var_call_tool (haplotypeCaller)", "1.0.7": "move vcf-filter-qual from vcf-anno, update filter hard with dragmap", "1.0.6": "update hard filtering task (QUAL filters removed)", "1.0.5": "docker gatk update", "1.0.4": "name of vcf change", "1.0.3": "docker updates"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "Vcf gz", "type": "File", "description": "Vcf file (bgzipped)"}'
    input_vcf_gz_tbi: '{"name": "Vcf gz tbi", "type": "File", "description": "Vcf file index"}'
    input_analysis_type: '{"name": "Analysis type", "type": "String", "constraints": {"values": ["exome", "genome"]},"description": "Analysis type. Decides which filtering mode will be applied (VQSR for WGS and hard filters for WES)"}'

    input_apply_ad_filter: '{"name": "Apply AD filtering?", "type": "Boolean", "description": "Decides whether apply AD filtering (for exome and genome data)"}'
    input_ad_binom_threshold: '{"name": "AD binomal threshold (AD filtering task)", "type": "Float", "description": "Threshold probability for the AD binomal test"}'
    input_vcf_array_length: '{"name": "Vcf array length", "type": "Int", "description": "Decides into how many chunks input vcf should be divided (to run in scatter vcf-vqsr-apply task)"}'
    input_indel_filter_level: '{"name": "INDEL VQSR threshold (apply VQSR task)", "type": "Float", "description": "Threshold value for VQSR recalibrated INDELs"}'
    input_snp_filter_level: '{"name": "SNP VQSR threshold (apply VQSR task)", "type": "Float", "description": "Threshold value for VQSR recalibrated SNPs"}'
    input_use_allele_specific_annotations: '{"name": "Use allele specific annotations? (VQSR tasks)", "type": "Boolean", "description": "Decides whether use allele specific annotations during VQSR recalibration"}'
    input_interval_list: '{"name": "Interval list (variant calling metrics task)", "type": "String", "description": "Interval list (should be the same as used for variant calling)"}'
    input_ref_genome: '{"name": "Reference genome (variant calling metrics task)", "type": "String", "constraints": {"values": ["hg38", "grch38-no-alt"]},"description": "Reference genome"}'

    output_filtered_vcf_gz: '{"name": "Filtered vcf gz", "type": "File", "copy": "True", "description": "VCF file with variants that passed all applied filters (bgzipped)"}'
    output_filtered_vcf_gz_tbi: '{"name": "Filtered vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the filtered vcf file"}'
    output_rejected_variants_vcfs:  '{"name": "Rejected variants vcfs gz", "type": "File[Array]", "copy": "True", "description": "VCF files with variants that failed any of filters applied (bgzipped)"}'
    output_rejected_variants_tbis:  '{"name": "Rejected variants tbis", "type": "File[Array]", "copy": "True", "description": "Indexes for the rejected variants vcfs"}'
    output_detail_metrics_file: '{"name": "Detail metrics file", "type": "File", "copy": "True", "description": "File with variant calling detailed metrics"}'
    output_summary_metrics_file: '{"name": "Summary metrics file", "type": "File", "copy": "True", "description": "File with variant calling summary metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  String module_name = "vcf_var_filter"
  String module_version = "1.0.8"

  String sample_id = "no-id"
  String aligner_tool = "bwa-mem"
  String var_call_tool = "haplotypeCaller"

  File vcf_gz
  File vcf_gz_tbi
  String analysis_type = "genome"
  Boolean apply_vqsr = false
  Boolean run_vqsr = if (apply_vqsr && analysis_type == "genome" && aligner_tool == "bwa-mem" && var_call_tool == "haplotypeCaller") then true else false

  # VQSR filter thresholds and parameters
  Float indel_filter_level = 99.0
  Float snp_filter_level = 99.7
  Boolean use_allele_specific_annotations = true
  Int vcf_array_length = 20

  # Hard filtering
  Float ad_binom_threshold = 0.01
  Boolean apply_ad_filter = true

  # Collect variant calling metrics
  File? interval_list
  String ref_genome = "hg38"

  # Set quality threshold
  Float quality_threshold = 300


  # 1. Filter VCF by quality
  call vcf_filter_qual_task.vcf_filter_qual {
      input:
          vcf_gz = vcf_gz,
          vcf_gz_tbi = vcf_gz_tbi,
          vcf_basename = sample_id,
          quality_threshold = quality_threshold
  }

  ### VQSR: for WGS

  if (run_vqsr) {
    # 2. VQSR: prepare sites only vcf and smaller vcf files
    call vcf_split_site_only_task.vcf_split_site_only {
      input:
        vcf_gz = vcf_filter_qual.filtered_by_qual_vcf_gz,
        vcf_gz_tbi = vcf_filter_qual.filtered_by_qual_vcf_gz_tbi,
        sample_id = sample_id,
        vcf_array_length = vcf_array_length
    }

    # 3. VQSR: calculate VQSLOD statistics for indels
    call vcf_vqsr_indel_recalib_task.vcf_vqsr_indel_recalib {
      input:
        sites_only_vcf_gz = vcf_split_site_only.sites_only_vcf_gz,
        sites_only_vcf_gz_tbi = vcf_split_site_only.sites_only_vcf_gz_tbi,
        sample_id = sample_id,
        use_allele_specific_annotations = use_allele_specific_annotations
    }

    # 4. VQSR: calculate VQSLOD statistics for snps
    call vcf_vqsr_snp_recalib_task.vcf_vqsr_snp_recalib {
      input:
        sites_only_vcf_gz = vcf_split_site_only.sites_only_vcf_gz,
        sites_only_vcf_gz_tbi = vcf_split_site_only.sites_only_vcf_gz_tbi,
        sample_id = sample_id,
        use_allele_specific_annotations = use_allele_specific_annotations
    }

    String vqsr_indels_basename = basename(vcf_vqsr_indel_recalib.indels_recalibration_tranches, ".tranches")
    String vqsr_snps_basename = basename(vcf_vqsr_snp_recalib.snps_recalibration_tranches, ".tranches")
    Boolean skip_vqsr = (vqsr_indels_basename == "empty" ) || (vqsr_snps_basename == "empty")

    if (!skip_vqsr) {
      Array[File] small_vcfs = vcf_split_site_only.split_vcf_gz
      Array[File] small_tbis = vcf_split_site_only.split_vcf_gz_tbi

      scatter (index in range(length(small_vcfs))) {
        # 5. VQSR: apply VQSR filter
        call vcf_vqsr_apply_task.vcf_vqsr_apply {
          input:
            index = index,
            vcf_gz = small_vcfs[index],
            vcf_gz_tbi = small_tbis[index],
            indels_recalibration = vcf_vqsr_indel_recalib.indels_recalibration_vcf,
            indels_recalibration_idx = vcf_vqsr_indel_recalib.indels_recalibration_vcf_idx,
            indels_tranches = vcf_vqsr_indel_recalib.indels_recalibration_tranches,
            snps_recalibration = vcf_vqsr_snp_recalib.snps_recalibration_vcf,
            snps_recalibration_idx = vcf_vqsr_snp_recalib.snps_recalibration_vcf_idx,
            snps_tranches = vcf_vqsr_snp_recalib.snps_recalibration_tranches,
            indel_filter_level = indel_filter_level,
            snp_filter_level = snp_filter_level,
            use_allele_specific_annotations = use_allele_specific_annotations
        }
      }

      # 6. VQSR: concatenete "good" vcfs
      call vcf_concat_task.vcf_concat as vqsr_passed_vcfs_concat {
        input:
          vcf_gz = vcf_vqsr_apply.vqsr_filtered_vcf_gz,
          vcf_basename = sample_id
      }

      # 7. VQSR: concateneate "bad" vcfs
      call vcf_concat_task.vcf_concat as vqsr_failed_vcfs_concat {
        input:
          vcf_gz = vcf_vqsr_apply.vqsr_rejected_vcf_gz,
          vcf_basename = sub(basename(vcf_vqsr_apply.vqsr_rejected_vcf_gz[0], ".vcf.gz"),"000.", "")
      }

      # 8. VQSR: concatenate recalibrated vcfs (bad sites marked only)
      call vcf_concat_task.vcf_concat as vqsr_marked_vcfs_concat {
        input:
          vcf_gz = vcf_vqsr_apply.vqsr_recalib_vcf_gz,
          vcf_basename = sub(basename(vcf_vqsr_apply.vqsr_recalib_vcf_gz[0], ".vcf.gz"), "000.", "")
      }
    }
  }

 ### Hard filtering: for WES and when vqrs failed

 # 1. Hard filtering
  if ((!run_vqsr) || (skip_vqsr)) {
    call vcf_filter_hard_task.vcf_filter_hard {
      input:
        vcf_gz = vcf_filter_qual.filtered_by_qual_vcf_gz,
        vcf_gz_tbi = vcf_filter_qual.filtered_by_qual_vcf_gz_tbi,
        reference_genome = ref_genome,
        sample_id  = sample_id,
        aligner_tool = aligner_tool
    }
  }

  # 2. Optional AD filter
  if (apply_ad_filter) {
    call vcf_filter_ad_task.vcf_filter_ad {
      input:
        vcf_gz = select_first([vcf_filter_hard.hard_filtered_vcf_gz, vqsr_passed_vcfs_concat.concatenated_vcf_gz]),
        vcf_gz_tbi = select_first([vcf_filter_hard.hard_filtered_vcf_gz_tbi, vqsr_passed_vcfs_concat.concatenated_vcf_gz_tbi]),
        sample_id = sample_id,
        ad_binom_threshold = ad_binom_threshold

    }
  }


  File final_filtered_vcf_gz = select_first([vcf_filter_ad.filtered_vcf_gz, vcf_filter_hard.hard_filtered_vcf_gz, vqsr_passed_vcfs_concat.concatenated_vcf_gz])
  File final_filtered_vcf_gz_tbi = select_first([vcf_filter_ad.filtered_vcf_gz_tbi, vcf_filter_hard.hard_filtered_vcf_gz_tbi, vqsr_passed_vcfs_concat.concatenated_vcf_gz_tbi])

  ### Variant calling stats

  call vcf_varcall_metrics_task.vcf_varcall_metrics {
    input:
      ref_genome = ref_genome,
      vcf_gz = final_filtered_vcf_gz,
      vcf_gz_tbi = final_filtered_vcf_gz_tbi,
      sample_id = sample_id,
      interval_list = interval_list
  }


  ### BCO merge

  Array[File] bco_tasks = select_all([vcf_filter_qual.bco, vcf_split_site_only.bco, vcf_vqsr_indel_recalib.bco, vcf_vqsr_snp_recalib.bco, vqsr_passed_vcfs_concat.bco, vqsr_failed_vcfs_concat.bco, vqsr_marked_vcfs_concat.bco, vcf_filter_hard.bco, vcf_filter_ad.bco, vcf_varcall_metrics.bco])
  Array[File] stdout_tasks = select_all([vcf_filter_qual.stdout_log, vcf_split_site_only.stdout_log, vcf_vqsr_indel_recalib.stdout_log, vcf_vqsr_snp_recalib.stdout_log, vqsr_passed_vcfs_concat.stdout_log, vqsr_failed_vcfs_concat.stdout_log, vqsr_marked_vcfs_concat.stdout_log, vcf_filter_hard.stdout_log, vcf_filter_ad.stdout_log, vcf_varcall_metrics.stdout_log])
  Array[File] stderr_tasks = select_all([vcf_filter_qual.stderr_log, vcf_split_site_only.stderr_log, vcf_vqsr_indel_recalib.stderr_log, vcf_vqsr_snp_recalib.stderr_log, vqsr_passed_vcfs_concat.stderr_log, vqsr_failed_vcfs_concat.stderr_log, vqsr_marked_vcfs_concat.stderr_log, vcf_filter_hard.stderr_log, vcf_filter_ad.stderr_log, vcf_varcall_metrics.stderr_log])


  Array[Array[File]] bco_scatters = select_all([bco_tasks, vcf_vqsr_apply.bco])
  Array[Array[File]] stdout_scatters = select_all([stdout_tasks, vcf_vqsr_apply.stdout_log])
  Array[Array[File]] stderr_scatters = select_all([stderr_tasks, vcf_vqsr_apply.stderr_log])

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
      bco_array = bco_array,
      stdout_array = stdout_array,
      stderr_array = stderr_array,
      module_name = module_name,
      module_version = module_version
  }


  output {

    ## Vcf with variants that passed all filters
    File filtered_vcf_gz = final_filtered_vcf_gz
    File filtered_vcf_gz_tbi = final_filtered_vcf_gz_tbi

    ## Rejected variants vcfs
    Array[File] rejected_variants_vcfs = select_all([vqsr_failed_vcfs_concat.concatenated_vcf_gz, vcf_filter_hard.hard_filter_rejected_vcf_gz, vcf_filter_ad.rejected_vcf_gz])
    Array[File] rejected_variants_tbis = select_all([vqsr_failed_vcfs_concat.concatenated_vcf_gz_tbi, vcf_filter_hard.hard_filter_rejected_vcf_gz_tbi, vcf_filter_ad.rejected_vcf_gz_tbi])

    ## Variant calling mertics
    File detail_metrics_file = vcf_varcall_metrics.detail_metrics_file
    File summary_metrics_file = vcf_varcall_metrics.summary_metrics_file

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}