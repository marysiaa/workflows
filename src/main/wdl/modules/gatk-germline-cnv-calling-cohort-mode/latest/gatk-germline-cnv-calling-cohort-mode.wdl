# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: GATK Germline CNV Calling - COHORT mode
#  version: latest
#  authors:
#    - https://gitlab.com/lltw
#  copyright: Copyright 2019 Intelliseq
#  description: |
#    This module implements a pipeline for Germline CNV discovery consisted of GATK4 tools running in COHORT mode.
#  changes:
#    latest:
#      - no changes
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/extract-samples-ids-from-bams/latest/extract-samples-ids-from-bams.wdl" as extract_samples_ids_from_bams_task
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/collect-read-counts/latest/collect-read-counts.wdl" as collect_read_counts_task
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/determine-germline-contig-ploidy-cohort-mode/latest/determine-germline-contig-ploidy-cohort-mode.wdl" as determine_germline_contig_ploidy_cohort_mode_task
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/divide-interval-list-chromosome-wise/latest/divide-interval-list-chromosome-wise.wdl" as divide_interval_list_chromosome_wise_task
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/run-germline-cnv-caller-cohort-mode/latest/run-germline-cnv-caller-cohort-mode.wdl" as run_germline_cnv_caller_cohort_mode_task
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/postprocess-germline-cnv-calls/latest/postprocess-germline-cnv-calls.wdl" as postprocess_germline_cnv_calls_task


workflow gatk_germline_cnv_calling_cohort_mode {

# @Input (name = "BAM files", desc = "BAM files must have 'SM' field (sample) in '@RG' header line (read groups) specified.")
  Array[File] bams

# @Input (name = "BAI indices")
  Array[File] bai_indices

# @Input (name = "Interval list")
  File interval_list

# @Input(name = "Cohort name")GermlineCNVCaller
  String cohort_name = "no_cohort_name_provided"

  Boolean chr_prefix = true

  # i. Get samples IDs from BAMs headers ("@RG" header line - "SM" field)
  call extract_samples_ids_from_bams_task.extract_samples_ids_from_bams {
    input:
      bams = bams
  }

  Array[File] samples_ids = extract_samples_ids_from_bams.samples_ids

  # ii. Collect read counts
  scatter (index in range(length(bams))) {
    call collect_read_counts_task.collect_read_counts {
      input:
        bam = bams[index],
        bai = bai_indices[index],
        sample_id = samples_ids[index],
        interval_list = interval_list
    }
  }

  # iii. Determine germline contig ploidy
  call determine_germline_contig_ploidy_cohort_mode_task.determine_germline_contig_ploidy_cohort_mode {
    input:
      counts_hdf5 = collect_read_counts.counts_hdf5,
      cohort_name = cohort_name
  }

  # iv. Divide interval_list file-chromosome wise
  call divide_interval_list_chromosome_wise_task.divide_interval_list_chromosome_wise {
    input:
      interval_list = interval_list,
      chr_prefix = chr_prefix
  }

  # v. Run germline CNV caller
  scatter (index in range(length(divide_interval_list_chromosome_wise.interval_lists))) {
    call run_germline_cnv_caller_cohort_mode_task.run_germline_cnv_caller_cohort_mode {
      input:
        counts_hdf5 = collect_read_counts.counts_hdf5,
        interval_list = divide_interval_list_chromosome_wise.interval_lists[index],
        germline_contig_ploidy_cohort_calls_tar_gz = determine_germline_contig_ploidy_cohort_mode.germline_contig_ploidy_cohort_calls_tar_gz,
        cohort_name = cohort_name
    }
  }

  # vi. Postprocess germline CSV calls
  scatter (index in range(length(bams))) {
    call postprocess_germline_cnv_calls_task.postprocess_germline_cnv_calls {
      input:
        sample_id = samples_ids[index],
        sample_index = index,
        germline_contig_ploidy_calls_tar_gz = determine_germline_contig_ploidy_cohort_mode.germline_contig_ploidy_cohort_calls_tar_gz,
        germline_cnv_caller_calls_tar_gz = run_germline_cnv_caller_cohort_mode.germline_cnv_caller_calls_tar_gz,
        germline_cnv_caller_model_tar_gz =  run_germline_cnv_caller_cohort_mode.germline_cnv_caller_model_tar_gz
    }
  }


  output {

    Array[File] genotyped_intervals_vcf_gz = postprocess_germline_cnv_calls.genotyped_intervals_vcf_gz
    Array[File] genotyped_intervals_vcf_gz_tbi = postprocess_germline_cnv_calls.genotyped_intervals_vcf_gz_tbi

    Array[File] genotyped_segments_vcf_gz = postprocess_germline_cnv_calls.genotyped_segments_vcf_gz
    Array[File] genotyped_segments_vcf_gz_tbi = postprocess_germline_cnv_calls.genotyped_segments_vcf_gz_tbi

    Array[File] denoised_copy_ratios_tsv = postprocess_germline_cnv_calls.denoised_copy_ratios_tsv

    File germline_contig_ploidy_cohort_model_tar_gz = determine_germline_contig_ploidy_cohort_mode.germline_contig_ploidy_cohort_model_tar_gz
    Array[File] chromosome_wise_germline_cnv_caller_models_tar_gz = run_germline_cnv_caller_cohort_mode.germline_cnv_caller_model_tar_gz

  }

}
