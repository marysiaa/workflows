import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.0.14/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/interval-group@1.1.3/src/main/wdl/tasks/interval-group/interval-group.wdl" as interval_group_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-gatk-m2@1.1.3/src/main/wdl/tasks/bam-gatk-m2/bam-gatk-m2.wdl" as bam_gatk_m2_task
import "https://gitlab.com/intelliseq/workflows/raw/m2-stats-merge@1.0.0/src/main/wdl/tasks/m2-stats-merge/m2-stats-merge.wdl" as m2_stats_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/pileup-merge@1.0.2/src/main/wdl/tasks/pileup-merge/pileup-merge.wdl" as pileup_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/pileup-calc-contam@1.0.0/src/main/wdl/tasks/pileup-calc-contam/pileup-calc-contam.wdl" as pileup_calc_contam_task
import "https://gitlab.com/intelliseq/workflows/raw/rom-for-m2filter@1.0.0/src/main/wdl/tasks/rom-for-m2filter/rom-for-m2filter.wdl" as rom_for_m2filter_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.4.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-concat@1.1.1/src/main/wdl/tasks/bam-concat/bam-concat.wdl" as bam_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-m2filter@1.0.5/src/main/wdl/tasks/vcf-m2filter/vcf-m2filter.wdl" as vcf_m2filter_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-align-errors@1.0.1/src/main/wdl/tasks/vcf-filter-align-errors/vcf-filter-align-errors.wdl" as vcf_filter_align_errors_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-m2genotype@1.0.0/src/main/wdl/tasks/vcf-m2genotype/vcf-m2genotype.wdl" as vcf_m2genotype_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow bam_varcalling_m2 {


  String module_name = "bam_varcalling_m2"
  String module_version = "1.1.3"

  String sample_id = "no_id_provided"

  ## intervals: genome or provided (if targeted analysis)
  File? interval_list
  Boolean is_interval_list_not_defined = !defined(interval_list)


  ## group intervals
  Int max_no_pieces_to_scatter_an_interval_file = 20

  ## mutect2
  File tumor_bam
  File tumor_bai
  File? normal_bam
  File? normal_bai
  File? custom_pon_file
  File? custom_pon_index
  Boolean is_normal_sample_provided = if defined(normal_bam) then true else false
  Boolean genotype_germline_sites = true
  Boolean genotype_pon_sites = false
  Boolean germline_analysis = false
  Boolean ignore_soft_clips = true
  String pcr_indel_model = "CONSERVATIVE" ## or NONE, AGGRESSIVE, HOSTILE

  ## mutect2 filter, see:
  ## https://gatk.broadinstitute.org/hc/en-us/articles/360051305471-FilterMutectCalls and
  ## https://github.com/broadinstitute/gatk/blob/master/docs/mutect/mutect.pdf
  ## to check the possible filtering parameters; the interesting ones (with defaults) are shown below.
  ## --max-alt-allele-count 1 => 2; --max-events-in-region 2 => 3; --min-median-base-quality 30;
  ## --min-median-mapping-quality 30; --f-score-beta 1; --unique-alt-read-count 1 =>2; --min-allele-fraction 0.0 => 0.02;
  ## --min-reads-per-strand false => 1; --distance-on-haplotype 100;
  String? m2_extra_filtering_args = "--max-alt-allele-count 2 --max-events-in-region 3 --unique-alt-read-count 2 --min-allele-fraction 0.02 --min-reads-per-strand 1"
  Float indel_threshold = 0.0 # change to 0.1 or so for iontorent analysis
  Boolean for_somatic_pipeline = true

  ## alignment artifact filter options, see:
  ## https://gatk.broadinstitute.org/hc/en-us/articles/360050814972-FilterAlignmentArtifacts-EXPERIMENTAL-
  ## some arguments are listed below (with defaults)
  ## --drop-ratio 0.2 => 0.1; --indel-start-tolerance 5 => 8; --max-reasonable-fragment-length 100000 (?);
  ## --min-aligner-score-difference-per-base 0.2; --min-mismatch-difference-per-base 0.02
  ## --num-regular-contigs 250000 (?);  -split-factor 0.5

  Boolean filter_on_alignment_artifacts = true
  String realignment_extra_args = "--num-regular-contigs 194 --max-reasonable-fragment-length 2000 --drop-ratio 0.1 --indel-start-tolerance 8"

  ## germline pipeline genotype options
  Float no_variant_thereshold = 0.1
  Float homozygote_threshold = 0.85


  # 1. Get intervals from docker
  if(is_interval_list_not_defined) {
    call resources_kit_task.resources_kit {
      input:
      kit = "genome",
      reference_genome = "grch38-no-alt"
    }
  File interval_resources = resources_kit.interval_list[0]
  }

  File interval_file = select_first([interval_list, interval_resources])


  # 2. Divide interval file into equally sized pieces
  call interval_group_task.interval_group {
    input:
      interval_file = interval_file,
      max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file
  }


  # 3. Call variants in parallel over divided calling intervals
  scatter (index in range(length(interval_group.grouped_calling_intervals))) {
    call bam_gatk_m2_task.bam_gatk_m2 {
      input:
        index = index,
        intervals = interval_group.grouped_calling_intervals[index],
        tumor_bam = tumor_bam,
        tumor_bai = tumor_bai,
        normal_bam = normal_bam,
        normal_bai = normal_bai,
        genotype_germline_sites = genotype_germline_sites,
        genotype_pon_sites = genotype_pon_sites,
        sample_id = sample_id,
        custom_pon_file = custom_pon_file,
        custom_pon_index = custom_pon_index,
        pcr_indel_model = pcr_indel_model,
        ignore_soft_clips = ignore_soft_clips
    }
  }


  # 4. Merge mutect2 stats
  call  m2_stats_merge_task.m2_stats_merge {
    input:
      m2_stats = bam_gatk_m2.stats,
      sample_id = sample_id
  }


  # 5. Merge tumor pileups
  Int tumor_pileups_len = length(bam_gatk_m2.tumor_pileups)
  if (tumor_pileups_len != 0) {
    call pileup_merge_task.pileup_merge as tumor_pileup_merge {
      input:
        pileups = bam_gatk_m2.tumor_pileups[0],
        sample_id = sample_id
    }
    if (tumor_pileup_merge.non_empty_pileup) {
      File merged_tumor_pileups = tumor_pileup_merge.merged_table
    }
  }


  # 6. Merge normal pileups
  Int normal_pileups_len = length(bam_gatk_m2.normal_pileups)
  if ((is_normal_sample_provided) && ( normal_pileups_len != 0)) {
    call pileup_merge_task.pileup_merge as normal_pileup_merge {
      input:
       pileups = bam_gatk_m2.normal_pileups[0],
       sample_id = sample_id
    }
    if (normal_pileup_merge.non_empty_pileup) {
      File merged_normal_pileups = normal_pileup_merge.merged_table
    }
  }


  # 7. Estimate contamination (this task fails if pileups are empty)
  Boolean estimate_contam  = defined(merged_tumor_pileups)
  if (estimate_contam)  {
    call pileup_calc_contam_task.pileup_calc_contam {
      input:
        tumor_table = merged_tumor_pileups,
        normal_table = merged_normal_pileups,
        sample_id = sample_id
    }
  }


  # 8. Calculate read orientation model
  call rom_for_m2filter_task.rom_for_m2filter {
    input:
      f1r2_tar_gz = bam_gatk_m2.f1r2_counts,
      sample_id = sample_id
  }


  # 9. Merge m2 vcfs
  call vcf_concat_task.vcf_concat {
    input:
      vcf_gz = bam_gatk_m2.unfiltered_vcf,
      vcf_basename = sample_id
  }


  # 10. Merge m2 bamOuts
  call bam_concat_task.bam_concat{
    input:
      interval_bams = bam_gatk_m2.bamOut,
      need_sort = "yes",
      validation_level = "SILENT",
      just_gather = true,
      sample_id = sample_id,
      bam_name = "m2-realigned"
  }


  # 11. Filter vcf with FilterMutectCalls
  call vcf_m2filter_task.vcf_m2filter {
    input:
      m2_vcf = vcf_concat.concatenated_vcf_gz,
      m2_vcf_tbi = vcf_concat.concatenated_vcf_gz_tbi,
      m2_stats = m2_stats_merge.merged_m2_stats,
      contamination_table = pileup_calc_contam.contamination_table,
      maf_segments = pileup_calc_contam.tumor_segments_table,
      artifact_priors_tar_gz = rom_for_m2filter.artifact_priors,
      m2_extra_filtering_args = m2_extra_filtering_args,
      sample_id = sample_id,
      indel_threshold = indel_threshold,
      for_somatic_pipeline = for_somatic_pipeline
  }


# 12. Filter vcf with FilterAlignmentArtifacts
  if (filter_on_alignment_artifacts) {
    call vcf_filter_align_errors_task.vcf_filter_align_errors {
      input:
        vcf = vcf_m2filter.filtered_m2_vcf,
        vcf_tbi = vcf_m2filter.filtered_m2_vcf_tbi,
        bamout = bam_concat.bam,
        bamout_bai = bam_concat.bai,
        realignment_extra_args = realignment_extra_args,
        sample_id = sample_id
   }
  }

  # 13. Assign genotypes (for germline samples analyzed with mutect only)
  if (!is_normal_sample_provided && germline_analysis) {
    call vcf_m2genotype_task.vcf_m2genotype {
      input:
        vcf = select_first([vcf_filter_align_errors.artifact_filtered_vcf, vcf_m2filter.filtered_m2_vcf]),
        tbi = select_first([vcf_filter_align_errors.artifact_filtered_vcf_tbi, vcf_m2filter.filtered_m2_vcf_tbi]),
        sample_id = sample_id,
        no_variant_thereshold = no_variant_thereshold,
        homozygote_threshold = homozygote_threshold

    }
  }


  # 13. Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([resources_kit.bco, interval_group.bco, m2_stats_merge.bco, tumor_pileup_merge.bco,
                                      normal_pileup_merge.bco, pileup_calc_contam.bco, rom_for_m2filter.bco,
                                      vcf_concat.bco, bam_concat.bco, vcf_m2filter.bco, vcf_m2genotype.bco,
                                      vcf_filter_align_errors.bco])
  Array[File] stdout_tasks = select_all([resources_kit.stdout_log, interval_group.stdout_log, m2_stats_merge.stdout_log,
                                        tumor_pileup_merge.stdout_log, normal_pileup_merge.stdout_log,
                                        pileup_calc_contam.stdout_log, rom_for_m2filter.stdout_log, vcf_concat.stdout_log,
                                        bam_concat.stdout_log, vcf_m2filter.stdout_log, vcf_m2genotype.stdout_log,
                                        vcf_filter_align_errors.stdout_log])
  Array[File] stderr_tasks = select_all([resources_kit.stderr_log, interval_group.stderr_log, m2_stats_merge.stderr_log,
                                        tumor_pileup_merge.stderr_log, normal_pileup_merge.stderr_log,
                                        pileup_calc_contam.stderr_log, rom_for_m2filter.stderr_log, vcf_concat.stderr_log,
                                        bam_concat.stderr_log, vcf_m2filter.stderr_log, vcf_m2genotype.stdout_log,
                                        vcf_filter_align_errors.stderr_log])

 Array[Array[File]] bco_scatters = [bco_tasks, bam_gatk_m2.bco]
 Array[Array[File]] stdout_scatters = [stdout_tasks, bam_gatk_m2.stdout_log]
 Array[Array[File]] stderr_scatters = [stderr_tasks, bam_gatk_m2.stderr_log]

 Array[File] bco_array = flatten(bco_scatters)
 Array[File] stdout_array = flatten(stdout_scatters)
 Array[File] stderr_array = flatten(stderr_scatters)

 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_array,
       stdout_array = stdout_array,
       stderr_array = stderr_array,
       module_name = module_name,
       module_version = module_version
 }



  output {

    ## FINAL VCF (output of MUTECT2 FILTER, ALIGN ARTIFACT FILTER or GENOTYPING task)
    # variants that passed the m2 filter
    File filtered_m2_vcf = select_first([vcf_m2genotype.genotyped_vcf, vcf_filter_align_errors.artifact_filtered_vcf, vcf_m2filter.filtered_m2_vcf])
    File filtered_m2_tbi = select_first([vcf_m2genotype.genotyped_tbi, vcf_filter_align_errors.artifact_filtered_vcf_tbi, vcf_m2filter.filtered_m2_vcf_tbi])
    # variants that failed the m2 filter
    File rejected_m2_vcf = vcf_m2filter.rejected_m2_vcf
    File rejected_m2_vcf_tbi = vcf_m2filter.rejected_m2_vcf_tbi
    File m2_filtering_stats = vcf_m2filter.filtering_stats


    ## MUTECT2 BAM
    File m2_bamout = bam_concat.bam
    File m2_bamout_bai = bam_concat.bai

    ## TUMOR SMAPLE NAME
    String tumor_sample_name = bam_gatk_m2.tumor_sample_name[0]

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
