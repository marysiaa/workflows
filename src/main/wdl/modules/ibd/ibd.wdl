import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@1.6.5/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-chrom@1.0.4/src/main/wdl/tasks/vcf-split-chrom/latest/vcf-split-chrom.wdl" as vcf_split_chrom_task
import "https://gitlab.com/intelliseq/workflows/raw/genotype-gvcf-on-dbsnp-positions@1.0.2/src/main/wdl/tasks/genotype-gvcf-on-dbsnp-positions/genotype-gvcf-on-dbsnp-positions.wdl" as genotype_gvcf_on_dbsnp_positions_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-hard@1.0.1/src/main/wdl/tasks/vcf-filter-hard/vcf-filter-hard.wdl" as vcf_filter_hard_task
import "https://gitlab.com/intelliseq/workflows/raw/filter-vcf-for-ibd@1.0.0/src/main/wdl/tasks/filter-vcf-for-ibd/filter-vcf-for-ibd.wdl" as filter_vcf_for_ibd_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-repeat@1.0.0/src/main/wdl/tasks/vcf-anno-repeat/latest/vcf-anno-repeat.wdl" as vcf_anno_repeat_task
import "https://gitlab.com/intelliseq/workflows/raw/find-ibd-regions@1.0.0/src/main/wdl/tasks/find-ibd-regions/find-ibd-regions.wdl" as find_ibd_regions_task
import "https://gitlab.com/intelliseq/workflows/raw/bed-concat@1.0.0/src/main/wdl/tasks/bed-concat/bed-concat.wdl" as bed_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/ibd-report@1.0.0/src/main/wdl/tasks/ibd-report/ibd-report.wdl" as ibd_report_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow ibd {

  meta {
    name: 'Identity by descent'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Finds long homozygous regions (identical by descent)'
    changes: '{"1.0.3": "docker gatk update; known user error: Bad input: We encountered a non-standard non-IUPAC base in the provided input sequence: \'0\'"}'

    input_gvcf_gz: '{"index": 7,"name": "gVCF file (bgzipped)", "type": "File", "extension": [".gvcf.gz"], "description": "Bgzipped gVCF file containing information for one sample"}'
    input_gvcf_gz_tbi: '{"index": 8, "name": "gVCF file index", "type": "File", "extension": [".gvcf.gz.tbi"], "description": "Tabix index of the bgzipped gVCF file"}'
    input_sample_id: '{"index": 1, "name": "Sample ID", "type": "String", "description": "Sample ID"}'
    input_sample_sex: '{"index": 9,"name": "Sample sex", "type": "String", "values":["F","M"], "description": "Sample sex, decides whether analyse chromX"}'
    input_reference_genome: '{"index": 10,"name": "Reference genome", "type": "String", "values": ["hg38", "grch38-no-alt"], "default": "broad-institute-hg38", "description": "Version of reference genome that was used to generate gVCF file"}'

    groups: '{"gene_panel": {"description": "Fill in at least 1 input below to generate the gene panel", "min_inputs": 1}}'
    input_hpo_terms: '{"index": 2, "name": "HPO terms", "type": "String", "groupname": "gene_panel", "description": "Enter HPO terms to narrow your search/analysis results (separate HPO terms with comma, for example: HP:0004942, HP:0011675)"}'
    input_genes: '{"index": 3, "name": "Genes names", "type": "String", "groupname": "gene_panel", "description": "Enter gene names to narrow your search/analysis results (separate gene names with comma, for example: HTT, FBN1)"}'
    input_diseases: '{"index": 4, "name": "Diseases", "type": "String", "groupname": "gene_panel", "description": "Enter disease names to narrow your search/analysis results (separate diseases names with comma; each disease name should be just a keyword, for example for Marfan Syndrome only Marfan should be written, for Ehlers-Danlos Syndrome: Ehlers-Danlos; other proper diseases names for example: Osteogenesis imperfecta, Tay-sachs, Hemochromatosis, Brugada, Canavan, etc.)"}'
    input_phenotypes_description: '{"index": 5, "name": "Description of patient phenotypes", "type": "String", "groupname": "gene_panel", "description": "Enter description of patient phenotypes"}'
    input_panel_names: '{"index": 6, "name": "Gene panel", "type": "Array[String]", "groupname": "gene_panel", "description": "Select gene panels", "constraints": {"values": ["None", "acmg-recommendation-panel", "COVID-19_research", "Cancer_Germline", "Cardiovascular_disorders", "Ciliopathies", "Dermatological_disorders", "Dysmorphic_and_congenital_abnormality_syndromes","Endocrine_disorders", "Gastroenterological_disorders", "Growth_disorders","Haematological_and_immunological_disorders", "Haematological_disorders", "Hearing_and_ear_disorders", "Metabolic_disorders", "Neurology_and_neurodevelopmental_disorders","Ophthalmological_disorders", "Rare_Diseases_others", "Renal_and_urinary_tract_disorders","Respiratory_disorders", "Rheumatological_disorders", "Skeletal_disorders","Tumour_syndromes"], "multiselect": true}}'

    output_ibd_bed_gz: '{"name": "Bed file", "type": "File", "copy": "True", "description": "Bed file containing information about detected homozygous (ibd) regions for one sample (bgzipped)"}'
    output_ibd_bed_gz_tbi: '{"name": "Bed file index", "type": "File", "copy": "True", "description": "Bed file index"}'
    output_ibd_report_pdf: '{"name": "IBD report pdf", "type": "File", "copy": "True", "description": "IBD report in pdf"}'
    output_ibd_report_html: '{"name": "IBD report html", "type": "File", "copy": "True", "description": "IBD report in html"}'
    output_ibd_report_odt: '{"name": "IBD report odt", "type": "File", "copy": "True", "description": "IBD report in odt"}'
    output_ibd_report_docx: '{"name": "IBD report docx", "type": "File", "copy": "True", "description": "IBD report in docx"}'


  }
  String module_name = "ibd"
  String module_version = "1.0.3"

  File gvcf_gz
  File gvcf_gz_tbi

  String sample_id = "no-id"
  String reference_genome = "hg38"
  Boolean old_gvcf = false
  String sample_sex = "M"

  File? panel
  Boolean is_panel_defined = defined(panel)
  String? hpo_terms
  String? genes
  String? diseases
  String? phenotypes_description
  Array[String]? panel_names
  Boolean is_input_for_panel_generate_defined = (defined(hpo_terms) || defined(genes) || defined(diseases) || defined(phenotypes_description) || defined(panel_names))

  Int chroms_array_length = if sample_sex == "F" then 23 else 22

  # 1. Prepare gene panel or use user defined
  if(is_input_for_panel_generate_defined && !is_panel_defined) {
     call panel_generate_task.panel_generate {
       input:
          sample_id = sample_id,
          hpo_terms = hpo_terms,
          genes = genes,
          diseases = diseases,
          phenotypes_description = phenotypes_description,
          panel_names = panel_names
        }
    }

  if (is_panel_defined || defined(panel_generate.panel)) {
  File panel_to_use = select_first([panel, panel_generate.panel])
  }

  # 2. Split gvcf into chromosome-wise gvcf files
  call vcf_split_chrom_task.vcf_split_chrom {
     input:
       vcf_gz = gvcf_gz,
       vcf_gz_tbi = gvcf_gz_tbi,
       vcf_basename = sample_id,
       return_y = false,
       gvcf_analysis = true
  }

  # 3. Genotype gVCF on positions from dbSNP database (output separate vcf for each chromosome).

  Array[File] chromosome_gvcfs  = vcf_split_chrom.chromosomes_vcf_gz
  Array[File] chromosome_gvcf_tbis = vcf_split_chrom.chromosomes_vcf_gz_tbi

  scatter (index in range(chroms_array_length)) {
  String string_to_remove = "-" + sample_id + ".*"
  String chrom_from_file_name = sub(basename(chromosome_gvcfs[index]), string_to_remove, "")

  call genotype_gvcf_on_dbsnp_positions_task.genotype_gvcf_on_dbsnp_positions {
     input:
       gvcf_gz = chromosome_gvcfs[index],
       gvcf_gz_tbi = chromosome_gvcf_tbis[index],
       basename = sample_id,
       chromosome = chrom_from_file_name,
       reference_genome = reference_genome,
       old_gvcf = old_gvcf
   }

   # 4. Apply hard filtering
   call vcf_filter_hard_task.vcf_filter_hard {
     input:
       vcf_gz = genotype_gvcf_on_dbsnp_positions.genotyped_on_dbsnp_vcf_gz,
       vcf_gz_tbi = genotype_gvcf_on_dbsnp_positions.genotyped_on_dbsnp_vcf_gz_tbi,
       reference_genome = reference_genome,
       sample_id  = sample_id
   }

    # 5. Prepare each chromosome-wise vcf for homozygous region detection
    call filter_vcf_for_ibd_task.filter_vcf_for_ibd {
      input:
        vcf_gz = vcf_filter_hard.hard_filtered_vcf_gz,
        vcf_gz_tbi = vcf_filter_hard.hard_filtered_vcf_gz_tbi,
        vcf_basename = sample_id,
        chr = chrom_from_file_name
    }
    
    # 6. Add simple repeat annotation to chomosome-wise vcf (prepared in the previous step)
    call vcf_anno_repeat_task.vcf_anno_repeat {
      input:
        vcf_gz = filter_vcf_for_ibd.filtered_for_ibd_vcf_gz,
        vcf_gz_tbi = filter_vcf_for_ibd.filtered_for_ibd_vcf_gz_tbi,
        vcf_basename = chrom_from_file_name + "-" + sample_id
    }
    
    # 7. Run homozygous region analysis for chromosome-wise vcf file (prepared in the previous step)
    call find_ibd_regions_task.find_ibd_regions {
      input:
        vcf_gz = vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz,
        vcf_gz_tbi =  vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz_tbi,
        basename = sample_id,
        chr = chrom_from_file_name
    }
  }
  
   # 8. Concatenate chromosome-wise bed files with homozygous regions annotation
   call bed_concat_task.bed_concat {
     input:
       chr_beds = find_ibd_regions.ibd_bed,
       basename = sample_id,
       need_sort = false
   }

  # 9. Create report
  call ibd_report_task.ibd_report {
     input:
        roh_tables = find_ibd_regions.roh_table,
        sample_id = sample_id,
        panel = panel_to_use
  }

  # 9. Merge bco and logs
  Array[File] bco_tasks = select_all([panel_generate.bco, vcf_split_chrom.bco, bed_concat.bco, ibd_report.bco])
  Array[File] stdout_tasks = select_all([panel_generate.stdout_log, vcf_split_chrom.stdout_log, bed_concat.stdout_log, ibd_report.stdout_log])
  Array[File] stderr_tasks = select_all([panel_generate.stderr_log, vcf_split_chrom.stderr_log, bed_concat.stderr_log, ibd_report.stderr_log])
  Array[Array[File]] bco_scatters = [bco_tasks, genotype_gvcf_on_dbsnp_positions.bco, vcf_filter_hard.bco, filter_vcf_for_ibd.bco, vcf_anno_repeat.bco, find_ibd_regions.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, genotype_gvcf_on_dbsnp_positions.stdout_log, vcf_filter_hard.stdout_log, filter_vcf_for_ibd.stdout_log, vcf_anno_repeat.stdout_log, find_ibd_regions.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, genotype_gvcf_on_dbsnp_positions.stderr_log, vcf_filter_hard.stderr_log, filter_vcf_for_ibd.stderr_log, vcf_anno_repeat.stderr_log, find_ibd_regions.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)



 call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }



  output {
  File ibd_bed_gz = bed_concat.bed_gz
  File ibd_bed_gz_tbi = bed_concat.bed_gz_tbi

  File ibd_report_pdf = ibd_report.ibd_report_pdf
  File ibd_report_html = ibd_report.ibd_report_html
  File ibd_report_odt = ibd_report.ibd_report_odt
  File ibd_report_docx = ibd_report.ibd_report_docx

  File stdout_log = bco_merge.stdout_log
  File stderr_log = bco_merge.stderr_log
  File bco = bco_merge.bco

  }
}
