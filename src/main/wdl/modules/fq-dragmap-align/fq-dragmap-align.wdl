import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.3/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-dragmap@1.0.2/src/main/wdl/tasks/fq-dragmap/fq-dragmap.wdl" as fq_dragmap_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-filter-mismatch@3.1.0/src/main/wdl/tasks/bam-filter-mismatch/bam-filter-mismatch.wdl" as bam_filter_mismatch_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-filter-chimeric@1.0.0/src/main/wdl/tasks/bam-filter-chimeric/bam-filter-chimeric.wdl" as bam_filter_chimeric_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-mark-dup@1.1.0/src/main/wdl/tasks/bam-mark-dup/bam-mark-dup.wdl" as bam_mark_dup_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-concat@1.1.2/src/main/wdl/tasks/bam-concat/bam-concat.wdl" as bam_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/calibrate-dragstr-model@1.0.1/src/main/wdl/tasks/calibrate-dragstr-model/calibrate-dragstr-model.wdl" as calibrate_dragstr_model_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow fq_dragmap_align {

  String module_name = "fq_dragmap_align"
  String module_version = "1.0.5"

  String reference_genome = "grch38-no-alt" #or "hg38"

  Array[File]? fastqs_left
  Array[File]? fastqs_right

  Array[File]? fastqs
  Boolean is_fastqs_defined = defined(fastqs)
  String platform = "Illumina" ## or IONTORRENT
  Boolean paired = true
  Boolean remove_chimeras = false
  Boolean split_files = true
  String bam_concat_name = "variant-calling-ready"
  String sample_id = "no_id_provided"
  String? chromosome

  ## Give false to turn off contamination filtering
  Boolean add_bam_filter = true
  ## Which filter should be applied?:
  ## 1 - removing reads with high fraction of mismatches
  ## 2 - removing reads which are both very short and with soft-clipped at both ends
  ## 3 - combining both filter types (1+2)
  ## 4 - 3 plus removal of reads with multiple indels (for iontorrent)
  String filter_choice = "3"
  ## Set mismatch fraction that is not accepted (for filters 1 and 3)
  Float mismatch_threshold = 0.1

  ## Set as none to disable optical duplicates marking, do not provide anything otherwise
  String? read_name_regex
  Boolean mark_dup = true

  String java_options = "-Xmx14g"


  # 0. Organize fastqs
  if(is_fastqs_defined) {
      call fq_organize_task.fq_organize {
          input:
              fastqs = fastqs,
              paired = paired,
              split_files = split_files
      }
  }

  Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
  Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])

  # 1. Align reads
  if (paired){
      scatter (index in range(length(fastqs_1))) {
          call fq_dragmap_task.fq_dragmap as paired_alignment{
              input:
                  fastq_1 = fastqs_1[index],
                  fastq_2 = fastqs_2[index],
                  sample_id = sample_id,
                  reference_genome = reference_genome,
                  index = index,
                  platform = platform,
                  chromosome = chromosome
         }
      }
  }
  if(!paired){
      scatter (index in range(length(fastqs_1))) {
          call fq_dragmap_task.fq_dragmap as single_end_alignment {
              input:
                  fastq_1 = fastqs_1[index],
                  sample_id = sample_id,
                  reference_genome = reference_genome,
                  index = index,
                  platform = platform,
                  chromosome = chromosome
        }
      }
  }
  Array[File] align_bams = select_first([single_end_alignment.dragmap_unsorted_bam, paired_alignment.dragmap_unsorted_bam])

  # 2. Filter out contaminated reads (optionally)
  if(add_bam_filter) {
      scatter (index in range(length(fastqs_1))) {
          call bam_filter_mismatch_task.bam_filter_mismatch {
              input:
                  bam = align_bams[index],
                  mismatch_threshold = mismatch_threshold,
                  filter_choice = filter_choice,
                  index = index
          }
      }
  }
  Array[File] bams_to_rv_chim = select_first([bam_filter_mismatch.filtered_bam, align_bams])
  # 3. Filter out chimeric reads (optionally)
  if (remove_chimeras){
      scatter (index in range(length(fastqs_1))) {
          call bam_filter_chimeric_task.bam_filter_chimeric {
              input:
                  bam = bams_to_rv_chim[index],
                  index = index,
                  sample_id = sample_id
          }
     }
  }
  Array[File] bams_to_md = select_first([bam_filter_chimeric.filtered_bam, bam_filter_mismatch.filtered_bam, align_bams])

  # 4. Mark duplicates, merge and sort (or just mege and sort)
  call bam_mark_dup_task.bam_mark_dup {
      input:
          bams = bams_to_md,
          sample_id = sample_id,
          read_name_regex = read_name_regex,
          md_java_options = java_options,
          sort_java_options = java_options,
          mark_dup = mark_dup
  }

  # 5. Merge the recalibrated BAM files resulting from by-interval recalibration (or just rename final bam)
  call bam_concat_task.bam_concat as bam_concat_recalib {
      input:
          interval_bams = [bam_mark_dup.mark_dup_bam],
          sample_id = sample_id,
          output_name = bam_concat_name
  }

  # 6. Calibrate dragstr model (used in bam-varcalling in haplotypeCaller with --dragen-mode option)   
  call calibrate_dragstr_model_task.calibrate_dragstr_model {
      input:
        bam = bam_concat_recalib.bam,
        bai = bam_concat_recalib.bai,
        sample_id = sample_id
  }

  # 7. Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([fq_organize.bco, bam_mark_dup.bco, bam_concat_recalib.bco, calibrate_dragstr_model.bco])
  Array[File] stdout_tasks = select_all([fq_organize.stdout_log, bam_mark_dup.stdout_log, bam_concat_recalib.stdout_log, calibrate_dragstr_model.stdout_log])
  Array[File] stderr_tasks = select_all([fq_organize.stderr_log, bam_mark_dup.stderr_log, bam_concat_recalib.stderr_log, calibrate_dragstr_model.stderr_log])

  Array[Array[File]] bco_scatters = select_all([bco_tasks, bam_filter_mismatch.bco, bam_filter_chimeric.bco,
                                               select_first([single_end_alignment.bco, paired_alignment.bco])])
  Array[Array[File]] stdout_scatters = select_all([stdout_tasks, bam_filter_mismatch.stdout_log, bam_filter_chimeric.stdout_log,
                                                select_first([single_end_alignment.stdout_log, paired_alignment.stdout_log])])
  Array[Array[File]] stderr_scatters = select_all([stderr_tasks, bam_filter_mismatch.stderr_log, bam_filter_chimeric.stderr_log,
                                                select_first([single_end_alignment.stderr_log, paired_alignment.stderr_log])])

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  output {
    File recalibrated_markdup_bam = bam_concat_recalib.bam
    File recalibrated_markdup_bai = bam_concat_recalib.bai

    File dragstr_model = calibrate_dragstr_model.dragstr_model

    File duplicates_metrics = bam_mark_dup.metrics_file
    Boolean empty_metrics = bam_mark_dup.empty_metrics
    Array[Float]? chimeric_reads_pecent = bam_filter_chimeric.chimeras_percent
    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}
