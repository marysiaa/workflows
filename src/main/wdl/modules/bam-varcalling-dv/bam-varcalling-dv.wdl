import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.0.14/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/interval-group@1.1.1/src/main/wdl/tasks/interval-group/interval-group.wdl" as interval_group_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-deepvariant@1.0.9/src/main/wdl/tasks/bam-deepvariant/bam-deepvariant.wdl" as bam_deepvariant_task
import "https://gitlab.com/intelliseq/workflows/raw/interval-list-to-bed@1.0.3/src/main/wdl/tasks/interval-list-to-bed/interval-list-to-bed.wdl" as interval_list_to_bed_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.4.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-merge@1.0.1/src/main/wdl/tasks/gvcf-merge/latest/gvcf-merge.wdl" as gvcf_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.2/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow bam_varcalling_dv {

  String module_name = "bam_varcalling_dv"
  String module_version = "1.0.6"

  File input_bam
  File input_bai

  String sample_id = "no_id_provided"
  String reference_genome = "grch38-no-alt" # or hg38

  File? interval_list

  String kit = "exome-v7" # or "exome-v6" or "genome"
  String model_type = if kit == "genome" then "WGS" else "WES"
  Boolean filter_pass = true
  Boolean is_pgx = false

  Boolean is_interval_list_not_defined = !defined(interval_list)
  Int max_no_pieces_to_scatter_an_interval_file = 2

  ### to get interval_list if not declared ###
  if(is_interval_list_not_defined) {
    call resources_kit_task.resources_kit {
      input:
        kit = kit,
        reference_genome = reference_genome
    }
    File interval_resources = resources_kit.interval_list[0]
  }

  File interval_file = select_first([interval_list, interval_resources])

  # i. Create grouped calling intervals
  call interval_group_task.interval_group {
    input:
      interval_file = interval_file,
      max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file
  }

  # ii. Concert grouped calling intervals to .bed format
  call interval_list_to_bed_task.interval_list_to_bed {
    input:
      calling_intervals = interval_group.grouped_calling_intervals
  }


  # iii. Call variants in parallel over converted .bed files
  scatter (index in range(length(interval_list_to_bed.converted_beds))) {
  call bam_deepvariant_task.bam_deepvariant {
    input:
      index = index,
      model_type = model_type,
      reference_genome = reference_genome,
      recalibrated_markdup_bam = input_bam,
      recalibrated_markdup_bai = input_bai,
      sample_id = sample_id,
      bed_file = interval_list_to_bed.converted_beds[index],
      filter_pass = filter_pass
    }
  }

  # iv. Merge vcfs
  call vcf_concat_task.vcf_concat {
    input:
      vcf_gz = bam_deepvariant.vcf_gz,
      vcf_basename = sample_id
 }

  # v. Merge per-interval GVCFs
  call gvcf_merge_task.gvcf_merge {
    input:
      gvcf_gzs = bam_deepvariant.gvcf_gz,
      gvcf_gz_tbis = bam_deepvariant.gvcf_gz_tbi,
      sample_id = sample_id
  }

# Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([resources_kit.bco, interval_group.bco, interval_list_to_bed.bco, vcf_concat.bco, gvcf_merge.bco])
  Array[File] stdout_tasks = select_all([resources_kit.stdout_log, interval_group.stdout_log, interval_list_to_bed.stdout_log, vcf_concat.stdout_log, gvcf_merge.stdout_log])
  Array[File] stderr_tasks = select_all([resources_kit.stderr_log, interval_group.stderr_log, interval_list_to_bed.stderr_log, vcf_concat.stderr_log, gvcf_merge.stderr_log])

  Array[Array[File]] bco_scatters = [bco_tasks, bam_deepvariant.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, bam_deepvariant.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, bam_deepvariant.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)


  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }


  ###########
  # Outputs #
  ###########

  output {

    File gvcf_gz = gvcf_merge.gvcf_gz
    File gvcf_gz_tbi = gvcf_merge.gvcf_gz_tbi

    File vcf_gz = vcf_concat.concatenated_vcf_gz
    File vcf_gz_tbi = vcf_concat.concatenated_vcf_gz_tbi

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}
