import "https://gitlab.com/intelliseq/workflows/raw/fq-bowtie2@1.0.1/src/main/wdl/tasks/fq-bowtie2/fq-bowtie2.wdl" as fq_bowtie2_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-mark-dup@1.0.0/src/main/wdl/tasks/bam-mark-dup/bam-mark-dup.wdl" as bam_mark_dup_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-wisecondorx@1.0.0/src/main/wdl/tasks/bam-wisecondorx/bam-wisecondorx.wdl" as bam_wisecondorx_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.1.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow fq_nipt {

  File? fastq_single_end

  #File? fastq_paired_1
  #File? fastq_paired_2

  String sample_id = 'no_id_provided'

  String module_name = "fq_nipt"
  String module_version = "1.0.0"


  # 1. Aligment by bowtie2
  call fq_bowtie2_task.fq_bowtie2{
    input:
        fastq_single_end = fastq_single_end,
        #fastq_paired_1 = fastq_paired_1,
        #fastq_paired_2 = fastq_paired_2,
        sample_id = sample_id
  }

  # 2. Mark duplicates, sort, index
  Array[File] bams = [fq_bowtie2.bam]
  call bam_mark_dup_task.bam_mark_dup{
    input:
        bams = bams,
        sample_id = sample_id
  }

  call bam_wisecondorx_task.bam_wisecondorx {
    input:
        bam = bam_mark_dup.mark_dup_bam,
        bai = bam_mark_dup.mark_dup_bai,
        sample_id = sample_id
  }


  # after all regular tasks merge bco
  # Merge bco, stdout, stderr files
 Array[File] bco_tasks = [fq_bowtie2.bco, bam_mark_dup.bco, bam_wisecondorx.bco]
 Array[File] stdout_tasks = [fq_bowtie2.stdout_log, bam_mark_dup.stdout_log, bam_wisecondorx.stdout_log]
 Array[File] stderr_tasks = [fq_bowtie2.stderr_log, bam_mark_dup.stderr_log, bam_wisecondorx.stderr_log]


 Array[Array[File]] bco_scatters = [bco_tasks]
 Array[Array[File]] stdout_scatters = [stdout_tasks]
 Array[Array[File]] stderr_scatters = [stderr_tasks]

 Array[File] bco_array = flatten(bco_scatters)
 Array[File] stdout_array = flatten(stdout_scatters)
 Array[File] stderr_array = flatten(stderr_scatters)

 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_array,
       stdout_array = stdout_array,
       stderr_array = stderr_array,
       module_name = module_name,
       module_version = module_version
 }



  output {

    Array[File] plots = bam_wisecondorx.plots

    File aberrations = bam_wisecondorx.aberrations
    File bins = bam_wisecondorx.bins
    File segments = bam_wisecondorx.segments
    File statistics = bam_wisecondorx.statistics

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
