import "https://gitlab.com/intelliseq/workflows/raw/sex-check-sry@1.0.3/src/main/wdl/tasks/sex-check-sry/sex-check-sry.wdl" as sex_check_sry_task
import "https://gitlab.com/intelliseq/workflows/raw/sex-check-chr-cov@1.0.3/src/main/wdl/tasks/sex-check-chr-cov/sex-check-chr-cov.wdl" as sex_check_chr_cov_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-repeat@1.0.0/src/main/wdl/tasks/vcf-anno-repeat/latest/vcf-anno-repeat.wdl" as vcf_anno_repeat_task
import "https://gitlab.com/intelliseq/workflows/raw/sex-check-x-zygosity@1.0.5/src/main/wdl/tasks/sex-check-x-zygosity/sex-check-x-zygosity.wdl" as sex_check_x_zygosity_task
import "https://gitlab.com/intelliseq/workflows/raw/report-sex-check@2.2.3/src/main/wdl/tasks/report-sex-check/report-sex-check.wdl" as report_sex_check_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow sex_check {

  meta {
    keywords: '{"keywords": ["sex", "biological sex", "chrY", "SRY"]}'
    name: 'Biological sex recognition'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Checks the biological sex of a sample based on a few tests: depth coverage of SRY gene region (GRCH38: chrY:2786989-2787603), coverage of X and Y chromosomes compared and - if vcf or annotated with simple repeats vcf is provided - zygosity on chromosome X excluding pseudoautosomal regions. Produces summary report.'
    changes: '{"1.1.5": "add analysisEnvironment (IntelliseqFlow, T-Systems) input to report", "1.1.4": "remove language choose", "1.1.3": "report name update", "1.1.2": "new report header and docker", "1.1.1": "new brand report sex-check", "1.1.0": "new jsons and new report", "1.0.2": "updated sex-check-x-zygosity (bug fix)","1.0.1": "error handling added to sex-check-chr-cov and sex-check-x-zygosity"}'


    input_bam: '{"name": "bam", "type": "File", "description": "Sample bam file, use *markdup.recalibrated.bam or *markdup.realigned.recalibrated.bam or *filtered.bam, DO NOT USE *realigned-haplotypecaller.bam", "extension": [".bam"]}'
    input_bai: '{"name": "bai", "type": "File", "description": "Sample bai file", "extension": [".bai"]}'

    input_vcf_gz: '{"name": "Vcf", "type": "File", "extension": [".vcf.gz"], "description": "Input vcf file (not annotated). If not provided, report will show one statistic less"}'
    input_vcf_gz_tbi: '{"name": "Vcf tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Index for the input vcf file"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Sample ID"}'
    input_sample_info_json: '{"name": "Patient and sample data", "type": "File", "extension": [".json"], "description": "Patient and sample data in json format, can include attributes: name, surname, sex, birthdate, pesel, ID, material, sequencing_type, sequencing_platform, sending_date, raport_date, doctor_name, doctor_surname", "required": "False"}'


    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Module console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Module console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Module biocompute object"}'

    output_sex_check_report_pdf: '{"name": "sex_check_report_pdf", "type": "File", "copy": "true", "description": "biological sex recognition report in pdf file"}'
    output_sex_check_report_odt: '{"name": "sex_check_report_odt", "type": "File", "copy": "true", "description": "biological sex recognition report in odt file"}'
    output_sex_check_report_docx: '{"name": "sex_check_report_docx", "type": "File", "copy": "true", "description": "biological sex recognition report in docx file"}'
  }

  File bam
  File bai

  File? vcf_gz
  File? vcf_gz_tbi

  File? sample_info_json
  String sample_id = "no_id_provided"

  Int timezoneDifference = 0
  String analysisEnvironment = "IntelliseqFlow"

  String module_name = "sex_check"
  String module_version = "1.1.5"


  call sex_check_sry_task.sex_check_sry {
	input:
		bai = bai,
		bam = bam
  }

  call sex_check_chr_cov_task.sex_check_chr_cov {
  input:
    bai = bai,
    bam = bam
  }

  if (defined(vcf_gz)) {

    call vcf_anno_repeat_task.vcf_anno_repeat {
    input:
      vcf_gz = vcf_gz,
      vcf_gz_tbi = vcf_gz_tbi
    }

    call sex_check_x_zygosity_task.sex_check_x_zygosity {
    input:
      vcf_gz = vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz,
      vcf_gz_tbi = vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz_tbi
    }
  }

  Array[File] sex_check_jsons = select_all([sex_check_sry.sex_report_json, sex_check_chr_cov.sex_report_json, sex_check_x_zygosity.sex_report_json])

  call report_sex_check_task.report_sex_check {
  input:
    sex_check_jsons = sex_check_jsons,
    sample_id = sample_id,
    sample_info_json = sample_info_json,
    timezoneDifference = timezoneDifference,
    analysisEnvironment = analysisEnvironment
  }

########
#Merge bco, stdout, stderr files

  Array[File] bco_array = select_all([sex_check_sry.bco, sex_check_chr_cov.bco, vcf_anno_repeat.bco, sex_check_x_zygosity.bco, report_sex_check.bco])
  Array[File] stdout_array = select_all([sex_check_sry.stdout_log, sex_check_chr_cov.stdout_log, vcf_anno_repeat.stdout_log, sex_check_x_zygosity.stdout_log, report_sex_check.stdout_log])
  Array[File] stderr_array = select_all([sex_check_sry.stderr_log, sex_check_chr_cov.stderr_log, vcf_anno_repeat.stderr_log, sex_check_x_zygosity.stderr_log, report_sex_check.stderr_log])

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

########

  output {

  File report_ang_pdf = report_sex_check.report_sex_check_ang_pdf
  File report_ang_docx = report_sex_check.report_sex_check_ang_docx

  File stdout_log = bco_merge.stdout_log
  File stderr_log = bco_merge.stderr_log
  File bco = bco_merge.bco

  }

}
