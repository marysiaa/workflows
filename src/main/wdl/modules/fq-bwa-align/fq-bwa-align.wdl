import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.4/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-mem@2.1.0/src/main/wdl/tasks/fq-bwa-mem/fq-bwa-mem.wdl" as fq_bwa_mem_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-filter-mismatch@3.1.0/src/main/wdl/tasks/bam-filter-mismatch/bam-filter-mismatch.wdl" as bam_filter_mismatch_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-filter-chimeric@1.0.0/src/main/wdl/tasks/bam-filter-chimeric/bam-filter-chimeric.wdl" as bam_filter_chimeric_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-mark-dup@1.1.0/src/main/wdl/tasks/bam-mark-dup/bam-mark-dup.wdl" as bam_mark_dup_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-concat@1.1.2/src/main/wdl/tasks/bam-concat/bam-concat.wdl" as bam_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-seq-grouping@1.0.2/src/main/wdl/tasks/bam-seq-grouping/bam-seq-grouping.wdl" as bam_seq_grouping_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-bsqr@1.1.3/src/main/wdl/tasks/bam-bsqr/bam-bsqr.wdl" as bam_bsqr_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-gather-bsqr@1.1.0/src/main/wdl/tasks/bam-gather-bsqr/bam-gather-bsqr.wdl" as bam_gather_bsqr_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-base-recalib@1.1.0/src/main/wdl/tasks/bam-base-recalib/bam-base-recalib.wdl" as bam_base_recalib_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow fq_bwa_align {

  meta {
  keywords: '{"keywords": ["fastq", "bam", "alignment"]}'
  name: 'Alignment'
  author: 'https://gitlab.com/marysiaa , https://gitlab.com/marpiech, https://gitlab.com/kattom'
  copyright: 'Copyright 2019 Intelliseq'
  description: 'Align reads with bwa-mem'
  changes: '{"1.6.1": "update fq-organize", 1.6.0": "optional bsqr, works with uncompressed and unpaired reads, optionally may remove chimeric reads", "1.5.14": "gatk docker update", "1.5.13": "optional duplicated reads marking", "1.5.12": "name of bam", "1.5.11": "dockers updates", "1.5.10": "fq-organize more ram and error msg", "1.5.9": "more RAM in fq-organize", "1.5.8": "update fq-organize to validate fq", "1.5.7": "update fq-organize", "1.5.6": "more disk space in fq-organize", "1.5.5": "mark duplicates metrics as output", "1.5.4": "fq-organize and bam-mark-dup with adjustable disks size", "1.5.3": "new version of fq-organize task", "1.5.2": "fq-bwa-mem with set -o -e pipefail (2.0.1)", "1.5.1": "new version of fq-organize task", "1.5.0": "duplicates marked with Picard, mismatch filtering added", "1.4.2": "Works with grch38-no-alt reference, latest dir removed"}'

  input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Enter a sample name (or identifier)"}'
  input_fastqs: '{"name": "Fastq files", "type": "Array[File]", "extension": [".fq", ".fastq", ".fq.gz", ".fastq.gz"], "description": "Choose list of fastq files [.fq.gz or .fastq.gz]"}'
  input_fastqs_left: '{"hidden":"true", "name": "First (left) fastq files", "type": "Array[File]", "extension": [".fq", ".fastq", ".fq.gz", ".fastq.gz"], "description": "Choose first (left) fastq files"}'
  input_fastqs_right: '{"hidden":"true", "name": "Second (right) fastq files", "type": "Array[File]", "extension": [".fq", ".fastq", ".fq.gz", ".fastq.gz"], "description": "Choose second (right) fastq files"}'
  input_add_bam_filter: '{"name": "Add bam filter?", "type": "Boolean", "default": true, "description": "Decides whether bam filtering on contamination should be applied"}'
  input_remove_chimeras: '{"name": "Remove chimeras?", "type": "Boolean", "default": false, "description": "Decides whether remove chimeric reads"}'
  input_filter_choice: '{"name": "Filter choice", "type": "String", "constraints": {"choices": ["1", "2", "3", "4"]}, "default": "3", "description": "Which filters should be applied: 1 - removing reads with high fraction of mismatches only, 2 - removing reads soft-clipped from both ends and with mapped part shorter than 33, 3 - both filters, 4 - 3 plus removal of reads with multiple indels (for iontorrent)"}'
  input_mismatch_threshold: '{"name": "Mismatch threshold (for filters 1 and 3)", "type": "Float", "constraints": {"min": 0.0, "max": 1.0}, "default": 0.1, "description": "Reads with fraction of mismatches/indels bigger than this value are removed"}'
  input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"choices": ["hg38", "grch38-no-alt"]}, "default": "grch38-no-alt", "description": "Reference genome"}'

  output_recalibrated_markdup_bam: '{"name": "Markdup bam", "type": "File", "description": "Aligned bam file"}'
  output_recalibrated_markdup_bai: '{"name": "Markdup bai", "type": "File", "description": "Index for the aligned bam file"}'
  output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
  output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
  output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  String module_name = "fq_bwa_align"
  String module_version = "1.6.1"

  String reference_genome = "grch38-no-alt" #or "hg38"

  Array[File]? fastqs_left
  Array[File]? fastqs_right

  Array[File]? fastqs
  Boolean is_fastqs_defined = defined(fastqs)
  String platform = "Illumina" ## or IONTORRENT
  Boolean paired = true
  Boolean run_bsqr = true
  Boolean remove_chimeras = false
  Boolean split_files = true
  String bam_concat_name = "variant-calling-ready"
  String sample_id = "no_id_provided"

  ## Give false to turn off contamination filtering
  Boolean add_bam_filter = true
  ## Which filter should be applied?:
  ## 1 - removing reads with high fraction of mismatches
  ## 2 - removing reads which are both very short and with soft-clipped at both ends
  ## 3 - combining both filter types (1+2)
  ## 4 - 3 plus removal of reads with multiple indels (for iontorrent)
  String filter_choice = "3"
  ## Set mismatch fraction that is not accepted (for filters 1 and 3)
  Float mismatch_threshold = 0.1

  ## Set as none to disable optical duplicates marking, do not provide anything otherwise
  String? read_name_regex
  Boolean mark_dup = true

  String java_options = "-Xmx14g"


  # 0. Organize fastqs
  if(is_fastqs_defined) {
      call fq_organize_task.fq_organize {
          input:
              fastqs = fastqs,
              paired = paired,
              split_files = split_files
      }
  }

  Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
  Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])

  # 1. Align reads
  if (paired){
      scatter (index in range(length(fastqs_1))) {
          call fq_bwa_mem_task.fq_bwa_mem as paired_alignment{
              input:
                  fastq_1 = fastqs_1[index],
                  fastq_2 = fastqs_2[index],
                  sample_id = sample_id,
                  reference_genome = reference_genome,
                  index = index,
                  platform = platform
         }
      }
  }
  if(!paired){
      scatter (index in range(length(fastqs_1))) {
          call fq_bwa_mem_task.fq_bwa_mem as single_end_alignment {
              input:
                  fastq_1 = fastqs_1[index],
                  sample_id = sample_id,
                  reference_genome = reference_genome,
                  index = index,
                  platform = platform
        }
      }
  }
   Array[File] align_bams = select_first([single_end_alignment.bwa_unsorted_bam, paired_alignment.bwa_unsorted_bam])

  # 2. Filter out contaminated reads (optionally)
  if(add_bam_filter) {
      scatter (index in range(length(fastqs_1))) {
          call bam_filter_mismatch_task.bam_filter_mismatch {
              input:
                  bam = align_bams[index],
                  mismatch_threshold = mismatch_threshold,
                  filter_choice = filter_choice,
                  index = index
          }
     }
  }
  Array[File] bams_to_rv_chim = select_first([bam_filter_mismatch.filtered_bam, align_bams])
  # 3. Filter out chimeric reads (optionally)
  if (remove_chimeras){
      scatter (index in range(length(fastqs_1))) {
          call bam_filter_chimeric_task.bam_filter_chimeric {
              input:
                  bam = bams_to_rv_chim[index],
                  index = index,
                  sample_id = sample_id
          }
     }
  }
  Array[File] bams_to_md = select_first([bam_filter_chimeric.filtered_bam, bam_filter_mismatch.filtered_bam, align_bams])

  # 4. Mark duplicates, merge and sort (or just mege and sort)
  call bam_mark_dup_task.bam_mark_dup {
      input:
          bams = bams_to_md,
          sample_id = sample_id,
          read_name_regex = read_name_regex,
          md_java_options = java_options,
          sort_java_options = java_options,
          mark_dup = mark_dup
  }


  # 5. Generate the recalibration model by interval
  if (run_bsqr){
      call bam_seq_grouping_task.bam_seq_grouping {
          input:
              reference_genome = reference_genome
     }

      Array[Array[String]] sequence_grouping = read_tsv(bam_seq_grouping.sequence_grouping_txt)
      Array[Array[String]] sequence_grouping_with_unmapped = read_tsv(bam_seq_grouping.sequence_grouping_with_unmapped_txt)

      scatter (index in range(length(sequence_grouping))) {
          call bam_bsqr_task.bam_bsqr {
              input:
                  index = index,
                  sequence_group_interval = sequence_grouping[index],
                  markdup_bam = bam_mark_dup.mark_dup_bam,
                  markdup_bai = bam_mark_dup.mark_dup_bai,
                  sample_id = sample_id
          }
      }

      # 7. Merge the recalibration reports resulting from by-interval recalibration
      call bam_gather_bsqr_task.bam_gather_bsqr {
          input:
              partial_recalibration_reports = bam_bsqr.partial_recalibration_report,
              sample_id = sample_id
      }

      # 8. Perform base recalibration
      scatter (index in range(length(sequence_grouping_with_unmapped))) {
          call bam_base_recalib_task.bam_base_recalib {
              input:
                  index = index,
                  sequence_group_interval = sequence_grouping_with_unmapped[index],
                  markdup_bam = bam_mark_dup.mark_dup_bam,
                  markdup_bai = bam_mark_dup.mark_dup_bai,
                  recalibration_report = bam_gather_bsqr.recalibration_report,
                  sample_id = sample_id
          }
     }
  }
  # 9. Merge the recalibrated BAM files resulting from by-interval recalibration (or just rename final bam)
  call bam_concat_task.bam_concat as bam_concat_recalib {
      input:
          interval_bams = select_first([bam_base_recalib.partial_recalibrated_markdup_bam, [bam_mark_dup.mark_dup_bam]]),
          sample_id = sample_id,
          output_name = bam_concat_name
  }

  # 10. Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([fq_organize.bco, bam_mark_dup.bco, bam_concat_recalib.bco, bam_seq_grouping.bco, bam_gather_bsqr.bco])
  Array[File] stdout_tasks = select_all([fq_organize.stdout_log, bam_mark_dup.stdout_log, bam_concat_recalib.stdout_log,
                                         bam_seq_grouping.stdout_log, bam_gather_bsqr.stdout_log])
  Array[File] stderr_tasks = select_all([fq_organize.stderr_log, bam_mark_dup.stderr_log, bam_concat_recalib.stderr_log,
                                         bam_seq_grouping.stderr_log, bam_gather_bsqr.stderr_log])

  Array[Array[File]] bco_scatters = select_all([bco_tasks, bam_filter_mismatch.bco, bam_filter_chimeric.bco,
                                               select_first([single_end_alignment.bco, paired_alignment.bco]), bam_bsqr.bco, bam_base_recalib.bco])
  Array[Array[File]] stdout_scatters = select_all([stdout_tasks, bam_filter_mismatch.stdout_log, bam_filter_chimeric.stdout_log,
                                                  select_first([single_end_alignment.stdout_log, paired_alignment.stdout_log]),
                                                  bam_bsqr.stdout_log, bam_base_recalib.stdout_log])
  Array[Array[File]] stderr_scatters = select_all([stderr_tasks, select_first([single_end_alignment.stderr_log, paired_alignment.stderr_log]),
                                                  bam_filter_mismatch.stderr_log, bam_filter_chimeric.stderr_log,
                                                  bam_bsqr.stderr_log, bam_base_recalib.stderr_log])

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  output {
    File recalibrated_markdup_bam = bam_concat_recalib.bam
    File recalibrated_markdup_bai = bam_concat_recalib.bai

    File duplicates_metrics = bam_mark_dup.metrics_file
    Boolean empty_metrics = bam_mark_dup.empty_metrics
    Array[Float]? chimeric_reads_pecent = bam_filter_chimeric.chimeras_percent
    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}
