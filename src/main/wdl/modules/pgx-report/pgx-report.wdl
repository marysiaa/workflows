import "https://gitlab.com/intelliseq/workflows/raw/pgx-prioritize-anno@1.0.2/src/main/wdl/tasks/pgx-prioritize-anno/pgx-prioritize-anno.wdl" as pgx_prioritize_anno_task
import "https://gitlab.com/intelliseq/workflows/raw/report-pgx@1.0.3/src/main/wdl/tasks/report-pgx/report-pgx.wdl" as report_pgx_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow pgx_report {

  File recommendations
  File vcf
  File? sample_info_json

  String sample_id = "no_id_provided"
  Int timezoneDifference = 0

  String module_name = "pgx_report"
  String module_version = "1.0.2"


  #1 Prioritize annotations
  call pgx_prioritize_anno_task.pgx_prioritize_anno {
    input:
      recommendations = recommendations,
      sample_id = sample_id
  }

  #2 Generate report
  call report_pgx_task.report_pgx {
    input:
      recommendations = pgx_prioritize_anno.prioritized,
      vcf = vcf,
      sample_id = sample_id,
      timezoneDifference = timezoneDifference
  }

  # after all regular tasks merge bco
  # Merge bco, stdout, stderr files
  Array[File] bco_tasks = [pgx_prioritize_anno.bco, report_pgx.bco]
  Array[File] stdout_tasks = [pgx_prioritize_anno.stdout_log, report_pgx.stdout_log]
  Array[File] stderr_tasks = [pgx_prioritize_anno.stderr_log, report_pgx.stderr_log]

  call bco_merge_task.bco_merge {
    input:
      bco_array = bco_tasks,
      stdout_array = stdout_tasks,
      stderr_array = stderr_tasks,
      module_name = module_name,
      module_version = module_version
  }

  output {

    File pgx_report_pdf = report_pgx.pgx_report_pdf
    File pgx_report_docx = report_pgx.pgx_report_docx

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

  }
