# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  Joint genotype GVCFs
#  ********************
#
#  Info:
#  -----
#    Name: joint_genotype_gvcfs
#    Version: v0.1
#    Status: Prototype
#    Last edited: 05-06-2019
#    Last edited by: Katarzyna Kolanek
#    Author(s):
#      + Katarzyna Kolanek, <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw
#    Maintainer(s):
#      + Katarzyna Kolanek <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw
#    Copyright: Copyright 2019 Intelliseq
#    License: All rights reserved
#
#  Description:
#  ------------
#    Tasks:
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/combine-gvcfs.wdl/v0.1/combine-gvcfs.wdl" as combine_gvcfs_workflow
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/genotype-gvcf/v0.1/genotype-gvcf.wdl" as genotype_gvcf_workflow
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/concatenate-vcfs/v0.1/concatenate-vcfs.wdl" as concatenate_vcfs_workflow

workflow joint_genotype_gvcfs_workflow {

  Array[File] gvcf_gz
  Array[File] gvcf_gz_tbi

  String vcf_basename

  String reference_genome
  Array[String] intervals


  scatter (interval in intervals) {
    call combine_gvcfs_workflow.combine_gvcfs {
      input:
        gvcf_gz = gvcf_gz,
        gvcf_gz_tbi = gvcf_gz_tbi,
        vcf_basename = vcf_basename,
        reference_genome = reference_genome,
        interval_list = interval
    }

    call genotype_gvcf_workflow.genotype_gvcf {
      input:
        gvcf_gz = combine_gvcfs.combined_gvcf_gz,
        gvcf_gz_tbi = combine_gvcfs.combined_gvcf_gz_tbi,
        vcf_basename = vcf_basename,
        reference_genome = reference_genome,
        interval_list = interval
    }
  }

  call concatenate_vcfs_workflow.concatenate_vcfs {
    input:
      vcfs_gz = genotype_gvcf.vcf_gz,
      vcfs_gz_tbi = genotype_gvcf.vcf_gz,
      vcf_basename = vcf_basename
  }

  output {

    File vcf_gz = concatenate_vcfs.concatenated_vcf_gz
    File vcf_gz_tbi = concatenate_vcfs.concatenated_vcf_gz_tbi

  }



}
