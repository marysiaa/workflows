import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-fastqc@1.0.9/src/main/wdl/tasks/rna-seq-fastqc/rna-seq-fastqc.wdl" as rna_seq_fastqc_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-fastqc-se@1.0.5/src/main/wdl/tasks/rna-seq-fastqc-se/rna-seq-fastqc-se.wdl" as rna_seq_fastqc_se_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-fastqc-overrep@1.0.3/src/main/wdl/tasks/rna-seq-fastqc-overrep/rna-seq-fastqc-overrep.wdl" as rna_seq_fastqc_overrep_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow rna_seq_fq_qc {

    Array[File] fastqs_1
    Array[File]? fastqs_2
    Array[File] fastqs_2_defined = select_first([fastqs_2, []])
    Array[String] samples_ids
    Boolean paired = true

    String module_name = "rna_seq_fq_qc"
    String module_version = "1.0.1"


    #1 first task to call
    if(paired) {
        scatter (index in range(length(fastqs_1))) {
            call rna_seq_fastqc_task.rna_seq_fastqc {
                input:
                    fastq_1 = fastqs_1[index],
                    fastq_2 = fastqs_2_defined[index],
                    sample_id = samples_ids[index],
                    index = index
            }
        }
    }

    if(!paired) {
        scatter (index in range(length(fastqs_1))) {
            call rna_seq_fastqc_se_task.rna_seq_fastqc_se {
                input:
                    fastq_1 = fastqs_1[index],
                    sample_id = samples_ids[index],
                    index = index
            }
        }
    }

    Array[Array[File]] zip_files = select_first([rna_seq_fastqc.zip_files, rna_seq_fastqc_se.zip_files])

    call rna_seq_fastqc_overrep_task.rna_seq_fastqc_overrep {
        input:
            zip_files = zip_files
    }

    # after all regular tasks merge bco
    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = [rna_seq_fastqc_overrep.bco]
    Array[File] stdout_tasks = [rna_seq_fastqc_overrep.stdout_log]
    Array[File] stderr_tasks = [rna_seq_fastqc_overrep.stderr_log]

    Array[Array[File]] bco_scatters = [bco_tasks, select_first([rna_seq_fastqc.bco, rna_seq_fastqc_se.bco])]
    Array[Array[File]] stdout_scatters = [stdout_tasks, select_first([rna_seq_fastqc.stdout_log, rna_seq_fastqc_se.stdout_log])]
    Array[Array[File]] stderr_scatters = [stderr_tasks, select_first([rna_seq_fastqc.stderr_log, rna_seq_fastqc_se.stderr_log])]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

        Array[Array[File]] fastqc_zip_files = zip_files
        File overrepresented = rna_seq_fastqc_overrep.overrepresented
        File overrepresented_csv = rna_seq_fastqc_overrep.overrepresented_csv

        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco

    }

}
