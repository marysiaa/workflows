import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@2.0.2/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/string-gp-to-json@2.1.0/src/main/wdl/tasks/string-gp-to-json/string-gp-to-json.wdl" as string_gp_to_json_task
import "https://gitlab.com/intelliseq/workflows/raw/report-panel@1.4.2/src/main/wdl/tasks/report-panel/report-panel.wdl" as report_panel_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow panel_hpo {

  String module_name = "panel_hpo"
  String module_version = "2.0.8"

  Array[String]? hpo_terms
  Array[String]? genes
  Array[String]? diseases
  String? phenotypes_description
  Array[String]? panel_names
  String? panel_json

  String? sample_id

  # 1. Prepare gene panel or use user defined
  if(!defined(panel_json)) {
    call panel_generate_task.panel_generate {
      input:
         sample_id = sample_id,
         hpo_terms = hpo_terms,
         genes = genes,
         diseases = diseases,
         phenotypes_description = phenotypes_description,
         panel_names = panel_names
    }
  }

  if(defined(panel_json)) {
    call string_gp_to_json_task.string_gp_to_json {
      input:
         panel_json = panel_json,
         hpo_terms = hpo_terms,
         genes = genes,
         diseases = diseases,
         phenotypes_description = phenotypes_description,
         panel_names = panel_names,
         sample_id = sample_id
    }
  }

  File panel_ready_or_created = select_first([panel_generate.panel, string_gp_to_json.validated_panel_json])
  File panel_inputs_ready_or_created = select_first([panel_generate.inputs, string_gp_to_json.validated_panel_inputs_json])
  File all_panels_json_ready_or_created = select_first([panel_generate.all_panels, string_gp_to_json.all_panels])

  call report_panel_task.report_panel{
    input:
       sample_id = sample_id,
       panel_json = panel_ready_or_created,
       panel_inputs_json = panel_inputs_ready_or_created
  }

  Array[File] bco_array = select_all([panel_generate.bco, string_gp_to_json.bco, report_panel.bco])
  Array[File] stdout_array = select_all([panel_generate.stdout_log, string_gp_to_json.stdout_log, report_panel.stdout_log])
  Array[File] stderr_array = select_all([panel_generate.stderr_log, string_gp_to_json.stderr_log, report_panel.stderr_log])

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  output {
    File panel = panel_ready_or_created
    File inputs = panel_inputs_ready_or_created
    File all_panels = all_panels_json_ready_or_created

    File panel_report_pdf = report_panel.panel_report_pdf
    File panel_report_docx = report_panel.panel_report_docx

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
