import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-wisecondorx@1.0.2/src/main/wdl/tasks/bam-wisecondorx/bam-wisecondorx.wdl" as bam_wisecondorx_task

workflow bam_nipt {

  String module_name = "bam_nipt"
  String module_version = "2.0.3"
  Array[File] bams
  Array[File] bais

  #1 first task to call
  scatter (index in range(length(bams))) {
    call bam_wisecondorx_task.bam_wisecondorx {
      input:
        bam = bams[index],
        bai = bais[index]
    }
  }

  Array[File] bco_tasks = []
  Array[File] stdout_tasks = []
  Array[File] stderr_tasks = []

  Array[Array[File]] bco_scatters = [bco_tasks]
  Array[Array[File]] stdout_scatters = [stdout_tasks]
  Array[Array[File]] stderr_scatters = [stderr_tasks]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
      bco_array = bco_array,
      stdout_array = stdout_array,
      stderr_array = stderr_array,
      module_name = module_name,
      module_version = module_version
  }

  output {

    Array[Array[File]] plots = bam_wisecondorx.plots
    Array[File] aberrations = bam_wisecondorx.aberrations
    Array[File] bins = bam_wisecondorx.bins
    Array[File] segments = bam_wisecondorx.segments
    Array[File] statistics = bam_wisecondorx.statistics

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
