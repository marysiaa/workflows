import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-panel@1.2.0/src/main/wdl/tasks/vcf-filter-panel/vcf-filter-panel.wdl" as vcf_filter_panel_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp3@1.1.2/src/main/wdl/tasks/vcf-acmg-bp3/vcf-acmg-bp3.wdl" as vcf_acmg_bp3_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp5@1.1.4/src/main/wdl/tasks/vcf-acmg-bp5/vcf-acmg-bp5.wdl" as vcf_acmg_bp5_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp7@1.3.2/src/main/wdl/tasks/vcf-acmg-bp7/vcf-acmg-bp7.wdl" as vcf_acmg_bp7_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp1@1.5.0/src/main/wdl/tasks/vcf-acmg-bp1/vcf-acmg-bp1.wdl" as vcf_acmg_bp1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-ba1@1.2.2/src/main/wdl/tasks/vcf-acmg-ba1/vcf-acmg-ba1.wdl" as vcf_acmg_ba1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bs2@1.1.2/src/main/wdl/tasks/vcf-acmg-bs2/vcf-acmg-bs2.wdl" as vcf_acmg_bs2_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm1@2.2.0/src/main/wdl/tasks/vcf-acmg-pm1/vcf-acmg-pm1.wdl" as vcf_acmg_pm1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm2@1.1.2/src/main/wdl/tasks/vcf-acmg-pm2/vcf-acmg-pm2.wdl" as vcf_acmg_pm2_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm5@1.5.0/src/main/wdl/tasks/vcf-acmg-pm5/vcf-acmg-pm5.wdl" as vcf_acmg_pm5_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp4@2.0.2/src/main/wdl/tasks/vcf-acmg-bp4/vcf-acmg-bp4.wdl" as vcf_acmg_bp4_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pp2@1.5.0/src/main/wdl/tasks/vcf-acmg-pp2/vcf-acmg-pp2.wdl" as vcf_acmg_pp2_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-ps1@1.5.0/src/main/wdl/tasks/vcf-acmg-ps1/vcf-acmg-ps1.wdl" as vcf_acmg_ps1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-ps3@1.5.0/src/main/wdl/tasks/vcf-acmg-ps3/vcf-acmg-ps3.wdl" as vcf_acmg_ps3_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pp3@2.0.2/src/main/wdl/tasks/vcf-acmg-pp3/vcf-acmg-pp3.wdl" as vcf_acmg_pp3_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm4@1.1.2/src/main/wdl/tasks/vcf-acmg-pm4/vcf-acmg-pm4.wdl" as vcf_acmg_pm4_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pvs1@2.4.0/src/main/wdl/tasks/vcf-acmg-pvs1/vcf-acmg-pvs1.wdl" as vcf_acmg_pvs1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pp4@1.0.2/src/main/wdl/tasks/vcf-acmg-pp4/vcf-acmg-pp4.wdl" as vcf_acmg_pp4_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-summary@1.2.4/src/main/wdl/tasks/vcf-acmg-summary/vcf-acmg-summary.wdl" as vcf_acmg_summary_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@1.0.5/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow vcf_anno_acmg {


    ## ClinVar update: 10-12-2022 
    ## UniProt update: 14-10-2022 (v 2022_4)

    String module_name = "vcf_anno_acmg"
    String module_version = "1.6.0"
    File vcf_gz
    File vcf_gz_tbi
    File? panel_json
    Boolean provided_panel_json = if defined(panel_json) then true else false
    Boolean apply_panel_filter = true
    String sample_id = "no_id_provided"
    Boolean split_ISEQ_REPORT_ANN_field = true

    ## filtering is optional
    call vcf_filter_panel_task.vcf_filter_panel {
        input:
            vcf_gz = vcf_gz,
            vcf_gz_tbi = vcf_gz_tbi,
            panel_json = panel_json,
            apply_filter = apply_panel_filter,
            sample_id = sample_id
    }

    call vcf_acmg_bp3_task.vcf_acmg_bp3 {
        input:
            vcf = vcf_filter_panel.filtered_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_bp1_task.vcf_acmg_bp1 {
        input:
            vcf = vcf_acmg_bp3.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_bp5_task.vcf_acmg_bp5 {
        input:
            vcf = vcf_acmg_bp1.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }
    call vcf_acmg_bp7_task.vcf_acmg_bp7 {
        input:
            vcf = vcf_acmg_bp5.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_ba1_task.vcf_acmg_ba1 {
        input:
            vcf = vcf_acmg_bp7.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_bs2_task.vcf_acmg_bs2 {
        input:
            vcf = vcf_acmg_ba1.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_pm1_task.vcf_acmg_pm1 {
        input:
            sample_id = sample_id,
            vcf = vcf_acmg_bs2.annotated_acmg_vcf_gz
    }

    call vcf_acmg_pm2_task.vcf_acmg_pm2 {
        input:
            vcf = vcf_acmg_pm1.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_ps1_task.vcf_acmg_ps1 {
        input:
            vcf = vcf_acmg_pm2.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_bp4_task.vcf_acmg_bp4 {
        input:
            vcf = vcf_acmg_ps1.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_pp2_task.vcf_acmg_pp2 {
        input:
            vcf = vcf_acmg_bp4.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_pm5_task.vcf_acmg_pm5 {
        input:
            vcf = vcf_acmg_pp2.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_ps3_task.vcf_acmg_ps3 {
        input:
            sample_id = sample_id,
            vcf = vcf_acmg_pm5.annotated_acmg_vcf_gz
    }

    call vcf_acmg_pp3_task.vcf_acmg_pp3 {
        input:
            vcf = vcf_acmg_ps3.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_pm4_task.vcf_acmg_pm4 {
        input:
            vcf = vcf_acmg_pp3.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    call vcf_acmg_pvs1_task.vcf_acmg_pvs1 {
        input:
            vcf = vcf_acmg_pm4.annotated_acmg_vcf_gz,
            sample_id = sample_id
    }

    if (provided_panel_json) {
        call vcf_acmg_pp4_task.vcf_acmg_pp4 {
            input:
                vcf = vcf_acmg_pvs1.annotated_acmg_vcf_gz,
                sample_id = sample_id
       }
    }

    call vcf_acmg_summary_task.vcf_acmg_summary {
        input:
            vcf_gz = select_first([vcf_acmg_pp4.annotated_acmg_vcf_gz, vcf_acmg_pvs1.annotated_acmg_vcf_gz]),
            sample_id = sample_id
    }

    call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv {
        input:
            vcf_gz = vcf_acmg_summary.annotated_acmg_vcf_gz,
            vcf_gz_tbi = vcf_acmg_summary.annotated_acmg_vcf_gz_tbi,
            split_ANN_field = false,
            split_CSQ_field = false,
            split_ISEQ_REPORT_ANN_field = split_ISEQ_REPORT_ANN_field,
            output_formats = "json tsv",
            sv_analysis = false,
            sample_id = sample_id
    }



    Array[File] bco_array = select_all([vcf_filter_panel.bco, vcf_acmg_ba1.bco, vcf_acmg_bp1.bco, vcf_acmg_bp3.bco,
                                       vcf_acmg_bp4.bco, vcf_acmg_bp5.bco, vcf_acmg_bp7.bco, vcf_acmg_bs2.bco,
                                       vcf_acmg_pm1.bco, vcf_acmg_pm4.bco, vcf_acmg_pm2.bco , vcf_acmg_pm5.bco,
                                       vcf_acmg_pp2.bco, vcf_acmg_pp3.bco, vcf_acmg_ps1.bco,  vcf_acmg_ps3.bco,
                                       vcf_acmg_pvs1.bco, vcf_acmg_pp4.bco, vcf_acmg_summary.bco])
    Array[File] stdout_array = select_all([vcf_filter_panel.stdout_log, vcf_acmg_ba1.stdout_log, vcf_acmg_bp1.stdout_log,
                                           vcf_acmg_bp3.stdout_log, vcf_acmg_bp4.stdout_log, vcf_acmg_bp5.stdout_log,
                                           vcf_acmg_bp7.stdout_log, vcf_acmg_bs2.stdout_log, vcf_acmg_pm1.stdout_log,
                                           vcf_acmg_pm4.stdout_log, vcf_acmg_pm2.stdout_log , vcf_acmg_pm5.stdout_log,
                                           vcf_acmg_pp2.stdout_log, vcf_acmg_pp3.stdout_log, vcf_acmg_ps1.stdout_log,
                                           vcf_acmg_ps3.stdout_log, vcf_acmg_pvs1.stdout_log, vcf_acmg_pp4.stdout_log,
                                           vcf_acmg_summary.stdout_log])
    Array[File] stderr_array = select_all([vcf_filter_panel.stderr_log, vcf_acmg_ba1.stderr_log, vcf_acmg_bp1.stderr_log,
                                           vcf_acmg_bp3.stderr_log, vcf_acmg_bp4.stderr_log, vcf_acmg_bp5.stderr_log,
                                           vcf_acmg_bp7.stderr_log, vcf_acmg_bs2.stderr_log, vcf_acmg_pm1.stderr_log,
                                           vcf_acmg_pm4.stderr_log, vcf_acmg_pm2.stderr_log , vcf_acmg_pm5.stderr_log,
                                           vcf_acmg_pp2.stderr_log, vcf_acmg_pp3.stderr_log, vcf_acmg_ps1.stderr_log,
                                           vcf_acmg_ps3.stderr_log, vcf_acmg_pvs1.stderr_log, vcf_acmg_pp4.stderr_log,
                                           vcf_acmg_summary.stderr_log])

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {
        File tsv_report = vcf_to_json_or_tsv.output_tsv
        File json_report = vcf_to_json_or_tsv.output_json
        File annotated_acmg_vcf_gz = vcf_acmg_summary.annotated_acmg_vcf_gz
        File annotated_acmg_vcf_gz_tbi = vcf_acmg_summary.annotated_acmg_vcf_gz_tbi
        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco
    }

}
