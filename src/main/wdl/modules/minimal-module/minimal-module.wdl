
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.1.0/src/main/wdl/tasks/bco-merge/2.0.0t/bco-merge.wdl" as bco_merge_task
import "https://gitlab.com/intelliseq/workflows/-/raw/minimal-task@2.0.0/src/main/wdl/tasks/minimal-task/minimal-task.wdl"as minimal_task_task

workflow minimal_module {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'minimal_module'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for module'
    changes: '{"2.0.0": "new module"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  String module_name = "minimal_module"
  String module_version = "2.0.0"


  call minimal_task_task.minimal_task

 Array[File] bco_tasks = [minimal_task.bco]
 Array[File] stdout_tasks = [minimal_task.stdout_log]
 Array[File] stderr_tasks = [minimal_task.stderr_log]


 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_tasks,
       stdout_array = stdout_tasks,
       stderr_array = stderr_tasks,
       module_name = module_name,
       module_version = module_version
 }

  output {

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
