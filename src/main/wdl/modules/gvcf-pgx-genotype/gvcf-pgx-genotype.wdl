import "https://gitlab.com/intelliseq/pgx/raw/resources-pgx-polygenic@1.2.1/src/main/wdl/tasks/resources-pgx-polygenic/resources-pgx-polygenic.wdl" as resources_pgx_polygenic_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-genotype-by-vcf@2.0.6/src/main/wdl/tasks/gvcf-genotype-by-vcf/gvcf-genotype-by-vcf.wdl" as gvcf_genotype_by_vcf_task
import "https://gitlab.com/intelliseq/workflows/-/raw/pgx-phase-beagle@1.1.2/src/main/wdl/tasks/pgx-phase-beagle/pgx-phase-beagle.wdl" as pgx_phase_beagle_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-merge-dv@1.0.0/src/main/wdl/tasks/pgx-merge-dv/pgx-merge-dv.wdl" as pgx_merge_dv_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-polygenic-haplotype@1.0.2/src/main/wdl/tasks/pgx-polygenic-haplotype/pgx-polygenic-haplotype.wdl" as pgx_polygenic_haplotype_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-get-rs-genotype@1.0.2/src/main/wdl/tasks/pgx-get-rs-genotype/pgx-get-rs-genotype.wdl" as pgx_get_rs_genotype_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-merge-jsons-query@1.0.0/src/main/wdl/tasks/pgx-merge-jsons-query/pgx-merge-jsons-query.wdl" as pgx_merge_jsons_query_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow gvcf_pgx_genotype {

  File gvcf_gz
  File gvcf_gz_tbi
  File? vcf_gz
  File? vcf_gz_tbi 
  String var_call_tool = "haplotypeCaller"
  String sample_id = "no_id_provided"
  Array[String]? selected_genes # for example ["CYP2C9", "CYP2C19", "CYP3A4"]
  Boolean run_phasing = true
  Boolean create_queries = true

  String module_name = "gvcf_pgx_genotype"
  String module_version = "1.1.3"


  # 1. Get resources
  call resources_pgx_polygenic_task.resources_pgx_polygenic {
    input:
      genes_list = selected_genes
  }

  if (var_call_tool == "haplotypeCaller") {
    # 2. Genotype gvcf
    call gvcf_genotype_by_vcf_task.gvcf_genotype_by_vcf {
      input:
        sample_id = sample_id,
        sample_gvcf_gz = gvcf_gz,
        sample_gvcf_gz_tbi = gvcf_gz_tbi,
        interval_vcf_gz = resources_pgx_polygenic.pgx_polygenic_vcf_gz,
        interval_vcf_gz_tbi = resources_pgx_polygenic.pgx_polygenic_vcf_gz_tbi
    }
  }

  if (var_call_tool == "deepVariant") {
    # 2. Merge vcf from DeepVariant with PGx polygenic vcf
    call pgx_merge_dv_task.pgx_merge_dv {
      input:
        sample_id = sample_id,
        vcf_gz = vcf_gz,
        vcf_gz_tbi = vcf_gz_tbi,
        pgx_polygenic_vcf = resources_pgx_polygenic.pgx_polygenic_vcf_gz,
        pgx_polygenic_vcf_tbi = resources_pgx_polygenic.pgx_polygenic_vcf_gz_tbi
    }
  } 

  File genotyped_vcf_gz = select_first([gvcf_genotype_by_vcf.genotyped_vcf_gz, pgx_merge_dv.pgx_merged_vcf])
  File genotyped_vcf_gz_tbi = select_first([gvcf_genotype_by_vcf.genotyped_vcf_gz_tbi, pgx_merge_dv.pgx_merged_vcf_tbi])

  # 3. Phasing beagle
  if (run_phasing) {
    call pgx_phase_beagle_task.pgx_phase_beagle {
      input:
        genotyped_vcf_gz = genotyped_vcf_gz,
        phasing_bed = resources_pgx_polygenic.pgx_polygenic_bed,
        sample_id = sample_id
    }
  }

  File final_vcf_gz = select_first([pgx_phase_beagle.vcf_phased, genotyped_vcf_gz])
  File final_vcf_gz_tbi = select_first([pgx_phase_beagle.vcf_phased_tbi, genotyped_vcf_gz_tbi])

  # 4. PGX Polygenic Haplotype
  call pgx_polygenic_haplotype_task.pgx_polygenic_haplotype {
    input:
      vcf = final_vcf_gz,
      sample_id = sample_id,
      selected_genes = selected_genes
  }

  if (create_queries){
    # 5. PGX Get RS Genotype
    call pgx_get_rs_genotype_task.pgx_get_rs_genotype {
      input:
        vcf = final_vcf_gz,
    }

    Array[File] queries = [pgx_polygenic_haplotype.openpgx_queries, pgx_polygenic_haplotype.pharmgkb_queries, 
                          pgx_get_rs_genotype.pharmgkb_rs_queries, pgx_get_rs_genotype.openpgx_rs_queries]

    # 6. PGX Merge JSONs Query
    call pgx_merge_jsons_query_task.pgx_merge_jsons_query {
      input:
        queries = queries
    }
  }

  # Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([resources_pgx_polygenic.bco, gvcf_genotype_by_vcf.bco, pgx_polygenic_haplotype.bco, 
                          pgx_merge_dv.bco, pgx_get_rs_genotype.bco, pgx_merge_jsons_query.bco])
  Array[File] stdout_tasks = select_all([resources_pgx_polygenic.stdout_log, gvcf_genotype_by_vcf.stdout_log, 
                          pgx_merge_dv.stdout_log, pgx_polygenic_haplotype.stdout_log, pgx_get_rs_genotype.stdout_log, pgx_merge_jsons_query.stdout_log])
  Array[File] stderr_tasks = select_all([resources_pgx_polygenic.stderr_log, gvcf_genotype_by_vcf.stderr_log, 
                          pgx_merge_dv.stderr_log, pgx_polygenic_haplotype.stderr_log, pgx_get_rs_genotype.stderr_log, pgx_merge_jsons_query.stderr_log])

  call bco_merge_task.bco_merge {
    input:
      bco_array = bco_tasks,
      stdout_array = stdout_tasks,
      stderr_array = stderr_tasks,
      module_name = module_name,
      module_version = module_version
  }

  output {

    File merged_models = pgx_polygenic_haplotype.merged_models
    File? pharmgkb_query = pgx_merge_jsons_query.pharmgkb_query
    File? openpgx_query = pgx_merge_jsons_query.openpgx_query

    # Genotyped or/and phased vcf
    File? genotyped_or_phased_vcf_gz = final_vcf_gz
    File? genotyped_or_phased_vcf_gz_tbi = final_vcf_gz_tbi

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
