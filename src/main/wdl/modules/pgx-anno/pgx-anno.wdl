import "https://gitlab.com/intelliseq/workflows/raw/openpgx@1.0.1/src/main/wdl/tasks/openpgx/openpgx.wdl" as openpgx_task
import "https://gitlab.com/intelliseq/workflows/raw/pharmgkb-get-annotations@1.0.4/src/main/wdl/tasks/pharmgkb-get-annotations/pharmgkb-get-annotations.wdl" as pharmgkb_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-merge-anno@1.0.1/src/main/wdl/tasks/pgx-merge-anno/pgx-merge-anno.wdl" as pgx_merge_anno_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow pgx_anno {

  File openpgx_query
  File pharmgkb_query

  String sample_id = "no_id_provided"

  String module_name = "pgx_anno"
  String module_version = "1.0.3"

  # 1. OpenPGx
  call openpgx_task.openpgx {
    input:
      genotype_json = openpgx_query,
      sample_id = sample_id
  }

  # 2. PharmGKB
  call pharmgkb_task.pharmgkb_get_annotations {
    input:
      genotypes = pharmgkb_query,
      sample_id = sample_id
  }

  # 3. Merge anno
  call pgx_merge_anno_task.pgx_merge_anno {
    input:
      openpgx_anno = openpgx.recommendation_json,
      pharmgkb_clinical_anno = pharmgkb_get_annotations.clinical_annotations,
      pharmgkb_variant_anno = pharmgkb_get_annotations.variant_annotations,
      sample_id = sample_id
  }

  Array[File] bco_tasks = [openpgx.bco, pharmgkb_get_annotations.bco, pgx_merge_anno.bco]
  Array[File] stdout_tasks = [openpgx.stdout_log, pharmgkb_get_annotations.stdout_log, pgx_merge_anno.stdout_log]
  Array[File] stderr_tasks = [openpgx.stderr_log, pharmgkb_get_annotations.stderr_log, pgx_merge_anno.stderr_log]

  call bco_merge_task.bco_merge {
    input:
      bco_array = bco_tasks,
      stdout_array = stdout_tasks,
      stderr_array = stderr_tasks,
      module_name = module_name,
      module_version = module_version
  }

  output {

    File merged_anno = pgx_merge_anno.merged_anno
    File variants_with_unknown_genotype = pharmgkb_get_annotations.variants_with_unknown_genotype

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
