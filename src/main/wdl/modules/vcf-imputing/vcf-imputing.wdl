import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-chrom@1.1.1/src/main/wdl/tasks/vcf-split-chrom/vcf-split-chrom.wdl" as vcf_split_chrom_task
import "https://gitlab.com/intelliseq/workflows/-/raw/phase-imput-beagle@1.1.0/src/main/wdl/tasks/phase-imput-beagle/phase-imput-beagle.wdl" as phase_imput_beagle_task
import "https://gitlab.com/intelliseq/workflows/-/raw/vcf-beagle-postproc@1.0.3/src/main/wdl/tasks/vcf-beagle-postproc/vcf-beagle-postproc.wdl" as vcf_beagle_postproc_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.4.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-beagle-merge@1.0.5/src/main/wdl/tasks/vcf-beagle-merge/vcf-beagle-merge.wdl" as vcf_beagle_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow vcf_imputing {

    String module_name = "vcf-imputing"
    String module_version = "2.1.1"

    File vcf_gz
    File vcf_gz_tbi
    Boolean gtc_sites_only = false

    String sample_id = "no_id_provided"
    String reference_genome = "grch38-no-alt-analysis-set"

    # 1. Split VCF by chromosomes
    call vcf_split_chrom_task.vcf_split_chrom {
        input:
            vcf_gz = vcf_gz,
            vcf_gz_tbi = vcf_gz_tbi,
            vcf_basename = sample_id,
            return_y = false
    }

    Array[File] chromosome_vcfs = vcf_split_chrom.chromosomes_vcf_gz
    Array[File] chromosome_tbis = vcf_split_chrom.chromosomes_vcf_gz_tbi

    scatter (index in range(length(chromosome_vcfs))) {
        String string_to_remove = "-" + sample_id + ".*"
        String chrom_from_file_name = sub(basename(chromosome_vcfs[index]), string_to_remove, "")
        # 2. Phasing and imputing (beagle)
        call phase_imput_beagle_task.phase_imput_beagle {
            input:
                vcf_gz = chromosome_vcfs[index],
                sample_id = sample_id,
                chromosome = chrom_from_file_name,
        }
    }

    Array[Array[File]] phase_imput_vcfs = phase_imput_beagle.phase_imput_vcfs_gz
    Array[File] phase_imput_vcfs_flatten = flatten(phase_imput_vcfs)

    # 3. Beagle postprocessing
    scatter (index in range(length(phase_imput_vcfs_flatten))) {
        call vcf_beagle_postproc_task.vcf_beagle_postproc {
            input:
                beagle_vcf = phase_imput_vcfs_flatten[index]
        }
    }

    # 4. Merge vcfs
    call vcf_concat_task.vcf_concat {
        input:
            vcf_gz = vcf_beagle_postproc.all_sites_out_vcf,
            vcf_basename = sample_id,
            beagle_merge = true
    }

    # 5. Merge gtc and imputed vcfs
    call vcf_beagle_merge_task.vcf_beagle_merge {
        input:
            gtc_vcf = vcf_gz,
            gtc_vcf_tbi = vcf_gz_tbi,
            gtc_sites_only = gtc_sites_only,
            beagle_vcf = vcf_concat.concatenated_vcf_gz,
            beagle_vcf_tbi = vcf_concat.concatenated_vcf_gz_tbi,
            sample_id = sample_id
    }


    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = [vcf_split_chrom.bco, vcf_concat.bco, vcf_beagle_merge.bco]
    Array[File] stdout_tasks = [vcf_split_chrom.stdout_log, vcf_concat.stdout_log, vcf_beagle_merge.stdout_log]
    Array[File] stderr_tasks = [vcf_split_chrom.stderr_log, vcf_concat.stderr_log, vcf_beagle_merge.stderr_log]

    Array[Array[File]] bco_scatters = [bco_tasks, phase_imput_beagle.bco, vcf_beagle_postproc.bco]
    Array[Array[File]] stdout_scatters = [stdout_tasks, phase_imput_beagle.stdout_log, vcf_beagle_postproc.stdout_log]
    Array[Array[File]] stderr_scatters = [stderr_tasks, phase_imput_beagle.stderr_log, vcf_beagle_postproc.stderr_log]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

        File imputed_vcf_gz = vcf_beagle_merge.merged_vcf
        File imputed_vcf_gz_tbi = vcf_beagle_merge.merged_vcf_tbi
        Array[File] imputed_vcfs_gz = vcf_beagle_merge.vcfs_gz
        Array[File] imputed_vcfs_gz_tbi = vcf_beagle_merge.vcfs_gz_tbi

        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco

    }
}

