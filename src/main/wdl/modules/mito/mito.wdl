import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.3.0/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-to-ubam@1.0.0/src/main/wdl/tasks/fq-to-ubam/fq-to-ubam.wdl" as fq_to_ubam_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-realign@1.0.2/src/main/wdl/tasks/mt-realign/mt-realign.wdl" as mt_realign_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-haplochecker@1.0.3/src/main/wdl/tasks/mt-haplochecker/mt-haplochecker.wdl" as mt_haplochecker_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-varcall-mutect2@1.0.2/src/main/wdl/tasks/mt-varcall-mutect2/mt-varcall-mutect2.wdl" as mt_varcall_mutect2_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-merge-mutect2-outputs@1.0.3/src/main/wdl/tasks/mt-merge-mutect2-outputs/mt-merge-mutect2-outputs.wdl" as mt_merge_mutect2_outputs_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-m2-vcf-filter@1.0.5/src/main/wdl/tasks/mt-m2-vcf-filter/mt-m2-vcf-filter.wdl" as mt_m2_vcf_filter_task
import "https://gitlab.com/intelliseq/workflows/raw/mt-anno@1.0.1/src/main/wdl/tasks/mt-anno/mt-anno.wdl" as mt_anno_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow mito {


    String module_name = "mito"
    String module_version = "1.0.1"
    Array[File] fastqs
    String sample_id = "no_id_provided"
    Boolean paired = true
    Boolean mark_dup = false
    
    ## mutect2 filtering
    Int max_alt_allele_count = 4
    Float vaf_filter_threshold = 0.05
    Float f_score_beta = 1.0
    Int min_alt_reads = 2


    # 1 organize, check and concatenate fastq files
    call fq_organize_task.fq_organize {
        input:
            fastqs = fastqs,
            paired = paired,
            split_files = false,
            concat_files = true
    }
  
        
    # 2. create unmapped BAM
    if (paired) {
        ## this is just for stupid cromwell
        Array[File] fastq2 = select_first([fq_organize.fastqs_2, fq_organize.fastqs_1])
        call fq_to_ubam_task.fq_to_ubam as paired_fq_to_bam {
            input:
                fastq_1 = fq_organize.fastqs_1[0],
                fastq_2 = fastq2[0],
                sample_id = sample_id
        }

    }
    
    if (! paired) {
        call fq_to_ubam_task.fq_to_ubam as single_fq_to_bam {
            input:
                fastq_1 = fq_organize.fastqs_1[0],
                sample_id = sample_id
        }

    }
    
    File ubam = select_first([paired_fq_to_bam.unmapped_bam, single_fq_to_bam.unmapped_bam])
    
    ## NORMAL REFERENCE
    # 3a. Align reads to normal mitochondrial fasta
    call mt_realign_task.mt_realign as normal_alignment {
        input:
            reverted_mt_bam = ubam,
            shifted_alignment = false,
            mark_dup = mark_dup,
            sample_id = sample_id
    }

    # 4a. Call variants aligned to normal mitochondrial fasta (everything except for control region)
    call mt_varcall_mutect2_task.mt_varcall_mutect2 as normal_varcall {
        input:
            mt_bam = normal_alignment.realigned_mt_bam,
            mt_bai = normal_alignment.realigned_mt_bai,
            shifted_bam = false,
            sample_id = sample_id
    }

    ## SHIFTED REFERENCE
    # 3b. Realign extracted reads to shifted mitochondrial fasta
    call mt_realign_task.mt_realign as shifted_alignment {
        input:
            reverted_mt_bam = ubam,
            shifted_alignment = true,
            mark_dup = mark_dup,
            sample_id = sample_id
    }

    # 4b. Call variants aligned to shifted mitochondrial fasta (control region only); liftover outputs (vcf and m2 bam)
    call mt_varcall_mutect2_task.mt_varcall_mutect2 as shifted_varcall {
        input:
            mt_bam = shifted_alignment.realigned_mt_bam,
            mt_bai = shifted_alignment.realigned_mt_bai,
            shifted_bam = true,
            sample_id = sample_id
    }

    # 5. Merge mutect2 outputs
    call mt_merge_mutect2_outputs_task.mt_merge_mutect2_outputs as merge_mt_outputs {
        input:
            mutect_vcfs = [shifted_varcall.mt_vcf_gz, normal_varcall.mt_vcf_gz],
            mutect_bams = [shifted_varcall.mutec2_bam, normal_varcall.mutec2_bam],
            mutect_stats = [shifted_varcall.mutec2_stats, normal_varcall.mutec2_stats],
            sample_id = sample_id,
            output_name = "mt-variant-sites-only"
    }

    # 6. Estimate contamination (haplochecker)
    call  mt_haplochecker_task.mt_haplochecker {
        input:
            mt_bam = normal_alignment.realigned_mt_bam,
            mt_bai = normal_alignment.realigned_mt_bai,
            sample_id = sample_id
    }

    # 7. Filter variants
    call mt_m2_vcf_filter_task.mt_m2_vcf_filter {
        input:
            m2_raw_vcf = merge_mt_outputs.merged_mt_vcf_gz,
            m2_raw_vcf_tbi = merge_mt_outputs.merged_mt_vcf_gz_tbi,
            mutect2_stats = merge_mt_outputs.merged_m2_stats,
            contamination = mt_haplochecker.minor_level,
            max_alt_allele_count = max_alt_allele_count,
            vaf_filter_threshold = vaf_filter_threshold,
            f_score_beta = f_score_beta,
            min_alt_reads = min_alt_reads,
            sample_id = sample_id
    }

    # 8. Annotate variants and prepare TSV file
    call mt_anno_task.mt_anno {
        input:
            vcf = mt_m2_vcf_filter.soft_filtered_mt_vcf_gz,
            tbi = mt_m2_vcf_filter.soft_filtered_mt_vcf_gz_tbi,
            sample_id = sample_id
    }

    
    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = select_all([fq_organize.bco, paired_fq_to_bam.bco, single_fq_to_bam.bco, normal_alignment.bco, normal_varcall.bco,
                                        shifted_alignment.bco, shifted_varcall.bco, merge_mt_outputs.bco, mt_haplochecker.bco, mt_m2_vcf_filter.bco, mt_anno.bco])
    Array[File] stdout_tasks = select_all([fq_organize.stdout_log, paired_fq_to_bam.stdout_log, single_fq_to_bam.stdout_log, normal_alignment.stdout_log, normal_varcall.stdout_log,
                                           shifted_alignment.stdout_log, shifted_varcall.stdout_log, merge_mt_outputs.stdout_log, mt_haplochecker.stdout_log, mt_m2_vcf_filter.stdout_log, 
                                           mt_anno.stdout_log])
    Array[File] stderr_tasks = select_all([fq_organize.stderr_log, paired_fq_to_bam.stderr_log, single_fq_to_bam.stderr_log, normal_alignment.stderr_log, normal_varcall.stderr_log,
                                           shifted_alignment.stderr_log, shifted_varcall.stderr_log, merge_mt_outputs.stderr_log, mt_haplochecker.stderr_log, mt_m2_vcf_filter.stderr_log, 
                                            mt_anno.stderr_log])


    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_tasks,
            stdout_array = stdout_tasks,
            stderr_array = stderr_tasks,
            module_name = module_name,
            module_version = module_version
    }



   output {

    # Soft filtered mt vcf
    File mt_soft_filtered_vcf_gz = mt_m2_vcf_filter.soft_filtered_mt_vcf_gz
    File mt_soft_filtered_vcf_gz_tbi = mt_m2_vcf_filter.soft_filtered_mt_vcf_gz_tbi

    # Mutect2 bam with artificial haplotypes
    File mutect2_mt_bam = merge_mt_outputs.merged_m2_mt_bam
    File mutect2_mt_bai = merge_mt_outputs.merged_m2_mt_bai

    # Bam from alignment to "normal" MT reference
    File realigned_mt_bam = normal_alignment.realigned_mt_bam
    File realigned_mt_bai = normal_alignment.realigned_mt_bai

    # Haplochecker reports
    File contamination_report = mt_haplochecker.contamination_file
    File haplogrep_report = mt_haplochecker.haplogrep_tsv
    File haplogrep_pileup = mt_haplochecker.haplogrep_pileup

    # Final annotated table and vcf
    File annotated_vcf_gz = mt_anno.annotated_vcf
    File annotated_vcf_gz_tbi = mt_anno.annotated_vcf_tbi
    File annotated_tsv = mt_anno.annotated_tsv

    # Duplicates metrics
    File duplication_metrics = normal_alignment.metrics_file

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

    

  }

}
