import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-multiallelic@1.2.1/src/main/wdl/tasks/vcf-split-multiallelic/vcf-split-multiallelic.wdl" as vcf_split_multiallelic_task
import "https://gitlab.com/intelliseq/workflows/-/raw/vcf-filter-clinical-test@1.0.2/src/main/wdl/tasks/vcf-filter-clinical-test/vcf-filter-clinical-test.wdl" as vcf_filter_clinical_test_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-chrom@1.2.0/src/main/wdl/tasks/vcf-split-chrom/vcf-split-chrom.wdl" as vcf_split_chrom_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-freq@2.2.0/src/main/wdl/tasks/vcf-anno-freq/vcf-anno-freq.wdl" as vcf_anno_freq_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-freq@1.3.1/src/main/wdl/tasks/vcf-filter-freq/vcf-filter-freq.wdl" as vcf_filter_freq_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-snpeff@1.5.0/src/main/wdl/tasks/vcf-anno-snpeff/vcf-anno-snpeff.wdl" as vcf_anno_snpeff_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-snpeff@1.2.1/src/main/wdl/tasks/vcf-filter-snpeff/vcf-filter-snpeff.wdl" as vcf_filter_snpeff_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-vep@1.4.0/src/main/wdl/tasks/vcf-anno-vep/vcf-anno-vep.wdl" as vcf_anno_vep_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-dbsnp@1.2.0/src/main/wdl/tasks/vcf-anno-dbsnp/vcf-anno-dbsnp.wdl" as vcf_anno_dbsnp_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-scores@1.1.1/src/main/wdl/tasks/vcf-anno-scores/vcf-anno-scores.wdl" as vcf_anno_scores_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-gnomadcov@1.4.0/src/main/wdl/tasks/vcf-anno-gnomadcov/vcf-anno-gnomadcov.wdl" as vcf_anno_gnomadcov_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-dbnsfp@1.4.0/src/main/wdl/tasks/vcf-anno-dbnsfp/vcf-anno-dbnsfp.wdl" as vcf_anno_dbnsfp_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.4.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-func-var@1.5.0/src/main/wdl/tasks/vcf-anno-func-var/vcf-anno-func-var.wdl" as vcf_anno_func_var_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-func-gene@1.3.6/src/main/wdl/tasks/vcf-anno-func-gene/vcf-anno-func-gene.wdl" as vcf_anno_func_gene_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-repeat@1.0.3/src/main/wdl/tasks/vcf-anno-repeat/vcf-anno-repeat.wdl" as vcf_anno_repeat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-ghr@1.0.4/src/main/wdl/tasks/vcf-anno-ghr/vcf-anno-ghr.wdl" as vcf_anno_ghr_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-cosmic@1.0.6/src/main/wdl/tasks/vcf-anno-cosmic/vcf-anno-cosmic.wdl" as vcf_anno_cosmic_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-civic@1.1.0/src/main/wdl/tasks/vcf-anno-civic/vcf-anno-civic.wdl" as vcf_anno_civic_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-hpo@1.1.1/src/main/wdl/tasks/vcf-anno-hpo/vcf-anno-hpo.wdl" as vcf_anno_hpo_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-add-id@1.0.0/src/main/wdl/tasks/vcf-add-id/vcf-add-id.wdl" as vcf_add_id_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-select-transc@1.0.1/src/main/wdl/tasks/vcf-select-transc/vcf-select-transc.wdl" as vcf_select_transc_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-civic-evidence@1.0.2/src/main/wdl/tasks/vcf-anno-civic-evidence/vcf-anno-civic-evidence.wdl" as vcf_anno_civic_evidence_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@1.0.5/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow vcf_anno {

    meta {
        keywords: '{"keywords": ["vcf", "annotation"]}'
        name: 'Vcf annotation'
        author: 'https://gitlab.com/marpiech'
        copyright: 'Copyright 2019 Intelliseq'
        description: 'Vcf annotation'
        changes: '{"1.17.0": "update Ensembl (snpEff, VEP) and ClinVar", "1.16.1": "small changes in civic evidence task", "1.16.0": "add civic evidence task", "1.15.15": "add civic tsv resources", "1.15.14": "ensembl-resources docker update", "1.15.13": "move vcf-filter-qual to vcf-var-filter", "1.15.12": "update nextProt (snpeff), ClinVar (vcf-anno-func-var) and clinical-test task (new pathogenic_low_penetrance record type in ClinVar)", "1.15.11": "preferentially show MANE selected transcript in report", "1.15.10: "exome frequencies and coverage for combined v6, v7, v8 intervals", "1.15.9": "update CIViC", "1.15.8": "report transcript selection moved to separate task to add NCBI transcript id (VEP based)", "1.15.7": "add IMPACT and AF thresholds for carrier screening and targeted", "1.15.6": "snpeff with nextprot annotation", "1.15.5": "deal with selenocysteine proteins (option in snpeff task)", "1.15.4": "added gnomAD-style id annotation", "1.15.3": "changed to not remove variants with `.` QUAL", "1.15.2": "bug fix in vcf-filter-snpeff", "1.15.1": "vcf-filter-qual added", "1.15.0": "vcf-filter-qual added", "1.14.16": "changed scatter - only for chromosomes with variants", "1.14.15": "docker gatk update", "1.14.14": "vcf-anno-ge-panels removed", "1.14.13": "anno-hpo (diseases names) task added", "1.14.12": "merge 11 and 10 ver", "1.14.11": "ge-panels and func-gene dockers update", "1.14.10": "curated clinvar variants pass clinical filter","1.14.9": "Updated database and fixed command for dbNSFP annotation", "1.14.8": "vcf-to-json-or-tsv new json", "1.14.7": "updated ClinVar resources (vcf-anno-func-var)","1.14.6": "docker updates", "1.14.5": "docker updates", "1.14.4": "vcf-anno-ghr bug fix", "1.14.3": "vcf-anno-ge-panels update", "1.14.2": "gene names bug fix", "1.14.1": "non split vcf-to-json-or-tsv", "1.14.0": "Adding dbscSNV scores, variants altering spicing are now kept regardless of their snpeff impact, removal of low quality mltiallelic variants", "1.13.0": "updated snpeff, vep, func-anno-var and ge-panel tasks and data", "1.12.4": "vcf-to-json-or-tsv with name like vcf_gz", "1.12.3": "adding snpeff filter for panel sequencing - to remove records wo ISEQ_REPORT_ANN field", "1.12.2": "Functional annotations task updated", "1.12.1": "CIViC updated","1.12.0": "Added CIViC annotation task", "1.11.1": "cosmic anno changed to 0-based and Number=. to allow multiallelic records", "1.11.0": "preparing the module to run the carrier screening pipeline, added new task vcf-filter-clinical-test", "1.10.6": "fix regex in Y-and-the rest in vcf-split-chrom","1.10.5": "new version of vcf-anno-dbnsfp with snpsift bug fixed, changed vcf merge tool (bcftools -> MergeVcfs (picrad))", "1.10.4": "vcf-anno-func-gene resources fixed", "1.10.3": "meta updated", "1.10.0": "chroms in scatter taken from file name, new dbSNP resources (154), new dbNSFP resources (4.1c), set -e -o pipefail fixes, all latest dir removed" , "1.9.0": "VEP and loftee added", "1.8.8": "Changed vcf-anno-snpeff (transcript filtering added)", "1.8.7": "Changed vcf-anno-snpeff (Format added to ISEQ_REPORT_ANN field)", "1.8.6": "New resources in freq and gnomadcov", "1.8.5" : "Updated vcf-anno-snpeff, vcf-anno-func-var, vcf-anno-func-gene and vcf-anno-gnomadcov tasks", "1.8.4": "Added vcf-to-json-or-tsv task", "1.8.2": "Typo fix", "1.8.1": "Added missing index", "1.8.0": "Optional filtering on snpEff and frequencies"}'
        
        input_vcf_anno_freq_genome_or_exome: '{"name": "Source of the frequencies annotation", "type": "Array[String]", "default": "exome", "advanced": true , "constraints": {"values": ["genome", "exome"]}, "description": "Decides which frequencies resources should be used for annotation; give: *genome* for WGS and custom target sequencing, *exome* for WES."}'
        input_gnomad_coverage_genome_or_exome: '{"name": "Source of the coverage annotation", "type": "Array[String]", "default": "exome", "advanced": true, "constraints": {"values": ["genome", "exome"]}, "description": "Decides which coverage resources should be used for annotation; give: *genome* for WGS and custom target sequencing, *exome* for WES."}'
        input_ignore_mult_stop_warning : '{"name": "Ignore multiple stop warning?", "type": "Boolean", "default": true, "description": "Decides whether snpEff WARNING_TRANSCRIPT_MULTIPLE_STOP_CODONS should be ignored"}'
        input_threshold_frequency : '{"name": "Frequency threshold", "type": "Float", "default": 0.05, "description": "Variants of higher population frequency are removed as benign"}'
        input_splicing_threshold : '{"name": "Splicing threshold", "type": "Float", "default": 0.6, "description": "Variants with splicing scores above this value are kept, regardless of their snpEff predicted impact"}'

        output_annotated_and_filtered_vcf:'{"name": "vcf.gz", "type": "File", "copy": "True", "description": "Variants"}'
        output_annotated_and_filtered_vcf_tbi: '{"name": "vcf.gz.tbi", "type": "File", "copy": "True", "description": "Variants index"}'
        output_tsv_from_vcf: '{"name": "tsv", "type": "File", "copy": "True", "description": "Output file annotated_and_filtered_vcf converted to TSV file"}'
        output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
        output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
        output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    }

    String module_name = "vcf_anno"
    String module_version = "1.17.0"

    File vcf_gz
    File vcf_gz_tbi

    String vcf_basename = "no_input_provided"

    String vcf_anno_freq_genome_or_exome = "exome"
    String gnomad_coverage_genome_or_exome = "exome"

    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin
    String snpeff_database = "grch38.107"

    Boolean carrier_screening = false

    String impact_vcf_anno_snpeff = "MODIFIER"
    String impact_threshold = "MODERATE"
    Boolean ignore_mult_stop_warning = true
    Float splicing_threshold = 0.6
    Float threshold_frequency = 0.05
    Float quality_threshold_multiallelic = 300

    # VEP options
    String vep_other_options = "--no_intergenic --hgvs --symbol --biotype --uniprot --total_length"
    String vep_transcript_filtering = "--exclude_predicted --gencode_basic"
    String vep_transcript_info = "--appris --tsl --xref_refseq --mane"

    Boolean apply_clinical_filter = if (carrier_screening) then true else false

    ## CIViC evidence thresholds
    String evidence_level_threshold = "C" ## required minimal CIViC evidence level, possible levels A (very well supported), B, C, D, E (not well supported)
    Int evidence_rating_threshold = 3 ## required minimal CIViC rating, from 1 to 5, where 1 is not very reliable and 5 is very reliable 

    # i. Left-normalize indels, split and mark mulitallelic sites
    call vcf_split_multiallelic_task.vcf_split_multiallelic {
        input:
            vcf_gz = vcf_gz,
            vcf_gz_tbi = vcf_gz_tbi,
            vcf_basename = vcf_basename,
            quality_threshold = quality_threshold_multiallelic
    }

    # ii. Annotate VCF functional annotation (variant level)
    call vcf_anno_func_var_task.vcf_anno_func_var {
        input:
            vcf_gz = vcf_split_multiallelic.normalized_vcf_gz,
            vcf_gz_tbi = vcf_split_multiallelic.normalized_vcf_gz_tbi,
            vcf_basename = vcf_basename
    }

    # iii. Filter VCF by clinical testing
    if (apply_clinical_filter) {
        call vcf_filter_clinical_test_task.vcf_filter_clinical_test {
            input:
                vcf_gz = vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz,
                vcf_gz_tbi = vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz_tbi,
                vcf_basename = vcf_basename
        }
    }

    File vcf_to_split = select_first([vcf_filter_clinical_test.filtered_vcf_gz, vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz])
    File vcf_tbi_to_split = select_first([vcf_filter_clinical_test.filtered_vcf_gz_tbi, vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz_tbi])

    # iv. Split VCF by chromosomes
    call vcf_split_chrom_task.vcf_split_chrom {
        input:
            vcf_gz = vcf_to_split,
            vcf_gz_tbi = vcf_tbi_to_split,
            vcf_basename = vcf_basename
    }

    Array[File] chromosome_vcfs = vcf_split_chrom.chromosomes_vcf_gz
    Array[File] chromosome_tbis = vcf_split_chrom.chromosomes_vcf_gz_tbi


    scatter (index in range(length(chromosome_vcfs))) {
    String string_to_remove = "-" + vcf_basename + ".*"
    String chrom_from_file_name = sub(basename(chromosome_vcfs[index]), string_to_remove, "")

        # v. Annotate VCFs with frequencies
        call vcf_anno_freq_task.vcf_anno_freq {
            input:
                vcf_gz = vcf_split_chrom.chromosomes_vcf_gz[index],
                vcf_gz_tbi = vcf_split_chrom.chromosomes_vcf_gz_tbi[index],
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                genome_or_exome = vcf_anno_freq_genome_or_exome,
                index = index

        }

        # vi. Filter VCFs by frequencies
        call vcf_filter_freq_task.vcf_filter_freq {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_anno_freq.annotated_with_frequencies_vcf_gz,
                vcf_gz_tbi = vcf_anno_freq.annotated_with_frequencies_vcf_gz_tbi,
                gnomad_af_popmax_threshold = threshold_frequency,
                mitomap_af_threshold = threshold_frequency,
                index = index
        }

        # vii. Annotate VCFs with SnpEff and gene names
        call vcf_anno_snpeff_task.vcf_anno_snpeff {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_filter_freq.filtered_by_frequencies_vcf_gz,
                vcf_gz_tbi = vcf_filter_freq.filtered_by_frequencies_vcf_gz_tbi,
                impact = impact_vcf_anno_snpeff,
                index = index,
                snpeff_java_mem = snpeff_java_mem,
                snpeff_database = snpeff_database,
                ignore_mult_stop_warning = ignore_mult_stop_warning
        }

        # viii. Annotate vcf with VEP, loftee and dbscSNV
        call vcf_anno_vep_task.vcf_anno_vep {
            input:
                vcf_gz = vcf_anno_snpeff.annotated_with_snpeff_vcf_gz,
                vcf_gz_tbi = vcf_anno_snpeff.annotated_with_snpeff_vcf_gz_tbi,
                sample_id = vcf_basename,
                chromosome = chrom_from_file_name,
                add_loftee = true,
                add_dbscsnv = true,
                other_options = vep_other_options,
                transcript_filtering = vep_transcript_filtering,
                transcript_info = vep_transcript_info,
                index = index
        }

        # ix. Filter VCFs by SnpEff impact
        call vcf_filter_snpeff_task.vcf_filter_snpeff {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_anno_vep.anno_vcf_gz,
                vcf_gz_tbi = vcf_anno_vep.anno_vcf_gz_tbi,
                impact = impact_threshold,
                splicing_threshold = splicing_threshold,
                index = index
        }


        # x. Annotate VCFs by dbSNP
        call vcf_anno_dbsnp_task.vcf_anno_dbsnp {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_filter_snpeff.filtered_by_snpeff_impact_vcf_gz,
                vcf_gz_tbi = vcf_filter_snpeff.filtered_by_snpeff_impact_vcf_gz_tbi,
                index = index
        }

        # xi. Annotate VCF with pathogenicity prediction scores, evolutionary
        #    conservation etc.
        call vcf_anno_scores_task.vcf_anno_scores {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_anno_dbsnp.annotated_with_dbsnp_rsids_vcf_gz,
                vcf_gz_tbi = vcf_anno_dbsnp.annotated_with_dbsnp_rsids_vcf_gz_tbi,
                index = index
        }

        # xii. Annotate VCFs with gnomad coverage
        call vcf_anno_gnomadcov_task.vcf_anno_gnomadcov {
            input:
                vcf_gz = vcf_anno_scores.annotated_with_scores_vcf_gz,
                vcf_gz_tbi = vcf_anno_scores.annotated_with_scores_vcf_gz_tbi,
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                genome_or_exome = gnomad_coverage_genome_or_exome,
                index = index
        }

        # xiii. Annotate VCFs by dbNSFP
        call vcf_anno_dbnsfp_task.vcf_anno_dbnsfp {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_anno_gnomadcov.annotated_with_gnomad_coverage_vcf_gz,
                vcf_gz_tbi = vcf_anno_gnomadcov.annotated_with_gnomad_coverage_vcf_gz_tbi,
                index = index

        }
    }

    # xiv. Concatenate VCFs
    call vcf_concat_task.vcf_concat {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_dbnsfp.annotate_vcf_with_dbnsfp_vcf_gz,
    }

    # xv. Annotate VCF functional annotation (gene) level
    call vcf_anno_func_gene_task.vcf_anno_func_gene {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_concat.concatenated_vcf_gz,
            vcf_gz_tbi = vcf_concat.concatenated_vcf_gz_tbi
    }

    # xvi. Annotate VCF simple repeats
    call vcf_anno_repeat_task.vcf_anno_repeat {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_func_gene.hpo_annotated_vcf_gz,
            vcf_gz_tbi = vcf_anno_func_gene.hpo_annotated_vcf_gz_tbi
    }

    # xvii. Annotate VCF ghr
    call vcf_anno_ghr_task.vcf_anno_ghr {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz,
            vcf_gz_tbi = vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz_tbi
    }

    # xviii. Annotate VCF hpo
    call vcf_anno_hpo_task.vcf_anno_hpo {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_ghr.annotated_with_ghr_vcf_gz,
            vcf_gz_tbi = vcf_anno_ghr.annotated_with_ghr_vcf_gz_tbi
    }

    # xix. Annotate VCF cosmic
    call vcf_anno_cosmic_task.vcf_anno_cosmic {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_anno_hpo.annotated_with_hpo_vcf_gz,
            vcf_gz_tbi = vcf_anno_hpo.annotated_with_hpo_vcf_gz_tbi,
    }
     # xx. Annotate VCF gnomAD like ID
    call vcf_add_id_task.vcf_add_id {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_anno_cosmic.annotated_vcf_gz,
            vcf_gz_tbi = vcf_anno_cosmic.annotated_vcf_gz_tbi
    }
     # xxi. Select report transcript, add NCBI ID
    call vcf_select_transc_task.vcf_select_transc {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_add_id.annotated_vcf_gz,
            vcf_gz_tbi = vcf_add_id.annotated_vcf_gz_tbi
    }

    # xxii. Annotate VCF civic
    call vcf_anno_civic_task.vcf_anno_civic {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_select_transc.output_vcf_gz,
            vcf_gz_tbi = vcf_select_transc.output_vcf_gz_tbi
    }
        
    # xxiii. Annotate VCF civic evidence
    call vcf_anno_civic_evidence_task.vcf_anno_civic_evidence {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_civic.annotated_vcf,
            vcf_gz_tbi = vcf_anno_civic.annotated_vcf_tbi,
            evidence_level_threshold = evidence_level_threshold,
            evidence_rating_threshold = evidence_rating_threshold
    }

    # xxiv. Convert VCF to TSV
    call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_anno_civic_evidence.annotated_vcf_gz,
            vcf_gz_tbi = vcf_anno_civic_evidence.annotated_vcf_gz_tbi,
            output_formats = 'tsv',
            split_ANN_field = false,
            split_CSQ_field = false,
            split_ISEQ_REPORT_ANN_field = true
    }

    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = select_all([vcf_split_multiallelic.bco, vcf_anno_func_var.bco, vcf_filter_clinical_test.bco,
                            vcf_split_chrom.bco, vcf_concat.bco, vcf_anno_func_gene.bco, vcf_anno_repeat.bco, vcf_anno_ghr.bco,
                            vcf_anno_cosmic.bco, vcf_anno_civic.bco, vcf_anno_hpo.bco, vcf_add_id.bco, vcf_select_transc.bco,
                            vcf_to_json_or_tsv.bco])
    Array[File] stdout_tasks = select_all([vcf_split_multiallelic.stdout_log, vcf_anno_func_var.stdout_log,
                               vcf_filter_clinical_test.stdout_log, vcf_split_chrom.stdout_log, vcf_concat.stdout_log,
                               vcf_anno_func_gene.stdout_log, vcf_anno_repeat.stdout_log, vcf_anno_ghr.stdout_log,
                               vcf_anno_cosmic.stdout_log, vcf_anno_civic.stdout_log, vcf_anno_hpo.stdout_log,
                               vcf_add_id.stdout_log, vcf_select_transc.stdout_log, vcf_to_json_or_tsv.stdout_log])
    Array[File] stderr_tasks = select_all([vcf_split_multiallelic.stderr_log, vcf_anno_func_var.stderr_log,
                               vcf_filter_clinical_test.stderr_log, vcf_split_chrom.stderr_log, vcf_concat.stderr_log,
                               vcf_anno_func_gene.stderr_log, vcf_anno_repeat.stderr_log, vcf_anno_ghr.stderr_log,
                               vcf_anno_cosmic.stderr_log, vcf_anno_cosmic.stderr_log, vcf_anno_hpo.stderr_log,
                               vcf_add_id.stderr_log, vcf_select_transc.stderr_log, vcf_to_json_or_tsv.stderr_log])

    Array[Array[File]] bco_scatters = [bco_tasks, vcf_anno_freq.bco, vcf_filter_freq.bco, vcf_anno_snpeff.bco, vcf_filter_snpeff.bco, vcf_anno_vep.bco,
                                      vcf_anno_dbsnp.bco, vcf_anno_scores.bco, vcf_anno_gnomadcov.bco, vcf_anno_dbnsfp.bco]
    Array[Array[File]] stdout_scatters = [stdout_tasks, vcf_anno_freq.stdout_log, vcf_filter_freq.stdout_log, vcf_anno_snpeff.stdout_log, vcf_filter_snpeff.stdout_log,
                                          vcf_anno_vep.stdout_log, vcf_anno_dbsnp.stdout_log, vcf_anno_scores.stdout_log, vcf_anno_gnomadcov.stdout_log, vcf_anno_dbnsfp.stdout_log]
    Array[Array[File]] stderr_scatters = [stderr_tasks, vcf_anno_freq.stderr_log, vcf_filter_freq.stderr_log, vcf_anno_snpeff.stderr_log, vcf_filter_snpeff.stderr_log,
                                          vcf_anno_vep.stderr_log, vcf_anno_dbsnp.stderr_log, vcf_anno_scores.stderr_log, vcf_anno_gnomadcov.stderr_log, vcf_anno_dbnsfp.stderr_log]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

     call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

        File annotated_and_filtered_vcf = vcf_anno_civic_evidence.annotated_vcf_gz
        File annotated_and_filtered_vcf_tbi = vcf_anno_civic_evidence.annotated_vcf_gz_tbi

        File tsv_from_vcf = vcf_to_json_or_tsv.output_tsv

        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco

    }

}
