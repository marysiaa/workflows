import "https://gitlab.com/intelliseq/workflows/-/raw/vcf-select-var@1.0.1/src/main/wdl/tasks/vcf-select-var/vcf-select-var.wdl" as vcf_select_var_task
import "https://gitlab.com/intelliseq/workflows/-/raw/json-select-var@1.0.2/src/main/wdl/tasks/json-select-var/json-select-var.wdl" as json_select_var_task
import "https://gitlab.com/intelliseq/workflows/-/raw/vcf-tag-inheritance@1.0.2/src/main/wdl/tasks/vcf-tag-inheritance/vcf-tag-inheritance.wdl" as vcf_tag_inheritance_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@1.0.6/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/-/raw/json-prioritize-var@1.2.4/src/main/wdl/tasks/json-prioritize-var/json-prioritize-var.wdl" as json_prioritize_var_task
import "https://gitlab.com/intelliseq/workflows/-/raw/igv-screenshots@3.2.2/src/main/wdl/tasks/igv-screenshots/igv-screenshots.wdl" as igv_screenshots_task
import "https://gitlab.com/intelliseq/workflows/-/raw/report-acmg@2.4.12/src/main/wdl/tasks/report-acmg/report-acmg.wdl"  as report_acmg_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.1/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow vcf_acmg_report {

    String module_name = "vcf_acmg_report"
    String module_version = "3.2.12"

    File? vcf_gz
    File? vcf_gz_tbi
    File? cnv_json
    File? variants_json
    String? variants_list
    Boolean variants_list_defined = defined(variants_list)
    Array[File]? bams
    Boolean is_bams_defined = defined(bams)
    File? panel_json
    Boolean provided_panel_json = if defined(panel_json) then true else false
    File? sample_info_json
    File? panel_inputs_json
    File? genes_bed_target_json
    File? gene_panel_logs
    Int timezoneDifference = 0

    Float threshold_frequency = 0.05
    String impact_threshold = "MODERATE"
    Float quality_threshold = 0
    String var_call_tool = "haplotypeCaller"
    String aligner_tool = "bwa-mem"

    Boolean germline_analysis = true
    String analysis_start = "vcf"
    String analysis_group = "germline"
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    String analysisEnvironment = "IntelliseqFlow"
    String genome_or_exome = "exome"
    Boolean apply_panel_filter = if (genome_or_exome == "target") then false else true
    String sample_id = "no_id_provided"
    String? kit
    Int max_igv_picture_processing_time_in_ms = 100000
    Boolean view_as_pairs = false

    ## severity threshold for acmg (variants of that severity+ will be selected)
    ## possible values "benign", "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"
    String acmg_threshold = "uncertain"

    ## severity threshold for ClinVar (variants of that severity+ will be selected)
    ## possible values "pathogenic", "likely_pathogenic", "uncertain"
    String clinvar_threshold = "likely_pathogenic"

    if(!defined(variants_json)) {
        call vcf_select_var_task.vcf_select_var {
            input:
                vcf_gz = vcf_gz,
                vcf_gz_tbi = vcf_gz_tbi,
                sample_id = sample_id,
                acmg_threshold = acmg_threshold,
                clinvar_threshold = clinvar_threshold
        }

        call vcf_tag_inheritance_task.vcf_tag_inheritance {
            input:
                vcf_gz = vcf_select_var.filtered_vcf_gz,
                vcf_gz_tbi = vcf_select_var.filtered_vcf_gz_tbi,
                sample_id = sample_id
        }

        call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv {
            input:
                vcf_gz = vcf_tag_inheritance.tagged_vcf_gz,
                vcf_gz_tbi = vcf_tag_inheritance.tagged_vcf_gz_tbi,
                split_ANN_field = false,
                split_CSQ_field = false,
                split_ISEQ_REPORT_ANN_field = true,
                output_formats = "json",
                sv_analysis = false,
                select_columns = true,
                sample_id = sample_id
        }
        call json_prioritize_var_task.json_prioritize_var {
                input:
                    var_json = vcf_to_json_or_tsv.output_json,
                    var_num = 50
        }
    }

    if(defined(variants_json)) {
        if(variants_list_defined) {
            call json_select_var_task.json_select_var {
                input:
                    variants_json = variants_json,
                    variants_list = variants_list,
                    sample_id = sample_id
            }
        }
        File selected_variants_json = select_first([json_select_var.selected_json, variants_json])
        call json_prioritize_var_task.json_prioritize_var as json_prioritize_var_input_json {
            input:
                var_json = selected_variants_json,
                var_num = 100
        }
    }

    File variant_json = select_first([json_prioritize_var.selected_var_json, json_prioritize_var_input_json.selected_var_json])
    File positions = select_first([json_prioritize_var.position_list, json_prioritize_var_input_json.position_list])


    if (is_bams_defined) {
        call igv_screenshots_task.igv_screenshots {
            input:
                positions_list = positions,
                bams = bams,
                sample_id = sample_id,
                max_picture_processing_time_in_ms = max_igv_picture_processing_time_in_ms,
                view_as_pairs = view_as_pairs
        }
        Int igv_output_length = length(igv_screenshots.compressed_igv_pngs)
        if(igv_output_length != 0 ) {
            File pictures_gz = igv_screenshots.compressed_igv_pngs[0]
            File jigv_html = igv_screenshots.jigv_html[0]
        }
    }

    call report_acmg_task.report_acmg {
        input:
            var_json = variant_json,
            cnv_json = cnv_json,
            sample_info_json = sample_info_json,
            pictures_gz = pictures_gz,
            panel_json = panel_json,
            panel_inputs_json = panel_inputs_json,
            genes_bed_target_json = genes_bed_target_json,
            gene_panel_logs = gene_panel_logs,
            timezoneDifference = timezoneDifference,
            sample_id = sample_id,
            genome_or_exome = genome_or_exome,
            kit = kit,
            var_call_tool = var_call_tool,
            aligner_tool = aligner_tool,
            germline_analysis = germline_analysis,
            analysis_start = analysis_start,
            analysis_group = analysis_group,
            analysisDescription = analysisDescription,
            analysisName = analysisName,
            analysisWorkflow = analysisWorkflow,
            analysisWorkflowVersion = analysisWorkflowVersion,
            analysisEnvironment = analysisEnvironment,
            threshold_frequency = threshold_frequency,
            impact_threshold = impact_threshold,
            quality_threshold = quality_threshold
    }

    Array[File] bco_array = select_all([json_prioritize_var.bco, report_acmg.bco, igv_screenshots.bco])
    Array[File] stdout_array = select_all([json_prioritize_var.stdout_log, report_acmg.stdout_log, igv_screenshots.stdout_log])
    Array[File] stderr_array = select_all([json_prioritize_var.stderr_log, report_acmg.stderr_log, igv_screenshots.stderr_log])

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

        File ang_docx_report = report_acmg.variants_report_ang_docx
        File ang_pdf_report = report_acmg.variants_report_ang_pdf
        File? igv_screenshots_tar_gz = pictures_gz
        File? jigv_html = jigv_html
        File? json_report = vcf_to_json_or_tsv.output_json
        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco
    }

}
