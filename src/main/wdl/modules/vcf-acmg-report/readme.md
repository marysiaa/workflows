## Panel

Panel shoud be in json format. Example:
```
[{"name":"CYP11A1","score":100.0,"type":"phenotype"},{"name":"FBN1","score":100.0,"type":"Marfan"},{"name":"CYB5A","score":66.66666666666666,"type":"Ehlers-Danlos"},{"name":"CACNA1S","score":50.0,"type":"user"}]
```
Type can be either "phenotype", "user" or the disease keyword.


The phenotypes panel is obtained by (here for phenotypes HP:0100543 and HP:0001251):
```
curl --request POST \
  --url http://genetraps.intelliseq.pl:8082/get-genes \
  --header 'content-type: application/json' \
  --data '{
    "hpoTerms": [
        "HP:0100543",
        "HP:0001251"
    ],
    "threshold": 0.25
}' | jq -c '[ .[] +{type:"phenotype"} ]' > phenotype-panel.json
```
The threshold can be changed if there are too many genes in the panel (>300). (To check how many genes are there use: `cat panel.json | sed 's/name/\n/g' | wc -l`).


The diseases panels are obtained by using a keyword (for example for Marfan Syndrome only "Marfan" should be written, here for Ehlers-Danlos Syndrome):
```
curl --request GET \
  --url 'http://genetraps.intelliseq.pl:8082/get-genes-by-disease-keyword?firstLetters=Ehlers-Danlos'\
  | jq -c '[{"name":.[]}]' | jq -c '[ .[] + {score:100.0, type:"Ehlers-Danlos"} ]' > ehlers-danlos-panel.json
```
For diseases with space between words use `20%` as a space (for example: `Osteogenesis%20imperfecta`).
For diseases the score is always 100.0.


The user-defined genes should be inputed manually, as below:
```
[{"name":"CYP11A1","score":100.0,"type":"user"},{"name":"AKAP9","score":100.0,"type":"user"}]
```
For user-defined genes the score has to be 100.0.

All the panels have to be merged afterwards:
```
jq -s 'add' *-panel.json | jq -M 'unique_by(.name)|sort_by(.name)|reverse|sort_by(.score)|reverse' | jq -c . > panel.json
```


## Phenotypes

Example phenotypes file:
```
["Hypertrophic cardiomyopathy HP:0001639",
"Atrial fibrillation HP:0005110"]
```
