import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-star@1.0.1/src/main/wdl/tasks/rna-seq-star/rna-seq-star.wdl" as rna_seq_star_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-bam-metrics@1.0.0/src/main/wdl/tasks/rna-seq-bam-metrics/rna-seq-bam-metrics.wdl" as rna_seq_bam_metrics_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.1/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow rna_seq_align {

    Array[File] fastqs_1
    Array[File]? fastqs_2
    Array[File] fastqs_2_defined = select_first([fastqs_2, []])
    Array[String] samples_ids
    Boolean paired = true
    Array[File] ref_genome_index
    File genome

    String module_name = "rna_seq_align"
    String module_version = "1.1.0"


    #1 first task to call
    if(paired) {
        scatter (index in range(length(fastqs_1))) {
            call rna_seq_star_task.rna_seq_star {
                input:
                    fastq_1 = fastqs_1[index],
                    fastq_2 = fastqs_2_defined[index],
                    ref_genome_index = ref_genome_index,
                    sample_id = samples_ids[index],
                    index = index
            }
        }
    }

    if(!paired) {
        scatter (index in range(length(fastqs_1))) {
            call rna_seq_star_task.rna_seq_star as rna_seq_star_se {
                input:
                    fastq_1 = fastqs_1[index],
                    ref_genome_index = ref_genome_index,
                    sample_id = samples_ids[index],
                    index = index
            }
        }
    }

    Array[File] bam_files = select_first([rna_seq_star.bam, rna_seq_star_se.bam])
    Array[File] bai_files = select_first([rna_seq_star.bai, rna_seq_star_se.bai])
    Array[File] summary = select_first([rna_seq_star.summary, rna_seq_star_se.summary])

    scatter (index in range(length(bam_files))) {
        call rna_seq_bam_metrics_task.rna_seq_bam_metrics {
            input:
                genome = genome,
                bam = bam_files[index],
                bai = bai_files[index],
                index = index
        }
    }

    # after all regular tasks merge bco
    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = []
    Array[File] stdout_tasks = []
    Array[File] stderr_tasks = []

    Array[Array[File]] bco_scatters = [bco_tasks, select_first([rna_seq_star.bco, rna_seq_star_se.bco]), rna_seq_bam_metrics.bco]
    Array[Array[File]] stdout_scatters = [stdout_tasks, select_first([rna_seq_star.stdout_log, rna_seq_star_se.stdout_log]), rna_seq_bam_metrics.stdout_log]
    Array[Array[File]] stderr_scatters = [stderr_tasks, select_first([rna_seq_star.stderr_log, rna_seq_star_se.stderr_log]), rna_seq_bam_metrics.stderr_log]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {
        Array[File] bams = bam_files
        Array[File] bams_bais = bai_files
        Array[File] summaries = summary
        Array[File] bam_metrics = rna_seq_bam_metrics.bam_metrics
        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco
    }
}
