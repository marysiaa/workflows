import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.4/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.8/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@2.0.2/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/string-gp-to-json@2.1.1/src/main/wdl/tasks/string-gp-to-json/string-gp-to-json.wdl" as string_gp_to_json_task
import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.4.0/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-qc@1.6.4/src/main/wdl/modules/fq-qc/fq-qc.wdl" as fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.1/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-dragmap-align@1.0.5/src/main/wdl/modules/fq-dragmap-align/fq-dragmap-align.wdl" as fq_dragmap_align_module
import "https://gitlab.com/intelliseq/workflows/raw/sv-calling@1.8.1/src/main/wdl/modules/sv-calling/sv-calling.wdl" as sv_calling_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling@1.3.11/src/main/wdl/modules/bam-varcalling/bam-varcalling.wdl" as bam_varcalling_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling-dv@1.0.5/src/main/wdl/modules/bam-varcalling-dv/bam-varcalling-dv.wdl" as bam_varcalling_dv_module
import "https://gitlab.com/intelliseq/workflows/raw/mt-varcall@1.0.9/src/main/wdl/modules/mt-varcall/mt-varcall.wdl" as mt_varcall_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-acmg@1.6.0/src/main/wdl/modules/vcf-anno-acmg/vcf-anno-acmg.wdl" as vcf_anno_acmg_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-report@3.2.10/src/main/wdl/modules/vcf-acmg-report/vcf-acmg-report.wdl" as vcf_acmg_report_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno@1.17.0/src/main/wdl/modules/vcf-anno/vcf-anno.wdl" as vcf_anno_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-qc@3.0.8/src/main/wdl/modules/bam-qc/bam-qc.wdl" as bam_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/detection-chance@1.5.2/src/main/wdl/modules/detection-chance/detection-chance.wdl" as detection_chance_module
import "https://gitlab.com/intelliseq/workflows/raw/sex-check@1.1.5/src/main/wdl/modules/sex-check/sex-check.wdl" as sex_check_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-var-filter@1.0.8/src/main/wdl/modules/vcf-var-filter/vcf-var-filter.wdl" as vcf_var_filter_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow germline {

    String pipeline_name = "germline"

    String pipeline_version = "1.28.0"

    String sample_id = "no_id_provided"
    String reference_genome = "grch38-no-alt"

    # Provide fastq files; they are obligatory
    Array[File] fastqs

    # Determine which varcalling tool should be used
    String var_call_tool = "haplotypeCaller"    #haplotypeCaller or deepVariant

    # Determine which aligning tool should be used
    String aligner_tool = "bwa-mem"

    # Gene panel
    Array[String]? hpo_terms
    Array[String]? genes
    Array[String]? diseases
    String? phenotypes_description
    Array[String]? panel_names
    Boolean is_input_for_panel_generate_defined = (defined(hpo_terms) || defined(genes) || defined(diseases) || defined(phenotypes_description) || defined(panel_names))
    String? panel_json

    # Patient information
    File? sample_info_json

    # Report information (time zone difference and ID)
    Int timezoneDifference = 0

    String genome_or_exome = "exome" # Possible values "exome", "genome", "target"
    String analysis_start = "fastq"
    String analysis_group = "germline"
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    String analysisEnvironment = "IntelliseqFlow"

    # for WGS or WES
    String kit = "exome-v7" # Possible values "exome-v6", "exome-v7"
    String kit_choice = if (genome_or_exome == "exome") then kit else genome_or_exome

    # for targeted-sequencing
    File? target_bed
    Array[String]? target_genes
    Int padding = 1000
    String freq_and_coverage_source = if (genome_or_exome == "exome") then "exome" else "genome"
    Boolean apply_panel_filter = if (genome_or_exome == "target") then false else true

    Int max_no_pieces_to_scatter_an_interval_file = 6
    Array[File]? other_bams = []
    Array[File]? other_bais = []

    ## Bam filtering options (within fq-bwa-align module)
    Boolean add_bam_filter = true
    ## Which filter should be applied?:
    ## 1 - removing reads with high fraction of mismatches
    ## 2 - removing reads which are both very short and with soft-clipped at both ends
    ## 3 - combining both filter types (1+2)
    String filter_choice = "3"
    ## Set mismatch fraction that is not accepted (for filters 1 and 3)
    Float mismatch_threshold = 0.1
    Boolean mark_dup = true

    # sv parameters
    # WGS - lumpy/smoove
    Boolean run_sv_calling = false
    Boolean add_snp_data_to_sv = true
    Float del_cov_max = 0.75
    Float dup_cov_min = 1.3
    Boolean exclude_lcr = true
    Boolean exclude_chroms = true

    # WES ExomeDepth
    File? ref_counts ## must be provided for ExomeDepth analysis
    File? custom_bed ## if kit other than v7 or v6 was used 
    Float bf_threshold = if (genome_or_exome == "target") then 0 else 40 ##??

    ## sv annotation
    Float panel_threshold1 = 30.0
    Float panel_threshold2 = 45.0
    String include_ci = "yes"   ## other option "no"
    Boolean apply_rank_filter = true
    Boolean apply_panel_filter_sv = true
    String rank_threshold = "likely_pathogenic" ## other options: "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"
    Boolean create_pictures = true ## create breakpoint pictures for WGS only
    Boolean create_coverage_plots = true ## create coverage plots for WGS only

    # HC parameters
    Float contamination = 0.0
    Boolean ignore_soft_clips = true

    # VQSR filtering
    Boolean apply_vqsr = false
    
    # AD filtering
    Float ad_binom_threshold = 0.01 # threshold probability for the AD binomal test
    Boolean apply_ad_filter = false # decides whether apply AD filtering (for exome and genome data)

    # mt filtering
    Int max_alt_allele_count = 4
    Float vaf_filter_threshold = 0.05
    Float f_score_beta = 1.0
    Int min_alt_reads = 2

    ## anno module filtering
    Float splicing_threshold = 0.6   # for dbscSNV ada and rf scores
    Float threshold_frequency = 0.05
    Float quality_threshold_multiallelic = if (aligner_tool == "bwa-mem" && var_call_tool == "haplotypeCaller") then 300 else if (aligner_tool == "dragmap" && var_call_tool == "haplotypeCaller") then 70 else 30 # for multiallelic variants removal
    Float quality_threshold = 200
    String impact_threshold = "MODERATE"
    Boolean ignore_mult_stop_warning = true
    ## CIViC evidence thresholds
    String evidence_level_threshold = "C" ## required minimal CIViC evidence level, possible levels A (very well supported), B, C, D, E (not well supported)
    Int evidence_rating_threshold = 3 ## required minimal CIViC rating, from 1 to 5, where 1 is not very reliable and 5 is very reliable 


    ## report variants selection
    ## severity threshold for acmg (variants of that severity+ will be selected)
    ## possible values "benign", "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"
    String acmg_threshold = "uncertain"

    ## severity threshold for ClinVar (variants of that severity+ will be selected)
    ## possible values "pathogenic", "likely_pathogenic", "uncertain"
    String clinvar_threshold = "likely_pathogenic"

    # run modules/tasks
    Boolean run_fq_qc = true
    Boolean run_vcf_var_filter = true
    Boolean run_vcf_anno = true
    Boolean run_vcf_acmg_report = true
    Boolean run_detection_chance = true
    Boolean run_sex_check = true
    Boolean run_bam_qc = true
    Boolean run_mt_varcall = true
    Boolean run_anno_acmg = true
    Boolean run_bsqr = if (var_call_tool == "haplotypeCaller") then true else false 

    # Carrier screening
    Boolean carrier_screening = false

    # Java options for bam_metrics and bam_gatk_hc
    String java_mem_options =  "-Xms4g -Xmx63g" # use "-Xms4g -Xmx8g" on anakin

    # Java options fo snpeff
    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin

    ## Bam WGS metrics algorithm (fast vs normal)
    Boolean use_fast_algorithm = true

    ## Analysis w-o panel - give false for iso test and WES/WGS/target research analysis (in meta.json)
    Boolean require_panel = true

    # 1. Prepare gene panel or use user defined
    if(!defined(panel_json) && is_input_for_panel_generate_defined) {
        call panel_generate_task.panel_generate {
            input:
                sample_id = sample_id,
                hpo_terms = hpo_terms,
                genes = genes,
                diseases = diseases,
                phenotypes_description = phenotypes_description,
                panel_names = panel_names
        }
    }

    if(defined(panel_json) && genome_or_exome != "target" && require_panel) {
        call string_gp_to_json_task.string_gp_to_json {
            input:
                panel_json = panel_json,
                hpo_terms = hpo_terms,
                genes = genes,
                diseases = diseases,
                phenotypes_description = phenotypes_description,
                panel_names = panel_names,
                sample_id = sample_id
        }
    }
    Array[File] is_gene_panel = select_all([panel_generate.panel, string_gp_to_json.validated_panel_json])
    if (length(is_gene_panel) > 0) {
        File panel_ready_or_created = select_first([panel_generate.panel, string_gp_to_json.validated_panel_json])
        File panel_inputs_ready_or_created = select_first([panel_generate.inputs, string_gp_to_json.validated_panel_inputs_json])
        File all_panels_json_ready_or_created = select_first([panel_generate.all_panels, string_gp_to_json.all_panels])
    }

    if (genome_or_exome != "target") {
        # 2a. Prepare interval_list for WES/WGS analysis
        call resources_kit_task.resources_kit {
            input:
                kit = kit_choice,
                reference_genome = reference_genome
        }

        if(kit_choice != "genome") {
            File? bait_file = resources_kit.bait[0]
            File? target_file = resources_kit.target[0]

        }
        File wes_wgs_interval_list = resources_kit.interval_list[0]

    }

    # 2b. Prepare interval list for targeted analysis
    if (genome_or_exome == "target") {
        call bed_to_interval_list_task.bed_to_interval_list {
            input:
                bed = target_bed,
                gene_list = target_genes,
                reference_genome = reference_genome,
                padding = padding,
                sample_id = sample_id
        }
        File target_nuclear_interval_list = bed_to_interval_list.nuclear_interval_file
        File target_mt_interval_list = bed_to_interval_list.mt_interval_file
        File target_interval_list = bed_to_interval_list.custom_interval_file
        File genes_bed_target_json = bed_to_interval_list.genes_bed_target_json
        Boolean mt_interval_list_not_empty = bed_to_interval_list.non_empty_mt_intervals
        Boolean nuclear_interval_list_not_empty = bed_to_interval_list.non_empty_nuclear_intervals
    }

    File interval_file = select_first([target_nuclear_interval_list, wes_wgs_interval_list])

    # 3. Organise fastq files
    call fq_organize_task.fq_organize {
        input:
            fastqs = fastqs,
            paired = true
    }

    # 4. Check quality of fastq files
    if(run_fq_qc) {
        call fq_qc_module.fq_qc {
            input:
                sample_id = sample_id,
                sample_info_json = sample_info_json,
                fastqs = fastqs
        }
    }

    # 5. Align reads
    if (aligner_tool == "bwa-mem"){
        call fq_bwa_align_module.fq_bwa_align {
            input:
                fastqs_left = fq_organize.fastqs_1,
                fastqs_right = fq_organize.fastqs_2,
                sample_id = sample_id,
                reference_genome = reference_genome,
                add_bam_filter = add_bam_filter,
                filter_choice = filter_choice,
                mismatch_threshold = mismatch_threshold,
                mark_dup = mark_dup,
                bam_concat_name = "variant-calling-ready",
                run_bsqr = run_bsqr
            }
    }
    if (aligner_tool == "dragmap"){
        call fq_dragmap_align_module.fq_dragmap_align {
            input:
                fastqs_left = fq_organize.fastqs_1,
                fastqs_right = fq_organize.fastqs_2,
                sample_id = sample_id,
                reference_genome = reference_genome,
                add_bam_filter = add_bam_filter,
                filter_choice = filter_choice,
                mismatch_threshold = mismatch_threshold,
                mark_dup = mark_dup,
                bam_concat_name = "variant-calling-ready"
        }
    }

            
    File recalibrated_markdup_bam = select_first([fq_bwa_align.recalibrated_markdup_bam, fq_dragmap_align.recalibrated_markdup_bam])
    File recalibrated_markdup_bai = select_first([fq_bwa_align.recalibrated_markdup_bai, fq_dragmap_align.recalibrated_markdup_bai])

    # 6. Call variants
    if((genome_or_exome != "target") || (genome_or_exome == "target" && nuclear_interval_list_not_empty)) {
        if (var_call_tool == "haplotypeCaller") {
            call bam_varcalling_module.bam_varcalling {
                input:
                    input_bam = recalibrated_markdup_bam,
                    input_bai = recalibrated_markdup_bai,
                    dragstr_model = fq_dragmap_align.dragstr_model,
                    sample_id = sample_id,
                    interval_list = interval_file,
                    max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file,
                    haplotype_caller_java_mem = java_mem_options,
                    bam_concat_name = "variants-sites-only",
                    contamination = contamination,
                    ignore_soft_clips = ignore_soft_clips,
                    reference_genome = reference_genome
                }
            }
        if (var_call_tool == "deepVariant") {
            call bam_varcalling_dv_module.bam_varcalling_dv {
                input:
                    input_bam = recalibrated_markdup_bam,
                    input_bai = recalibrated_markdup_bai,
                    interval_list = interval_file,
                    max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file,
                    sample_id = sample_id,
                    reference_genome = reference_genome
                }
            }
        
        File? varcalling_vcf_gz = select_first([bam_varcalling.vcf_gz, bam_varcalling_dv.vcf_gz])
        File? varcalling_vcf_gz_tbi = select_first([bam_varcalling.vcf_gz_tbi, bam_varcalling_dv.vcf_gz_tbi])
    
        File? varcalling_gvcf_gz = select_first([bam_varcalling.gvcf_gz, bam_varcalling_dv.gvcf_gz])
        File? varcalling_gvcf_gz_tbi = select_first([bam_varcalling.gvcf_gz_tbi, bam_varcalling_dv.gvcf_gz_tbi])
    }

    # 7. Filter variants
    if((run_vcf_var_filter && genome_or_exome != "target") || (run_vcf_var_filter && genome_or_exome == "target" && nuclear_interval_list_not_empty)) {
        call vcf_var_filter_module.vcf_var_filter {
            input:
                vcf_gz = varcalling_vcf_gz,
                vcf_gz_tbi = varcalling_vcf_gz_tbi,
                sample_id = sample_id,
                interval_list = interval_file,
                apply_ad_filter = apply_ad_filter,
                ad_binom_threshold = ad_binom_threshold,
                apply_vqsr = apply_vqsr,
                analysis_type = genome_or_exome,
                aligner_tool = aligner_tool,
                var_call_tool = var_call_tool,
                quality_threshold = quality_threshold
        }
    }


    # 8. Call mitochondrial variants

    if ((run_mt_varcall && genome_or_exome != "target") || (run_mt_varcall && genome_or_exome == "target" && mt_interval_list_not_empty)) {
        call mt_varcall_module.mt_varcall {
            input:
                bam = recalibrated_markdup_bam,
                bai = recalibrated_markdup_bai,
                mt_intervals = target_mt_interval_list,
                genome_or_exome = genome_or_exome,
                sample_id = sample_id,
                max_alt_allele_count = max_alt_allele_count,
                vaf_filter_threshold = vaf_filter_threshold,
                f_score_beta = f_score_beta,
                min_alt_reads = min_alt_reads,
                hc_vcf_gz = vcf_var_filter.filtered_vcf_gz,
                hc_bamout = bam_varcalling.haplotype_caller_bam,
                bam_concat_name = "variants-sites-only"
        }
    }

    # 9. Annotate and filter variants
    if(run_vcf_anno) {
        call vcf_anno_module.vcf_anno {
            input:
                vcf_gz = select_first([mt_varcall.allcontigs_vcf_gz, vcf_var_filter.filtered_vcf_gz, mt_varcall.mt_filtered_vcf_gz]),
                vcf_gz_tbi = select_first([mt_varcall.allcontigs_vcf_gz_tbi, vcf_var_filter.filtered_vcf_gz_tbi, mt_varcall.mt_filtered_vcf_gz_tbi]),
                vcf_anno_freq_genome_or_exome = freq_and_coverage_source,
                gnomad_coverage_genome_or_exome = freq_and_coverage_source,
                vcf_basename = sample_id,
                carrier_screening = carrier_screening,
                snpeff_java_mem = snpeff_java_mem,
                splicing_threshold = splicing_threshold,
                impact_threshold = impact_threshold,
                threshold_frequency = threshold_frequency,
                quality_threshold_multiallelic = quality_threshold_multiallelic,
                ignore_mult_stop_warning = ignore_mult_stop_warning,
                evidence_level_threshold = evidence_level_threshold,
                evidence_rating_threshold = evidence_rating_threshold 

        }
    }

    if (var_call_tool == "haplotypeCaller"){
        File caller_bamout = select_first([mt_varcall.allcontigs_bamout, bam_varcalling.haplotype_caller_bam, mt_varcall.mutect2_mt_bam])
        File caller_bamout_bai = select_first([mt_varcall.allcontigs_bamout_bai, bam_varcalling.haplotype_caller_bai, mt_varcall.mutect2_mt_bai])
    }

    # 10. Annotate variants with ACMG
     if(run_anno_acmg) {
        call vcf_anno_acmg_module.vcf_anno_acmg {
            input:
                panel_json = panel_ready_or_created,
                vcf_gz = vcf_anno.annotated_and_filtered_vcf,
                vcf_gz_tbi = vcf_anno.annotated_and_filtered_vcf_tbi,
                sample_id = sample_id,
                apply_panel_filter = apply_panel_filter,
                split_ISEQ_REPORT_ANN_field = true
        }
    }

    # 11. Create ACMG report
    if(run_vcf_acmg_report) {
        call vcf_acmg_report_module.vcf_acmg_report {
            input:
                vcf_gz = vcf_anno_acmg.annotated_acmg_vcf_gz,
                vcf_gz_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi,
                cnv_json = sv_calling.filtered_json,
                acmg_threshold = acmg_threshold,
                clinvar_threshold = clinvar_threshold,
                panel_json = panel_ready_or_created,
                panel_inputs_json = panel_inputs_ready_or_created,
                genes_bed_target_json = genes_bed_target_json,
                sample_info_json = sample_info_json,
                timezoneDifference = timezoneDifference,
                bams = select_all([recalibrated_markdup_bam, caller_bamout]),
                sample_id = sample_id,
                genome_or_exome = genome_or_exome,
                kit = kit,
                analysis_start = analysis_start,
                analysis_group = analysis_group,
                analysisDescription = analysisDescription,
                analysisName = analysisName,
                analysisWorkflow = analysisWorkflow,
                analysisWorkflowVersion = analysisWorkflowVersion,
                analysisEnvironment = analysisEnvironment,
                gene_panel_logs = panel_generate.gene_panel_logs,
                impact_threshold = impact_threshold,
                threshold_frequency = threshold_frequency,
                quality_threshold = quality_threshold,
                var_call_tool = var_call_tool,
                aligner_tool = aligner_tool
        }
    }

    # 12. Call structural variants
    if (add_snp_data_to_sv) {
        File? vcf_to_sv = vcf_var_filter.filtered_vcf_gz
        File? vcf_tbi_to_sv = vcf_var_filter.filtered_vcf_gz_tbi
    }

    if(run_sv_calling) {
        call sv_calling_module.sv_calling {
            input:
                del_cov_max = del_cov_max,
                dup_cov_min = dup_cov_min,
                create_pictures = create_pictures,
                create_coverage_plots = create_coverage_plots,
                exclude_lcr = exclude_lcr,
                exclude_chroms = exclude_chroms,
                bam = recalibrated_markdup_bam,
                bai = recalibrated_markdup_bai,
                vcf_gz = vcf_to_sv,
                vcf_gz_tbi = vcf_tbi_to_sv,
                gene_panel = panel_ready_or_created,
                all_gene_panel = all_panels_json_ready_or_created,
                panel_threshold1 = panel_threshold1,
                panel_threshold2 = panel_threshold2,
                apply_rank_filter = apply_rank_filter,
                rank_threshold = rank_threshold,
                apply_panel_filter = apply_panel_filter_sv,
                include_ci =include_ci,
                sample_id = sample_id,
                reference_genome = reference_genome,
                ref_counts = ref_counts,
                custom_bed = custom_bed, 
                kit = kit,
                bf_threshold = bf_threshold,
                genome_or_exome = genome_or_exome
        }
    }

    # 13. Estimate detection chance
    Array[File] is_gvcf = select_all([bam_varcalling.gvcf_gz])
    if (run_detection_chance && length(is_gvcf) != 0 && var_call_tool == "haplotypeCaller") {
        call detection_chance_module.detection_chance {
            input:
                sample_id = sample_id,
                sample_gvcf_gz = varcalling_gvcf_gz,
                sample_gvcf_gz_tbi = varcalling_gvcf_gz_tbi,
                timezoneDifference = timezoneDifference,
                analysisEnvironment = analysisEnvironment,
                panel_json = panel_ready_or_created,
                bam = recalibrated_markdup_bam,
                bai = recalibrated_markdup_bai,
                bed = target_bed
        }
    }

    # 14. Check quality of bam files
    if (run_bam_qc) {
        call bam_qc_module.bam_qc {
            input:
                sample_id = sample_id,
                bam = recalibrated_markdup_bam,
                bai = recalibrated_markdup_bai,
                interval_list = interval_file,
                custom_interval_list = target_interval_list,
                target = target_file,
                bait = bait_file,
                kit = kit_choice,
                reference_genome = reference_genome,
                dupl_metrics = fq_bwa_align.duplicates_metrics,
                dupl_metrics_empty = fq_bwa_align.empty_metrics,
                use_fast_algorithm = use_fast_algorithm,
                sample_info_json = sample_info_json,
                report_title = "Bam_quality_report",
                bam_metrics_java_options = java_mem_options,
                bam_name = "variant-calling-ready"
        }
    }

    # 15. Genetic sex verification
    if (run_sex_check) {
        call sex_check_module.sex_check {
            input:
                bam = recalibrated_markdup_bam,
                bai = recalibrated_markdup_bai,
                vcf_gz = varcalling_vcf_gz,
                vcf_gz_tbi = varcalling_vcf_gz_tbi,
                sample_id = sample_id,
                timezoneDifference = timezoneDifference,
                analysisEnvironment = analysisEnvironment
        }
    }

    # 16. Merge BCO and prepare report pdf
    Array[File] bcos_module = select_all([panel_generate.bco, string_gp_to_json.bco, resources_kit.bco, bed_to_interval_list.bco, fq_organize.bco, fq_qc.bco, fq_bwa_align.bco, fq_dragmap_align.bco,
                                            bam_varcalling.bco, bam_varcalling_dv.bco, vcf_var_filter.bco, vcf_anno.bco, vcf_anno_acmg.bco, vcf_acmg_report.bco, sv_calling.bco,
                                            sex_check.bco, detection_chance.bco, bam_qc.bco])
    Array[File] stdout_module = select_all([panel_generate.stdout_log,  string_gp_to_json.stdout_log, resources_kit.stdout_log, bed_to_interval_list.stdout_log,  fq_organize.stdout_log,
                                            fq_qc.stdout_log, fq_bwa_align.stdout_log, fq_dragmap_align.stdout_log, bam_varcalling.stdout_log, bam_varcalling_dv.stdout_log, vcf_var_filter.stdout_log,
                                            vcf_anno.stdout_log, vcf_anno_acmg.stdout_log, vcf_acmg_report.stdout_log, sv_calling.stdout_log,
                                            sex_check.stdout_log, detection_chance.stdout_log, bam_qc.stdout_log ])
    Array[File] stderr_module = select_all([panel_generate.stderr_log,  string_gp_to_json.stderr_log, resources_kit.stderr_log, bed_to_interval_list.stderr_log, fq_organize.stderr_log,
                                            fq_qc.stderr_log, fq_bwa_align.stderr_log, fq_dragmap_align.stderr_log,  bam_varcalling.stderr_log, bam_varcalling_dv.stderr_log, vcf_var_filter.stderr_log,
                                            vcf_anno.stderr_log, vcf_anno_acmg.stderr_log, vcf_acmg_report.stderr_log, sv_calling.stderr_log,
                                            sex_check.stderr_log, detection_chance.stderr_log, bam_qc.stderr_log])

    call bco_module.bco as merge_bco {
        input:
            bco_array = bcos_module,
            stdout_array = stdout_module,
            stderr_array = stderr_module,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = sample_id,
            sample_info_json = sample_info_json,
            timezoneDifference = timezoneDifference
    }

    output {


        ## FINAL REPORTS
        File? ang_pdf_report = vcf_acmg_report.ang_pdf_report
        File? ang_docx_report = vcf_acmg_report.ang_docx_report


        ## QUALITY CHECK REPORTS AND OTHER FILES
        # fastq
        Array[File]? fastqc_zips = fq_qc.fastqc_zips
        File? multiqc_report = fq_qc.multiqc_report
        File? multiqc_data_json = fq_qc.multiqc_data_json

        # bam
        File? bam_qc_data_json = bam_qc.data_json
        File? bam_qc_report_html = bam_qc.report_html
        File? bam_qc_general_stats_txt = bam_qc.general_stats_txt
        File? bam_qc_sources = bam_qc.multiqc_sources

        # detection chance
        File? detection_chance_report_pdf = detection_chance.detection_chance_report_pdf
        File? detection_chance_report_docx = detection_chance.detection_chance_report_docx

        # genetic sex
        File? sex_check_report_docx = sex_check.report_ang_docx
        File? sex_check_report_pdf = sex_check.report_ang_pdf


        ## ALIGNMENT RESULTS
        File? final_bam = recalibrated_markdup_bam
        File? final_bai = recalibrated_markdup_bai


        ## SNPs AND INDELs VARIANT CALLING FILES
        # gvcf
        File? gvcf_gz = varcalling_gvcf_gz
        File? gvcf_gz_tbi = varcalling_gvcf_gz_tbi

        # genotyped and filtered vcf
        File? vcf_gz = select_first([mt_varcall.allcontigs_vcf_gz, vcf_var_filter.filtered_vcf_gz, mt_varcall.mt_filtered_vcf_gz])
        File? vcf_gz_tbi = select_first([mt_varcall.allcontigs_vcf_gz_tbi, vcf_var_filter.filtered_vcf_gz_tbi, mt_varcall.mt_filtered_vcf_gz_tbi])

        # caller bamouts
        File? final_realigned_bam = caller_bamout
        File? final_realigned_bai = caller_bamout_bai


        ## REJECTED VARIANTS (SNPs and INDELs)
        # autosomal
        Array[File]?  rejected_variants_vcfs = vcf_var_filter.rejected_variants_vcfs
        Array[File]?  rejected_variants_tbis = vcf_var_filter.rejected_variants_tbis
        File? detail_metrics_file = vcf_var_filter.detail_metrics_file
        File? summary_metrics_file = vcf_var_filter.summary_metrics_file

        # mitochondrial
        File? mt_rejected_vcf_gz = mt_varcall.mt_rejected_vcf_gz
        File? mt_rejected_vcf_gz_tbi = mt_varcall.mt_rejected_vcf_gz_tbi
        File? mt_contamination_report = mt_varcall.contamination_report


        ## ANNOTATED VCFs and TSV
        # annotation module
        File? annotated_vcf = vcf_anno.annotated_and_filtered_vcf
        File? annotated_vcf_tbi = vcf_anno.annotated_and_filtered_vcf_tbi
        File? tsv_from_vcf = vcf_anno.tsv_from_vcf

        # acmg
        File? annotated_acmg_vcf_gz = vcf_anno_acmg.annotated_acmg_vcf_gz
        File? annotated_acmg_vcf_gz_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi
        File? igv_screenshots_tar_gz = vcf_acmg_report.igv_screenshots_tar_gz
        File? igv_html = vcf_acmg_report.jigv_html
        File? annotated_acmg_tsv = vcf_anno_acmg.tsv_report
        File? annotated_acmg_json = vcf_acmg_report.json_report

        # gene panel logs
        File? gene_panel_logs = panel_generate.gene_panel_logs


        ## SV CALLING
        # vcf
        File? sv_vcf = sv_calling.sv_vcf
        File? sv_vcf_tbi = sv_calling.sv_vcf_tbi
        File? annotated_sv_vcf_gz = sv_calling.annotated_sv_vcf_gz
        File? annotated_sv_vcf_gz_tbi = sv_calling.annotated_sv_vcf_gz_tbi
        #Float? ref_to_sample_fit = sv_calling.ref_to_sample_fit ## useful for raport
        #File? annotated_filtered_sv_vcf_gz = sv_calling.annotated_filtered_sv_vcf_gz ## for report
        #File? annotated_filtered_sv_vcf_gz_tbi = sv_calling.annotated_filtered_sv_vcf_gz_tbi ## for report

        # reports
        File? sv_tsv = sv_calling.tsv_full_report
        File? sv_igv_pngs = sv_calling.igv_pngs
        File? sv_coverage_plots = sv_calling.coverage_plots
        File? sv_report_pdf = sv_calling.sv_report_pdf
        File? sv_report_html = sv_calling.sv_report_html
        #File? filtered_json = sv_calling.filtered_json ## for report


        ## BCO
        #bco, stdout, stderr
        File? bco = merge_bco.bco_merged
        File? stdout_log = merge_bco.stdout_log
        File? stderr_log = merge_bco.stderr_log

        #bco report (pdf, docx, html)
        File? bco_report_pdf = merge_bco.bco_report_pdf
        File? bco_report_docx = merge_bco.bco_report_docx

        #bco table (csv)
        File? bco_table_csv = merge_bco.bco_table_csv



    }
}
