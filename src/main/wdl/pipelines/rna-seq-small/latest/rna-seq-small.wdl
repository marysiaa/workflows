import "https://gitlab.com/intelliseq/workflows/-/raw/fq-organize@2.2.2/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-custom-bwa@1.0.0/src/main/wdl/tasks/rna-seq-custom-bwa/rna-seq-custom-bwa.wdl" as rna_seq_custom_bwa_task
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-ensembl-bwa@1.0.3/src/main/wdl/tasks/rna-seq-ensembl-bwa/rna-seq-ensembl-bwa.wdl" as rna_seq_ensembl_bwa_task
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-bwa@1.0.3/src/main/wdl/tasks/rna-seq-bwa/rna-seq-bwa.wdl" as rna_seq_bwa_task
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-count@1.0.3/src/main/wdl/tasks/rna-seq-count/rna-seq-count.wdl" as rna_seq_count_task
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-count-concat@1.0.2/src/main/wdl/tasks/rna-seq-count-concat/rna-seq-count-concat.wdl" as rna_seq_count_concat_task
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-align-stats-bwa@1.0.0/src/main/wdl/tasks/rna-seq-align-stats-bwa/rna-seq-align-stats-bwa.wdl" as rna_seq_align_stats_bwa_task
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow rna_seq_small {

    Array[File]? fastqs
    Boolean is_fastqs_defined = defined(fastqs)
    Array[File]? fastqs_left # use it when you do not want call fq_organize (NOTE must be used with samples_names)
    Array[String]? samples_names # use it when you do not want call fq_organize (NOTE must be used with fastqs_left)
    File? ref_genome # use it when you have downloaded reference genome earlier (NOTE must be used with gtf)
    File? gtf # use it when you have downloaded gtf earlier (NOTE must be used with ref_genome)
    Boolean is_ref_genome_defined = defined(ref_genome)
    String organism_name = "Homo sapiens"
    String release_version = "104" # ensembl version
    String analysis_id = "no_id_provided"
    String genome_basename = sub(organism_name, " ", "_") + "_genome"
    String chromosome_name = "primary_assembly"
    Boolean run_bco = true
    String pipeline_name = "rna_seq_small"
    String pipeline_version = "1.11.6"

    if(is_fastqs_defined) {
        call fq_organize.fq_organize {
            input:
                fastqs = fastqs,
                paired = false,
                split_files = false
        }
    }
    Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
    Array[String] samples_ids = select_first([fq_organize.samples_ids, samples_names])

    if(is_ref_genome_defined) {
        call rna_seq_custom_bwa_task.rna_seq_custom_bwa {
            input:
                ref_genome = ref_genome,
                genome_basename = genome_basename
        }
    }

    if(!is_ref_genome_defined) {
        call rna_seq_ensembl_bwa_task.rna_seq_ensembl_bwa {
            input:
                release_version = release_version,
                chromosome_name = chromosome_name,
                organism_name = organism_name,
                genome_basename = genome_basename
        }
    }

    Array[File] ref_genome_index = select_first([rna_seq_custom_bwa.ref_genome_index, rna_seq_ensembl_bwa.ref_genome_index])
    File gtf_file = select_first([gtf, rna_seq_ensembl_bwa.gtf_file])

    scatter (index in range(length(fastqs_1))) {
        call rna_seq_bwa_task.rna_seq_bwa {
            input:
                fastq_1 = fastqs_1[index],
                sample_id = samples_ids[index],
                ref_genome_index = ref_genome_index,
                genome_basename = genome_basename,
                index = index
        }
    }

    call rna_seq_align_stats_bwa_task.rna_seq_align_stats_bwa {
        input:
            bwa_align_stats = rna_seq_bwa.stats,
            analysis_id = analysis_id
    }

    scatter (index in range(length(fastqs_1))) {
        call rna_seq_count_task.rna_seq_count {
            input:
                gtf_file = gtf_file,
                bam_file = rna_seq_bwa.bam_file[index],
                bam_bai_file = rna_seq_bwa.bam_bai_file[index],
                sample_id = samples_ids[index],
                index = index
        }
    }

    call rna_seq_count_concat_task.rna_seq_count_concat {
        input:
            count_reads = rna_seq_count.count_reads,
            analysis_id = analysis_id
    }

    #Merge BCO and prepare report pdf
    if (run_bco) {
        File indexing_genome_bco = select_first([rna_seq_ensembl_bwa.bco, rna_seq_custom_bwa.bco])
        File indexing_genome_stdout = select_first([rna_seq_ensembl_bwa.stdout_log, rna_seq_custom_bwa.stdout_log])
        File indexing_genome_stderr = select_first([rna_seq_ensembl_bwa.stderr_log, rna_seq_custom_bwa.stderr_log])

        Array[File] bco_tasks = select_all([fq_organize.bco, indexing_genome_bco, rna_seq_align_stats_bwa.bco, rna_seq_count_concat.bco])
        Array[File] stdout_tasks = select_all([fq_organize.stdout_log, indexing_genome_stdout, rna_seq_align_stats_bwa.stdout_log,  rna_seq_count_concat.stdout_log])
        Array[File] stderr_tasks = select_all([fq_organize.stderr_log, indexing_genome_stderr, rna_seq_align_stats_bwa.stderr_log, rna_seq_count_concat.stderr_log])

        Array[Array[File]] bco_scatters = [bco_tasks, rna_seq_bwa.bco, rna_seq_count.bco]
        Array[Array[File]] stdout_scatters = [stdout_tasks, rna_seq_bwa.stdout_log, rna_seq_count.stdout_log]
        Array[Array[File]] stderr_scatters = [stderr_tasks, rna_seq_bwa.stderr_log, rna_seq_count.stderr_log]

        Array[File] bco_array = flatten(bco_scatters)
        Array[File] stdout_array = flatten(stdout_scatters)
        Array[File] stderr_array = flatten(stderr_scatters)

        call bco_module.bco {
            input:
                bco_array = bco_array,
                stdout_array = stdout_array,
                stderr_array = stderr_array,
                is_rna_seq = true,
                pipeline_name = pipeline_name,
                pipeline_version = pipeline_version,
                sample_id = analysis_id
        }
    }

    output {

        Array[File] bams = rna_seq_bwa.bam_file
        Array[File] bams_bais = rna_seq_bwa.bam_bai_file
        File stats_concat_excel_file = rna_seq_align_stats_bwa.stats_concat_excel_file
        File stats_concat_tsv_file = rna_seq_align_stats_bwa.stats_concat_tsv_file
        File count_concat_excel_file = rna_seq_count_concat.count_concat_excel_file
        File count_concat_tsv_file = rna_seq_count_concat.count_concat_tsv_file

        #bco, stdout, stderr
        File? bco = bco.bco_merged
        File? stdout_log = bco.stdout_log
        File? stderr_log = bco.stderr_log

        #bco report (pdf, docx, html)
        File? bco_report_pdf = bco.bco_report_pdf
        File? bco_report_docx = bco.bco_report_docx

        #bco table (csv)
        File? bco_table_csv = bco.bco_table_csv
    }
}
