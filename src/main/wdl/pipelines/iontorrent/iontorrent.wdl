import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.8/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-qc@1.6.4/src/main/wdl/modules/fq-qc/fq-qc.wdl" as fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.1/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-qc@3.0.8/src/main/wdl/modules/bam-qc/bam-qc.wdl" as bam_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling-m2@1.1.1/src/main/wdl/modules/bam-varcalling-m2/bam-varcalling-m2.wdl" as bam_varcalling_m2_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno@1.17.0/src/main/wdl/modules/vcf-anno/vcf-anno.wdl" as vcf_anno_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-acmg@1.6.0/src/main/wdl/modules/vcf-anno-acmg/vcf-anno-acmg.wdl" as vcf_anno_acmg_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-report@3.2.9/src/main/wdl/modules/vcf-acmg-report/vcf-acmg-report.wdl" as vcf_acmg_report_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow iontorrent {

    String pipeline_name = "iontorrent"
    String pipeline_version = "1.1.0"
    String sample_id = "no_id_provided"
    String reference_genome = "grch38-no-alt"
    Boolean germline_analysis

    # PATIENT INFORMATION
    File? sample_info_json

    # ALIGNMENT
    # fastq files: paired or single-end, may be compessed, fq-organize inside alignment
    Array[File] fastqs
    Boolean paired = false
    Boolean mark_dup = false
    Boolean run_bsqr = false
    Boolean split_files = true
    String platform = "IONTORRENT"
    String bam_concat_name = "variant-calling-ready"

    ## BAM FILTERING
    Boolean add_bam_filter = true
    ## Which filter should be applied?:
    ## 1 - removing reads with high fraction of mismatches
    ## 2 - removing reads which are both very short and with soft-clipped at both ends
    ## 3 - combining both filter types (1+2)
    ## 4 - 3 plus removal of reads with more than one indel
    String filter_choice = "4"
    ## Set mismatch fraction that is not accepted (for filters 1 and 3)
    Float mismatch_threshold = 0.1
    Boolean remove_chimeras = true

    ## INTERVALS
    File? target_bed
    Array[String]? target_genes
    Int padding = 1000

    ## VARIANT CALLING AND INITIAL FILTERING
    Int max_no_pieces_to_scatter_an_interval_file = 4
    File? custom_pon_file
    File? custom_pon_index
    Boolean genotype_germline_sites = true
    Boolean genotype_pon_sites = true
    String pcr_indel_model = "AGGRESSIVE" ##  NONE, CONSERTATIVE, AGGRESSIVE, HOSTILE
    String m2_germline_args = "--max-alt-allele-count 2 --max-events-in-region 3 --unique-alt-read-count 2 --min-allele-fraction 0.1 --min-reads-per-strand 1"
    String m2_somatic_args = "--max-alt-allele-count 2 --max-events-in-region 3 --unique-alt-read-count 2 --min-allele-fraction 0.05 --min-reads-per-strand 1"
    String? m2_extra_filtering_args = if (germline_analysis) then m2_germline_args else m2_somatic_args
    Float indel_threshold = 0.1
    Boolean filter_on_alignment_artifacts = true
    String realignment_extra_args = "--num-regular-contigs 194 --max-reasonable-fragment-length 2000 --drop-ratio 0.1 --indel-start-tolerance 8"
    ## germline pipeline genotype options
    Float no_variant_thereshold = 0.2
    Float homozygote_threshold = 0.85

    ## FUNCTIONAL FILTERING
    String freq_and_coverage_source = "genome"
    Boolean apply_panel_filter = false
    Boolean ignore_mult_stop_warning = true
    ## anno module filtering
    Float threshold_frequency = 0.05 # works id panel_sequencing set as false
    String impact_threshold = "MODERATE" # works id panel_sequencing set as false

    Float splicing_threshold = 0.6   # for dbscSNV ada and rf scores
    Float quality_threshold_multiallelic = 0 # for muttiallelic variants
    ## CIViC evidence thresholds
    String evidence_level_threshold = "C" ## required minimal CIViC evidence level, possible levels A (very well supported), B, C, D, E (not well supported)
    Int evidence_rating_threshold = 3 ## required minimal CIViC rating, from 1 to 5, where 1 is not very reliable and 5 is very reliable 

    ## REPORT OPTIONS
    Int timezoneDifference = 0
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    ## severity threshold for acmg (variants of that severity+ will be selected)
    ## possible values "benign", "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"
    String acmg_threshold = "uncertain"

    ## severity threshold for ClinVar (variants of that severity+ will be selected)
    ## possible values "pathogenic", "likely_pathogenic", "uncertain"
    String clinvar_threshold = "likely_pathogenic"

    ## OTHER OPTIONS
     # Java options for bam_metrics and bam_gatk_hc
    String java_mem_options =  "-Xms4g -Xmx63g" # use "-Xms4g -Xmx8g" on anakin
    # Java options fo snpeff
    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin
    ## Bam WGS metrics algorithm (fast vs normal)
    Boolean use_fast_algorithm = true

    # 1. PREPARING INTERVALS
    call bed_to_interval_list_task.bed_to_interval_list {
        input:
            bed = target_bed,
            gene_list = target_genes,
            reference_genome = reference_genome,
            padding = padding,
            sample_id = sample_id
    }
    File target_interval_list = bed_to_interval_list.custom_interval_file
    File genes_bed_target_json = bed_to_interval_list.genes_bed_target_json
    Boolean mt_interval_list_not_empty = bed_to_interval_list.non_empty_mt_intervals
    Boolean nuclear_interval_list_not_empty = bed_to_interval_list.non_empty_nuclear_intervals

    # 2. ALIGNMENT
    call fq_bwa_align_module.fq_bwa_align {
        input:
            fastqs = fastqs,
            sample_id = sample_id,
            reference_genome = reference_genome,
            add_bam_filter = add_bam_filter,
            filter_choice = filter_choice,
            mismatch_threshold = mismatch_threshold,
            mark_dup = mark_dup,
            paired = paired,
            run_bsqr = run_bsqr,
            split_files = split_files,
            remove_chimeras = remove_chimeras,
            bam_concat_name = "variant-calling-ready",
            platform = platform
   }

    if(nuclear_interval_list_not_empty || mt_interval_list_not_empty) {
        # 3. VARIANT CALLING
        call bam_varcalling_m2_module.bam_varcalling_m2 {
            input:
                tumor_bam = fq_bwa_align.recalibrated_markdup_bam,
                tumor_bai = fq_bwa_align.recalibrated_markdup_bai,
                genotype_germline_sites = genotype_germline_sites,
                genotype_pon_sites = genotype_pon_sites,
                custom_pon_file = custom_pon_file,
                custom_pon_index = custom_pon_index,
                m2_extra_filtering_args = m2_extra_filtering_args,
                realignment_extra_args = realignment_extra_args,
                sample_id = sample_id,
                interval_list = target_interval_list,
                max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file,
                filter_on_alignment_artifacts = filter_on_alignment_artifacts,
                germline_analysis = germline_analysis,
                no_variant_thereshold = no_variant_thereshold,
                homozygote_threshold = homozygote_threshold,
                indel_threshold = indel_threshold,
                pcr_indel_model = pcr_indel_model
        }
        # 4. ANNOTATION
        call vcf_anno_module.vcf_anno {
            input:
                vcf_gz = bam_varcalling_m2.filtered_m2_vcf,
                vcf_gz_tbi = bam_varcalling_m2.filtered_m2_tbi,
                vcf_anno_freq_genome_or_exome = freq_and_coverage_source,
                gnomad_coverage_genome_or_exome = freq_and_coverage_source,
                vcf_basename = sample_id,
                carrier_screening = false,
                threshold_frequency = threshold_frequency,
                splicing_threshold = splicing_threshold,
                threshold_frequency = threshold_frequency,
                quality_threshold_multiallelic = quality_threshold_multiallelic,
                impact_threshold = impact_threshold,
                ignore_mult_stop_warning = ignore_mult_stop_warning,
                snpeff_java_mem = snpeff_java_mem,
                evidence_level_threshold = evidence_level_threshold,
                evidence_rating_threshold = evidence_rating_threshold 
       }

       # 5. ADD ACMG CLASSIFICATION
       call vcf_anno_acmg_module.vcf_anno_acmg {
            input:
                vcf_gz = vcf_anno.annotated_and_filtered_vcf,
                vcf_gz_tbi = vcf_anno.annotated_and_filtered_vcf_tbi,
                sample_id = sample_id,
                apply_panel_filter = apply_panel_filter,
                split_ISEQ_REPORT_ANN_field = true
       }

       # 6. GENERATE REPORT
       call vcf_acmg_report_module.vcf_acmg_report {
            input:
                vcf_gz = vcf_anno_acmg.annotated_acmg_vcf_gz,
                vcf_gz_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi,
                acmg_threshold = acmg_threshold,
                clinvar_threshold = clinvar_threshold,
                genes_bed_target_json = genes_bed_target_json,
                sample_info_json = sample_info_json,
                timezoneDifference = timezoneDifference,
                bams = [fq_bwa_align.recalibrated_markdup_bam, bam_varcalling_m2.m2_bamout],
                sample_id = sample_id,
                genome_or_exome = "iontorrent",
                analysis_start = "fastq",
                analysis_group = "germline",
                analysisDescription = analysisDescription,
                analysisName = analysisName,
                analysisWorkflow = analysisWorkflow,
                analysisWorkflowVersion = analysisWorkflowVersion,
                var_call_tool = "mutec",
                germline_analysis = germline_analysis
        }
    }
    # 7. CHECK FASTQ QUALITY
     call fq_qc_module.fq_qc {
         input:
             sample_id = sample_id,
             sample_info_json = sample_info_json,
             fastqs = fastqs
     }

    # 8. CHECK BAM QUALITY
    call bam_qc_module.bam_qc {
        input:
            sample_id = sample_id,
            bam = fq_bwa_align.recalibrated_markdup_bam,
            bai = fq_bwa_align.recalibrated_markdup_bai,
            custom_interval_list = target_interval_list,
            kit = "target",
            reference_genome = reference_genome,
            dupl_metrics = fq_bwa_align.duplicates_metrics,
            dupl_metrics_empty = fq_bwa_align.empty_metrics,
            use_fast_algorithm = use_fast_algorithm,
            sample_info_json = sample_info_json,
            report_title = "Bam_quality_report",
            bam_metrics_java_options = java_mem_options
    }

    # 9. RUN BCO
    Array[File] bcos_module = select_all([ bed_to_interval_list.bco, fq_qc.bco, fq_bwa_align.bco,
                                          bam_varcalling_m2.bco, vcf_anno.bco, vcf_anno_acmg.bco, vcf_acmg_report.bco,
                                          bam_qc.bco])
    Array[File] stdout_module = select_all([bed_to_interval_list.stdout_log,
                                           fq_qc.stdout_log, fq_bwa_align.stdout_log, bam_varcalling_m2.stdout_log,
                                           vcf_anno.stdout_log, vcf_anno_acmg.stdout_log, vcf_acmg_report.stdout_log,
                                           bam_qc.stdout_log ])
    Array[File] stderr_module = select_all([bed_to_interval_list.stderr_log,
                                           fq_qc.stderr_log, fq_bwa_align.stderr_log,  bam_varcalling_m2.stderr_log,
                                           vcf_anno.stderr_log, vcf_anno_acmg.stderr_log, vcf_acmg_report.stderr_log,
                                           bam_qc.stderr_log])
    call bco_module.bco as merge_bco {
        input:
            bco_array = bcos_module,
            stdout_array = stdout_module,
            stderr_array = stderr_module,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = sample_id,
            sample_info_json = sample_info_json,
            timezoneDifference = timezoneDifference
    }

    output {
        ## FINAL REPORTS
        File? ang_pdf_report = vcf_acmg_report.ang_pdf_report
        File? ang_docx_report = vcf_acmg_report.ang_docx_report


        ## QUALITY CHECK REPORTS AND OTHER FILES
        # fastq
        Array[File] fastqc_zips = fq_qc.fastqc_zips
        File multiqc_report = fq_qc.multiqc_report
        File multiqc_data_json = fq_qc.multiqc_data_json

        # bam
        File? bam_qc_data_json = bam_qc.data_json
        File? bam_qc_report_html = bam_qc.report_html
        File? bam_qc_general_stats_txt = bam_qc.general_stats_txt
        File? bam_qc_sources = bam_qc.multiqc_sources
        #Array[Float]? chimeric_reads_pecent = fq_bwa_align.chimeric_reads_pecent

        ## ALIGNMENT RESULTS
        File final_bam = fq_bwa_align.recalibrated_markdup_bam
        File final_bai = fq_bwa_align.recalibrated_markdup_bai


        ## SNPs AND INDELs VARIANT CALLING FILES
        # genotyped and filtered vcf
        File? vcf_gz = bam_varcalling_m2.filtered_m2_vcf
        File? vcf_gz_tbi = bam_varcalling_m2.filtered_m2_tbi

        # caller bamouts
        File? realigned_bam = bam_varcalling_m2.m2_bamout
        File? realigned_bai = bam_varcalling_m2.m2_bamout_bai


        ## REJECTED VARIANTS (SNPs and INDELs)
        File?  rejected_variants_vcf = bam_varcalling_m2.rejected_m2_vcf
        File?  rejected_variants_tbi = bam_varcalling_m2.rejected_m2_vcf_tbi


        ## ANNOTATED VCFs and TSV
        # annotation module
        File? annotated_vcf = vcf_anno.annotated_and_filtered_vcf
        File? annotated_vcf_tbi = vcf_anno.annotated_and_filtered_vcf_tbi
        File? tsv_from_vcf = vcf_anno.tsv_from_vcf

        # acmg
        File? annotated_acmg_vcf_gz = vcf_anno_acmg.annotated_acmg_vcf_gz
        File? annotated_acmg_vcf_gz_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi
        File? igv_screenshots_tar_gz = vcf_acmg_report.igv_screenshots_tar_gz
        File? igv_html = vcf_acmg_report.jigv_html
        File? annotated_acmg_tsv = vcf_anno_acmg.tsv_report

        ## BCO
        #bco, stdout, stderr
        File bco = merge_bco.bco_merged
        File stdout_log = merge_bco.stdout_log
        File stderr_log = merge_bco.stderr_log

        #bco report (pdf, docx, html)
        File bco_report_pdf = merge_bco.bco_report_pdf
        File bco_report_docx = merge_bco.bco_report_docx

        #bco table (csv)
        File bco_table_csv = merge_bco.bco_table_csv
    }
}


