import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task
import "https://gitlab.com/intelliseq/workflows/-/raw/minimal-module@2.0.0/src/main/wdl/modules/minimal-module/minimal-module.wdl" as minimal_module_module

workflow minimal_pipeline {

  meta {
    name: 'Germline minimal-pipeline workflow'
    author: 'https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019-2020 Intelliseq'
  }

  String pipeline_name = "minimal_pipeline"
  String pipeline_version = "2.0.0"

  call minimal_module_module.minimal_module


  Array[File] bcos_module = [minimal_module.bco]
  Array[File] stdout_module = [minimal_module.stdout_log]
  Array[File] stderr_module = [minimal_module.stderr_log]

  call bco_merge_task.bco_merge as bco_merge_pipeline {
    input:
        bco_array = bcos_module,
        stdout_array = stdout_module,
        stderr_array = stderr_module,
        pipeline_name = pipeline_name,
        pipeline_version = pipeline_version
  }

  output {

    File bco = bco_merge_pipeline.bco
    File stdout_log = bco_merge_pipeline.stdout_log
    File stderr_log = bco_merge_pipeline.stderr_log

  }
}
