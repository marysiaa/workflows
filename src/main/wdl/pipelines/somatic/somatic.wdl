import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@2.0.2/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.4/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.8/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.4.0/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.1/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-qc@1.6.3/src/main/wdl/modules/fq-qc/fq-qc.wdl" as fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling-m2@1.1.3/src/main/wdl/modules/bam-varcalling-m2/bam-varcalling-m2.wdl" as bam_varcalling_m2_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno@1.17.0/src/main/wdl/modules/vcf-anno/vcf-anno.wdl" as vcf_anno_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-acmg@1.6.0/src/main/wdl/modules/vcf-anno-acmg/vcf-anno-acmg.wdl" as vcf_anno_acmg_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-somatic-report@1.1.2/src/main/wdl/modules/vcf-somatic-report/vcf-somatic-report.wdl" as vcf_somatic_report_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.0.0/src/main/wdl/modules/bco/bco.wdl" as bco_module


workflow somatic {

    String pipeline_name = "somatic"

    String pipeline_version = "1.10.1"

    String sample_id = "no_id_provided"
    String reference_genome = "grch38-no-alt" ## or hg38
    File? sample_info_json

    # Gene panel
    Array[String]? genes

    ## Provide paired fastq files if you would like to start analysis from the alignment step
    Array[File] tumor_fastqs = []
    Array[File] normal_fastqs = []
    Boolean tumor_only = if (length(normal_fastqs) == 0) then true else false
    Boolean start_from_fastq = if (length(tumor_fastqs) != 0) then true else false

    ## Provide bam file(s) if you would like to start analysis from variant calling
    File? tumor_bam
    File? tumor_bai
    File? normal_bam
    File? normal_bai

    ## Check if normal - control files are provided
    Boolean are_normal_fastqs_given = if (length(normal_fastqs) != 0) then true else false
    Boolean is_normal_bam_given = defined(normal_bam)

    ## Resources parameters
    String genome_or_exome = "genome" # Possible values "exome", "genome", "target"

    ## Interface inputs to report
    Int timezoneDifference = 0
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    String analysisEnvironment = "IntelliseqFlow"

    # for WGS or WES
    String kit = "exome-v7" # Possible values "exome-v6", "exome-v7", "exome-v8", "genome", "targeted"
    String kit_choice = if (genome_or_exome == "genome") then "genome" else kit

    # for targeted-sequencing
    File? target_bed
    Array[String]? target_genes
    Int padding = 1000

    ## Bam filtering options (within fq-bwa-align module)
    Boolean add_bam_filter = true
    ## Which filter should be applied?:
    ## 1 - removing reads with high fraction of mismatches
    ## 2 - removing reads which are both very short and with soft-clipped at both ends
    ## 3 - combining both filter types (1+2)
    String filter_choice = "3"
    ## Set mismatch fraction that is not accepted (for filters 1 and 3)
    Float mismatch_threshold = 0.1
    Boolean mark_dup = true

    ## M2 module parameters
    ## Decides whether Mutect2 should analyze also evident germline sites
    Boolean genotype_germline_sites = true
    Boolean ignore_soft_clips = true
    String m2_extra_filtering_args = "--max-alt-allele-count 2 --max-events-in-region 3 --unique-alt-read-count 2 --min-allele-fraction 0.02 --min-reads-per-strand 1"
    Boolean filter_on_alignment_artifacts = true
    String realignment_extra_args = "--num-regular-contigs 194 --max-reasonable-fragment-length 2000 --drop-ratio 0.1 --indel-start-tolerance 8"
    Int max_no_pieces_to_scatter_an_interval_file = 20

    ## Annotation
    # Java options fo snpeff
    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin
    String freq_and_coverage_source = if (genome_or_exome == "exome") then "exome" else "genome"
    Float splicing_threshold = 0.6   # for dbscSNV ada and rf scores
    Float threshold_frequency = 0.05
    String impact_threshold = "MODERATE"
    Float quality_threshold_multiallelic = 0    # for multiallelic variants removal (do not remove any)
    Boolean split_ISEQ_REPORT_ANN_field = false
    Boolean ignore_mult_stop_warning = true
    ## CIViC evidence thresholds
    String evidence_level_threshold = "C" ## required minimal CIViC evidence level, possible levels A (very well supported), B, C, D, E (not well supported)
    Int evidence_rating_threshold = 3 ## required minimal CIViC rating, from 1 to 5, where 1 is not very reliable and 5 is very reliable 

    ## skip filtering on oncogenes for targeted analysis
    Boolean target = if (genome_or_exome == "target") then true else false

    # 1. Prepare gene panel or use user defined
    if(defined(genes)) {
        call panel_generate_task.panel_generate {
            input:
                sample_id = sample_id,
                genes = genes
        }
    }

    ## INTERVALS
    # 1a. Prepare interval_list for WES/WGS analysis
    if (genome_or_exome != "target") {
        call resources_kit_task.resources_kit {
            input:
                kit = kit_choice,
                reference_genome = reference_genome
        }

        if(kit_choice != "genome") {
            File? bait_file = resources_kit.bait[0]
            File? target_file = resources_kit.target[0]

        }
        File wes_wgs_interval_list = resources_kit.interval_list[0]
    }

    # 1b. Prepare interval list for targeted analysis
    if (genome_or_exome == "target") {
        call bed_to_interval_list_task.bed_to_interval_list {
            input:
                bed = target_bed,
                gene_list = target_genes,
                reference_genome = reference_genome,
                padding = padding,
                sample_id = sample_id
        }
        File target_interval_list = bed_to_interval_list.nuclear_interval_file
        File genes_bed_target_json = bed_to_interval_list.genes_bed_target_json
    }

    File interval_file = select_first([target_interval_list, wes_wgs_interval_list])


    ## ALIGNMENT TUMOR
    if(start_from_fastq) {
        # 2a. Organise tumor fastq files
        call fq_organize_task.fq_organize as tumor_fq_organize {
            input:
                fastqs = tumor_fastqs,
                paired = true,
                possible_left_suffixes = ['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz', '1.clean.fq.gz'],
                possible_right_suffixes = ['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz', '2.clean.fq.gz']
        }

        # 3a. Align tumor reads
        call fq_bwa_align_module.fq_bwa_align as tumor_alignment {
            input:
                fastqs_left = tumor_fq_organize.fastqs_1,
                fastqs_right = tumor_fq_organize.fastqs_2,
                sample_id = sample_id + "_tumor",
                reference_genome = reference_genome,
                add_bam_filter = add_bam_filter,
                filter_choice = filter_choice,
                mismatch_threshold = mismatch_threshold,
                mark_dup = mark_dup
        }
    }

    ## ALIGNMENT NORMAL
    if(start_from_fastq && are_normal_fastqs_given) {
        # 2b. Organise tumor fastq files
        call fq_organize_task.fq_organize as normal_fq_organize {
            input:
                fastqs = normal_fastqs,
                paired = true,
                possible_left_suffixes = ['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz', '1.clean.fq.gz'],
                possible_right_suffixes = ['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz', '2.clean.fq.gz']
        }

        # 3b. Align normal reads
        call fq_bwa_align_module.fq_bwa_align as normal_alignment {
            input:
                fastqs_left = normal_fq_organize.fastqs_1,
                fastqs_right = normal_fq_organize.fastqs_2,
                sample_id = sample_id + "_normal",
                reference_genome = reference_genome,
                add_bam_filter = add_bam_filter,
                filter_choice = filter_choice,
                mismatch_threshold = mismatch_threshold,
                mark_dup = mark_dup
        }
    }


    ## VARIANT CALLING AND FILTERING
    File tumor_bam_to_m2 = select_first([tumor_alignment.recalibrated_markdup_bam, tumor_bam])
    File tumor_bai_to_m2 = select_first([tumor_alignment.recalibrated_markdup_bai, tumor_bai])

    if(are_normal_fastqs_given || is_normal_bam_given){
        File normal_bam_to_m2 = select_first([normal_alignment.recalibrated_markdup_bam, normal_bam])
        File normal_bai_to_m2 = select_first([normal_alignment.recalibrated_markdup_bai, normal_bai])
    }

    call bam_varcalling_m2_module.bam_varcalling_m2 {
        input:
            tumor_bam = tumor_bam_to_m2,
            tumor_bai = tumor_bai_to_m2,
            normal_bam = normal_bam_to_m2,
            normal_bai = normal_bai_to_m2,
            genotype_germline_sites = genotype_germline_sites,
            ignore_soft_clips = ignore_soft_clips,
            for_somatic_pipeline = true,
            m2_extra_filtering_args = m2_extra_filtering_args,
            realignment_extra_args = realignment_extra_args,
            sample_id = sample_id,
            interval_list = interval_file,
            max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file,
            filter_on_alignment_artifacts = filter_on_alignment_artifacts
    }

    ## VARIANT ANNOTATION
    call vcf_anno_module.vcf_anno {
        input:
            vcf_gz = bam_varcalling_m2.filtered_m2_vcf,
            vcf_gz_tbi = bam_varcalling_m2.filtered_m2_tbi,
            vcf_anno_freq_genome_or_exome = freq_and_coverage_source,
            gnomad_coverage_genome_or_exome = freq_and_coverage_source,
            vcf_basename = sample_id,
            carrier_screening = false,
            splicing_threshold = splicing_threshold,
            threshold_frequency = threshold_frequency,
            impact_threshold = impact_threshold,
            quality_threshold_multiallelic = quality_threshold_multiallelic,
            ignore_mult_stop_warning = ignore_mult_stop_warning,
            snpeff_java_mem = snpeff_java_mem,
            evidence_level_threshold = evidence_level_threshold,
            evidence_rating_threshold = evidence_rating_threshold 
    }

    ## ADD ACMG ANNOTATIONS
    call vcf_anno_acmg_module.vcf_anno_acmg {
        input:
            vcf_gz = vcf_anno.annotated_and_filtered_vcf,
            vcf_gz_tbi = vcf_anno.annotated_and_filtered_vcf_tbi,
            sample_id = sample_id,
            split_ISEQ_REPORT_ANN_field = split_ISEQ_REPORT_ANN_field,
            panel_json = panel_generate.panel
    }

    ## FQ-QC
    if(start_from_fastq){
        call fq_qc_module.fq_qc {
            input:
                fastqs = flatten([tumor_fastqs, normal_fastqs]),
                sample_info_json = sample_info_json,
                sample_id = sample_id,
                analysis_type = genome_or_exome
        }
    }

    ## report
    call vcf_somatic_report_module.vcf_somatic_report {
        input:
            vcf_gz = vcf_anno_acmg.annotated_acmg_vcf_gz,
            vcf_gz_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi,
            panel_json = panel_generate.panel,
            panel_inputs_json = panel_generate.inputs,
            genes_bed_target_json = genes_bed_target_json,
            tumor_only = tumor_only,
            sample_info_json = sample_info_json,
            timezoneDifference = timezoneDifference,
            bams = select_all([tumor_alignment.recalibrated_markdup_bam, normal_alignment.recalibrated_markdup_bam, bam_varcalling_m2.m2_bamout]),
            sample_id = sample_id,
            genome_or_exome = genome_or_exome,
            kit = kit,
            analysisDescription = analysisDescription,
            analysisName = analysisName,
            analysisWorkflow = analysisWorkflow,
            analysisWorkflowVersion = analysisWorkflowVersion,
            analysisEnvironment = analysisEnvironment,
            gene_panel_logs = panel_generate.gene_panel_logs,
            target = target,
            tumor_sample_name = bam_varcalling_m2.tumor_sample_name
    }

    ## BCO
    Array[File] bcos_module = select_all([resources_kit.bco, bed_to_interval_list.bco, tumor_fq_organize.bco,
                                          tumor_alignment.bco, normal_fq_organize.bco, normal_alignment.bco,
                                          fq_qc.bco, bam_varcalling_m2.bco, vcf_anno.bco, vcf_anno_acmg.bco, vcf_somatic_report.bco])
    Array[File] stdout_module = select_all([resources_kit.stdout_log, bed_to_interval_list.stdout_log,
                                            tumor_fq_organize.stdout_log, tumor_alignment.stdout_log,
                                            normal_fq_organize.stdout_log, normal_alignment.stdout_log,
                                            fq_qc.stdout_log, bam_varcalling_m2.stdout_log, vcf_anno.stdout_log, vcf_anno_acmg.stdout_log, vcf_somatic_report.stdout_log])
    Array[File] stderr_module = select_all([resources_kit.stderr_log, bed_to_interval_list.stderr_log,
                                            tumor_fq_organize.stderr_log, tumor_alignment.stderr_log,
                                            normal_fq_organize.stderr_log, normal_alignment.stderr_log,
                                            fq_qc.stderr_log, bam_varcalling_m2.stderr_log, vcf_anno.stderr_log, vcf_anno_acmg.stderr_log, vcf_somatic_report.stderr_log])

    call bco_module.bco as merge_bco {
        input:
            bco_array = bcos_module,
            stdout_array = stdout_module,
            stderr_array = stderr_module,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = sample_id
    }

    output {

        ## FINAL REPORTS
        File? ang_pdf_report = vcf_somatic_report.ang_pdf_report
        File? ang_docx_report = vcf_somatic_report.ang_docx_report

        ## ALIGNMENT RESULTS
        File? tumor_final_bam = tumor_alignment.recalibrated_markdup_bam
        File? tumor_final_bai = tumor_alignment.recalibrated_markdup_bai
        File? normal_final_bam = normal_alignment.recalibrated_markdup_bam
        File? normal_final_bai = normal_alignment.recalibrated_markdup_bai
        File? tumor_dupl_metrics = tumor_alignment.duplicates_metrics
        File? normal_dupl_metrics = normal_alignment.duplicates_metrics

        ## SNPs AND INDELs VARIANT CALLING FILES
        # genotyped and filtered m2 vcf
        File vcf_gz = bam_varcalling_m2.filtered_m2_vcf
        File vcf_gz_tbi = bam_varcalling_m2.filtered_m2_tbi
        # rejected variants
        File rejected_m2_vcf = bam_varcalling_m2.rejected_m2_vcf
        File rejected_m2_vcf_tbi = bam_varcalling_m2.rejected_m2_vcf_tbi

        # m2 bamouts
        File m2_bamout = bam_varcalling_m2.m2_bamout
        File m2_bamout_bai = bam_varcalling_m2.m2_bamout_bai

        # m2 filter stats
        File m2_filtering_stats = bam_varcalling_m2.m2_filtering_stats

        ## ANNOTATED VCFs, JSON and TSV
        # annotation module
        File annotated_vcf = vcf_anno_acmg.annotated_acmg_vcf_gz
        File annotated_vcf_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi
        File tsv_from_vcf = vcf_anno_acmg.tsv_report
        File json_from_vcf = vcf_anno_acmg.json_report

        ## FQ-QC
        Array[File]? fastqc_zips = fq_qc.fastqc_zips
        File? multiqc_report = fq_qc.multiqc_report
        File? multiqc_data_json = fq_qc.multiqc_data_json

        ## BCO
        #bco, stdout, stderr
        File bco = merge_bco.bco_merged
        File stdout_log = merge_bco.stdout_log
        File stderr_log = merge_bco.stderr_log

        #bco report (pdf, docx, html)
        File bco_report_pdf = merge_bco.bco_report_pdf
        File bco_report_html = merge_bco.bco_report_html

        #bco table (csv)
        File bco_table_csv = merge_bco.bco_table_csv

    }
}