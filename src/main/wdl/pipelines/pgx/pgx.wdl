import "https://gitlab.com/intelliseq/pgx/raw/resources-pgx-polygenic@1.2.0/src/main/wdl/tasks/resources-pgx-polygenic/resources-pgx-polygenic.wdl" as resources_pgx_polygenic_task
import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.8/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.3/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-qc@1.6.6/src/main/wdl/modules/fq-qc/fq-qc.wdl" as fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.0/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-dragmap-align@1.0.5/src/main/wdl/modules/fq-dragmap-align/fq-dragmap-align.wdl" as fq_dragmap_align_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling@1.3.12/src/main/wdl/modules/bam-varcalling/bam-varcalling.wdl" as bam_varcalling_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling-dv@1.0.6/src/main/wdl/modules/bam-varcalling-dv/bam-varcalling-dv.wdl" as bam_varcalling_dv_module
import "https://gitlab.com/intelliseq/workflows/-/raw/gvcf-pgx-genotype@1.1.2/src/main/wdl/modules/gvcf-pgx-genotype/gvcf-pgx-genotype.wdl" as gvcf_pgx_genotype_module
import "https://gitlab.com/intelliseq/workflows/-/raw/pgx-anno@1.0.3/src/main/wdl/modules/pgx-anno/pgx-anno.wdl" as pgx_anno_module
import "https://gitlab.com/intelliseq/workflows/-/raw/pgx-report@1.0.2/src/main/wdl/modules/pgx-report/pgx-report.wdl" as pgx_report_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow pgx {

    String pipeline_name = "pgx"
    String pipeline_version = "2.0.0"

    # Inputs
    Array[File] fastqs
    Array[String]? selected_genes # for example ["CYP2C9", "CYP2C19", "CYP3A4"]

    String sample_id = "no_id_provided" 

    # Report information
    Int timezoneDifference = 0
    Array[String]? drugs # drugs you want to be reported 1st, for example ["Warfarin", "Clopidogrel"]
    File? sample_info_json

    # Variant calling tool
    String var_call_tool = "haplotypeCaller"

    # Alignment tool
    String aligner_tool = "bwa-mem"

    # Java options for bam_metrics and bam_gatk_hc
    String java_mem_options =  "-Xms4g -Xmx63g" # use "-Xms4g -Xmx8g" on anakin

    # run modules/tasks
    Boolean run_phasing = true
    Boolean run_bsqr = if (var_call_tool == "haplotypeCaller") then true else false 

    # 1. Prepare interval_list
    call resources_pgx_polygenic_task.resources_pgx_polygenic {
        input:
            genes_list = selected_genes
    }

    # 2. Organise fastq files
    call fq_organize_task.fq_organize {
        input:
            fastqs = fastqs,
            paired = true,
            possible_left_suffixes = ['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz', '1.clean.fq.gz'],
            possible_right_suffixes = ['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz', '2.clean.fq.gz']
    }

    # 3. Check quality of fastq files
    call fq_qc_module.fq_qc {
        input:
            sample_id = sample_id,
            sample_info_json = sample_info_json,
            fastqs = fastqs
    }

    # 4a. Align reads with bwa-mem
    if (aligner_tool == "bwa-mem"){
        call fq_bwa_align_module.fq_bwa_align {
            input:
                fastqs_left = fq_organize.fastqs_1,
                fastqs_right = fq_organize.fastqs_2,
                sample_id = sample_id,
                bam_concat_name = "variant-calling-ready",
                run_bsqr = run_bsqr
        }
    }

    # 4b. Align reads with dragmap
    if (aligner_tool == "dragmap"){
        call fq_dragmap_align_module.fq_dragmap_align {
            input:
                fastqs_left = fq_organize.fastqs_1,
                fastqs_right = fq_organize.fastqs_2,
                sample_id = sample_id,
                bam_concat_name = "variant-calling-ready"
        }
    }

    File recalibrated_markdup_bam = select_first([fq_bwa_align.recalibrated_markdup_bam, fq_dragmap_align.recalibrated_markdup_bam])
    File recalibrated_markdup_bai = select_first([fq_bwa_align.recalibrated_markdup_bai, fq_dragmap_align.recalibrated_markdup_bai])

    # 5a. Call variants with HaplotypeCaller
    if (var_call_tool == "haplotypeCaller") {
        call bam_varcalling_module.bam_varcalling {
            input:
                input_bam = recalibrated_markdup_bam,
                input_bai = recalibrated_markdup_bai,
                dragstr_model = fq_dragmap_align.dragstr_model,
                sample_id = sample_id,
                interval_list = resources_pgx_polygenic.pgx_polygenic_interval_list,
                haplotype_caller_java_mem = java_mem_options,
                bam_concat_name = "variants-sites-only",
                remove_tags = true
        }
    }
    # 5b. Call variants with deepVariant
    if (var_call_tool == "deepVariant") {
        call bam_varcalling_dv_module.bam_varcalling_dv {
            input:
                input_bam = recalibrated_markdup_bam,
                input_bai = recalibrated_markdup_bai,
                interval_list = resources_pgx_polygenic.pgx_polygenic_interval_list,
                sample_id = sample_id,
                filter_pass = false,
                is_pgx = true
        }
    }

    File? varcalling_vcf_gz = select_first([bam_varcalling.vcf_gz, bam_varcalling_dv.vcf_gz])
    File? varcalling_vcf_gz_tbi = select_first([bam_varcalling.vcf_gz_tbi, bam_varcalling_dv.vcf_gz_tbi])

    File? varcalling_gvcf_gz = select_first([bam_varcalling.gvcf_gz, bam_varcalling_dv.gvcf_gz])
    File? varcalling_gvcf_gz_tbi = select_first([bam_varcalling.gvcf_gz_tbi, bam_varcalling_dv.gvcf_gz_tbi])

    # 6. Genotyping
    ## TODO change import version when ready and tagged
    call gvcf_pgx_genotype_module.gvcf_pgx_genotype {
        input:
            gvcf_gz = varcalling_gvcf_gz,
            gvcf_gz_tbi = varcalling_gvcf_gz_tbi,
            vcf_gz = varcalling_vcf_gz,
            vcf_gz_tbi = varcalling_vcf_gz_tbi,
            sample_id = sample_id,
            var_call_tool = var_call_tool,
            run_phasing = run_phasing
    }

    # 7. Create recommendations 
    call pgx_anno_module.pgx_anno {
        input:
            openpgx_query = gvcf_pgx_genotype.openpgx_query,
            pharmgkb_query = gvcf_pgx_genotype.pharmgkb_query,
            sample_id = sample_id
    }

    # 8. Create report
    call pgx_report_module.pgx_report {
        input:
            recommendations = pgx_anno.merged_anno,
            vcf = gvcf_pgx_genotype.genotyped_or_phased_vcf_gz,
            sample_info_json = sample_info_json,
            sample_id = sample_id,
            timezoneDifference = timezoneDifference
    }

    # after all regular tasks merge bco
    # Merge bco, stdout, stderr files
    Array[File] bcos_module = select_all([resources_pgx_polygenic.bco, fq_organize.bco, fq_qc.bco, 
                                          fq_bwa_align.bco, fq_dragmap_align.bco, bam_varcalling.bco, bam_varcalling_dv.bco, 
                                          gvcf_pgx_genotype.bco, pgx_anno.bco, pgx_report.bco])
    Array[File] stdout_module = select_all([resources_pgx_polygenic.stdout_log, fq_organize.stdout_log, fq_qc.stdout_log, 
                                            fq_bwa_align.stdout_log, fq_dragmap_align.stdout_log, bam_varcalling.stdout_log, bam_varcalling_dv.stdout_log,
                                            gvcf_pgx_genotype.stdout_log, pgx_anno.stdout_log, pgx_report.stdout_log])
    Array[File] stderr_module = select_all([resources_pgx_polygenic.stderr_log, fq_organize.stderr_log, fq_qc.stderr_log, 
                                            fq_bwa_align.stderr_log, fq_dragmap_align.stderr_log, bam_varcalling.stderr_log, bam_varcalling_dv.stderr_log,
                                            gvcf_pgx_genotype.stderr_log, pgx_anno.stderr_log, pgx_report.stderr_log])

    call bco_module.bco as merge_bco {
        input:
            bco_array = bcos_module,
            stdout_array = stdout_module,
            stderr_array = stderr_module,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = sample_id,
            sample_info_json = sample_info_json,
            timezoneDifference = timezoneDifference
    }

    output {

        ## FINAL REPORTS
        File report_pdf = pgx_report.pgx_report_pdf
        File report_docx = pgx_report.pgx_report_docx

        ## Recommendations
        File recommendations = pgx_anno.merged_anno
        File variants_with_unknown_genotype = pgx_anno.variants_with_unknown_genotype

        ## QUALITY CHECK REPORTS AND OTHER FILES
        # fastq
        Array[File]? fastqc_zips = fq_qc.fastqc_zips
        File? multiqc_report = fq_qc.multiqc_report
        File? multiqc_data_json = fq_qc.multiqc_data_json

        ## ALIGNMENT RESULTS
        File? final_bam = recalibrated_markdup_bam
        File? final_bai = recalibrated_markdup_bai

        ## SNPs AND INDELs VARIANT CALLING FILES
        # gvcf
        File? gvcf_gz = varcalling_gvcf_gz
        File? gvcf_gz_tbi = varcalling_gvcf_gz_tbi

        # genotyped or/and phased vcf
        File? vcf_gz = gvcf_pgx_genotype.genotyped_or_phased_vcf_gz
        File? vcf_gz_tbi = gvcf_pgx_genotype.genotyped_or_phased_vcf_gz_tbi

        # caller bamouts
        File? final_realigned_bam = bam_varcalling.haplotype_caller_bam
        File? final_realigned_bai = bam_varcalling.haplotype_caller_bai

        ## BCO
        # bco, stdout, stderr
        File bco = merge_bco.bco_merged
        File stdout_log = merge_bco.stdout_log
        File stderr_log = merge_bco.stderr_log

        # bco report (pdf, docx, html)
        File bco_report_pdf = merge_bco.bco_report_pdf
        File bco_report_docx = merge_bco.bco_report_docx

        # bco table (csv)
        File bco_table_csv = merge_bco.bco_table_csv

    }

}
