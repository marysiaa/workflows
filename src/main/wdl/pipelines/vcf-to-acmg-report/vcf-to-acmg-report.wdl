import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@2.0.2/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/string-gp-to-json@2.1.1/src/main/wdl/tasks/string-gp-to-json/string-gp-to-json.wdl" as string_gp_to_json_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-prepare@1.0.7/src/main/wdl/tasks/vcf-prepare/vcf-prepare.wdl" as vcf_prepare_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno@1.17.0/src/main/wdl/modules/vcf-anno/vcf-anno.wdl" as vcf_anno_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-acmg@1.6.0/src/main/wdl/modules/vcf-anno-acmg/vcf-anno-acmg.wdl" as vcf_anno_acmg_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-report@3.2.9/src/main/wdl/modules/vcf-acmg-report/vcf-acmg-report.wdl" as vcf_acmg_report_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow vcf_to_acmg_report {

    String sample_id = "no_id_provided"
    File vcf
    Array[File]? bams

    # Gene panel
    Array[String]? hpo_terms
    Array[String]? genes
    Array[String]? diseases
    String? phenotypes_description
    Array[String]? panel_names
    Boolean is_input_for_panel_generate_defined = (defined(hpo_terms) || defined(genes) || defined(diseases) || defined(phenotypes_description) || defined(panel_names))
    String? panel_json

    ## anno module filtering
    Float splicing_threshold = 0.6 # for dbscSNV ada and rf scores
    Float threshold_frequency = 0.05
    Float quality_threshold_multiallelic = 0 # for multiallelic variants removal
    String impact_threshold = "MODERATE"
    ## CIViC evidence thresholds
    String evidence_level_threshold = "C" ## required minimal CIViC evidence level, possible levels A (very well supported), B, C, D, E (not well supported)
    Int evidence_rating_threshold = 3 ## required minimal CIViC rating, from 1 to 5, where 1 is not very reliable and 5 is very reliable 

    ## report variants selection
    ## severity threshold for acmg (variants of that severity+ will be selected)
    ## possible values "benign", "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"
    String acmg_threshold = "uncertain"

    ## severity threshold for ClinVar (variants of that severity+ will be selected)
    ## possible values "pathogenic", "likely_pathogenic", "uncertain"
    String clinvar_threshold = "likely_pathogenic"

    # Patient information
    File? sample_info_json

    # Report information (time zone difference and ID)
    Int timezoneDifference = 0

    String var_call_tool = "haplotypeCaller"
    String genome_or_exome = "exome" # Possible values "exome", "genome", "target"
    String analysis_start = "vcf"
    String? analysisDescription
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion
    String analysisEnvironment = "IntelliseqFlow"

    Boolean apply_panel_filter = if (genome_or_exome == "target") then false else true
    String freq_and_coverage_source = if (genome_or_exome == "exome") then "exome" else "genome"
    Boolean ignore_mult_stop_warning = true

    # Java options fo snpeff
    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin

    String pipeline_name = "vcf_to_acmg_report"
    String pipeline_version = "1.8.0"

    # 1. Prepare the given vcf to run vcf-anno
    call vcf_prepare_task.vcf_prepare {
        input:
            vcf = vcf,
            vcf_basename = sample_id
    }

    # 2. Prepare gene panel or use user defined
    if(!defined(panel_json) && is_input_for_panel_generate_defined) {
        call panel_generate_task.panel_generate {
            input:
                sample_id = sample_id,
                hpo_terms = hpo_terms,
                genes = genes,
                diseases = diseases,
                phenotypes_description = phenotypes_description,
                panel_names = panel_names
        }
    }

    if(defined(panel_json) && genome_or_exome != "target") {
        call string_gp_to_json_task.string_gp_to_json {
            input:
                panel_json = panel_json,
                hpo_terms = hpo_terms,
                genes = genes,
                diseases = diseases,
                phenotypes_description = phenotypes_description,
                panel_names = panel_names,
                sample_id = sample_id
        }
    }
    Array[File] is_gene_panel = select_all([panel_generate.panel, string_gp_to_json.validated_panel_json])
    if (length(is_gene_panel) > 0) {
        File panel_ready_or_created = select_first([panel_generate.panel, string_gp_to_json.validated_panel_json])
        File panel_inputs_ready_or_created = select_first([panel_generate.inputs, string_gp_to_json.validated_panel_inputs_json])
        File all_panels_json_ready_or_created = select_first([panel_generate.all_panels, string_gp_to_json.all_panels])
    }

    # 3. Annotate and filter variants
    call vcf_anno_module.vcf_anno {
        input:
            vcf_gz = vcf_prepare.prepared_vcf_gz,
            vcf_gz_tbi = vcf_prepare.prepared_vcf_gz_tbi,
            vcf_anno_freq_genome_or_exome = freq_and_coverage_source,
            gnomad_coverage_genome_or_exome = freq_and_coverage_source,
            vcf_basename = sample_id,
            snpeff_java_mem = snpeff_java_mem,
            splicing_threshold = splicing_threshold,
            impact_threshold = impact_threshold,
            threshold_frequency = threshold_frequency,
            quality_threshold_multiallelic = quality_threshold_multiallelic,
            ignore_mult_stop_warning = ignore_mult_stop_warning,
            evidence_level_threshold = evidence_level_threshold,
            evidence_rating_threshold = evidence_rating_threshold 
    }

    # 4. Annotate variants according to ACMG recommendation
     call vcf_anno_acmg_module.vcf_anno_acmg {
        input:
            panel_json = panel_ready_or_created,
            vcf_gz = vcf_anno.annotated_and_filtered_vcf,
            vcf_gz_tbi = vcf_anno.annotated_and_filtered_vcf_tbi,
            sample_id = sample_id,
            apply_panel_filter = apply_panel_filter,
            split_ISEQ_REPORT_ANN_field = true
     }

    # 5. Create ACMG report
    call vcf_acmg_report_module.vcf_acmg_report {
        input:
            vcf_gz = vcf_anno_acmg.annotated_acmg_vcf_gz,
            vcf_gz_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi,
            acmg_threshold = acmg_threshold,
            clinvar_threshold = clinvar_threshold,
            panel_json = panel_ready_or_created,
            panel_inputs_json = panel_inputs_ready_or_created,
            sample_info_json = sample_info_json,
            timezoneDifference = timezoneDifference,
            bams = bams,
            sample_id = sample_id,
            genome_or_exome = genome_or_exome,
            analysis_start = analysis_start,
            analysisDescription = analysisDescription,
            analysisName = analysisName,
            analysisWorkflow = analysisWorkflow,
            analysisWorkflowVersion = analysisWorkflowVersion,
            analysisEnvironment = analysisEnvironment,
            gene_panel_logs = panel_generate.gene_panel_logs,
            impact_threshold = impact_threshold,
            threshold_frequency = threshold_frequency,
            var_call_tool = var_call_tool
    }



    # 5. Merge BCO and prepare report pdf
    Array[File] bcos_module = select_all([panel_generate.bco, string_gp_to_json.bco, vcf_prepare.bco, vcf_anno.bco,
                                         vcf_anno_acmg.bco, vcf_acmg_report.bco])
    Array[File] stdout_module = select_all([panel_generate.stdout_log, string_gp_to_json.stdout_log, vcf_prepare.stdout_log,
                                           vcf_anno.stdout_log, vcf_anno_acmg.stdout_log, vcf_acmg_report.stdout_log])
    Array[File] stderr_module = select_all([panel_generate.stderr_log, string_gp_to_json.stderr_log, vcf_prepare.stderr_log,
                                           vcf_anno.stderr_log, vcf_anno_acmg.stderr_log, vcf_acmg_report.stderr_log])

    call bco_module.bco as merge_bcos {
        input:
            bco_array = bcos_module,
            stdout_array = stdout_module,
            stderr_array = stderr_module,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_info_json = sample_info_json,
            timezoneDifference = timezoneDifference,
            sample_id = sample_id
    }

    output {
        # 1. Annotate and filter variants
        File? annotated_vcf = vcf_anno.annotated_and_filtered_vcf
        File? annotated_vcf_tbi = vcf_anno.annotated_and_filtered_vcf_tbi
        File? annotated_tsv = vcf_anno.tsv_from_vcf

        # 2. Annotate variants according to ACMG recommendation
        File? annotated_acmg_vcf_gz = vcf_anno_acmg.annotated_acmg_vcf_gz
        File? annotated_acmg_vcf_gz_tbi = vcf_anno_acmg.annotated_acmg_vcf_gz_tbi
        File? igv_screenshots_tar_gz = vcf_acmg_report.igv_screenshots_tar_gz
        File? igv_html = vcf_acmg_report.jigv_html
        File? annotated_acmg_tsv = vcf_anno_acmg.tsv_report

        # 3. Report files
        File? ang_pdf_report = vcf_acmg_report.ang_pdf_report
        File? ang_docx_report = vcf_acmg_report.ang_docx_report
        File? annotated_acmg_json = vcf_acmg_report.json_report

        # 4. Gene panel logs
        File? gene_panel_logs = panel_generate.gene_panel_logs

        # 5. Merge BCO and prepare report pdf
        #bco, stdout, stderr
        File? bco = merge_bcos.bco_merged
        File? stdout_log = merge_bcos.stdout_log
        File? stderr_log = merge_bcos.stderr_log

        #bco report (pdf, html)
        File? bco_report_pdf = merge_bcos.bco_report_pdf
        File? bco_report_docx = merge_bcos.bco_report_docx

        #bco table (csv)
        File? bco_table_csv = merge_bcos.bco_table_csv
    }
}