import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.8/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.4/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-gatk-m2@1.1.1/src/main/wdl/tasks/bam-gatk-m2/bam-gatk-m2.wdl" as bam_gatk_m2_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-multiallelic@1.2.1/src/main/wdl/tasks/vcf-split-multiallelic/vcf-split-multiallelic.wdl" as vcf_split_multiallelic_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-sample-merge@1.0.0/src/main/wdl/tasks/vcf-sample-merge/vcf-sample-merge.wdl" as vcf_sample_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.0/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-create-pon@1.0.0/src/main/wdl/tasks/vcf-create-pon/vcf-create-pon.wdl" as vcf_create_pon_task
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module


workflow pon {


  String pipeline_name = "pon"
  String pipeline_version = "1.0.7"
  String batch_id = "pon_samples"
  Int timezoneDifference = 0

  Array[File] fastqs
  Boolean paired = false
  Boolean mark_dup = false
  Boolean run_bsqr = false
  String platform = "IONTORRENT" # Sequencing platfom name: IONTORRENT or illumina
  String bam_concat_name = "variant-calling-ready"
  String reference_genome = "grch38-no-alt" # hg38 or grch38-no-alt (the default)

  ## BAM FILTERING
  Boolean add_bam_filter = true
  ## Which filter should be applied?:
  ## 1 - removing reads with high fraction of mismatches
  ## 2 - removing reads which are both very short and with soft-clipped at both ends
  ## 3 - combining both filter types (1+2)
  ## 4 - 3 plus removal of reads with more than one indel
  String filter_choice = "4"
  ## Set mismatch fraction that is not accepted (for filters 1 and 3)
  Float mismatch_threshold = 0.1
  Boolean remove_chimeras = true

  ## INTERVALS
  File? target_bed
  Array[String]? target_genes
  Int padding = 1000

  ## VARIANT CALLING AND INITIAL FILTERING
  Int max_no_pieces_to_scatter_an_interval_file = 1
  Boolean genotype_germline_sites = true
  Boolean genotype_pon_sites = true
  String pcr_indel_model = "CONSERVATIVE" ##  NONE, CONSERVATIVE, AGGRESSIVE, HOSTILE

  # 1. PREPARING INTERVALS
  call bed_to_interval_list_task.bed_to_interval_list {
      input:
          bed = target_bed,
          gene_list = target_genes,
          reference_genome = reference_genome,
          padding = padding,
          sample_id = batch_id
  }
  File target_interval_list = bed_to_interval_list.custom_interval_file
  File genes_bed_target_json = bed_to_interval_list.genes_bed_target_json
  Boolean mt_interval_list_not_empty = bed_to_interval_list.non_empty_mt_intervals
  Boolean nuclear_interval_list_not_empty = bed_to_interval_list.non_empty_nuclear_intervals

  # 2. ORGANIZE FASQS
   call fq_organize_task.fq_organize {
          input:
              fastqs = fastqs,
              paired = paired,
              split_files = false
   }

  Array[File] fastqs_1 = fq_organize.fastqs_1
  #Array[File] fastqs_2 = fq_organize.fastqs_2
  #Boolean defined_fastqs2 = defined(fastqs_2)

  # 2. ALIGNMENT

   if (paired) {
       Array[File] fastqs_2 = flatten(select_all([fq_organize.fastqs_2]))
       scatter (index in range(length(fastqs_1))) {
           call fq_bwa_align_module.fq_bwa_align as paired_align {
               input:
                   fastqs_left = [fastqs_1[index]],
                   fastqs_right = [fastqs_2[index]],
                   sample_id = batch_id + "_" + index,
                   reference_genome = reference_genome,
                   add_bam_filter = add_bam_filter,
                   filter_choice = filter_choice,
                   mismatch_threshold = mismatch_threshold,
                   mark_dup = mark_dup,
                   paired = true,
                   run_bsqr = run_bsqr,
                   remove_chimeras = remove_chimeras,
                   bam_concat_name = "variant-calling-ready",
                   platform = platform
           }
       }
  }
  if (! paired) {
       scatter (index in range(length(fastqs_1))) {
           call fq_bwa_align_module.fq_bwa_align as single_end_align {
               input:
                   fastqs_left = [fastqs_1[index]],
                   fastqs_right = [],
                   sample_id = batch_id + "_" + index,
                   reference_genome = reference_genome,
                   add_bam_filter = add_bam_filter,
                   filter_choice = filter_choice,
                   mismatch_threshold = mismatch_threshold,
                   mark_dup = mark_dup,
                   paired = false,
                   run_bsqr = run_bsqr,
                   remove_chimeras = remove_chimeras,
                   bam_concat_name = "variant-calling-ready",
                   platform = platform
           }
       }
  }

  Array[File] bams = select_first([single_end_align.recalibrated_markdup_bam, paired_align.recalibrated_markdup_bam])
  Array[File] bais = select_first([single_end_align.recalibrated_markdup_bai, paired_align.recalibrated_markdup_bai])


  if(nuclear_interval_list_not_empty || mt_interval_list_not_empty) {
  # 4. VARIANT CALLING AND PON PREPARING
      scatter (index in range(length(bams))) {
          call bam_gatk_m2_task.bam_gatk_m2 {
              input:
                  index = index,
                  intervals = target_interval_list,
                  tumor_bam = bams[index],
                  tumor_bai = bais[index],
                  genotype_germline_sites = true,
                  genotype_pon_sites = true,
                  pcr_indel_model = pcr_indel_model,
                  sample_id = batch_id + "_" + index
         }

          # 5. SPLIT MULTIALLELIC SITES
          call vcf_split_multiallelic_task.vcf_split_multiallelic {
              input:
                  index = index,
                  vcf_gz = bam_gatk_m2.unfiltered_vcf,
                  vcf_gz_tbi = bam_gatk_m2.unfiltered_vcf_tbi,
                  vcf_basename = batch_id + "_" + index,

         }
  }

      # 6. MERGE VCFs
      call vcf_sample_merge_task.vcf_sample_merge {
          input:
              vcfs = vcf_split_multiallelic.normalized_vcf_gz,
              tbis = vcf_split_multiallelic.normalized_vcf_gz_tbi,
              batch_id = batch_id,
              missing_to_ref = true
      }

      # 7. CREATE PON
      call vcf_create_pon_task.vcf_create_pon{
          input:
              vcf_gz = vcf_sample_merge.merged_vcf,
              vcf_gz_tbi = vcf_sample_merge.merged_vcf_tbi,
              batch_id = batch_id
      }
  }

   # Merge bco, stdout, stderr files
    Array[File] bco_tasks = select_all([bed_to_interval_list.bco, fq_organize.bco, vcf_sample_merge.bco, vcf_create_pon.bco])
    Array[File] stdout_tasks = select_all([bed_to_interval_list.stdout_log, fq_organize.stdout_log, vcf_sample_merge.stdout_log, vcf_create_pon.stdout_log])
    Array[File] stderr_tasks = select_all([bed_to_interval_list.stderr_log, fq_organize.stderr_log, vcf_sample_merge.stderr_log, vcf_create_pon.stderr_log])

    Array[Array[File]] bco_scatters = select_all([bco_tasks, bam_gatk_m2.bco, single_end_align.bco, paired_align.bco,
                                                 bam_gatk_m2.bco, vcf_split_multiallelic.bco])
    Array[Array[File]] stdout_scatters = [stdout_tasks]
    Array[Array[File]] stderr_scatters = [stderr_tasks]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_module.bco as bco_report {
        input:
            bco_array = bco_array,
            #stdout_array = stdout_array,
            #stderr_array = stderr_array,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = batch_id,
            timezoneDifference = timezoneDifference
    }


  output {

    File? pon = vcf_create_pon.pon_vcf_gz
    File? pon_tbi = vcf_create_pon.pon_vcf_gz_tbi
    File stdout_log = bco_report.stdout_log
    File stderr_log = bco_report.stderr_log
    File bco = bco_report.bco_merged

  }

}
