import "https://gitlab.com/intelliseq/workflows/raw/cnv-tsv-read@1.0.0/src/main/wdl/tasks/cnv-tsv-read/cnv-tsv-read.wdl" as cnv_tsv_read_task
import "https://gitlab.com/intelliseq/workflows/raw/array-split@1.0.0/src/main/wdl/tasks/array-split/array-split.wdl" as array_split_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.4/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.1/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/bed-check@1.0.0/src/main/wdl/tasks/bed-check/bed-check.wdl" as bed_check_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-exon-depth-ref@1.0.4/src/main/wdl/tasks/bam-exon-depth-ref/bam-exon-depth-ref.wdl" as bam_exon_depth_ref_task
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow cnv_ref {
    
    String pipeline_name = "cnv_ref"
    String pipeline_version = "1.0.7"
    String batch_id = "cnv_samples"
    Int timezoneDifference = 0
  
    File sample_mapping
    Array[File] fastqs
    Boolean paired = true
    Boolean mark_dup = true
    Boolean run_bsqr = true
    String platform = "illumina"
    String bam_concat_name = "variant-calling-ready"
    String reference_genome = "grch38-no-alt"

    ## BAM FILTERING
    Boolean add_bam_filter = true
    ## Which filter should be applied?:
    ## 1 - removing reads with high fraction of mismatches
    ## 2 - removing reads which are both very short and with soft-clipped at both ends
    ## 3 - combining both filter types (1+2)
    ## 4 - 3 plus removal of reads with more than one indel
    String filter_choice = "3"
    ## Set mismatch fraction that is not accepted (for filters 1 and 3)
    Float mismatch_threshold = 0.1
    Boolean remove_chimeras = false

    ## EXON BOUNDARIES DEFINITION
    File? custom_bed  ## must be exactly the same, as bed that will be used in exome cnv analyses
    String? kit = "exome-v7" # Possible values "exome-v6", "exome-v7", "exome-v8"

    # 1. GET SAMPLE NAMES
    call cnv_tsv_read_task.cnv_tsv_read {
        input:
            sample_mapping = sample_mapping
    }

    scatter (index in range(length(cnv_tsv_read.sample_ids))) { 
        String sample_id = cnv_tsv_read.sample_ids[index]
        # 2. ASSIGN FASTQS TO SAMPLES
        call array_split_task.array_split {
            input:
                fastqs = fastqs,
                sample_id = sample_id,
                sample_mapping = sample_mapping,
                index = index
        }

        #. 3 ORGANIZE FASTQS
        call fq_organize_task.fq_organize {
            input:
                fastqs = array_split.sample_fastqs,
                paired = paired,
                split_files = true,
                index = index
        }

        # 4. ALIGNMENT  
        call fq_bwa_align_module.fq_bwa_align as fq_bwa_align {
            input:
                fastqs_left = fq_organize.fastqs_1,
                fastqs_right = fq_organize.fastqs_2,
                sample_id = sample_id + "_" + batch_id,
                reference_genome = reference_genome,
                add_bam_filter = add_bam_filter,
                filter_choice = filter_choice,
                mismatch_threshold = mismatch_threshold,
                mark_dup = mark_dup,
                paired = true,
                run_bsqr = run_bsqr,
                remove_chimeras = remove_chimeras,
                bam_concat_name = "variant-calling-ready",
                platform = platform
        }
    }

    if (defined(custom_bed)){
        call bed_check_task.bed_check{
            input:
                bed = custom_bed
        }
    }

    call bam_exon_depth_ref_task.bam_exon_depth_ref {
        input:
            bams = fq_bwa_align.recalibrated_markdup_bam,
            bais = fq_bwa_align.recalibrated_markdup_bai,
            batch_id = batch_id,
            custom_bed = bed_check.cnv_bed,
            kit = kit
    }

    # BCO
    Array[File] bco_tasks = select_all([bed_check.bco, bam_exon_depth_ref.bco, cnv_tsv_read.bco])
    Array[File] stdout_tasks = select_all([bed_check.stdout_log, bam_exon_depth_ref.stdout_log, cnv_tsv_read.stdout_log])
    Array[File] stderr_tasks = select_all([bed_check.stderr_log, bam_exon_depth_ref.stderr_log, cnv_tsv_read.stderr_log])

    Array[Array[File]] bco_scatters = [bco_tasks, array_split.bco, fq_organize.bco, fq_bwa_align.bco]    
    Array[Array[File]] stdout_scatters = [stdout_tasks, array_split.stdout_log, fq_organize.stdout_log, fq_bwa_align.stdout_log]
    Array[Array[File]] stderr_scatters = [stderr_tasks, array_split.stderr_log, fq_organize.stderr_log, fq_bwa_align.stderr_log]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_module.bco as bco_report {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = batch_id,
            timezoneDifference = timezoneDifference
    }

    output {

    File cnv_ref = bam_exon_depth_ref.cnv_reference_counts
    Array[File] bams = fq_bwa_align.recalibrated_markdup_bam
    Array[File] bams_bais = fq_bwa_align.recalibrated_markdup_bai
    
    File stdout_log = bco_report.stdout_log
    File stderr_log = bco_report.stderr_log
    File bco = bco_report.bco_merged

    }

}