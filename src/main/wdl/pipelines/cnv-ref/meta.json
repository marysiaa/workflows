{
   "author": "kattom (e-mail: <katarzyna.tomala@intelliseq.pl>)",
   "beta": false,
   "changes": {
      "1.0.7": "update fq-organize",
      "1.0.6": "Update meta",
      "1.0.5": "Update R script (related to filtering of the inconsistent variant type and reads ratio calls)",
      "1.0.4": "Add bed checking, update R script (related to no-calls bug)",
      "1.0.3": "Update meta",
      "1.0.2": "Add exome-v8 kit",
      "1.0.1": "Create meta",
      "1.0.0": "First version"
   },
   "copyright": "Copyright 2019-2020 Intelliseq",
   "description": "Creates reference count data",
   "groupInputs": {
      "target_regions": {
         "name": "Target regions",
         "description": "Set proper value for Kit input if v6, v7 or v8 were used for exome enrichment OR chose bed file with custom regions.",
         "constraints": {
            "minInputs": 1
         }
      }
   },
   "longDescription": "This workflow generates a reference count data. Once generated, this output can be used in two workflows: NGS panel structural variants research analysis and WES structural variants research analysis. There is no definitive rule for how many samples should be used. However, in practice, a minimum of 5 samples is recommended.\n\nWorkflow stages:\n\n1) Alignment and bam files processing\nReads are aligned to the human reference genome GRCh38 without alternative loci with the BWA-MEM software. Mapped reads are then examined for contamination. Chimeric reads, reads with multiple INDELS or a high fraction of mismatched bases and gap openings (above 0.1), as well as very short reads with soft-clipped bases at both ends, are removed. Duplicates are marked with GATK MarkDuplicates. \n\n2) Create count data \nCreate count data from BAM files with function getBamCounts from R ExomeDepth library.",
   "name": "Reference count data for structural variants analysis (input: fastq)",
   "price": "50",
   "tag": "Utilities",
   "input_batch_id": {
      "name": "Batch id",
      "description": "Enter a samples batch id. It is used to name final files.",
      "type": "String",
      "default": "cnv_samples",
      "required": true,
      "index": 1
   },
   "input_custom_bed": {
      "name": "Custom bed file",
      "description": "Choose bed file with exon boundaries, must be coordinate sorted and uncompressed. Needed only if kits other that v6, v7 or v8 were used for exome enrichment. The bed file has to have GRCh38 coordinates.",
      "type": "File",
      "constraints": {
         "extensions": [
            ".bed"
         ]
      },
      "index": 4,
      "groupId": "target_regions"
   },
   "input_fastqs": {
      "name": "Fastq files",
      "description": "Choose paired-end fastq files (may be bgzipped) for all samples. Included samples should be from unrelated persons.",
      "type": "Array[File]",
      "index": 2,
      "required": true,
      "constraints": {
         "extensions": [
            ".fastq.gz",
            ".fastq",
            ".fq.gz",
            ".fq"
         ],
         "paired": true
      }
   },
   "input_kit": {
      "name": "Kit",
      "description": "Choose comprehensive exome kit from exome-v6 for Agilent SureSelect Human All Exon V6, exome-v7 for Agilent SureSelect Human All Exon V7 or exome-v8 for Agilent SureSelect Human All Exon V8. Not needed if custom BED is provided.",
      "type": "String",
      "constraints": {
         "values": [
            "exome-v6",
            "exome-v7",
            "exome-v8"
         ]
      },
      "index": 5,
      "groupId": "target_regions"
   },
   "input_sample_mapping": {
      "name": "Sample mapping file",
      "description": "Choose tab delimited file with samples names (first column) and fastq files names (second column). See example: https://gitlab.com/intelliseq/workflows/raw/dev/src/test/resources/data/txt/cnv-mapping.tsv.",
      "type": "File",
      "required": true,
      "constraints": {
         "extensions": [
            ".tsv"
         ]
      },
      "index": 3
   },
   "output_bco": {
      "name": "Biocompute object",
      "description": "Biocompute object",
      "type": "File",
      "copy": true,
      "groupId": "logs"
   },
   "output_cnv_ref": {
      "name": "Reference count data",
      "description": "Reference count data",
      "type": "File",
      "copy": true
   },
   "output_bams": {
      "name": "Bams",
      "description": "Alignment results",
      "type": "Array[File]",
      "copy": true,
      "groupId": "alignment"
   },
   "output_bams_bais": {
      "name": "Bams bais",
      "description": "Alignment results indexes",
      "type": "Array[File]",
      "copy": true,
      "groupId": "alignment"
   },
   "output_stderr_log": {
      "name": "Standard err",
      "description": "Standard error",
      "type": "File",
      "copy": true,
      "groupId": "logs"
   },
   "output_stdout_log": {
      "name": "Standard out",
      "description": "Standard out",
      "type": "File",
      "copy": true,
      "groupId": "logs"
   },
   "groupOutputs": {
      "alignment": {
         "name": "Alignment and bam files processing",
         "description": ""
      },
      "logs": {
         "name": "Logs",
         "description": ""
      }
   }
}