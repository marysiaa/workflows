import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.4/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-fq-qc@1.0.1/src/main/wdl/modules/rna-seq-fq-qc/rna-seq-fq-qc.wdl" as rna_seq_fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-prep-data@1.1.1/src/main/wdl/modules/rna-seq-prep-data/rna-seq-prep-data.wdl" as rna_seq_prep_data_module
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-align@1.1.0/src/main/wdl/modules/rna-seq-align/rna-seq-align.wdl" as rna_seq_align_module
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-infer-experiment@1.0.1/src/main/wdl/tasks/rna-seq-infer-experiment/rna-seq-infer-experiment.wdl" as rna_seq_infer_experiment_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-most-occur-stranded@1.0.0/src/main/wdl/tasks/rna-seq-most-occur-stranded/rna-seq-most-occur-stranded.wdl" as rna_seq_most_occur_stranded_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-abundance@3.1.2/src/main/wdl/modules/rna-seq-abundance/rna-seq-abundance.wdl" as rna_seq_abundance_module
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-multiqc@1.1.1/src/main/wdl/tasks/rna-seq-multiqc/rna-seq-multiqc.wdl" as rna_seq_multiqc_task
import "https://gitlab.com/intelliseq/workflows/raw/report-rna-seq@1.0.5/src/main/wdl/tasks/report-rna-seq/report-rna-seq.wdl" as report_rna_seq_task
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow rna_seq {

    Array[File]? fastqs
    Boolean is_fastqs_defined = defined(fastqs)
    Array[File]? fastqs_left # use it when you do not want call fq_organize (NOTE must be used with samples_names)
    Array[File]? fastqs_right # use it when you do not want call fq_organize (NOTE must be used with samples_names)
    Array[String]? samples_names # use it when you do not want call fq_organize (NOTE must be used with fastqs_left (single end) or fastqs_left and fastqs_right (paired end))

    Array[String]? genes_anno_list = ["Chromosome/scaffold name", "Gene start (bp)", "Gene end (bp)", "Transcript stable ID", "Gene name", "Gene type", "Gene Synonym", "Gene description", "Exon stable ID"]
    Array[String]? transcripts_anno_list = ["Chromosome/scaffold name", "Transcript start (bp)", "Transcript end (bp)", "Gene stable ID", "Transcript stable ID version", "Gene name", "Transcript type", "Gene Synonym", "Gene description", "Exon stable ID"]

    String organism_name = "Human"
    String release_version = "104" # ensembl version
    String analysis_id = "no_id_provided"
    String chromosome_name = "primary_assembly"
    String stranded = "unknown" # Possible values include: unstranded, stranded and reversely_stranded
    Boolean paired = true
    Boolean known_stranded = if stranded == "unknown" then false else true

    # for single end reads for kallisto-quant tool
    Float fragment_length = 200
    Float fragment_length_sd = 20 #sd - standard deviation

    Boolean run_bco = true

    # rna-seq report
    String? analysisName
    String? analysisWorkflow
    String? analysisWorkflowVersion

    String pipeline_name = "rna_seq"
    String pipeline_version = "1.3.13"

    #1 fq-organize
    if(is_fastqs_defined) {
        call fq_organize_task.fq_organize {
            input:
                fastqs = fastqs,
                paired = paired,
                split_files = false
        }
    }
    Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
    Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])
    Array[String] samples_ids = select_first([fq_organize.samples_ids, samples_names])

    #2 Quality control module
    call rna_seq_fq_qc_module.rna_seq_fq_qc {
        input:
            fastqs_1 = fastqs_1,
            fastqs_2 = fastqs_2,
            samples_ids = samples_ids,
            paired = paired
    }

    #3 Data preparation module
    call rna_seq_prep_data_module.rna_seq_prep_data {
        input:
            release_version = release_version,
            organism_name = organism_name,
            chromosome_name = chromosome_name,
            analysis_id = analysis_id,
            genes_anno_list = genes_anno_list,
            transcripts_anno_list = transcripts_anno_list
    }

    #4 Alignment module
    call rna_seq_align_module.rna_seq_align {
        input:
            fastqs_1 = fastqs_1,
            fastqs_2 = fastqs_2,
            samples_ids = samples_ids,
            ref_genome_index = rna_seq_prep_data.ref_genome_index,
            genome = rna_seq_prep_data.genome,
            paired = paired
    }

    if(!known_stranded) {
        scatter (index in range(length(rna_seq_align.bams))) {
            call rna_seq_infer_experiment_task.rna_seq_infer_experiment {
                input:
                    bam = rna_seq_align.bams[index],
                    bai = rna_seq_align.bams[index],
                    bed = rna_seq_prep_data.bed,
                    index = index
            }
        }

        call rna_seq_most_occur_stranded_task.rna_seq_most_occur_stranded {
            input:
                stranded_array = rna_seq_infer_experiment.stranded
        }
    }

    String strand = select_first([rna_seq_most_occur_stranded.stranded, stranded])

    #5 Counts reads module
    call rna_seq_abundance_module.rna_seq_abundance {
        input:
            bams = rna_seq_align.bams,
            bams_bais = rna_seq_align.bams_bais,
            ensembl_gtf = rna_seq_prep_data.gtf,
            genome = rna_seq_prep_data.genome,
            genes_ensembl_tsv = rna_seq_prep_data.genes_ensembl_tsv,
            transcripts_ensembl_tsv = rna_seq_prep_data.transcripts_ensembl_tsv,
            fastqs_1 = fastqs_1,
            fastqs_2 = fastqs_2,
            transcriptome_index = rna_seq_prep_data.transcriptome_index,
            samples_ids = samples_ids,
            chromosomes = rna_seq_prep_data.chromosomes,
            fragment_length = fragment_length,
            fragment_length_sd = fragment_length_sd,
            is_paired_end = paired,
            stranded = strand,
            analysis_id = analysis_id
    }

    #6 MultiQC report
    call rna_seq_multiqc_task.rna_seq_multiqc {
        input:
            fastqc_zip_files = rna_seq_fq_qc.fastqc_zip_files,
            align_summaries = rna_seq_align.summaries,
            feature_count_gene_level_log = rna_seq_abundance.gene_level_summary,
            bam_metrics = rna_seq_align.bam_metrics,
            infer_experiment_log = rna_seq_infer_experiment.infer_experiment_log,
            analysis_id = analysis_id
    }

    #7 Merge BCO and prepare report pdf
    if (run_bco) {

        Array[File] bco_array = select_all([fq_organize.bco, rna_seq_fq_qc.bco, rna_seq_prep_data.bco, rna_seq_align.bco, rna_seq_abundance.bco, rna_seq_multiqc.bco])
        Array[File] stdout_array = select_all([fq_organize.stdout_log, rna_seq_fq_qc.stdout_log, rna_seq_prep_data.stdout_log, rna_seq_align.stdout_log, rna_seq_abundance.stdout_log, rna_seq_multiqc.stdout_log])
        Array[File] stderr_array = select_all([fq_organize.stderr_log, rna_seq_fq_qc.stderr_log, rna_seq_prep_data.stderr_log, rna_seq_align.stderr_log, rna_seq_abundance.stderr_log, rna_seq_multiqc.stderr_log])

        call bco_module.bco {
            input:
                bco_array = bco_array,
                stdout_array = stdout_array,
                stderr_array = stderr_array,
                is_rna_seq = true,
                pipeline_name = pipeline_name,
                pipeline_version = pipeline_version,
                sample_id = analysis_id
        }
    }

    File bco_json = select_first([bco.bco_merged, ""])

    #8 report
    call report_rna_seq_task.report_rna_seq {
        input:
            bco_json = bco_json,
            organism = organism_name,
            number_of_samples = length(fastqs_1) + length(fastqs_2),
            ensembl_version = release_version,
            analysis_id = analysis_id,
            analysisName = analysisName,
            analysisWorkflow = analysisWorkflow,
            analysisWorkflowVersion = analysisWorkflowVersion,
            paired = paired,
            stranded = strand,
            fragment_length = fragment_length,
            fragment_length_sd = fragment_length_sd
    }

    output {

        #Overrepresented sequences
        File overrepresented = rna_seq_fq_qc.overrepresented
        File overrepresented_csv = rna_seq_fq_qc.overrepresented_csv

        #Bams, bams bais
        Array[File] bams = rna_seq_align.bams
        Array[File] bams_bais = rna_seq_align.bams_bais

        #Counts reads in features (gene or exone) with featurecount
        File gene_level = rna_seq_abundance.gene_level

        #Merged tables from the quant kallisto tool
        File est_counts_tsv = rna_seq_abundance.est_counts_tsv
        File tpm_tsv = rna_seq_abundance.tpm_tsv

        #MultiQC html report
        File report_html = rna_seq_multiqc.report_html

        #Final reports
        File ang_pdf_report = report_rna_seq.rna_seq_report_ang_pdf
        File ang_docx_report = report_rna_seq.rna_seq_report_ang_pdf

        #bco, stdout, stderr
        File? bco = bco.bco_merged
        File? stdout_log = bco.stdout_log
        File? stderr_log = bco.stderr_log

        #bco report (pdf, docx, html)
        File? bco_report_pdf = bco.bco_report_pdf
        File? bco_report_docx = bco.bco_report_docx

        #bco table (csv)
        File? bco_table_csv = bco.bco_table_csv
    }
}
