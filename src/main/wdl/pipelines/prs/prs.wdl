import "https://gitlab.com/intelliseq/workflows/-/raw/resources-polygenic@1.0.0/src/main/wdl/tasks/resources-polygenic/resources-polygenic.wdl" as resources_polygenic_task
import "https://gitlab.com/intelliseq/workflows/-/raw/gvcf-genotype-by-vcf@2.0.5/src/main/wdl/tasks/gvcf-genotype-by-vcf/gvcf-genotype-by-vcf.wdl" as gvcf_genotype_by_vcf_task
import "https://gitlab.com/intelliseq/workflows/-/raw/vcf-imputing@2.1.1/src/main/wdl/modules/vcf-imputing/vcf-imputing.wdl" as vcf_imputing_module
import "https://gitlab.com/intelliseq/workflows/-/raw/prs-models@1.0.6/src/main/wdl/tasks/prs-models/prs-models.wdl" as prs_models_task
import "https://gitlab.com/intelliseq/workflows/-/raw/mobigen-models@4.1.3/src/main/wdl/tasks/mobigen-models/mobigen-models.wdl" as mobigen_models_task
import "https://gitlab.com/intelliseq/workflows/-/raw/prs-health-report@1.1.7/src/main/wdl/tasks/prs-health-report/prs-health-report.wdl" as prs_health_report_task
import "https://gitlab.com/intelliseq/workflows/-/raw/prs-wellness-report@1.1.7/src/main/wdl/tasks/prs-wellness-report/prs-wellness-report.wdl" as prs_wellness_report_task
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow prs {

    File? gvcf_gz
    File? gvcf_gz_tbi
    File? vcf
    Boolean vcf_defined = if defined(vcf) then true else false

    File? sample_info_json
    Int timezoneDifference = 0
    String analysisEnvironment = "IntelliseqFlow"
    Boolean vitalleo_analysis = false
    String analysisRun = if vitalleo_analysis then "Vitalleo" else analysisEnvironment

    String model_type = "health" # options: health, wellness
    String reference_genome = "grch38-no-alt-analysis-set"
    String population_name = "eas"
    Boolean gtc_sites_only = false

    Boolean create_report = true
    String sample_id = "no_id_provided"

    String pipeline_name = "prs"
    String pipeline_version = "1.0.18"

    if (!vcf_defined) {
        #1 Prepare resources
        call resources_polygenic_task.resources_polygenic {}

        #2 Genotype gvcf
        call gvcf_genotype_by_vcf_task.gvcf_genotype_by_vcf {
            input:
                sample_gvcf_gz = gvcf_gz,
                sample_gvcf_gz_tbi = gvcf_gz_tbi,
                interval_vcf_gz = resources_polygenic.polygenic_vcf_gz,
                interval_vcf_gz_tbi = resources_polygenic.polygenic_vcf_gz_tbi,
                sample_id = sample_id,
                small_task = false,
                fix_id = true,
                format_and_genotype_fix = true
        }

        #3 VCF imputing module
        call vcf_imputing_module.vcf_imputing {
            input:
                vcf_gz = gvcf_genotype_by_vcf.genotyped_vcf_gz,
                vcf_gz_tbi = gvcf_genotype_by_vcf.genotyped_vcf_gz_tbi,
                gtc_sites_only = gtc_sites_only,
                reference_genome = reference_genome,
                sample_id = sample_id
        }
    }

    File vcf_gz = select_first([vcf, vcf_imputing.imputed_vcf_gz])

    #4 Get .yml models from the docker
    call prs_models_task.prs_models {
        input:
            sample_info = sample_info_json,
            model_type = model_type,
            analysisRun = analysisRun
    }
#TODO
    Array[File] vcfs_gz = [vcf_gz]
    #5 Create mobigen models
    call mobigen_models_task.mobigen_models {
        input:
            vcfs_gz = vcfs_gz,
            population_name = population_name,
            models = prs_models.yml_models
    }

    if (create_report) {
        #6 health-report
        if (model_type == "health") {
            call prs_health_report_task.prs_health_report {
                input:
                    models = mobigen_models.out_files,
                    sample_info_json = sample_info_json,
                    timezoneDifference = timezoneDifference,
                    sample_id = sample_id,
                    analysisEnvironment = analysisRun
            }
        }

        #7 wellness-report
        if (model_type == "wellness") {
            call prs_wellness_report_task.prs_wellness_report {
                input:
                    models = mobigen_models.out_files,
                    sample_info_json = sample_info_json,
                    timezoneDifference = timezoneDifference,
                    sample_id = sample_id,
                    analysisEnvironment = analysisRun
            }
        }
    }
#
    # after all regular modules merge bco
    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = select_all([resources_polygenic.bco, gvcf_genotype_by_vcf.bco, vcf_imputing.bco,
                                       prs_models.bco, mobigen_models.bco, prs_health_report.bco, prs_wellness_report.bco])
    Array[File] stdout_tasks = select_all([resources_polygenic.stdout_log, gvcf_genotype_by_vcf.stdout_log, vcf_imputing.stdout_log,
                                          prs_models.stdout_log, mobigen_models.stdout_log, prs_health_report.stdout_log, prs_wellness_report.stdout_log])
    Array[File] stderr_tasks = select_all([resources_polygenic.stderr_log, gvcf_genotype_by_vcf.stderr_log, vcf_imputing.stderr_log,
                                          prs_models.stderr_log, mobigen_models.stderr_log, prs_health_report.stderr_log, prs_wellness_report.stderr_log])

    Array[Array[File]] bco_scatters = [bco_tasks]
    Array[Array[File]] stdout_scatters = [stdout_tasks]
    Array[Array[File]] stderr_scatters = [stderr_tasks]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

    call bco_module.bco {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = sample_id
    }

    output {

        # imputed vcf_gz with index
        File? imputed_vcf_gz = vcf_imputing.imputed_vcf_gz
        File? imputed_vcf_gz_tbi = vcf_imputing.imputed_vcf_gz_tbi

        File? health_html_report = prs_health_report.result_health_html
        File? wellness_html_report = prs_wellness_report.result_wellness_html

        #bco, stdout, stderr
        File bco_merged = bco.bco_merged
        File stdout_log = bco.stdout_log
        File stderr_log = bco.stderr_log

        #bco report (pdf, odt, docx, html)
        File bco_report_pdf = bco.bco_report_pdf
        File bco_report_docx = bco.bco_report_docx

        #bco table (csv)
        File bco_table_csv = bco.bco_table_csv

    }

}
