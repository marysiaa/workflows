
import "https://gitlab.com/intelliseq/pgx/raw/resources-pgx-polygenic@1.2.1/src/main/wdl/tasks/resources-pgx-polygenic/resources-pgx-polygenic.wdl" as resources_pgx_polygenic_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.3/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-qc@1.6.6/src/main/wdl/modules/fq-qc/fq-qc.wdl" as fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.0/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling@1.3.12/src/main/wdl/modules/bam-varcalling/bam-varcalling.wdl" as bam_varcalling_module
import "https://gitlab.com/intelliseq/workflows/-/raw/gvcf-pgx-genotype@1.1.3/src/main/wdl/modules/gvcf-pgx-genotype/gvcf-pgx-genotype.wdl" as gvcf_pgx_genotype_module
import "https://gitlab.com/intelliseq/workflows/-/raw/pgx-create-final-json-report@1.0.1/src/main/wdl/tasks/pgx-create-final-json-report/pgx-create-final-json-report.wdl" as pgx_create_final_json_report_task
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow pgx_cbd_thc {

    String pipeline_name = "pgx_cbd_thc"
    String pipeline_version = "1.0.3"

    # Inputs
    Array[File] fastqs
    Array[String] selected_genes = ["CYP2C9", "CYP2C19", "CYP3A4"]

    Boolean mark_dup = false
    Boolean run_phasing = false
    String sample_id = "no_id_provided" 

    # Java options for bam_metrics and bam_gatk_hc
    String java_mem_options =  "-Xms4g -Xmx63g" # use "-Xms4g -Xmx8g" on anakin

    # 1. Prepare interval_list
    call resources_pgx_polygenic_task.resources_pgx_polygenic {
        input:
            genes_list = selected_genes
    }

    # 2. Organise fastq files
    call fq_organize_task.fq_organize {
        input:
            fastqs = fastqs,
            paired = true,
            possible_left_suffixes = ['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz', '1.clean.fq.gz'],
            possible_right_suffixes = ['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz', '2.clean.fq.gz']
    }

    # 3. Check quality of fastq files
    call fq_qc_module.fq_qc {
        input:
            sample_id = sample_id,
            fastqs = fastqs
    }

    # 4. Align reads with bwa-mem
    call fq_bwa_align_module.fq_bwa_align {
        input:
            fastqs_left = fq_organize.fastqs_1,
            fastqs_right = fq_organize.fastqs_2,
            sample_id = sample_id,
            bam_concat_name = "variant-calling-ready",
            mark_dup = mark_dup
    }

    # 5. Call variants with HaplotypeCaller
    call bam_varcalling_module.bam_varcalling {
        input:
            input_bam = fq_bwa_align.recalibrated_markdup_bam,
            input_bai = fq_bwa_align.recalibrated_markdup_bai,
            sample_id = sample_id,
            interval_list = resources_pgx_polygenic.pgx_polygenic_interval_list,
            haplotype_caller_java_mem = java_mem_options,
            bam_concat_name = "variants-sites-only",
            remove_tags = true
    }

    # 6. Genotyping
    call gvcf_pgx_genotype_module.gvcf_pgx_genotype {
        input:
            gvcf_gz = bam_varcalling.gvcf_gz,
            gvcf_gz_tbi = bam_varcalling.gvcf_gz_tbi,
            sample_id = sample_id,
            selected_genes = selected_genes,
            run_phasing = run_phasing,
            create_queries = false
    }

    # 7. Create final json report
    call pgx_create_final_json_report_task.pgx_create_final_json_report {
        input:
            merged_models = gvcf_pgx_genotype.merged_models,
            sample_id = sample_id,
            analysis_version = pipeline_version
    }

    # after all regular tasks merge bco
    # Merge bco, stdout, stderr files
    Array[File] bcos_module = [resources_pgx_polygenic.bco, fq_organize.bco, fq_qc.bco, 
            fq_bwa_align.bco, bam_varcalling.bco, gvcf_pgx_genotype.bco, pgx_create_final_json_report.bco]
    Array[File] stdout_module = [resources_pgx_polygenic.stdout_log, fq_organize.stdout_log, fq_qc.stdout_log, 
            fq_bwa_align.stdout_log, bam_varcalling.stdout_log, gvcf_pgx_genotype.stdout_log, pgx_create_final_json_report.stdout_log]
    Array[File] stderr_module = [resources_pgx_polygenic.stderr_log, fq_organize.stderr_log, fq_qc.stderr_log, 
            fq_bwa_align.stderr_log, bam_varcalling.stderr_log, gvcf_pgx_genotype.stderr_log, pgx_create_final_json_report.stderr_log]

    call bco_module.bco as merge_bco {
        input:
            bco_array = bcos_module,
            stdout_array = stdout_module,
            stderr_array = stderr_module,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = sample_id
    }

    output {

        ## Merged models
        File merged_models = gvcf_pgx_genotype.merged_models

        ## Final json report
        File final_json_report = pgx_create_final_json_report.final_json_report

        ## QUALITY CHECK REPORTS AND OTHER FILES
        # fastq
        Array[File]? fastqc_zips = fq_qc.fastqc_zips
        File? multiqc_report = fq_qc.multiqc_report
        File? multiqc_data_json = fq_qc.multiqc_data_json

        ## ALIGNMENT RESULTS
        File? final_bam = fq_bwa_align.recalibrated_markdup_bam
        File? final_bai = fq_bwa_align.recalibrated_markdup_bai

        ## SNPs AND INDELs VARIANT CALLING FILES
        # gvcf
        File? gvcf_gz = bam_varcalling.gvcf_gz
        File? gvcf_gz_tbi = bam_varcalling.gvcf_gz_tbi

        # genotyped or/and phased vcf
        File? vcf_gz = gvcf_pgx_genotype.genotyped_or_phased_vcf_gz
        File? vcf_gz_tbi = gvcf_pgx_genotype.genotyped_or_phased_vcf_gz_tbi

        # caller bamouts
        File? final_realigned_bam = bam_varcalling.haplotype_caller_bam
        File? final_realigned_bai = bam_varcalling.haplotype_caller_bai

        ## BCO
        # bco, stdout, stderr
        File bco = merge_bco.bco_merged
        File stdout_log = merge_bco.stdout_log
        File stderr_log = merge_bco.stderr_log

        # bco report (pdf, docx, html)
        File bco_report_pdf = merge_bco.bco_report_pdf
        File bco_report_docx = merge_bco.bco_report_docx

        # bco table (csv)
        File bco_table_csv = merge_bco.bco_table_csv

    }

}
