# Description of content of directory bam-quality-check


## JSON files
### Purpose
Each json file describes metrics that will be presented in bam-quality-check report (output of module bam-qc.wdl: .odt, .pdf, .docx, .html ).

This files are specific for some kind of sequencing (example: sureselect-human-all-exon-v6-r2, sureselect-human-all-exon-v7, wgs).

This files are input to task bam-metrics.wdl (script bam-metrics.py)

### Json fields description

***Some of them are obligatory:***
```json
"name": "Human readable name",
"description": "Usefull description",
"query": "CollectAlignmentSummaryMetrics_PF_READS_ALIGNED/1000000",
```
- "query" field contain operation that will be evaluated in Python method ```.eval(query)``` Examples: ```"(CollectAlignmentSummaryMetrics_PCT_PF_READS_ALIGNED)*100"``` or ```(CollectAlignmentSummaryMetrics_PF_ALIGNED_BASES/CollectQualityYieldMetrics_TOTAL_BASES)*100```.
All names in "query" derive from gatk metrics tools, described here: https://broadinstitute.github.io/picard/picard-metric-definitions.html
- "description" usually derives from tool documentation

***Some of them are optional fields***

Optional fields if are not provided will be filled in bam-metrics.py with some default values, to be input for jinja template. 

```json
"warning": "computed_value > 5",
"error": "computed_value > 10",
"expected": "<5 %"
"metric_class": "reads",
"source": "Some link or "

```
- "warning" and "error" will be evaluated using .eval(error) method the same as query. ```computed_value``` is the result from "query" field. Error and warning can be any conditional in Python syntax. 
- "expected" is only some comment,
- "metric_class" is our classification of the metrics, and determines in which section metric will appear in report. If empty, metric will be placed in "other" section. can be: "reads", "bases", "coverage"


## Txt file bam-flagstat-description.txt
### Purpose 
### How was made
