#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg
cd $PROJECT_DIR/src/main/scripts/acmg/


wget -O /tmp/test/acmg/clinvar-protein-changes-dictionary.json http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/22-11-2021/clinvar-protein-changes-dictionary.json

$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz"  \
  --output-vcf /tmp/test/acmg/annotated-ps1.vcf.gz \
  --clinvar-changes /tmp/test/acmg/clinvar-protein-changes-dictionary.json



#OUTPUT_FILE_PATH=/tmp/test/acmg/annotated.vcf.gz
#OUTPUT_FILE_CONTENT=$(zcat $OUTPUT_FILE_PATH)

#TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep "##INFO=ISEQ_BA1" | wc -l)
#if [ $TEST_RESULT -ge 0 ]; then echo ".vcf file is annotated with ISEQ_BA1 field"; else echo "error. Annotation with ISEQ_BA1 field failed"; fi
#TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -v "ISEQ_BA1_SCORE=0" | grep -v "##" | wc -l )
#if [ $TEST_RESULT -ge 0 ]; then echo "ISEQ_BA1_SCORE is annotated successfully. OK"; else echo "error. Annotation with ISEQ_BA1 field failed. Check your script or change test-vcf file"; fi

#$SCRIPT --version
