#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg

$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/acmg-test.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated.vcf.gz \
  --predictor $PROJECT_DIR/src/main/scripts/acmg/predictor-data.json

$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/aloft.vcf.gz" \
  --output-vcf /tmp/test/acmg/aloft-annotated.vcf.gz \
  --predictor $PROJECT_DIR/src/main/scripts/acmg/predictor-data.json

$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/strange-aloft.vcf.gz" \
  --output-vcf /tmp/test/acmg/strange-aloft-annotated.vcf.gz \
  --predictor $PROJECT_DIR/src/main/scripts/acmg/predictor-data.json

echo "output file:"
echo "/tmp/test/acmg/annotated.vcf.gz"
echo "/tmp/test/acmg/aloft-annotated.vcf.gz"
echo "/tmp/test/acmg/strange-aloft-annotated.vcf.gz"
#$SCRIPT --version
