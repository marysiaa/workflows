#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg
python3 $SCRIPT -v

python3 $SCRIPT \
  --input-vcf $RESOURCES_DIR"/data/vcf/annot-with-panel.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated.vcf.gz

python3 $SCRIPT \
  --input-vcf $RESOURCES_DIR"/data/vcf/empty.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated-empty.vcf.gz
