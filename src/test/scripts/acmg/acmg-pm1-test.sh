#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

OUR_PATH=$(pwd)

cd $PROJECT_DIR/src/main/scripts/acmg/

mkdir -p /tmp/test/acmg

wget -O /tmp/test/acmg/clinvar-pathogenic-sites.tab.gz  http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/22-11-2021/clinvar-pathogenic-sites.tab.gz
wget -O /tmp/test/acmg/clinvar-pathogenic-sites.tab.gz.tbi  http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/22-11-2021/clinvar-pathogenic-sites.tab.gz.tbi


$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/pm1-test.vcf.gz" \
  --output-vcf /tmp/test/acmg/nextprot-annotated-pm1.vcf.gz \
  --clinvar-patho /tmp/test/acmg/clinvar-pathogenic-sites.tab.gz








