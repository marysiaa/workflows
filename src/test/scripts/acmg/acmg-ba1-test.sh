#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg

$SCRIPT -v
echo $?
$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated.vcf.gz
echo $?

