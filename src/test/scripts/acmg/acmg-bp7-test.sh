#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg
$SCRIPT -v 

$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/modifier-low.vcf.gz" \
  --output-vcf /tmp/test/acmg/modifier-low-annotated.vcf.gz

$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/acmg-test.vcf.gz" \
  --output-vcf /tmp/test/acmg/acmg-test-annotated.vcf.gz


$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/synonimous.vcf.gz" \
  --output-vcf /tmp/test/acmg/synonimous-annotated.vcf.gz


echo "Output files:"
echo "/tmp/test/acmg/modifier-low-annotated.vcf.gz"
echo "/tmp/test/acmg/acmg-test-annotated.vcf.gz"
echo "/tmp/test/acmg/synonimous-annotated.vcf.gz"
$SCRIPT --version
