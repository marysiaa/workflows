#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg
cd $PROJECT_DIR/src/main/scripts/acmg/






wget -O /tmp/test/acmg/clinvar-lof-dictionary.json \
 http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/22-11-2021/clinvar-lof-dictionary.json
wget -O /tmp/test/acmg/gnomad-lof-dictionary.json \
 http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/gnomad/v2/gnomad-lof-dictionary.json



$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz" \
  --output-vcf /tmp/test/acmg/267-annotated.vcf.gz \
  --gnomad-lof /tmp/test/acmg/gnomad-lof-dictionary.json \
  --clinvar-lof /tmp/test/acmg/clinvar-lof-dictionary.json \
  --loftee-filters  loftee-filters.json

$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/acmg-test.vcf.gz" \
  --output-vcf /tmp/test/acmg/test-annotated-pvs1.vcf.gz \
  --gnomad-lof /tmp/test/acmg/gnomad-lof-dictionary.json \
  --clinvar-lof /tmp/test/acmg/clinvar-lof-dictionary.json \
  --loftee-filters  loftee-filters.json



echo "output files:"
echo "/tmp/test/acmg/267-annotated.vcf.gz"
echo "/tmp/test/acmg/test-annotated-pvs1.vcf.gz"
