#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg

cd $PROJECT_DIR/src/main/scripts/acmg/

zcat $RESOURCES_DIR"data/vcf/complete.anno.v2.vcf.gz" > myvcf

$SCRIPT -i myvcf > /tmp/test/acmg/annotated-with-acmg-summary.vcf

bgzip /tmp/test/acmg/annotated-with-acmg-summary.vcf

$SCRIPT -v



