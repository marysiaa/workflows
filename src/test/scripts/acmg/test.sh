#!/bin/bash

#creates clinvar_gene_lof_dict.txt and clinvar_gene_changes_dict.txt in the present directory
#both json files are needed for acmg annotation, clinvar_ann.vcf: ClinVar vcf with annotations added

python3 clinvar_gene_dict.py /data/public/intelliseqngs/workflows/resources/acmg_files/clinvar_ann.vcf 


#creates gnomad_lof_dict.txt json file in the present directory
python3 gnomad_lof_dict.py  /data/public/intelliseqngs/workflows/resources/acmg_files/gnomad_final.txt

#creates meadian.gnomad.exomes.coverage.summary.tsv  and  over_1.gnomad.exomes.coverage.summary.tsv json files in the present directory (for pm2)
python3 gnomad_coverage_dict.py /data/public/intelliseqngs/workflows/resources/acmg_files 

#makes ACMG PVS1 annotations (how it should work: description_acmg_pvs1.txt)
python3 acmg_pvs1.py /data/test/large-data/acmg_test_files/415-439.annotated-and-filtered.vcf output_file


#makes ACMG PS1 annotations (how it should work: description_acmg_ps1.txt)
python3 acmg_ps1.py  /data/test/large-data/acmg_test_files/415-439.annotated-and-filtered.vcf output_file _name


#both "acmg" files use:
#vcf_line.py
#constants.py
#and the above jsons
