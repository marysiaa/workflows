#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg

cd $PROJECT_DIR/src/main/scripts/acmg/

wget -O /tmp/test/acmg/clinvar-ms-dictionary.json http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/22-11-2021/clinvar-ms-dictionary.json

$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated.vcf.gz \
  --clinvar-ms /tmp/test/acmg/clinvar-ms-dictionary.json


#zcat /tmp/test/acmg/annotated.vcf.gz | grep -v "#" | head



#$SCRIPT --version
