#!/bin/bash

#test to creating coverage statistics report coverage-v2

# tak wygląda uruchomienie tego skryptu w wdl, ale jeszcze nie zadziała bo muszą być ustawione odpowiednie ścieżki. 
workflows/src/main/scripts/reports/generate-report.sh \
--json coverage=/workflows/src/test/resources/data/json/hs_metrics.json \
--template /workflows/src/main/scripts/reports/templates/coverage-v2/content.xml \
--name generate-report
