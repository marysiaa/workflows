#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"


mkdir -p /tmp/test/report-fq-qc
OUTPUT_FILE_PATH="/tmp/test/report-fq-qc/bash-test-fq-qc-report.docx"


docker run --rm -it -v /tmp/test/report-fq-qc:/outputs \
  -v $RESOURCES_DIR/data/json/282-sample-info.json:/resources/282-sample-info.json \
  -v $RESOURCES_DIR/data/json/report_1-quality-check.json:/resources/report_1-quality-check.json \
  -v $RESOURCES_DIR/data/json/report_2-quality-check.json:/resources/report_2-quality-check.json \
  -v $RESOURCES_DIR/data/other/report_1-qc.png:/resources/report_1-qc.png \
  -v $RESOURCES_DIR/data/other/report_2-qc.png:/resources/report_2-qc.png \
  -v $TEMPLATE_DIR/fq-qc:/intelliseqtools/reports/templates/testtools \
  intelliseqngs/report_fq-qc:1.1.0 \
  python3 /intelliseqtools/reports/templates/testtools/report-fq-qc.py \
        --input-template "/intelliseqtools/reports/templates/testtools/fq-qc-template.docx" \
        --input-sample-info-json "/resources/282-sample-info.json" \
        --input-dict-json "/intelliseqtools/reports/templates/testtools/dict-ang.json" \
        --input-qc-json "/resources/report_1-quality-check.json" "/resources/report_2-quality-check.json" \
        --input-qc-png "/resources/report_1-qc.png" "/resources/report_2-qc.png" \
        --input-name "bash-test-fq-qc-report" \
        --output-dir "/outputs"


soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-fq-qc"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH | grep -c 'Measure')

if [ -f "$OUTPUT_FILE_PATH" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "Measure phrase exists"; fi
