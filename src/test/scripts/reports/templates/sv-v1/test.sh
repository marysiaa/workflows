#!/bin/bash

template="sv-v1"

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
TEST_DIR="/tmp/test/reports-test"
mkdir -p $TEST_DIR/"$template"

cp $PROJECT_DIR/src/test/resources/data/other/sv-m.svg $TEST_DIR/$template/sv.svg

cp $PROJECT_DIR/src/test/resources/data/json/sv-picture.json $TEST_DIR/$template
echo '[]' > $TEST_DIR/$template/empty.json
#TEST_DATA_DIR=$PROJECT_DIR/src/test/resources/data/json
$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--template $PROJECT_DIR/src/main/scripts/reports/templates/$template/content.jinja \
--pictures $TEST_DIR/$template/sv-picture.json \
--name sv-report \
--output-dir $TEST_DIR/$template/ \
--json empty=$TEST_DIR/$template/empty.json
