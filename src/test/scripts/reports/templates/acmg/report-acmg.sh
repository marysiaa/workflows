#!/bin/bash
set -e

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-acmg
echo "[]" > /tmp/test/report-acmg/genes_bed_target.json
echo "[]" > /tmp/test/report-acmg/picture-generate-report-cnv-sh.json

#### Test Germline
echo "######################################TEST_REPORT_GERMLINE######################################"
OUTPUT_FILE_PATH_GERMLINE="/tmp/test/report-acmg/germline.docx"

  docker run --rm -it -v /tmp/test/report-acmg:/outputs \
    -v $RESOURCES_DIR/data/json:/resources \
    -v $PROJECT_DIR/src/main/scripts/annotsv/gain.json:/resources/gain.json \
    -v $PROJECT_DIR/src/main/scripts/annotsv/loss.json:/resources/loss.json \
    -v /tmp/test/report-acmg/genes_bed_target.json:/resources/genes_bed_target.json \
    -v /tmp/test/report-acmg/picture-generate-report-cnv-sh.json:/resources/picture-generate-report-cnv-sh.json \
    -v $PROJECT_DIR/src/test/scripts/reports/templates/content/germline-content.json:/resources/content.json \
    -v $PROJECT_DIR/src/main/scripts/reports:/intelliseqtools/reports \
    -v $PROJECT_DIR/src/main/scripts/reports/images:/resources/images \
  intelliseqngs/reports:4.0.24 \
  python3 /intelliseqtools/reports/templates/acmg/report-acmg.py \
  --template "/intelliseqtools/reports/templates/acmg/report-acmg-template.docx" \
  --variants-json "/resources/report-acmg/variants-with-cnv.json" \
  --cnv-variants-json "/resources/report-acmg/cnv-variants.json" \
  --other-jsons "/resources/report-acmg/sample-info.json" \
                "/resources/panel.json" \
                "/resources/content.json" \
                "/resources/panel_inputs.json" \
                "/resources/gene_panel_logs.json" \
                "/resources/genes_bed_target.json" \
                "/resources/picture-generate-report-sh.json" \
                "/resources/picture-generate-report-cnv-sh.json" \
                "/resources/images/images-width.json" \
                "/resources/images/logos.json" \
                "/resources/gain.json" \
                "/resources/loss.json" \
  --analysis-type "exome" \
  --analysis-start "fastq" \
  --analysis-group "germline" \
  --analysis-description "Lorem ipsum dolor sit amet, consectetuer adipiscing elit." \
  --analysis-workflow-name "WES hereditary disorders ACMG report (input: fastq)" \
  --analysis-workflow-version "1.19.2" \
  --sample-id "test_sample" \
  --germline-analysis \
  --output-filename "/outputs/germline.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH_GERMLINE" \
        --outdir "/tmp/test/report-acmg"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH_GERMLINE | grep -c 'ACMG')

if [ -f "$OUTPUT_FILE_PATH_GERMLINE" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "ACMG phrase exists"; fi

### Test Carrier Screening
echo "######################################TEST_REPORT_CARRIER_SCREENING######################################"
OUTPUT_FILE_PATH_CARRIER_SCREENING="/tmp/test/report-acmg/carrier-screening.docx"

  docker run --rm -it -v /tmp/test/report-acmg:/outputs \
    -v $RESOURCES_DIR/data/json:/resources \
    -v $PROJECT_DIR/src/main/scripts/annotsv/gain.json:/resources/gain.json \
    -v $PROJECT_DIR/src/main/scripts/annotsv/loss.json:/resources/loss.json \
    -v /tmp/test/report-acmg/genes_bed_target.json:/resources/genes_bed_target.json \
    -v /tmp/test/report-acmg/picture-generate-report-cnv-sh.json:/resources/picture-generate-report-cnv-sh.json \
    -v $PROJECT_DIR/src/test/scripts/reports/templates/content/carrier_screening-content.json:/resources/content.json \
    -v $PROJECT_DIR/src/main/scripts/reports:/intelliseqtools/reports \
    -v $PROJECT_DIR/src/main/scripts/reports/images:/resources/images \
  intelliseqngs/reports:4.0.24 \
  python3 /intelliseqtools/reports/templates/acmg/report-acmg.py \
  --template "/intelliseqtools/reports/templates/acmg/report-carrier-screening-template.docx" \
  --variants-json "/resources/report-acmg/variants.json" \
  --other-jsons "/resources/report-acmg/sample-info.json" \
                "/resources/panel.json" \
                "/resources/content.json" \
                "/resources/genes_bed_target.json" \
                "/resources/picture-generate-report-sh.json" \
                "/resources/picture-generate-report-cnv-sh.json" \
                "/resources/images/images-width.json" \
                "/resources/images/logos.json" \
                "/resources/gain.json" \
                "/resources/loss.json" \
  --analysis-type "exome" \
  --analysis-start "fastq" \
  --analysis-group "carrier_screening" \
  --analysis-workflow-name "WES carrier screening (input: fastq)" \
  --analysis-workflow-version "1.19.2" \
  --sample-id "test_sample" \
  --output-filename "/outputs/carrier-screening.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH_CARRIER_SCREENING" \
        --outdir "/tmp/test/report-acmg"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH_CARRIER_SCREENING | grep -c 'ACMG')

if [ -f "$OUTPUT_FILE_PATH_CARRIER_SCREENING" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "ACMG phrase exists"; fi


