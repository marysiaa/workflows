#!/bin/bash

set -e

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"
echo $TEMPLATE_DIR
mkdir -p /tmp/test/prepare-sample-info-json

docker run --rm -it -v $TEMPLATE_DIR:/outputs -w /outputs -u "$(id -u):$(id -g)" \
intelliseqngs/reports:4.0.4 bash /outputs/prepare-sample-info-json.sh --sample-info-json "" --timezoneDifference 0 --sampleID "no_id_provided"

rm $TEMPLATE_DIR/sample_info.json.tmp $TEMPLATE_DIR/tmp.json
mv $TEMPLATE_DIR/sample_info.json /tmp/test/prepare-sample-info-json/
ls /tmp/test/prepare-sample-info-json/

cat /tmp/test/prepare-sample-info-json/sample_info.json
ls /tmp/test/prepare-sample-info-json/




