#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-rna-seq

echo "#### RNA-SEQ SINGLE END ####"
OUTPUT_FILE_PATH="/tmp/test/report-rna-seq/report-rna-seq-single.docx"

docker run --rm -it -v /tmp/test/report-rna-seq:/outputs \
  -v "$RESOURCES_DIR"/data/json/bco/bco.json:/resources/bco.json \
  -v "$TEMPLATE_DIR"/rna-seq:/intelliseqtools/reports/templates/testtools \
intelliseqngs/reports:4.0.10 \
python3 /intelliseqtools/reports/templates/testtools/report-rna-seq.py \
        --template "/intelliseqtools/reports/templates/testtools/rna-seq-template.docx" \
        --bco "/resources/bco.json" \
        --organism "Human" \
        --number_of_samples 12 \
        --ensembl_version 104 \
        --analysis_id "Demo" \
        --analysis_name "RNA-seq Single-end" \
        --workflow_name "RNA-seq single-end" \
        --workflow_version "1.1.3" \
        --reference_genome "GRCh38.p13" \
        --is_paired "false" \
        --stranded "unstranded" \
        --fragment_length 200 \
        --fragment_length_sd 20 \
        --output_filename "/outputs/report-rna-seq-single.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-rna-seq"

echo "#### RNA-SEQ PAIRED END ####"
OUTPUT_FILE_PATH="/tmp/test/report-rna-seq/report-rna-seq-paired.docx"

docker run --rm -it -v /tmp/test/report-rna-seq:/outputs \
  -v "$RESOURCES_DIR"/data/json/bco/bco.json:/resources/bco.json \
  -v "$TEMPLATE_DIR"/rna-seq:/intelliseqtools/reports/templates/testtools \
intelliseqngs/reports:4.0.10 \
python3 /intelliseqtools/reports/templates/testtools/report-rna-seq.py \
        --template "/intelliseqtools/reports/templates/testtools/rna-seq-template.docx" \
        --bco "/resources/bco.json" \
        --organism "Human" \
        --number_of_samples 24 \
        --ensembl_version 104 \
        --analysis_id "Demo" \
        --analysis_name "RNA-seq Paired-end" \
        --workflow_name "RNA-seq paired-end" \
        --workflow_version "1.1.3" \
        --reference_genome "GRCh38.p13" \
        --is_paired "true" \
        --stranded "stranded" \
        --output_filename "/outputs/report-rna-seq-paired.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-rna-seq"