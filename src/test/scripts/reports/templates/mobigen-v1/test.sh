#!/bin/bash

template=mobigen-v1
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"


CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports-test/

[ -z $TEST_DIR/$template ] | rm -r $TEST_DIR/$template
mkdir -p $TEST_DIR/$template
#cp /home/kt/Pictures/* $TEST_DIR/$template
#cp $PROJECT_DIR/src/test/resources/data/other/ldl_model.svg $TEST_DIR/$template
#cp $PROJECT_DIR/src/test/resources/data/json/mobigen-picture.json $TEST_DIR/$template
#cp $PROJECT_DIR/src/test/resources/data/json/mobigen-patient.json $TEST_DIR/$template
#cp $PROJECT_DIR/src/test/resources/data/json/mobigen-sample.json $TEST_DIR/$template
#cp $PROJECT_DIR/src/test/resources/data/json/mobigen-example.json $TEST_DIR/$template
cp /home/kt/mobigen-test/* $TEST_DIR/$template/
#cp $PROJECT_DIR/src/test/resources/data/json/references.json $TEST_DIR/$template
#cp /home/kt/references.json $TEST_DIR/$template
TEST_DATA_DIR=$PROJECT_DIR/src/test/resources/data/json
$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json mobigen=$TEST_DIR/$template/BGIWGS.sample.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/$template/content.jinja \
--pictures $TEST_DIR/$template/picture.json \
--name report \
--output-dir $TEST_DIR/$template/ \
--json patient=$TEST_DIR/$template/mobigen-patient.json \
--json sample=$TEST_DIR/$template/mobigen-sample.json \
--json references=$TEST_DIR/$template/references.json
