#!/bin/bash
set -e

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-somatic
echo "[]" > /tmp/test/report-somatic/genes_bed_target.json
echo "[]" > /tmp/test/report-somatic/panel.json

#### Test Somatic
echo "######################################TEST_REPORT_SOMATIC######################################"
OUTPUT_FILE_PATH="/tmp/test/report-somatic/somatic.docx"

  docker run --rm -it -v /tmp/test/report-somatic:/outputs \
    -v $RESOURCES_DIR/data/json:/resources \
    -v /tmp/test/report-somatic/genes_bed_target.json:/resources/genes_bed_target.json \
    -v /tmp/test/report-somatic/panel.json:/resources/panel.json \
    -v $PROJECT_DIR/src/test/scripts/reports/templates/content/somatic-content.json:/resources/content.json \
    -v $PROJECT_DIR/src/main/scripts/reports:/intelliseqtools/reports \
    -v $PROJECT_DIR/src/main/scripts/reports/images:/resources/images \
  intelliseqngs/reports:4.0.37 \
  python3 /intelliseqtools/reports/templates/somatic/report-somatic.py \
  --template "/intelliseqtools/reports/templates/somatic/report-somatic-template.docx" \
  --variants-json "/resources/report-acmg/somatic-variants.json" \
  --other-jsons "/resources/report-acmg/sample-info.json" \
                "/resources/panel.json" \
                "/resources/content.json" \
                "/resources/panel_inputs.json" \
                "/resources/gene_panel_logs.json" \
                "/resources/genes_bed_target.json" \
                "/resources/picture-generate-report-sh.json" \
                "/resources/images/images-width.json" \
                "/resources/images/logos.json" \
  --analysis-type "exome" \
  --sample-id "test_sample" \
  --output-filename "/outputs/somatic.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-somatic"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH | grep -c 'somatic')

if [ -f "$OUTPUT_FILE_PATH" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "somatic phrase exists"; fi
