#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"


mkdir -p /tmp/test/report-bam-qc
OUTPUT_FILE_PATH="/tmp/test/report-bam-qc/bash-test-bam-qc-report.docx"


docker run --rm -it -v /tmp/test/report-bam-qc:/outputs \
  -v $RESOURCES_DIR/data/json/merged_metrics.json:/resources/merged_metrics.json \
  -v $RESOURCES_DIR/data/json/282-sample-info.json:/resources/282-sample-info.json \
  -v $TEMPLATE_DIR/bam-qc:/intelliseqtools/reports/templates/testtools \
intelliseqngs/report_bam-qc:1.1.0 \
python3 /intelliseqtools/reports/templates/testtools/report-bam-qc.py \
        --input-template "/intelliseqtools/reports/templates/testtools/bam-qc-template.docx" \
        --input-coverage-stats-json "/resources/merged_metrics.json" \
        --input-sample-info-json "/resources/282-sample-info.json" \
        --input-name "bash-test-bam-qc-report" \
        --output-dir "/outputs"


soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-bam-qc"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH | grep -c 'Metric')

if [ -f "$OUTPUT_FILE_PATH" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "Metric phrase exists"; fi
