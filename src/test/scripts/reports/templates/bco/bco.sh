#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-bco
OUTPUT_FILE_PATH="/tmp/test/report-bco/report-bco.docx"

docker run --rm -it -v /tmp/test/report-bco:/outputs \
  -v $RESOURCES_DIR//data/json/report-acmg/carrier-screening/sample-info.json:/resources/sample-info.json \
  -v $RESOURCES_DIR//data/json/bco/bco.json:/resources/bco.json \
  -v $TEMPLATE_DIR/bco:/intelliseqtools/reports/templates/testtools \
intelliseqngs/reports:4.0.10 \
python3 /intelliseqtools/reports/templates/testtools/report-bco.py \
        --input-template "/intelliseqtools/reports/templates/testtools/bco-template.docx" \
        --input-sample-info-json "/resources/sample-info.json" \
        --input-bco-json "/resources/bco.json" \
        --output-filename "/outputs/report-bco.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-bco"

### RNA-Seq test
OUTPUT_FILE_PATH="/tmp/test/report-bco/report-bco-rna-seq.docx"
docker run --rm -it -v /tmp/test/report-bco:/outputs \
  -v $RESOURCES_DIR//data/json/report-acmg/carrier-screening/sample-info.json:/resources/sample-info.json \
  -v $RESOURCES_DIR//data/json/bco/bco.json:/resources/bco.json \
  -v $TEMPLATE_DIR/bco:/intelliseqtools/reports/templates/testtools \
intelliseqngs/reports:4.0.10 \
python3 /intelliseqtools/reports/templates/testtools/report-bco.py \
        --input-template "/intelliseqtools/reports/templates/testtools/bco-template-rna-seq.docx" \
        --input-sample-info-json "/resources/sample-info.json" \
        --input-bco-json "/resources/bco.json" \
        --is-rna-seq True \
        --output-filename "/outputs/report-bco-rna-seq.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-bco"
