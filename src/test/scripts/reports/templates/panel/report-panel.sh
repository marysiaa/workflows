#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-panel
OUTPUT_FILE_PATH="/tmp/test/report-panel/bash-test-panel-report.docx"

docker run --rm -it -v /tmp/test/report-panel:/outputs \
  -v $RESOURCES_DIR/data/json/282-sample-info.json:/resources/282-sample-info.json \
  -v $RESOURCES_DIR/data/json/panel.json:/resources/panel.json \
  -v $RESOURCES_DIR/data/json/panel_inputs.json:/resources/panel_inputs.json \
  -v $TEMPLATE_DIR/panel:/intelliseqtools/reports/templates/testtools \
intelliseqngs/report_panel:1.1.0 \
python3 /intelliseqtools/reports/templates/testtools/report-panel.py \
        --input-template "/intelliseqtools/reports/templates/testtools/panel-template.docx" \
        --input-sample-info-json "/resources/282-sample-info.json" \
        --input-panel-json "/resources/panel.json" \
        --input-panel-inputs-json "/resources/panel_inputs.json" \
        --input-name "bash-test-panel-report" \
        --output-dir "/outputs"


soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-panel"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH | grep -c 'Gene')

if [ -f "$OUTPUT_FILE_PATH" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "Gene phrase exists"; fi
