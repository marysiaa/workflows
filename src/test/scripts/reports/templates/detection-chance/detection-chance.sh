#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-detection-chance
OUTPUT_FILE_PATH="/tmp/test/report-detection-chance/report-detection-chance.docx"

docker run --rm -it -v /tmp/test/report-detection-chance:/outputs \
  -v $RESOURCES_DIR/data/json/report-acmg/sample-info.json:/resources/sample-info.json \
  -v $RESOURCES_DIR/data/json/report_detect_stats.json:/resources/stats.json \
  -v $TEMPLATE_DIR/detection-chance:/intelliseqtools/reports/templates/testtools \
  -v $PROJECT_DIR/src/main/scripts/reports:/intelliseqtools/reports \
  -v $PROJECT_DIR/src/main/scripts/reports/images:/resources/images \
intelliseqngs/reports:4.0.24 \
python3 /intelliseqtools/reports/templates/testtools/report-detection-chance.py \
        --input-template "/intelliseqtools/reports/templates/testtools/detection-chance-template.docx" \
        --input-sample-info-json "/resources/sample-info.json" \
        --input-detection-stats-json "/resources/stats.json" \
        --output-filename "/outputs/report-detection-chance.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-detection-chance"
