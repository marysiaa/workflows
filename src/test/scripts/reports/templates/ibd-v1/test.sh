#!/bin/bash

template="ibd-v1"

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
TEST_DIR="/tmp/test/reports-test"
mkdir -p $TEST_DIR/"$template"

cp $PROJECT_DIR/src/test/resources/data/other/roh.svg $TEST_DIR/$template

cp $PROJECT_DIR/src/test/resources/data/json/roh-picture.json $TEST_DIR/$template
cp $PROJECT_DIR/src/test/resources/data/json/roh-picture-height.json $TEST_DIR/$template
cp $PROJECT_DIR/src/test/resources/data/json/test-roh.json $TEST_DIR/$template ## add data
cp $PROJECT_DIR/src/test/resources/data/json/null-roh.json $TEST_DIR/$template
cp $PROJECT_DIR/src/test/resources/data/json/roh-genes.json $TEST_DIR/$template
cp $PROJECT_DIR/src/test/resources/data/json/roh-null-genes.json $TEST_DIR/$template
#TEST_DATA_DIR=$PROJECT_DIR/src/test/resources/data/json
$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json roh_regions=$TEST_DIR/$template/test-roh.json \
--json roh_genes=$TEST_DIR/$template/roh-genes.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/$template/content-with-table.jinja \
--pictures $TEST_DIR/$template/roh-picture.json \
--name report-with-table \
--output-dir $TEST_DIR/$template/ \
--json picture_height=$TEST_DIR/$template/roh-picture-height.json

$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json roh_regions=$TEST_DIR/$template/null-roh.json \
--json roh_genes=$TEST_DIR/$template/roh-null-genes.json  \
--template $PROJECT_DIR/src/main/scripts/reports/templates/$template/content-with-table.jinja \
--pictures $TEST_DIR/$template/roh-picture.json \
--name report-with-empty-table \
--output-dir $TEST_DIR/$template/ \
--json picture_height=$TEST_DIR/$template/roh-picture-height.json

echo "{}" >  $TEST_DIR/$template/empty-file.json
$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json roh_regions=$TEST_DIR/$template/empty-file.json \
--json roh_genes=$TEST_DIR/$template/roh-null-genes.json  \
--template $PROJECT_DIR/src/main/scripts/reports/templates/$template/content-without-table.jinja \
--pictures $TEST_DIR/$template/roh-picture.json \
--name report-without-table \
--output-dir $TEST_DIR/$template/ \
--json picture_height=$TEST_DIR/$template/roh-picture-height.json

