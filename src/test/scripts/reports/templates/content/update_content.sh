#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/update-content
chmod +x $SCRIPT

$SCRIPT \
    --basic-content "$PROJECT_DIR/src/main/scripts/reports/templates/content/basic-content.json" \
    --content "$PROJECT_DIR/src/main/scripts/reports/templates/content/exome-content.json" \
    --analysis-start "fastq" \
    --analysis-group "germline" \
    --kit-short-name "exome-v7" \
    --af 0.05 \
    --impact "MODERATE" \
    --qual 100 \
    --var-call-tool "haplotypeCaller" \
    --aligner-tool "bwa-mem" \
    --output-filename "/tmp/test/tools/update-content/final_content.json"