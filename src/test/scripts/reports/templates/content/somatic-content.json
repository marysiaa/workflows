{
    "name": "Tumour vs. control whole exome sequencing analysis report",
    "methods": {
      "fastq_quality_check": "The quality of the raw reads was analyzed with the fastQC software.",
      "alignment_and_bam_files_processing": "Reads were aligned to the human reference genome GRCh38 without alternative loci with the bwa-mem software. Mapped reads were then examined for contamination. Reads with a high fraction of mismatched bases and gap openings (above 0.1), as well as very short reads with soft-clipped bases at both ends, were removed. Duplicates were marked with GATK MarkDuplicates. The GATK BaseRecalibrator and ApplyBQSR tools were used to adjust base qualities.",
      "variant_calling_and_spurious_variant_removing": [
        "Variants were called with GATK Mutect2. Intervals appropriate for a given kit: Agilent SureSelect Human All Exon exome-v7 (with 200bp padding) were used.",
        "Resulting vcf was filtered with GATK FilterMutectCalls with the following parameters: --max-alt-allele-count 2 --max-events-in-region 3 --unique-alt-read-count 2 --min-allele-fraction 0.02 --min-reads-per-strand 1. We also mark variants that did not pass the FilterAlignmentArtifacts (GATK) filter. The following parameters were used: -num-regularcontigs 194 --max-reasonable-fragment-length 2000 --drop-ratio 0.1 --indel-start-tolerance 8. (See: https://gatk.broadinstitute.org/hc/en-us/articles/360051305471-FilterMutectCalls, https://github.com/broadinstitute/gatk/blob/master/docs/mutect/mutect.pdf and https://gatk.broadinstitute.org/hc/en-us/articles/360050814972-FilterAlignmentArtifacts-EXPERIMENTAL for more information)."
      ],
      "variant_annotation_and_annotation_based_filtering": [
        "Information that can be used for variant pathogenicity evaluation was added to the vcf file. Databases used for annotation included: gnomAD v2.1 and v3 (frequencies, coverage, constraint), 1000Genomes (frequencies), MITOMAP (frequencies, contributed diseases), ClinVar (contributed diseases, pathogenicity), HPO (inheritance mode, contributed phenotypes and diseases), UCSC (repeats, PHAST conservation scores), SIFT4G (constraint), SnpEff (predicted impact on gene product), dbSNP (rsID), Ensembl (gene and transcript information), COSMIC (somatic mutations data), CIViC (clinical interpretation of somatic variants). Common and low impact variants were then filtered out (with max frequency threshold of 0.05 and minimal SnpEff predicted impact on gene product set as MODERATE).",
        "Raport shows variants with CIViC annotations (level A, B, C and rating above 2), variants with multiple records in the COSMIC database as well as known pathogenic and likely pathogenic variants in oncogenes."
      ]
    },
    "igv_plot_description": "The upper panel presents NGS reads as aligned by BWA. Each panel is divided into coverage and alignment sections. The coverage section presents a cumulative histogram. In alignment section sequencing reads are drawn as grey horizontal bars, with variant bases highlighted in color. Color intensity indicates the quality of the base call. The reference genome sequence is presented above alignment panels. Gene annotation track with marked exons (dark blue rectangles) and introns (dark blue line) is presented below the alignment panels.",
    "limitations_of_liability": {
      "header": "The clinical diagnosis should never be based on the Patient\u2019s genomic sequence analysis alone.",
      "body": "Factors such as errors due to sample contamination, rare events of technical errors, genetic events affecting the patient\u2019s condition impossible to detect using currently available knowledge as well as other technical limitations should always be considered. This report should be one of many aspects used by the healthcare provider to help with the diagnosis and treatment plan, but it is not the diagnosis itself."
    },
    "data_protection": "According to Article 24(1) of the Law of 29 August 1997 on Personal Data Protection, the patient's Personal data will be used solely for the purpose of conducting the DNA analysis and will not be shared with any third party. Intelliseq SA declares that it applies all necessary measures to protect the patient's personal data, and in particular will refrain from sharing the data with any unauthorized party as well as prevent the loss of data or data corruption.",
    "limitations_of_the_method": "The employed method is based on the DNA isolated from blood samples and consequently might not reflect the changes in other parts of the body or detect mosaic mutations. Regions characterized with high homology to other parts of genome may not be represented accurately. Some parts of exome have limited coverage which impacts the variant detection accuracy. The ability to detect structural variants is limited. The method is largely based on utilizing the information from external databases, hence the ability to detect variants relevant to patient's condition is limited by the scope of information present in those databases.",
    "versions_of_tools_and_databases": "The analysis was performed using {analysis_name} workflow version {analysis_version}. Detailed information on databases and tool versions are available in the bco.json file in the analysis results folder.",
    "diseases_names": "Diseases names are assigned based on the association with a particular gene symbol in the Human Phenotype Ontology database or the Genetics Home Reference database. The names should be inspected and approved by a laboratory analyst or physician based on other sources of information.",
    "inheritance": "The inheritance pattern is assigned based on the Human Phenotype Ontology. This information should also be confirmed by a laboratory analyst or physician.",
    "inheritance_pattern_match": {
      "text": [
        "The value in this field indicates whether the patient's genotype is a probable cause of the disease, given the inheritance pattern of the damaged gene:"
      ],
      "set": [
        "Dominant: given to heterozygous and homozygous genotypes within the gene of dominant inheritance;",
        "Possibly dominant: given to heterozygous variants within the gene for which both modes of inheritance (dominant and recessive) are assigned;",
        "Recessive: given to homozygous variants of the gene with a recessive mode of inheritance;",
        "Likely compound het: assigned to variants within the gene of recessive inheritance pattern if we found more than one probably damaging variant of the gene. Of note, we do not know the phasing of the variants, and compound heterozygote occurs only when variants are on different chromosomes (and damage both copies of the gene);",
        "Mitochondrial: is given to all mitochondrial variants. Of note, such variants can be damaging even if present only in some fraction of mitochondria;",
        "None: assigned to variants with genotype insufficient to cause disease (for example, heterozygous variants within the gene having recessive inheritance pattern)."
      ]
    },
    "description_of_the_gene_panel": {
      "type": "Type - the source of the gene list included in the extended panel. Possible sources are: gene list provided by a physician, diseases provided by a physician, HPO terms, panels from Genomics England Panel App or epicrysis converted to HPO terms.",
      "phenotype_match": "Phenotype match - percent of a maximum possible score that is attributed to a gene. For each HPO term and/or phenotype description provided by a physician, the algorithm traverses the phenotype ontology tree searching for a match with each gene and assigning a score based on the distance between the original term and the term that matches the gene. For specific genes provided by a physician, the phenotype match is assigned to be 75%. For specific diseases provided by a physician, the phenotype match is assigned to be 50%. For genes from the gene panel, phenotype match is assigned to be 30%."
    },
    "description_of_unused_or_changed_genes": "Part of the genes were not found in Ensembl database and were not used in later analyses or a synonym was found for the gene and its name was changed. Detailed information are available in the {sample_id}_panel_logs.json file in the analysis results folder."
  }