#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-sex-check
OUTPUT_FILE_PATH="/tmp/test/report-sex-check/bash-test-sex-check-report.docx"

docker run --rm -it -v /tmp/test/report-sex-check:/outputs \
  -v $RESOURCES_DIR/data/json/282-sample-info.json:/resources/282-sample-info.json \
  -v $RESOURCES_DIR/data/json/sex_recognition_sry_gene.json:/resources/sex_recognition_sry_gene.json \
  -v $RESOURCES_DIR/data/json/sex_recognition_x_zygosity.json:/resources/sex_recognition_x_zygosity.json \
  -v $RESOURCES_DIR/data/json/sex_recognition_chr_cov.json:/resources/sex_recognition_chr_cov.json \
  -v $TEMPLATE_DIR/sex-check:/intelliseqtools/reports/templates/testtools \
  -v $PROJECT_DIR/src/main/scripts/reports:/intelliseqtools/reports \
  -v $PROJECT_DIR/src/main/scripts/reports/images:/resources/images \
intelliseqngs/report_sex-check:1.2.0 \
python3 /intelliseqtools/reports/templates/testtools/report-sex-check.py \
        --input-template "/intelliseqtools/reports/templates/testtools/sex-check-template.docx" \
        --input-sample-info-json "/resources/282-sample-info.json" \
        --input-dict-json "/intelliseqtools/reports/templates/testtools/dict-ang.json" \
        --input-sex-check-jsons "/resources/sex_recognition_sry_gene.json" "/resources/sex_recognition_x_zygosity.json" "/resources/sex_recognition_chr_cov.json" \
        --output-filename "/outputs/bash-test-sex-check-report.docx"


soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-sex-check"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH | grep -c 'Method')

if [ -f "$OUTPUT_FILE_PATH" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "Method phrase exists"; fi



