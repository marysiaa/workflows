#!/bin/bash

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')

CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports-test/

for template_test in $PROJECT_DIR/src/test/scripts/reports/templates/*/test.sh; do
  printf "\n \n \n RUN TEST FOR $template_test  \n \n \n"
  bash $template_test -H || break

done