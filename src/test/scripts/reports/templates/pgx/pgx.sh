#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-pgx
OUTPUT_FILE_PATH="/tmp/test/report-pgx/report-pgx.docx"

docker run --rm -it -v /tmp/test/report-pgx:/outputs \
  -v $RESOURCES_DIR/data/json/282-sample-info.json:/resources/sample-info.json \
  -v $RESOURCES_DIR/data/json/recommendations.json:/resources/recommendations.json \
  -v $RESOURCES_DIR/data/tsv/allele_match_data.tsv:/resources/allele_match_data.tsv \
  -v $TEMPLATE_DIR/pgx:/intelliseqtools/reports/templates/testtools \
intelliseqngs/reports:4.0.32 \
python3 /intelliseqtools/reports/templates/testtools/report-pgx.py \
        --input-template "/intelliseqtools/reports/templates/testtools/pgx-template.docx" \
        --input-recommendations "/resources/recommendations.json" \
        --input-tsv-from-vcf "/resources/allele_match_data.tsv" \
        --input-sample-info-json "/resources/sample-info.json" \
        --drug-order "tenoxicam;flurbiprofen" \
        --output-filename "/outputs/report-pgx.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-pgx"
