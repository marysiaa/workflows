#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

#test to creating coverage statistics report coverage-v2

CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports/$(date +%Y-%m-%d-%T | sed 's/:/-/g')/
mkdir -p $TEST_DIR/Pictures

cp $PROJECT_DIR/src/test/resources/data/other/breast_cancer_PGS000001_nfe_model.jpg $TEST_DIR/my1.jpg
cp $PROJECT_DIR/src/test/resources/data/other/breast_cancer_PGS000001_nfe_model.jpg $TEST_DIR/my2.jpg
cp $PROJECT_DIR/src/test/resources/data/other/breast_cancer_PGS000001_nfe_model.jpg $TEST_DIR/my3.jpg




echo '[{"path":"my1.jpg"},{"path":"my2.jpg"},{"path":"my3.jpg"}]' > $TEST_DIR/pictures.json

$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json mobigen=$PROJECT_DIR/src/test/resources/data/json/mobigen.json \
--json picture=$TEST_DIR/pictures.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/mobigen-v1/content.xml \
--pictures $TEST_DIR/pictures.json \
--name report \
--output-dir $TEST_DIR
