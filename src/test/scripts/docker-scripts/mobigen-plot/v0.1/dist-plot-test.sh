#!/bin/bash

printf " \n"
printf " Test for dist-plot.R script\n"
printf " ***************************\n"
printf "\n"

### paths #####################################################################################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.R/' )
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../../.." >/dev/null 2>&1 && pwd )"
INPUT=$PROJECT_DIR"/src/test/resources/data/txt/hdl_nfe_data.tab"

### help ######################################################################################
printf " To get help type: Rscript dist-plot.R -h\n"
printf " Help screen:\n"
Rscript $SCRIPT -h

### plotting ##################################################################################
echo
printf " Plotting:\n"
printf " Expected result: hdl_nfe.svg file (created in the working diectory)\n"
Rscript $SCRIPT $INPUT hdl_nfe.svg HDL-C 3.2466 4.0875 4.45 High_risk Low_risk
printf "\n"

### checking if plot was created #############################################################

printf " Checking if picture file was created:\n"
if [[ -f hdl_nfe.svg ]]
then
   printf " OK: output file present\n"
else
   printf " Test failed: no output file\n"
fi
