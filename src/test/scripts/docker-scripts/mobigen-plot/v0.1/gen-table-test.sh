#!/bin/bash

printf " \n"
printf " Test for gen-table.R script\n"
printf " ***************************\n"
printf "\n"

### paths #####################################################################################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.R/' )
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../../.." >/dev/null 2>&1 && pwd )"
INPUT=$PROJECT_DIR"/src/test/resources/data/txt/lactose_data.txt"

### help ######################################################################################
printf " To get help type: Rscript gen-table.R -h\n"
printf " Help screen:\n"
Rscript $SCRIPT -h

### making table ###############################################################################
echo
printf " Making table:\n"
printf " Expected result: lactose.svg file (created in the working diectory)\n"
Rscript $SCRIPT $INPUT lactose.svg AG Low_risk Lactose_intolerance
#Rscript $SCRIPT $INPUT lactose.svg AG 0
printf "\n"

### checking if plot was created #############################################################

printf " Checking if picture file was created:\n"
if [[ -f lactose.svg ]]
then
   printf " OK: output file present\n"
else
   printf " Test failed: no output file\n"
fi
