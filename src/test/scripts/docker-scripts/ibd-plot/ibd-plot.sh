#!/bin/bash

printf " \n"
printf " Test for ibd-plot.R script\n"
printf " ***************************\n"
printf "\n"

### paths #####################################################################################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.R/' )
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../.." >/dev/null 2>&1 && pwd )"
INPUT=$PROJECT_DIR"/src/test/resources/data/txt/360-roh-plot-data.txt"

### help ######################################################################################
printf " To get help type: Rscript ibd-plot.R -h\n"
printf " Help screen:\n"
Rscript $SCRIPT -h

### plotting ##################################################################################
mkdir -p /tmp/test/ibd-plot
echo
printf " Plotting:\n"
printf " Expected result: hdl_nfe.svg file (created in the working diectory)\n"
Rscript $SCRIPT --input $INPUT --output /tmp/test/ibd-plot/roh.svg --height 9.82
printf "\n"

### checking if plot was created #############################################################

printf " Checking if picture file was created:\n"
if [[ -f /tmp/test/ibd-plot/roh.svg ]]
then
   printf " OK: output file present\n"
else
   printf " Test failed: no output file\n"
fi

