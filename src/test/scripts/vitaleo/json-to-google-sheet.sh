#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/sheet.sh/sheet.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/vitaleo

$SCRIPT \
    --model-json "https://gitlab.com/intelliseq/mobigen/raw/develop/src/main/resources/vitalleo_traits/hdl_nfe_model.json"