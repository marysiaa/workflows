#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/\.sh/.py/')
SCRIPT_DIR=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/shoot-test\.sh//')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/python/igv
chmod -R g+w /tmp/test/python

cd /tmp/test/python/igv

set -e
echo "shoot script test"

python3 $SCRIPT \
  --js_file $SCRIPT_DIR"igv.min.js" \
  --list $RESOURCES_DIR"data/txt/position-list" \
  --range 1000 \
  --sample "test" $RESOURCES_DIR"large-data/bam/267.markdup.realigned.recalibrated.bam" $RESOURCES_DIR"large-data/bam/267.markdup.realigned.recalibrated.bam"


python3 $SCRIPT \
  --js_file $PROJECT_DIR/src/main/scripts/igv/"igv.min.js" \
  --list $RESOURCES_DIR"data/txt/sv-shoot-test.txt" \
  --range 300 \
  --sample "test" \
  $RESOURCES_DIR"data/bam/460_small.bam" \
  --sv \
  --softclip

#echo "igv-picture script test"

#for i in $( ls *-igv-screenshot.html )
# do
#  input_file_path=$( readlink -f $i )
#  output_basename=$( basename $i | cut -f1 -d '.' )
#  node  $SCRIPT_DIR"igv-picture.js" --input "$input_file_path" --output "$output_basename".png --delay 1000000
#done
