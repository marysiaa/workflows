#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

#$SCRIPT describe --wdl "src/main/wdl/tasks/collect-hs-metrics/latest/collect-hs-metrics.wdl"

#$SCRIPT plot --wdl "src/main/wdl/tasks/collect-hs-metrics/latest/collect-hs-metrics.wdl"
#$SCRIPT plot --wdl "src/main/wdl/tasks/report-coverage-stats/latest/report-coverage-stats.wdl"
#$SCRIPT plot --wdl "src/main/wdl/tasks/merge-bco/latest/merge-bco.wdl"
#$SCRIPT plot --wdl "src/main/wdl/modules/coverage-statistics/latest/coverage-statistics.wdl" | tee >( grep '<svg' >/tmp/plot.svg) /tmp/complete.txt | grep -v "<svg"


#$SCRIPT plot --wdl "src/main/wdl/tasks/collect-hs-metrics/latest/collect-hs-metrics.wdl"
#$SCRIPT plot --wdl "src/main/wdl/tasks/minimal-task/v1.0/minimal-task.wdl"
#$SCRIPT plot --wdl "src/main/wdl/tasks/minimal-task/v2.0/minimal-task.wdl"
#$SCRIPT plot --wdl "src/main/wdl/modules/minimal-module/latest/minimal-module.wdl"
#$SCRIPT test --wdl "src/main/wdl/tasks/mytask/latest/mytask.wdl"
