#!/bin/bash
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data/tsv"


mkdir -p  /tmp/test/civic-id-arriba

docker run --rm -it -v /tmp/test/civic-id-arriba:/home \
    -v $RESOURCES_DIR/:/tmp \
    -v $PROJECT_DIR/src/main/scripts/tools:/intelliseqtools \
    intelliseqngs/task_arriba-anno-civic:1.0.0 \
    python3 /intelliseqtools/annot_arriba_tsv_with_civic.py \
        --arriba-tsv /tmp/arriba_fusions.tsv \
        --civic-fusions /resources/civic/01-10-2022/civic-fusions.tsv \
        --output /home/annotated.tsv

      