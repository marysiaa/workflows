#!/bin/bash 

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR1=$PROJECT_DIR"src/test/resources/data/txt"
RESOURCES_DIR2=$PROJECT_DIR"src/main/resources/bam-quality-check"
echo $PROJECT_DIR
mkdir -p /tmp/test/samtools-flagstats-to-json 

python3 $SCRIPT \
         --input $RESOURCES_DIR1"/test_flagstat.txt" \
         --output /tmp/test/samtools-flagstats-to-json/test_mapping-stats1.json \
         --percent all  --warnings $RESOURCES_DIR2"/bam-flagstats-description.txt"

echo
printf "Fragment of the output file content\n"
printf "descriptions added, percent should be calculated for total and duplicated reads:\n"
printf "********************************************************************\n"
printf "\n"

cat /tmp/test/samtools-flagstats-to-json/test_mapping-stats1.json | jq .[].name | grep 'In total\|Duplic'
cat /tmp/test/samtools-flagstats-to-json/test_mapping-stats1.json | jq .[].description | grep -i 'passed\|duplic'

python3 $SCRIPT \
         --input $RESOURCES_DIR1/test_flagstat.txt \
         --output /tmp/test/samtools-flagstats-to-json/test_mapping-stats.json \
         --percent  --warnings 

printf "Output file content\n"
printf "descriptions not added, percent not calculated:\n"
printf "********************************************************************\n"
printf "\n"
cat /tmp/test/samtools-flagstats-to-json/test_mapping-stats.json | jq .[].name | grep 'In total\|Duplic'
cat /tmp/test/samtools-flagstats-to-json/test_mapping-stats.json | jq .[].description | grep -i 'passed\|duplic\|in total'
 
python3 $SCRIPT \
         --input $RESOURCES_DIR1/test_flagstat.txt \
         --output /tmp/test/samtools-flagstats-to-json/test_mapping-stats.json \
         --percent all  --warnings 

printf "Output file content\n"
printf "descriptions not added, percent calculated for all statistics\n"
printf "(except for read1 and read2):\n"
printf "********************************************************************\n"
printf "\n"

cat /tmp/test/samtools-flagstats-to-json/test_mapping-stats.json | jq .[].name | grep 'In total\|Duplic'
cat /tmp/test/samtools-flagstats-to-json/test_mapping-stats.json | jq .[].description | grep -i 'passed\|duplic\|in total'


