#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
VCF="$RESOURCES_DIR/data/vcf/test-inheritance-tag.vcf.gz"

mkdir -p /tmp/test/gnomad-id
chmod +x $SCRIPT

python3 $SCRIPT \
  --input-vcf $VCF \
  --output-vcf /tmp/test/gnomad-id/output.vcf.gz
