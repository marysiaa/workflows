#!/bin/bash 
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

$SCRIPT -f $RESOURCES_DIR/vcf/495.fbn1-capn_genotyped_by_clinvar.vcf.gz -o "out.json" -c "$RESOURCES_DIR/json/fbn1-capn3-bam-panel-coverage.json"
[ -z /tmp/test ] || mkdir -p /tmp/test

mv out.json /tmp/test/vcf-detects-stats.json

echo "output" /tmp/test/vcf-detects-stats.json
cat /tmp/test/vcf-detects-stats.json
echo ''