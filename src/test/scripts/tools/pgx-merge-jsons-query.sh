#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/pgx-merge-jsons
chmod +x $SCRIPT

$SCRIPT \
  --input-directory $RESOURCES_DIR"/data/json/pgx-queries" \
  --output-directory "/tmp/test/tools/pgx-merge-jsons"
