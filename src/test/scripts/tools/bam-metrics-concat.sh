#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"


wes_dir=/tmp/test/tools/bam-metrics-concat/wes
wgs_dir=/tmp/test/tools/bam-metrics-concat/wgs

mkdir -p $wes_dir
mkdir -p $wgs_dir

#WGS test
python3 $SCRIPT \
  --final_json $RESOURCES_DIR"/data/json/wgs_14_460_final.json" \
  --final_json $RESOURCES_DIR"/data/json/wgs_12_460_final.json" \
  --kit_json "$PROJECT_DIR/src/main/resources/bam-quality-check/wgs.json" \
  --output_name "/tmp/test/merged_metrics_WGS.json"

  echo "WGS output (WHOLE GENOME SEQUENCING):"
  echo "/tmp/test/merged_metrics_WGS.json"


#WES test
python3 $SCRIPT \
  --final_json $RESOURCES_DIR"/data/json/wes_14_460_final.json" \
  --final_json $RESOURCES_DIR"/data/json/wes_12_460_final.json" \
  --kit_json "$PROJECT_DIR/src/main/resources/bam-quality-check/sure-select-human-all-exon-v7.json" \
  --output_name "/tmp/test/merged_metrics_WES.json"

echo "WES output (EXOME v6-v7)"
echo "/tmp/test/merged_metrics_WES.json"

