#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/python/acmg
chmod +x $SCRIPT

python3 $SCRIPT \
  --input $RESOURCES_DIR"data/json/report-acmg/10-variants.json" \
  --output-pathogenic /tmp/test/python/acmg/pathogenic.json \
  --output-likely-pathogenic /tmp/test/python/acmg/likely-pathogenic.json \
  --output-others /tmp/test/python/acmg/others.json
