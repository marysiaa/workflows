#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

#mkdir -p /tmp/test/tools
#cd $PROJECT_DIR/src/main/scripts/tools/
# shellcheck disable=SC1072


#$SCRIPT \
#  --input-vcf $RESOURCES_DIR"data/vcf/acmg_test.vcf" \
#  --output-json 'output.json'

python3 $SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/annoted-acmg-vcf/267_anno-acmg.vcf" \
  --output-json 'output.json'
