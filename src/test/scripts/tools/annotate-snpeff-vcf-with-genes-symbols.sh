#!/bin/bash

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_VCF=$RESOURCES_DIR/vcf/snpeff.vcf
TEST_VCF2=$RESOURCES_DIR/vcf/snpeff-with-nextprot.vcf.gz
OUT_DIR="/tmp/test/snpeff"

mkdir -p $OUT_DIR
 
cat $TEST_VCF | \
  python3 $SCRIPT \
       --impact MODERATE \
       --genes-field-name ISEQ_GENES_NAMES \
       --impact-field-name ISEQ_HIGHEST_IMPACT \
       --snpeff-version 5.0c \
  | bgzip > $OUT_DIR/modified-snpeff.vcf.gz

zcat $TEST_VCF2 | \
  python3 $SCRIPT \
       --impact MODERATE \
      --genes-field-name ISEQ_GENES_NAMES \
      --impact-field-name ISEQ_HIGHEST_IMPACT \
      --snpeff-version 5.1 \
      | bgzip > $OUT_DIR/modified-snpeff-with-nextprot.vcf.gz
