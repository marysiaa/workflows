#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/json-select-fields/
chmod +x $SCRIPT

$SCRIPT \
  --variants-json "$RESOURCES_DIR/data/json/variants.json" \
  --variants-list "2-151606620-C-A; 4-150867669-A-G;4-122936155-C-T; 5-1282455-C-T; 2-9527798-CTCTG-C" \
  --output-filename "/tmp/test/tools/json-select-fields/test_selected.json"
