#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/pgx-prioritize-anno
chmod +x $SCRIPT

$SCRIPT \
  --recommendations $RESOURCES_DIR"data/json/pgx_merged_recommendations.json" \
  --drugs-to-filter $PROJECT_DIR"/resources/pgx/drugs/drugs_to_filter.tsv" \
  --output-filename "/tmp/test/tools/pgx-prioritize-anno/prioritized.json"
