#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/yaml.sh/yaml.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/updated-yaml

python3 $SCRIPT \
  --input-credentials-json "" \
  --input-google-spreadsheet-key "19qpLmoCYn3riPoNbhHGxbgt00mzoJdmE03nb-k9rSgI" \
  --input-model-yaml "$RESOURCES_DIR/data/other/HC710.yml" \
  --output-model-yaml "/tmp/test/tools/updated-yaml/described-model.yml"
