#!/bin/bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../.." >/dev/null 2>&1 && pwd)"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

OUTPUT=/tmp/test/vcf-anno-hpo
mkdir -p $OUTPUT

docker run --rm -it \
  -v "$PROJECT_DIR"/src/main/scripts/tools/vcf_anno_hpo.py:/tools/vcf_anno_hpo.py \
  -v "$RESOURCES_DIR"/large-data/vcf/:/test-resources \
  -v $OUTPUT:/tmp/ \
  intelliseqngs/task_vcf-anno-hpo:1.0.1 \
  python3 /tools/vcf_anno_hpo.py \
  --input_vcf "/test-resources/267.vcf.gz" \
  --output_vcf /tmp/anno-hpo.vcf.gz


docker run --rm -it \
  -v "$PROJECT_DIR"/src/main/scripts/tools/vcf_anno_hpo.py:/tools/vcf_anno_hpo.py \
  -v "$RESOURCES_DIR"/data/vcf/:/test-resources \
  -v $OUTPUT:/tmp/ \
  intelliseqngs/task_vcf-anno-hpo:1.0.1 \
  python3 /tools/vcf_anno_hpo.py \
  --input_vcf "/test-resources/FEA-small_annotated-with-annotsv-sorted.vcf.gz" \
  --output_vcf /tmp/anno-hpo-sv.vcf.gz \
  --sv 