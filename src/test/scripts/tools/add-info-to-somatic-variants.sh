#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/add-info-to-somatic-variants
chmod +x $SCRIPT

$SCRIPT \
  --variants $RESOURCES_DIR"data/json/report-acmg/somatic-variants.json" \
  --info $PROJECT_DIR"/resources/cancer-gene-census/cancer-gene-census-filtered.tsv" \
  --output-filename "/tmp/test/tools/add-info-to-somatic-variants/variants.json"
