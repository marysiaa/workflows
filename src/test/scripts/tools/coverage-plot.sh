#!/bin/bash
set -e

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/coverage-plot

docker run --rm -it -v /tmp/test/coverage-plot:/home \
    -v $RESOURCES_DIR/data/txt:/tmp \
    -v $PROJECT_DIR/src/main/scripts/tools:/intelliseqtools \
     intelliseqngs/task_sv-cov-plot:1.0.0 \
  Rscript /intelliseqtools/coverage-plot.R \
    --depth /tmp/17_DEL-depth.tsv \
    --variant_start 23683355 \
    --variant_end 23685427 \
    --outfile /home/17_DEL.svg
