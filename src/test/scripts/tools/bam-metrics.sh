#!/bin/bash

set -e

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

TEST_DIR="/tmp/test/tools/bam-metrics"
[ -d $TEST_DIR ] || mkdir -p $TEST_DIR
SAMPLE="460"
CHROMOSOME="chr1"

# Test for WGS
$SCRIPT \
  --metrics CollectAlignmentSummaryMetrics \
  --metrics MarkDuplicates \
  --metrics CollectQualityYieldMetrics \
  --metrics CollectWgsMetrics \
  --chromosome "$CHROMOSOME" \
  --sample $SAMPLE \
  --workdir "$RESOURCES_DIR/txt/"

mv "$CHROMOSOME"_"$SAMPLE"_final.json $TEST_DIR/wgs_"$CHROMOSOME"_"$SAMPLE"_final.json
echo "Outputs for WGS"
echo $TEST_DIR/wgs_"$CHROMOSOME"_"$SAMPLE"_final.json

# Test for exome
$SCRIPT \
  --metrics CollectAlignmentSummaryMetrics \
  --metrics MarkDuplicates \
  --metrics CollectHsMetrics \
  --chromosome "$CHROMOSOME" \
  --sample $SAMPLE \
  --workdir "$RESOURCES_DIR/txt/"

mv "$CHROMOSOME"_"$SAMPLE"_final.json $TEST_DIR/wes_"$CHROMOSOME"_"$SAMPLE"_final.json
echo "Outputs for WES:"
echo $TEST_DIR/wes_"$CHROMOSOME"_"$SAMPLE"_final.json

