#!/bin/bash

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_VCF1=$RESOURCES_DIR/vcf/transcript-selection-test.vcf.gz
TEST_VCF2="/data/test/large-data/vcf/iso-vep-and-snpeff-annot.vcf.gz"
OUT_DIR="/tmp/test/transcript-selection"

mkdir -p $OUT_DIR

python3 $SCRIPT -i $TEST_VCF1 -o $OUT_DIR/out1.vcf.gz

python3 $SCRIPT -i $TEST_VCF2 -o $OUT_DIR/out2.vcf.gz
