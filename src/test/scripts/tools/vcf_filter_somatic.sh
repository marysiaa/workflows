#!/bin/bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../.." >/dev/null 2>&1 && pwd)"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

OUTPUT=/tmp/test/vcf-select-var-somatic
mkdir -p $OUTPUT

docker run --rm -it \
  -v $PROJECT_DIR/src/main/scripts/tools/vcf_filter_somatic.py:/tools/vcf_filter_somatic.py \
  -v $RESOURCES_DIR/data/vcf/anno-civic-evidence.vcf.gz:/test-resources/anno-civic-evidence.vcf.gz \
  -v $RESOURCES_DIR/data/vcf/anno-civic-evidence.vcf.gz.tbi:/test-resources/anno-civic-evidence.vcf.gz.tbi \
  -v $OUTPUT:/tmp/ \
  intelliseqngs/task_vcf-select-var-somatic:1.0.0 \
  python3 /tools/vcf_filter_somatic.py \
  --input_vcf "/test-resources/anno-civic-evidence.vcf.gz" \
  --output_vcf /tmp/filtered.vcf.gz \
  --oncogenes /resources/oncogenes/29-11-2022/oncogenes.txt \
  --sample breast-cancer_tumor