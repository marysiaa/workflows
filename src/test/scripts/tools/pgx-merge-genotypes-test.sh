#!/bin/bash
set -e

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

out_dir=/tmp/test/tools/pgx-merge-genotypes/

mkdir -p $out_dir

cd $out_dir

python3 $SCRIPT \
  -c $RESOURCES_DIR/data/txt/test-cyrius.tsv \
  -p $RESOURCES_DIR/data/json/no_id_provided_pgx.report.json \
  -s $RESOURCES_DIR/data/txt/test_sample_astrolabe-out.tsv \
  -l $RESOURCES_DIR/data/txt/out.txt \
  -i "no_id_provided"

python3 $SCRIPT -c /tmp/monika/pgx-intervals/442/bam-442-wgs-cyrius.tsv \
  -p $RESOURCES_DIR/data/txt/bam-442-wgs_pgx.report.json \
  -s $RESOURCES_DIR/data/txt/bam-442-wgs_astrolabe-out.tsv \
  -l $RESOURCES_DIR/data/txt/out.txt \
  -i "442"

python3 $SCRIPT \
  -p $RESOURCES_DIR/data/json/no_id_provided_pgx.report.json \
  -s $RESOURCES_DIR/data/txt/test_sample_astrolabe-out.tsv \
  -i "test_sample"

#cat $out_dir/no_id_provided_merged-diplotypes.tsv
#cat $out_dir/no_id_provided_merged-diplotypes.json | head
echo "STOP"
