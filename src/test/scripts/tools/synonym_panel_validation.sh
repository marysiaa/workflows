#!/usr/bin/env bash

set -e

PROJECT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')

mkdir -p /tmp/synonym_panel_validation/

docker run --rm -it \
  -w /tmp \
  -v ${PROJECT_DIR}/src/main/scripts/tools/:/test/intelliseqtools/ \
  -v ${PROJECT_DIR}/src/test/resources/data/json:/test/resources \
  -v /tmp/synonym_panel_validation/:/tmp/ \
  intelliseqngs/task_panel-generate:0.1.0 \
  python3 /test/intelliseqtools/synonym_panel_validation.py \
  --panel /test/resources/panel-282-data.json \
  --panel_output /tmp/validate-panel.json \
  --warning_output /tmp/warnings-errors.json

cat /tmp/synonym_panel_validation/validate-panel.json
cat /tmp/synonym_panel_validation/warnings-errors.json

diff /tmp/synonym_panel_validation/validate-panel.json ${PROJECT_DIR}/src/test/resources/data/json/equal-validate-panel.json