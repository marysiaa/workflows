#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/python/acmg
chmod -R g+w /tmp/test/python
cd /tmp/test/python/acmg
python3 $SCRIPT \
  --input $RESOURCES_DIR"data/json/251-wes-annotated-with-acmg.json" \
  --output-benign benign.json \
  --output-likely-benign likely-benign.json \
  --output-uncertain uncertain.json \
  --output-likely-pathogenic likely-pathogenic.json \
  --output-pathogenic pathogenic.json \
  --output-undefined undefined.json
