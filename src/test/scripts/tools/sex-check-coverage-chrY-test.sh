#!/bin/bash

set -e

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

if [ -d /tmp/test/sex-check-coverage-chrY ]; then rm -Rf -r /tmp/test/sex-check-coverage-chrY; fi

mkdir -p /tmp/test/sex-check-coverage-chrY

# Main command
python3 $SCRIPT "$RESOURCES_DIR/bam/220-chr2.bam" > /tmp/test/sex-check-coverage-chrY/sex_recognition.json

echo "Output of the script is here:"
echo "/tmp/test/sex-check-coverage-chrY/sex_recognition.json"

echo " The output file:"
cat /tmp/test/sex-check-coverage-chrY/sex_recognition.json

TEST_RESULT=$(grep "chrY\:2786989-2787603 coverage" /tmp/test/sex-check-coverage-chrY/sex_recognition.json | wc -l)
if [ $TEST_RESULT == 1 ]; then echo "Success"; else echo "ERROR test failed"; fi
