#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../" >/dev/null 2>&1 && pwd )"


#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/main/scripts/tools
mkdir -p /tmp/test/custom-intervals


python3 $SCRIPT \
  --input-ensembl-tsv "$PROJECT_DIR/test/resources/data/txt/ensembl-genes.tsv" \
  --output '/tmp/test/custom-intervals/ensembl-gene.bed'


