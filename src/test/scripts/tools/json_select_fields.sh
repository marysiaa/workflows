#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/json-select-fields/
chmod +x $SCRIPT

$SCRIPT \
  --json "$RESOURCES_DIR/data/json/282-chr15_47497301-49497301_annotated-with-acmg-summary.json" \
  --csv "$PROJECT_DIR/resources/column-list-to-json/column-list-to-json.csv" \
  --output_filename "/tmp/test/tools/json-select-fields/final.json"
