#!/bin/bash
set -e -o pipefail
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
VCF="$RESOURCES_DIR/data/vcf/test-inheritance-tag.vcf.gz"
MT_VCF="$RESOURCES_DIR/data/vcf//test-anno-acmg-mt.vcf.gz"
EMPTY_VCF="$RESOURCES_DIR/data/vcf/empty.vcf.gz"

mkdir -p /tmp/test/inheritance
chmod +x $SCRIPT

bcftools query -f '[%GT]\t%INFO/ISEQ_GENES_NAMES\n' $VCF \
  | cut -f1 -d ':' | grep -v '\.$' | sed 's/0[/|]1/1/' | sed 's/1[/|]1/2/' > /tmp/test/inheritance/gt-gene.tsv

awk '{arr[$2]+=$1 } END {for (key in arr) printf("%s\t%s\n", key, arr[key])}' /tmp/test/inheritance/gt-gene.tsv  \
  | sort -k1,1 > /tmp/test/inheritance/gene-alt-count.tsv
awk '{arr[$2]+=1 } END {for (key in arr) printf("%s\t%s\n", key, arr[key])}' /tmp/test/inheritance/gt-gene.tsv \
  | sort -k1,1 > /tmp/test/inheritance/gene-variant-count.tsv

python3 $SCRIPT \
  --input-vcf $VCF \
  --output-vcf /tmp/test/inheritance/output.vcf.gz \
  --gene-variant-count /tmp/test/inheritance/gene-variant-count.tsv \
  --gene-alt-count /tmp/test/inheritance/gene-alt-count.tsv


bcftools query -f '[%GT]\t%INFO/ISEQ_GENES_NAMES\n' $MT_VCF \
  | cut -f1 -d ':' | grep -v '\.$' | sed 's/0[/|]1/1/' | sed 's/1[/|]1/2/' > /tmp/test/inheritance/gt-gene.tsv

awk '{arr[$2]+=$1 } END {for (key in arr) printf("%s\t%s\n", key, arr[key])}' /tmp/test/inheritance/gt-gene.tsv  \
  | sort -k1,1 > /tmp/test/inheritance/gene-alt-count.tsv
awk '{arr[$2]+=1 } END {for (key in arr) printf("%s\t%s\n", key, arr[key])}' /tmp/test/inheritance/gt-gene.tsv \
  | sort -k1,1 > /tmp/test/inheritance/gene-variant-count.tsv

python3 $SCRIPT \
  --input-vcf $MT_VCF \
  --output-vcf /tmp/test/inheritance/output-mt.vcf.gz \
  --gene-variant-count /tmp/test/inheritance/gene-variant-count.tsv \
  --gene-alt-count /tmp/test/inheritance/gene-alt-count.tsv

touch /tmp/test/inheritance/empty-gene-alt-count.tsv
touch /tmp/test/inheritance/empty-gene-variant-count.tsv

python3 $SCRIPT \
  --input-vcf $EMPTY_VCF \
  --output-vcf /tmp/test/inheritance/output-empty.vcf.gz \
  --gene-variant-count /tmp/test/inheritance/empty-gene-variant-count.tsv \
  --gene-alt-count /tmp/test/inheritance/empty-gene-alt-count.tsv
