#!/bin/bash
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data/vcf"


mkdir -p  /tmp/test/civic-id

docker run --rm -it -v /tmp/test/civic-id:/home \
    -v $RESOURCES_DIR/:/tmp \
    -v $PROJECT_DIR/src/main/scripts/tools:/intelliseqtools \
    intelliseqngs/task_vcf-anno-civic:1.1.0 \
    python3 /intelliseqtools/match_civic_id.py \
        --input-vcf /tmp/small-breast-cancer.vcf.gz  \
        --output-vcf /home/annotated.vcf.gz \
        --civic-json /resources/civic/01-10-2022/civic.json



