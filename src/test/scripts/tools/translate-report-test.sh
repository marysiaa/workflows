#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATES_DIR=$PROJECT_DIR"/src/main/scripts/reports/templates"

mkdir -p /tmp/test/tools/translate-report

$SCRIPT \
  --input-xlsx $RESOURCES_DIR"/data/other/target-seq-reports-content.xlsx" \
  --input-jinja $TEMPLATES_DIR"/report-ang-v1" \
  --output-jinja /tmp/test/tools/translate-report
