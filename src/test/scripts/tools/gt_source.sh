#!/bin/bash

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"

mkdir -p /tmp/test/gc-source


python3 $SCRIPT \
		--input-vcf $RESOURCES_DIR/vcf/small-gtc-genotyped.vcf.gz \
		--output-vcf /tmp/test/gc-source/out.vcf.gz \
		--bcftools-tsv $RESOURCES_DIR/tsv/small-gtc.tsv