#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data/vcf"

OUTPUT="/tmp/test/anno-dict"
mkdir -p $OUTPUT

# HPO - phenotypes
HPO_PHENOTYPES_HEADER='##INFO=<ID=ISEQ_HPO_PHENOTYPES,Number=A,Type=String,Description="'"Description"'",Source="HPO",Version="'"version"'">'
printf "ATAD3A\tfirst_phenotype_adat3a,second_phenotype_adat3a\n" > $OUTPUT/hpo-pheno.tsv 
printf "ATAD3C\tfirst_phenotype_adat3c,second_phenotype_adat3c\n" >> $OUTPUT/hpo-pheno.tsv

# HPO - inheritance
HPO_INHERITANCE_HEADER='##INFO=<ID=ISEQ_HPO_INHERITANCE,Number=A,Type=String,Description="'"Description"'",Source="HPO",Version="'"version}"'">'
printf "ATAD3A\tfirst_inheritance_mode_adat3a,second_inheritance_mode_adat3a\n" > $OUTPUT/hpo-inheritance.tsv 
printf "ATAD3C\tfirst_inheritance_mode_adat3c\n" >> $OUTPUT/hpo-inheritance.tsv




zcat "$RESOURCES_DIR/FEA-small_annotated-with-annotsv-sorted.vcf.gz" | python3 "$SCRIPT" \
      $OUTPUT/hpo-pheno.tsv \
     Gene_name  \
     ISEQ_HPO_PHENOTYPES \
     -k "|" \
     "$HPO_PHENOTYPES_HEADER" \
     | bgzip > $OUTPUT//FEA_annotated-with-pheno.vcf.gz


zcat "$RESOURCES_DIR/FEA-small_annotated-with-annotsv-sorted.vcf.gz" | python3 "$SCRIPT" \
    $OUTPUT/hpo-inheritance.tsv \
    Gene_name \
    ISEQ_HPO_INHERITANCE  \
    -k "|" \
    "$HPO_INHERITANCE_HEADER" \
    | bgzip > $OUTPUT/FEA_annotated-with-inh.vcf.gz