#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/vcf-anno-ghr

$SCRIPT \
  --input-vcf $RESOURCES_DIR"/large-data/vcf/267.vcf.gz" \
  --input-json $RESOURCES_DIR"/large-data/json/ghr-summaries.json" \
  --output-vcf /tmp/test/tools/vcf-anno-ghr/annotate.vcf.gz

$SCRIPT \
  --input-vcf $RESOURCES_DIR"/data/vcf/FEA-small_annotated-with-annotsv-sorted.vcf.gz" \
  --input-json $RESOURCES_DIR"/large-data/json/ghr-summaries.json" \
  --output-vcf /tmp/test/tools/vcf-anno-ghr/annotate-sv.vcf.gz \
  --sv 