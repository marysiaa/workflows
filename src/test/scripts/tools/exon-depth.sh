#!/bin/bash
set -e

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/exon-depth
wget -O /tmp/test/exon-depth/small-wes-sample.bam http://anakin.intelliseq.pl/public/large-data/bam/small-wes-sample.bam
wget -O /tmp/test/exon-depth/small-wes-sample.bam.bai http://anakin.intelliseq.pl/public/large-data/bam/small-wes-sample.bam.bai

docker run --rm -it -v /tmp/test/exon-depth:/home \
    -v $RESOURCES_DIR/data/:/tmp \
    -v $PROJECT_DIR/src/main/scripts/tools:/intelliseqtools \
    intelliseqngs/task_bam-exon-depth:1.0.0 \
    Rscript /intelliseqtools/exon-depth.R \
    --exon-bed /tmp/bed/small-exon.bed \
    --bam-dir /home \
    --outfile /home/out.vcf \
    --reference /tmp/txt/small-ref-counts.tsv \
    --bf-threshold 10




