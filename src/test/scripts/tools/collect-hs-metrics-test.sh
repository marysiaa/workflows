#!/bin/bash 
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

$SCRIPT "$RESOURCES_DIR/json/final_json.json" "$RESOURCES_DIR/json/json-with-errors-coverage-stats.json"
