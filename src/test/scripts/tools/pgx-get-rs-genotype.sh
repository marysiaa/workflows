#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/pgx-get-rs-genotype
chmod +x $SCRIPT

# PharmGKB queries
$SCRIPT \
  --rs-from-vcf $RESOURCES_DIR"/data/tsv/rs_from_vcf.tsv" \
  --rs-to-filter $PROJECT_DIR"/resources/pgx/rs/rs_Ummar.tsv" \
  --output-filename "/tmp/test/tools/pgx-get-rs-genotype/pharmgkb_queries.json"

# OpenPGX queries
$SCRIPT \
  --rs-from-vcf $RESOURCES_DIR"/data/tsv/rs_from_vcf.tsv" \
  --rs-to-filter $PROJECT_DIR"/resources/pgx/rs/genes_encoding.tsv" \
  --query-for-openpgx \
  --output-filename "/tmp/test/tools/pgx-get-rs-genotype/openpgx_queries.json"