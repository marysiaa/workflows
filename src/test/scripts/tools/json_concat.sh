#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/_test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

#mkdir -p /tmp/test/tools/json-concat-prodia
mkdir -p /tmp/test/json-concat-prodia

chmod +x "$SCRIPT"

$SCRIPT \
  --input_qc "$RESOURCES_DIR/json/qc_prodia.json" \
  --input_results "$RESOURCES_DIR/json/clustered_204800980122_R01C02-breast_prodia.yml-cancer.sample.json" "$RESOURCES_DIR/json/unclustered_204800980122_R01C02-breast_prodia.yml-cancer.sample.json" "$RESOURCES_DIR/json/unclustered_204800980122_R01C02-hirisplex.yml-diabetes.sample.json" \
  --output_file "/tmp/test/json-concat-prodia/out-array.json"

$SCRIPT \
  --is_nipt \
  --input_qc $RESOURCES_DIR/json/nipt-test_multiqc-stats.json \
  --input_sample_info $RESOURCES_DIR/json/nipt_sample_info.json \
  --input_syndromes $RESOURCES_DIR/tsv/P19_abberations-anno-syndromes.tsv \
  --input_clinvar $RESOURCES_DIR/tsv/P19_abberations-anno-clinvar.tsv \
  --input_aberrations $RESOURCES_DIR/bed/P19_aberrations.bed \
  --input_illumina_plots $RESOURCES_DIR/png/P19_chr13.png $RESOURCES_DIR/png/P19_all-chr.png \
  --input_plots  RESOURCES_DIR/png/P19_wsx-chr2.png \
  --input_syndromes_json $RESOURCES_DIR/json/P19_syndromes.json \
  --input_illumina_report $RESOURCES_DIR/tsv/P19_report.tsv \
  --input_wsx_run_status $RESOURCES_DIR/txt/P19_run-status.txt \
  --output_file "/tmp/test/json-concat-prodia/out-nipt.json"
