#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/main/scripts/tools/biomart
mkdir -p /tmp/test/acmg


python $SCRIPT \
  --input-dataset "hsapiens_gene_ensembl" \
  --input-host "http://ensembl.org" \
  --output-biomart-csv '/tmp/test/genes.csv'

