#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/multiqc-create-config
mkdir -p /tmp/test/tools/multiqc-create-config-with-title

python3 $SCRIPT \
  --type "genome" \
  --sample-info $RESOURCES_DIR"/data/json/267-sample.json" \
  --info-dict $PROJECT_DIR"/src/main/docker/multiqc/multiqc-config-sample-info-dict.json"

mv multiqc_config.yaml /tmp/test/tools/multiqc-create-config
echo "Output: /tmp/test/tools/multiqc-create-config/multiqc_config.yaml"

python3 $SCRIPT \
  --info-dict $PROJECT_DIR"/src/main/docker/multiqc/multiqc-config-sample-info-dict.json" \
  --title Nice_rna_seq_report
mv multiqc_config.yaml /tmp/test/tools/multiqc-create-config-with-title
echo "Output: /tmp/test/tools/multiqc-create-config-with-title/multiqc_config.yaml"
