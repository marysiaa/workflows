#!/bin/bash
#!/bin/bash

##paths ######################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_BED=$RESOURCES_DIR/bed/refGene.test.sorted.bed

#echo $SCRIPT
#echo $PROJECT_DIR
#echo $RESOURCES_DIR
#echo $TEST_VCF

### help #############################

printf "\n"
printf "Help screen:\n"
printf "************\n"

python3 $SCRIPT -h

### usage ###########################

printf "\n"
printf "Running script: \n"
printf "************\n"

mkdir -p /tmp/test/change-starts
OUTDIR="/tmp/test/change-starts"

python3 $SCRIPT \
       --input_bed $TEST_BED \
       --output $OUTDIR/output.bed 
       

printf "First line of the input bed file: \n"
head -1 $TEST_BED
printf "First line of the output bed file: \n" 
head -1 $OUTDIR/output.bed
printf "Values in \"start\" columns (2,7,9) in output file should be +1 vs columns in the input bed\n"  
