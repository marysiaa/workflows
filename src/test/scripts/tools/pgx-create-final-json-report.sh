#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/pgx-create-final-json-report
chmod +x $SCRIPT

$SCRIPT \
  --models $RESOURCES_DIR"/data/json/merged_models.json" \
  --diplotype-phenotype-table $PROJECT_DIR"/resources/pgx/diplotype-phenotype-table/Diplotype_Phenotype_Table.xlsx" \
  --analysis-name "PGx Cannabinoid report" \
  --analysis-version "1.0.0" \
  --output-filename "/tmp/test/tools/pgx-create-final-json-report/final_report.json"
