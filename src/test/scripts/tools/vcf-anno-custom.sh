#!/usr/bin/env bash

set -ex

if [ -f /tmp/conf.toml ]; then
  rm /tmp/conf.toml
fi

docker run -u "$(id -u):$(id -g)" --rm -it -v /tmp:/tmp \
  -v $PWD/:/intelliseqtools -w /tmp intelliseqngs/task_vcf-anno-custom:1.0.0 bash -c '
    python3 /intelliseqtools/src/main/scripts/tools/vcf-anno-custom.py \
      --output_path /tmp/conf.toml \
    --query_vcf /intelliseqtools/src/test/resources/data/vcf/282-chr15_47497301-49497301_annotated-with-snpeff.vcf.gz \
    --query_bed /intelliseqtools/src/test/resources/data/bed/vcf-anno-custom.bed.gz && \
    vcfanno /tmp/conf.toml /intelliseqtools/src/test/resources/data/vcf/282-chr15.47497301-49497301.vcf.gz > /tmp/temp_annotated.vcf 
  '

expected="$PWD/src/test/resources/data/vcf/282-chr15.47497301-49497301_annotated_by_vcf-anno-custom.vcf"

if [ -f $expected ]; then
    diff <(cat /tmp/temp_annotated.vcf) <(cat $expected)
else
    cp /tmp/temp_annotated.vcf $expected 
fi

