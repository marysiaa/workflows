#!/bin/bash

##paths ######################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_VCF=$RESOURCES_DIR/vcf/282-chr15_47497301-49497301_annotated-with-snpeff.vcf.gz
TEST_PANEL=$RESOURCES_DIR/json/example-panel.json

mkdir -p /tmp/test/filter-vcf-by-gene-panel/

## add annotation and filter
zcat $TEST_VCF | python3 $SCRIPT $TEST_PANEL | bgzip >  /tmp/test/filter-vcf-by-gene-panel/example.filtered.vcf.gz

## add annotation, do not filter
zcat $TEST_VCF | python3 $SCRIPT $TEST_PANEL --skip_filter | bgzip >  /tmp/test/filter-vcf-by-gene-panel/example.not-filtered.vcf.gz
