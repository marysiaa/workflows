#!/bin/bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../.." >/dev/null 2>&1 && pwd)"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

OUTPUT=/tmp/test/vcf-anno-civic-evidence
mkdir -p $OUTPUT

docker run --rm -it \
  -v $PROJECT_DIR/src/main/scripts/tools/vcf-anno-civic-evidence.py:/tools/vcf-anno-civic-evidence.py \
  -v $RESOURCES_DIR/data/vcf/civic_annotated.vcf.gz:/test-resources/civic_annotated.vcf.gz \
  -v $RESOURCES_DIR/data/vcf/civic_annotated.vcf.gz.tbi:/test-resources/civic_annotated.vcf.gz.tbi \
  -v $OUTPUT:/tmp/ \
  intelliseqngs/task_vcf-anno-civic-evidence:1.0.2 \
  python3 /tools/vcf-anno-civic-evidence.py \
  --input_vcf "/test-resources/civic_annotated.vcf.gz" \
  --output_vcf /tmp/anno-civic-evidence.vcf.gz \
  --minimal_evidence_level A \
  --minimal_evidence_rating 5
