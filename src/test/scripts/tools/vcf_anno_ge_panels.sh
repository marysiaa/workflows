#!/bin/bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../.." >/dev/null 2>&1 && pwd)"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data/vcf/"

OUTPUT=/tmp/test/vcf-anno-ge-panels
mkdir -p $OUTPUT

docker run --rm -it \
  -v $PROJECT_DIR/src/main/scripts/tools/vcf_anno_ge_panels.py:/tools/vcf_anno_ge_panels.py \
  -v $RESOURCES_DIR:/test-resources \
  -v $OUTPUT:/tmp/ \
  intelliseqngs/task_vcf-anno-ge-panels:1.3.0 \
  python3 /tools/vcf_anno_ge_panels.py \
  --input_vcf "/test-resources/282-chr15.47497301-49497301.annotated-with-functional-annotations-variant-level.vcf.gz" \
  --output_vcf /tmp/anno-ge-panels.vcf.gz
