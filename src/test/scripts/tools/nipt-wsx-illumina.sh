#!/bin/bash
set -e

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data/"

mkdir -p /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/txt/1804400325_S3_R1_001_sorted-dup-marked_statistics.txt /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/txt/1806600529_S5_R1_001_sorted-dup-marked_statistics.txt /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/txt/1804800173_S4_R1_001_sorted-dup-marked_statistics.txt /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/txt/RAS0255697_S8_R1_001_sorted-dup-marked_statistics.txt /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/tsv/1804400325_cnv-summary.tsv /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/tsv/1806600529_cnv-summary.tsv /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/tsv/1804800173_cnv-summary.tsv /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/tsv/RAS0255697_cnv-summary.tsv /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/txt/test_NIPT_RESULT.csv /tmp/test/wsx-illumina-plots
cp $RESOURCES_DIR/json/nipt_sample_info.json /tmp/test/wsx-illumina-plots



docker run --rm -it -v /tmp/test/wsx-illumina-plots:/home \
    -v $PROJECT_DIR/src/main/scripts/tools:/intelliseqtools \
    intelliseqngs/task_nipt-wsx-illumina:1.0.0 \
  Rscript /intelliseqtools/wsx-illumina-plot.R \
    --wsx_dir /home \
    --reports_dir /home \
    --out_dir /home \
    --cutoff /resources/AdjZCutOff.tsv \
    --illumina_nipt /home/test_NIPT_RESULT.csv \
    --example_samples /resources/cnv.tsv \
    --sample_info /home/nipt_sample_info.json \
    --wsx_name_pattern "_.*_sorted-dup-marked_statistics.txt$" \
    --clin_comment /resources/ClinicalComment.tsv \
    --cnv_dir /home \
    --cnv_name_pattern "_cnv-summary.tsv"