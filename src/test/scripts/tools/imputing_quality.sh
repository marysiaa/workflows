#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"

DATE=$(date +%d-%m-%Y | sed 's/:/-/g')
mkdir -p /tmp/${DATE};
python $SCRIPT -i $RESOURCES_DIR/txt/imputing-quality-test-input -o /tmp/${DATE}/out.json