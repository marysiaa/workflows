#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/multiqc-stats

python3 $SCRIPT \
  --stats-json $RESOURCES_DIR"/data/json/multiqc_stats_reduced.json" \
  --thresholds $PROJECT_DIR"/src/main/docker/multiqc/multiqc-thresholds.json" \
  --id "test"

mv test_multiqc-stats.json /tmp/test/tools/multiqc-stats
echo "Output: /tmp/test/tools/multiqc-stats/test_multiqc-stats.json"

