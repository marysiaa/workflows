#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/tools/pgx-merge-anno
chmod +x $SCRIPT

$SCRIPT \
  --openpgx-anno $RESOURCES_DIR"data/json/pgx_openpgx_recommendations.json" \
  --pharmgkb-clinical-anno $RESOURCES_DIR"data/json/pgx_pharmgkb_clinical_recommendations.json" \
  --pharmgkb-variant-anno $RESOURCES_DIR"data/json/pgx_pharmgkb_variant_recommendations.json" \
  --output-filename "/tmp/test/tools/pgx-merge-anno/merged.json"
