#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/_test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/vcf-filter-clinical-test
chmod +x $SCRIPT

$SCRIPT \
  --input-vcf "$RESOURCES_DIR/data/vcf/test-clinvar-pathogenic-clinical.vcf.gz" \
  --output-vcf "/tmp/test/tools/vcf-filter-clinical-test/filtered1.vcf.gz"


$SCRIPT \
  --input-vcf "$RESOURCES_DIR/data/vcf/clinvar-low-penetrance-test.vcf.gz" \
  --output-vcf "/tmp/test/tools/vcf-filter-clinical-test/filtered2.vcf.gz"  