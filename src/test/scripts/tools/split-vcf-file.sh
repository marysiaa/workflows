SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_VCF=$RESOURCES_DIR/vcf/sv-annot.vcf.gz

#echo $SCRIPT
#echo $PROJECT_DIR
#echo $RESOURCES_DIR
#echo $TEST_VCF

### help #############################
printf "\n"
printf "Help screen:\n"
printf "************\n"

python3 $SCRIPT -h

### usage ###########################

printf "\n"
printf "Running script: \n"
printf "************\n"

mkdir -p /tmp/test/split-vcf-file
OUTDIR="/tmp/test/split-vcf-file"


python3 $SCRIPT \
       $TEST_VCF -n "test".vcf.gz 20 -d  $OUTDIR

