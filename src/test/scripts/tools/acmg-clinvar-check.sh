#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/acmg-clinvar-check

$SCRIPT \
  --input-csv $RESOURCES_DIR"/data/txt/converted-vcf-to-csv.csv" \
  --output-likelypathogenic "/tmp/test/tools/acmg-clinvar-check/Pathogenic_and_LikelyPathogenic-Uncertain.csv" \
  --output-pathogenic "/tmp/test/tools/acmg-clinvar-check/Pathogenic-Uncertain.csv" \
  --output-benign "/tmp/test/tools/acmg-clinvar-check/Benign-Uncertain.csv" \
  --output-crosstab "/tmp/test/tools/acmg-clinvar-check/Overall_crosstab.csv"