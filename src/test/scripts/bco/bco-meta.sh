#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"

chmod +x $SCRIPT

$SCRIPT "https://gitlab.com/intelliseq/workflows/-/raw/report-acmg@2.2.8/src/main/wdl/tasks/report-acmg/report-acmg.wdl" -p
