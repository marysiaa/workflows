#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/bco-merge

$SCRIPT \
  --input-path $RESOURCES_DIR"/data/json/bco" \
  --pipeline-name "test" \
  --pipeline-version "1.0.0" \
  --output-file "/tmp/test/tools/bco-merge/bco.json"
