#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test/rna-seq/paired-end

$SCRIPT \
  --input-path-to-summary-files $RESOURCES_DIR"/txt/hisat-summary/paired-end" \
  --output-summary-file-name /tmp/test/rna-seq/paired-end/summary



