#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

chmod +x "$SCRIPT"
mkdir -p /tmp/test/tools/check-stranded

$SCRIPT \
  --input_rseqc_log "$RESOURCES_DIR"/data/txt/infer_experiment.txt \
  --output_file /tmp/test/tools/check-stranded/stranded.txt



