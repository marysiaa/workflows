#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test/rna-seq/abund-aggreg

echo "##########TEST_GENE_LEVEL##########"

$SCRIPT \
  --count-files "$RESOURCES_DIR/txt/count-reads/ERR187488.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187493.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187495.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187571.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187580.tsv" \
  --tbl-suffix "gene" \
  --output-file-name "/tmp/test/rna-seq/abund-aggreg/htseq-count-gene-level.tsv"

echo "First and last 5 rows in output file"
head -n 5 /tmp/test/rna-seq/abund-aggreg/htseq-count-gene-level.tsv
tail -n 5 /tmp/test/rna-seq/abund-aggreg/htseq-count-gene-level.tsv

echo "##########TEST_EXON_LEVEL##########"
$SCRIPT \
  --count-files "$RESOURCES_DIR/txt/count-reads/ERR187488.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187493.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187495.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187571.tsv" \
                "$RESOURCES_DIR/txt/count-reads/ERR187580.tsv" \
  --tbl-suffix "exon" \
  --output-file-name "/tmp/test/rna-seq/abund-aggreg/htseq-count-exon-level.tsv"

echo "First and last 5 rows in output file"
head -n 5 /tmp/test/rna-seq/abund-aggreg/htseq-count-exon-level.tsv
tail -n 5 /tmp/test/rna-seq/abund-aggreg/htseq-count-exon-level.tsv
