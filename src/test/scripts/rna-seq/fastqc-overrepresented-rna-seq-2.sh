#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test/rna-seq/overrepresented/second-script

$SCRIPT \
  --input-path-to-overrepresented-files $RESOURCES_DIR"/txt/overrepresented" \
  --output-file-name /tmp/test/rna-seq/overrepresented/second-script/overrepresented



