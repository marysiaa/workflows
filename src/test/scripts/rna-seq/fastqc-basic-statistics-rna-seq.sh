#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test/rna-seq/basic-statistic/paired-end

$SCRIPT \
  --input-path-to-fastqc-data-files $RESOURCES_DIR"/txt/fastqc/paired-end/fastqc-data" \
  --input-path-to-summary-files $RESOURCES_DIR"/txt/fastqc/paired-end/summary-files" \
  --output-file-name /tmp/test/rna-seq/basic-statistic/paired-end/basic-statistic



