#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test

python3 $SCRIPT \
    --input-tsv "${RESOURCES_DIR}/txt/raw-gene-level.tsv" \
    --output-tsv "/tmp/test/gene-level.tsv"