#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test


echo "1. Gene test"
python3 $SCRIPT \
    --input-file-to-annotate "${RESOURCES_DIR}/txt/gene-level.tsv" \
    --input-ensembl-anno-tsv "${RESOURCES_DIR}/txt/small-ensembl-anno.tsv" \
    --ensembl-column-to-merge-on "Gene stable ID" \
    --id-column-to-merge-on "Geneid" \
    --output-file-annotated "/tmp/test/annotated-gene-level.tsv"


echo "2. Transcript test"
python3 $SCRIPT \
    --input-file-to-annotate "${RESOURCES_DIR}/txt/tpm.tsv" \
    --input-ensembl-anno-tsv "${RESOURCES_DIR}/txt/small-transcript-ensembl-anno.tsv" \
    --ensembl-column-to-merge-on "Transcript stable ID version" \
    --id-column-to-merge-on "target_id" \
    --output-file-annotated "/tmp/test/annotated-tpm.tsv"