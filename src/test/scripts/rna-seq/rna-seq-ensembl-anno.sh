#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"

python3 "${SCRIPT}" \
    --input-organism-name 'Human' \
    --input-version '104' \
    --input-genes-anno-list "Chromosome/scaffold name" "Gene start (bp)" "Gene end (bp)" "Transcript stable ID" "Gene name" "Gene type" "Gene Synonym" "Gene description" "Exon stable ID" \
    --input-transcripts-anno-list "Chromosome/scaffold name" "Transcript start (bp)" "Transcript end (bp)" "Gene stable ID" "Transcript stable ID version" "Gene name" "Transcript type" "Gene Synonym" "Gene description" "Exon stable ID" \
    --input-versions-json "${PROJECT_DIR}/resources/ensembl-genes/ensembl-versions.json" \
    --output-dir '/tmp/test/ensembl-rna-seq'

