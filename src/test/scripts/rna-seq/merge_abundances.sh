#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test/rna-seq/merge-abundances-files
chmod +x $SCRIPT

$SCRIPT \
  --tsv-files $RESOURCES_DIR"/txt/first_sample_eff_length.tsv" \
          $RESOURCES_DIR"/txt/second_sample_eff_length.tsv" \
          $RESOURCES_DIR"/txt/first_sample_est_counts.tsv" \
          $RESOURCES_DIR"/txt/second_sample_est_counts.tsv" \
          $RESOURCES_DIR"/txt/first_sample_tpm.tsv" \
          $RESOURCES_DIR"/txt/second_sample_tpm.tsv" \
  --analysis-id "test" \
  --output-file-path "/tmp/test/rna-seq/merge-abundances-files"

echo -e "##########First and last 5 rows in test_eff_length.tsv##########"
head -n 5 /tmp/test/rna-seq/merge-abundances-files/test_eff_length.tsv
tail -n 5 /tmp/test/rna-seq/merge-abundances-files/test_eff_length.tsv

echo ""
echo "##########First and last 5 rows in test_est_counts.tsv##########"
head -n 5 /tmp/test/rna-seq/merge-abundances-files/test_est_counts.tsv
tail -n 5 /tmp/test/rna-seq/merge-abundances-files/test_est_counts.tsv

echo ""
echo "##########First and last 5 rows in test_tpm.tsv##########"
head -n 5 /tmp/test/rna-seq/merge-abundances-files/test_tpm.tsv
tail -n 5 /tmp/test/rna-seq/merge-abundances-files/test_tpm.tsv