#!/bin/bash

AWS_ADDRESS="http://ec2-100-25-40-253.compute-1.amazonaws.com/api/workflows/v1"
SAMPLEID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

ID=$(curl -s -X POST $AWS_ADDRESS \
  -H "accept: application/json" \
  -H "Content-Type: multipart/form-data" \
  -F "workflowUrl=https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/minimal-task/v1.0/minimal.wdl" \
  -F "workflowInputs={\"minimal_workflow.minimal.str\": \"testing string\"}" \
  -F "labels={\"sampleid\":\"$SAMPLEID\"}" \
  -F "workflowType=WDL" | jq -r .id)

echo $ID

STATUS="BEFORE"
while [ ! $STATUS == "Succeeded" ] ; do
    echo "$AWS_ADDRESS/$ID/status"
    STATUS=$(curl -s -X GET "$AWS_ADDRESS/$ID/status" -H "accept: application/json" | jq -r .status)
    echo $STATUS
done

OUTPUT=$(curl -s -X GET "$AWS_ADDRESS/$ID/outputs" -H "accept: application/json" | jq -r '.outputs["minimal_workflow.minimal.out"]')
echo $OUtPUT
