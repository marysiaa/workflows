import pytest
from . import process_docker_images_output

out_str = \
'''intelliseqngs/bwa-mem   broad-institute-hg38_chromosome-wise_1.0.0-chr15   e16ee720ff87    4 days ago          885MB
intelliseqngs/bwa-mem   broad-institute-hg38_1.0.0  b5db3ad3b017        7 days ago          7.13GB
intelliseqngs/bwa-mem   v1.2                                    7a8db6ab6d67        7 months ago        9.26GB'''


def test_some_images():
    expected_output = [
        'intelliseqngs/bwa-mem:broad-institute-hg38_chromosome-wise_1.0.0-chr15',
        'intelliseqngs/bwa-mem:broad-institute-hg38_1.0.0',
        'intelliseqngs/bwa-mem:v1.2'
    ]
    assert process_docker_images_output(out_str) == expected_output


def test_empty_output():
    assert process_docker_images_output("") == []