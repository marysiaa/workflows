import pytest
from . import Docker


@pytest.fixture(scope="module")
def docker_one_observer() -> Docker:
    return Docker('1', {'b'})


@pytest.fixture(scope="module")
def docker_two_observers() -> Docker:
    return Docker('1', {'a', 'b'})


class TestEq():
    def test_eq_one_observer(self, docker_one_observer: Docker):
        assert Docker('1', {'b'}) == docker_one_observer

    def test_docker_two_observers(self, docker_two_observers: Docker):
        assert Docker('1', {'b', 'a'}) == docker_two_observers

    def test_not_equal(self, docker_one_observer: Docker, docker_two_observers: Docker):
        assert docker_one_observer != docker_two_observers

    def test_wrong_type(self, docker_one_observer: Docker):
        assert docker_one_observer != ('1', {'b'})
