import pytest
import pathlib
from . import CantFindTestForWDL
from . import find_tests_for_wdl


def test_no_tests():
    with pytest.raises(CantFindTestForWDL):
        find_tests_for_wdl('src/main/wdl/fake/path.wdl')
