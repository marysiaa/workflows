import pytest
import pathlib
from . import create_wdl_object
from . import WDL


class MockPath(object):
    def __init__(self, attr=None):
        self.attr = attr

    def rglob(self, _):
        return [self.attr] if self.attr else []


def set_up(mocker, monkeypatch):
    monkeypatch.setattr(pathlib, 'Path', mock_pathlib_path)
    mocked_find_tests_for_wdl = mocker.patch('scripts.lib.WDL.find_tests_for_wdl')
    mocked_find_tests_for_wdl.return_value = ['test.sh']
    mocked_os_getcwd = mocker.patch('os.getcwd')
    mocked_os_getcwd.return_value = '.'
    mocked_is_imported_in_the_file = mocker.patch('scripts.lib.WDL.is_imported_in_the_file')
    mocked_is_imported_in_the_file.return_value = True


def mock_pathlib_path(path: str):
    if 'modules' in path:
        return MockPath('/wdl/modules/fake.wdl')
    if 'pipelines' in path:
        return MockPath('/wdl/pipelines/fake.wdl')


def test_pipelines(mocker, monkeypatch):
    set_up(mocker, monkeypatch)
    assert create_wdl_object('docker1', '/wdl/pipelines/fake.wdl', '/fake_dir') == WDL(
        '/wdl/pipelines/fake.wdl', ['test.sh'], 'docker1', [])


def test_modules(mocker, monkeypatch):
    set_up(mocker, monkeypatch)
    # assert create_wdl_object('docker1', '/wdl/modules/fake.wdl', '/fake_dir') == WDL(
    #     '/wdl/modules/fake.wdl', ['test.sh'], 'docker1', [WDL('/wdl/pipelines/fake.wdl', ['test.sh'], None, [])])
    assert create_wdl_object('docker1', '/wdl/modules/fake.wdl', '/fake_dir') == WDL(
        '/wdl/modules/fake.wdl', ['test.sh'], 'docker1', [])


def test_tasks(mocker, monkeypatch):
    set_up(mocker, monkeypatch)
    # assert create_wdl_object('docker1', '/wdl/tasks/fake.wdl', '/fake_dir') == WDL(
    #     '/wdl/tasks/fake.wdl', ['test.sh'], 'docker1', [WDL(
    #         '/wdl/modules/fake.wdl', ['test.sh'], None, [WDL('/wdl/pipelines/fake.wdl', ['test.sh'], None, [])])])
    assert create_wdl_object('docker1', '/wdl/tasks/fake.wdl', '/fake_dir') == WDL(
        '/wdl/tasks/fake.wdl', ['test.sh'], 'docker1', [WDL(
            '/wdl/modules/fake.wdl', ['test.sh'], None, [])])


def test_side_effect(mocker, monkeypatch):
    set_up(mocker, monkeypatch)
    side_dict = {}
    create_wdl_object('docker1', '/wdl/modules/fake.wdl', '/fake_dir', already_produced_objects=side_dict)
    # side_dict_expected = {
    #     '/wdl/pipelines/fake.wdl': WDL('/wdl/pipelines/fake.wdl', ['test.sh'], None, []),
    #     '/wdl/modules/fake.wdl': WDL('/wdl/modules/fake.wdl', ['test.sh'], 'docker1',
    #                                  [WDL('/wdl/pipelines/fake.wdl', ['test.sh'], None, [])])
    # }
    side_dict_expected = {
        '/wdl/modules/fake.wdl': WDL('/wdl/modules/fake.wdl', ['test.sh'], 'docker1', [])
    }
    assert side_dict == side_dict_expected
