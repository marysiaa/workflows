import pytest
from . import WDL


@pytest.fixture(scope="module")
def simple_wdl() -> WDL:
    return WDL('wdl', ['test1'], 'dock', ['obs1'])


@pytest.fixture(scope="module")
def complex_wdl() -> WDL:
    return WDL('wdl', ['test1', 'test2'], 'dock', ['obs1', 'obs2'])


class TestEq():
    def test_eq_simple_wdl(self, simple_wdl: WDL):
        assert WDL('wdl', ['test1'], 'dock', ['obs1']) == simple_wdl

    def test_eq_complex_wdl(self, complex_wdl: WDL):
        assert WDL('wdl', ['test2', 'test1'], 'dock', ['obs2', 'obs1']) == complex_wdl

    def test_ne_simple_wdl(self, simple_wdl: WDL):
        assert WDL('wdl', ['test2'], 'dock', ['obs1']) != simple_wdl

    def test_ne_complex_wdl_tests(self, complex_wdl: WDL):
        assert WDL('wdl', ['test1', 'test1'], 'dock', ['obs2', 'obs1']) != complex_wdl

    def test_ne_complex_wdl_observers(self, complex_wdl: WDL):
        assert WDL('wdl', ['test1', 'test2'], 'dock', ['obs2', 'obs2']) != complex_wdl
