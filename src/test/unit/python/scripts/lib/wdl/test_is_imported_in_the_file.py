import pytest
import tempfile
from . import is_imported_in_the_file

CONTENT = b'''
import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/mobigen-models/latest/mobigen-models.wdl" as mobigen_models_task
import "https://gitlab.com/intelliseq/workflows/-/raw/dev/src/main/wdl/tasks/mobigen_plot/latest/mobigen_plot.wdl" as mobigen_plot_task
 #import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/qc-fq-mapping/latest/qc-fq-mapping.wdl" as qc_fq_mapping_workflow
'''


@pytest.fixture(scope="module")
def temporary_file_with_string():
    with tempfile.NamedTemporaryFile(delete=False) as f:
        f.write(CONTENT)
    yield f.name
    f.close()


def test_is_imported_1(temporary_file_with_string: str):
    assert is_imported_in_the_file('src/main/wdl/tasks/mobigen-models/latest/mobigen-models.wdl',
                                   temporary_file_with_string)


def test_is_imported_2(temporary_file_with_string: str):
    assert is_imported_in_the_file('src/main/wdl/tasks/mobigen_plot/latest/mobigen_plot.wdl',
                                   temporary_file_with_string)


def test_is_not_imported(temporary_file_with_string: str):
    assert not is_imported_in_the_file('src/main/wdl/tasks/mobigen_plot/latest/fake.wdl', temporary_file_with_string)


def test_commented_line_is_not_imported(temporary_file_with_string: str):
    assert not is_imported_in_the_file('src/main/wdl/tasks/qc-fq-mapping/latest/qc-fq-mapping.wdl', temporary_file_with_string)
