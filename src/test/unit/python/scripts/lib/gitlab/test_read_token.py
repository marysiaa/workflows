import pytest
import tempfile
import json
from . import read_token

CONTENT = {
    "info": "someinfo",
    "token": "imatoken"
}


@pytest.fixture(scope="module")
def temporary_file():
    with tempfile.NamedTemporaryFile(delete=False) as f:
        binary_content = json.dumps(CONTENT).encode()
        f.write(binary_content)
    yield f.name
    f.close()


def test_proper_token(temporary_file: str):
    assert read_token(temporary_file) == 'imatoken'
