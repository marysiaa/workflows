import os
import sys

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 8)[0])

from . import get_pipelines_generator
from . import NoMoreDataException
from scripts.lib import gitlab

sample_gitlab_pipelines = [
            {'id': 130607479, 'sha': '859a2293d2993eec842a0fe6ed4e2bf824258b02', 'ref': 'acmg-readme',
             'status': 'success'},
            {'id': 130607007, 'sha': 'dc1f8308740f5814d9e194b9ae29f1f4c80d070b', 'ref': 'acmg-readme',
             'status': 'failed'},
            {'id': 130606855, 'sha': '3d76e90bfe47e2fbf0acb34ea4d4319c45032710', 'ref': 'acmg-readme',
             'status': 'failed'}
        ]


def test(monkeypatch):
    def mockreturn(_, __, page:int, ___):
        if page == 1:
            return sample_gitlab_pipelines[:2]
        elif page == 2:
            return [sample_gitlab_pipelines[2]]
        else:
            raise NoMoreDataException
    monkeypatch.setattr(gitlab, "get_pipelines", mockreturn)
    assert(list(get_pipelines_generator(None, None, None)) == sample_gitlab_pipelines)
