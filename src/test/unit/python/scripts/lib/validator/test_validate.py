import pytest
import json
import requests
from typing import Dict
from . import validate


class MockResponse(object):
    def __init__(self, code:int, message:str):
        self.status_code = code
        self.__message = message

    def json(self) -> Dict:
        return {'message':self.__message}


def set_up(monkeypatch, proper=True):
    if proper:
        monkeypatch.setattr(requests, 'get', mock_get_proper)
    else:
        monkeypatch.setattr(requests, 'get', mock_get_improper)


def mock_get_proper(*args):
    return MockResponse(200, "")


def mock_get_improper(*args):
    return MockResponse(500, "Malformed meta yaml")


def test_proper(monkeypatch):
    set_up(monkeypatch, proper=True)
    assert json.loads(validate('url_with_proper_meta')) == {"code": 200, "error_message": ""}


def test_improper(monkeypatch):
    set_up(monkeypatch, proper=False)
    assert json.loads(validate('url_with_proper_meta')) == {"code": 500, "error_message": "Malformed meta yaml"}
