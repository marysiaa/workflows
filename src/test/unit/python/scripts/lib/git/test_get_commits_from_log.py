import pytest
from . import get_commits_from_log

log = \
    '''commit 330d19450085e6f18a757b2159ee422b81a128d6 (HEAD -> test_temp, origin/test_temp)
Author: wojciech-galan <wojciech.galan@gmail.com>
Date:   Wed Feb 26 11:22:29 2020 +0100

    checking message from server

commit f25d319d69640c335d874162c1b8731d67af9da9 (origin/test, test)
Author: wojciech-galan <wojciech.galan@gmail.com>
Date:   Wed Feb 26 09:33:44 2020 +0100

    getting previous successfull commit from gitlab pipelines api

commit 5ee8d909befcde9f5941c43f2b848b258b602b02
Author: wojciech-galan <wojciech.galan@gmail.com>
Date:   Wed Feb 26 08:32:05 2020 +0100

    test log
    '''


def test_proper_log():
    assert get_commits_from_log(log) == {'330d19450085e6f18a757b2159ee422b81a128d6',
                                         'f25d319d69640c335d874162c1b8731d67af9da9',
                                         '5ee8d909befcde9f5941c43f2b848b258b602b02'}
