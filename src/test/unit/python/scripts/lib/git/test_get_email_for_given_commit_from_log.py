import pytest
from . import get_email_for_given_commit_from_log

log_proper = '''commit d593322f75590a6cf0ab53ddbf1f882d1a85167a (HEAD -> test)
Author: wojciech-galan <wojciech.galan@gmail.com>
Date:   Thu Feb 13 21:28:05 2020 +0100

    renamed

commit 9d5ff17429a4d9320c2846f7596d43979f26175f
Author: Mateusz Marynowski <mateusz.marynowski97@gmail.com>
Date:   Thu Feb 13 15:12:19 2020 +0000

    Update vcf-anno-ghr.wdl
'''


def test_proper_log_first_commit():
    assert get_email_for_given_commit_from_log('d593322f75590a6cf0ab53ddbf1f882d1a85167a',
                                               log_proper) == 'wojciech.galan@gmail.com'


def test_proper_log_second_commit():
    assert get_email_for_given_commit_from_log('9d5ff17429a4d9320c2846f7596d43979f26175f',
                                               log_proper) == 'mateusz.marynowski97@gmail.com'


def test_commit_not_in_the_log():
    with pytest.raises(AttributeError):
        get_email_for_given_commit_from_log('0de7481057e5a418f498d92d539550a14629c155', log_proper)


def test_commit_string_only_partially_match():
    with pytest.raises(AttributeError):
        get_email_for_given_commit_from_log('d593322f75590a6cf0ab53ddbf1f882d1a85167', log_proper)
