import os
import sys

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 6)[0])

from scripts.lib.my_sh_utils import get_output_from_command_unsafe
