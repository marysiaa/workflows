import pytest
import subprocess
from . import get_output_from_command_unsafe


class MockCompletedProcess(object):
    stdout = b' output '


def test_command_output(monkeypatch):
    def mock_run(*args, **kwargs) -> str:
        return MockCompletedProcess()

    monkeypatch.setattr(subprocess, 'run', mock_run)
    result = get_output_from_command_unsafe('')
    assert result == 'output'
