import os
import sys

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 6)[0])

from scripts.server_for_testing_purposes import validate_input_keys, ImproperUserInputException
