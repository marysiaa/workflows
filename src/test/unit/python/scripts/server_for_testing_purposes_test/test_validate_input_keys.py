import pytest
from typing import Dict
from . import validate_input_keys
from . import ImproperUserInputException


@pytest.mark.parametrize("input", [{}, {'branch': ''}])
def test_improper_input(input: Dict[str, str]):
    with pytest.raises(ImproperUserInputException):
        validate_input_keys(input)


def test_proper_input():
    validate_input_keys({'commit': ""})
