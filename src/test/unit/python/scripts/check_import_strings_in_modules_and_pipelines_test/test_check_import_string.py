import re
import pytest
from . import check_import_string, ImproperImportStringError

PATTERN = re.compile('import "https://gitlab.com/intelliseq/workflows/(-/)?raw/(dev|master)/')


@pytest.mark.parametrize("s", [
    'import "https://gitlab.com/intelliseq/workflows/-/raw/dev/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl',
    'import "https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl',
    'import "https://gitlab.com/intelliseq/workflows/-/raw/master/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl',
    'import "https://gitlab.com/intelliseq/workflows/raw/master/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl'
])
def test_proper_import_string(s: str):
    check_import_string(s, '/path/to/a/file', PATTERN)


@pytest.mark.parametrize("s", [
    'import "https://gitlab.com/intelliseq/workflows/-/raw/bad_branch/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl',
    'import "https://gitlab.com/intelliseq/workflows/raw/bad_branch/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl',
    'import "https://gitlab.com/intelliseq/workflows/-/raw/bad_branch/master/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl',
    'import "https://gitlab.com/intelliseq/workflows/raw/bad_branch/master/src/main/wdl/tasks/vcf-anno-ghr/latest/vcf-anno-ghr.wdl'
])
def test_improper_import_string(s: str):
    with pytest.raises(ImproperImportStringError):
        check_import_string(s, '/path/to/a/file', PATTERN)
