import pytest
import warnings
from . import deal_with_complex_values


def test_one_element_tuple():
    assert deal_with_complex_values('key', ('value', ), set()) == ["('value',)"]


def test_two_elements_tuple():
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        assert deal_with_complex_values('key', ('value', None), set()) == ["('value', None)"]


def test_simple_dict_one_pair_1():
    assert deal_with_complex_values('key', {'d_key':'d_value'}, set()) == ["d_value"]


def test_simple_dict_one_pair_2():
    assert deal_with_complex_values('key', {'d_key':('d_value', )}, set()) == ["('d_value',)"]


def test_simple_dict_two_pairs_1():
    assert deal_with_complex_values('key', {'d_key1':'d_value1', 'd_key2':'d_value2'}, set()) == ["d_value1", "d_value2"]


def test_simple_dict_two_pairs_2():
    assert deal_with_complex_values('key', {'d_key1':('d_value1', ), 'd_key2':('d_value2', )}, set()) == ["('d_value1',)", "('d_value2',)"]


def test_complex_dict_one_pair_1():
    assert deal_with_complex_values('key', {'d_key':{'sub_d_key':'sub_d_value'}}, set()) == ["sub_d_value"]


def test_str():
    with pytest.raises(TypeError):
        deal_with_complex_values('key', 'value')