import os
import sys

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 9)[0])

from src.main.scripts.tools.vcf_to_json_or_tsv import *