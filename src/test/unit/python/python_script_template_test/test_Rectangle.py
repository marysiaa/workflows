import pytest
from . import Rectangle


@pytest.mark.parametrize("a, b", [(1, 2), (1., 2.), (1, 2.), (1., 2.)])
def test_init_proper_argument_type(a, b):
    Rectangle(a, b)


@pytest.mark.parametrize("a, b", [('1', 2), (1., []), (-1., 2), (1., -2), (True, 1), (1, True)])
def test_init_improper_argument_type(a, b):
    with pytest.raises(AssertionError):
        Rectangle(a, b)


# so called fixture
@pytest.fixture
def sample_rectangle() -> Rectangle:
    return Rectangle(5, 4)


def test_perimeter(sample_rectangle: Rectangle):
    assert sample_rectangle.perimeter() == 18


def test_area(sample_rectangle: Rectangle):
    assert sample_rectangle.area() == 20
