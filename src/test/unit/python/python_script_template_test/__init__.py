import os
import sys

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 6)[0])

from docs.templates.python_script_template import fib, RsInfo, Rectangle
