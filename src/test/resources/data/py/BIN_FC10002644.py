from polygenic.seqql.score import PolygenicRiskScore
from polygenic.seqql.score import ModelData
from polygenic.seqql.category import QuantitativeCategory

model = PolygenicRiskScore(
categories=[
QuantitativeCategory(from_=-0.08008984, to=-0.10470959792991272, category_name='Reduced'),
QuantitativeCategory(from_=-0.10470959792991272, to=0.04894168835991272, category_name='Average'),
QuantitativeCategory(from_=0.04894168835991272, to=0.020295657, category_name='Increased')
],
snips_and_coefficients={
'rs10800598': ModelData(effect_allele='C', coeff_value= 1.133102e-02),
'rs1051858': ModelData(effect_allele='G', coeff_value= 6.627847e-03),
'rs16969968': ModelData(effect_allele='A', coeff_value=-6.929218e-02),
'rs146186553': ModelData(effect_allele='A', coeff_value=-7.175181e-03),
'rs3130453': ModelData(effect_allele='T', coeff_value=-1.007512e-03),
'rs3021518': ModelData(effect_allele='A', coeff_value=-2.614967e-03),
'rs3739261': ModelData(effect_allele='C', coeff_value= 8.361500e-04),
'rs237520': ModelData(effect_allele='G', coeff_value= 1.500640e-03)},
model_type='beta'
)
description = {
    "mean": -0.027883954784999998,
    "sd": 0.04670251862912627,
    "min": -0.08008984,
    "max": 0.020295657
}