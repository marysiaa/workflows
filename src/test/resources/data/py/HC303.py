from polygenic.seqql.score import PolygenicRiskScore
from polygenic.seqql.score import ModelData
from polygenic.seqql.category import QuantitativeCategory

model = PolygenicRiskScore(
categories=[
QuantitativeCategory(from_=-3.1879015456499973, to=-1.2230928782475319, category_name='Reduced'),
QuantitativeCategory(from_=-1.2230928782475319, to=0.33589053250613154, category_name='Average'),
QuantitativeCategory(from_=0.33589053250613154, to=3.7646308529199994, category_name='Increased')
],
snips_and_coefficients={
'rs3748816': ModelData(effect_allele='G', coeff_value=-6.426879e-02),
'rs12727642': ModelData(effect_allele='A', coeff_value= 2.331283e-03),
'rs12405873': ModelData(effect_allele='T', coeff_value= 6.788394e-03),
'rs694214': ModelData(effect_allele='T', coeff_value=-1.761631e-02),
'rs196432': ModelData(effect_allele='A', coeff_value=-8.653850e-03),
'rs1880418': ModelData(effect_allele='G', coeff_value=-7.626590e-03),
'rs6691768': ModelData(effect_allele='A', coeff_value= 2.614848e-02),
'rs11208062': ModelData(effect_allele='A', coeff_value=-8.443953e-03),
'rs560827': ModelData(effect_allele='A', coeff_value=-2.788996e-03),
'rs2336645': ModelData(effect_allele='G', coeff_value= 1.736051e-02),
'rs7545406': ModelData(effect_allele='T', coeff_value= 9.805414e-03),
'rs1055935': ModelData(effect_allele='G', coeff_value= 7.688400e-04),
'rs11264498': ModelData(effect_allele='G', coeff_value= 2.044232e-02),
'rs2778009': ModelData(effect_allele='T', coeff_value=-5.394817e-03),
'rs863362': ModelData(effect_allele='T', coeff_value=-1.343838e-03),
'rs857827': ModelData(effect_allele='G', coeff_value=-1.350156e-02),
'rs864537': ModelData(effect_allele='G', coeff_value=-2.823373e-02),
'rs2056626': ModelData(effect_allele='G', coeff_value=-7.413840e-03),
'rs12118303': ModelData(effect_allele='C', coeff_value=-7.305451e-03),
'rs3131332': ModelData(effect_allele='T', coeff_value=-4.735286e-03),
'rs1323295': ModelData(effect_allele='T', coeff_value= 7.229936e-02),
'rs296547': ModelData(effect_allele='C', coeff_value= 3.523846e-02),
'rs17615': ModelData(effect_allele='A', coeff_value=-1.665490e-02),
'rs12123877': ModelData(effect_allele='A', coeff_value=-1.794020e-03),
'rs1538982': ModelData(effect_allele='T', coeff_value=-1.156792e-02),
'rs3003303': ModelData(effect_allele='G', coeff_value= 4.962438e-03),
'rs12759395': ModelData(effect_allele='A', coeff_value= 1.721948e-03),
'rs1574269': ModelData(effect_allele='T', coeff_value= 4.087176e-03),
'rs947474': ModelData(effect_allele='A', coeff_value= 1.472150e-02),
'rs2505323': ModelData(effect_allele='G', coeff_value= 2.315946e-02),
'rs1053266': ModelData(effect_allele='T', coeff_value= 6.161571e-04),
'rs10998957': ModelData(effect_allele='C', coeff_value= 5.875727e-03),
'rs11592462': ModelData(effect_allele='G', coeff_value=-9.103483e-03),
'rs6586146': ModelData(effect_allele='C', coeff_value= 3.614878e-02),
'rs4751996': ModelData(effect_allele='G', coeff_value= 1.776554e-02),
'rs4562730': ModelData(effect_allele='A', coeff_value=-6.490012e-03),
'rs2246614': ModelData(effect_allele='G', coeff_value= 2.807418e-03),
'rs4391795': ModelData(effect_allele='T', coeff_value=-5.367603e-03),
'rs11041828': ModelData(effect_allele='T', coeff_value=-1.602446e-02),
'rs757081': ModelData(effect_allele='G', coeff_value= 2.346385e-03),
'rs2702661': ModelData(effect_allele='A', coeff_value= 5.864080e-03),
'rs1867116': ModelData(effect_allele='G', coeff_value= 2.185319e-02),
'rs10833243': ModelData(effect_allele='C', coeff_value= 3.328067e-03),
'rs3924551': ModelData(effect_allele='G', coeff_value=-1.901273e-03),
'rs11456363': ModelData(effect_allele='CT', coeff_value=-1.880630e-02),
'rs12796683': ModelData(effect_allele='T', coeff_value= 1.935934e-02),
'rs544864': ModelData(effect_allele='T', coeff_value= 3.400567e-02),
'rs4938941': ModelData(effect_allele='G', coeff_value= 1.446154e-03),
'rs108499': ModelData(effect_allele='T', coeff_value=-2.888699e-02),
'rs11231397': ModelData(effect_allele='G', coeff_value=-2.683633e-02),
'rs11601325': ModelData(effect_allele='A', coeff_value=-7.452157e-04),
'rs2276118': ModelData(effect_allele='T', coeff_value= 9.306210e-03),
'rs11236460': ModelData(effect_allele='A', coeff_value= 1.384722e-03),
'rs645065': ModelData(effect_allele='A', coeff_value= 4.066858e-04),
'rs7126901': ModelData(effect_allele='C', coeff_value=-2.658103e-03),
'rs498872': ModelData(effect_allele='G', coeff_value= 3.779095e-03),
'rs11216995': ModelData(effect_allele='A', coeff_value=-1.511459e-02),
'rs2060008': ModelData(effect_allele='T', coeff_value=-1.241129e-02),
'rs3088241': ModelData(effect_allele='G', coeff_value=-2.679377e-04),
'rs1919268': ModelData(effect_allele='A', coeff_value= 7.932436e-04),
'rs7115037': ModelData(effect_allele='C', coeff_value=-4.243759e-03),
'rs654723': ModelData(effect_allele='A', coeff_value= 4.950816e-03),
'rs10893918': ModelData(effect_allele='T', coeff_value= 3.624955e-04),
'rs1793792': ModelData(effect_allele='G', coeff_value=-5.723397e-03),
'rs141823469': ModelData(effect_allele='TC', coeff_value= 6.585333e-03),
'rs112979434': ModelData(effect_allele='ATAAGT', coeff_value=-1.511697e-02),
'rs113710768': ModelData(effect_allele='T', coeff_value=-1.001808e-02),
'rs1852450': ModelData(effect_allele='A', coeff_value=-7.021370e-03),
'rs4963805': ModelData(effect_allele='T', coeff_value= 9.608920e-03),
'rs7137522': ModelData(effect_allele='C', coeff_value=-3.726564e-02),
'rs2732441': ModelData(effect_allele='G', coeff_value=-4.756398e-03),
'rs638043': ModelData(effect_allele='T', coeff_value= 1.830816e-03),
'rs12830313': ModelData(effect_allele='C', coeff_value= 2.415704e-03),
'rs60331076': ModelData(effect_allele='T', coeff_value=-6.670730e-03),
'rs12422853': ModelData(effect_allele='A', coeff_value= 1.236624e-02),
'rs3184504': ModelData(effect_allele='C', coeff_value=-1.481494e-01),
'rs15895': ModelData(effect_allele='G', coeff_value=-8.356025e-03),
'rs4766957': ModelData(effect_allele='C', coeff_value=-1.710058e-02),
'rs697793': ModelData(effect_allele='A', coeff_value= 1.397711e-02),
'rs6490859': ModelData(effect_allele='G', coeff_value=-4.094445e-03),
'rs172179': ModelData(effect_allele='C', coeff_value= 1.666266e-02),
'rs2234216': ModelData(effect_allele='C', coeff_value=-1.843549e-02),
'rs1333010': ModelData(effect_allele='A', coeff_value=-3.593895e-02),
'rs7995648': ModelData(effect_allele='T', coeff_value= 9.279532e-03),
'rs17277221': ModelData(effect_allele='C', coeff_value=-2.232562e-02),
'rs1953558': ModelData(effect_allele='C', coeff_value=-9.734001e-04),
'rs1242926': ModelData(effect_allele='T', coeff_value= 7.703140e-04),
'rs2231798': ModelData(effect_allele='C', coeff_value= 4.585674e-02),
'rs1278917': ModelData(effect_allele='C', coeff_value=-2.575310e-02),
'rs10129645': ModelData(effect_allele='C', coeff_value= 8.189811e-03),
'rs6572579': ModelData(effect_allele='T', coeff_value= 1.270060e-02),
'rs1609698': ModelData(effect_allele='G', coeff_value= 3.079741e-02),
'rs3829765': ModelData(effect_allele='A', coeff_value=-1.777758e-02),
'rs960070': ModelData(effect_allele='C', coeff_value=-2.669517e-02),
'rs4899260': ModelData(effect_allele='T', coeff_value= 2.174451e-02),
'rs2270424': ModelData(effect_allele='A', coeff_value=-1.691310e-02),
'rs2286913': ModelData(effect_allele='A', coeff_value= 5.934085e-03),
'rs4533197': ModelData(effect_allele='T', coeff_value= 2.112327e-02),
'rs8006817': ModelData(effect_allele='A', coeff_value= 2.617672e-03),
'rs9671369': ModelData(effect_allele='A', coeff_value=-3.170761e-02),
'rs10133464': ModelData(effect_allele='C', coeff_value=-1.244809e-02),
'rs691': ModelData(effect_allele='T', coeff_value= 3.103129e-04),
'rs11072021': ModelData(effect_allele='A', coeff_value=-1.921964e-03),
'rs1036938': ModelData(effect_allele='G', coeff_value=-1.154977e-03),
'rs289470': ModelData(effect_allele='C', coeff_value=-1.740104e-02),
'rs863980': ModelData(effect_allele='T', coeff_value=-1.391914e-02),
'rs12931602': ModelData(effect_allele='G', coeff_value=-1.191319e-02),
'rs17325092': ModelData(effect_allele='C', coeff_value= 3.860842e-03),
'rs12596308': ModelData(effect_allele='T', coeff_value= 1.705309e-02),
'rs4325560': ModelData(effect_allele='G', coeff_value= 2.613055e-03),
'rs4888982': ModelData(effect_allele='C', coeff_value=-1.595220e-02),
'rs11149569': ModelData(effect_allele='T', coeff_value=-2.897869e-03),
'rs8057031': ModelData(effect_allele='G', coeff_value= 3.129283e-03),
'rs36075163': ModelData(effect_allele='C', coeff_value= 5.050129e-03),
'rs6502542': ModelData(effect_allele='T', coeff_value= 5.204525e-04),
'rs3215407': ModelData(effect_allele='T', coeff_value= 1.960079e-02),
'rs11650083': ModelData(effect_allele='A', coeff_value=-1.657497e-02),
'rs11870104': ModelData(effect_allele='C', coeff_value= 1.120968e-02),
'rs17663499': ModelData(effect_allele='T', coeff_value=-3.191853e-04),
'rs17663882': ModelData(effect_allele='G', coeff_value=-9.238512e-03),
'rs2072279': ModelData(effect_allele='A', coeff_value= 3.720665e-03),
'rs3751993': ModelData(effect_allele='T', coeff_value= 1.540367e-02),
'rs9907704': ModelData(effect_allele='A', coeff_value=-1.947000e-03),
'rs9901901': ModelData(effect_allele='C', coeff_value= 1.467182e-02),
'rs2230553': ModelData(effect_allele='G', coeff_value=-1.322797e-02),
'rs4602': ModelData(effect_allele='C', coeff_value=-1.344941e-03),
'rs1122326': ModelData(effect_allele='C', coeff_value= 2.159147e-03),
'rs72833110': ModelData(effect_allele='G', coeff_value=-8.647947e-03),
'rs2326017': ModelData(effect_allele='T', coeff_value= 2.509001e-02),
'rs17746153': ModelData(effect_allele='G', coeff_value=-1.711795e-03),
'rs12942924': ModelData(effect_allele='T', coeff_value= 1.259914e-02),
'rs2716173': ModelData(effect_allele='G', coeff_value=-2.990172e-03),
'rs9038': ModelData(effect_allele='C', coeff_value=-3.983828e-03),
'rs2377301': ModelData(effect_allele='T', coeff_value=-1.815842e-02),
'rs9949149': ModelData(effect_allele='A', coeff_value= 4.692807e-03),
'rs3760551': ModelData(effect_allele='A', coeff_value=-2.169646e-02),
'rs1893217': ModelData(effect_allele='G', coeff_value= 4.881048e-02),
'rs12959354': ModelData(effect_allele='T', coeff_value=-4.240902e-04),
'rs62095588': ModelData(effect_allele='A', coeff_value=-2.018368e-02),
'rs2852748': ModelData(effect_allele='G', coeff_value= 6.209271e-03),
'rs2289519': ModelData(effect_allele='C', coeff_value=-1.659126e-02),
'rs1455555': ModelData(effect_allele='G', coeff_value=-7.706885e-03),
'rs4940595': ModelData(effect_allele='G', coeff_value= 1.279963e-03),
'rs763361': ModelData(effect_allele='C', coeff_value=-5.074490e-02),
'rs660328': ModelData(effect_allele='A', coeff_value=-1.092056e-02),
'rs12975183': ModelData(effect_allele='C', coeff_value=-1.470493e-02),
'rs4804833': ModelData(effect_allele='G', coeff_value=-5.533191e-04),
'rs968502': ModelData(effect_allele='C', coeff_value= 7.589340e-03),
'rs3745340': ModelData(effect_allele='T', coeff_value=-1.158646e-03),
'rs11554159': ModelData(effect_allele='A', coeff_value= 4.154049e-02),
'rs268687': ModelData(effect_allele='C', coeff_value= 2.743705e-02),
'rs3760627': ModelData(effect_allele='C', coeff_value=-1.788500e-02),
'rs2286756': ModelData(effect_allele='T', coeff_value=-2.528218e-02),
'rs919364': ModelData(effect_allele='A', coeff_value= 1.609671e-02),
'rs7250053': ModelData(effect_allele='T', coeff_value= 2.219209e-03),
'rs10423424': ModelData(effect_allele='G', coeff_value= 1.927337e-02),
'rs306506': ModelData(effect_allele='C', coeff_value= 2.681220e-02),
'rs4668562': ModelData(effect_allele='A', coeff_value= 1.238171e-02),
'rs1525063': ModelData(effect_allele='C', coeff_value= 5.537110e-03),
'rs711244': ModelData(effect_allele='T', coeff_value= 7.724076e-03),
'rs6718520': ModelData(effect_allele='G', coeff_value=-4.023014e-04),
'rs4148211': ModelData(effect_allele='G', coeff_value= 1.961646e-02),
'rs895632': ModelData(effect_allele='C', coeff_value=-1.921622e-02),
'rs12470147': ModelData(effect_allele='C', coeff_value= 5.885054e-04),
'rs10188217': ModelData(effect_allele='C', coeff_value= 1.148738e-02),
'rs11904379': ModelData(effect_allele='T', coeff_value= 8.638917e-03),
'rs9309413': ModelData(effect_allele='C', coeff_value= 1.403010e-02),
'rs2305160': ModelData(effect_allele='G', coeff_value= 3.868250e-03),
'rs917997': ModelData(effect_allele='C', coeff_value=-2.691499e-02),
'rs837854': ModelData(effect_allele='C', coeff_value= 2.347661e-02),
'rs816884': ModelData(effect_allele='G', coeff_value= 1.982902e-02),
'rs2116665': ModelData(effect_allele='A', coeff_value=-2.173418e-02),
'rs2672657': ModelData(effect_allele='T', coeff_value= 1.566881e-03),
'rs13010713': ModelData(effect_allele='G', coeff_value= 2.984586e-02),
'rs55961288': ModelData(effect_allele='A', coeff_value=-8.387189e-03),
'rs13022344': ModelData(effect_allele='T', coeff_value=-1.719908e-02),
'rs10931983': ModelData(effect_allele='A', coeff_value=-5.282604e-03),
'rs7420136': ModelData(effect_allele='T', coeff_value=-2.019775e-02),
'rs4675374': ModelData(effect_allele='C', coeff_value=-1.633249e-02),
'rs7349269': ModelData(effect_allele='G', coeff_value=-1.800613e-02),
'rs2034648': ModelData(effect_allele='A', coeff_value=-1.713854e-02),
'rs13006939': ModelData(effect_allele='T', coeff_value= 5.183907e-03),
'rs6084383': ModelData(effect_allele='C', coeff_value= 6.706377e-03),
'rs1999663': ModelData(effect_allele='C', coeff_value=-1.510960e-02),
'rs738703': ModelData(effect_allele='T', coeff_value=-9.567487e-03),
'rs4358188': ModelData(effect_allele='A', coeff_value= 2.173944e-03),
'rs3209183': ModelData(effect_allele='A', coeff_value= 3.054739e-03),
'rs11552145': ModelData(effect_allele='A', coeff_value= 2.810458e-02),
'rs8957': ModelData(effect_allele='T', coeff_value=-4.025173e-02),
'rs2822919': ModelData(effect_allele='A', coeff_value=-2.712289e-03),
'rs73212037': ModelData(effect_allele='A', coeff_value=-9.504681e-04),
'rs370850': ModelData(effect_allele='T', coeff_value=-1.210705e-02),
'rs1892650': ModelData(effect_allele='A', coeff_value= 5.121932e-03),
'rs35640344': ModelData(effect_allele='T', coeff_value= 1.897096e-02),
'rs15661': ModelData(effect_allele='A', coeff_value= 8.185299e-03),
'rs11203203': ModelData(effect_allele='A', coeff_value= 4.569624e-04),
'rs1061325': ModelData(effect_allele='C', coeff_value= 3.283451e-03),
'rs5996763': ModelData(effect_allele='C', coeff_value=-1.522757e-02),
'rs563325': ModelData(effect_allele='G', coeff_value= 2.722124e-02),
'rs132549': ModelData(effect_allele='T', coeff_value=-7.154382e-03),
'rs737976': ModelData(effect_allele='T', coeff_value=-5.957649e-04),
'rs2006771': ModelData(effect_allele='A', coeff_value=-2.400328e-03),
'rs11089806': ModelData(effect_allele='A', coeff_value= 1.487407e-02),
'rs6007243': ModelData(effect_allele='A', coeff_value=-1.665206e-02),
'rs5764698': ModelData(effect_allele='T', coeff_value= 8.866268e-03),
'rs6007807': ModelData(effect_allele='G', coeff_value=-9.168394e-03),
'rs9628315': ModelData(effect_allele='T', coeff_value= 1.103592e-02),
'rs140524': ModelData(effect_allele='T', coeff_value=-2.054661e-02),
'rs55708684': ModelData(effect_allele='G', coeff_value= 2.856971e-03),
'rs708567': ModelData(effect_allele='T', coeff_value=-2.979238e-02),
'rs9830798': ModelData(effect_allele='T', coeff_value=-1.088501e-02),
'rs9990362': ModelData(effect_allele='G', coeff_value=-9.177112e-04),
'rs1449694': ModelData(effect_allele='T', coeff_value=-2.039019e-03),
'rs7632357': ModelData(effect_allele='G', coeff_value= 1.556720e-03),
'rs3203777': ModelData(effect_allele='A', coeff_value=-2.505038e-02),
'rs6795970': ModelData(effect_allele='G', coeff_value= 3.319281e-03),
'rs2133073': ModelData(effect_allele='T', coeff_value= 4.653217e-03),
'rs11713101': ModelData(effect_allele='T', coeff_value= 2.441986e-03),
'rs3732530': ModelData(effect_allele='A', coeff_value= 1.103304e-02),
'rs12491849': ModelData(effect_allele='T', coeff_value=-3.253243e-02),
'rs678': ModelData(effect_allele='T', coeff_value= 6.668409e-03),
'rs12631451': ModelData(effect_allele='G', coeff_value=-1.378650e-02),
'rs902321': ModelData(effect_allele='G', coeff_value= 2.685890e-02),
'rs12488576': ModelData(effect_allele='G', coeff_value= 1.790173e-03),
'rs9826076': ModelData(effect_allele='T', coeff_value= 3.300657e-03),
'rs820555': ModelData(effect_allele='G', coeff_value=-4.466494e-04),
'rs11714850': ModelData(effect_allele='G', coeff_value=-5.853471e-03),
'rs6785504': ModelData(effect_allele='T', coeff_value= 9.189778e-05),
'rs9438': ModelData(effect_allele='C', coeff_value= 3.400686e-02),
'rs8665': ModelData(effect_allele='A', coeff_value=-2.597232e-03),
'rs73170828': ModelData(effect_allele='A', coeff_value= 1.181781e-02),
'rs3911714': ModelData(effect_allele='G', coeff_value=-2.544101e-03),
'rs13096767': ModelData(effect_allele='C', coeff_value=-8.793942e-03),
'rs7617348': ModelData(effect_allele='C', coeff_value=-9.147436e-03),
'rs6443454': ModelData(effect_allele='T', coeff_value=-3.407872e-02),
'rs9851967': ModelData(effect_allele='T', coeff_value=-8.800174e-04),
'rs11707807': ModelData(effect_allele='G', coeff_value=-2.826901e-02),
'rs1464510': ModelData(effect_allele='A', coeff_value= 6.757359e-02),
'rs2246901': ModelData(effect_allele='A', coeff_value= 1.558602e-02),
'rs16837960': ModelData(effect_allele='A', coeff_value= 3.291983e-04),
'rs6820230': ModelData(effect_allele='T', coeff_value=-8.643325e-03),
'rs1384837': ModelData(effect_allele='T', coeff_value=-8.904563e-03),
'rs987399': ModelData(effect_allele='G', coeff_value= 4.868324e-02),
'rs2646357': ModelData(effect_allele='G', coeff_value= 3.036474e-03),
'rs59615456': ModelData(effect_allele='C', coeff_value= 1.788749e-03),
'rs2305339': ModelData(effect_allele='A', coeff_value= 1.367139e-02),
'rs6853742': ModelData(effect_allele='T', coeff_value= 3.950707e-03),
'rs2306949': ModelData(effect_allele='A', coeff_value=-9.190208e-03),
'rs7664835': ModelData(effect_allele='C', coeff_value=-5.482274e-03),
'rs2589514': ModelData(effect_allele='T', coeff_value=-1.971128e-02),
'rs9995879': ModelData(effect_allele='T', coeff_value=-1.279192e-03),
'rs3733197': ModelData(effect_allele='A', coeff_value=-2.943388e-02),
'rs34135604': ModelData(effect_allele='A', coeff_value=-4.166640e-02),
'rs6822844': ModelData(effect_allele='T', coeff_value=-9.848614e-02),
'rs6816198': ModelData(effect_allele='T', coeff_value= 1.296514e-03),
'rs2070951': ModelData(effect_allele='C', coeff_value=-8.619194e-03),
'rs3733391': ModelData(effect_allele='A', coeff_value= 1.189639e-03),
'rs4690370': ModelData(effect_allele='C', coeff_value= 3.126104e-03),
'rs8180173': ModelData(effect_allele='G', coeff_value=-1.817915e-02),
'rs1508855': ModelData(effect_allele='G', coeff_value= 1.618920e-04),
'rs4235598': ModelData(effect_allele='T', coeff_value= 1.405534e-02),
'rs971715': ModelData(effect_allele='A', coeff_value=-2.103015e-03),
'rs3195676': ModelData(effect_allele='T', coeff_value=-1.346992e-02),
'rs4371784': ModelData(effect_allele='G', coeff_value=-9.520737e-03),
'rs7730288': ModelData(effect_allele='C', coeff_value=-5.123382e-03),
'rs7731626': ModelData(effect_allele='A', coeff_value=-1.412972e-02),
'rs4704643': ModelData(effect_allele='T', coeff_value=-4.112823e-04),
'rs10062611': ModelData(effect_allele='T', coeff_value= 3.326821e-03),
'rs2043502': ModelData(effect_allele='G', coeff_value=-3.321116e-04),
'rs2963775': ModelData(effect_allele='C', coeff_value=-6.346112e-03),
'rs364064': ModelData(effect_allele='G', coeff_value=-7.982236e-03),
'rs34627787': ModelData(effect_allele='C', coeff_value=-5.495315e-03),
'rs17549366': ModelData(effect_allele='G', coeff_value=-1.227776e-02),
'rs12522256': ModelData(effect_allele='A', coeff_value=-2.217961e-03),
'rs3832324': ModelData(effect_allele='C', coeff_value=-7.008718e-03),
'rs6891821': ModelData(effect_allele='G', coeff_value=-2.336763e-03),
'rs34454184': ModelData(effect_allele='C', coeff_value= 2.257138e-02),
'rs872305': ModelData(effect_allele='C', coeff_value=-9.364732e-03),
'rs2913748': ModelData(effect_allele='T', coeff_value= 1.499016e-02),
'rs1611076': ModelData(effect_allele='GTA', coeff_value=-5.878897e-03),
'rs42414': ModelData(effect_allele='G', coeff_value=-9.221108e-04),
'rs761340': ModelData(effect_allele='T', coeff_value=-1.172468e-04),
'rs13201655': ModelData(effect_allele='T', coeff_value= 1.516962e-03),
'rs3131044': ModelData(effect_allele='C', coeff_value= 1.849387e-02),
'rs1130838': ModelData(effect_allele='C', coeff_value=-2.224284e-03),
'rs2523587': ModelData(effect_allele='C', coeff_value=-4.434024e-02),
'rs9267488': ModelData(effect_allele='G', coeff_value= 1.631916e-01),
'rs1052486': ModelData(effect_allele='G', coeff_value=-5.194032e-02),
'rs204989': ModelData(effect_allele='A', coeff_value= 1.341116e-02),
'rs1265754': ModelData(effect_allele='A', coeff_value= 2.849767e-02),
'rs9268384': ModelData(effect_allele='G', coeff_value=-3.287204e-02),
'rs3129941': ModelData(effect_allele='G', coeff_value=-2.668458e-02),
'rs9268557': ModelData(effect_allele='C', coeff_value=-1.506960e-02),
'rs8084': ModelData(effect_allele='C', coeff_value=-5.234215e-02),
'rs9273529': ModelData(effect_allele='T', coeff_value= 5.623942e-01),
'rs9274253': ModelData(effect_allele='A', coeff_value= 1.836969e-01),
'rs3891175': ModelData(effect_allele='T', coeff_value= 2.456085e-01),
'rs9469219': ModelData(effect_allele='C', coeff_value=-6.708221e-02),
'rs9469220': ModelData(effect_allele='A', coeff_value=-7.826736e-02),
'rs1383264': ModelData(effect_allele='T', coeff_value=-1.635486e-01),
'rs241448': ModelData(effect_allele='G', coeff_value=-3.278471e-02),
'rs206767': ModelData(effect_allele='A', coeff_value= 2.497747e-02),
'rs2581': ModelData(effect_allele='T', coeff_value=-1.435438e-02),
'rs3135021': ModelData(effect_allele='A', coeff_value=-2.511791e-02),
'rs1042153': ModelData(effect_allele='A', coeff_value= 4.206788e-02),
'rs141530233': ModelData(effect_allele='GA', coeff_value= 2.080424e-01),
'rs9277471': ModelData(effect_allele='A', coeff_value= 1.204301e-02),
'rs4713614': ModelData(effect_allele='A', coeff_value=-3.457484e-02),
'rs4992764': ModelData(effect_allele='G', coeff_value= 5.730322e-02),
'rs2242416': ModelData(effect_allele='G', coeff_value=-1.016704e-03),
'rs167772': ModelData(effect_allele='T', coeff_value=-1.547706e-02),
'rs8589': ModelData(effect_allele='C', coeff_value=-1.557973e-02),
'rs1058768': ModelData(effect_allele='C', coeff_value=-1.287499e-02),
'rs12176543': ModelData(effect_allele='G', coeff_value=-3.230554e-02),
'rs12200969': ModelData(effect_allele='C', coeff_value= 8.569467e-03),
'rs1761608': ModelData(effect_allele='A', coeff_value= 3.385977e-02),
'rs156061': ModelData(effect_allele='T', coeff_value= 3.442731e-02),
'rs802734': ModelData(effect_allele='G', coeff_value= 2.016554e-02),
'rs17066096': ModelData(effect_allele='G', coeff_value=-2.177826e-03),
'rs6941060': ModelData(effect_allele='C', coeff_value= 1.057208e-02),
'rs9373523': ModelData(effect_allele='G', coeff_value=-2.134319e-03),
'rs1589734': ModelData(effect_allele='A', coeff_value= 2.287868e-03),
'rs237025': ModelData(effect_allele='A', coeff_value=-1.410767e-02),
'rs3823310': ModelData(effect_allele='C', coeff_value=-3.444990e-02),
'rs790263': ModelData(effect_allele='T', coeff_value= 1.693959e-02),
'rs1738074': ModelData(effect_allele='C', coeff_value=-5.730800e-03),
'rs212388': ModelData(effect_allele='T', coeff_value=-2.093106e-02),
'rs3798911': ModelData(effect_allele='G', coeff_value=-8.515201e-03),
'rs2305089': ModelData(effect_allele='T', coeff_value=-1.224299e-02),
'rs10447683': ModelData(effect_allele='C', coeff_value= 9.498037e-03),
'rs7779296': ModelData(effect_allele='G', coeff_value=-1.459517e-02),
'rs6463991': ModelData(effect_allele='C', coeff_value=-1.785931e-03),
'rs2058640': ModelData(effect_allele='C', coeff_value= 3.215908e-03),
'rs9791942': ModelData(effect_allele='C', coeff_value=-9.915166e-03),
'rs10239501': ModelData(effect_allele='G', coeff_value=-2.019518e-03),
'rs274624': ModelData(effect_allele='C', coeff_value=-1.167844e-03),
'rs2107285': ModelData(effect_allele='T', coeff_value=-6.074815e-03),
'rs3735557': ModelData(effect_allele='C', coeff_value=-7.221555e-03),
'rs213950': ModelData(effect_allele='A', coeff_value=-5.798412e-03),
'rs1506639': ModelData(effect_allele='C', coeff_value=-4.782917e-03),
'rs292592': ModelData(effect_allele='A', coeff_value= 7.780556e-02),
'rs12667644': ModelData(effect_allele='C', coeff_value= 1.066706e-02),
'rs28625723': ModelData(effect_allele='C', coeff_value=-3.280890e-02),
'rs3823715': ModelData(effect_allele='T', coeff_value=-5.237125e-02),
'rs4725816': ModelData(effect_allele='A', coeff_value= 1.292318e-03),
'rs3735169': ModelData(effect_allele='G', coeff_value= 9.800967e-03),
'rs10266230': ModelData(effect_allele='T', coeff_value=-4.246478e-03),
'rs3501': ModelData(effect_allele='C', coeff_value=-9.854625e-03),
'rs2617101': ModelData(effect_allele='C', coeff_value= 2.104167e-02),
'rs1048101': ModelData(effect_allele='G', coeff_value= 3.706238e-03),
'rs6983315': ModelData(effect_allele='A', coeff_value=-3.730140e-03),
'rs1963982': ModelData(effect_allele='G', coeff_value= 1.432579e-02),
'rs4739901': ModelData(effect_allele='T', coeff_value=-1.357942e-04),
'rs10104444': ModelData(effect_allele='G', coeff_value= 3.377678e-03),
'rs62514205': ModelData(effect_allele='G', coeff_value=-5.263720e-03),
'rs1209879': ModelData(effect_allele='T', coeff_value= 1.022749e-02),
'rs4014084': ModelData(effect_allele='C', coeff_value= 1.088862e-02),
'rs944141': ModelData(effect_allele='T', coeff_value= 2.530181e-02),
'rs7021389': ModelData(effect_allele='C', coeff_value= 1.093644e-02),
'rs2792218': ModelData(effect_allele='A', coeff_value=-4.691453e-03),
'rs7018498': ModelData(effect_allele='G', coeff_value= 2.567122e-04),
'rs7874881': ModelData(effect_allele='C', coeff_value=-1.063233e-02),
'rs12551927': ModelData(effect_allele='G', coeff_value= 1.195701e-03),
'rs4744346': ModelData(effect_allele='T', coeff_value=-8.471700e-03),
'rs4325016': ModelData(effect_allele='A', coeff_value=-5.253585e-05),
'rs10818113': ModelData(effect_allele='T', coeff_value=-2.795485e-03),
'rs2225659': ModelData(effect_allele='G', coeff_value=-4.431536e-03),
'rs3761847': ModelData(effect_allele='A', coeff_value=-4.742413e-04),
'rs2966332': ModelData(effect_allele='T', coeff_value=-2.147609e-02),
'rs1887309': ModelData(effect_allele='T', coeff_value= 1.690135e-03),
'rs34217442': ModelData(effect_allele='C', coeff_value=-1.228641e-02),
'rs5934812': ModelData(effect_allele='A', coeff_value=-2.950153e-02),
'rs10449070': ModelData(effect_allele='G', coeff_value= 1.898044e-05),
'rs5935650': ModelData(effect_allele='T', coeff_value=-1.465097e-02),
'rs5909459': ModelData(effect_allele='T', coeff_value= 4.018566e-02),
'rs56372937': ModelData(effect_allele='T', coeff_value= 1.910400e-02),
'rs4829392': ModelData(effect_allele='T', coeff_value= 3.319304e-02),
'rs1132201': ModelData(effect_allele='T', coeff_value=-1.659690e-03),
'rs5906109': ModelData(effect_allele='T', coeff_value= 1.502283e-02),
'rs55839103': ModelData(effect_allele='G', coeff_value=-3.024173e-03),
'rs5905683': ModelData(effect_allele='G', coeff_value= 4.937399e-03),
'rs6625359': ModelData(effect_allele='A', coeff_value=-9.374789e-03),
'rs149789635': ModelData(effect_allele='G', coeff_value= 2.167785e-03),
'rs59690498': ModelData(effect_allele='G', coeff_value= 5.396906e-03),
'rs142289898': ModelData(effect_allele='C', coeff_value= 1.450828e-02),
'rs2579004': ModelData(effect_allele='G', coeff_value= 5.946332e-03),
'rs5909807': ModelData(effect_allele='C', coeff_value=-7.049175e-03),
'rs3101163': ModelData(effect_allele='T', coeff_value= 1.191884e-02),
'rs2233058': ModelData(effect_allele='A', coeff_value=-5.891481e-03),
'rs5908275': ModelData(effect_allele='T', coeff_value=-2.025882e-02),
'rs5953823': ModelData(effect_allele='A', coeff_value= 7.853041e-03),
'rs7064371': ModelData(effect_allele='C', coeff_value= 6.686767e-03),
'rs1983610': ModelData(effect_allele='G', coeff_value=-9.620363e-04),
'rs561077': ModelData(effect_allele='G', coeff_value= 1.261497e-02),
'rs13440581': ModelData(effect_allele='G', coeff_value=-3.595636e-03),
'rs28599889': ModelData(effect_allele='C', coeff_value= 2.071745e-02),
'rs56178950': ModelData(effect_allele='C', coeff_value= 1.156860e-02),
'rs113329185': ModelData(effect_allele='C', coeff_value= 4.828264e-02)},
model_type='beta'
)
description = {
    "mean": -0.4436011728707001,
    "sd": 0.4738551400467062,
    "min": -3.1879015456499973,
    "max": 3.7646308529199994
}