## htsjdk.samtools.metrics.StringHeader
# CollectQualityYieldMetrics  --INPUT /data/ngs-projects/fichna/per-patient-info/shipment-38-wgs_460-498/run-analysis/results/460/460.markdup.recalibrated.bam --OUTPUT 460-CollectQualityYieldMetrics.txt --REFERENCE_SEQUENCE /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa  --USE_ORIGINAL_QUALITIES true --INCLUDE_SECONDARY_ALIGNMENTS false --INCLUDE_SUPPLEMENTAL_ALIGNMENTS false --ASSUME_SORTED true --STOP_AFTER 0 --VERBOSITY INFO --QUIET false --VALIDATION_STRINGENCY STRICT --COMPRESSION_LEVEL 2 --MAX_RECORDS_IN_RAM 500000 --CREATE_INDEX false --CREATE_MD5_FILE false --GA4GH_CLIENT_SECRETS client_secrets.json --help false --version false --showHidden false --USE_JDK_DEFLATER false --USE_JDK_INFLATER false
## htsjdk.samtools.metrics.StringHeader
# Started on: Thu May 28 09:52:06 UTC 2020

## METRICS CLASS	picard.analysis.CollectQualityYieldMetrics$QualityYieldMetrics
TOTAL_READS	PF_READS	READ_LENGTH	TOTAL_BASES	PF_BASES	Q20_BASES	PF_Q20_BASES	Q30_BASES	PF_Q30_BASES	Q20_EQUIVALENT_YIELD	PF_Q20_EQUIVALENT_YIELD
644072242	644072242	150	96610836300	96610836300	94362505582	94362505582	91510366183	91510366183	141183963032	141183963032


