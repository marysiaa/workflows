#description: HPO annotations for rare diseases [7801: OMIM; 47: DECIPHER; 3958 ORPHANET]
#date: 2020-08-11
#tracker: https://github.com/obophenotype/human-phenotype-ontology
#HPO-version: http://purl.obolibrary.org/obo/hp.obo/hp/releases/2020-08-11/hp.obo.owl
#DatabaseID	DiseaseName	Qualifier	HPO_ID	Reference	Evidence	Onset	Frequency	Sex	Modifier	Aspect	Biocuration
OMIM:618850	Hypervalinemia or hyperleucine-isoleucinemia		HP:0010913	PMID:25653144	PCS		1/1			P	HPO:probinson[2020-07-23];HPO:probinson[2020-07-23]
OMIM:618850	Hypervalinemia or hyperleucine-isoleucinemia		HP:0000007	PMID:25653144	PCS					I	HPO:probinson[2020-07-23];HPO:probinson[2020-07-23]
OMIM:618850	Hypervalinemia or hyperleucine-isoleucinemia		HP:0002922	PMID:25653144	PCS		1/1			P	HPO:probinson[2020-07-23];HPO:probinson[2020-07-23]
OMIM:618850	Hypervalinemia or hyperleucine-isoleucinemia		HP:0031993	PMID:25653144	PCS		1/1			P	HPO:probinson[2020-07-23];HPO:probinson[2020-07-23]
OMIM:618850	Hypervalinemia or hyperleucine-isoleucinemia		HP:0002315	PMID:25653144	PCS		1/1			P	HPO:probinson[2020-07-23];HPO:probinson[2020-07-23]
