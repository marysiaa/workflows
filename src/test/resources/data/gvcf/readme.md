# TEST FILES - GVCFS  

This directory contains small gVCF files for testing purposes


## Table of Contents  

* **gVCFs**  
 * [WES samples resticted to single genes](#wes-samples-resticted-to-single-genes)  
    * 424-nf2-gene.gvcf.gz
  * [WES samples resticted to intervals](#wes-samples-resticted-to-intervals)  
    * 220-chr11.46332116-48332116.chr1.1000000-1200000.g.vcf.gz
    * 220-chr11.46332116-48332116.chr11.46332116-48332116.g.vcf.gz

---  


###  WES samples resticted to single genes

|                           | |  
|---------------------------|-|  
| **Samples IDs:** |  424 |  
| **Seqencing type**:       | WES |  
| **Reference genome:** | grch38-no-alt-analysis-set |  
| **Regions:** | chr22:29601556-29700600 (NF2 gene)|  

**How it was generated?**  
 * FASTQ files were aligned with the use of Sentieon app available on DNAnexus
 * Resulting gVCF was restricted to NF2 gene (chr22:29601556-2970060)

**List of files:**  
```  
424-nf2-gene.gvcf.gz
424-nf2-gene.gvcf.gz.tbi
```  
[Return to the  table of contents](#test-files---gvcfs)  


###  WES samples resticted to intervals

|                           | |  
|---------------------------|-|  
| **Samples IDs:** |  220 |  
| **Seqencing type**:       | WES |  
| **Reference genome:** | grch38-no-alt-analysis-set |  
| **Regions:** | chr1:1000000-1200000, chr11:46332116-48332116 |

**How it was generated?**  
 * From test file 220-chr11.46332116-48332116.markdup.recalibrated.bam by part of variant-calling module
 * One resulting gVCF was restricted to chr1:1000000-1200000, second to chr11:46332116-48332116

**List of files:**  
```  
220-chr11.46332116-48332116.chr1.1000000-1200000.g.vcf.gz
220-chr11.46332116-48332116.chr1.1000000-1200000.g.vcf.gz.tbi
220-chr11.46332116-48332116.chr11.46332116-48332116.g.vcf.gz
220-chr11.46332116-48332116.chr11.46332116-48332116.g.vcf.gz.tbi
```
[Return to the  table of contents](#test-files---gvcfs)  
