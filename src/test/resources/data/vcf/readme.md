# TEST FILES - VCFS  

This directory contains small VCF files for testing purposes.
While adding new files you need to create new section as in examples below.
Make sure the link to the section and return link are working before pushing on dev!
In each section you should describe added files (their content, what you can do with them) and provide their list.
Do not include '-' sign in section's name. It causes problems with linking.

## Table of Contents


* [NF2 gene, chr22](#nf2-gene-chr22)
* [Parliament2 SVs VCFs 5 WGS samples GTS project](#parliament2-svs-vcfs-5-wgs-samples-gts-project)  
* [Annotated Parliament2 SVs VCFs 5 WGS samples GTS project](#annotated-parliament2-svs-vcfs-5-wgs-samples-gts-project)  
* [Sample 282 chr15 47497301 49497301 test samples for some tasks and modules in target seq](#sample-282-chr15-47497301-49497301-test-samples-for-some-tasks-and-modules-in-target-seq)
* [Sample 495 chr15 42357500 42414317, chr15 48406306 48647788, test sample for module detection chance](#sample-495-chr15-42357500-42414317-chr15-48406306-48647788-test-sample-for-module-detection-chance)
* [Sample 495 gvcf test sample for gvcf-genotype-by-vcf and pgx module with 1 variant](#sample-495-gvcf-test-sample-for-gvcf-genotype-by-vcf-and-pgx-module-with-1-variant)  
* [Pgx vcf with one variant: chr22	42126611](#pgx-vcf-with-one-variant-chr2242126611)
* [VCF from ClinVar, described in resources miscellanous, only with variants in 2 genes, FBN1 and CAPN3](#vcf-from-clinvar-described-in-resources-miscellanous-only-with-variants-in-2-genes-fbn1-and-capn3)
* [Sample 495 with genes FBN1 and CAPN3 genotyped by VCF from ClinVar, test sample for task vcf detect stats](#sample-495-with-genes-fbn1-and-capn3-genotyped-by-vcf-from-clinvar-test-sample-for-task-vcf-detect-stats)
* [Parliament2 SVs VCFs all WGS samples GTS project](#parliament2-svs-vcfs-all-wgs-samples-gts-project)
* [Sample 415, chr X and chr Y, test sample for task sex check x zygosity](#sample-415-chr-x-and-chr-y-test-sample-for-task-sex-check-x-zygosity)
* [Sample clinvar-annotation-file test samples for vcf-filter-clinical-test](#sample-clinvar-annotation-file-test-samples-for-vcf-filter-clinical-test)  
* [Small breast cancer](#small-breast-cancer)
---  

### NF2 gene, chr22
|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      | synthetic sample           |
| **Seqencing type**    | N/A                        |
| **Reference genome:** | grch38-no-alt-analysis-set |
| **Regions:**          | chr22                      |

List of variants included in VCF file:
[nsv4453783](https://www.ncbi.nlm.nih.gov/dbvar/variants/nsv4453783/)
[rs1555978369 547703](https://www.ncbi.nlm.nih.gov/clinvar/variation/547703/)
[654267](https://www.ncbi.nlm.nih.gov/clinvar/variation/654267/)
[nsv3889739](https://www.ncbi.nlm.nih.gov/dbvar/variants/nsv3889739/)
[rs755969033 341174](https://www.ncbi.nlm.nih.gov/clinvar/variation/341174/)

VCF file was created with the use of  [**create-nf2-sample-variants-vcf 1.0.0**](https://gitlab.com/intelliseq/workflows/-/blob/create-nf2-sample-variants-vcf.py@1.0.0/src/main/scripts/resources-tools/create-nf2-sample-variants-vcf.py)

```bash
REF_FASTA_GZ="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/chromosome-wise/chr22/chr22.GRCh38.no_alt_analysis_set.fa.gz"

python3 create-nf2-sample-variants-vcf.py  $REF_FASTA_GZ | bgzip > nf2-gene-variants-set-1.vcf.gz
```
[Return to the  table of contents](#table-of-contents)  

---

### Parliament2 SVs VCFs 5 WGS samples GTS project

|                           | |  
|---------------------------|-|  
| **Samples IDs:** |  S_136, S_170c, S_170d, S_6981, S_69812 |  
| **Seqencing type**:       | WGS |  
| **Reference genome:** | grch38-no-alt-analysis-set |  
| **Regions:** | chr22 |  

**How it was generated?**  
 * FASTQ files were aligned with the use of Sentieon app availible on DNAnexus  
 * SV were called using Parliament2 app availible on DNAnexus  
 * Resulting VCFs were restricted to chr22, sorted and bgzipped  

**List of files:**  

```  
S_136.chr22.parliament2.vcf.gz  
S_136.chr22.parliament2.vcf.gz.tbi  
S_170c.chr22.parliament2.vcf.gz  
S_170c.chr22.parliament2.vcf.gz.tbi  
S_170d.chr22.parliament2.vcf.gz  
S_170d.chr22.parliament2.vcf.gz.tbi  
S_6981.chr22.parliament2.vcf.gz  
S_6981.chr22.parliament2.vcf.gz.tbi  
S_6982.chr22.parliament2.vcf.gz  
S_6982.chr22.parliament2.vcf.gz.tbi  

```  
[Return to the  table of contents](#table-of-contents)  

---

### Annotated Parliament2 SVs VCFs 5 WGS samples GTS project

|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      |  S_136, S_170c, S_170d, S_6981, S_69812       |
| **Seqencing type**    |  WGS                      |
| **Reference genome:** | grch38-no-alt-analysis-set  |
| **Regions:**          | chr22                      |

```bash
REF_FASTA_GZ="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/chromosome-wise/chr22/chr22.GRCh38.no_alt_analysis_set.fa.gz"

**How it was generated?**  
 * FASTQ files were aligned with the use of Sentieon app availible on DNAnexus  
 * SV were called using Parliament2 app availible on DNAnexus  
 * Resulting VCFs were restricted to chr22, sorted and bgzipped ant then annotated with AnnotSV as implemented in this [annotsv.wdl](https://gitlab.com/intelliseq/workflows/blob/4a95674467404ff3c4073986c6ca827f7fb5782a/src/main/wdl/tasks/annotsv/latest/annotsv.wdl)  
```

**List of files:**  

```
S_136.chr22.parliament2-annotsv.vcf.gz  
S_136.chr22.parliament2-annotsv.vcf.gz.tbi  
S_170c.chr22.parliament2-annotsv.vcf.gz  
S_170c.chr22.parliament2-annotsv.vcf.gz.tbi  
S_170d.chr22.parliament2-annotsv.vcf.gz  
S_170d.chr22.parliament2-annotsv.vcf.gz.tbi  
S_6981.chr22.parliament2-annotsv.vcf.gz  
S_6981.chr22.parliament2-annotsv.vcf.gz.tbi  
S_6982.chr22.parliament2-annotsv.vcf.gz  
S_6982.chr22.parliament2-annotsv.vcf.gz.tbi  

```  
[Return to the  table of contents](#table-of-contents)  

---

### Sample 282 chr15 47497301 49497301 test samples for some tasks and modules in target seq

|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      | 282        |
| **Seqencing type**    |  WGS                      |
| **Reference genome:** | grch38-no-alt-analysis-set  |
| **Regions:**          | chr15:47497301-49497301,                    |



**List of files:**  

There is index .tbi for each file listed below

```  
described:

├── 282-chr15.47497301-49497301.annotated-with-functional-annotations-variant-level.vcf.gz - output of task vcf-anno-func-var.wdl
├── 282-chr15.47497301-49497301.gvcf.gz - generated using module variatn-calling (task: haplotype-caller)
├── 282-chr15_47497301-49497301.vcf.gz - vcf generated using variant-calling module
├── 282-chr15_47497301-49497301_anno-acmg-and-var-descr.vcf.gz
├── 282-chr15_47497301-49497301_anno-acmg.vcf.gz - annoted using report-acmg module 
├── 282-chr15_47497301-49497301_anno-ghr.vcf.gz - annoted using vcf-annotation module  
├── chr15.282-chr15.47497301-49497301.annotated-with-dbnsfp.vcf.gz - annoted by dbNSFP
├──chr15.282-chr15.47497301-49497301.anno-cov.vcf.gz - annotated by task annotate_vcf_with_gnomad_coverage in module vcf-annotation

not described: 

├── 282-chr15_47497301-49497301_annotated-with-snpeff.vcf.gz
├── 282.annotated-with-scores.vcf
├── 282.annotated-with-scores.vcf.gz
├── chr15.282-chr15_47497301-49497301.annotated-with-frequencies.vcf.gz 

```

[Return to the  table of contents](#table-of-contents)  

---

### Parliament2 SVs VCFs all WGS samples GTS project


|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      | all GTS samples      |
| **Seqencing type**    |  WGS                      |
| **Reference genome:** | grch38-no-alt-analysis-set  |
| **Regions:**          | chr22                    |


**How it was generated?**  
 * FASTQ files were aligned with the use of Sentieon app availible on DNAnexus  
 * SV were called using Parliament2 app availible on DNAnexus  
 * VCF files were merged with the use of SURVIVOR
 * Resulting VCFs were restricted to chr22, sorted and bgzipped  

**List of files:**  
```  
chr22-all-gts-samples.parliament2.vcf.gz  
chr22-all-gts-samples.parliament2.vcf.gz.tbi
```  
[Return to the  table of contents](#table-of-contents)  


---

### Sample 495 chr15 42357500 42414317, chr15 48406306 48647788, test sample for module detection chance



|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      | 495     |
| **Seqencing type**    |  WGS                      |
| **Reference genome:** | grch38-no-alt-analysis-set  |
| **Regions:**          |     chr15 42357500 42414317 (fbn1),    chr15 48406306 48647788 (capn3)     |

- created using Makefile script: scripts/test/genotype-gvcf/Makefile
- test sample for module detection chance

**List of files:**  

```
495.fbn1-capn3.bed.gz
495.fbn1-capn3.bed.gz.tbi
495.fbn1-capn3.gvcf.gz
495.fbn1-capn3.gvcf.gz.tbi
```

### Sample 495 gvcf test sample for gvcf-genotype-by-vcf and pgx module with 1 variant

|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      | 495     |
| **Seqencing type**    |  WGS                      |
| **Reference genome:** | grch38-no-alt-analysis-set  |
| **Regions:**          |  chr22	42126611 |

```
├── 495_chr22_42126611.g.vcf.gz
├── 495_chr22_42126611.g.vcf.gz.tbi
```

[Return to the  table of contents](#table-of-contents)


### Pgx vcf with one variant: chr22	42126611

```
├── pgx_ch22_42126611.vcf.gz
├── pgx_ch22_42126611.vcf.gz.tbi
```

[Return to the  table of contents](#table-of-contents)

```  


```
### VCF from ClinVar, described in resources miscellanous, only with variants in 2 genes, FBN1 and CAPN3


|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      | Synthetic sample |
| **Seqencing type**    | -                 |
| **Reference genome:** | grch38  |
| **Regions:**          |  only variants with GENE_INFO FBN1 and CAPN3  |

- created using Makefile script: scripts/test/genotype-gvcf/Makefile
- test sample for module detection chance


**List of files:**  

```
clinvar-fbn1-capn3.bed.gz
clinvar-fbn1-capn3.bed.gz.tbi
clinvar-fbn1-capn3.vcf.gz
clinvar-fbn1-capn3.vcf.gz.tbi


```
[Return to the  table of contents](#table-of-contents)

---

### Sample 495 with genes FBN1 and CAPN3 genotyped by VCF from ClinVar, test sample for task vcf detect stats

- this is the output of task gvcf-genotype-by-vcf (version 0.1.0)
- as input was used 495.fbn1-capn3.gvcf.gz (described above) and clinvar vcf that was provided in docker-image resource-clinvar:0.1.2

**List of files:**  

```
495.fbn1-capn_genotyped_by_clinvar.vcf.gz


```
[Return to the  table of contents](#table-of-contents)


---

### Sample 415, chr X and chr Y, test sample for task sex check x zygosity

Sample created by:
```bash
zcat /data/ngs-projects/shipment-28-wes/vcf-analysis/vcf-annotating-and-filtering/415-439.annotated-with-scores.vcf.gz | bcftools view -s 415 | grep '^#' > 415-chrXY.vcf
zcat /data/ngs-projects/shipment-28-wes/vcf-analysis/vcf-annotating-and-filtering/415-439.annotated-with-scores.vcf.gz | bcftools view -s 415 | grep -v '^#' | grep -E 'chrX|chrY' >> 415-chrXY.vcf
```

**List of files:**  

```
415-chrXY.vcf.gz 
```

[Return to the  table of contents](#table-of-contents)

---

### NF2 gene, chr22
|                       |                            |
|-----------------------|----------------------------|
| **Samples IDs:**      | synthetic sample           |
| **Seqencing type**    | N/A                        |
| **Reference genome:** | grch38-no-alt-analysis-set |
| **Regions:**          | chr22                      |

List of variants included in VCF file:
[nsv4453783](https://www.ncbi.nlm.nih.gov/dbvar/variants/nsv4453783/)
[rs1555978369 547703](https://www.ncbi.nlm.nih.gov/clinvar/variation/547703/)
[654267](https://www.ncbi.nlm.nih.gov/clinvar/variation/654267/)
[nsv3889739](https://www.ncbi.nlm.nih.gov/dbvar/variants/nsv3889739/)
[rs755969033 341174](https://www.ncbi.nlm.nih.gov/clinvar/variation/341174/)

VCF file was created with the use of  [**create-nf2-sample-variants-vcf 1.0.0**](https://gitlab.com/intelliseq/workflows/-/blob/create-nf2-sample-variants-vcf.py@1.0.0/src/main/scripts/resources-tools/create-nf2-sample-variants-vcf.py)

```bash
REF_FASTA_GZ="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/chromosome-wise/chr22/chr22.GRCh38.no_alt_analysis_set.fa.gz"

python3 create-nf2-sample-variants-vcf.py  $REF_FASTA_GZ | bgzip > nf2-gene-variants-set-1.vcf.gz
```
[Return to the  table of contents](#table-of-contents)  

---

### Small breast cancer

The fastq files were downloaded from https://pmbio.org/module-02-inputs/0002/05/01/Data/ (WGS Normal Lanes 1 and 2 and
Tumor lanes 1 and 2). Fastq files were aligned with our alignment module (grch38-no-alt reference), called with the
Mutect2 in *tumor-normal* mode and annotated with vcf-anno module (without civic). Finally, two variants were extracted
(TP53 - chr17:7675088 and BRCA2 - chr13:32339132). The chosen variant of the TP53 gene has record in the CIViC database.   

**List of files:**  

```  
├──small-breast-cancer.vcf.gz
├──small-breast-cancer.vcf.gz.tbi  

```

[Return to the  table of contents](#table-of-contents)

---