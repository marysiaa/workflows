#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject exists and contains task name
OUTPUT_NAME="bco" ### name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if bioobject exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi

### tests if bioobject contains task name
if grep -q $UNDERSCORED_TASK_NAME $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: can't find task name in bco"; fi

### tests if bioobject contains all domains: provenance_domain execution_domain parametric_domain description_domain
UNIT_TEST_NAME="bco domains test"
if grep -q provenance_domain $OUTPUT_FILE_PATH && grep -q execution_domain $OUTPUT_FILE_PATH && grep -q parametric_domain $OUTPUT_FILE_PATH && grep -q description_domain $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: bco is missing at least one of those domains: provenance_domain, execution_domain, parametric_domain, description_domain"; fi

### tests if bioobject domains are not empty
TEST_RESULT=$(jq 'if (.provenance_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.execution_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.parametric_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.description_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)

if echo $TEST_RESULT | grep -q empty; then error "$UNIT_TEST_NAME failed: at least one domain is empty (or does not exist)"; else ok "$UNIT_TEST_NAME passed"; fi


### Tests if stdout file exists
OUTPUT_NAME="stdout_log"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if file exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi


### tests if file exists
OUTPUT_NAME1="gvcf_gz"
OUTPUT_NAME2="gvcf_gz_tbi"
OUTPUT_NAME3="realigned_bam"
OUTPUT_NAME4="realigned_bai"
UNIT_TEST_NAME="output files test"

info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."

OUTPUT1=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME1
OUTPUT_FILE_PATH1=$(get_path_by_output_name $OUTPUT1)
OUTPUT2=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME2
OUTPUT_FILE_PATH2=$(get_path_by_output_name $OUTPUT2)
OUTPUT3=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME3
OUTPUT_FILE_PATH3=$(get_path_by_output_name $OUTPUT3)
OUTPUT4=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME4
OUTPUT_FILE_PATH4=$(get_path_by_output_name $OUTPUT4)

if [ -f "$OUTPUT_FILE_PATH1" ] && [ -f "$OUTPUT_FILE_PATH2" ]; then ok "gVCF file (bgziped) and its index file are present."; else error "Output gVCF.gz file or its index is not present."; fi
if [ -f "$OUTPUT_FILE_PATH3" ] && [ -f "$OUTPUT_FILE_PATH4" ]; then ok "Realigned bam and bai files are present."; else error "Output realigned bam or its bai file is not present.";fi

UNIT_TEST_NAME="output files content check"

info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
if samtools view $OUTPUT_FILE_PATH3 | grep -q 'chr' ; then ok "Passed: realigned bam file is not empty"; else error "Failed: realigned bam file is empty.";fi
if zcat $OUTPUT_FILE_PATH1 | grep '<NON_REF>' | grep -v -q '^#' ; then ok "Passed: gvcf file contains data lines"; else error "Failed:  gvcf file is empty or is malformated.";fi
