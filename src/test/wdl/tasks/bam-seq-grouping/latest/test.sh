#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $UNDERSCORED_TASK_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if stdout file exists
OUTPUT_NAME="stdout_log" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)

### Tests if sequence_grouping.txt exists
OUTPUT_NAME="sequence_grouping.txt" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)


# ### Tests sequence_grouping_with_unmapped.txt file 
UNIT_TEST_NAME="sequence_grouping_with_unmapped.txt"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
EXECUTION_DIR=$WORKING_DIR/cromwell-executions/$WORKFLOW_DIR/$EXECUTION_ID/$CALL_DIR/execution/
OUTPUT_FILE_NAME=sequence_grouping_with_unmapped.txt
GLOB_DIR=$(ls $EXECUTION_DIR | grep glob | grep -v '.list')
OUTPUT_FILE_PATH=${EXECUTION_DIR}/${GLOB_DIR}/${OUTPUT_FILE_NAME}
if [ -z $OUTPUT_FILE_PATH ]; then
    error "$UNIT_TEST_NAME failed"
else
    ok "$UNIT_TEST_NAME passed"
fi

### tests if file exists
TEST_RESULT=$([ -f "$OUTPUT_FILE_PATH" ]) ### should be provided
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
