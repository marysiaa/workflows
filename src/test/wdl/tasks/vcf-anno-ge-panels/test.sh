#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
export ROOTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
wdltest -t $( dirname "$SOURCE" )/test.json
echo $?