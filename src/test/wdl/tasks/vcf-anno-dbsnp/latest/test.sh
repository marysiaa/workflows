#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $UNDERSCORED_TASK_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if stdout file exists
OUTPUT_NAME="stdout_log" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### tests if file exists
OUTPUT_NAME1="annotated_with_dbsnp_rsids_vcf_gz" ### should be provided
OUTPUT_NAME2="annotated_with_dbsnp_rsids_vcf_gz_tbi"
UNIT_TEST_NAME="$OUTPUT_NAME1 and $OUTPUT_NAME2 test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT1=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME1
OUTPUT2=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME2
OUTPUT_FILE_PATH1=$(get_path_by_output_name $OUTPUT1)
OUTPUT_FILE_PATH2=$(get_path_by_output_name $OUTPUT2)
TEST_RESULT=$([ -f "$OUTPUT_FILE_PATH1" ] && [ -f "$OUTPUT_FILE_PATH2" ]) ### should be provided
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed: vcf file and its index are present"; else error "$UNIT_TEST_NAME failed: not all expected output files are present"; fi
ANNOTATED_LINES=$( zcat "$OUTPUT_FILE_PATH1" |  grep -v '^#' | grep -c 'ISEQ_DBSNP_RS' )
if [ "$ANNOTATED_LINES" -gt 0 ]; then ok "$UNIT_TEST_NAME passed: annotations were added"; else error "$UNIT_TEST_NAME failed: annotation not added to any vcf line"; fi
