#!/bin/bash

### variables ###
TEST_NAME="fastq_lines_split"
LAST_EDITED="10.07.2019"
LAST_EDITED_BY="marysia"
AUTHORS="      + Marcin Piechota, <piechota@intelliseq.pl>, https://gitlab.com/marpiech
      + Katarzyna Kolanek <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw"
MAINTAINER="Katarzyna Kolanek <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw"
TEST_EXECUTION_DIR="/tmp/workflows/test"
CROMWELL_JAR="/opt/tools/cromwell/cromwell.jar"
INPUT_FILE="input.json"

### resolve script path ###
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
TEST_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
UTILS_DIR=$(echo $TEST_DIR | sed 's/test\/wdl.*/test\/scripts\/generic/')


### get utils ###
source $UTILS_DIR"/help.sh"
source $UTILS_DIR"/arguments.sh"
source $UTILS_DIR"/colorcodes.sh"

info "Name: $TEST_NAME"

### get required paths ###
MAIN_DIR=$(echo $TEST_DIR | sed 's/test/main/')
TEST_RESOURCES_DIR=$(echo $TEST_DIR | sed 's/test\/wdl/test\/resources/g')
WDL=$(ls -d $MAIN_DIR/*.wdl | rev | cut -d '/' -f 1 | rev)
TASK_NAME=${WDL%.wdl}
DATE_AND_TIME=$(date +%Y-%m-%d_%H-%M)
WORKING_DIR=$TEST_EXECUTION_DIR/$TASK_NAME/$DATE_AND_TIME

info "Running: $WDL"
info "Working directory: $WORKING_DIR"

### Set working directory, create symlinks to input/output directories
DATE_AND_TIME=$(date +%Y-%m-%d_%H-%M)
WORKING_DIR=$TEST_EXECUTION_DIR/$TASK_NAME/$DATE_AND_TIME
mkdir -p $WORKING_DIR
cd $WORKING_DIR
ln -s $TEST_RESOURCES_DIR/inputs
ln -s $TEST_RESOURCES_DIR/outputs
cat $TEST_DIR/$INPUT_FILE | sed "s|\$TASK_TEST_RESOURCES|$WORKING_DIR|g" > $WORKING_DIR/$INPUT_FILE

### Starting cromwell
info "Starting Cromwell"
info "Cromwell path: $CROMWELL_JAR"
info "Input path: $WORKING_DIR/$INPUT_FILE"
info "Wdl path: $MAIN_DIR/$WDL"
if [ ! "$VERBOSE" == "TRUE" ]; then
  java -jar "$CROMWELL_JAR" run -i $WORKING_DIR/$INPUT_FILE $MAIN_DIR/$WDL &> cromwell-execution.log
else
  java -jar "$CROMWELL_JAR" run -i $WORKING_DIR/$INPUT_FILE $MAIN_DIR/$WDL | tee cromwell-execution.log
fi
info "Finished Task"
info "Log: $WORKING_DIR/cromwell-execution.log"

### Get task ID and task name
TASK_NAME_LOG=$(cat cromwell-execution.log | grep -Eo "Starting.*\..*" | cut -d " " -f 2)
info "Task name: $TASK_NAME_LOG"
TASK_ID=$(cat cromwell-execution.log | grep "\":" | sed 's/"//g' | sed 's/ //g' | grep "id:" | cut -d ':' -f 2)
info "Task ID: $TASK_ID"
TASK_STATUS=$(cat cromwell-execution.log | grep -Eo "finished with status .*'" | cut -d ' ' -f 4 | sed "s/'//g")
info "Workflow status: $TASK_STATUS"

### Check task status
if [ ! $TASK_STATUS == "Succeeded" ]; then
  error "Generic test status: FAILED"; exit 1;
else
  ok "Generic test status: PASSED";
fi


##############
# Task tests #
##############

info "Task tests"


# 1. Test if the outputs are identical to test outputs using "diff" command
   # Get cromwell-executions subdirectory ID

  EXECUTION_ID=$(ls $WORKING_DIR/cromwell-executions/*_workflow)
  TASK_NAME_HARD_SPACES=$(echo $TASK_NAME | sed "s/-/_/g" )
  WORKFLOW_DIR=$TASK_NAME_HARD_SPACES"_workflow"
  CALL_DIR="call-"$TASK_NAME_HARD_SPACES

  if [ ! "$VERBOSE" == "TRUE" ]; then

    OUTPUT_FILE=p.H2T2TBBXX_L4_1.fq.gz
    FILE_STATUS=$(diff -s $WORKING_DIR/cromwell-executions/$WORKFLOW_DIR/$EXECUTION_ID/$CALL_DIR/execution/$OUTPUT_FILE $TEST_RESOURCES_DIR/outputs/$OUTPUT_FILE)
    echo $FILE_STATUS >> $WORKING_DIR/task-test-1.log
    OUTPUT_FILE=p.H2TVLBBXX_L5_1.fq.gz
    FILE_STATUS=$(diff -s $WORKING_DIR/cromwell-executions/$WORKFLOW_DIR/$EXECUTION_ID/$CALL_DIR/execution/$OUTPUT_FILE $TEST_RESOURCES_DIR/outputs/$OUTPUT_FILE)
    echo $FILE_STATUS >> $WORKING_DIR/task-test-1.log

  else

    OUTPUT_FILE=p.H2T2TBBXX_L4_1.fq.gz
    FILE_STATUS=$(diff -s $WORKING_DIR/cromwell-executions/$WORKFLOW_DIR/$EXECUTION_ID/$CALL_DIR/execution/$OUTPUT_FILE $TEST_RESOURCES_DIR/outputs/$OUTPUT_FILE)
    echo $FILE_STATUS
    echo $FILE_STATUS >> $WORKING_DIR/task-test-1.log
    OUTPUT_FILE=p.H2TVLBBXX_L5_1.fq.gz
    FILE_STATUS=$(diff -s $WORKING_DIR/cromwell-executions/$WORKFLOW_DIR/$EXECUTION_ID/$CALL_DIR/execution/$OUTPUT_FILE $TEST_RESOURCES_DIR/outputs/$OUTPUT_FILE)
    echo $FILE_STATUS
    echo $FILE_STATUS >> $WORKING_DIR/task-test-1.log


  fi

  NO_IDENTICAL_OUTPUT=$(cat task-test-1.log | grep "identical" | wc -l )

  info "Description: Test if the outputs are identical to test outputs using \"diff\" command"
  info "Log path: $WORKING_DIR/task-test-1.log"

  if [ ! $NO_IDENTICAL_OUTPUT == "2" ]; then
    error "Task-test-1 status: FAILED"; exit 1;
  else
    ok "Task-test-1 status: PASSED";
  fi
