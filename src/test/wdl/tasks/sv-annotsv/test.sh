#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $UNDERSCORED_TASK_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if stdout file exists
OUTPUT_NAME1="full_annotsv_tsv_gz" ### should be provided
OUTPUT1=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME1
OUTPUT_FILE_PATH1=$(get_path_by_output_name $OUTPUT1 | sed 's/]//' )
#echo $OUTPUT_FILE_PATH1
LINES1=$( zcat $OUTPUT_FILE_PATH1 | awk -F "\t" '$16 == "full"' | wc -l )
#printf "Good transcript: ENST00000241337\n"
#printf "Annotsv transcript:\n"
#zcat $OUTPUT_FILE_PATH1  | grep 'GSTM2' | grep 'split' | cut -f 17
OUTPUT_NAME2="annotated_vcf_gz" ### should be provided
UNIT_TEST_NAME2="$OUTPUT_NAME2 test"
OUTPUT2=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME2
OUTPUT_FILE_PATH2=$(get_path_by_output_name $OUTPUT2)
LINES2=$( zcat $OUTPUT_FILE_PATH2 |  grep -v '^#' | wc -l )
UNIT_TEST_NAME1="AnnotSV output files test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
if [ $LINES2 -gt 0 ] && [ $LINES1 -eq $LINES2 ]; then ok "Output vcf contains all data lines";else error "Output vcf is empty or not all data lines present";fi
printf "Result of vcf-validator: \n"
vcf-validator $OUTPUT_FILE_PATH2
