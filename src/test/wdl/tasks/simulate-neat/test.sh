#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject exists and contains task name
OUTPUT_NAME="bco" ### name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if bioobject exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi

### tests if bioobject contains task name
if grep -q $UNDERSCORED_TASK_NAME $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: can't find task name in bco"; fi

### tests if bioobject contains all domains: provenance_domain execution_domain parametric_domain description_domain
UNIT_TEST_NAME="bco domains test"
if grep -q provenance_domain $OUTPUT_FILE_PATH && grep -q execution_domain $OUTPUT_FILE_PATH && grep -q parametric_domain $OUTPUT_FILE_PATH && grep -q description_domain $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: bco is missing at least one of those domains: provenance_domain, execution_domain, parametric_domain, description_domain"; fi

### tests if bioobject domains are not empty
TEST_RESULT=$(jq 'if (.provenance_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.execution_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.parametric_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.description_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)

if echo $TEST_RESULT | grep -q empty; then error "$UNIT_TEST_NAME failed: at least one domain is empty (or does not exist)"; else ok "$UNIT_TEST_NAME passed"; fi


### Tests if stdout file exists
OUTPUT_NAME="stdout_log"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if file exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi


### Tests if output files were created

OUTPUT_NAME="fastq_1_gz"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if file exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; fi
TEST_RESULT=$(zcat $OUTPUT_FILE_PATH | grep -c '+')
if [ $TEST_RESULT -gt 10 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi


OUTPUT_NAME="golden_vcf_gz"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if file exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; fi

#test vcf specifications
if zcat $OUTPUT_FILE_PATH | head -1 | grep -q "##fileformat=VCF"; then ok "Passed: VCF file content begins with ##fileformat=VCF"; else error "Header does not begin with ##fileformat=VCF"; fi
if zcat $OUTPUT_FILE_PATH | grep -q "#CHROM"; then ok "VCF file contains #CHROM"; else error "Output vcf.gz does not conatin proper header with #CHROM"; fi

TEST_RESULT_1=$(zcat $OUTPUT_FILE_PATH | grep -v '^#' | grep -c 'chr22')
TEST_RESULT_2=$(zcat $OUTPUT_FILE_PATH | grep -c '#INFO')
if [ $TEST_RESULT_1 -gt 10 ] && [ $TEST_RESULT_2 -gt 3 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
