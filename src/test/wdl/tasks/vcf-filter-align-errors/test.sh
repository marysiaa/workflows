#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
export ROOTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
wdltest -t $( dirname "$SOURCE" )/test.json
EXITCODE=$?
echo exitcode $EXITCODE
if [ "$EXITCODE" -eq "0" ]; then
   echo "TEST SUCCEEDED";
else
   echo "TEST FAILED";
fi
exit $EXITCODE
