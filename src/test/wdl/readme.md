# TESTS WRITING README
## New task tests
Script `scripts/create-task` produces files `test.sh` and `test.json` from test template. File `test.sh` is identical for each test:
```
#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
export ROOTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
wdltest -t $( dirname "$SOURCE" )/test.json
echo $?
```
File `test.json` contains basic tests of bco and stdout output files:
```
{
    "wdl":"wdltaskpath",
    "tests": [
        {
            "name":"Primary test",
            "inputs": {
            },
            "conditions": [
              {
                  "file":"bco",
                  "name":"Bco exists",
                  "error_message":"Bco does not exist",
                  "command":"echo $file"
              },
              {
                  "file":"bco",
                  "name":"Provenance domain exists and is not empty",
                  "error_message":"Provenance domain not found in bco or is empty",
                  "command":"grep -q -m1 provenance_domain $file && jq -e 'if (.provenance_domain | length) == 0 then false else true end' $file"
              },
              {
                  "file":"bco",
                  "name":"Execution domain exists and is not empty",
                  "error_message":"Execution domain not found in bco or is empty",
                  "command":"grep -q -m1 execution_domain $file && jq -e 'if (.execution_domain | length) == 0 then false else true end' $file"
              },
              {
                  "file":"bco",
                  "name":"Parametric domain exists and is not empty",
                  "error_message":"Parametric domain not found in bco or is empty",
                  "command":"grep -q -m1 parametric_domain $file && jq -e 'if (.parametric_domain | length) == 0 then false else true end' $file"
              },
              {
                  "file":"bco",
                  "name":"Description domain exists and is not empty",
                  "error_message":"Description domain not found in bco or is empty",
                  "command":"grep -q -m1 description_domain $file && jq -e 'if (.description_domain | length) == 0 then false else true end' $file"
              },
              {
                  "file":"stdout_log",
                  "name":"Stdout exits",
                  "error_message":"Stdout does not exist",
                  "command":"echo $file"
              },
              {
                  "file":"OUTPUT-FILE-TO-ADD",
                  "name":"File OUTPUT-FILE-TO-ADD exists",
                  "error_message":"OUTPUT-FILE-TO-ADD does not exist",
                  "command":"echo $file"
              }
            ]
        }
    ]
}
```

Tests are executed by `wdltest` - https://github.com/marpiech/wdltest. To install locally use `pip3 install --upgrade --index-url https://test.pypi.org/simple/ --no-deps wdltest`. You could also need to run `pip3 install regex`.

## Examples
**INPUTS**:
```            
"inputs": {
              "vcf_anno.vcf_gz": "${ROOTDIR}/src/test/resources/data/vcf/test-sample.vcf.gz",
              "vcf_anno.vcf_gz_tbi": "${ROOTDIR}/src/test/resources/data/vcf/test-sample.vcf.gz.tbi",
              "vcf_anno.panel_sequencing": false
            }
```
**TESTING SPECIFIC OUTPUTS**
```
              {
                  "file":"panel",
                  "name":"File panel exists",
                  "error_message":"File panel does not exist",
                  "command":"echo $file"
              },
              {
                  "file":"panel",
                  "name":"File panel contains specific strings",
                  "error_message":"File panel does not contain specific strings",
                  "command":"grep -c -e 'HTT' -e 'FANCF' $file"
              }
```
**DIFFERENT INPUTS**
If you want to add the next test with different inputs, copy `Primary test` and add it below, remember to change the test name, inputs and specific tests.
```
"tests": [
        {
            "name":"Primary test",
            "inputs": {
            (...)
        },
        {
            "name":"Second test",
            "inputs": {
            (...)
        }
    ]
```
If you want to add some new kind of test, please read below "Good ways of testing" and "DO NOT USE".



****


****


****


# OLD TEST WRITING README


## Old test template

Script `scripts/create-task` produces files `test.sh` and `test-input.json` from test template. This file contains a basic test of bco and stdout output files:
```bash
#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject exists and contains task name
OUTPUT_NAME="bco" ### name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if bioobject exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi

### tests if bioobject contains task name
if grep -q $UNDERSCORED_TASK_NAME $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: can't find task name in bco"; fi

### tests if bioobject contains all domains: provenance_domain execution_domain parametric_domain description_domain
UNIT_TEST_NAME="bco domains test"
if grep -q provenance_domain $OUTPUT_FILE_PATH && grep -q execution_domain $OUTPUT_FILE_PATH && grep -q parametric_domain $OUTPUT_FILE_PATH && grep -q description_domain $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: bco is missing at least one of those domains: provenance_domain, execution_domain, parametric_domain, description_domain"; fi

### tests if bioobject domains are not empty
TEST_RESULT=$(jq 'if (.provenance_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.execution_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.parametric_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.description_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)

if echo $TEST_RESULT | grep -q empty; then error "$UNIT_TEST_NAME failed: at least one domain is empty (or does not exist)"; else ok "$UNIT_TEST_NAME passed"; fi


### Tests if stdout file exists
OUTPUT_NAME="stdout_log"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if file exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi
```

The specific task tests should be added to this file.

****

## Good ways of testing
To check if filepath is not empty (`[ -z "$OUTPUT_FILE_PATH" ]` is true if string `OUTPUT_FILE_PATH` is empty/doesn't exist). It does not check if the filepath is valid or if the file exists.
```bash
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; fi
```
To check if the file exists:
```bash
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; fi
```
In cases above `exit 1;` is added before `fi` while checking bco and stdout files, because if they don't exist it does not make sense to check anything else. Also use `exit 1`, if you want to use `cat $OUTPUT` later, because using `cat` on non-existing file cause the test to freeze.
Check two files at once - if they are not empty:
```bash
if [ -s "$OUTPUT_FILE_PATH1" ] && [ -s "$OUTPUT_FILE_PATH2" ]; then ok "files are not empty - PASSED"; else error "one or both files are empty"; fi
```
__IMPORTANT:__ The spaces near brackets are required (e.g. `[-z "$OUTPUT_FILE_PATH"]` is wrong). Check WRONG section if you want do **anything** differently

To check if file contains specific string use:
```bash
if grep -q $SPECIFIC_STRING $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
```
`grep -q` do not write anything to standard output, exits immediately with zero status if any match is found, even if an error was detected.
Use short `grep -c "name" $OUTPUT_FILE_PATH` instead long `cat $OUTPUT_FILE_PATH | grep "string" | wc -l`.

Some ways to check vcf header:
```bash
if zcat $OUTPUT_FILE_PATH | head -1 | grep -q "##fileformat=VCF"; then ok "VCF file content begins with ##fileformat=VCF"; else error "Header does not begin with ##fileformat=VCF"; fi

if zcat $OUTPUT_FILE_PATH1 | grep -q "#CHROM"; then ok "VCF file contains #CHROM"; else error "Output vcf.gz does not contain proper header with #CHROM"; fi

if zcat $OUTPUT_FILE_PATH1 | grep -q "##contig=<ID="; then ok "VCF file contains contigs"; else error "Output vcf.gz does not conatin contigs"; fi
```
There is also a tool `vcf-validator`:
```bash
if vcf-validator $OUTPUT_FILE_PATH1; then ok "PASSED; the vcf-validator comment is above (if exists)"; else error "vcf-validator error (above)"; fi
```
Such test works, but be careful when changing it
```bash
TEST_RESULT=$(grep "chrY\:2786989-2787603 coverage" $OUTPUT_FILE_PATH | wc -l)
if [ $TEST_RESULT == 1 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
if [ $TEST_RESULT -eq 1 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
if [ $TEST_RESULT -gt 0 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
```
For testing html files (for example raports - because they can pass not-empty test, but be broken), you can use something like this (open html in text editor to get some string like here):
```bash
if grep -F -q "<b>Gene</b></p>" $OUTPUT_FILE_PATH; then ok "Passed: file $OUTPUT_NAME contains specific string"; else error "File $OUTPUT_NAME does not contain specific string"; fi
```
Please, don't leave useless comments in your tests.
****

## DO NOT USE


Here, even if `$SOMETHING` is not present in file, the test will pass. The output of grep is not stored in `$TEST_RESULT`, it is an empty variable. `if $TEST_RESULT` for an empty variable returns true.
```bash
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $SOMETHING)
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
```
The same situation is here, `$TEST_RESULT` is always empty.
```bash
TEST_RESULT=$([ -f "$OUTPUT_FILE_PATH" ])
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
```
Again:
```bash
TEST_RESULT=$([ $( zcat "$OUTPUT_FILE_PATH" | grep -c '##fileformat=VCFv4.2' ) -eq 1 ])
if $TEST_RESULT; then ok "$UNIT_TEST_NAME header passed: ##fileformat=VCFv4.2 is there"; else error "$UNIT_TEST_NAME header failed: ##fileformat=VCFv4.2 isn't there"; fi
TEST_RESULT=$([ $( zcat "$OUTPUT_FILE_PATH" | grep -v '^#' | grep -c 'chr11' ) -gt 10 ])
if $TEST_RESULT; then ok "$UNIT_TEST_NAME first sample test passed"; else error "$UNIT_TEST_NAME first sample test failed"; fi

```
