#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $UNDERSCORED_TASK_NAME)
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if stdout file exists
OUTPUT_NAME="stdout_log"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### tests if file exists
TEST_RESULT=$([ -f "$OUTPUT_FILE_PATH" ]) ### should be provided
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

OUTPUT_NAME="vcf_gz" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)


### Tests if output files were created
OUTPUT_NAME1="vcf_gz"
OUTPUT_NAME2="vcf_gz_tbi"

UNIT_TEST_NAME="Output files test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT1=$WORKFLOW_NAME"."$OUTPUT_NAME1
OUTPUT_FILE_PATH1=$(get_path_by_output_name $OUTPUT1)
OUTPUT2=$WORKFLOW_NAME"."$OUTPUT_NAME2
OUTPUT_FILE_PATH2=$(get_path_by_output_name $OUTPUT2)

if [ -f "$OUTPUT_FILE_PATH1" ] && [ -f "$OUTPUT_FILE_PATH2" ]; then ok "VCF file (bgziped) and its index file are present."; else error "Output gVCF.gz file or its index is not present."; fi
if [ -s "$OUTPUT_FILE_PATH1" ] && [ -s "$OUTPUT_FILE_PATH2" ]; then ok "VCF file (bgziped) and its index are not empty"; else error "Output gVCF.gz file or its index is empty"; fi
if zcat $OUTPUT_FILE_PATH1 | head | grep -q "##fileformat=VCF"; then ok "VCF file content is ok"; else error "Output vcf.gz does not conatin proper content"; fi

UNIT_TEST_NAME="$OUTPUT_NAME content test - two samples"
PHRASE_COUNT=$(zcat $OUTPUT_FILE_PATH1 | grep -c '220	424')
TEST_RESULT=$( [ $PHRASE_COUNT -gt 0 ])
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

UNIT_TEST_NAME="$OUTPUT_NAME content test - chr 11"
PHRASE_COUNT=$(zcat $OUTPUT_FILE_PATH1 | grep -v '^#' |grep -c 'chr 11')
TEST_RESULT=$( [ $PHRASE_COUNT -gt 10 ])
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

UNIT_TEST_NAME="$OUTPUT_NAME content test - chr 22"
PHRASE_COUNT=$(zcat $OUTPUT_FILE_PATH1 | grep -v '^#' |grep -c 'chr 22')
TEST_RESULT=$( [ $PHRASE_COUNT -gt 10 ])
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
