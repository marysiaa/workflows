#!/bin/bash
SCRIPT_DIR=`dirname $(readlink -f "$0")`
SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains module name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### test if bioobject contains module name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $UNDERSCORED_TASK_NAME) # in fact it's not UNDERSCORED_TASK_NAME, but module name
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if output file exists
OUTPUT_NAME="report_odt" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT | sed 's/\]//g')
### tests if file exists
TEST_RESULT=$([ -f "$OUTPUT_FILE_PATH" ]) ### should be provided
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests template.html for correctness
OUTPUT_NAME="report_html" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT | sed 's/\]//g')
VENV_DIR=$(mktemp -d)
virtualenv $VENV_DIR -p /usr/bin/python3
source $VENV_DIR/bin/activate
pip install beautifulsoup4 lxml
PARSED_TEMPLATE_FILE=$(mktemp)
PARSED_OUTPUT_FILE=$(mktemp)
# output and template are parsed and the content of vit_D model output is compared
python3 $SCRIPT_DIR/parse_html.py -f $SCRIPT_DIR/template.html > $PARSED_TEMPLATE_FILE
python3 $SCRIPT_DIR/parse_html.py -f "$OUTPUT_FILE_PATH" > $PARSED_OUTPUT_FILE
if cmp -s $PARSED_TEMPLATE_FILE $PARSED_OUTPUT_FILE;
    then ok "$UNIT_TEST_NAME passed";
    else error "$UNIT_TEST_NAME failed";
fi
rm $PARSED_OUTPUT_FILE
rm $PARSED_TEMPLATE_FILE
rm -r $VENV_DIR