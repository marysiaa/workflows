#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains module name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $MODULE_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)

### test if bioobject contains module name
MODULE_NAME="mt_varcall"
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $MODULE_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if output vcf is not empty and contains only variants that passed all filters
OUTPUT_NAME="mt_filtered_vcf_gz"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
DATA=$( zcat $OUTPUT_FILE_PATH | grep -v "#" | grep -m1 -c '^chrM' )
if [ $DATA -gt 0 ]; then ok "$OUTPUT contains data lines"; else error "$OUTPUT has no data"; fi
DATA2=$( zcat $OUTPUT_FILE_PATH | grep -v "#" | grep -v "PASS" | wc -l )
if [ $DATA2 -eq 0 ]; then ok "$OUTPUT contains only variants that passed the filters"; else error "$OUTPUT contains variants that are rejected"; fi
echo "result of the vcf-validator (should be empty):"
vcf-validator $OUTPUT_FILE_PATH
echo

### Tests if output mutect2 bam is not empty
OUTPUT_NAME="mutect2_mt_bam"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
DATA=$( samtools view $OUTPUT_FILE_PATH chrM | grep  -m1 -c '^' )
if [ $DATA -gt 0 ]; then ok "$OUTPUT contains data lines"; else error "$OUTPUT has no data"; fi

### Tests if output "normal" bam is not empty
OUTPUT_NAME="realigned_mt_bam"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
DATA=$( samtools view $OUTPUT_FILE_PATH chrM | grep  -m1 -c '^' )
if [ $DATA -gt 0 ]; then ok "$OUTPUT contains data lines"; else error "$OUTPUT has no data"; fi