#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)

### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $TASK_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if bam output file exists
OUTPUT_NAME="filtered_bam" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi

### Test if bam file has more than 60 000 reads on chr15
TEST_RESULT=$(samtools view $OUTPUT_FILE_PATH | grep "chr15" | wc -l)
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
info "Testing if bam file has more than 60 000 reads on chr15"
if [ "$TEST_RESULT" -gt 60000 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; exit 1; fi

