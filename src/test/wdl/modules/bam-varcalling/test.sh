#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

TEST_NUMBER=1

### Tests if output files exist
OUTPUT_NAME0="grouped_calling_intervals"
OUTPUT_NAME1="gvcf_gz" 
OUTPUT_NAME2="gvcf_gz_tbi"
OUTPUT_NAME3="vcf_gz_tbi" 
OUTPUT_NAME4="vcf_gz_tbi" 
OUTPUT_NAME5="haplotype_caller_bam" 
OUTPUT_NAME6="haplotype_caller_bai" 
 
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT0=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME0
OUTPUT1=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME1
OUTPUT2=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME2
OUTPUT3=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME3
OUTPUT4=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME4
OUTPUT5=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME5
OUTPUT6=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME6
OUTPUT_FILE_PATH0=$(get_path_by_output_name $OUTPUT0)
#echo $OUTPUT_FILE_PATH0
OUTPUT_FILE_PATH1=$(get_path_by_output_name $OUTPUT1)
OUTPUT_FILE_PATH2=$(get_path_by_output_name $OUTPUT2)
OUTPUT_FILE_PATH3=$(get_path_by_output_name $OUTPUT3)
OUTPUT_FILE_PATH4=$(get_path_by_output_name $OUTPUT4)
OUTPUT_FILE_PATH5=$(get_path_by_output_name $OUTPUT5)
OUTPUT_FILE_PATH6=$(get_path_by_output_name $OUTPUT6)
if [ -f $OUTPUT_FILE_PATH0 ]; then ok "Interval files were created"; else error "Interval files are not present."; fi
if [ -f $OUTPUT_FILE_PATH1 ] && [ -f $OUTPUT_FILE_PATH2 ]; then ok "Both gVCF_gz and gVCF_gz_tbi files are present"; else error "gVCF_gz file or its index is not present."; fi
if [ -f $OUTPUT_FILE_PATH3 ] && [ -f $OUTPUT_FILE_PATH4 ]; then ok "Both genotyped vcf_gz file and its index file are present"; else error "Genotyped VCF_gz file or its index is not present."; fi
if [ -f $OUTPUT_FILE_PATH5 ] && [ -f $OUTPUT_FILE_PATH6 ]; then ok "Realigned bam file and its index file are present"; else error "Realigned bam file or its index is not present"; fi

