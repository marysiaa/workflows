#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
export ROOTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"

if [[ $# -eq 0 ]];then
    wdltest -t $( dirname "$SOURCE" )/test.json
    echo $?
elif [ $1 -ge 0 ];then
    wdltest -t $( dirname "$SOURCE" )/test.json -i $1
    echo $?
else
    echo "Bad positional argument, must be integer"

fi

