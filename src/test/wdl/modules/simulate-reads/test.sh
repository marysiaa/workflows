#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $UNDERSCORED_TASK_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if stdout file exists
OUTPUT_NAME="stdout_log" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)
### tests if file exists
TEST_RESULT=$([ -f "$OUTPUT_FILE_PATH" ]) ### should be provided
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

OUTPUT_NAME="fastq_1_gz" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
TEST_RESULT=$(zcat $OUTPUT_FILE_PATH | grep -c '+')
if [ $TEST_RESULT -gt 10 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi


OUTPUT_NAME="golden_vcf_gz" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
TEST_RESULT_1=$(zcat $OUTPUT_FILE_PATH | grep -c '##fileformat=VCFv4.1')
TEST_RESULT_2=$(zcat $OUTPUT_FILE_PATH | grep -c '#CHROM')
TEST_RESULT=$((TEST_RESULT_1+TEST_RESULT_2))
if [ $TEST_RESULT == 2 ]; then ok "$UNIT_TEST_NAME 1 passed"; else error "$UNIT_TEST_NAME 1 failed"; fi
TEST_RESULT_1=$(zcat $OUTPUT_FILE_PATH | grep -v '^#' | grep -c 'chr22')
TEST_RESULT_2=$(zcat $OUTPUT_FILE_PATH | grep -c '#INFO')
if [ $TEST_RESULT_1 -gt 10 ] && [ $TEST_RESULT_2 -gt 3 ]; then ok "$UNIT_TEST_NAME 2 passed"; else error "$UNIT_TEST_NAME 2 failed"; fi
