# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  info:
#  -----
#    name: task-1
#    status: Correct File
#    author(s):
#      - Correct File
#    license: All rights reserved
#
#  description: |
#  ------------
#    Concatenate VCFs. Example: chromosome-wise VCFs (chr1, chr2, ..., chr22, chrX, chrY-and-the-rest).
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

workflow concatenate_vcfs_workflow { call concatenate_vcfs {} }

task concatenate_vcfs {

# @Input (name = "VCF files array (bgzipped)")
  Array[File]? vcf_gz
# @Input (name = "VCF TBI indices array")
  Array[File] vcf_gz_tbi = abb

# @Input (name = "VCF basename")
  String vcf_basename

# @Input(name = "Field Name", desc = "Field description")
aaa bbb
# @Input(min = 0, max = 10)
ccc ddd
# @Input(name = "Field Name", desc = "Field description", min = 0.0, max = 10.6)
eee fff
# @Input(type = "enum", values = ["value1", "value2", "value3"])
ggg hhh

# @Input()
iii jjj

# @Input
kkk lll


  output {

    # @Output(required=true,directory="/task_name_TAG",filename="stdout.log")
    File stdout_log = stdout()
    # @Output(required=true,directory="/task_name_TAG",filename="stderr.log")
    File stderr_log = stderr()
    # @Output(required=true,directory="/task_name_TAG",filename="bioobject.json")
    File bioobject = "bioobject.json"

  }

