Here you can find JSON files with released workflows available for each environment:

* intelliseqflow-> Intelliseqflow
* clinical-> Clinical
* tgenome -> T-genome

In addition, the other JSON files contain released workflows for selected organizations (test organization on staging)

* 635c1d6e-a1c7-4be0-b910-b74715ca32ea.json -> custom2
* 0a831b20-5ffc-4b80-b17d-27586b76faa5.json -> custom1