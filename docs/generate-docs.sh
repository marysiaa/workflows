#!/bin/bash
echo "# Workflows" > docs/workflows.md
echo "## Pipelines" >> docs/workflows.md
find src/main/wdl/pipelines/ -name "*.wdl" | grep latest | xargs -I @@ bash -c 'src/main/scripts/wdltools/wdltools.py describe --wdl @@ 2>/dev/null >> docs/workflows.md; echo \`\`\` >> docs/workflows.md; src/main/scripts/wdltools/wdltools.py test --wdl @@ 2>/dev/null >> docs/workflows.md; echo \`\`\` >> docs/workflows.md'
echo "---" >> docs/workflows.md
echo "## Modules" >> docs/workflows.md
find src/main/wdl/modules/ -name "*.wdl" | grep latest | xargs -I @@ bash -c 'src/main/scripts/wdltools/wdltools.py describe --wdl @@ 2>/dev/null >> docs/workflows.md; echo \`\`\` >> docs/workflows.md; src/main/scripts/wdltools/wdltools.py test --wdl @@ 2>/dev/null >> docs/workflows.md; echo \`\`\` >> docs/workflows.md'
echo "---" >> docs/workflows.md
echo "## Tasks" >> docs/workflows.md
find src/main/wdl/tasks/ -name "*.wdl" | grep latest | xargs -I @@ bash -c 'src/main/scripts/wdltools/wdltools.py describe --wdl @@ 2>/dev/null >> docs/workflows.md; echo \`\`\` >> docs/workflows.md; src/main/scripts/wdltools/wdltools.py test --wdl @@ 2>/dev/null >> docs/workflows.md; echo \`\`\` >> docs/workflows.md'
echo "---" >> docs/workflows.md
