Developer tips
==================

info stąd: https://gitlab.com/intelliseq/workflows/-/tree/master#developer-tips-also-for-dummies

Fixing production issues
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    1. Make a new branch from the version tag that is in production
    2. Fix the bug
    3. Tag the changes with the same version as in production, but add a suffix to the tag, such as "task_x-bug-fix"
    4. Test it in the interface (on the small input and the input which caused an error)
    5. Merge changes to dev
    6. Replace the version of wdl in the file with wdl released (on the dev and master)


Generate google credentials
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before you start modifying meta make sure you are using account with intelliseq domain!
  1. Go to the google dev console site https://console.cloud.google.com and create a new project.
  2. To do this you need to look at the top of site and find current chosen project with an arrow pointing down.
  3. After clicking it you should see pop up window containing "new project" button.
  4. For the moment we don't have any naming convention for it so name it as you wish to.
  5. Click create and then select this new project. You can do this in the same place as at the beginning (current chosen project at the top of the site).
  6. Choose "APIs & Services" which is on the left of site.
  7. Choose "Enable APIs and Services" option when you selected your project.
  8. Search for "Google Drive API" and "Google Sheets API" in the search bar located at the top of the site.
  9. Enable both after clicking each of them. Now it's time to create credentials.
  10. On the left of the site you need to click "Credentials" and then "Create credentials" on the top of the site.
  11. Choose "Service account" from the options. Give it a name (there is no naming convention too).
  12. Click  "Create". As a role you should choose "Editor". Click "continue".
  13. After creating Service account browse your "Service accounts" which can be found on the left bar.
  14. Choose your Service account and click "Keys" bookmark. Then "add key" and finally "create new key".
  15. In "Create key" option choose "JSON" and "Create". Then download the .json file with the credentials.
  16. After downloading credentials file rename it to credentials.json.
  17. In credentials.json select the value of "client_email", go to your google spreadsheet and share permission to edit with that copied email.
