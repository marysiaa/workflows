Error messages for users
===========================

To display a specific message about some particular error to the **IntelliseqFlow** user
(for example: that the uploaded input file has the wrong format) you should:
 * think about command (referred below as *test_command*) which checks the occurrence of that particular error
   and fails on it (giving exit code other than 0);
 * add that command to your task along with the error message written to stderr
 * force task to exit with exit code **86**


This can be done as follow (in the command section of the task):

.. code-block:: bash

  test_command || echo "Task failed with error code 86: YOUR MESSAGE TO USER;" >&2 | exit 86


Notes:
 * the exit code should be **86**
 * the message should start with the phrase: `Task failed with error code 86:` , and end with a semicolon `;`.
 * you can (and should) add the `set -e -o pipefail` statement at the
   beginning of the wdl command section to ensure error checking of the remaining parts of the task


