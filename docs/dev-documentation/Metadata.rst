Metadata
===============

Meta is used to describe all variables in the .wdl file, including inputs, outputs and workflow parameters.
It is also used to show and format them in the interface as .wdl file representation.
There are two exemplary files: meta-test.wdl containing all basic inputs and complementary meta.json with more detailed comments.
This file is located there: ``src/main/wdl/tasks/meta-test/all-inputs/meta-test-all-inputs.wdl`` and it's meta.json: ``src/main/wdl/tasks/meta-test/all-inputs/meta.json``.
You can see meta-test in the interface and test all its parameters.

In the same catalog there are also fields.json files containing inputs, outputs and workflows fields description
used as documentation and used by various scripts (meta-validator, create-meta) located there: ``scripts/meta``.
Let's describe structure of these .json files:

inputs-fields.json
    Description for input parameters used in the interface

    1. default_explanation - used when creating meta from .wdl file
    2. excluded_input_fields - inputs defined in wdl code but they are ignored by interface because they are useless for the user
    3. general_fields - the list of non-variant input fields in meta
    4. variant_specific_fields - the list of variant input fields in meta


workflows-fields.json
    Parameters describing non-input and non-output meta such as general utility fields (for example workflow's name, price, changes)

outputs-fields
    Description for output parameters used in the interface

    1. default_explanation - used when creating meta from .wdl file

All general_fields and variant_specific fields of inputs, outputs and workflows have the same subparameters:
    - field_name - parameter name
    - field_explanation - it's value may be "hard" (then field_example is default value defined in inputs-fields.json) or "auto-complete" (standard, fill based on .wdl code)
    - field_example - an example of a value used to get the type of the parameter while validating meta by script meta-validator
    - list_of_types_with_this_field - the list of wdl types to check with meta-validator
    - obligatory - indicates if this parameter must be in meta.json

Various variants are used to represent many usage scenarios in the interface.
Each variant has its name and other parameters and can be launched in analysis individually.

IMPORTANT:
    1. The analysis variant number must not be changed in workflows released.
    In the parser, nameid is added to the metadata (the words pipeline / module / task will be concatenated with the name wdl and the number of the workflow variant).
    nameid is used by the backend to identify analyzes.
    On the backend side, additional information about the workflow is stored in the database and is identified just by nameid.

    Example: if in the fq-qc module we replace variant 1 with variant 2, then additional information stored by the backend for the wes analysis will be assigned to the ggs analysis and vice versa.

    2. The analysis variants must be sequentially (numbering), there must be no missing one in a row, for example 1, 2, 4.
    In this case, displaying the analyzes in the interface will be a problem.
    Only 1 and 2 will appear.

    3. Workflows that do not have a name field in the meta are filtered out on the frontend side: they must have a name field! (in some older analyzes it may not be there yet).

Our metadata tools
^^^

There is a library called iseqmetatools (https://gitlab.com/intelliseq/iseqmetatools) which encapsulates following functionalities:
    1. Updating meta.json content basing on data from google spreadsheet (python scripts to_inputs, to_outputs and to_workflows)
    2. Generating meta.json from .wdl file inputs and outputs or old-style meta in .wdl (python scripts create_meta and parse_meta)
    3. Converting meta.json file content into other form (you can set it's transform rules)
