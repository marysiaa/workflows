WDL
=========

Creating WDL
^^^^^^^^^^^^^^^^^^^^^

    do tworzenia wdl zawsze używaj skryptu:
    scripts/create-task --name task-name
    or
    scripts/create-module --name module-name
    or
    scripts/create-pipeline --name pipeline-name <- not implemented yet

    link do szczegółów dotyczących tego taska (:ref:`Creating task, module, pipeline`.)

WDL names
^^^^^^^^^^^^^^^^^^^^^

zasady nazywania wdl:
    * have up to 20 characters,
    * be confirmed by marpiech,
    * have "-" insted of "_",

WDL inputs
^^^^^^^^^^^^^^^^^^^^^

info, że istnieją inputy, które zawsze powinny wyglądać tak samo:
sample_id
sample_info_json

info, że przy uruchamianiu Workflows w IntelliseqFlow (:ref:`IntelliseqFlow`) przekazywane są dodatkowe inputy (timeZoneDifference i inne)

WDL outputs
^^^^^^^^^^^^^^^^^^^^^

info, że każdy wdl powinien zwracać poniżej wymienione elementy, dlatego są one zawarte w templatce
bco (:ref:`BCO`)
stderr
stdout

Every output file should have name according to convention: ${sample_id}_output-name.extension1(.extension2)

WDL runtime section
^^^^^^^^^^^^^^^^^^^^^

WDL content
^^^^^^^^^^^^^^^^^^^^^

z templatki do taska dodawane są skrypty do BCO (:ref:`BCO`). Jeśli jest to moduł lub pajplaj dodawane są importy taksów łączących bco
skrypt pythonowy powinien być umieszczony w osobnym pliku (:ref:`Intelliseq tools`)

Imports
^^^^^^^^^^^^^^^^^^^^^

konwencja nazywania zaimportowanych tasków/modułów (as task_name_task i as module_name_module)

import "https://.../task-name.wdl" as task_name_task
import "https://.../task-name.wdl" as task_name_module

Testing WDL
^^^^^^^^^^^^^^^^^^^^^

General information

do testowania wdl używaj skryptu test.sh, który powstaje przy tworzeniu wdl. Skrypt ten uruchamia wdltest. Sprawdź jak tego używać (:ref:`wdltest`)
testowanie na google cloud

How to write a nice test

info, że testy powinny sprawdzać każdy możliwy przebieg wdl
info jak i gdzie dodawać dane do testów (:ref:`Data for testing`)

WDL tips
^^^^^^^^^^^^^^^^^^^^^
