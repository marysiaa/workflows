Docker
=========

Install iseqdockertools lib
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Optional steps (create virtual environment):
::
    python3 -m venv venv
    source tutorial_env/bin/activate


Obligatory steps:
::
    python3 -m pip install --upgrade pip
    pip install iseqdockertools

More info about the package:
https://pypi.org/project/iseqdockertools/

Building docker images
^^^^^^^^^^^^^^^^^^^^^

Script automatically takes name and version (value is set according to semantic version control rules) of the docker image from its header.
It has to be in the following format (at the beginning of the Dockerfile):
::
    # name: name
    # version: 1.0.0

- if docker is created for specific task name of its directory should be: task_task-name; place it here https://gitlab.com/intelliseq/workflows/-/tree/dev/src/main/docker/task
- if docker is not going to be used in wdl script should have ENTRYPOINT line to run script automatically with command ```docker run```
- dockerfile should be created based on: https://gitlab.com/intelliseq/iseqdockertools/-/blob/master/resources/examples/Dockerfile and placed in workflows' repository catalog [src/main/docker/[task-name/script-name]/[version]]
- dockers containing only specific tools are called the name of that tool and stored here https://gitlab.com/intelliseq/workflows/-/tree/dev/src/main/docker
    for example: https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/docker/kallisto/Dockerfile
- if the dockerfile is complex you should describe it in comments
- every tool should be placed in catalog named `/tools/<tool_name>/<tool_version>` due to BCO script requirements; in case of previous presence of that tool in the docker just create empty catalog like here https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/docker/gatk/gatk/Dockerfile

Most dockers are built on a basic docker with the latest (LTS) version of ubuntu https://gitlab.com/intelliseq/workflows/-/tree/dev/src/main/docker/ubuntu-minimal/ubuntu-minimal-20.04. However, there are dockers where we use dockers provided by the developers of the tools, because building this tool from scratch turned out to be super difficult, for example: https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/docker/gatk/gatk/Dockerfile
The one thing you need to remember: include these settings/tools:
:: 
    RUN pip3 install miniwdl==0.7.3
    RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
        dpkg-reconfigure --frontend=noninteractive locales && \
        update-locale LANG=en_US.UTF-8
    ENV LANG en_US.UTF-8

    RUN mkdir -p /intelliseqtools
    ADD src/main/scripts/bco/bco-after-start.sh /intelliseqtools/bco-after-start.sh
    ADD src/main/scripts/bco/bco-before-finish.sh /intelliseqtools/bco-before-finish.sh
    ADD src/main/scripts/bco/bco-meta.py /intelliseqtools/bco-meta.py
    ADD src/main/scripts/bco/bco-versions.py /intelliseqtools/bco-versions.py

    RUN mkdir -p /root/.local/bin && chmod -R go+rx /root

    RUN chmod -R go+rx /var/cache
    RUN chmod -R go+rx /intelliseqtools/

    RUN touch docker_size && chmod go+w docker_size
    RUN touch starttime && chmod go+w starttime
    RUN touch software_bco_apts.bco && chmod go+w software_bco_apts.bco
    RUN touch software_bco_custom.bco && chmod go+w software_bco_custom.bco
    RUN touch software_bco_intelliseq.bco && chmod go+w software_bco_intelliseq.bco
    RUN touch datasource_bco_recources.bco && chmod go+w datasource_bco_recources.bco
    RUN touch meta.json && chmod go+w meta.json
    RUN touch meta.json.tmp && chmod go+w meta.json.tmp
    RUN touch software_ubuntu.bco && chmod go+w software_ubuntu.bco
    RUN touch bco.json && chmod go+w bco.json

    ENV PATH="/root/.local/bin:${PATH}"

They include adding BCO scripts and granting appropriate permissions.

To create a directory use ``mkdir -p`` instead of ``curl --create-dirs``,  because ``curl --create-dirs`` grants permissions 750 for catalogs and 664 for files and we don't want it.
Building has default context to repository and you can use ADD with path to the files from here. 
Default option is to push to the dockerhub. If the dockerfile exists it won't be pushed. To develop dockerimage locally use flag --nopush. 

When you finish developing your Docker please remember to add all tools and databases here https://gitlab.com/intelliseq/iseqresources


Dockers for resources
^^^^^^^^^^^^^^^^^^^^^^^
We decided to keep resources in dockers (instead of the server). The example is there https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/ensembl-genes.
When you design a dockerfile in order to keep resources in you need to place them there https://gitlab.com/intelliseq/workflows/-/tree/dev/resources.
Docker images for big resources should be done with the dockerfile with the ubuntu operating system (to prevent coping from docker image to local storage).

Launching docker images in interactive mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
It is possible to access dockers' content without unpacking it.
It can be beneficial if you want to run some tests inside.
You just need to run the docker in interactive mode.
In order to do this choose the repository (or script's directory) and other files to attach as volume (`docker run -v` option, more info here: https://docs.docker.com/storage/volumes/#choose-the--v-or---mount-flag).
For example:
::
    docker run -it -v /path/to/repository:/repository yourdockerimage:version

You can add the `--rm` option to clean up the container and remove the file system when exiting (more info here: https://docs.docker.com/engine/reference/run/#clean-up---rm).
For example:
::
    docker run --rm -it -v /path/to/repository:/repository yourdockerimage:version

REMEMBER: DO NOT MODIFY ATTACHED VOLUME FILES INSIDE THE DOCKER (it can affect the permissions).
Do it locally instead. It is modified both locally and inside the docker then.

Implement docker into WDL
^^^^^^^^^^^^^^^^^^^^^^^^^^^
While developing WDL script you should define variable named `docker_image` which is `String` type.
This will be used in `runtime` section. For example:
::
    String docker_image = "intelliseqngs/annotsv:" + reference_genome + "_1.0.0"


Building images script usage (when you are in workflows' root directory):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

General usage:

    ``dockerbuilder -d path/to/dockerfile``

for example: 
::
    dockerbuilder -d src/main/docker/reports/reports/Dockerfile

Script dockerbuider contains --help section with all the flags:

1. Print 'docker build' and 'docker push' logs
::
    dockerbuilder -d path/to/dockerfile --quiet

2. Do not push docker image(s) to repository
::    
    dockerbuilder -d path/to/dockerfile --nopush

3. Do not use cache when building the docker image(s)
::
    dockerbuilder -d path/to/dockerfile --nocache

4. Build separate images for all chromosomes
::
    dockerbuilder -d path/to/dockerfile --chromosome

5. Push image even if exists in repository
::
    dockerbuilder -d path/to/dockerfile --forcepush

6. Set current directory as context
::    
    dockerbuilder -d path/to/dockerfile --context


Name of the docker image vs location of its dockerfile
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Location: 
```src/main/docker/[catalog-name]/[version]/Dockefile```

Name of the image: ```intelliseqngs/[catalog-name]:[version]```
