BCO
=======

This section provides basic information about BCO (BioCompute Object).
Scripts for generating BCO and for merging multiple BCO into one are also described in this section.

What is BCO?
^^^^^^^^^^^^^^

BioCompute is a standardized way to communicate an analysis pipeline. BioCompute substantially improves the clarity and
reproducibility of an analysis, and can be packaged with other standards, such as the Common Workflow Language.
An analysis that is reported in a way that conforms to the BioCompute specification is called a BioCompute Object (BCO).
A BCO provides the structure that dictates what information must be present in a report [#]_.

A BCO includes [#]_:

- Information about parameters and versions of the executable programs in a pipeline
- Reference to input and output test data for verification of the pipeline
- A usability domain
- Keywords
- A list of agents involved along with other important metadata, such as their specific contribution

Scripts for generating BCO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All scripts generating BCO are in the repository under the path: `src/main/scripts/bco` [#]_.

.. list-table:: BCO scripts
   :widths: 75 50 250
   :header-rows: 1

   * - Name
     - Version
     - Description
   * - bco-after-start.sh
     - 1.2.3
     - Script for getting information about the size of the docker and the start time of the task.
   * - bco-before-finish.sh
     - 1.3.0
     - Script that runs other scripts to generate a BCO (`bco-meta.py`, `bco-versions.py`), aggregates all information and produces the resulting BCO file.
   * - bco-meta.py
     - 1.3.0
     - Script for getting information about metadata from a WDL task or `meta.json` file, located in the same directory as the WDL task.
   * - bco-versions.py
     - 1.2.3
     - Script fot getting information from the docker about the versions of used tools, scripts and resources.
   * - bco-merge.py
     - 1.0.1
     - Script for merging multiple BCO files into one resulting BCO file.

The repository also includes a module that generates a report (pdf, docx, odt and html formats)
and a csv table from the input BCO files [#]_.
The module consists of the tasks presented in the table below.

.. list-table:: BCO workflows
   :widths: 75 50 100
   :header-rows: 1

   * - Name
     - Version
     - Description
   * - bco-merge.wdl
     - 1.4.1
     - Merges bco, stdout and stderr files from previous tasks.
   * - report-bco.wdl
     - 1.0.1
     - Generates a report in pdf, docx, odt and html formats.
   * - bco-json-to-csv.wdl
     - 2.0.1
     - Converts BCO in json format to csv format.

BCO validation
^^^^^^^^^^^^^^^^

The correctness of the BCO is checked in the following places:

- `bco-meta.py` - validates metadata using miniwdl python library and in case of a metadata misinterpretation, the described errors will be redirected to the standard error (stderr),
- `bco-merge.py` - checks the correctness of input bco files to be merged and in the case of missing basic information (e.g. incorrect description domain) in the file, redirected an error to the standard error (stderr),
- `wdltest` [#]_ - utility that runs tests to WDL tasks and then checks for the existence of domains (provenance domain, execution domain, parametric domain, description domain) in the resulting `bco.json` file.

^^^^^^^^^^

.. [#] https://www.biocomputeobject.org/
.. [#] https://github.com/biocompute-objects/BCO_Specification
.. [#] https://gitlab.com/intelliseq/workflows/-/tree/dev/src/main/scripts/bco
.. [#] https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/wdl/modules/bco/bco.wdl
.. [#] https://github.com/marpiech/wdltest

