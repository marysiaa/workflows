Developer tools
===================

Creating task, module, pipeline
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To create wdl always use script:
scripts/create-task --name task-name
or
scripts/create-module --name module-name
or
scripts/create-pipeline --name pipeline-name <- not implemented yed
How does it work?
(to na dole trzeba uprzątnąć tak, żeby miało też sens dla modułów i pajplajnów)
Script creates wdl and test files based on git user.name and task/module/pipeline name.
create-task creates proper directories in scr/main/wdl/tasks/task-name/ and src/test/wdl/tasks/task-name/ and puts there:
WDL scripts: src/main/wdl/tasks/task-name/task-name.wdl
Script to run test for task-name task: src/test/wdl/tasks/task-name/test.sh
Test for task-name task: src/main/wdl/tasks/test-test/test.json
Additionally, src/test/wdl/tasks/task-name/ is symlinked to src/main/wdl/tasks/task-name/ directory.
info, że templatki do test.sh i test.json są trzymane w https://gitlab.com/intelliseq/workflows/-/blob/dev/docs/templates/task-template, a w module-template i pipeline-template są symlinki do test.sh i test.json z task-template (więc jak coś zmieniamy to wystarczy w jednym miejscu)

Testing wdl scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Creating template
####################

Each wdl script should contain test. Test files are created automatically during wdl creation. Please modify test.json to specify inputs and test outputs.

All files needed to create test for specific wdl script are prepared automatically during wdl creation:

for task:
::

    scripts/create-task -n some-task

for module:
::

    scripts/create-module -n some-module

for pipeline:
::

    scripts/create-pipeline -n some-pipeline

Created files appear in test directory for specific wdl.
Example:
test files for wdl
::
    src/main/wdl/tasks/pgx-aldy/pgx-aldy.wdl

appears in directory:
::
    src/test/wdl/tasks/pgx-aldy/

Files:

- test.sh
- test.json

Editing template
######################

To test your wdl please edit test.json. This json is only the template. It is not ready to test your scripts. You need to fill fields by yourself:

**1. Specify wdl inputs:**

.. code-block:: json

    "inputs": {
        "some_input": "${ROOTDIR}/src/test/resources/data..."
    }

Do not write inputs by yourself, you can use special tool:
::
    womtool inputs path_to_your_wdl

...and copy inputs you need to test.json.

Please provide path for test files (if input is File type) using example path with **${ROOTDIR}** at the beginning the same as example above.

**2. Fill section "conditions"**


In section "conditions" there is one example how test should be for each output you want to test.:
::
    "file": "OUTPUT-FILE-TO-ADD"

- provide name of your output, exactly the same as is provided in wdl output section,
- specify name and error message for this test,
- specify command in bash. Example checks if file exists, but you can make some grep or validation.
n
You can make multiple tests for each output.

**3. Give a name for your test**

In section "name" instead of "Primary test":

.. code-block:: json

    "name": "Primary test"

**4. Add more tests for "tests"**

Fill array "tests" using the same schema as in 1, 2 and 3 points.

Why more tests? Look at your wdl script.

- If you have optional inputs - please provide them in one test, in another not,
- If you have Boolean inputs - run wdl with True option and False option.
- If wdl input is bam, vcf, fastq or some bioinformatics format provide at least 2 different test samples in separated test,
- Do you have reference genome to choose in your wdl script? Please test all reference genomes possible,
- and more you think is important to check

Usage of your test
##########################

**1. run your test:**
::

    test.sh

To run only one test from your "tests" section in test.json, please provide index order of this test, according to tests order in json:
::

    test.sh -i 1

**2. Look on logs and outputs**

Paths to outputs and logs will be printed in terminal.  Outputs of cromwell-executions always appear in **/tmp** directory.
To see logs and the same write them to file please use **tee**:
::

    test.sh | tee logs.log


Testing data
#############

- You can use all files in workflows repository to test your script, providing path in inputs. But main place for testing data is in `src/test/resources/data/`
- Data is divided in directories by extension (bed, fastq, vcf, bam, json, txt and more).
- Before choosing some data please read documentation (each data directory should contain readme).
- Before placing your own data in directory please write your documentation about this in readme (how was created, for what purpose, more information you will provide the better)

WDL tagging
^^^^^^^^^^^^^^
Firstly, you need to install GitPython and packaging with your pip (preferably in your venv).
For example:
    ``pip install GitPython``
and
    ``pip install packaging``

While using vscode as IDE you need to turn off terminal authentication in Settings (press 'Ctrl + ,' and search for git.terminalAuthentication).

Script usage on example:

    ``WDL="src/main/wdl/pipelines/germline/germline.wdl"``

    ``python3 scripts/wdl-tagger.py -w $WDL``

If you wish to tag a workflow without the proper meta.json file you can add the `-i` flag.
For example:

    ``python3 scripts/wdl-tagger.py -w $WDL -i``

Where:
    | WDL is the path to tagged .wdl file. In the same directory should be meta.json
For example: ``/path/to/wdl/file/file.wdl`` and ``/path/to/wdl/file/meta.json``.
This is really important to name these files as mentioned.
Script looks for them named like that in chosen directory.
Tag version is taken from .wdl file.
Script looks for the first line containing ``String`` and ``version`` keywords with one of the following words: ``pipeline/module/task``.

Wdl-tagger:
    | 1. Compares version in wdl and meta.json file
    | 2. Gets to the workflows repository
    | 3. Fetches the workflows repository
    | 4. Creates a new tag based on workflow name (finds pipeline/module/task name in wdl file) and workflow version
    | 5. Checks if the new tag is not occupied and compares it with latest version in repository (to check if it's latest)
    | 6. Compares wdl from latest tag version with wdl from dev and checks if it's content is the same
    | 7. Pushes new tag to the repository

Preparing wdl script to run using interface
^^^^^^^^^^^^^^^^^^^^^^^^^^

Every wdl (task, module or pipeline) need to create meta.json in the same folder as wdl file, to run using interface.
- Meta.json is not obligatory for each wdl.
- Meta.json contains general information about wdl script and describes inputs and outputs.
- Meta.json works only if is on dev branch.
- Meta.json is creeated automatically, only once and can be validated using validation script.
- *If you want to use meta section inside wdl script check section "Parsing meta to json"

Creating meta.json from scratch
###############################


**Short instruction:**

Before reading instruction make sure you have iseqmetatools package installed.
Link: https://pypi.org/project/iseqmetatools/

**1. Commit all changes**
* This step is needed to create "author" field, based on git-blame.

**2. Run script:**
::

    create_meta \
    --path /path/to/wdl > /new/meta/directory


You can also change templates configuration path (if the fields.json change before iseqmetatools update):

::
    create_meta \
    --path /path/to/wdl
    --templates-path /path/to/templates > /new/meta/directory


**3. Edit meta.json**

- open new_meta.json,
- follow instructions written in each "description":
    - remove or keep fields with phrase "OPTIONAL field" and define proper value,
    - define fields with phrase "OBLIGATORY field"


**4. Validate meta.json**

Please check section Validating meta

**4. Move new_meta.json to wdl directory**
::

    mv new_meta.json PATH_TO_WDL_DIR/meta.json


**Detailed information**


Meta is created automatically based on wdl script and 3 files:

- workflows-fields.json
- inputs-fields.json
- outputs-fields.json

They are located in the iseqmetatools package.
Specific fields with default value (usually always the same) are defined:

- for inputs-fields.json and outputs-fields.json:
    in section ``"default_explanations"``. Example of field with defaut description: sample_id, bco.
    If you want to create default description for some field please add it to ``default_explanation`` and use name of input/output the same as in wdl as key and default description as value.

- for workflows-fields.json:
    if "field_explanation" = "hard" "field_example" is default value for this field.
    If you want to change some field for default value please set "field_explanation" to "hard" and write default text in "field_example".

Some values are created automatically, based on wdl code. Every value with this functionality has value "auto-complete" in key "field_explanation":
`"field_explanation": "auto-complete"`
To better understand how does it works there are some exapmles with tips how to edit it:

**1. Extensions**
Field `extensions` for inputs is created based on input name. It detects some typical extensions (json, tsv, vcf) in input name and automatically create this field:
    `input_json' -> `[".json"]`

    `input_gvcf` -> `[".gvcf.gz", ".g.vcf.gz"]`
If you run create_meta.py and you have Exception: `Cannot generate extensions for File: `you have to change python code. It is easy, look for function `define_extensions`

**2. Type**

Field "type" is created automatically based on wdl data type.

*3. Index*

Field "index" is created for all inputs using lines order in wdl code.

**4. Name**

Its just name of wdl file

**5. Author**

Based on git-blame

Not "auto-complete" fields have automatically created instructions how to define them. Please note that auto-complete fields described above can be not actuall in the future beacause of some update.

Excluded inputs

Some inputs are defined in wdl code but we do not want to have them in meta - there are useless for interface user.
Script excludes those inputs by looking phrases described in field "excluded_input_fields" in inputs-fields.json. Please note - values defined in "excluded_input_fields" do not need to be full names of some input. It can be some phrase.
If you want to exclude some input from meta please add those name or phrase always presented in this kind of input in those section. Example of input useless for interface user:

`Boolean is_fastqs_defined = defined(fastqs)` - this input is excluded from meta.json based on `defined` phrase inside


Parsing meta to json
^^^^^^^^^^^^^^^^^^^^^^^

Before reading instruction make sure you have iseqmetatools package installed.
Link: https://pypi.org/project/iseqmetatools/

Creating meta.json from meta that already are inside wdl code in "meta" section:
::

    parse_meta \
    --path /path/to/wdl


Validating meta
^^^^^^^^^^^^^^^^^^^

To validate meta follow these steps:
- while on the interface click up-right corner (your name)
- choose admin panel
- log in
- at the left side of the page you will see Navigation column
- choose Tools
- choose Workflow validator
- enter workflow url to verify (remember to paste "raw" link)
- click "Run validation"


Parsing google spreadsheet to meta
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Firstly, you need to generate your own credentials.json file.
It can be done with this instruction: :ref:`Generate google credentials`.
Install gspread library using your pip with pip install gspread.
Now you just need to modify the values in google spreadsheet and then run script to change meta.json of the chosen workflow.
The google spreadsheet which was mentioned above is here: https://docs.google.com/spreadsheets/d/12iYYF_JV-xu_dAkgDndIZtMPiMV-iAwIV7kIoVAHn80/edit.
There are three scripts (for inputs, outputs and general workflows parameters), the update process is described below.
Every beginning or ending whitespaces in keys or values will be removed (for example if you type in google worksheet something like "File " it will be parsed to "File").

Package iseqmetatools installation
############

Optional steps (create virtual environment):
::

    python3 -m venv venv
    source venv/bin/activate

Obligatory steps:
::

    python3 -m pip install --upgrade pip
    pip install iseqmetatools


Meta update
#############
You can parse inputs, outputs or workflows parameters for the workflow.
Example use for workflows parameters (after installing):

Fill:

    - `--workflow-name` workflow's name you want to modify
    - `--credentials-json` a path to credentials.json generated in a way described in a section above
    - `--meta-json` a path to workflow's meta.json file
    - `--google-spreadsheet-key` can be found in your published Google sheets URL; select the string of data found between the slashes after spreadsheets/d in your Google Sheet URL
    - `--output-meta-json` a path to modified meta.json file


For example:
::

    parse_inputs \
        --workflow-name "germline" \
        --credentials "/path/to/credentials.json" \
        --meta-json "/path/to/wdl/meta.json" \
        --spreadsheet-key "key" \
        --output-meta-json "/path/to/output/meta.json"

::

    parse_outputs \
        --workflow-name "germline" \
        --credentials "/path/to/credentials.json" \
        --meta-json "/path/to/wdl/meta.json" \
        --spreadsheet-key "key" \
        --output-meta-json "/path/to/output/meta.json"

::
    
    parse_workflows \
        --workflow-name "germline" \
        --credentials "/path/to/credentials.json" \
        --meta-json "/path/to/wdl/meta.json" \
        --spreadsheet-key "key" \
        --output-meta-json "/path/to/output/meta.json"


Converting meta.json after an update of meta rules
^^^^^

You can convert meta.json just by providing a json file containing translated new keys and removed old keys in meta.json conversion.
An example of this file:

    {
        "rename": {
            "workflows": {},
            "inputs": {"extension": "constraints.extensions", "paired": "constraints.paired", "groupname": "groupId"},
            "outputs": {"groupname": "groupId"},
            "groups": {"hReadableName": "name", "minInputs": "constraints.minInputs", "maxInputs": "constraints.maxInputs"}
        },
        "remove": {
            "workflows": ["keywords"],
            "inputs": ["multiselect", "constraints.multiselect"],
            "outputs": [],
            "groups": []
        }
    }



"Rename" section stands for changing keys names or even embedding keys into subdict structure.
"Remove" section takes list of parameters to remove.

After preparing such a file you can start conversion:

::
    
    convert_meta \
    --input-meta-json /path/to/meta.json
    --input-dict-json /path/to/translated-keys.json
    --output-meta-json /path/to/meta.json


Google cloud
^^^^^^^^^^^^^^^

Authorization
#################

To get access to the gcloud commands and it's content you need to authorize yourself using credentials.
It can be done through logging in on anakin and executing command: ``/opt/auth/gcp-auth-cromwell``.
The granted access is valid only on anakin.
