Intelliseq tools
=====================

Info stąd: https://gitlab.com/intelliseq/workflows/-/tree/master#4-developing-scripts-python-bash-r-jinja-templates-used-in-wdl-commandline
brakuje info, że każdy skrypt powinien w helpie mieć dokładne info o tym, co on robi (tak, żeby pisanie dodatkowej dokumentacji do skryptów było niepotrzebne - chyba, że są to jakieś bardzo zaawansowane analizy np. acmg)

General information
^^^^^^^^^^^^^^^^^^^^^^

Python scripts
Bash scripts
R scripts

Generating polygenic models
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Firstly, setup polygenic library for Python.
You need to get Python version 3.8 or above to get proper libraries.
Install polygenic using instructions under this link: https://github.com/intelliseq/polygenic/blob/master/docs/polygenicmaker.md

After getting the model in `.yml` format you are ready to execute the Python script which adds or updates description key
based on google sheets content.

Script is located in `src/main/scripts/tools/health-google-spreadsheet-to-yaml.py`

Script looks for a description key in a yaml file. There are two options:
    1) description key is found and renamed to descriptions and it's value is replaced with content from google spreadsheet
    2) descriptions key is created and it's value is filled with content from google spreadsheet

Usage:

    ``credentials=/path/to/google/credentials.json``

    ``key=1fLemB1hO9Hn6C3oqaOpKzyw6mrZ3ES_bp9lNccXdrNU``

    ``model=/path/to/model/you/want/to/modify/model.yml``

    ``out=/path/to/output/file/described-model.yml``

    ``python3 health-google-spreadsheet-to-yaml.py -c $credentials -k $key -m $model -o $out``

Where:

- `credentials.json` can be generated in a way described in https://gitlab.com/intelliseq/workflows#parsing-google-spreadsheet-to-meta
- `key` can be found in your published Google sheets URL; select the string of data found between the slashes after spreadsheets/d in your Google Sheet URL
- `model` is the file in which descriptions key would be added or updated
- `out` is the name of the resulting file

To test the script you need to modify the credentials-json input in the path `src/test/scripts/tools/health-google-spreadsheet-to-yaml.sh` and run it.

Remember to remove the path to your `credentials.json` file before pushing to repo!