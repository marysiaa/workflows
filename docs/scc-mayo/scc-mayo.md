### Proposed workflows for Mayo clinic presentation
```
### repo with large data
git clone https://gitlab.com/intelliseq/workflows.git
cd workflows/src/test/resources/
rm large-data
wget --no-parent -r http://anakin.intelliseq.pl/public/large-data
mv anakin.intelliseq.pl/public/large-data .
rm -r anakin.intelliseq.pl
cd ../../..
```

```
### testing framework
sudo -H pip3 install --upgrade --no-deps wdltest==1.0.7
```

```
### proposed workflow 1
### acmg classification
src/test/wdl/modules/vcf-acmg-report/test.sh
```

```
### proposed workflow 2
### pharmacogenomic pharmcat report
src/test/wdl/modules/pgx-genotyping-report/test.sh
```

```
### proposed workflow 3
### Panel coverage quality and sensitivity test
src/test/wdl/modules/detection-chance/test.sh
```

Warto też zastanowić się nad uruchomieniem workflowu z dwolonego repozytorium np. 
[Linkd do repozytorium GATK](https://github.com/gatk-workflows/gatk4-basic-joint-genotyping)

Zobaczcie też na dockstore i jak broad umożliwia import tych workflowów do dockstore
[Link do Dockstore](https://dockstore.org/)
[Link do yml w repo GATK](https://github.com/gatk-workflows/utility-wdls/blob/main/.dockstore.yml)
