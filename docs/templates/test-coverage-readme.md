### Installation
```bash
pip install pytest-cov
```

### Example use
```bash
pytest --cov-report term-missing --cov=python_script_template .
```
